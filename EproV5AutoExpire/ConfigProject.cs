﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Net.Mail;
using OfficeMate.Framework.Security.Cryptography;

namespace EproV5AutoExpire
{
	public abstract class ConfigProject
	{
		public static bool IsProduction
		{
			get { return Boolean.Parse(ConfigurationManager.AppSettings["IsProduction"]); }
		}
		public static string SmsUserName
		{
			get { return ConnectionStringEncryption.Decrypt(ConfigurationManager.AppSettings["SmsUserName"]); }
		}
		public static string SmsPassword
		{
			get { return ConnectionStringEncryption.Decrypt(ConfigurationManager.AppSettings["SmsPassword"]); }
		}
		public static string SmsSenderName
		{
			get { return ConnectionStringEncryption.Decrypt(ConfigurationManager.AppSettings["SmsSenderName"]); }
		}
		public static string ConnectionString
		{
			get
			{
				if (IsProduction)
				{
					return ConfigurationManager.ConnectionStrings["ProductionServer"].ConnectionString;
				}
				else
				{
					return ConfigurationManager.ConnectionStrings["DevelopmentServer"].ConnectionString;
				}
			}
		}
		public static string SmtpServer
		{
			get
			{
				if (IsProduction)
				{
					return ConfigurationManager.AppSettings["ProductionSmtpServer"];
				}
				else
				{
					return ConfigurationManager.AppSettings["DevelopmentSmtpServer"];
				}
			}
		}
		public static string SMTPUsername
		{
			get
			{
				if (IsProduction)
				{
					return ConfigurationManager.AppSettings["ProductionSmtpUsername"];
				}
				else
				{
					return ConfigurationManager.AppSettings["DevelopmentSmtpUsername"];
				}
			}
		}
		public static string SMTPPassword
		{
			get
			{
				if (IsProduction)
				{
					return ConfigurationManager.AppSettings["ProductionSmtpPassword"];
				}
				else
				{
					return ConfigurationManager.AppSettings["DevelopmentSmtpPassword"];
				}
			}
		}
		/*************** Email Send Out Of System ********************/
		public static string EmailFromAddressName()
		{
			if (IsProduction)
			{
				return "eprocurement@officemate.co.th";
			}
			else
			{
				return "contact@trendyprint.int";
			}
		}
		/*************** Mail BCC Admin ********************/
		public static List<MailAddress> EmailAddressBCCToAdmin()
		{
			List<MailAddress> mailto = new List<MailAddress>();
			if (IsProduction)
			{
				mailto.Add(new MailAddress("officemate.th@gmail.com"));
			}
			else
			{
				mailto.Add(new MailAddress("contact@trendyprint.int"));
			}
			return mailto;
		}
		/*************** Mail Summary To Admin ********************/
		public static List<MailAddress> EmailAddressSummeryToAdmin()
		{
			List<MailAddress> mailto = new List<MailAddress>();
			if (IsProduction)
			{
				mailto.Add(new MailAddress("servicedesk@cenergy.co.th"));
				mailto.Add(new MailAddress("surapon@cenergy.co.th"));
				mailto.Add(new MailAddress("waraporn.ki@cenergy.co.th"));
				mailto.Add(new MailAddress("duangnapa@cenergy.co.th"));
			}
			else
			{
				mailto.Add(new MailAddress("surapon@cenergy.co.th"));
				mailto.Add(new MailAddress("waraporn.ki@cenergy.co.th"));
				mailto.Add(new MailAddress("duangnapa@cenergy.co.th"));
			}
			return mailto;
		}

	}
}
