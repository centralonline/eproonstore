﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using EproV5AutoExpire.Models;
using System.Data;

namespace EproV5AutoExpire
{
	class ProductRepository
	{
		public SqlConnection connection { get; set; }
		public ProductRepository(SqlConnection _connection) { connection = _connection; }

		public List<ProductNotifyMe> GetProductNotifyMe()
		{
			string SqlString = @"SELECT	nm.NotifyID as 'Id',nm.PID as ProductId,pc.pTName,pc.pEName,nm.UserID as 'NotifyMeEmail',nm.Remark as 'Remark',
										isnull(nm.MobileNo,'') as 'MobileNo',cm.DefaultLang,nm.CompanyID,cm.UseSMSFeature,UI.UserTName,UI.UserEName
								FROM	TBNotifyMe as nm WITH (NOLOCK) INNER JOIN TBProductCenter as pc WITH (NOLOCK) ON nm.PID = pc.pID 
										INNER JOIN TBCompanyInfo as cm WITH (NOLOCK) ON cm.CompanyID = nm.CompanyID
										INNER JOIN TBUserInfo as ui WITH (NOLOCK) ON ui.UserID = nm.UserID
								WHERE	nm.NotifyMailsuccess = 'No' AND pc.Status IN ('Stock','Limited','NonStock') AND DATEDIFF(day,nm.CreateOn,getdate()) < 30";
			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				List<ProductNotifyMe> items = null;
				try
				{
					cmd.CommandType = CommandType.Text;
					connection.Open();
					using (SqlDataReader reader = cmd.ExecuteReader())
					{
						ProductNotifyMe notify = null;
						items = new List<ProductNotifyMe>();
						while (reader.Read())
						{
							notify = new ProductNotifyMe();
							notify.NotifyID = (int)reader["id"];
							notify.ProductID = (string)reader["productId"];
							notify.ProductThaiName = (string)reader["ptname"];
							notify.ProductEngName = (string)reader["pename"];
							notify.NotifyMeEmail = (string)reader["notifymeemail"];
							notify.NotifyRemark = (string)reader["remark"];
							notify.MobileNo = (string)reader["MobileNo"];
							notify.IsUseSMSFeature = reader["UseSMSFeature"].ToString().Equals("Yes") ? true : false;
							notify.Language = (ProductNotifyMe.NotifyLanguage)Enum.Parse(typeof(ProductNotifyMe.NotifyLanguage), (string)reader["DefaultLang"]);
							notify.CompanyId = (string)reader["CompanyID"];
							notify.UserTName = (string)reader["UserTName"];
							notify.UserEName = (string)reader["UserEName"];
							items.Add(notify);
						}
					}
					connection.Close();
				}
				catch (Exception e)
				{
					Log4me.SaveLog(e);
				}
				return items;
			}
		}
		public void UpdateMailSuccess(int Id)
		{
			string SqlString = @"Update tbnotifyme SET  NotifyMailsuccess = 'Yes' WHERE NotifyID = @NotifyID";
			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				cmd.CommandType = CommandType.Text;
				cmd.Parameters.Clear();
				cmd.Parameters.AddWithValue("@NotifyID", Id);
				connection.Open();
				cmd.ExecuteNonQuery();
				connection.Close();
			}
		}
	}
}
