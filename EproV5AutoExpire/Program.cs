﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using EproV5AutoExpire.Models;
using System.Data;
using OfficeMate.SmsService;

namespace EproV5AutoExpire
{
	class Program
	{
		static void Main()
		{
			try
			{

				string Message = ""; //ใช้ส่งเมล์สรุปหา admin

				Log4me.SaveLog("[EproV5 Execute Task] Start Application");
				Console.WriteLine("[EproV5 Execute Task] Start Application Time: {0}", DateTime.Now.ToString());
				Message += string.Format("[EproV5 Execute Task] Start Application Time: {0} <br/>", DateTime.Now.ToString()); //Start Task

				Console.WriteLine(ConnectDataBaseTest()); //Connect DataBase

				Console.WriteLine("*******************************************");
				Console.Write("\n\n\n");
				Message += "******************************************* <br/><br/><br/>";

				Log4me.SaveLog("Execute Expire Process");
				Console.WriteLine("Execute Expire Process");
				Console.WriteLine("-------------------------");
				Message += "Execute Expire Process <br/> ------------------------- <br/> ";
				Message += SetOrderExpire();
				Console.Write("\n\n\n");
				Message += "<br/><br/><br/>";

				Log4me.SaveLog("Execute Warning Process");
				Console.WriteLine("Execute Warning Process");
				Console.WriteLine("-------------------------");
				Message += "Execute Warning Process <br/> ------------------------- <br/> ";
				Message += SetOrderWarning();
				Console.Write("\n\n\n");
				Message += "<br/><br/><br/>";

				Log4me.SaveLog("Execute Shipped Process");
				Console.WriteLine("Execute Shipped Process");
				Console.WriteLine("-------------------------");
				Message += "Execute Shipped Process <br/> ------------------------- <br/> ";
				Message += SendOrderShipped();
				Console.Write("\n\n\n");
				Message += "<br/><br/><br/>";

				Log4me.SaveLog("Execute Notify Product Process");
				Console.WriteLine("Execute Notify Product Process");
				Console.WriteLine("-------------------------");
				Message += "Execute Notify Product Process <br/> ------------------------- <br/> ";
				Message += SendNotifyProduct();
				Console.Write("\n\n\n");
				Message += "<br/><br/><br/>";

				Console.WriteLine("*******************************************");
				Console.Write("\n");
				Message += "******************************************* <br/>";

				Log4me.SaveLog("[EproV5 Execute Task] End Application");

				Console.WriteLine("[EproV5 Execute Task] End Application Time: {0}", DateTime.Now.ToString());
				Message += string.Format("[EproV5 Execute Task] End Application Time: {0} <br/>", DateTime.Now.ToString()); //End Task

				Message += "******************************************* <br/><br/><br/>";

				SendExecuteTask(Message);

				//Console.Read();
			}
			catch (Exception e)
			{
				Log4me.SaveLog(e);
			}

		}

		private static string SetOrderExpire()
		{
			using (SqlConnection connection = new SqlConnection(ConfigProject.ConnectionString))
			{
				string subjectMail, Message = "";
				var orderRepo = new OrderRepository(connection);
				bool isSendMailComplete = false;
				bool isSendMailCompleteApprover = false;
				bool isSendMailForwardComplete = false;
				bool isSendMailForwardCompleteApprover = false;
				string tempContentThai = clsMailService.GetMailContent(clsMailService.mailType.ExpireToRequester, clsMailService.mailLanguage.THAI);
				string tempContentEng = clsMailService.GetMailContent(clsMailService.mailType.ExpireToRequester, clsMailService.mailLanguage.ENGLISH);
				string tempContentForwardThai = clsMailService.GetMailContent(clsMailService.mailType.ForwardOrderToRequester, clsMailService.mailLanguage.THAI);
				string tempContentForwardEng = clsMailService.GetMailContent(clsMailService.mailType.ForwardOrderToRequester, clsMailService.mailLanguage.ENGLISH);
				string tempContentApproverThai = clsMailService.GetMailContent(clsMailService.mailType.ExpireToApprover, clsMailService.mailLanguage.THAI);
				string tempContentApproverEng = clsMailService.GetMailContent(clsMailService.mailType.ExpireToApprover, clsMailService.mailLanguage.ENGLISH);
				string tempContentForwardNextApproverThai = clsMailService.GetMailContent(clsMailService.mailType.ForwardOrderToApprover, clsMailService.mailLanguage.THAI);
				string tempContentForwardNextApproverEng = clsMailService.GetMailContent(clsMailService.mailType.ForwardOrderToApprover, clsMailService.mailLanguage.ENGLISH);

				List<Order> Orders = orderRepo.GetOrderExpire();

				if (Orders != null)
				{
					foreach (var order in Orders)
					{
						subjectMail = "";
						Console.Write(order.OrderId);
						Message += order.OrderId;
						if (String.IsNullOrEmpty(order.NextAppId)) //ไม่มีผู้อนุมัติคนต่อไประบบจะ Expire
						{
							if (order.Language == OrderLanguage.TH)
							{
								subjectMail += "OfficeMate e-Procurement ใบขอซื้อเพื่อการอนุมัติเลขที่ : " + order.OrderId + " ได้หมดอายุแล้ว";
								isSendMailComplete = clsMailService.SendMailExpireToRequester(order, subjectMail, tempContentThai);
							}
							else
							{
								subjectMail += "OfficeMate e-Procurement This PO No. " + order.OrderId + " has currently expired.";
								isSendMailComplete = clsMailService.SendMailExpireToRequester(order, subjectMail, tempContentEng);
							}
							if (order.CurrentApproverLanguage == OrderLanguage.TH)
							{
								subjectMail += "OfficeMate e-Procurement ใบขอซื้อเพื่อการอนุมัติเลขที่ : " + order.OrderId + " ได้หมดอายุแล้ว";
								isSendMailCompleteApprover = clsMailService.SendMailExpireToApprover(order, subjectMail, tempContentApproverThai);
							}
							else
							{
								subjectMail += "OfficeMate e-Procurement This PO No. " + order.OrderId + " has currently expired.";
								isSendMailCompleteApprover = clsMailService.SendMailExpireToApprover(order, subjectMail, tempContentApproverEng);
							}

							orderRepo.SetStatusExpire(order.OrderId);
							orderRepo.InsertOrderActivity(order.OrderId, true);

							if (isSendMailComplete)
							{
								Console.Write(" [OK - Send Expire Mail To Requester]");
								Message += " [OK - Send Expire Mail To Requester]";
							}
							else
							{
								Console.Write(" [Error - Send Expire Mail To Requester]");
								Message += " [Error - Send Expire Mail To Requester]";
							}
							if (isSendMailCompleteApprover)
							{
								Console.Write(" [OK - Send Expire Mail To Approver]");
								Message += " [OK - Send Expire Mail To Approver]";
							}
							else
							{
								Console.Write(" [Error - Send Expire SendMail To Approver]");
								Message += " [Error - Send Expire SendMail To Approver]";
							}
						}
						else
						{
							switch (order.Status)
							{
								case OrderStatus.Waiting:
								case OrderStatus.Partial:
								case OrderStatus.AdminAllow:
									if (order.Language == OrderLanguage.TH)
									{
										subjectMail += "OfficeMate e-Procurement ใบขอซื้อเพื่อการอนุมัติ เลขที่ : " + order.OrderId + " ถูกส่งต่อไปยังผู้อนุมัติลำดับถัดไป";
										isSendMailForwardComplete = clsMailService.SendMailForwardOrderToRequester(order, subjectMail, tempContentForwardThai);
									}
									else
									{
										subjectMail += "OfficeMate e-Procurement This PO No. " + order.OrderId + " has been forwarded to next approval level";
										isSendMailForwardComplete = clsMailService.SendMailForwardOrderToRequester(order, subjectMail, tempContentForwardEng);
									}
									if (order.NextApproverLanguage == OrderLanguage.TH)
									{
										subjectMail += "OfficeMate e-Procurement ใบขอซื้อเพื่อการอนุมัติ เลขที่ : " + order.OrderId + " ถูกส่งต่อไปยังผู้อนุมัติลำดับถัดไป";
										isSendMailForwardCompleteApprover = clsMailService.SendMailForwardOrderToApprover(order, subjectMail, tempContentForwardNextApproverThai);
									}
									else
									{
										subjectMail += "OfficeMate e-Procurement This PO No. " + order.OrderId + " has been forwarded to next approval level";
										isSendMailForwardCompleteApprover = clsMailService.SendMailForwardOrderToApprover(order, subjectMail, tempContentForwardNextApproverEng);
									}
									orderRepo.AutoForwordApprove(order);
									orderRepo.InsertOrderActivity(order.OrderId, false);
									break;
								case OrderStatus.WaitingAdmin:
									orderRepo.SetStatusExpire(order.OrderId);
									orderRepo.InsertOrderActivity(order.OrderId, true);
									break;
							}

							if (isSendMailForwardComplete)
							{
								Console.Write(" [OK - Send Expire Mail Forward To Approver]");
								Message += " [OK - Send Expire Mail Forward To Approver]";
							}
							else
							{
								Console.Write(" [Error - Send Expire Mail Forward To Requester]");
								Message += " [Error - Send Expire Mail Forward To Requester]";
							}
							if (isSendMailForwardCompleteApprover)
							{
								Console.Write(" [OK - Send Expire Mail Forward To Approver]");
								Message += " [OK - Send Expire Mail Forward To Approver]";
							}
							else
							{
								Console.Write(" [Error - Send Expire Mail Forward To Approver]");
								Message += " [Error - Send Expire Mail Forward To Approver]";
							}
						}
						Console.Write("\n");
						Message += "<br/>";
					}
				}
				return Message;
			}
		}
		private static string SetOrderWarning()
		{
			using (SqlConnection connection = new SqlConnection(ConfigProject.ConnectionString))
			{
				string subjectMail, Message = "";
				var orderRepo = new OrderRepository(connection);
				bool isSendMailComplete = false;
				bool isSendMailCompleteApprover = false;
				string tempContentThai = clsMailService.GetMailContent(clsMailService.mailType.WarningToRequester, clsMailService.mailLanguage.THAI);
				string tempContentEng = clsMailService.GetMailContent(clsMailService.mailType.WarningToRequester, clsMailService.mailLanguage.ENGLISH);
				string tempContentApproverThai = clsMailService.GetMailContent(clsMailService.mailType.WarningToApprover, clsMailService.mailLanguage.THAI);
				string tempContentApproverEng = clsMailService.GetMailContent(clsMailService.mailType.WarningToApprover, clsMailService.mailLanguage.ENGLISH);

				List<Order> Orders = orderRepo.GetOrderWarning();

				if (Orders != null)
				{
					foreach (var order in Orders)
					{
						subjectMail = "";
						Console.Write(order.OrderId);
						Message += order.OrderId;
						if (order.Language == OrderLanguage.TH)
						{
							subjectMail += "OfficeMate e-Procurement ใบขอซื้อเพื่อการอนุมัติเลขที่ : " + order.OrderId + " กำลังจะหมดอายุภายใน 2 วันทำการ";
							isSendMailComplete = clsMailService.SendMailWarningToRequester(order, subjectMail, tempContentThai);
						}
						else
						{
							subjectMail += "OfficeMate e-Procurement This PO No. " + order.OrderId + " will expire within 2 days.";
							isSendMailComplete = clsMailService.SendMailWarningToRequester(order, subjectMail, tempContentEng);
						}
						if (order.CurrentApproverLanguage == OrderLanguage.TH)
						{
							subjectMail += "OfficeMate e-Procurement ใบขอซื้อเพื่อการอนุมัติเลขที่ : " + order.OrderId + " กำลังจะหมดอายุภายใน 2 วันทำการ";
							isSendMailCompleteApprover = clsMailService.SendMailWarningToApprover(order, subjectMail, tempContentApproverThai);
						}
						else
						{
							subjectMail += "OfficeMate e-Procurement This PO No. " + order.OrderId + " will expire within 2 days.";
							isSendMailCompleteApprover = clsMailService.SendMailWarningToApprover(order, subjectMail, tempContentApproverEng);
						}

						if (isSendMailComplete)
						{
							Console.Write(" [OK - Send Warning Mail To Requester]");
							Message += " [OK - Send Warning Mail To Requester]";
						}
						else
						{
							Console.Write(" [Error - Send Warning Mail To Requester]");
							Message += " [Error - Send Warning Mail To Requester]";
						}
						if (isSendMailCompleteApprover)
						{
							Console.Write(" [OK - Send Warning Mail To Approver]");
							Message += " [OK - Send Warning Mail To Approver]";
						}
						else
						{
							Console.Write(" [Error - Send Warning Mail To Approver]");
							Message += " [Error - Send Warning Mail To Approver]";
						}
						Console.Write("\n");
						Message += "<br/>";
					}
				}
				return Message;
			}
		}
		private static string SendOrderShipped()
		{
			using (SqlConnection connection = new SqlConnection(ConfigProject.ConnectionString))
			{
				string subjectMail, Message = "";
				var orderRepo = new OrderRepository(connection);
				bool isSendMailComplete = false;
				string tempContent = clsMailService.GetMailContent(clsMailService.mailType.ShippedOrder, clsMailService.mailLanguage.THAI);

				IEnumerable<OrderMail> Orders = orderRepo.GetOrderShipped();
				int count, page;
				if (Orders.Any())
				{
					page = 50;
					count = Convert.ToInt16(Math.Ceiling(Orders.Count() / Convert.ToDouble(page)));
					for (int i = 1; i <= count; i++)
					{
						int index = ((i - 1) * page) + 1;
						connection.Open();
						foreach (var order in Orders.Where(o => o.Row >= index).Take(page))
						{
							order.DetailOrder = orderRepo.GetOrderDetailShipped(order.OrderID);
							subjectMail = "";
							Console.Write(order.OrderID);
							Message += order.OrderID;

							subjectMail += "OfficeMate e-Procurement ใบขอซื้อ เลขที่ : " + order.OrderID + " ได้ดำเนินการขั้นตอนจัดส่งสินค้าให้คุณ";
							isSendMailComplete = clsMailService.SendMailShippedOrder(order, subjectMail, tempContent);

							if (isSendMailComplete)
							{
								orderRepo.ChangeStatusConfirmMailSuccess(order.OrderID);
								Console.Write("[OK - SendMail Shipped To Requester]");
								Message += "[OK - SendMail Shipped To Requester]";
							}
							else
							{
								Console.Write("[Error - Send Mail Shipped To Requester]");
								Message += "[Error - Send Mail Shipped To Requester]";
							}
							Console.Write("\n");
							Message += "<br/>";
						}
						connection.Close();
					}
				}
				return Message;
			}
		}
		private static string SendNotifyProduct()
		{
			using (SqlConnection connection = new SqlConnection(ConfigProject.ConnectionString))
			{
				string subjectMail, smsText = "", Message = "";
				var NotifyMeDataRepo = new ProductRepository(connection);
				bool isSendMailComplete = false;
				string tempContentThai = clsMailService.GetMailContent(clsMailService.mailType.NotifyProduct, clsMailService.mailLanguage.THAI);
				string tempContentEng = clsMailService.GetMailContent(clsMailService.mailType.NotifyProduct, clsMailService.mailLanguage.ENGLISH);

				List<ProductNotifyMe> product = NotifyMeDataRepo.GetProductNotifyMe();

				foreach (var item in product)
				{
					subjectMail = "";
					Console.Write("pId: " + item.ProductID);
					Message += "pId: " + item.ProductID;
					if (item.Language == ProductNotifyMe.NotifyLanguage.TH)
					{
						subjectMail = "OfficeMate e-Procurement แจ้งสินค้าพร้อมจำหน่าย ถึงคุณค่ะ";
						isSendMailComplete = clsMailService.SendMailNotifyProduct(item, subjectMail, tempContentThai);
						smsText = string.Format("{0} พร้อมสั่งซื้อที่ Eprocurement แล้วค่ะ", item.ProductThaiName);
					}
					else
					{
						subjectMail = "OfficeMate e-Procurement your product is comming for sell";
						isSendMailComplete = clsMailService.SendMailNotifyProduct(item, subjectMail, tempContentEng);
						smsText = string.Format("{0} ready product for sale at Eprocurement ", item.ProductEngName);
					}

					if (item.IsUseSMSFeature && !string.IsNullOrEmpty(item.MobileNo) && IsCorrectPattern(item.MobileNo))
					{
						Message += SendSms(item.MobileNo, smsText);
					}

					if (isSendMailComplete)
					{
						NotifyMeDataRepo.UpdateMailSuccess(item.NotifyID);
						Console.Write("[OK - SendMail Notify To Requester]");
						Message += "[OK - SendMail Notify To Requester]";
					}
					else
					{
						Console.Write("[Error - Send Mail Notify To Requester]");
						Message += "[Error - Send Mail Notify To Requester]";
					}
					Console.Write("\n");
					Message += "<br/>";
				}
				return Message;
			}
		}
		private static bool IsCorrectPattern(string mobile)
		{
			string pattern = @"^\d{2,3}-?\d{3}-?\d{4}$";
			return System.Text.RegularExpressions.Regex.IsMatch(mobile, pattern, System.Text.RegularExpressions.RegexOptions.IgnoreCase);
		}
		private static void SendExecuteTask(string Message)
		{
			string subjectMail = "Escalation Log on OfficeMate e-Procurement Version5";
			bool isSendMailComplete = false;
			isSendMailComplete = clsMailService.SendExecuteTaskMail(subjectMail, Message);
			try
			{
				if (isSendMailComplete)
				{
					Console.Write("[OK - Send Execute Task Mail]");
				}
				else
				{
					Console.Write("[Error - Send Execute Task Mail]");
				}
			}
			catch (Exception e)
			{
				Log4me.SaveLog(e);
				Console.Write("[Error - Send Execute Task Mail]");
			}
		}
		public static string ConnectDataBaseTest()
		{
			using (SqlConnection connection = new SqlConnection(ConfigProject.ConnectionString))
			{
				string SqlString = @"select '[OK] Connection DB Complete . . .  ' as Message ";
				using (SqlCommand command = new SqlCommand(SqlString, connection))
				{
					string message = "[Fail] Can not Connection DB..... ";
					try
					{
						command.CommandType = CommandType.Text;
						connection.Open();
						using (SqlDataReader reader = command.ExecuteReader())
						{
							if (reader.Read())
							{
								message = (string)reader["Message"];
							}
						}
						connection.Close();
					}
					catch (Exception e)
					{
						Log4me.SaveLog(e);
					}
					return message;
				}
			}
		}
		protected static string SendSms(string phoneNumber, string message)
		{
			PieSoftSmsService _smsService = new PieSoftSmsService(ConfigProject.SmsUserName, ConfigProject.SmsPassword);
			try
			{
				if (_smsService.GetRemainingCredit() > 0)
				{
					_smsService.SendMessage(message, ConfigProject.SmsSenderName, phoneNumber);
					Console.Write("[OK - Send Sms To " + phoneNumber + "]");
					return "[OK - Send SMS To " + phoneNumber + "]";
				}
				else
				{
					Console.Write("[Error - Send Sms to:" + phoneNumber + " : out of credit");
					return "[Error - Send Sms to:" + phoneNumber + " : out of credit";
				}
			}
			catch (Exception e)
			{
				Log4me.SaveLog(e);
				Console.Write("[Error - Send Sms to:" + phoneNumber + "]");
				return "[Error - Send Sms to:" + phoneNumber + "]";
			}
		}
	}
}
