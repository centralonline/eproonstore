﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using EproV5AutoExpire.Models;
using System.Data;
using System.Net;

namespace EproV5AutoExpire
{
	public class OrderRepository
	{
		public SqlConnection connection { get; set; }
		public OrderRepository(SqlConnection _connection) { connection = _connection; }

		public List<Order> GetOrderExpire()
		{
			string SqlString = @"Select	distinct OrderID,OrderGUID,CustID,t1.CompanyID,DepartmentID,t1.CostcenterID,OrderStatus,t1.ParkDay,WarningExpireDate,ExpireDate,
										t1.UserID,t1.UserTName,t1.UserEName,t2.DefaultLang,
										CurrentAppID,t3.UserTName as CurrentAppTName,t3.UserEName as CurrentAppEName,t4.APPLevel as CurrentAppLevel,t3.UserLang as CurrentAppLang,
										NextAppID,isnull(t5.UserTName,'') as NextAppTName,isnull(t5.UserEName,'') as NextAppEName,isnull(t6.APPLevel,0) as NextAppLevel,isnull(t5.UserLang,'TH') as NextAppLang
								from	TBOrder t1 
										INNER JOIN TBCompanyInfo t2 ON t1.CompanyID = t2.CompanyID 
										INNER JOIN TBUserInfo t3 ON t1.CurrentAppID = t3.UserID  
										INNER JOIN TBRequesterLine t4 ON t1.CurrentAppID = t4.APPUserID AND t1.UserID = t4.ReqUserID AND t1.CompanyID = t4.CompanyID AND t1.CostcenterID = t4.CostcenterID
										LEFT JOIN TBUserInfo t5 ON t1.NextAppID = t5.UserID  
										LEFT JOIN TBRequesterLine t6 ON t1.NextAppID = t6.APPUserID AND t1.UserID = t6.ReqUserID AND t1.CompanyID = t6.CompanyID AND t1.CostcenterID = t6.CostcenterID
								Where	DATEDIFF(day,ExpireDate,GETDATE()) = 0 AND OrderStatus IN ('Waiting','Partial','WaitingAdmin','AdminAllow')
								";

			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				List<Order> items = null;
				try
				{
					cmd.CommandType = CommandType.Text;
					connection.Open();
					using (SqlDataReader reader = cmd.ExecuteReader())
					{
						Order order = null;
						items = new List<Order>();
						while (reader.Read())
						{
							order = new Order();
							order.OrderId = (string)reader["OrderID"];
							order.OrderGuid = (string)reader["OrderGUID"];
							order.UserId = (string)reader["UserId"];
							order.UserThaiName = (string)reader["UserTName"];
							order.UserEngName = (string)reader["UserEName"];
							order.Language = (OrderLanguage)Enum.Parse(typeof(OrderLanguage), (string)reader["DefaultLang"]);

							/*get current app*/
							order.CurrentAppId = (string)reader["CurrentAppID"];
							order.CurrentApproverThaiName = (string)reader["CurrentAppTName"];
							order.CurrentApproverEngName = (string)reader["CurrentAppEName"];
							order.CurrentApproverLevel = (int)reader["CurrentAppLevel"];
							order.CurrentApproverLanguage = (OrderLanguage)Enum.Parse(typeof(OrderLanguage), (string)reader["CurrentAppLang"]);

							/*get next app*/
							order.NextAppId = (string)reader["NextAppID"];
							order.NextApproverThaiName = (string)reader["NextAppTName"];
							order.NextApproverEngName = (string)reader["NextAppEName"];
							order.NextApproverLevel = (int)reader["NextAppLevel"];
							order.NextApproverLanguage = (OrderLanguage)Enum.Parse(typeof(OrderLanguage), (string)reader["NextAppLang"]);

							order.Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), (string)reader["OrderStatus"]);
							order.Guid = (string)reader["OrderGUID"];
							order.CustId = (string)reader["CustID"];
							order.CompanyId = (string)reader["CompanyID"];
							order.DepartmentId = (string)reader["DepartmentId"];
							order.CostcenterId = (string)reader["CostcenterId"];
							order.ParkDay = (int)reader["ParkDay"];
							order.WarningDate = (DateTime)reader["WarningExpireDate"];
							order.ExpireDate = (DateTime)reader["ExpireDate"];

							items.Add(order);
						}
					}
				}
				catch (Exception e)
				{
					Log4me.SaveLog(e);
				}
				return items;
			}
		}

		public List<Order> GetOrderWarning()
		{
			string SqlString = @"Select	distinct OrderID,OrderGUID,CustID,t1.CompanyID,DepartmentID,t1.CostcenterID,OrderStatus,t1.ParkDay,WarningExpireDate,ExpireDate,
										t1.UserID,t1.UserTName,t1.UserEName,t2.DefaultLang,
										CurrentAppID,t3.UserTName as CurrentAppTName,t3.UserEName as CurrentAppEName,t4.APPLevel as CurrentAppLevel,t3.UserLang as CurrentAppLang,
										NextAppID,isnull(t5.UserTName,'') as NextAppTName,isnull(t5.UserEName,'') as NextAppEName,isnull(t6.APPLevel,0) as NextAppLevel,isnull(t5.UserLang,'TH') as NextAppLang
								from	TBOrder t1 
										INNER JOIN TBCompanyInfo t2 ON t1.CompanyID = t2.CompanyID 
										INNER JOIN TBUserInfo t3 ON t1.CurrentAppID = t3.UserID  
										INNER JOIN TBRequesterLine t4 ON t1.CurrentAppID = t4.APPUserID AND t1.UserID = t4.ReqUserID AND t1.CompanyID = t4.CompanyID AND t1.CostcenterID = t4.CostcenterID
										LEFT JOIN TBUserInfo t5 ON t1.NextAppID = t5.UserID  
										LEFT JOIN TBRequesterLine t6 ON t1.NextAppID = t6.APPUserID AND t1.UserID = t6.ReqUserID AND t1.CompanyID = t6.CompanyID AND t1.CostcenterID = t6.CostcenterID
								where	DATEDIFF(day,WarningExpireDate,GETDATE()) = 0 AND OrderStatus IN ('Waiting','Partial','WaitingAdmin','AdminAllow')
								";

			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				List<Order> items = null;
				try
				{
					cmd.CommandType = CommandType.Text;
					connection.Open();
					using (SqlDataReader reader = cmd.ExecuteReader())
					{
						Order order = null;
						items = new List<Order>();
						while (reader.Read())
						{
							order = new Order();
							order.OrderId = (string)reader["OrderID"];
							order.OrderGuid = (string)reader["OrderGUID"];
							order.UserId = (string)reader["UserId"];
							order.UserThaiName = (string)reader["UserTName"];
							order.UserEngName = (string)reader["UserEName"];
							order.Language = (OrderLanguage)Enum.Parse(typeof(OrderLanguage), (string)reader["DefaultLang"]);

							/*get current app*/
							order.CurrentAppId = (string)reader["CurrentAppID"];
							order.CurrentApproverThaiName = (string)reader["CurrentAppTName"];
							order.CurrentApproverEngName = (string)reader["CurrentAppEName"];
							order.CurrentApproverLevel = (int)reader["CurrentAppLevel"];
							order.CurrentApproverLanguage = (OrderLanguage)Enum.Parse(typeof(OrderLanguage), (string)reader["CurrentAppLang"]);

							/*get next app*/
							order.NextAppId = (string)reader["NextAppID"];
							order.NextApproverThaiName = (string)reader["NextAppTName"];
							order.NextApproverEngName = (string)reader["NextAppEName"];
							order.NextApproverLevel = (int)reader["NextAppLevel"];
							order.NextApproverLanguage = (OrderLanguage)Enum.Parse(typeof(OrderLanguage), (string)reader["NextAppLang"]);

							order.Status = (OrderStatus)Enum.Parse(typeof(OrderStatus), (string)reader["OrderStatus"]);
							order.Guid = (string)reader["OrderGUID"];
							order.CustId = (string)reader["CustID"];
							order.CompanyId = (string)reader["CompanyID"];
							order.DepartmentId = (string)reader["DepartmentId"];
							order.CostcenterId = (string)reader["CostcenterId"];
							order.ParkDay = (int)reader["ParkDay"];
							order.WarningDate = (DateTime)reader["WarningExpireDate"];
							order.ExpireDate = (DateTime)reader["ExpireDate"];

							items.Add(order);
						}
					}
				}
				catch (Exception e)
				{
					Log4me.SaveLog(e);
				}
				return items;
			}
		}

		public List<OrderMail> GetOrderShipped()
		{
			string SqlString = @"SELECT	row_number() OVER(ORDER BY OrderID) AS Row,OrderID,OrderGUID,OrderDate,TBO.UserID,TBO.UserTName,TBO.UserEName,
										ContactorName,ContactorEmail,ContactorPhone,ContactMobileNo,ContactorExtension,ContactorFax,
										TBO.CompanyID,TBC.CompTName,CompEName,isnull(TBO.DepartmentID,'')as DepartmentID,isnull(TBD.DeptTName,'')as DeptTName,isnull(TBD.DeptEName,'')as DeptEName,
										TBO.CostcenterID,TBDs.CostTName,TBDs.CostEName,InvAddr1,InvAddr2,InvAddr3,InvAddr4,ShipContactor,ShipAddr1,ShipAddr2,ShipAddr3,ShipAddr4,ShipMobileNo,ShipPhoneNo,
										TBO.CustId,ApproverRemark,OFMRemark,ReferenceRemark,TBU.UserLang,CountProduct,NetAmt,VatAmt,DeliveryFee,VatProdNetAmt,NonVatProdNetAmt,GrandTotalAmt
								FROM	TBOrder TBO INNER JOIN TBCompanyInfo TBC  ON TBO.CompanyID = TBC.CompanyID
										LEFT JOIN TBDepartment TBD ON TBO.DepartmentID = TBD.DepartmentID AND  TBO.CompanyID = TBD.CompanyID
										INNER JOIN TBCostcenter TBDs ON TBO.CostcenterID = TBDs.CostcenterID AND  TBO.CompanyID = TBDs.CompanyID
										INNER JOIN TBUserInfo TBU on TBU.UserID = TBO.UserID
								WHERE	datediff(day,CompleteMailDate,getdate()) = 0 AND CompleteMailSuccess = 'No' AND OrderStatus = 'Shipped'
								";
			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				List<OrderMail> items = null;
				try
				{
					cmd.CommandType = CommandType.Text;
					connection.Open();
					using (SqlDataReader reader = cmd.ExecuteReader())
					{
						OrderMail order = null;
						items = new List<OrderMail>();
						while (reader.Read())
						{
							order = new OrderMail();
							order.Row = Convert.ToInt16(reader["Row"]);
							order.OrderID = (string)reader["OrderID"];
							order.OrderGuid = (string)reader["OrderGUID"];
							order.OrderDate = (DateTime)reader["OrderDate"];
							order.UserID = (string)reader["UserID"];
							order.CurrentRequesterThaiName = (string)reader["UserTName"];
							order.CurrentRequesterEngName = (string)reader["UserEName"];
							order.ContactorName = (string)reader["ContactorName"];
							order.ContactEmail = (string)reader["ContactorEmail"];
							order.ContactorPhone = (string)reader["ContactorPhone"];
							order.ContactorExtension = (string)reader["ContactorExtension"];
							order.ContactorFax = (string)reader["ContactorFax"];
							order.ContactMobileNo = (string)reader["ContactMobileNo"];
							order.CompanyId = (string)reader["CompanyID"];
							order.CompanyThaiName = (string)reader["CompTName"];
							order.CompanyEnameName = (string)reader["CompEName"];
							order.DepartmentID = (string)reader["DepartmentID"];
							order.DepartmentThaiName = (string)reader["DeptTName"];
							order.DepartmentEngName = (string)reader["DeptEName"];
							order.CostCenterID = (string)reader["CostcenterID"];
							order.CostCenterThaiName = (string)reader["CostTName"];
							order.CostCenterEngName = (string)reader["CostEName"];
							order.InvoiceAddress1 = (string)reader["InvAddr1"];
							order.InvoiceAddress2 = (string)reader["InvAddr2"];
							order.InvoiceAddress3 = (string)reader["InvAddr3"];
							order.InvoiceAddress4 = (string)reader["InvAddr4"];
							order.ShipContactor = (string)reader["ShipContactor"];
							order.ShippingAddress1 = (string)reader["ShipAddr1"];
							order.ShippingAddress2 = (string)reader["ShipAddr2"];
							order.ShippingAddress3 = (string)reader["ShipAddr3"];
							order.ShippingAddress4 = (string)reader["ShipAddr4"];
							order.ShippingMobileNo = (string)reader["ShipMobileNo"];
							order.ShippingPhoneNo = (string)reader["ShipPhoneNo"];
							order.CustId = (string)reader["CustId"];
							order.ApproverRemark = (string)reader["ApproverRemark"];
							order.OFMRemark = (string)reader["OFMRemark"];
							order.ReferenceRemark = (string)reader["ReferenceRemark"];
							order.UserLang = (string)reader["UserLang"];
							order.ItemCountOrder = (int)reader["CountProduct"];
							order.TotalNetAmt = (decimal)reader["NetAmt"];
							order.TotalVatAmt = (decimal)reader["VatAmt"];
							order.DeliveryFee = (decimal)reader["DeliveryFee"];
							order.VatProdNetAmt = (decimal)reader["VatProdNetAmt"];
							order.NonVatProdNetAmt = (decimal)reader["NonVatProdNetAmt"];
							order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];

							items.Add(order);
						}
					}
					connection.Close();
				}
				catch (Exception e)
				{
					Log4me.SaveLog(e);
				}
				return items;
			}
		}

		public List<OrderMailDetail> GetOrderDetailShipped(string orderId)
		{
			List<OrderMailDetail> items = null;
			string SqlString = @"SELECT pID, pTName, pEName, pTUnit, pEUnit, BestDealFlag
									 , Qty, DiscountRate, ExcVatPrice, DiscAmt, IsVat
								FROM
									TBOrderDetail
								WHERE
									OrderID = @OrderID";

			using (SqlCommand cmd_del = new SqlCommand(SqlString, connection))
			{
				cmd_del.Parameters.AddWithValue("@OrderID", orderId);
				try
				{
					using (SqlDataReader reader = cmd_del.ExecuteReader())
					{
						OrderMailDetail order = null;
						items = new List<OrderMailDetail>();
						while (reader.Read())
						{
							order = new OrderMailDetail();
							order.PId = (string)reader["pID"];
							order.ProductThaiName = (string)reader["pTName"];
							order.ProductEngName = (string)reader["pEName"];
							order.ProductThaiUnit = (string)reader["pTUnit"];
							order.ProductEngUnit = (string)reader["pEUnit"];
							order.Quantity = (int)reader["Qty"];
							order.DiscountRate = (decimal)reader["DiscountRate"];
							order.PriceExcVat = (decimal)reader["ExcVatPrice"];
							order.DiscAmt = (decimal)reader["DiscAmt"];
							order.IsVat = reader["IsVat"].ToString().Equals("Yes");
							order.IsBestDeal = reader["BestDealFlag"].ToString().Equals("Yes");

							items.Add(order);
						}
					}
				}
				catch (Exception e)
				{
					Log4me.SaveLog(e);
				}
			}
			return items;
		}

		public void SetStatusExpire(string orderId)
		{
			string SqlString = @"UPDATE	TBOrder
								SET		OrderBeforeStatus=OrderStatus, OrderStatus=@Status,
										PreviousAppID=CurrentAppID, CurrentAppID='', NextAppID='',
										UpdateOn=getdate(),UpdateBy=@UpdateBy
								WHERE	OrderID=@OrderID";
			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				cmd.Parameters.AddWithValue("@OrderID", orderId);
				cmd.Parameters.AddWithValue("@Status", OrderStatus.Expired.ToString());
				cmd.Parameters.AddWithValue("@UpdateBy", "Task EproV5");
				cmd.ExecuteNonQuery();
			}
		}

		public void AutoForwordApprove(Order order)
		{
			string SqlString = @"UPDATE	TBOrder
								SET		OrderBeforeStatus=OrderStatus, OrderStatus=@Status,
										PreviousAppID=CurrentAppID, CurrentAppID=NextAppID, NextAppID=@NextAppID,
										WarningExpireDate=@WarningExpireDate, ExpireDate=@ExpireDate,
										UpdateOn=getdate(),UpdateBy=@UpdateBy
								WHERE	OrderID = @OrderID";
			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				cmd.Parameters.AddWithValue("@OrderID", order.OrderId);
				cmd.Parameters.AddWithValue("@Status", OrderStatus.Partial.ToString());
				cmd.Parameters.AddWithValue("@NextAppID", GetNextApproverID(order.NextAppId, order));
				cmd.Parameters.AddWithValue("@WarningExpireDate", CalculateWarningExpireDate(order.ExpireDate, order.ParkDay));
				cmd.Parameters.AddWithValue("@ExpireDate", CalculateExprieDate(order.ExpireDate, order.ParkDay));
				cmd.Parameters.AddWithValue("@UpdateBy", "Task EproV5");
				cmd.ExecuteNonQuery();
			}
		}

		public void ChangeStatusConfirmMailSuccess(string orderid)
		{
			string SqlString = @"UPDATE TBOrder SET CompleteMailSuccess = 'Yes', UpdateBy=@updateby, UpdateOn=getdate() WHERE Orderid = @orderid";
			using (SqlCommand command = new SqlCommand(SqlString, connection))
			{
				command.Parameters.AddWithValue("@orderid", orderid);
				command.Parameters.AddWithValue("@updateby", "Task EproV5");
				command.ExecuteNonQuery();
			}
		}

		public void InsertOrderActivity(string orderId, bool IsExpire)
		{
			string SqlString = @"insert into TBOrderActivity (OrderID,ActivityTName,ActivityEName,Remark,IPAddress,CreateOn,CreateBy)
								values (@OrderID,@ActivityTName,@ActivityEName,@Remark,@IPAddress,getdate(),@CreateBy)";
			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				cmd.Parameters.AddWithValue("@OrderID", orderId);
				if (IsExpire)
				{
					cmd.Parameters.AddWithValue("@ActivityTName", "ใบสั่งซื้อ หมดอายุ");
					cmd.Parameters.AddWithValue("@ActivityEName", "Order Expire");
					cmd.Parameters.AddWithValue("@Remark", "ใบสั่งซื้อหมดอายุโดยอัตโนมัติ");
				}
				else
				{
					cmd.Parameters.AddWithValue("@ActivityTName", "ใบสั่งซื้อผ่านการอนุมัติแล้วเบื้องต้น");
					cmd.Parameters.AddWithValue("@ActivityEName", "Order Partial Approved");
					cmd.Parameters.AddWithValue("@Remark", "ระบบได้ส่งต่อใบสั่งซื้อโดยอัตโนมัติ");
				}
				cmd.Parameters.AddWithValue("@IPAddress", GetIP());
				cmd.Parameters.AddWithValue("@CreateBy", "Task EproV5");
				cmd.ExecuteNonQuery();
			}
		}

		public IEnumerable<UserApprover> GetListUserApprover(string companyId, string costcenterId, string userId)
		{
			string SqlString = @"SELECT	AppUserID,UserTName,UserEName,AppLevel,AppCreditLimit,ParkDay
								FROM	TBRequesterLine t1 inner join TBUserInfo t2 on t1.AppUserID = t2.UserID
										inner join TBCompanyInfo t3 on t1.CompanyID = t3.CompanyID
										inner join TBUserCompany t4 on t1.CompanyID = t4.CompanyID AND t1.APPUserID = t4.UserID
								Where	ReqUserID=@UserID and t1.CompanyID = @CompanyID and t1.CostCenterId = @CostCenterId and UserStatus = @Userstatus
										ORDER BY APPLevel";
			List<UserApprover> item = new List<UserApprover>();
			using (SqlCommand cmd = new SqlCommand(SqlString, connection))
			{
				cmd.Parameters.AddWithValue("@UserID", userId);
				cmd.Parameters.AddWithValue("@CompanyID", companyId);
				cmd.Parameters.AddWithValue("@CostCenterId", costcenterId);
				cmd.Parameters.AddWithValue("@Userstatus", "Active");
				using (SqlDataReader reader = cmd.ExecuteReader())
				{

					UserApprover user = null;
					while (reader.Read())
					{
						user = new UserApprover();
						user.UserId = (string)reader["AppUserID"];
						user.UserThaiName = (string)reader["UserTName"];
						user.UserEngName = (string)reader["UserEName"];
						user.Level = (int)reader["AppLevel"];
						user.ParkDay = (int)reader["ParkDay"];
						user.BudgetAmount = (decimal)reader["AppCreditLimit"];
						item.Add(user);
					}
				}
			}
			return item;
		}

		private string GetNextApproverID(string currentApproverId, Order order)
		{
			IEnumerable<UserApprover> userApprover = GetListUserApprover(order.CompanyId, order.CostcenterId, order.UserId);
			int currentLevel = userApprover.Where(o => o.UserId == currentApproverId).Select(o => o.Level).Single();

			if (userApprover.Count() == 1)  //ถ้ามีผู้อนุมัติคนเดียวแสดงว่าไม่มีคนลำดับถัดไป
			{
				return string.Empty;
			}
			if (currentLevel == userApprover.Count()) //แสดงว่าเป็นผู้อนุมัติคนสุดท้าย
			{
				return string.Empty;
			}
			else
			{
				currentLevel += 1; //ขยับไปคนถัดไป
				return userApprover.Where(o => o.Level == currentLevel).Select(o => o.UserId).Single();
			}
		}

		private DateTime CalculateExprieDate(DateTime OrderDate, int parkday)
		{
			IEnumerable<Holiday> ListHoliday = GetHoliday();
			DateTime Expiredate = OrderDate.Date;

			ListHoliday = ListHoliday.Where(o => o.HolidayDate >= Expiredate).ToList(); //ดึงเฉพาะวันที่มากกว่าเท่ากับ  Expiredate จะได้ไม่ต้องคิดวันหยุดที่ผ่านมาแล้ว

			while (parkday != 0)
			{
				Expiredate = Expiredate.AddDays(1);
				if (Expiredate.DayOfWeek == DayOfWeek.Saturday || Expiredate.DayOfWeek == DayOfWeek.Sunday) { continue; } //ถ้าเป็นวันเสาร์หรือวันอาทิตย์จะข้ามไป
				else
				{
					foreach (var item in ListHoliday)
					{
						if (DateTime.Compare(Expiredate, item.HolidayDate) == 0) { continue; }
					}
				}
				parkday--;
			}
			return Expiredate;
		}

		private DateTime CalculateWarningExpireDate(DateTime OrderDate, int parkday)
		{
			return CalculateExprieDate(OrderDate, parkday - 2);
		}

		private IEnumerable<Holiday> GetHoliday()
		{
			List<Holiday> item = new List<Holiday>();
			string SqlString = @"select Holdate,Holdesc from  DBMaster..TBHoliday";
			using (SqlCommand command = new SqlCommand(SqlString, connection))
			{
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Holiday holiday = new Holiday();
						holiday.HolidayDate = Convert.ToDateTime(reader["Holdate"]);
						holiday.HolidayName = (string)reader["Holdesc"];
						item.Add(holiday);
					}
				}
			}
			return item;
		}

		private string GetIP()
		{
			string strHostName = "";
			strHostName = System.Net.Dns.GetHostName();
			IPHostEntry ipEntry = System.Net.Dns.GetHostEntry(strHostName);
			IPAddress[] addr = ipEntry.AddressList;
			return addr[addr.Length - 1].ToString();
		}
	}
}
