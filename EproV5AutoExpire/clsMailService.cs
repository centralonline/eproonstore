﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using EproV5AutoExpire.Models;
using System.Net.Mail;
using System.Net;
using System.IO;

namespace EproV5AutoExpire
{
	public abstract class clsMailService
	{
		public static bool SendMailWarningToRequester(Order order, string subjectMail, string contentmail)
		{
			clsMailInfo tempMail = new clsMailInfo();

			string tempContent = contentmail;
			bool isSendMailComplete = false;

			List<clsMailParameter> parameterlist = new List<clsMailParameter>();
			parameterlist.Add(new clsMailParameter("[<^UserTName>]", order.UserThaiName));
			parameterlist.Add(new clsMailParameter("[<^UserEName>]", order.UserEngName));
			parameterlist.Add(new clsMailParameter("[<^OrderID>]", order.OrderId));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverEngName>]", order.CurrentApproverEngName));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverThaiName>]", order.CurrentApproverThaiName));


			foreach (var param in parameterlist)
			{
				tempContent = tempContent.Replace(param.TagName, param.Replace);
			}
			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");

				tempMail.MailTo = new List<MailAddress>();
				tempMail.MailTo.Add(new MailAddress(order.UserId));

				tempMail.MailBcc = ConfigProject.EmailAddressBCCToAdmin();

				tempMail.MailContent = tempContent;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}

			return isSendMailComplete;
		}
		public static bool SendMailWarningToApprover(Order order, string subjectMail, string contentmail)
		{
			clsMailInfo tempMail = new clsMailInfo();

			string tempContent = contentmail;
			bool isSendMailComplete = false;

			List<clsMailParameter> parameterlist = new List<clsMailParameter>();
			parameterlist.Add(new clsMailParameter("[<^UserTName>]", order.UserThaiName));
			parameterlist.Add(new clsMailParameter("[<^UserEName>]", order.UserEngName));
			parameterlist.Add(new clsMailParameter("[<^OrderID>]", order.OrderId));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverEngName>]", order.CurrentApproverEngName));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverThaiName>]", order.CurrentApproverThaiName));

			string Url = ConfigProject.IsProduction ? "http://eprocurement.officemate.co.th/Account/ViewOrderForApprovalFromMail?id={0}" : "http://eprocurement.officemate.dev/Account/ViewOrderForApprovalFromMail?id={0}";
			parameterlist.Add(new clsMailParameter("[<^LinkTH>]", string.Format("<a href='" + Url + "'>คลิกที่นี่เพื่อพิจารณาใบสั่งซื้อ</a>", order.OrderGuid)));
			parameterlist.Add(new clsMailParameter("[<^LinkEN>]", string.Format("<a href='" + Url + "'>Click here to approve page</a>", order.OrderGuid)));

			foreach (var param in parameterlist)
			{
				tempContent = tempContent.Replace(param.TagName, param.Replace);
			}
			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");

				tempMail.MailTo = new List<MailAddress>();
				tempMail.MailTo.Add(new MailAddress(order.CurrentAppId));

				tempMail.MailBcc = ConfigProject.EmailAddressBCCToAdmin();

				tempMail.MailContent = tempContent;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}

			return isSendMailComplete;
		}
		public static bool SendMailExpireToRequester(Order order, string subjectMail, string contentmail)
		{
			clsMailInfo tempMail = new clsMailInfo();

			string tempContent = contentmail;
			bool isSendMailComplete = false;

			List<clsMailParameter> parameterlist = new List<clsMailParameter>();
			parameterlist.Add(new clsMailParameter("[<^UserTName>]", order.UserThaiName));
			parameterlist.Add(new clsMailParameter("[<^UserEName>]", order.UserEngName));
			parameterlist.Add(new clsMailParameter("[<^OrderID>]", order.OrderId));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverEngName>]", order.CurrentApproverEngName));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverThaiName>]", order.CurrentApproverThaiName));


			foreach (var param in parameterlist)
			{
				tempContent = tempContent.Replace(param.TagName, param.Replace);
			}
			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");

				tempMail.MailTo = new List<MailAddress>();
				tempMail.MailTo.Add(new MailAddress(order.UserId));

				tempMail.MailBcc = ConfigProject.EmailAddressBCCToAdmin();

				tempMail.MailContent = tempContent;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}

			return isSendMailComplete;
		}
		public static bool SendMailExpireToApprover(Order order, string subjectMail, string contentmail)
		{
			clsMailInfo tempMail = new clsMailInfo();

			string tempContent = contentmail;
			bool isSendMailComplete = false;

			List<clsMailParameter> parameterlist = new List<clsMailParameter>();
			parameterlist.Add(new clsMailParameter("[<^UserTName>]", order.UserThaiName));
			parameterlist.Add(new clsMailParameter("[<^UserEName>]", order.UserEngName));
			parameterlist.Add(new clsMailParameter("[<^OrderID>]", order.OrderId));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverEngName>]", order.CurrentApproverEngName));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverThaiName>]", order.CurrentApproverThaiName));


			foreach (var param in parameterlist)
			{
				tempContent = tempContent.Replace(param.TagName, param.Replace);
			}
			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");

				tempMail.MailTo = new List<MailAddress>();
				tempMail.MailTo.Add(new MailAddress(order.CurrentAppId));

				tempMail.MailBcc = ConfigProject.EmailAddressBCCToAdmin();

				tempMail.MailContent = tempContent;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}

			return isSendMailComplete;
		}
		public static bool SendMailForwardOrderToRequester(Order order, string subjectMail, string contentmail)
		{

			clsMailInfo tempMail = new clsMailInfo();

			string tempContent = contentmail;
			bool isSendMailComplete = false;

			List<clsMailParameter> parameterlist = new List<clsMailParameter>();
			parameterlist.Add(new clsMailParameter("[<^UserTName>]", order.UserThaiName));
			parameterlist.Add(new clsMailParameter("[<^UserEName>]", order.UserEngName));
			parameterlist.Add(new clsMailParameter("[<^OrderID>]", order.OrderId));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverThaiName>]", order.CurrentApproverThaiName));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverEngName>]", order.CurrentApproverEngName));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverLevel>]", order.CurrentApproverLevel.ToString()));
			parameterlist.Add(new clsMailParameter("[<^NextApproverThaiName>]", order.NextApproverThaiName));
			parameterlist.Add(new clsMailParameter("[<^NextApproverEngName>]", order.NextApproverEngName));
			parameterlist.Add(new clsMailParameter("[<^NextApproverLevel>]", order.NextApproverLevel.ToString()));

			foreach (var param in parameterlist)
			{
				tempContent = tempContent.Replace(param.TagName, param.Replace);
			}
			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");

				tempMail.MailTo = new List<MailAddress>();
				tempMail.MailTo.Add(new MailAddress(order.UserId));

				tempMail.MailBcc = ConfigProject.EmailAddressBCCToAdmin();

				tempMail.MailContent = tempContent;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}

			return isSendMailComplete;
		}
		public static bool SendMailForwardOrderToApprover(Order order, string subjectMail, string contentmail)
		{

			clsMailInfo tempMail = new clsMailInfo();

			string tempContent = contentmail;
			bool isSendMailComplete = false;

			List<clsMailParameter> parameterlist = new List<clsMailParameter>();
			parameterlist.Add(new clsMailParameter("[<^UserTName>]", order.UserThaiName));
			parameterlist.Add(new clsMailParameter("[<^UserEName>]", order.UserEngName));
			parameterlist.Add(new clsMailParameter("[<^OrderID>]", order.OrderId));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverThaiName>]", order.CurrentApproverThaiName));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverEngName>]", order.CurrentApproverEngName));
			parameterlist.Add(new clsMailParameter("[<^CurrentApproverLevel>]", order.CurrentApproverLevel.ToString()));
			parameterlist.Add(new clsMailParameter("[<^NextApproverThaiName>]", order.NextApproverThaiName));
			parameterlist.Add(new clsMailParameter("[<^NextApproverEngName>]", order.NextApproverEngName));
			parameterlist.Add(new clsMailParameter("[<^NextApproverLevel>]", order.NextApproverLevel.ToString()));
			parameterlist.Add(new clsMailParameter("[<^ParkDay>]", order.ParkDay.ToString()));

			string Url = ConfigProject.IsProduction ? "http://eprocurement.officemate.co.th/Account/ViewOrderForApprovalFromMail?id={0}" : "http://eprocurement.officemate.dev/Account/ViewOrderForApprovalFromMail?id={0}";
			parameterlist.Add(new clsMailParameter("[<^LinkTH>]", string.Format("<a href='" + Url + "'>คลิกที่นี่เพื่อพิจารณาใบสั่งซื้อ</a>", order.OrderGuid)));
			parameterlist.Add(new clsMailParameter("[<^LinkEN>]", string.Format("<a href='" + Url + "'>Click here to approve page</a>", order.OrderGuid)));

			foreach (var param in parameterlist)
			{
				tempContent = tempContent.Replace(param.TagName, param.Replace);
			}
			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");

				tempMail.MailTo = new List<MailAddress>();
				tempMail.MailTo.Add(new MailAddress(order.CurrentAppId));

				tempMail.MailBcc = ConfigProject.EmailAddressBCCToAdmin();

				tempMail.MailContent = tempContent;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}

			return isSendMailComplete;
		}
		public static bool SendMailShippedOrder(OrderMail order, string subjectMail, string contentmail)
		{
			clsMailInfo tempMail = new clsMailInfo();

			string tempContent = contentmail;
			bool isSendMailComplete = false;

			List<clsMailParameter> parameterlist = new List<clsMailParameter>();

			if (!String.IsNullOrEmpty(order.ContactorExtension))
			{
				order.ContactorPhone += " - " + order.ContactorExtension;
			}

			if (String.IsNullOrEmpty(order.ApproverRemark))
			{
				order.ApproverRemark = "-";
			}

			if (String.IsNullOrEmpty(order.OFMRemark))
			{
				order.OFMRemark = "-";
			}

			if (String.IsNullOrEmpty(order.ReferenceRemark))
			{
				order.ReferenceRemark = "-";
			}

			parameterlist.Add(new clsMailParameter("[<^Model.OrderID>]", order.OrderID));
			parameterlist.Add(new clsMailParameter("[<^Model.Company.CompanyId>]", order.CompanyId));
			parameterlist.Add(new clsMailParameter("[<^Model.Company.CompanyName>]", order.CompanyThaiName));
			parameterlist.Add(new clsMailParameter("[<^Model.Department.DepartmentID>]", order.DepartmentID));
			parameterlist.Add(new clsMailParameter("[<^Model.CostCenter.CostCenterID>]", order.CostCenterID));
			parameterlist.Add(new clsMailParameter("[<^Model.CostCenter.CostCenterCustID>]", order.CustId));

			parameterlist.Add(new clsMailParameter("[<^CurrentRequesterThaiName>]", order.CurrentRequesterThaiName));
			parameterlist.Add(new clsMailParameter("[<^Model.Contact.ContactorName>]", order.ContactorName));
			parameterlist.Add(new clsMailParameter("[<^Model.Contact.Email>]", order.ContactEmail));
			parameterlist.Add(new clsMailParameter("[<^Model.Contact.ContactorPhone>]", order.ContactorPhone));
			parameterlist.Add(new clsMailParameter("[<^Model.Contact.ContactMobileNo>]", order.ContactMobileNo));
			parameterlist.Add(new clsMailParameter("[<^Model.Contact.ContactorFax>]", order.ContactorFax));

			parameterlist.Add(new clsMailParameter("[<^Model.ApproverRemark>]", order.ApproverRemark));
			parameterlist.Add(new clsMailParameter("[<^Model.OFMRemark>]", order.OFMRemark));
			parameterlist.Add(new clsMailParameter("[<^Model.ReferenceRemark>]", order.ReferenceRemark));

			parameterlist.Add(new clsMailParameter("[<^Model.OrderDate>]", order.OrderDate.ToString("dd/MM/yyyy")));
			parameterlist.Add(new clsMailParameter("[<^Model.Invoice.Address1>]", order.InvoiceAddress1));
			parameterlist.Add(new clsMailParameter("[<^Model.Invoice.Address2>]", order.InvoiceAddress2));
			parameterlist.Add(new clsMailParameter("[<^Model.Invoice.Address3>]", order.InvoiceAddress3));
			parameterlist.Add(new clsMailParameter("[<^Model.Invoice.Address4>]", order.InvoiceAddress4));

			parameterlist.Add(new clsMailParameter("[<^Model.Shipping.Address1>]", order.ShippingAddress1));
			parameterlist.Add(new clsMailParameter("[<^Model.Shipping.Address2>]", order.ShippingAddress2));
			parameterlist.Add(new clsMailParameter("[<^Model.Shipping.Address3>]", order.ShippingAddress3));
			parameterlist.Add(new clsMailParameter("[<^Model.Shipping.Address4>]", order.ShippingAddress4));
			parameterlist.Add(new clsMailParameter("[<^Model.Shipping.ShipContactor>]", order.ShipContactor));
			parameterlist.Add(new clsMailParameter("[<^Model.Shipping.ShipMobileNo>]", order.ShippingMobileNo));
			parameterlist.Add(new clsMailParameter("[<^Model.Shipping.ShipPhoneNo>]", order.ShippingPhoneNo));

			parameterlist.Add(new clsMailParameter("[<^Model.ItemCountOrder>]", order.ItemCountOrder.ToString()));
			parameterlist.Add(new clsMailParameter("[<^Model.TotalNetAmt>]", order.TotalNetAmt.ToString("#,##0.00")));
			parameterlist.Add(new clsMailParameter("[<^Model.TotalDeliveryFee>]", order.DeliveryFee.ToString("#,##0.00")));
			parameterlist.Add(new clsMailParameter("[<^Model.TotalPriceProductNoneVatAmount>]", order.NonVatProdNetAmt.ToString("#,##0.00")));
			parameterlist.Add(new clsMailParameter("[<^Model.TotalPriceProductExcVatAmount>]", order.VatProdNetAmt.ToString("#,##0.00")));
			parameterlist.Add(new clsMailParameter("[<^Model.TotalVatAmt>]", order.TotalVatAmt.ToString("#,##0.00")));
			parameterlist.Add(new clsMailParameter("[<^Model.GrandTotalAmt>]", order.GrandTotalAmt.ToString("#,##0.00")));

			StringBuilder content = new StringBuilder();
			foreach (var obj in order.DetailOrder)
			{
				content.AppendLine("<tr>");
				content.AppendLine("<td style='margin-top: 10px;'>");
				content.AppendLine("<div align='center'>");
				content.AppendLine(obj.PId);
				content.AppendLine("</div>");
				content.AppendLine("</td>");
				content.AppendLine("<td style='margin-top: 10px;'>");
				content.AppendLine("<div class='content-shopping-cart-table'>");
				content.AppendLine("<p class='productnameCart'>");
				if (order.UserLang == "TH") { content.AppendLine("" + obj.ProductThaiName + ""); }
				else { content.AppendLine("" + obj.ProductEngName + ""); }
				content.AppendLine("</p>");
				content.AppendLine("</div>");
				content.AppendLine("</td>");
				content.AppendLine("<td valign='top'>");
				content.AppendLine("<div align='center'>");
				content.AppendLine("" + obj.ItemIncVatPrice.ToString("#,##0.00") + "");
				content.AppendLine("</div>");
				content.AppendLine("</td>");
				content.AppendLine("<td valign='top'>");
				content.AppendLine("<div align='center'>");
				content.AppendLine("" + obj.ItemIncVatPrice.ToString("#,##0.00") + "");
				content.AppendLine("</div>");
				content.AppendLine("</td>");
				content.AppendLine("	<td valign='top'>");
				content.AppendLine("<div align='center'>");
				content.AppendLine("" + obj.Quantity + "<br />");
				content.AppendLine("</div>");
				content.AppendLine("</td>");
				content.AppendLine("<td valign='top'>");
				content.AppendLine("<div align='center'>");
				if (order.UserLang == "TH") { content.AppendLine("" + obj.ProductThaiUnit + ""); }
				else { content.AppendLine("" + obj.ProductEngUnit + ""); }
				content.AppendLine("</div>");
				content.AppendLine("</td>");
				content.AppendLine("<td colspan='2' valign='top'>");
				content.AppendLine("<div align='center'>");
				content.AppendLine("" + obj.ItemPriceNet.ToString("#,##0.00") + "");
				content.AppendLine("</div>");
				content.AppendLine("</td>");
				content.AppendLine("</tr>");
			}
			parameterlist.Add(new clsMailParameter("[<^Model.ContentOrderDetail>]", content.ToString()));

			string urllink = "";
			if (ConfigProject.IsProduction)
			{
				urllink = string.Format("<a href='http://eprocurement.officemate.co.th/Account/CompleteOrder?guid={0}'>ยืนยันการรับสินค้า</a>", order.OrderGuid);
			}
			else
			{
				urllink = string.Format("<a href='http://eprocurement.officemate.dev/Account/CompleteOrder?guid={0}'>ยืนยันการรับสินค้า</a>", order.OrderGuid);
			}
			parameterlist.Add(new clsMailParameter("[<^Model.ContentUrl>]", urllink));

			foreach (var param in parameterlist)
			{
				tempContent = tempContent.Replace(param.TagName, param.Replace);
			}

			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");

				tempMail.MailTo = new List<MailAddress>();
				tempMail.MailTo.Add(new MailAddress(order.ContactEmail));

				tempMail.MailBcc = ConfigProject.EmailAddressBCCToAdmin();

				tempMail.MailContent = tempContent;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}
			return isSendMailComplete;
		}
		public static bool SendMailNotifyProduct(ProductNotifyMe notify, string subjectMail, string contentmail)
		{
			clsMailInfo tempMail = new clsMailInfo();

			string tempContent = contentmail;
			bool isSendMailComplete = false;

			List<clsMailParameter> parameterlist = new List<clsMailParameter>();

			parameterlist.Add(new clsMailParameter("[<^ProductId>]", notify.ProductID));
			parameterlist.Add(new clsMailParameter("[<^ProductThaiName>]", notify.ProductThaiName));
			parameterlist.Add(new clsMailParameter("[<^ProductEngName>]", notify.ProductEngName));
			parameterlist.Add(new clsMailParameter("[<^ProductImage>]", notify.ProductImage));
			parameterlist.Add(new clsMailParameter("[<^URL>]", notify.SitePath));
			parameterlist.Add(new clsMailParameter("[<^UserTName>]", notify.UserTName));
			parameterlist.Add(new clsMailParameter("[<^UserEName>]", notify.UserEName));

			foreach (var param in parameterlist)
			{
				tempContent = tempContent.Replace(param.TagName, param.Replace);
			}
			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");

				tempMail.MailTo = new List<MailAddress>();
				tempMail.MailTo.Add(new MailAddress(notify.NotifyMeEmail));

				tempMail.MailContent = tempContent;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}

			return isSendMailComplete;
		}
		public static bool SendExecuteTaskMail(string subjectMail, string contentmail)
		{
			clsMailInfo tempMail = new clsMailInfo();
			bool isSendMailComplete = true;
			try
			{
				tempMail.MailSubject = subjectMail;
				tempMail.MailFrom = new MailAddress(ConfigProject.EmailFromAddressName(), "e-Procurement System");
				tempMail.MailTo = ConfigProject.EmailAddressSummeryToAdmin();
				tempMail.MailContent = contentmail;
				tempMail.IsHtml = true;
				isSendMailComplete = Sendmail(tempMail);
			}
			catch (Exception e)
			{
				isSendMailComplete = false;
				Log4me.SaveLog(e);
			}
			return isSendMailComplete;
		}

		public static string GetMailContent(mailType type, mailLanguage language)
		{
			string contentPath = RootPath(type, language);
			StreamReader fp = File.OpenText(contentPath);
			string tempContent = fp.ReadToEnd();
			fp.Close();
			return tempContent;
		}
		private static string RootPath(mailType type, mailLanguage language)
		{
			switch (language)
			{
				case mailLanguage.ENGLISH:
					return Path.GetFullPath(GetMailPathEnglish(type));
				case mailLanguage.THAI:
					return Path.GetFullPath(GetMailPathThai(type));
				default:
					return Path.GetFullPath(GetMailPathThai(type));
			}
		}
		private static string GetMailPathThai(mailType type)
		{
			switch (type)
			{
				case mailType.ExpireToRequester:
					return "MailTemplates/mailExpireThaiToRequester.txt";
				case mailType.ExpireToApprover:
					return "MailTemplates/mailExpireThaiToApprover.txt";
				case mailType.ForwardOrderToRequester:
					return "MailTemplates/mailForwardOrderThaiToRequester.txt";
				case mailType.ForwardOrderToApprover:
					return "MailTemplates/mailForwardOrderThaiToApprover.txt";
				case mailType.WarningToRequester:
					return "MailTemplates/mailWarningThaiToRequester.txt";
				case mailType.WarningToApprover:
					return "MailTemplates/mailWarningThaiToApprover.txt";
				case mailType.ShippedOrder:
					return "MailTemplates/mailShippedThaiToRequester.txt";
				case mailType.NotifyProduct:
					return "MailTemplates/mailNotifyProductThai.txt";
				case mailType.StartExecute:
					return "MailTemplates/ExecuteStartTaskMail.txt";
				case mailType.EndExeCute:
					return "MailTemplates/ExecuteEndTaskMail.txt";
				default:
					return "";
			}
		}
		private static string GetMailPathEnglish(mailType type)
		{
			switch (type)
			{
				case mailType.ExpireToRequester:
					return "MailTemplates/mailExpireEngToRequester.txt";
				case mailType.ExpireToApprover:
					return "MailTemplates/mailExpireEngToApprover.txt";
				case mailType.ForwardOrderToRequester:
					return "MailTemplates/mailForwardOrderEngToRequester.txt";
				case mailType.ForwardOrderToApprover:
					return "MailTemplates/mailForwardOrderEngToApprover.txt";
				case mailType.WarningToRequester:
					return "MailTemplates/mailWarningEngToRequester.txt";
				case mailType.WarningToApprover:
					return "MailTemplates/mailWarningEngToApprover.txt";
				case mailType.ShippedOrder:
					return "MailTemplates/mailShippedThaiToRequester.txt";
				case mailType.NotifyProduct:
					return "MailTemplates/mailNotifyProductEng.txt";
				case mailType.StartExecute:
					return "MailTemplates/ExecuteStartTaskMail.txt";
				case mailType.EndExeCute:
					return "MailTemplates/ExecuteEndTaskMail.txt";
				default:
					return "";
			}
		}
		private static bool Sendmail(clsMailInfo mailInfo)
		{

			bool resultSuccessSendmail = true;
			try
			{
				SmtpClient tempSmtpClient = new SmtpClient();
				MailMessage tempMailMessage = new MailMessage();

				tempSmtpClient.Host = ConfigProject.SmtpServer;
				tempSmtpClient.Port = 25;

				tempMailMessage.From = mailInfo.MailFrom;
				tempMailMessage.Subject = mailInfo.MailSubject;
				tempMailMessage.BodyEncoding = System.Text.Encoding.GetEncoding(mailInfo.MailEncode);
				tempSmtpClient.Credentials = new NetworkCredential(ConfigProject.SMTPUsername, ConfigProject.SMTPPassword);
				tempMailMessage.IsBodyHtml = mailInfo.IsHtml;
				tempMailMessage.Body = mailInfo.MailContent;

				if (mailInfo.MailTo != null)
				{
					foreach (var mail in mailInfo.MailTo)
					{
						tempMailMessage.To.Add(mail);
					}
				}

				if (mailInfo.MailCC != null)
				{
					foreach (var mail in mailInfo.MailCC)
					{
						tempMailMessage.CC.Add(mail);
					}
				}

				if (mailInfo.MailBcc != null)
				{
					foreach (var mail in mailInfo.MailBcc)
					{
						tempMailMessage.Bcc.Add(mail);
					}
				}

				tempSmtpClient.Send(tempMailMessage);

			}
			catch (Exception e)
			{
				resultSuccessSendmail = false;
				Log4me.SaveLog(e);
			}
			return resultSuccessSendmail;
		}
		public enum mailType
		{
			ExpireToRequester,
			ExpireToApprover,
			ForwardOrderToRequester,
			ForwardOrderToApprover,
			WarningToRequester,
			WarningToApprover,
			StartExecute,
			EndExeCute,
			ShippedOrder,
			NotifyProduct
		}
		public enum mailLanguage
		{
			THAI,
			ENGLISH
		}
	}
	class clsMailInfo
	{
		public MailAddress MailFrom { get; set; }
		public List<MailAddress> MailTo { get; set; }
		public List<MailAddress> MailCC { get; set; }
		public List<MailAddress> MailBcc { get; set; }
		public string MailEncode { get { return "utf-8"; } }
		public string MailSubject { get; set; }
		public string MailContent { get; set; }
		public bool IsHtml { get; set; }
	}
	class clsMailParameter
	{
		public clsMailParameter(string TagName, string Replace)
		{
			this.TagName = TagName;
			this.Replace = Replace;
		}
		public string TagName { get; set; }
		public string Replace { get; set; }
	}
}
