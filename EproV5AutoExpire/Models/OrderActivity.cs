﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EproV5AutoExpire.Models
{
	public class OrderActivity
	{
		public int LogID { get; set; }
		public string OrderId { get; set; }
		public string ActivityTName { get; set; }
		public string ActivityEName { get; set; }
		public string IPAddress { get; set; }
		public string Remark { get; set; }
		public string CreateBy { get; set; }
		public DateTime CreateOn { get; set; }
		public string UpdateBy { get; set; }
		public DateTime UpdateOn { get; set; }
	}
}
