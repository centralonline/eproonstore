﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EproV5AutoExpire.Models
{
	public class UserApprover
	{
		public string UserId { get; set; }
		public string UserThaiName { get; set; }
		public string UserEngName { get; set; }
		public int ParkDay { get; set; }
		public int Level { get; set; }
		public string ApproverRemark { get; set; }
		public decimal BudgetAmount { get; set; }
	}
}
