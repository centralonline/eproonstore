﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EproV5AutoExpire.Models
{
	public class OrderMail
	{
		public int Row { get; set; }
		public string CurrentRequesterThaiName { get; set; }
		public string CurrentRequesterEngName { get; set; }
		public string UserID { get; set; }
		public string CustId { get; set; }
		public string OrderID { get; set; }
		public string OrderGuid { get; set; }
		public string ContactorName { get; set; }
		public string ContactEmail { get; set; }
		public string ContactorPhone { get; set; }
		public string ContactorExtension { get; set; }
		public string ContactMobileNo { get; set; }
		public string ContactorFax { get; set; }
		public string CompanyId { get; set; }
		public string CompanyName { get; set; }
		public string CompanyThaiName { get; set; }
		public string CompanyEnameName { get; set; }
		public string DepartmentID { get; set; }
		public string DepartmentName { get; set; }
		public string DepartmentThaiName { get; set; }
		public string DepartmentEngName { get; set; }
		public string CostCenterID { get; set; }
		public string CostCenterName { get; set; }
		public string CostCenterThaiName { get; set; }
		public string CostCenterEngName { get; set; }
		public DateTime OrderDate { get; set; }

		public string InvoiceAddress1 { get; set; }
		public string InvoiceAddress2 { get; set; }
		public string InvoiceAddress3 { get; set; }
		public string InvoiceAddress4 { get; set; }

		public string ShipContactor { get; set; }
		public string ShippingAddress1 { get; set; }
		public string ShippingAddress2 { get; set; }
		public string ShippingAddress3 { get; set; }
		public string ShippingAddress4 { get; set; }
		public string ShippingMobileNo { get; set; }
		public string ShippingPhoneNo { get; set; }

		public string ApproverRemark { get; set; }
		public string OFMRemark { get; set; }
		public string ReferenceRemark { get; set; }

		public string UserLang { get; set; }
		public int ItemCountOrder { get; set; }
		public decimal TotalNetAmt { get; set; }
		public decimal TotalVatAmt { get; set; }
		public decimal DeliveryFee { get; set; }
		public decimal VatProdNetAmt { get; set; }
		public decimal NonVatProdNetAmt { get; set; }
		public decimal GrandTotalAmt { get; set; }

		public List<OrderMailDetail> DetailOrder { get; set; }

	}

}
