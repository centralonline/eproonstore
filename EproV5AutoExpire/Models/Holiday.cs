﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EproV5AutoExpire.Models
{
	public class Holiday
	{
		public DateTime HolidayDate { get; set; }
		public string HolidayName { get; set; }
	}
}
