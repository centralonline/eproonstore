﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EproV5AutoExpire.Models
{
	public class OrderMailDetail
	{
		public string PId { get; set; }
		public string ProductThaiName { get; set; }
		public string ProductEngName { get; set; }
		public string ProductThaiUnit { get; set; }
		public string ProductEngUnit { get; set; }
		public bool IsVat { get; set; }
		public bool IsBestDeal { get; set; }
		public bool IsPromotion { get; set; }
		public decimal PriceExcVat { get; set; }
		public decimal DiscountRate { get; set; }
		public decimal DiscAmt { get; set; }
		public int Quantity { get; set; }
		//แสดงราคาสินค้าที่ลบส่วนลดแล้ว แต่ยังไม่ได้คูณ Quantity
		public decimal ItemPriceNet
		{
			get
			{
				return (PriceExcVat * Quantity) - DiscAmt;
			}
		}
		public decimal PriceDiscount
		{
			get
			{
				if (IsBestDeal || IsPromotion)
				{
					return 0;
				}
				else
				{
					return (Math.Round((PriceExcVat * DiscountRate) / 100, 2, MidpointRounding.AwayFromZero));
				}
			}
		}
		public decimal ItemIncVatPrice
		{
			get
			{
				if (IsVat)
				{
					return (Math.Round((ItemExcVatPrice * Convert.ToDecimal(1.07)), 2, MidpointRounding.AwayFromZero));
				}
				else
				{
					return PriceExcVat - PriceDiscount;
				}
			}
		}

		public decimal ItemExcVatPrice
		{
			get
			{
				return PriceExcVat - PriceDiscount;
			}
		}
	}
}
