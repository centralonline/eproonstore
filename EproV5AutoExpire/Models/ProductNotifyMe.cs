﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace EproV5AutoExpire.Models
{
	public class ProductNotifyMe
	{
		private const string _imageSmallPath = "/images/tpimage/{0}.jpg";
		public int NotifyID { get; set; }
		public string ProductID { get; set; }
		public string CompanyId { get; set; }
		public string ProductThaiName { get; set; }
		public string ProductEngName { get; set; }
		public string UserTName { get; set; }
		public string UserEName { get; set; }
		public NotifyLanguage Language { get; set; }
		public bool IsUseSMSFeature { get; set; }
		public string SitePath
		{
			get
			{
				if (ConfigProject.IsProduction)
				{
					return "http://eprocurement.officemate.co.th/";
				}
				else
				{
					return "http://eprocurement.officemate.dev/";
				}
			}
		}
		public string ProductImage
		{
			get
			{
				return SitePath + string.Format(_imageSmallPath, ProductID);
			}
		}
		public int Qty { get; set; }
		public string NotifyMeEmail { get; set; }
		public string MobileNo { get; set; }
		public string NotifyRemark { get; set; }
		public enum NotifyLanguage
		{
			TH,
			EN
		}
	}
}
