﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace EproV5AutoExpire.Models
{
	public class Order
	{
		public string OrderId { get; set; }
		public string OrderGuid { get; set; }
		public OrderStatus Status { get; set; }
		public OrderLanguage Language { get; set; }
		public string Guid { get; set; }
		public string UserId { get; set; }
		public string UserThaiName { get; set; }
		public string UserEngName { get; set; }
		public string CurrentAppId { get; set; }
		public string CurrentApproverThaiName { get; set; }
		public string CurrentApproverEngName { get; set; }
		public int CurrentApproverLevel { get; set; }
		public OrderLanguage CurrentApproverLanguage { get; set; }
		public string NextAppId { get; set; }
		public string NextApproverThaiName { get; set; }
		public string NextApproverEngName { get; set; }
		public int NextApproverLevel { get; set; }
		public OrderLanguage NextApproverLanguage { get; set; }
		public string CustId { get; set; }
		public string CompanyId { get; set; }
		public string DepartmentId { get; set; }
		public string CostcenterId { get; set; }
		public DateTime ExpireDate { get; set; }
		public DateTime WarningDate { get; set; }

		public int ParkDay { get; set; }
	}

	public enum OrderStatus
	{
		Waiting,
		Approved,
		Partial,
		Revise,
		WaitingAdmin,
		AdminAllow,
		Shipped,
		Deleted,
		Expired,
		Completed
	}

	public enum OrderLanguage
	{
		TH,
		EN
	}

}
