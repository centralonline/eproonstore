﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace EproV5AutoExpire
{
	public abstract class Log4me
	{
		public static void SaveLog(Exception exception)
		{
			// Create a writer and open the file:
			StreamWriter log;
			string startupPath = System.IO.Directory.GetCurrentDirectory();
			string path = startupPath + "\\logfile.txt";

			if (!File.Exists(path))
			{
				log = new StreamWriter("logfile.txt");
			}
			else
			{
				log = File.AppendText("logfile.txt");
			}

			// Write to the file:
			log.WriteLine(DateTime.Now);
			log.WriteLine(exception.Message);
			log.WriteLine(exception.StackTrace);
			log.WriteLine(exception.TargetSite);
			log.WriteLine();
			// Close the stream:
			log.Close();
		}

		public static void SaveLog(string logtxt)
		{
			// Create a writer and open the file:
			StreamWriter log;
			string startupPath = System.IO.Directory.GetCurrentDirectory();
			string path = startupPath + "\\logfile.txt";

			if (!File.Exists(path))
			{
				log = new StreamWriter("logfile.txt");
			}
			else
			{
				log = File.AppendText("logfile.txt");
			}

			// Write to the file:
			log.WriteLine(DateTime.Now);
			log.WriteLine(logtxt);
			log.WriteLine();
			// Close the stream:
			log.Close();
		}
	}
}
