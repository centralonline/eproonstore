﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ForgotPasswordData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.Dear")%> <%=Model.UserName %>
	<br />
	<br />
	<%=Html._("Mail.ForgotPasswordMail.Alternate")%>
	<br />
	<%=Html._("Mail.User")%> [<%=Model.UserId %>] <%=Model.UserName %>
	<br />
	<br />
	<a href="<%=Url.FullUrlAction("SetPassword", "Account", new { Model.UserGuid, Model.VerifyKey})%>" style="font-size: 16px"><b><%=Html._("Mail.ForgotPassword")%></b></a>
	<br />
	<br />
	mail reference: 18(Forgot Password)
</asp:Content>
