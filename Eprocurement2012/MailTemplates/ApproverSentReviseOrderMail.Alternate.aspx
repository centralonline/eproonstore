﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.Dear")%>
	<%=Model.Order.Contact.ContactorName%>
	<br />
	<br />
	<%=string.Format(Html._("Mail.ApproverSentReviseOrderMail.Content"), Model.Order.PreviousApprover.Approver.DisplayName, Model.Order.OrderID)%>
	<br />
	<br />
	<a href="<%=Url.FullUrlAction("LogIn", "Account")%>"><%=Html._("Mail.ClickEditOrder")%></a>
	<br />
	<br />
	<%=Html._("Mail.ApproverRemark")%>
	<%=string.IsNullOrEmpty(Model.Order.ApproverRemark)? "-": Model.Order.ApproverRemark%>
	<br />
	<br />
	mail reference: 04(Revise Order)
</asp:Content>
