﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ForgotPasswordData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

<%--	<p>OfficeMate ขอขอบคุณที่ท่านไว้วางใจใช้งานระบบ OfficeMate e-Procurement ขอต้อนรับคุณเข้าสู่ระบบจัดซื้อออนไลน์สำหรับองค์กรของคุณ</p>
	<br />
	<br />
	เรียนคุณ <%=Model.UserName %>
	<br />
	<br />
	ขณะนี้ผู้ดูและระบบของคุณได้ทำการลงทะเบียนการเป็นสมาชิกเว็บไซต์เรียบร้อยแล้ว เพียงคุณคลิกยืนยันการเป็นสมาชิกก็จะเริ่มใช้งานได้ทันทีค่ะ
	<br />
	<br />
	ชื่อลงทะเบียนสมาชิกของคุณคือ <%=Model.UserId %>
	<br />
	<br />
	กรุณาคลิกลิงค์ด้านล่างเพื่อยืนยัน และเปิดใช้บัญชิส่วนตัวของคุณ
	<br />
	<a href="<%=Url.FullUrlAction("VerifyUser", "Account", new { Model.UserGuid, Model.VerifyKey})%>" style="font-size: 16px"><b>กรุณาคลิกที่นี่ เพื่อกำหนดรหัสผ่านใหม่</b></a>
	<br />
	<br />
	ความเหนือกว่าของระบบ OfficeMate e-Procurement 
	<br />	• ออกแบบขี้น เพื่อให้เหมาะสมกับความต้องการของแต่ละองค์กรโดยเฉพาะ
	<br />• เป็นระบบที่ช่วยลดเวลา และขั้นตอนในการดำเนินการตั้งแต่ขั้นตอนการสั่งซื้อไปจนถึงขั้นตอนการอนุมัติ
	<br />• ช่วยลดค่าใช้จ่ายอื่นๆ ในกระบวนการจัดซื้อ เช่น ต้นทุนการผลิตแฝงที่เกิดจากการจัดซื้อนอกระบบ
	<br />• สามารถติดตาม ตรวจสอบ ควบคุมและอนุมัติการสั่งซื้อด้วยระบบ Paperless ได้ทุกที่ ทุกเวลาจากระบบออนไลน์
	<br />• สามารถติดตามและตรวจสอบขั้นตอนการสั่งซื้อได้อย่างละเอียดด้วยระบบ Report ที่สมบูรณ์แบบที่สุด
	<br />
	<p>OfficeMate e-Procurement จึงพัฒนาขึ้นมาเพื่อตอบสนองความต้องการอีกขั้นหนึ่งของการจัดซื้อได้อย่างมีประสิทธิภาพ และเหมาะสมที่สุดสำหรับทุกองค์กร</p>
	<br />
	<br />--%>

	<p><%=Html._("Mail.VerifyUser.WelcomeContent")%></p>
	<br />
	<br />
	<%=Html._("Mail.Dear")%> <%=Model.UserName %>
	<br />
	<br />
	<%=Html._("Mail.VerifyUser.CompleteRegister")%>
	<br />
	<br />
	<%=Html._("Mail.VerifyUser.RegisterName")%> <%=Model.UserId %>
	<br />
	<br />
	<%=Html._("Mail.VerifyUser.Confirm")%>
	<br />
	<a href="<%=Url.FullUrlAction("VerifyUser", "Account", new { Model.UserGuid, Model.VerifyKey})%>" style="font-size: 16px"><b><%=Html._("Mail.ForgotPassword")%></b></a>
	<br />
	<br />
	<%=Html._("Mail.VerifyUser.Content")%>
	<p><%=Html._("Mail.VerifyUser.Requirements")%></p>
	<br />
	<br />
	mail reference: 18(Verify User)
</asp:Content>
