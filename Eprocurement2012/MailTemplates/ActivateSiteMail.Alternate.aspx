﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.DearUser")%>
	<br />
	<br />
	<%=Html._("Mail.ActivateSiteMail.Alternate")%>
	<br />
	<br />
	mail reference: 16(Activate Site)
</asp:Content>
