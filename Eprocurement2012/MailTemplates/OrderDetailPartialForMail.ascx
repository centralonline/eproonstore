﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Order>" %>
<div class="centerwrap">
	<div id="content-shopping-cart-detail">
		<div>
			<%=Html._("Mail.OrderID")%><%=Model.OrderID%>
			<table>
				<tr>
					<td>
						<%=Html._("Mail.Requester")%>
					</td>
					<td>
						<%=Model.Contact.ContactorName%>
					</td>
					<td>
						<%=Html._("Mail.CompanyID")%>
					</td>
					<td>
						<%=Model.Company.CompanyId%>
					</td>
				</tr>
				<tr>
					<td>
						<%=Html._("Mail.UserID")%>
					</td>
					<td>
						<%=Model.Contact.Email%>
					</td>
					<td>
						<%=Html._("Mail.CompanyName")%>
					</td>
					<td>
						<%=Model.Company.CompanyName%>
					</td>
				</tr>
				<tr>
					<td>
						<%=Html._("Mail.PhoneNo")%>
					</td>
					<td>
						<%=Model.Contact.ContactorPhone%>
						<%=string.IsNullOrEmpty(Model.Contact.ContactorExtension) ? "" : " #" + Model.Contact.ContactorExtension%>
					</td>
					<td>
						<%=Html._("Mail.Department")%>
					</td>
					<td>
						<%=string.IsNullOrEmpty(Model.Department.DepartmentID)? "-": "[" + Model.Department.DepartmentID + "] " + Model.Department.DepartmentName %>
					</td>
				</tr>
				<tr>
					<td>
						<%=Html._("Mail.MobileNo")%>
					</td>
					<td>
						<%=Model.Contact.ContactMobileNo%>
					</td>
					<td>
						<%=Html._("Mail.CostCenter")%>
					</td>
					<td>
						[<%=Model.CostCenter.CostCenterID%>]
						<%=Model.CostCenter.CostCenterName%>
					</td>
				</tr>
				<tr>
					<td>
						<%=Html._("Mail.FaxNo")%>
					</td>
					<td>
						<%=Model.Contact.ContactorFax%>
					</td>
					<td>
						<%=Html._("Mail.OrderDate")%>
					</td>
					<td>
						<%=Model.OrderDate.ToString("dd/MM/yyyy HH:mm")%>
					</td>
				</tr>
				<tr>
					<td>
						
					</td>
					<td>
						
					</td>
					<td>
						<%=Html._("Mail.CustID")%>
					</td>
					<td>
						<%=Model.CostCenter.CostCenterCustID%>
					</td>
				</tr>
			</table>
		</div>
		<hr />
		<div>
			<table>
				<tr>
					<td>
						<%=Html._("Mail.InvoiceAddress")%>
					</td>
					<td>
						<%=Model.Invoice.Address1%>
					</td>
					<td>
						<%=Html._("Mail.ShippingAddress")%>
					</td>
					<td>
						<%=Model.Shipping.Address1%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<%=Model.Invoice.Address2%>
					</td>
					<td>
					</td>
					<td>
						<%=Model.Shipping.Address2%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<%=Model.Invoice.Address3%>
					</td>
					<td>
					</td>
					<td>
						<%=Model.Shipping.Address3%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<%=Model.Invoice.Address4%>
					</td>
					<td>
					</td>
					<td>
						<%=Model.Shipping.Address4%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<%=Html._("Mail.Contactor")%>
					</td>
					<td>
						<%=Model.Shipping.ShipContactor%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<%=Html._("Mail.MobileNo")%>
					</td>
					<td>
						<%=Model.Shipping.ShipMobileNo%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<%=Html._("Mail.PhoneNo")%>
					</td>
					<td>
						<%=Model.Shipping.ShipPhoneNo%>
					</td>
				</tr>
			</table>
		</div>
		<hr />
		<%=Html._("Mail.OrderItems")%>
		<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
			border: solid 1px #c5c5c5">
			<tr>
				<td width="80" class="thead" align="center">
					<%=Html._("CartDetailPartial.ProductId")%>
				</td>
				<td class="thead" align="center">
					<%=Html._("CartDetailPartial.ProductName")%>
				</td>
				<td width="80" class="thead" align="center">
					<%=Html._("CartDetailPartial.FullPrice")%>
					<br />
					<%=Html._("CartDetailPartial.IncVat")%>
				</td>
				<td width="80" class="thead" align="center">
					<%=Html._("CartDetailPartial.Price")%>
					<br />
					<%=Html._("CartDetailPartial.ExcVat")%>
				</td>
				<td width="40" class="thead" align="center">
					<%=Html._("Shared.OrderDetailPartial.Quantity")%>
				</td>
				<td width="40" class="thead" align="center">
					<%--<%=Html._("CartDetailPartial.Discount")%>--%>
					<%=Html._("CartDetailPartial.Unit")%>
				</td>
				<td width="80" class="thead" align="center">
					<%=Html._("CartDetailPartial.NetAmt")%>
					<br />
					<%=Html._("CartDetailPartial.ExcVat")%>
				</td>
			</tr>
			<%
				foreach (Eprocurement2012.Models.OrderDetail Items in Model.ItemOrders)
				{ 
			%>
			<tr>
				<td style="margin-top: 10px;">
					<div align="center">
						<strong style="color: #000066;">
							<%=Html.Encode(Items.Product.Id)%></strong>
					</div>
				</td>
				<td style="margin-top: 10px;">
					<div class="content-shopping-cart-table">
						<p class="productnameCart">
							<%=Html.Encode(Items.Product.Name)%>
						</p>
					</div>
				</td>
				<td valign="top">
					<div align="center">
						<%--<%=Html.Encode(Items.Product.PriceIncVat.ToString(new MoneyFormat()))%>--%>
						<%=Html.Encode(Items.ItemIncVatPrice.ToString(new MoneyFormat()))%>
					</div>
				</td>
				<td valign="top">
					<div align="center">
						<%--<%=Html.Encode(Items.Product.PriceExcVat.ToString(new MoneyFormat()))%>--%>
						<%=Html.Encode(Items.ItemExcVatPrice.ToString(new MoneyFormat()))%>
					</div>
				</td>
				<td valign="top">
					<div align="center">
						<%=Html.Encode(Items.Quantity)%><br />
						<%--<%=Html.Encode(Items.Product.Unit)%>--%>
					</div>
				</td>
				<td valign="top">
					<div align="center">
						<%--<%=Items.DiscAmt.ToString(new MoneyFormat())%>--%>
						<%=Html.Encode(Items.Product.Unit)%>
					</div>
				</td>
				<td colspan="2" valign="top">
					<div align="center">
						<%=Html.Encode(Items.ItemPriceNet.ToString(new MoneyFormat()))%>
					</div>
				</td>
			</tr>
			<%
				}
			%>
			<tr style="line-height: 25px">
				<td valign="top">
					<div align="center">
					</div>
				</td>
				<td valign="top">
					<div align="right">
						<%=Html._("CartDetailPartial.NetAmt")%>&nbsp;</div>
				</td>
				<td valign="top">
					<div align="center">
					</div>
				</td>
				<td valign="top">
					<div align="center">
					</div>
				</td>
				<td valign="top">
					<div align="center">
						<b>
							<%=Html.Encode(Model.ItemCountOrder.ToString())%></b>
						<%--<%=Html._("CartDetailPartial.Item")%>--%>
					</div>
				</td>
				<td valign="top">
					<%--<div align="center">
						<b>
							<%=Html.Encode(Model.TotalAllDiscount.ToString(new MoneyFormat()))%></b></div>--%>
					<div align="center">
						<b>
							<%=Html._("CartDetailPartial.Item")%></b></div>
				</td>
				<td valign="top">
					<div align="center">
						<b>
							<%=Html.Encode(Model.TotalNetAmt.ToString(new MoneyFormat()))%></b></div>
				</td>
			</tr>
		</table>
		<div style="width: 105%">
			<div id="total" class=" margin-top-10 gray-m">
				<%if (Model.TotalDeliveryFee > 0)
				{ %>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalDeliveryFee")%></label>
					<span>
						<%=Html.Encode(Model.TotalDeliveryFee.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<%} %>
				<%
					if (Model.TotalDeliveryCharge > 0)
					{%>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalDeliveryCharge")%></label>
					<span>
						<%=Html.Encode(Model.TotalDeliveryCharge.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<%	} %>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalPriceProductNoneVatWithDisCountAmount")%></label>
					<span>
						<%=Html.Encode(Model.TotalPriceProductNoneVatAmount.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalPriceProductExcVatWithDisCountAmount")%></label>
					<span>
						<%=Html.Encode(Model.TotalPriceProductExcVatAmount.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalVatAmt")%></label>
					<span>
						<%=Html.Encode(Model.TotalVatAmt)%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<div style="border-bottom: 1px solid #999999; width: 370px; float: right; margin-top: 5px;">
				</div>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.GrandTotalAmt")%></label>
					<span class="price" style="color: #CC0000; font-size: 13px; font-weight: bolder;">
						<%=Html.Encode(Model.GrandTotalAmt.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
			</div>
			<br class="clear" />
		</div>
		<br class="clear" />
	</div>
</div>
<br class="clear" />
