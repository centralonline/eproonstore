﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.GoodReceive>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.Dear")%>
	<%=Model.UserApprover.Approver.DisplayName%>
	<br />
	<br />
	<%if (Model.Status == Eprocurement2012.Models.GoodReceive.GoodReceiveStatus.Completed)
   {%>
	   	<%=string.Format(Html._("Mail.GoodReceiveOrderMail.ReceiveAllContent"), Model.Requester.DisplayName, Model.Order.OrderID)%>
   <%} %>
   <%else
   {%>
		<%=string.Format(Html._("Mail.GoodReceiveOrderMail.Content"), Model.Requester.DisplayName, Model.Order.OrderID)%>
	   	
   <%} %>
	<br />
	<br />
	<div class="centerwrap">
		<div>
			<%=Html._("Order.PartialReceive.OrderId")%><%=Model.Order.OrderID%>
			<table>
				<tr>
					<td>
						<%=Html._("Mail.Requester")%>
					</td>
					<td>
						<%=Model.Order.Contact.ContactorName%>
					</td>
					<td>
						<%=Html._("Mail.GoodReceive.CompanyId")%>
					</td>
					<td>
						<%=Model.Order.Company.CompanyId%>
					</td>
				</tr>
				<tr>
					<td>
						<%=Html._("Mail.UserID")%>
					</td>
					<td>
						<%=Model.Order.Contact.Email%>
					</td>
					<td>
						<%=Html._("Mail.GoodReceive.CompanyName")%>
					</td>
					<td>
						<%=Model.Order.Company.CompanyName%>
					</td>
				</tr>
				<tr>
					<td>
						<%=Html._("Mail.GoodReceive.Phone")%>
					</td>
					<td>
						<%=Model.Order.Contact.ContactorPhone%>
						<%=string.IsNullOrEmpty(Model.Order.Contact.ContactorExtension) ? "" : " #" + Model.Order.Contact.ContactorExtension%>
					</td>
					<td>
						<%=Html._("Mail.Department")%>
					</td>
					<td>
						<%=string.IsNullOrEmpty(Model.Order.Department.DepartmentID)? "-": "[" + Model.Order.Department.DepartmentID + "] " + Model.Order.Department.DepartmentName %>
					</td>
				</tr>
				<tr>
					<td>
						<%=Html._("Mail.GoodReceive.Mobile")%>
					</td>
					<td>
						<%=Model.Order.Contact.ContactMobileNo%>
					</td>
					<td>
						<%=Html._("Mail.CostCenter")%>
					</td>
					<td>
						[<%=Model.Order.CostCenter.CostCenterID%>]
						<%=Model.Order.CostCenter.CostCenterName%>
					</td>
				</tr>
				<tr>
					<td>
						<%=Html._("Mail.GoodReceive.Fax")%>
					</td>
					<td>
						<%=Model.Order.Contact.ContactorFax%>
					</td>
					<td>
						<%=Html._("Mail.OrderDate")%>
					</td>
					<td>
						<%=Model.Order.OrderDate.ToString("dd/MM/yyyy HH:mm")%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<%=Html._("Mail.CustID")%>
					</td>
					<td>
						<%=Model.Order.CostCenter.CostCenterCustID%>
					</td>
				</tr>
			</table>
		</div>
		<hr />
		<div>
			<table>
				<tr>
					<td>
						<%=Html._("Mail.GoodReceive.InvoiceAddress")%>
					</td>
					<td>
						<%=Model.Order.Invoice.Address1%>
					</td>
					<td>
						<%=Html._("Mail.GoodReceive.ShippingAddress")%>
					</td>
					<td>
						<%=Model.Order.Shipping.Address1%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<%=Model.Order.Invoice.Address2%>
					</td>
					<td>
					</td>
					<td>
						<%=Model.Order.Shipping.Address2%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<%=Model.Order.Invoice.Address3%>
					</td>
					<td>
					</td>
					<td>
						<%=Model.Order.Shipping.Address3%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
						<%=Model.Order.Invoice.Address4%>
					</td>
					<td>
					</td>
					<td>
						<%=Model.Order.Shipping.Address4%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<%=Html._("Mail.Contactor")%>
					</td>
					<td>
						<%=Model.Order.Shipping.ShipContactor%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<%=Html._("Mail.GoodReceive.Mobile")%>
					</td>
					<td>
						<%=Model.Order.Shipping.ShipMobileNo%>
					</td>
				</tr>
				<tr>
					<td>
					</td>
					<td>
					</td>
					<td>
						<%=Html._("Mail.GoodReceive.Phone")%>
					</td>
					<td>
						<%=Model.Order.Shipping.ShipPhoneNo%>
					</td>
				</tr>
			</table>
		</div>
		<hr />
		<div class="GroupData">
			<%=Html._("Order.PartialReceive.DataGoodReceive")%>
		</div>
		<br />
		<div id="content-shopping-cart-detail">
			<label>
				<%=Html._("Order.PartialReceive.OrderId")%></label>
			<%=Model.OrderId %><br />
			<label>
				<%=Html._("Order.PartialReceive.GoodReceiveStatus")%></label>
			<%=Html._("Eprocurement2012.Models.GoodReceive+GoodReceiveStatus." + Model.Status.ToString())%>
		</div>
		<br />
		<div class="eclear">
		</div>
		<div class="GroupData">
			<%=Html._("Order.PartialReceive.OrderItems")%>
		</div>
		<div id="content-shopping-cart-detail">
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
				border: solid 1px #c5c5c5; text-align: center;">
				<tr>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductId")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.Items")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductUnit")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductOriginal")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductAction")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductCancel")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductRemain")%>
					</td>
				</tr>
				<%foreach (var item in Model.ItemReceiveDetail.Where(o => o.NumOfAction == 0))
	  {%>
				<tr>
					<td>
						<%=item.PId %>
					</td>
					<td>
						<%=item.Product.ThaiName %>
					</td>
					<td>
						<%=item.Product.ThaiUnit %>
					</td>
					<td>
						<%=item.OriginalQty %>
					</td>
					<td>
						<%=item.ActionQty %>
					</td>
					<td>
						<%=item.CancelQty %>
					</td>
					<td>
						<%=item.RemainQty %>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<br />
		<%foreach (var groupItem in Model.ItemReceiveDetail.Where(g => g.NumOfAction != 0).GroupBy(d => d.NumOfAction))
	{%>
		<div id="content-shopping-cart-detail">
			<label>
				<%=Html._("Order.PartialReceive.NumOfAction")%>
				<%=groupItem.First().NumOfAction %></label><br />
			<label>
				<%=Html._("Order.PartialReceive.ActionDate")%>
				<%=groupItem.First().ActionDate.ToString(new DateFormat()) %></label>
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
				border: solid 1px #c5c5c5; text-align: center;">
				<tr>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductId")%>
					</td>
					<td class="thead">
						<%=Html._("") %>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.Items")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.Remain")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductAction")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductCancel")%>
					</td>
					<td class="thead">
						<%=Html._("Order.PartialReceive.ProductRemain")%>
					</td>
				</tr>
				<%foreach (var item in groupItem)
	  {%>
				<tr>
					<td>
						<%=item.PId%>
					</td>
					<td>
						<%=item.Product.ThaiName%>
					</td>
					<td>
						<%=item.Product.ThaiUnit%>
					</td>
					<td>
						<%=item.OriginalQty%>
					</td>
					<td>
						<%=item.ActionQty%>
					</td>
					<td>
						<%=item.CancelQty%>
					</td>
					<td>
						<%=item.RemainQty%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<br />
		<%} %>
	</div>
</asp:Content>
