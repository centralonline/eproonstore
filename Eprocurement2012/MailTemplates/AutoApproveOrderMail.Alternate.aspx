﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.Dear")%>
	<%=Model.Order.Contact.ContactorName%>
	<br />
	<br />
	<%=string.Format(Html._("Mail.ApproveOrderMail.ContentCOL"), Model.Order.OrderID)%>
	<br />
	<br />
	<%Html.RenderPartial("OrderDetailPartialForMail", Model.Order);%>
	<br />
	<br />
	<u>
		<%=Html._("Mail.OtherRemark")%></u>
	<br />
	<%=Html._("Mail.ReferenceRemark")%>
	<%=string.IsNullOrEmpty(Model.Order.ApproverRemark) ? "-" : Model.Order.ApproverRemark%>
	<br />
	<%=Html._("Mail.OFMRemark")%>
	<%=string.IsNullOrEmpty(Model.Order.OFMRemark)? "-": Model.Order.OFMRemark%>
	<br />
	<%--<%=string.IsNullOrEmpty(Model.Order.AttachFile)? "" : "เอกสารอ้างอิง:" + Model.Order.AttachFile%>--%>
	<%=string.IsNullOrEmpty(Model.Order.CustFileName) ? "" : Html._("Mail.AttachFile") + Model.Order.CustFileName%>
	<br />
	<%=string.IsNullOrEmpty(Model.Order.CallBackRequestText) ? "" : "*" + Model.Order.CallBackRequestText%>
	<br />
	<br />
	mail reference: 23(Auto Approved)
</asp:Content>
