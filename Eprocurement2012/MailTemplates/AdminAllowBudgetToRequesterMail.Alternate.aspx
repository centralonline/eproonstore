﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.Dear")%>
	<%=Model.Order.Contact.ContactorName%>
	<br />
	<br />
	<%=string.Format(Html._("Mail.AdminAllowBudgetToRequesterMail.Content"), Model.Order.PreviousApprover.Approver.DisplayName, Model.Order.OrderID)%>
	<br />
	<br />
	mail reference: 09(Admin Allow)
</asp:Content>
