﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ApplyNow>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<b><%=Html._("Mail.ApplyNow.Information")%></b>
	<br />
	<br />
	<%=Html._("Mail.CompanyName")%>
	<%=Model.CompanyName %>
	<br />
	<%=Html._("Mail.BusinessType")%>
	<%=Model.BusinessType %>
	<br />
	<%=Html._("Mail.CustEmpNum")%>
	<%=Model.CustEmpNum%>
	<br />
	<%=Html._("Mail.NumOfCompany")%>
	<%=Model.NumOfCompany%>
	<br />
	<%=Html._("Mail.Name")%>
	<%=Model.Name %>
	<br />
	<%=Html._("Mail.Position")%>
	<%=Model.Position %>
	<br />
	<%=Html._("Mail.PhoneNo")%>
	<%=Model.PhoneNo %>
	<br />
	<%=Html._("Mail.MobileNo")%>
	<%=Model.MobileNo %>
	<br />
	<%=Html._("Mail.Email")%>
	<%=Model.Email %>
	<br />
	<br />
	<b><%=Html._("Mail.Question")%></b>
	<br />
	<b><%=Html._("Mail.IsCustomerDisplay")%></b>
	<br />
	<%=Model.IsCustomerDisplay %>
	<br />
	<br />
	<b><%=Html._("Mail.InterestProgram")%></b>
	<br />
	<%=Model.InterestProgram %>
	<br />
	<br />
	<b><%=Html._("Mail.UseOnline")%></b>
	<br />
	<%=Model.UseOnline %>
	<br />
	<br />
	mail reference: 19(Apply Now)
</asp:Content>
