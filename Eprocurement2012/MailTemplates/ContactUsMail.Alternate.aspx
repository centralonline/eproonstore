﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ContactUs>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Home.ContactUs.UserName")%>:
	<%=Model.UserName %>
	<br />
	<%=Html._("Home.ContactUs.Email")%>:
	<%=Model.UserID %>
	<br />
	<%=Html._("Home.ContactUs.CompanyId")%>:
	<%=Model.CompanyId %>
	<br />
	<%=Html._("Home.ContactUs.CompanyName")%>:
	<%=Model.CompanyName %>
	<br />
	<%=Html._("Home.ContactUs.PhoneNo")%>:
	<%=Model.PhoneNo %>
	<br />
	<br />
	<%=Html._("Home.ContactUs.ContactTitle")%>:
	<%=Model.ContactTitle %>
	<br />
	<%=Html._("Home.ContactUs.ContactDetail")%>:
	<%=Model.ContactDetail %>
	<br />
	<br />
	mail reference: 11(Contact eProcurement)
</asp:Content>
