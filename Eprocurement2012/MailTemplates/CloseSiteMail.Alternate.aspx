﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Site>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.DearUser")%>
	<br />
	<br />
	<%=Html._("Mail.CloseSiteMail.Alternate")%>
	<br />
	<br />
	<%=Html._("Mail.Remark")%>
	<%=Model.AdminRemarkMail %>
	<br />
	<br />
	mail reference: 17(Close Site)
</asp:Content>
