﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.Dear")%>
	<%=Model.Order.CurrentApprover.Approver.DisplayName%>
	<br />
	<br />
	<%=string.Format(Html._("Mail.ApproverDeleteOrderMail.Content"), Model.Order.Contact.ContactorName, Model.Order.OrderID)%>
	<br />
	<br />
	<%=Html._("Mail.DeleteRemark")%>
	<%=string.IsNullOrEmpty(Model.Order.ApproverRemark) ? "-" : Model.Order.ApproverRemark%>
	<br />
	<br />
	mail reference: 07(Requester Delete Order)
</asp:Content>
