﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.Dear")%>
	<%=Model.Order.CurrentApprover.Approver.DisplayName%>
	<br />
	<br />
	<%=string.Format(Html._("Mail.CreateOrderMail.Content"), Model.Order.Contact.ContactorName, Model.Order.OrderID, Model.Order.ParkDay)%>
	<br />
	<br />
	<%Html.RenderPartial("OrderDetailPartialForMail", Model.Order);%>
	<br />
	<br />
	<u><%=Html._("Mail.OtherRemark")%></u>
	<br />
	<%=Html._("Mail.ReferenceRemark")%>
	<%=string.IsNullOrEmpty(Model.Order.ApproverRemark) ? "-" : Model.Order.ApproverRemark%>
	<br />
	<%=Html._("Mail.OFMRemark")%>
	<%=string.IsNullOrEmpty(Model.Order.OFMRemark)? "-": Model.Order.OFMRemark%>
	<br />
	<%=Html._("Mail.AttachFile")%>
	<%--<%=string.IsNullOrEmpty(Model.Order.AttachFile) ? "-" : Model.Order.AttachFile%>--%>
	<%=string.IsNullOrEmpty(Model.Order.CustFileName) ? "-" : Model.Order.CustFileName%>
	<br />
	<%=Html._("Mail.ReferenceNo")%>
	<%=string.IsNullOrEmpty(Model.Order.ReferenceRemark) ? "-" : Model.Order.ReferenceRemark%>
	<br />
	<%=string.IsNullOrEmpty(Model.Order.CallBackRequestText) ? "" : "*" + Model.Order.CallBackRequestText%>
	<br />
	<br />
	<a href="<%=Url.FullUrlAction("ViewOrderForApprovalFromMail", "Account", new { id = Model.Order.OrderGuid })%>">
		<%=Html._("Mail.ClickOrderForApprovalFromMail")%></a>
	<br />
	<br />
	mail reference: 01(Waiting For Approve)
</asp:Content>
