﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.NotifyMeMailData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
	e-Mail Notify
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<p><%=string.Format(Html._("Mail.Notify.Content"), Model.NotifyEmail)%></p>
	<p>
		<%=Html._("Mail.Notify.Remark")%>
		<%=Model.NotifyRemark %></p>
	<p>
		<%=Html._("Mail.Notify.ProductName")%>
		<%=Model.ProductDetail.Name%></p>
	<p>
		<%=Html._("Mail.Notify.ProductID")%>
		<%=Model.ProductDetail.Id%></p>
	<br />
	<br />
	<img src="<%=Url.GetFullUrl(Model.ProductDetail.ImageSmallUrl) %>" alt="<%=Html.Encode(Model.ProductDetail.Name) %>" />
</asp:Content>
