﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.NewSiteData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%=Html._("Mail.Createnewsite")%><br />
		<table>
			<tr>
				<td>
					<%=Html._("Mail.CompanyID")%>
				</td>
				<td>
					<%=Model.CompanyId %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.CustomerID")%>
				</td>
				<td>
					<%=Model.CustId %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.CompanyTName")%>
				</td>
				<td>
					<%=Model.CompanyTName %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.CompanyEName")%>
				</td>
				<td>
					<%=Model.CompanyEName %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.PriceFormat")%>
				</td>
				<td>
					<%=Model.DisplayPriceType %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.DiscountRate")%>
				</td>
				<td>
					<%=Model.DiscountRate %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.InvoiceAddress")%>
				</td>
				<td>
					<%=Model.InvoiceAddress.Address1 %>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<%=Model.InvoiceAddress.Address2 %>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<%=Model.InvoiceAddress.Address3 %>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<%=Model.InvoiceAddress.Address4 %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.DeliveryAddress")%>
				</td>
				<td>
					<%=Model.ShippingAddress.Address1 %>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<%=Model.ShippingAddress.Address2 %>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<%=Model.ShippingAddress.Address3 %>
				</td>
			</tr>
			<tr>
				<td>
				</td>
				<td>
					<%=Model.ShippingAddress.Address4 %>
				</td>
			</tr>
		</table>
	</div>
	<br />
	<div>
		<%=Html._("Mail.DataSetting")%>
		<table>
			<tr>
				<td>
					<%=Html._("Mail.CompanyModel")%>
				</td>
				<td>
					<%=Model.DisplayCompanyModel %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.OrderControlType")%>
				</td>
				<td>
					<%=Model.DisplayOrderControlType %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.BudgetLevelType")%>
				</td>
				<td>
					<%=Model.DisplayBudgetLevelType %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.BudgetPeriodType")%>
				</td>
				<td>
					<%=Model.DisplayBudgetPeriodType %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.ParkDay")%>
				</td>
				<td>
					<%=string.Format(Html._("Mail.DefaultParkDay"),Model.DefaultParkDay)%>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.UseOfmCatalog")%>
				</td>
				<td>
					<%=Model.DisplayUseOfmCatalog %>
				</td>
			</tr>
		</table>
	</div>
	<br />
	<div>
		<%=Html._("Mail.Authorization")%>
		<table>
			<tr>
				<td>
					<%=Html._("Mail.BasicFeatures")%>
				</td>
				<%if (Model.ShowContactUs)
	  {%>
				<td>
					<%=Html._("Mail.ShowContactUs")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.ShowContactUs")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.ShowSpecialProd)
	  {%>
				<td>
					<%=Html._("Mail.ShowSpecialProd")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.ShowSpecialProd")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.UseCompanyNews)
	  {%>
				<td>
					<%=Html._("Mail.UseCompanyNews")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.UseCompanyNews")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.UseOfmNews)
	  {%>
				<td>
					<%=Html._("Mail.UseOfmNews")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.UseOfmNews")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.UseSMSFeature)
	  {%>
				<td>
					<%=Html._("Mail.UseSMSFeature")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.UseSMSFeature")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.SpecialFeatures")%>
				</td>
				<%if (Model.IsByPassApprover)
	  {%>
				<td>
					<%=Html._("Mail.IsByPassApprover")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.IsByPassApprover")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.IsByPassAdmin)
	  {%>
				<td>
					<%=Html._("Mail.IsByPassAdmin")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.IsByPassAdmin")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.IsAutoApprove)
	  {%>
				<td>
					<%=Html._("Mail.IsAutoApprove")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.IsAutoApprove")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.DefaultDeliCharge)
	  {%>
				<td>
					<%=Html._("Mail.DefaultDeliCharge")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.DefaultDeliCharge")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.AllowAddProduct)
	  {%>
				<td>
					<%=Html._("Mail.AllowAddProduct")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.AllowAddProduct")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.ShowOutofStock)
	  {%>
				<td>
					<%=Html._("Mail.ShowOutofStock")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.ShowOutofStock")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.ShowOfmCat)
	  {%>
				<td>
					<%=Html._("Mail.ShowOfmCat")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.ShowOfmCat")%>
				</td>
				<%} %>
			</tr>
			<tr>
				<td>
				</td>
				<%if (Model.UseReferenceCode)
	  {%>
				<td>
					<%=Html._("Mail.UseReferenceCode")%>
				</td>
				<%} %>
				<%else
	  {%>
				<td>
					<span style="color: Red; text-decoration: underline; font-weight: bold;">
						<%=Html._("Mail.Not")%></span>
					<%=Html._("Mail.UseReferenceCode")%>
				</td>
				<%} %>
			</tr>
		</table>
		<br />
		<table>
			<tr>
				<td>
					<%=Html._("Mail.AdminOfmRemark")%>
				</td>
				<td>
					<%=Model.AdminOfmRemark %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.CreateBy")%>
				</td>
				<td>
					<%=Model.CreateBy %>
				</td>
			</tr>
			<tr>
				<td>
					<%=Html._("Mail.SaleRequest")%>
				</td>
				<td>
					<%=Model.SaleRequest %>
				</td>
			</tr>
		</table>
	</div>
	<br />
	mail reference: 20 (Create New Site)
</asp:Content>
