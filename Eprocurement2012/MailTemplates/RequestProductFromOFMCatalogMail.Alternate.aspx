﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.RequestSpecialProduct>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	
	<%=Html._("Mail.Requestingform")%>
	<br />
	<br />
	<%=Html._("Mail.CompanyID")%>
	<%=Model.CompanyId%>
	<br />
	<%=Html._("Mail.CompanyName")%>
	<%=Model.UserName%>
	(<%=Model.UserId%>)
	<br />
	<%=Html._("Mail.RequestingDate")%>
	<%=Model.CreateOn%>
	<br />
	<br />
	<%=Html._("Mail.DearAdmin")%>
	<br />
	<%=string.Format(Html._("Mail.RequestProductFromOFMCatalogMail.Alternate"),Model.UserName)%>
	<br />
	<br />
	<b><%=Html._("Mail.ProductDetail")%></b>
	<br />
	<%=Html._("Mail.ProductName")%><%=Model.ProductName%>
	<br />
	<%=Html._("Mail.TimeOrder")%><%=Model.Period%>
	<br />
	<%=Html._("Mail.Remark")%><%=Model.Remark%>
	<br />
	<br />
	mail reference: 12(Requester product from Officemate Catalog)
</asp:Content>
