﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MailTemplates/HeaderMail.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="TitleContent" runat="server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html._("Mail.Dear")%>
	<%=Model.Order.CurrentApprover.Approver.DisplayName%>
	<br />
	<br />
	<%=string.Format(Html._("Mail.AdminAllowBudgetToApproveMail.Content"), Model.Order.PreviousApprover.Approver.DisplayName, Model.Order.OrderID, Model.Order.ParkDay)%>
	<br />
	<br />
	<a href="<%=Url.FullUrlAction("ViewOrderForApprovalFromMail", "Account", new { id = Model.Order.OrderGuid })%>">
		<%=Html._("Mail.ClickOrderForApprovalFromMail")%></a>
	<br />
	<br />
	mail reference: 10(Waiting for Admin Allow)
</asp:Content>
