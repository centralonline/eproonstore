﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;
using log4net;
using System.Data.SqlClient;
using System.Web.Configuration;
using Eprocurement2012.Models.Repositories;
using System.Net.Mail;
using Eprocurement2012.Controllers.Mails;
using OfficeMate.Framework.Security.Cryptography;
using OfficeMate.SmsService;

namespace Eprocurement2012.Controllers
{
	public class AbstractEproController : Controller
	{

		protected ILog Log { get; private set; }
		protected string cacheKeyLastActive = "LastActiveEpro/{0}";
		public AbstractEproController(ILoginProvider loginProvider, User user)
		{
			Log = LogManager.GetLogger(this.GetType());
			LoginProvider = loginProvider;
			User = user ?? new User();
		}
		public AbstractEproController() : this(null, null) { }
		public BackToShoppingCookieProvider BackToShoppingProvider { get; set; }
		public new User User { get; set; }
		public User LoadUser() //ดึงข้อมูลผู้ใช้งานด้วย userGuid (ต้อง login แล้วเท่านั้น)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				string userGuid = LoginProvider.ReadUserGuid();
				if (userGuid != null)
				{
					var userRepo = new UserRepository(connection);
					User = userRepo.GetUser(userGuid, User.UserStatus.Active);
					if (User != null)
					{
						User.UserContact = userRepo.GetUserDataContact(User.UserId, User.CurrentCompanyId);
						User.UserPermission = userRepo.GetUserPermission(User.UserId, User.CurrentCompanyId);
						if (User.UserContact.Phone != null)
						{
							User.TelephoneId = User.UserContact.Phone.Id;
							User.TelephoneNumber = User.UserContact.Phone.PhoneNo;
							User.ExtTelephoneNumber = User.UserContact.Phone.Extension;
						}
						if (User.UserContact.Mobile != null)
						{
							User.MobileId = User.UserContact.Mobile.Id;
							User.MobileNumber = User.UserContact.Mobile.PhoneNo;
						}
						if (User.UserContact.Fax != null)
						{
							User.FaxId = User.UserContact.Fax.Id;
							User.FaxNumber = User.UserContact.Fax.PhoneNo;
						}
						User.IPAddress = IPAddress;
						User.IsMultiCompany = userRepo.IsUserMultiCompany(User.UserId);
					}
					else if (User == null && userRepo.IsOFMAdmin(userGuid))
					{
						User = userRepo.GetUserOFMAdmin(userGuid, User.UserStatus.Active);
						User.IPAddress = IPAddress;
					}
				}
			}
			return User;
		}
		public ILoginProvider LoginProvider { get; set; }
		public IProductHistoryProvider ProductHistoryProvider { get; set; }
		public string CurrentUrl { get; set; }
		public string ReferrerUrl { get; set; }
		public string IPAddress { get; set; }
		public bool IsFirstVisitOfTheDay { get; set; }
		public bool IsSecureConnection { get; set; }
		public TrackingCookieProvider TrackingCookieProvider { get; set; }
		public ShoppingCartProvider ShoppingCartProvider { get; set; }
		protected virtual bool IsProduction
		{
			get { return Boolean.Parse(GetSetting("IsProduction")); }
		}
		protected string GetSetting(string settingKey)
		{
			return WebConfigurationManager.AppSettings[settingKey];
		}
		protected virtual string ConnectionString
		{
			get
			{
				if (IsProduction)
				{
					return ConnectionStringEncryption.Decrypt(WebConfigurationManager.ConnectionStrings["ProductionServer"].ConnectionString);
				}
				else
				{
					return ConnectionStringEncryption.Decrypt(WebConfigurationManager.ConnectionStrings["DevelopmentServer"].ConnectionString);
				}
			}
		}
		//protected virtual string ConnectionStringMaster
		//{
		//    get
		//    {
		//        if (IsProduction)
		//        {
		//            return ConnectionStringEncryption.Decrypt(WebConfigurationManager.ConnectionStrings["ProductionServerMaster"].ConnectionString);
		//        }
		//        else
		//        {
		//            return ConnectionStringEncryption.Decrypt(WebConfigurationManager.ConnectionStrings["DevelopmentServerMaster"].ConnectionString);
		//        }
		//    }
		//}
		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			try
			{

				LoginProvider = LoginProvider ?? new LoginCookieProvider(filterContext);
				ProductHistoryProvider = new ProductHistoryCookieProvider(filterContext);
				BackToShoppingProvider = new BackToShoppingCookieProvider(filterContext);

				if (filterContext.ActionDescriptor.GetCustomAttributes(typeof(BackToShoppingAttribute), true).Length != 0)
				{
					BackToShoppingProvider.Set(filterContext.HttpContext.Request.Url.ToString());
				}

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					string userGuid = LoginProvider.ReadUserGuid();
					if (userGuid != null)
					{
						LoadUser(); //ดึงข้อมูลผู้ใช้งานด้วย userGuid (ต้อง login แล้วเท่านั้น)
						cacheKeyLastActive = string.Format(cacheKeyLastActive, userGuid);
						if (System.Web.Hosting.HostingEnvironment.Cache.Get(cacheKeyLastActive) == null)
						{
							new UserRepository(connection).UpdateLastActive(User.UserId);
							System.Web.Hosting.HostingEnvironment.Cache.Insert(cacheKeyLastActive, true, null, DateTime.Now.AddHours(1), System.Web.Caching.Cache.NoSlidingExpiration);
						}
					}
				}

				ShoppingCartProvider = new ShoppingCartProvider(filterContext, User, ConnectionString);
				CurrentUrl = filterContext.HttpContext.Request.Url.ToString();
				ReferrerUrl = filterContext.HttpContext.Request.UrlReferrer != null ? filterContext.HttpContext.Request.UrlReferrer.ToString() : String.Empty;
				IPAddress = filterContext.HttpContext.Request.IPAddress();
				IsFirstVisitOfTheDay = new FirstVisitOfTheDayProvider(filterContext).GetFirstVisitOfTheDay();
				IsSecureConnection = filterContext.HttpContext.Request.IsSecureConnection;

				if (filterContext.RouteData.Values.ContainsKey("locale"))
				{
					System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture(filterContext.RouteData.Values["locale"].ToString());
				}
				else
				{
					System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture(System.Threading.Thread.CurrentThread.CurrentUICulture.TwoLetterISOLanguageName);
				}
				base.OnActionExecuting(filterContext);
			}
			catch (Exception exception)
			{
				if (IsProduction)
				{
					Log.Error(new ExceptionContext(filterContext, exception));
					filterContext.Result = Http500();
				}
				else
				{
					throw;
				}
			}
		}
		protected override void OnException(ExceptionContext filterContext)
		{
			if (IsProduction)
			{
				Log.Error(filterContext);
				Http500().ExecuteResult(ControllerContext);
				filterContext.ExceptionHandled = true;
				base.OnException(filterContext);
			}
			else
			{
				base.OnException(filterContext);
			}
		}
		protected override void HandleUnknownAction(string actionName)
		{
			Http404().ExecuteResult(ControllerContext);
		}
		private SmtpClient _smtpClient;
		protected virtual void SendMail(AbstractMailTemplate message) /*สำหรับ mail ที่ไม่ต้องเก็บ log*/
		{
			if (_smtpClient == null)
			{
				string host = IsProduction ? GetSetting("ProductionSmtpServer") : GetSetting("DevelopmentSmtpServer");
				_smtpClient = new SmtpClient(host);
				if (IsProduction)
				{
					_smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("ProductionSmtpServerUsername"), GetSetting("ProductionSmtpServerPassword"));
				}
				else
				{
					_smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("DevelopmentSmtpServerUsername"), GetSetting("DevelopmentSmtpServerPassword"));
				}
			}
			try
			{
				_smtpClient.Send(message);
			}
			catch (System.Net.Mail.SmtpFailedRecipientException)
			{
				Log.Warn("System.Net.Mail.SmtpFailedRecipientException Swallowed");
			}
		}
		protected virtual void SendMailNotify(AbstractMailTemplate message, MailType mailType, string username) /*สำหรับ mail Notify product ต้องเก็บ log*/
		{
			if (_smtpClient == null)
			{
				string host = IsProduction ? GetSetting("ProductionSmtpServer") : GetSetting("DevelopmentSmtpServer");
				_smtpClient = new SmtpClient(host);
				if (IsProduction)
				{
					_smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("ProductionSmtpServerUsername"), GetSetting("ProductionSmtpServerPassword"));
				}
				else
				{
					_smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("DevelopmentSmtpServerUsername"), GetSetting("DevelopmentSmtpServerPassword"));
				}
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				EmailRepository eMailRepo = new EmailRepository(connection);
				try
				{
					_smtpClient.Send(message);
					eMailRepo.AddNotifyProductMailLog(message, mailType, "Yes", username);
				}
				catch (System.Net.Mail.SmtpFailedRecipientException)
				{
					Log.Warn("System.Net.Mail.SmtpFailedRecipientException Swallowed");
					eMailRepo.AddNotifyProductMailLog(message, mailType, "No", username);
				}
				catch (Exception)
				{
					eMailRepo.AddNotifyProductMailLog(message, mailType, "No", username);
					throw;
				}
			}
		}
		protected virtual void SendMail(AbstractMailTemplate message, string orderId, MailType mailType, string username) /*สำหรับ mail ที่ต้องเก็บ log*/
		{
			if (_smtpClient == null)
			{
				string host = IsProduction ? GetSetting("ProductionSmtpServer") : GetSetting("DevelopmentSmtpServer");
				_smtpClient = new SmtpClient(host);
				if (IsProduction)
				{
					_smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("ProductionSmtpServerUsername"), GetSetting("ProductionSmtpServerPassword"));
				}
				else
				{
					_smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("DevelopmentSmtpServerUsername"), GetSetting("DevelopmentSmtpServerPassword"));
				}
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				EmailRepository eMailRepo = new EmailRepository(connection);
				try
				{
					_smtpClient.Send(message);
					eMailRepo.AddMailTranLog(message, orderId, mailType, "Yes", username,"");
				}
				catch (System.Net.Mail.SmtpFailedRecipientException ex)
				{
					Log.Warn("System.Net.Mail.SmtpFailedRecipientException Swallowed");
					eMailRepo.AddMailTranLog(message, orderId, mailType, "No", username, ex.Message);
				}
				catch (Exception ex)
				{
					eMailRepo.AddMailTranLog(message, orderId, mailType, "No", username, ex.Message);
					throw;
				}
			}
		}

		private PieSoftSmsService _smsService;
		protected virtual string SmsSenderName
		{
			get { return ConnectionStringEncryption.Decrypt(GetSetting("ProductionSmsSenderName")); }
		}

		protected void SendSms(string phoneNumber, string message)
		{
			if (_smsService == null)
			{
				string _smsUserName = ConnectionStringEncryption.Decrypt(GetSetting("ProductionSmsUser"));
				string _smsPassword = ConnectionStringEncryption.Decrypt(GetSetting("ProductionSmsPassword"));
				_smsService = new PieSoftSmsService(_smsUserName, _smsPassword);
			}

			if (!string.IsNullOrEmpty(phoneNumber))
			{
				try
				{
					if (_smsService.GetRemainingCredit() > 0)
					{
						_smsService.SendMessage(message, SmsSenderName, phoneNumber);
					}
					else
					{
						Log.Warn("Sms service : out of credit");
					}
				}
				catch (Exception ex)
				{
					Log.Error("Sms Error : " + ex.ToString());
					Log.Warn("Sms Service can not send sms message: " + message + " to:" + phoneNumber);
				}
			}
		}

		protected void SendSmsOrder(string phoneNumber, string message)
		{
			if (Boolean.Parse(GetSetting("IsSmsOrder")))
			{
				SendSms(phoneNumber, message);
			}
		}

		protected override ViewResult View(IView view, object model)
		{
			model = MergeMasterPageData(model);
			return base.View(view, model);
		}
		protected override ViewResult View(string viewName, string masterName, object model)
		{
			model = MergeMasterPageData(model);
			return base.View(viewName, masterName, model);
		}
		private object MergeMasterPageData(object model)
		{
			model = model ?? ViewData.Model ?? new MasterPageData();
			var data = model as IMasterPageData;
			if (data != null)
			{
				data.User = User;
				var redirectUrlValue = ValueProvider.GetValue("RedirectUrl");
				data.RedirectUrl = redirectUrlValue != null ? redirectUrlValue.AttemptedValue : CurrentUrl;
				data.CurrentUrl = CurrentUrl;
				if (ShoppingCartProvider != null)
				{
					data.ItemCount = ShoppingCartProvider.ItemCount;
					data.ItemCountQty = ShoppingCartProvider.ItemCountQty;
				}
				data.IsSecureConnection = IsSecureConnection;
				data.IsProduction = IsProduction;

			}
			return model;
		}
		protected ActionResult Http500()
		{
			Response.StatusCode = 500;
			ViewData["Error"] = "Http500";
			ViewData["Messages"] = "ขอโทษค่ะ ระบบไม่สามารถแสดงหน้านี้ได้ในขณะนี้";
			ViewData["CurrentUrl"] = CurrentUrl;
			return View("Error");
		}
		protected ActionResult Http404()
		{
			Response.StatusCode = 404;
			ViewData["Error"] = "Http404";
			ViewData["Messages"] = "ขอโทษค่ะ หน้าที่คุณต้องการไม่มีในระบบ";
			ViewData["CurrentUrl"] = CurrentUrl;
			return View("Error");
		}
		protected ActionResult Http403()
		{
			Response.StatusCode = 403;
			ViewData["Error"] = "Http403";
			ViewData["Messages"] = "ขอโทษค่ะ คุณไม่มีสิทธิ์เข้าหน้านี้";
			ViewData["CurrentUrl"] = CurrentUrl;
			return View("Error");
		}
		protected ActionResult Offline()
		{
			Response.StatusCode = 500;
			return View("Offline");
		}
		private bool IsSecureConnectionButNotRequiredByAction(ActionExecutingContext filterContext)
		{
			return filterContext.HttpContext.Request.IsSecureConnection && filterContext.ActionDescriptor.GetCustomAttributes(typeof(RequireHttpsAttribute), true).Length == 0 && filterContext.ActionDescriptor.GetCustomAttributes(typeof(AcceptBothHttpHttpsAttribute), true).Length == 0;
		}

	}
}
