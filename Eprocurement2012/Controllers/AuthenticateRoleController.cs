﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;

namespace Eprocurement2012.Controllers
{
	public class AuthenticateRoleController : AbstractEproController
	{
		// GET: /AuthenticateRole/
		protected override void OnActionExecuting(ActionExecutingContext filterContext)
		{
			base.OnActionExecuting(filterContext);
			if (!User.IsLogin) { filterContext.Result = RedirectToAction("LogIn", "Account", new { RedirectUrl = CurrentUrl }); return; }

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				AuthorizeRepository authorizeRepo = new AuthorizeRepository(connection);
				if (!authorizeRepo.AuthorizeMenu(User.UserId, User.CurrentCompanyId, filterContext.RouteData.Values["action"].ToString(), filterContext.RouteData.Values["controller"].ToString()))
				{
					filterContext.Result = Http403();
				}
				System.Threading.Thread.CurrentThread.CurrentUICulture = System.Globalization.CultureInfo.CreateSpecificCulture(User.UserLanguage.ToString());
			}
		}
	}
}
