﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers
{
	public class ApproveOrderController : AuthenticateRoleController
	{

		public ActionResult ViewOrderForApprover(string Id) //เข้าจากหน้าเว็บ
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(Id);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
				if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)//หากใช้ campany catalog และแสดง ofm catalog จะต้องเช็คว่า สินค้าใดมาจาก ofm catalog บ้าง
				{
					IEnumerable<string> listId = new ProductRepository(connection, User).IsListPidInProductCatalog();
					foreach (var prod in orderData.Order.ItemOrders)
					{
						if (!listId.Any(p => p == prod.Product.Id))
						{
							prod.IsOfmCatalog = true;
							orderData.Order.SendToAdminAppProd = true;
						}
					}
				}
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)//หากมีการคุม budget จะต้องเอายอดใบสั่งซื้อไปลบออกจาก budget
				{
					BudgetRepository repo = new BudgetRepository(connection);
					orderData.CurrentBudget = repo.GetBudgetByCostcenterID(orderData.Order.CostCenter.CostCenterID, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					orderData.CurrentBudget.WaitingOrderAmt = repo.GetWaitingOrderOnThisBudget(orderData.CurrentBudget.GroupID, User.CurrentCompanyId, User.Company.BudgetLevelType);
					orderData.Order.SendToAdminAppBudg = orderData.Order.GrandTotalAmt > orderData.CurrentBudget.RemainBudgetAmt;
				}
				orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
				orderData.Order.User = User; //ใส่ค่า User  เพื่อไว้เช็คใน Partial
				return View("ViewOrderForApprover", orderData);
			}
		}

		public ActionResult ConfirmHandleOrder(ImageInputData action, string orderGuid)
		{
			ConfirmHandelOrder data = new ConfirmHandelOrder();
			data.InputActionName = action.Name;
			data.OrderGuid = orderGuid;
			return View("ConfirmHandelOrder", data);
		}

		public ActionResult HandleEditOrder(ImageInputData action, string[] productID, int[] quantity)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(action.Value);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order และส่งไปแสดงที่ ViewOrder
				if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)//หากใช้ campany catalog และแสดง ofm catalog จะต้องเช็คว่า สินค้าใดมาจาก ofm catalog บ้าง
				{
					IEnumerable<string> listId = new ProductRepository(connection, User).IsListPidInProductCatalog();
					foreach (var prod in orderData.Order.ItemOrders)
					{
						if (!listId.Any(p => p == prod.Product.Id))
						{
							prod.IsOfmCatalog = true;
							orderData.Order.SendToAdminAppProd = true;
						}
					}
				}
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)//หากมีการคุม budget จะต้องเอายอดใบสั่งซื้อไปลบออกจาก budget
				{
					BudgetRepository repo = new BudgetRepository(connection);
					orderData.CurrentBudget = repo.GetBudgetByCostcenterID(orderData.Order.CostCenter.CostCenterID, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					orderData.CurrentBudget.WaitingOrderAmt = repo.GetWaitingOrderOnThisBudget(orderData.CurrentBudget.GroupID, User.CurrentCompanyId, User.Company.BudgetLevelType);
					orderData.Order.SendToAdminAppBudg = orderData.Order.GrandTotalAmt > orderData.CurrentBudget.RemainBudgetAmt;
				}
				orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
				orderData.Order.User = User; //กรณี LogIn แล้ว มีข้อมูล User ดึงข้อมูลมาใส่ เพื่อไว้เช็คว่า คนอนุมัติต้องเป็น ผู้อนุมัติ ,Admin หรือ ผู้ช่วย Admin

				if (action.Name == "EditOrder")
				{
					orderData.Order.IsEdit = true;
					return View("ViewOrderForApprover", orderData);
				}
				else if (action.Name == "SaveOrder")
				{
					Dictionary<string, int> newPropQty;
					if (SetUpdateItem(productID, quantity, out newPropQty)) //check ความถูกต้องของข้อมูล
					{
						orderData.Order.User.IPAddress = IPAddress;
						ReCalulateOrder(orderData.Order, newPropQty);
						orderRePo.UpdateOrderEditbyApprover(orderData.Order);

						orderData.Order = orderRePo.GetMyOrder(orderId); // นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
						orderData.User = User;
						orderRePo.CreateOrderDetailTransactionLog(orderData, OrderData.OrderStatusLog.Revise); //เก็บ log
					}
					else
					{
						orderData.Order.IsEdit = true;
						return View("ViewOrderForApprover", orderData);
					}
				}
				return RedirectToAction("ViewOrderForApprover", new { Id = action.Value });
			}
		}

		//Action สำหรับแก้ไขสินค้าใน Cart
		private bool SetUpdateItem(string[] productID, int[] quantity, out Dictionary<string, int> newPropQty)
		{
			Dictionary<string, int> newCart = new Dictionary<string, int>();
			bool IsValid = true;

			for (int i = 0; i < productID.Length; i++)
			{
				if (quantity[i] <= 0)
				{
					TempData["QuantityErrorPid" + productID[i]] = new ResourceString("ErrorMessage.Quantity") + quantity[i] + new ResourceString("ErrorMessage.Incorrect");
					//IsValid = false;
				}
				//else
				//{
				newCart.Add(productID[i], quantity[i]);
				//}
			}

			if (quantity.All(i => i == 0)) { IsValid = false; }

			newPropQty = newCart;
			return IsValid;
		}

		private Order ReCalulateOrder(Order order, Dictionary<string, int> newPropQty)
		{
			foreach (var item in order.ItemOrders)
			{
				foreach (var keyPair in newPropQty)
				{
					if (keyPair.Key == item.Product.Id)
					{
						item.Quantity = keyPair.Value;
						item.DiscAmt = item.DiscountRate > 0 ? Math.Round((item.Product.PriceExcVat * order.DiscountRate) / 100, 2, MidpointRounding.AwayFromZero) * item.Quantity : 0;
					}
				}
			}

			order.TotalAllDiscount = order.ItemOrders.Where(Item => Item.DiscountRate > 0 && Item.Quantity > 0).Sum(Item => Math.Round((Item.Product.PriceExcVat * order.DiscountRate) / 100, 2, MidpointRounding.AwayFromZero) * Item.Quantity);
			order.TotalAllDiscountExcVat = order.ItemOrders.Where(Item => Item.DiscountRate > 0 && Item.Product.IsVat && Item.Quantity > 0).Sum(Item => Math.Round((Item.Product.PriceExcVat * order.DiscountRate) / 100, 2, MidpointRounding.AwayFromZero) * Item.Quantity);
			order.TotalAllDiscountNonVat = order.ItemOrders.Where(Item => Item.DiscountRate > 0 && !Item.Product.IsVat && Item.Quantity > 0).Sum(Item => Math.Round((Item.Product.PriceExcVat * order.DiscountRate) / 100, 2, MidpointRounding.AwayFromZero) * Item.Quantity);

			order.TotalPriceProductExcVat = Math.Round(order.ItemOrders.Where(Item => Item.Product.IsVat && Item.Quantity > 0).Sum(Item => Item.Product.PriceExcVat * Item.Quantity), 2, MidpointRounding.AwayFromZero);
			order.TotalPriceProductNonVat = Math.Round(order.ItemOrders.Where(Item => !Item.Product.IsVat && Item.Quantity > 0).Sum(Item => Item.Product.PriceExcVat * Item.Quantity), 2, MidpointRounding.AwayFromZero);
			order.TotalPriceProductExcVatAmount = order.TotalPriceProductExcVat - order.TotalAllDiscountExcVat;
			order.TotalPriceProductNoneVatAmount = order.TotalPriceProductNonVat - order.TotalAllDiscountNonVat;

			order.TotalDeliveryCharge = order.ItemOrders.Sum(Item => Item.Product.TransCharge * Item.Quantity);
			order.ItemCountOrder = order.ItemOrders.Sum(Item => Item.Quantity);
			order.TotalAmt = Math.Round(order.TotalPriceProductExcVat + order.TotalPriceProductNonVat + order.TotalDeliveryCharge, 2);
			order.TotalNetAmt = Math.Round(order.TotalPriceProductExcVat + order.TotalPriceProductNonVat + order.TotalDeliveryCharge - order.TotalAllDiscount, 2);
			order.TotalVatAmt = Math.Round(((order.TotalPriceProductExcVat - Math.Abs(order.TotalAllDiscountExcVat)) * 7) / 100, 2);
			order.GrandTotalAmt = Math.Round((order.TotalPriceProductExcVat + order.TotalPriceProductNonVat + order.TotalVatAmt + order.TotalDeliveryCharge) - Math.Abs(order.TotalAllDiscount), 2);

			return order;
		}

		public ActionResult HandleOrder(ConfirmHandelOrder confirmHandelOrder)
		{
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					if (!new UserRepository(connection).IsPasswordCorrect(User.UserId, confirmHandelOrder.Password))
					{
						TempData["Error"] = new ResourceString("Approver.Login.ErrorPasswordEmptry");
						return View("ConfirmHandelOrder", confirmHandelOrder);
					}

					//กรณีที่กดปุ่ม ApproveOrder ให้ไปเช็คว่าเคยทำการ Approve ไปแล้วหรือไม่
					OrderRepository orderRepo = new OrderRepository(connection);
					string orderId = orderRepo.GetOrderIdByGuid(confirmHandelOrder.OrderGuid);

					if (orderRepo.IsApproveOrder(orderId, Order.OrderStatus.Approved))
					{
						return ViewOrderForApprover(confirmHandelOrder.OrderGuid);
					}
				}
				if (string.IsNullOrEmpty(confirmHandelOrder.Remark)) { confirmHandelOrder.Remark = ""; }
				bool isMultiOrder = false;
				switch (confirmHandelOrder.InputActionName)
				{
					case "PartialApprove":
						return PartialApproveOrder(confirmHandelOrder.OrderGuid, confirmHandelOrder.Remark, isMultiOrder);
					case "ApproveOrder":
						return ApprovedOrder(confirmHandelOrder.OrderGuid, confirmHandelOrder.Remark, isMultiOrder);
					case "DeleteOrder":
						return DeleteOrder(confirmHandelOrder.OrderGuid, confirmHandelOrder.Remark, isMultiOrder);
					case "ReviseOrder":
						return ReviseOrder(confirmHandelOrder.OrderGuid, confirmHandelOrder.Remark, isMultiOrder);
					case "AdminAllow":
						return AdminPartialApproveOrder(confirmHandelOrder.OrderGuid, confirmHandelOrder.Remark, isMultiOrder);
					default:
						return ViewOrderForApprover(confirmHandelOrder.OrderGuid);
				}
			}
			return View("ConfirmHandelOrder", confirmHandelOrder);
		}

		public ActionResult ApprovedOrder(string orderGuid, string remark, bool isMultiOrder)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				if (User.CurrentCompanyId == "OFMNJ")//สำหรับองค์กร OFMNJ สั่งของที่คลังหนองจอก 
				{
					orderRePo.ApproveOrderOFM(orderGuid, User.UserId, remark);//set DownladStatus,IsDownload เป็น No
				}
				else
				{
					orderRePo.ApproveOrder(orderGuid, User.UserId, remark);
				}

				orderData.Order = orderRePo.GetMyOrder(orderId);
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)//หากมีการคุม budget จะต้องเอายอดใบสั่งซื้อไปลบออกจาก budget
				{
					BudgetRepository repo = new BudgetRepository(connection);
					orderData.CurrentBudget = repo.GetBudgetByCostcenterID(orderData.Order.CostCenter.CostCenterID, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					repo.UpdateBudgetAfterApproveOrder(orderData.CurrentBudget, orderData.Order.OrderID, orderData.Order.GrandTotalAmt, User.UserId);
				}
				if (User.Company.UseGoodReceive)
				{
					orderRePo.CreateGoodReceive(orderData.Order.OrderID, User.UserId);//Insert ข้อมูลลง GoodReceive
				}
				orderData.CurrentOrderActivity = SetActivityForApprove(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreateApprovedOrderTransactionLog(orderData);
				if (User.CurrentCompanyId == "OFMNJ")
				{
					SendMail(new Eprocurement2012.Controllers.Mails.ApproveOrderMailOFM(ControllerContext, orderData), orderId, MailType.ApproveOrder, User.UserId);
				}
				else
				{
					SendMail(new Eprocurement2012.Controllers.Mails.ApproveOrderMail(ControllerContext, orderData), orderId, MailType.ApproveOrder, User.UserId);
				}
				//--------------ส่ง SMS หากมีการเลือกรับ SMS แจ้งให้ผู้สั่งซื้อรับทราบ-----------------------------
				if (orderData.Order.UseSMSFeature)
				{
					if (orderData.Order.Requester.UserId != null)
					{
						UserRepository repo = new UserRepository(connection);
						Contact requester = repo.GetUserDataContact(orderData.Order.Requester.UserId, orderData.Order.Company.CompanyId);
						if (requester.Mobile != null)
						{
							if (requester.Mobile.PhoneNo.Length == 10)
							{
								SendSMS_OFM02(orderData.Order.OrderID, orderData.Order.CostCenter.CostCenterCustID, orderData.Order.Requester.UserThaiName, requester);
							}
						}
					}
				}
			}
			if (isMultiOrder)
			{
				return RedirectToAction("GetDataOrderApproveProcess", "Order");
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}

		public ActionResult PartialApproveOrder(string orderGuid, string remark, bool isMultiOrder)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid); //หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);
				orderRePo.PartialApproveOrder(orderGuid, orderData.Order.Requester.UserId, User.UserId, orderData.Order.Company.CompanyId, orderData.Order.CostCenter.CostCenterID, orderData.Order.ParkDay, remark);
				orderData.Order = orderRePo.GetMyOrder(orderId); // ดึงค่า Order ใหม่หลังจาก Status เปลี่ยนแล้ว
				orderData.CurrentOrderActivity = SetActivityForPartialApprove(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreatePartialApproveOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.ApproveAndForwardMail(ControllerContext, orderData), orderId, MailType.ApproveAndForwardOrder, User.UserId);
				//--------------ส่ง SMS หากมีการเลือกรับ SMS แจ้งให้ผู้สั่งซื้อรับทราบ-----------------------------
				if (orderData.Order.UseSMSFeature)
				{
					if (orderData.Order.Requester.UserId != null)
					{
						UserRepository repo = new UserRepository(connection);
						Contact requester = repo.GetUserDataContact(orderData.Order.Requester.UserId, orderData.Order.Company.CompanyId);
						if (requester.Mobile != null)
						{
							if (requester.Mobile.PhoneNo.Length == 10)
							{
								SendSMS_OFM02(orderData.Order.OrderID, orderData.Order.CostCenter.CostCenterCustID, orderData.Order.Requester.UserThaiName, requester);
							}
						}
					}
				}
			}

			if (isMultiOrder)
			{
				return RedirectToAction("GetDataOrderApproveProcess", "Order");
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}

		public ActionResult AdminPartialApproveOrder(string orderGuid, string remark, bool isMultiOrder)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid); //หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);
				orderRePo.AdminPartialApproveOrder(orderGuid, orderData.Order.Requester.UserId, orderData.Order.NextApprover.Approver.UserId, User.UserId, orderData.Order.Company.CompanyId, orderData.Order.CostCenter.CostCenterID, orderData.Order.ParkDay, remark);
				orderData.Order = orderRePo.GetMyOrder(orderId); // ดึงค่า Order ใหม่หลังจาก Status เปลี่ยนแล้ว
				orderData.CurrentOrderActivity = SetActivityForAdminPartialApprove(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreateAdminPartialApproveOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.AdminAllowBudgetToRequesterMail(ControllerContext, orderData), orderId, MailType.AdminAllowBudgetToRequester, User.UserId);
				SendMail(new Eprocurement2012.Controllers.Mails.AdminAllowBudgetToApproveMail(ControllerContext, orderData), orderId, MailType.AdminAllowBudgetToApprover, User.UserId);
				//--------------ส่ง SMS หากมีการเลือกรับ SMS แจ้งให้ผู้สั่งซื้อรับทราบ-----------------------------
				if (orderData.Order.UseSMSFeature)
				{
					if (orderData.Order.Requester.UserId != null)
					{
						UserRepository repo = new UserRepository(connection);
						Contact requester = repo.GetUserDataContact(orderData.Order.Requester.UserId, orderData.Order.Company.CompanyId);
						if (requester.Mobile != null)
						{
							if (requester.Mobile.PhoneNo.Length == 10)
							{
								SendSMS_OFM02(orderData.Order.OrderID, orderData.Order.CostCenter.CostCenterCustID, orderData.Order.Requester.UserThaiName, requester);
							}
						}
					}
				}
				if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)//หากใช้ campany catalog และแสดง ofm catalog จะต้องเช็คว่า สินค้าใดมาจาก ofm catalog บ้าง
				{
					IEnumerable<string> listId = new ProductRepository(connection, User).IsListPidInProductCatalog();
					foreach (var prod in orderData.Order.ItemOrders)
					{
						if (!listId.Any(p => p == prod.Product.Id))
						{
							return RedirectToAction("AddProductFromOrder", new { guid = orderGuid });
						}
					}
				}
			}
			if (isMultiOrder)
			{
				return RedirectToAction("GetDataOrderApproveProcess", "Order");
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}
		private void SendSMS_OFM02(string orderId, string custId, string userName, Contact contact)
		{
			string message = string.Format("ใบสั่งซื้อเลขที่ {0} อนุมัติเรียบร้อยแล้วค่ะ", orderId);
			SendSMSTemplate(orderId, custId, userName, contact, message, "SMS_E-Pro02:ยืนยันการอนุมัติ");
		}

		private void SendSMSTemplate(string orderId, string custId, string userName, Contact contact, string message, string templateName)
		{
			SendSmsOrder(contact.Mobile.PhoneNo, message);
			var smsLog = new OfficeMate.Framework.Data.SmsLog();
			smsLog.SMSTemplate = templateName;
			smsLog.SMSTopic = message;
			smsLog.SenderName = "E-Pro";
			smsLog.CustId = custId;
			smsLog.ContactName = userName;
			smsLog.ContactMobileNo = contact.Mobile.PhoneNo;
			smsLog.WebOrderId = orderId;
			smsLog.InvNo = "";
			smsLog.CreateBy = "System";
			using (SqlConnection connectionMaster = new SqlConnection(ConnectionString))
			{
				new OfficeMate.Framework.Data.Repositories.SmsLogRepository(connectionMaster).InsertLog(smsLog);
			}
		}
		public ActionResult DeleteOrder(string orderGuid, string remark, bool isMultiOrder)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderRePo.DeleteOrder(orderGuid, User.UserId, remark);
				orderData.Order = orderRePo.GetMyOrder(orderId);
				orderData.CurrentOrderActivity = SetActivityForDelete(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreateDeleteOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.ApproverDeleteOrderMail(ControllerContext, orderData), orderId, MailType.ApproverDeleteOrder, User.UserId);
			}
			if (isMultiOrder)
			{
				return RedirectToAction("GetDataOrderApproveProcess", "Order");
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}

		public ActionResult ReviseOrder(string orderGuid, string remark, bool isMultiOrder)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderRePo.ReviseOrder(orderGuid, User.UserId, remark); // Update Order เป็น Status Revise
				orderData.Order = orderRePo.GetMyOrder(orderId); // ดึงค่า Order ใหม่หลังจาก Status เปลี่ยนแล้ว
				orderData.CurrentOrderActivity = SetActivityForRevise(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreateReviseOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.ApproverSentReviseOrderMail(ControllerContext, orderData), orderId, MailType.ApproverSentReviseOrder, User.UserId);
			}
			if (isMultiOrder)
			{
				return RedirectToAction("GetDataOrderApproveProcess", "Order");
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}


		//สร้างสำหรับ Approve Order โดยไม่ต้องเข้าไปทำทีละใบ
		public ActionResult HandleDataOrder(ImageInputData action, string orderGuid, string remark)
		{
			if (string.IsNullOrEmpty(remark)) { remark = ""; }
			bool isMultiOrder = true;
			switch (action.Name)
			{
				case "PartialApprove":
					return PartialApproveOrder(orderGuid, remark, isMultiOrder);
				case "ApproveOrder":
					return ApprovedOrder(orderGuid, remark, isMultiOrder);
				case "DeleteOrder":
					return DeleteOrder(orderGuid, remark, isMultiOrder);
				case "ReviseOrder":
					return ReviseOrder(orderGuid, remark, isMultiOrder);
				case "AdminAllow":
					return AdminPartialApproveOrder(orderGuid, remark, isMultiOrder);
				default:
					return RedirectToAction("GetDataOrderApproveProcess", "Order");
			}
		}

		[HttpGet]
		public ActionResult AddProductFromOrder(string guid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository prodRepo = new ProductRepository(connection, User);
				RePurchase order = new RePurchase();
				List<Product> product = new List<Product>();
				order.OrderId = new OrderRepository(connection).GetOrderIdByGuid(guid);
				IEnumerable<string> productId = new OrderRepository(connection).GetMyOrder(order.OrderId).ItemOrders.Select(p => p.Product.Id).ToList();
				IEnumerable<string> listId = new ProductRepository(connection, User).IsListPidInProductCatalog();
				foreach (var id in productId)
				{
					if (!listId.Any(p => p == id))
					{
						product.Add(prodRepo.GetProductDetail(id));
					}
				}
				order.Products = product;
				order.OrderGuid = guid;
				return View("AddProductFromOrder", order);
			}
		}

		[HttpPost]
		public ActionResult AddProductFromOrder(string[] productId, string guid, ImageInputData action)
		{
			if (action.Name == "AddToCatalog" && productId != null && productId.Any())
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					ProductCatalogRepository catalogRepo = new ProductCatalogRepository(connection);
					foreach (var Id in productId)
					{
						if (!catalogRepo.HaveProductInYourCompanyList(Id, User.CurrentCompanyId))
						{
							catalogRepo.AddProductCatalog(Id, User.CurrentCompanyId, DateTime.Now, DateTime.Now.AddYears(50), User.UserId);
						}
					}
				}
			}
			return RedirectToAction("ViewOrderForApprover", new { id = guid });
		}

		public ActionResult ExpandBudget(string costid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				BudgetRepository repo = new BudgetRepository(connection);
				Budget budget = new Budget();
				budget = repo.GetBudgetByCostcenterID(costid, User.Company.CompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
				repo.GetBudgetDatailForApprover(budget, User.UserId);
				return View(budget);
			}
		}

		private OrderActivity SetActivityForApprove(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อผ่านการอนุมัติ";
			orderActivity.ActivityEName = "Approved Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการอนุมัติใบสั่งซื้อหมายเลข: " + order.OrderID;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityForPartialApprove(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อผ่านการอนุมัติแล้วเบื้องต้น";
			orderActivity.ActivityEName = "Partial Approved Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการอนุมัติใบสั่งซื้อหมายเลข: " + order.OrderID + " แล้วเบื้องต้น";
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityForAdminPartialApprove(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อผ่านการอนุมัติเบื้องต้นจากผู้ดูแลระบบแล้ว";
			orderActivity.ActivityEName = "Admin Partial Approved Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการอนุมัติใบสั่งซื้อหมายเลข: " + order.OrderID + " เบื้องต้นแล้ว";
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityForRevise(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อถุกส่งกลับไปแก้ไข";
			orderActivity.ActivityEName = "Revise Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการส่งใบสั่งซื้อหมายเลข: " + order.OrderID + " กลับไปแก้ไข";
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityForDelete(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อถูกยกเลิก";
			orderActivity.ActivityEName = "Deleted Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการยกเลิกใบสั่งซื้อหมายเลข: " + order.OrderID;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		public ActionResult ViewOrderAdjustment(string Id)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				string orderId = orderRePo.GetOrderIdByGuid(Id);
				OrderActivityTransLog orderTrans = new OrderActivityTransLog();
				orderTrans.OrderTransLogs = orderRePo.GetOrderTransLog(orderId);
				orderTrans.OrderDetailTransLogs = orderRePo.GetOrderDetailTransLog(orderId);
				return View(orderTrans);
			}
		}
	}
}
