﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
using System.Data.SqlClient;
using Eprocurement2012.Controllers.Helpers;
using System.Text.RegularExpressions;

namespace Eprocurement2012.Controllers
{
	[HandleError]
	public class HomeController : AuthenticateRoleController
	{
		public ActionResult Index()
		{
			if (!Boolean.Parse(GetSetting("IsHomeIndexEnabled"))) { return Offline(); }
			if (!User.Company.IsSiteActive) { return RedirectToAction("GoToSiteUnAvilablePage", "Account"); }
			if (IsFirstVisitOfTheDay && DateTime.Parse(GetSetting("SplashPageFrom")) <= DateTime.Today && DateTime.Today <= DateTime.Parse(GetSetting("SplashPageTo"))) { return View("SplashPage"); }
			HomePageData hp = new HomePageData();
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository repoProduct = new ProductRepository(connection, User);

				hp.SiteDirectory = new DepartmentRepository(connection, User).GetChildrenAndGrandChildrenDepartments(0);

				IEnumerable<string> ProductHistoryList = ProductHistoryProvider.GetExceptCurrent(null);
				hp.ProductHistory = repoProduct.GetProducts(ProductHistoryList);

				NewsRepository newsRepo = new NewsRepository(connection);
				hp.CompanyNews = newsRepo.GetAllNewsCompany(User.CurrentCompanyId).Where(o => o.NewsStatus == "Active").Take(2);
				foreach (var item in hp.CompanyNews)
				{
					item.NewsDetail = Regex.Replace(item.NewsDetail, @"<.*?>", String.Empty);
				}

				hp.OFMNews = newsRepo.GetAllOFMNews().Where(o => o.NewsStatus == "Active").Take(2);
				foreach (var item in hp.OFMNews)
				{
					item.NewsDetail = Regex.Replace(item.NewsDetail, @"<.*?>", String.Empty);
				}

				hp.ItemOrderProcess = CountProcess();
				hp.SearchOrder = new SearchOrder();
				hp.ItemOrderAllStatus = new OrderRepository(connection).GetOrderAllStatus(User.CurrentCompanyId, User.UserId);
				hp.IsGoodReceivePeriod = new OrderRepository(connection).GetGoodReceivePeriod(User.Company.GoodReceivePeriod, User.CurrentCompanyId, User.UserId);

				UserRepository userRepo = new UserRepository(connection);
				hp.RootAdmin = userRepo.GetAllUserAdmin(User.CurrentCompanyId).Where(u => u.UserRoleName == Models.User.UserRole.RootAdmin).Single();
				hp.RootAdmin.UserContact = userRepo.GetUserDataContact(hp.RootAdmin.UserId, hp.RootAdmin.CurrentCompanyId);
			}
			return View(hp);
		}

		public CountOrderProcess CountProcess()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository repoOrder = new OrderRepository(connection);
				CountOrderProcess countOrderProcess = new CountOrderProcess();
				countOrderProcess.CountOrderWaiting = repoOrder.GetCountOrderOnProcess(User.UserId, User.CurrentCompanyId);
				countOrderProcess.CountOrderRevise = repoOrder.GetCountOrderRevise(User.UserId, User.CurrentCompanyId);
				countOrderProcess.CountOrderApprove = repoOrder.GetCountOrderWaitingApproved(User.UserId, User.CurrentCompanyId);
				return countOrderProcess;
			}
		}

		[AcceptBothHttpHttps]
		public ActionResult PatialMenu(bool? isSmall)
		{
			IEnumerable<Department> Dept;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository repoProduct = new ProductRepository(connection, User);
				Dept = new DepartmentRepository(connection, User).GetChildrenAndGrandChildrenDepartments(0);
			}
			return isSmall.HasValue && isSmall.Value ? PartialView("SmallMenu", Dept) : PartialView("LeftMenu", Dept);
		}

		[HttpGet]
		public ActionResult ViewContactUs()
		{
			ContactUs contactUs = new ContactUs();
			contactUs.CompanyId = User.CurrentCompanyId;
			contactUs.CompanyName = User.Company.CompanyName;
			contactUs.UserName = User.DisplayName;
			contactUs.UserID = User.UserId;
			contactUs.PhoneNo = User.TelephoneNumber;
			return View("ContactUs", contactUs);
		}

		[HttpPost]
		public ActionResult ViewContactUs(ContactUs contact)
		{
			if (ModelState.IsValid)
			{
				SendMail(new Eprocurement2012.Controllers.Mails.ContactUsMail(ControllerContext, contact), "", MailType.ContactUs, User.UserId);
				return ContactUsComplete();
			}
			return View("ContactUs", contact);
		}

		private ActionResult ContactUsComplete()
		{
			TempData["SendmailContactMessage"] = "ดำเนินการส่งเมล์เรียบร้อยแล้วค่ะ";
			return RedirectToAction("ViewContactUs");
		}

		[HttpGet]
		public ActionResult ViewContactUsSpecialProduct()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepos = new UserRepository(connection);
				ContactUs contactUs = new ContactUs();
				contactUs.CompanyId = User.CurrentCompanyId;
				contactUs.CompanyName = User.Company.CompanyName;
				contactUs.UserName = User.DisplayName;
				contactUs.UserID = User.UserId;
				contactUs.PhoneNo = User.TelephoneNumber;
				contactUs.ListMyAdmin = userRepos.GetAllUserAdmin(contactUs.CompanyId);
				contactUs.ContactTitle = "ขอรายการสินค้าพิเศษ";
				return View("ContactUsSpecialProduct", contactUs);
			}

		}

		[HttpPost]
		public ActionResult ViewContactUsSpecialProduct(ContactUs contact)
		{
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					UserRepository userRepos = new UserRepository(connection);
					contact.ListMyAdmin = userRepos.GetAllUserAdmin(contact.CompanyId);
				}
				SendMail(new Eprocurement2012.Controllers.Mails.ContactUsSpecialProductMail(ControllerContext, contact), "", MailType.RequestSpecialProduct, User.UserId);
				return ContactUsComplete();
			}
			return View("ContactUsSpecialProduct", contact);
		}

		private ActionResult ContactUsSpecialProductComplete()
		{
			TempData["SendmailContactMessage"] = new ResourceString("Message.SendmailContact");
			return RedirectToAction("ViewContactUsSpecialProduct");
		}

		[HttpGet]
		public ActionResult ViewContactUsAdmin()
		{
			ContactUs contactUs = new ContactUs();
			contactUs.CompanyId = User.CurrentCompanyId;
			contactUs.CompanyName = User.Company.CompanyName;
			contactUs.UserName = User.DisplayName;
			contactUs.UserID = User.UserId;
			contactUs.PhoneNo = User.TelephoneNumber;
			return View("ContactUsAdmin", contactUs);
		}

		[HttpPost]
		public ActionResult ViewContactUsAdmin(ContactUs contact)
		{
			if (ModelState.IsValid)
			{
				SendMail(new Eprocurement2012.Controllers.Mails.ContactUsMail(ControllerContext, contact), "", MailType.ContactUs, User.UserId);
				TempData["SendmailContactMessage"] = new ResourceString("Message.SendmailContact");
			}
			return View("ContactUsAdmin", contact);
		}

		[HttpGet]
		public ActionResult CompanyLogoImage(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ImageData companyLogo = new CompanyRepository(connection).GetCompanyLogo(companyId);
				if (companyLogo.Bytes.All(b => b == 0))
				{
					return new FilePathResult("/images/logocompany.jpg", "image/gif");
				}
				else
				{
					return new FileContentResult(companyLogo.Bytes, "image/png");
				}
			}
		}

		public ActionResult NewsCompany(string newsType)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				NewsRepository newsRepo = new NewsRepository(connection);
				IEnumerable<News> itemNews = null;
				if (newsType == "Private")
				{
					itemNews = newsRepo.GetAllNewsCompany(User.CurrentCompanyId);
				}
				else
				{
					itemNews = newsRepo.GetAllOFMNews();
				}
				return View(new MasterPageData<IEnumerable<News>>(itemNews));
			}
		}

		public ActionResult NewsCompanyDetail(string newsGuid, string newsType)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				NewsRepository newsRepo = new NewsRepository(connection);
				News news = new News();
				if (newsType == "Private")
				{
					news = newsRepo.GetNewsDetail(newsGuid, User.CurrentCompanyId);
				}
				else
				{
					news = newsRepo.GetNewsDetail(newsGuid, "OfficeMate");
				}
				if (news == null) { return Index(); }
				return View(news);
			}
		}
	}
}
