﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Controllers.Helpers;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.Hosting;
using CrystalDecisions.Shared;
using System.Web.Configuration;
using System.IO;

namespace Eprocurement2012.Controllers
{

	[HandleError]
	public class AccountController : AbstractEproController
	{

		public ActionResult Logout()
		{
			cacheKeyLastActive = string.Format(cacheKeyLastActive, User.UserGuid);
			System.Web.Hosting.HostingEnvironment.Cache.Remove(cacheKeyLastActive);
			LoginProvider.Logout();
			return RedirectToAction("LogIn");
		}

		[HttpGet]
		public ActionResult LogIn()
		{
			if (!Boolean.Parse(GetSetting("IsHomeIndexEnabled"))) { return Offline(); }
			if (User.IsLogin) { return CheckRoleForGoToPage(); }
			return View("LogInCenter", new LogIn());
		}

		[HttpPost]
		public ActionResult LogIn(LogIn model, string redirectUrl)
		{
			if (!ModelState.IsValid) { return View("LogInCenter", model); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);
				string userGuid;
				bool haveDefaultCompany;
				if (repo.TryGetUserGuid(model.UserId, model.Password, out userGuid, out haveDefaultCompany))
				{
					if (haveDefaultCompany)
					{
						if (repo.IsVerfyUser(model.UserId))
						{
							LoginProvider.Login(userGuid, model.Remember);
							repo.UpdateLastActive(model.UserId); //อัพเดตวันที่ล่าสุดที่เข้าระบบ
							LoadUser(); //ดึงข้อมูลผู้ใช้งานด้วย userGuid (ต้อง login แล้วเท่านั้น)
							return CheckRoleForGoToPage(); //ตรวจสอบเงื่อนไขการเข้าใช้งาน
						}
						ViewData["userGuid"] = userGuid;
						return View("NotVerifyUser");
					}
					ModelState.AddModelError("UserId", "Shared.LogInCenter.ErrorDefaultCompany");
					return View("LogInCenter", model);
				}
				ModelState.AddModelError("UserId", "Shared.LogInCenter.Error");
				return View("LogInCenter", model);
			}
		}

		// ส่งเมลล์ Verify User ใหม่อีกครั้ง  link เข้ามจาก View NotVerifyUser
		public ActionResult SendMailVerifyUser(string userGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				string verifyKey, userId;
				userId = userRepo.GetUserIdFromUserGuid(userGuid);
				if (userRepo.TryVerifyUser(userId, out verifyKey))
				{
					ForgotPasswordData forgot = userRepo.GetForgotPasswordData(userId);//ใช้ Model เหมือนกัน
					SendMail(new Eprocurement2012.Controllers.Mails.VerifyUserMail(ControllerContext, forgot), "", MailType.VerifyUser, userId);
					ViewData["Data"] = new ResourceString("Message.CompleteSentMail");
				}
			}
			ViewData["userGuid"] = userGuid;
			return View("NotVerifyUser");
		}

		// ตรวจสอบเงื่อนไขต่างๆในการเข้าใช้งาน
		private ActionResult CheckRoleForGoToPage()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);

				string userGuid = LoginProvider.ReadUserGuid();

				// เคสพิเศษกรณีเป็น OFMAdmin ไป Site OFMAdmin
				if (repo.IsOFMAdmin(userGuid)) { return GoToOFMAdmin(); }

				// 1.ตรวจสอบอายุของรหัสผ่านว่าถึงกำหนดเปลี่ยนรหัสผ่านหรือไม่
				if (repo.IsForceChangePassword(User.UserId)) { return ForceChangePassword(); }

				// 2.ตรวจสอบว่าผู้ใช้งานรายนี้มีชื่ออยู่หลายบริษัทหรือไม่ ถ้าใช่ ไปที่หน้า  SelectCompany
				if (repo.IsUserMultiCompany(User.UserId)) { return GoToSelectCompanyPage(); }

				// 3.ตรวจสอบว่า Site นี้ Admin  กำลังปรับปรุงอยู่หรือไม่ ถ้า IsSiteActive = "No" ไปที่หน้า CloseSite
				if (!new SiteRepository(connection).IsSiteActive(User.CurrentCompanyId)) { return GoToSiteUnAvilablePage(); }

				// 4.นอกเหนื่อจากเงื่อนไข 1-2 ไปที่หน้า Page ตามเงื่อนไข Admin และ User
				return GoToMainPage();
			}
		}

		// ไปสู่หน้า เว็ปไซตืปิดปรับปรุง
		public ActionResult GoToSiteUnAvilablePage()
		{
			if (!User.IsLogin) { return View("LogInCenter", new LogIn()); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				SiteRepository siteRepo = new SiteRepository(connection);
				Site site = new Site();
				site = siteRepo.GetDataActiveSite(User.CurrentCompanyId, Site.OpenSite.No); //ดึงข้อมูลล่าสุดของผู้ปิด/เปิด Site ใน Log
				site.ListUserAdmins = userRepo.GetAllUserAdmin(User.CurrentCompanyId); //หาข้อมูล Admin และ ผู้ช่วย ของ Site นั้น
				for (int i = 0; i < site.ListUserAdmins.ToList().Count(); i++)
				{
					site.ListUserAdmins.ToList()[i].UserContact = userRepo.GetUserDataContact(site.ListUserAdmins.ToList()[i].UserId, site.ListUserAdmins.ToList()[i].CurrentCompanyId); //นำรายชื่อ Admin มาหาข้อมูลการติดต่อของตัวเอง
				}
				return View("SiteUnAvailable", site);
			}
		}

		// ไปหน้าเพจหลัก แยกกันระหว่าง Only Admin หรือ Only OBserve กับ ผู้ใช้งาน Role อื่นๆ
		private ActionResult GoToMainPage()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);
				if (repo.IsOnlyAdmin(User.UserId, User.CurrentCompanyId)) // ตรวจสอบว่า เป็น Only Admin (เกิด 1 Row และ เป็น RootAdmin เท่านั้น)
				{
					return RedirectToAction("GotoSelectSiteAdmin");
				}
				else if (repo.IsOnlyObserve(User.UserId, User.CurrentCompanyId)) // ตรวจสอบว่า เป็น Only OBserve (เกิด 1 Row และ เป็น OBserve เท่านั้น)
				{
					return GoToViewReport();
				}
				return RedirectToAction("Index", "Home");
			}
		}

		// บันทึกข้อมูลรหัสผ่านผู้ใช้ใหม่
		[HttpPost]
		public ActionResult ForceChangePassword(Password password)
		{
			if (!ModelState.IsValid) { return View(password); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				if (userRepo.IsPasswordCorrect(User.UserId, password.OldPassword))
				{
					userRepo.UpdateChangPassword(User.UserId, password.NewPassword);
					return View("ChangedPasswordSuccess");
				}
				else
				{
					ModelState.AddModelError("OldPassword", "Shared.LogInCenter.ErrorOldPasswordNotCorrect");
					return View(password);
				}
			}

		}

		// ไปสู่หน้า เปลี่ยนรหัสผ่าน
		[HttpGet]
		public ActionResult ForceChangePassword()
		{
			return View("ForceChangePassword", new Password());
		}

		// ไปสู่หน้า ปิดไซต์
		public ActionResult GoToCloseSitePage()
		{
			return View("CloseSite");
		}

		// ไปสู่หน้า เปิดไซต์
		public ActionResult GoToOpenSitePage()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);
				if (repo.IsOnlyObserve(User.UserId, User.CurrentCompanyId)) // ตรวจสอบว่า เป็น Only OBserve (เกิด 1 Row และ เป็น OBserve เท่านั้น)
				{
					return Logout();
				}
				if (User.IsObserve && (User.IsApprover || User.IsRequester))
				{
					return RedirectToAction("Index", "Home");
				}
				return View("OpenSite");
			}
		}

		// ไปสู่หน้า OFMAdmin
		public ActionResult GoToOFMAdmin()
		{
			return RedirectToAction("Index", "OFMAdmin");
		}

		// ไปสู่หน้า AdminReport
		public ActionResult GoToViewReport()
		{
			return RedirectToAction("Index", "Report");
		}

		// ไปสู่หน้า เลือกบริษัท กรณี 1 UserID มีหลาย Company
		public ActionResult GoToSelectCompanyPage()
		{
			if (!User.IsLogin) { return View("LogInCenter", new LogIn()); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				IEnumerable<Company> itemAllMyCompany = compRepo.GetAllMyCompany(User.UserId, Company.CompanyStatus.Active); //ดึงข้อมูล บริษัท ของ User (1:1 ,1:N)
				return View("SelectCompany", itemAllMyCompany);
			}
		}

		// ผู้ใช้งานเลือกบริษัทื่เข้าใช้งานและอัพเดตข้อมูล IsDefaultCompany = 'Yes'
		[HttpPost]
		public ActionResult UserSelectCompany(string companyId)
		{
			if (!User.IsLogin) { return View("LogInCenter", new LogIn()); }
			if (string.IsNullOrEmpty(companyId)) { return GoToSelectCompanyPage(); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				compRepo.SetCurrentUserCompany(User.UserId, companyId); // Set DefalutCompany ที่ใช้ล่าสุด IsDefaultCompany = 'Yes'
				LoadUser(); //ดึงข้อมูลผู้ใช้งานด้วย userGuid (ต้อง login แล้วเท่านั้น)
				// 3.ตรวจสอบว่า Site นี้ Admin  กำลังปรับปรุงอยู่หรือไม่ ถ้า IsSiteActive = "No" ไปที่หน้า CloseSite
				if (!new SiteRepository(connection).IsSiteActive(User.CurrentCompanyId)) { return GoToSiteUnAvilablePage(); }
			}
			return GoToMainPage();
		}

		// ไปสู่หน้าเลือกการทำงานของ Admin ระหว่าง ไป ดูรายงาน กับ ไป ตั้งค่าระบบ
		public ActionResult GotoSelectSiteAdmin()
		{
			if (!User.IsLogin) { return View("LogInCenter", new LogIn()); }
			return View("SelectSiteAdmin");
		}

		[HttpGet]
		public ActionResult ForgotPassword()
		{
			return View();
		}

		[HttpPost]
		public ActionResult ForgotPassword(string userId)
		{
			if (string.IsNullOrEmpty(userId))
			{
				TempData["Message"] = new ResourceString("Shared.Register.ErrorEmailEmpty");
				return View();
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				string verifyKey;
				UserRepository repo = new UserRepository(connection);
				if (repo.TryForgotPasswordUser(userId, out verifyKey))
				{
					ForgotPasswordData forgot = repo.GetForgotPasswordData(userId);
					SendMail(new Eprocurement2012.Controllers.Mails.ForgotPasswordMail(ControllerContext, forgot), "", MailType.ForgotPassword, userId);
					return View("ForgotPasswordSuccess");
				}
				else
				{
					return View("ForgotPasswordFail");
				}
			}
		}

		[HttpGet]
		public ActionResult SetPassword(string userGuid, string verifyKey)
		{
			if (String.IsNullOrEmpty(userGuid)) { return RedirectToAction("Index", "Home"); }
			if (String.IsNullOrEmpty(verifyKey)) { return RedirectToAction("Index", "Home"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);
				if (repo.VerifyUserForgotPassword(userGuid, verifyKey))
				{
					var password = new SetPassword();
					password.UserGuid = userGuid;
					password.VerifyKey = verifyKey;
					password.IsFirstPassword = false;
					return View(password);
				}
				else
				{
					return View("SetPasswordFail");
				}
			}
		}

		[HttpGet]
		public ActionResult VerifyUser(string userGuid, string verifyKey)
		{
			if (String.IsNullOrEmpty(userGuid)) { return RedirectToAction("Index", "Home"); }
			if (String.IsNullOrEmpty(verifyKey)) { return RedirectToAction("Index", "Home"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);
				if (repo.VerifyUser(userGuid, verifyKey))
				{
					var password = new SetPassword();
					password.UserGuid = userGuid;
					password.VerifyKey = verifyKey;
					password.IsFirstPassword = true;
					return View("VerifyUserSuccess", password);
				}
				else
				{
					return View("VerifyUserFail");
				}
			}
		}

		[HttpPost]
		public ActionResult SetPassword(SetPassword password)
		{
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					UserRepository repo = new UserRepository(connection);
					if (repo.SetPasswordAuthentication(password.UserGuid, password.NewPassword.Trim(), password.VerifyKey, password.IsFirstPassword))
					{
						return View("SetPasswordSuccess");
					}
					else
					{
						return View("SetPasswordFail");
					}
				}
			}
			return View(password);
		}

		[HttpGet]
		public ActionResult ApplyNow()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);
				ApplyNow applyNow = new ApplyNow();
				applyNow.ListBusinessType = repo.GetBusinessDetail();
				return View(applyNow);
			}
		}

		[HttpPost]
		public ActionResult ApplyNow(ApplyNow applyNow)
		{

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);
				applyNow.ListBusinessType = repo.GetBusinessDetail();

				if (Session["Captcha"] == null || Session["Captcha"].ToString() != applyNow.Captcha)
				{
					ModelState.AddModelError("Captcha", "Wrong value, please try again.");
					return View(applyNow);
				}
				if (ModelState.IsValid)
				{
					SendMail(new Eprocurement2012.Controllers.Mails.ApplyNowMail(ControllerContext, applyNow), "", MailType.ApplyNow, User.UserId);
					return View("ApplyNowSuccess");
				}
				return View(applyNow);
			}
		}

		public ActionResult ViewOrderForApprovalFromMail(string Id) //เข้ามาจาก link ใน CreateOrderMail
		{
			if (Id == null) { throw new ArgumentNullException("OrderGuid"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				LoadUser();
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(Id);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				if (!string.IsNullOrEmpty(orderId))
				{
					orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
					if (orderData.Order.CurrentApprover != null)
					{
						if (User.IsLogin && orderData.Order.Company.CompanyId == User.CurrentCompanyId && orderData.Order.CurrentApprover.Approver.UserId == User.UserId) { return RedirectToAction("ViewOrderForApprover", "ApproveOrder", new { Id = Id }); }
					}
					orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
					orderData.Order.User = User; //ใส่ค่า User เพื่อไว้เช็คใน Partial
					return View("ViewOrderForApprovalFromMail", orderData);
				}
				ViewData["Error"] = new ResourceString("ErrorMessage.OrderNotFound");
				return View("ErrorOrderNotFound");
			}
		}

		[HttpPost]
		public ActionResult LogInForApprovalOrder(string UserId, string Password, string id, bool IsGoodReceive)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository repo = new UserRepository(connection);
				string userGuid;
				bool haveDefaultCompany;
				if (string.IsNullOrEmpty(UserId) || string.IsNullOrEmpty(Password))
				{
					TempData["Error"] = new ResourceString("Approver.Login.ErrorUserNameAndPasswordEmptry");
				}
				else if (repo.TryGetUserGuid(UserId, Password, out userGuid, out haveDefaultCompany))
				{
					LoginProvider.Login(userGuid, true);
					repo.UpdateIsDefaultCompanyUser(UserId, id);//Update ปรับ Defult Company  ตาม Order ที่เข้ามา
					LoadUser(); //ดึงข้อมูลผู้ใช้งานด้วย userGuid (ต้อง login แล้วเท่านั้น)
					if (User == null)
					{
						return RedirectToAction("Login", "Account");
					}
					repo.UpdateLastActive(User.UserId); //อัพเดตวันที่ล่าสุดที่เข้าระบบ
					if (IsGoodReceive && User.IsRequester && User.Company.UseGoodReceive)//เช็คเพิ่มสำหรับลูกค้ากด ยืนยันการรับสินค้า แต่องค์กรใช้ GoodReceive ให้แสดง ViewOrder แทนการ Update OrderStatus=Complete
					{
						return RedirectToAction("ViewOrderDetail", "Order", new { Id = id });
					}
					if (User.IsAdmin && !User.IsApprover)
					{
						return RedirectToAction("ViewOrderForApprover", "Admin", new { Id = id });
					}
					return RedirectToAction("ViewOrderForApprover", "ApproveOrder", new { Id = id });
				}
				return ViewOrderForApprovalFromMail(id);
			}
		}

		public ActionResult ViewOrderFromOasys(string Id) //เข้ามาจาก link ใน CreateOrderMail
		{
			if (Id == null) { throw new ArgumentNullException("OrderGuid"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				LoadUser();
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(Id);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				if (!string.IsNullOrEmpty(orderId))
				{
					orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
					orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
					orderData.Order.User = User; //ใส่ค่า User เพื่อไว้เช็คใน Partial
					return View("ViewOrderFromOasys", orderData);
				}
				ViewData["Error"] = new ResourceString("ErrorMessage.OrderEproV5");
				return View("ErrorOrderNotFound");
			}
		}

		/*-----------------------Start download attachFile--------------------------*/
		public FilePathResult DownloadAttachFile(string systemFileName)
		{
			string path = Path.Combine(Server.MapPath("~/AttachFile"), systemFileName);
			string contentType = System.IO.Path.GetExtension(systemFileName);
			return File(path, contentType, systemFileName);
		}

		/*-----------------------Start Print file PDF--------------------------*/
		public ActionResult PrintPo(string orderGuid, string ActionName)
		{
			if (ActionName == "PrintEnPO")
			{
				return PrintEnPO(orderGuid);
			}
			else if (ActionName == "PrintThQU")
			{
				return PrintThQU(orderGuid);
			}
			else if (ActionName == "PrintEnQU")
			{
				return PrintEnQU(orderGuid);
			}
			else
			{
				return PrintThPO(orderGuid);
			}
		}

		private ActionResult PrintEnPO(string orderGuid)
		{
			ReportClass rptH = new ReportClass();
			rptH.FileName = HostingEnvironment.MapPath("~/App_Data/ofmEnPo_COL.rpt");
			rptH.Load();
			SetConnection(rptH, orderGuid);
			return File(rptH.ExportToStream(ExportFormatType.PortableDocFormat), "application/pdf");
		}

		private ActionResult PrintThPO(string orderGuid)
		{
			ReportClass rptH = new ReportClass();
			rptH.FileName = HostingEnvironment.MapPath("~/App_Data/ofmThPo_COL.rpt");
			rptH.Load();
			SetConnection(rptH, orderGuid);
			return File(rptH.ExportToStream(ExportFormatType.PortableDocFormat), "application/pdf");
		}

		private ActionResult PrintThQU(string orderGuid)
		{
			ReportClass rptH = new ReportClass();
			rptH.FileName = HostingEnvironment.MapPath("~/App_Data/ofmThQu_COL.rpt");
			rptH.Load();
			SetConnection(rptH, orderGuid);
			return File(rptH.ExportToStream(ExportFormatType.PortableDocFormat), "application/pdf");
		}

		private ActionResult PrintEnQU(string orderGuid)
		{
			ReportClass rptH = new ReportClass();
			rptH.FileName = HostingEnvironment.MapPath("~/App_Data/ofmEnQu_COL.rpt");
			rptH.Load();
			SetConnection(rptH, orderGuid);
			return File(rptH.ExportToStream(ExportFormatType.PortableDocFormat), "application/pdf");
		}

		private static void SetConnection(ReportDocument report, string id)
		{
			for (var i = 0; i < report.DataSourceConnections.Count; i++)
			{
				if (Boolean.Parse(WebConfigurationManager.AppSettings["IsProduction"]))
				{
					// Production
					report.DataSourceConnections[i].SetConnection("DBLog", "DBStoreSupplyUse", "intra", "intra12");
				}
				else
				{
					//Dev-server
					report.DataSourceConnections[i].SetConnection("Dev-Server", "DBStroeSupplyUse_New", "sa", "ofm@dev");
				}
			}
			List<string> ThisCRParamName = new List<string>();
			List<string> ThisCRParamValue = new List<string>();
			ThisCRParamName.Add("@OrderGUID");
			ThisCRParamValue.Add("'" + id + "'");

			for (var i = 0; i < ThisCRParamName.Count; i++)
			{
				ParameterDiscreteValue pdvParam = new ParameterDiscreteValue();
				ParameterValues pvParam = new ParameterValues();
				pdvParam.Value = ThisCRParamValue[i].ToString();
				pvParam.Add(pdvParam);
				report.DataDefinition.ParameterFields[ThisCRParamName[i]].ApplyCurrentValues(pvParam);
			}
		}
		/*-----------------------End Print file PDF--------------------------*/

		public ActionResult Video()
		{
			return View();
		}

		public FileResult DownloadFile()
		{
			return File("~/App_Data/Manual-eprocurementv5.pdf", "application/pdf");
		}

		public ActionResult CompleteOrder(string guid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRepo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRepo.GetOrderIdByGuid(guid);
				orderData.Order = orderRepo.GetMyOrder(orderId);//ดึงข้อมูลสำหรับเช็คว่าลูกค้าใช้ GoodReceive หรือไม่

				if (orderData.Order.Company.UseGoodReceive)
				{
					return ViewOrderGoodReceive(guid);
				}

				if (!orderRepo.IsCompleteOrder(guid))
				{
					orderRepo.CompleteOrder(guid, IPAddress);
					ViewData["message"] = "ใบสั่งซื้อนี้ได้รับการยืนยันแล้วค่ะ ขอขอบพระคุณที่คุณได้ไว้วางใจเลือกสั่งซื้อสินค้าจากเราค่ะ";
				}
			}
			ViewData["message"] = "ขอขอบพระคุณที่คุณได้ไว้วางใจเลือกสั่งซื้อสินค้าจากเราค่ะ";
			return View("SurveyService");
		}

		public ActionResult UrlNotFound()
		{
			return Http404();
		}

		public ActionResult ViewOrderAdjustment(string Id)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				string orderId = orderRePo.GetOrderIdByGuid(Id);
				OrderActivityTransLog orderTrans = new OrderActivityTransLog();
				orderTrans.OrderTransLogs = orderRePo.GetOrderTransLog(orderId);
				orderTrans.OrderDetailTransLogs = orderRePo.GetOrderDetailTransLog(orderId);
				return View(orderTrans);
			}
		}

		//ฟังก์ชันสำหรับคลิกลิงค์ ยืนยันการรับสินค้า จากเมล์ กรณีลูกค้าใช้ฟีเจอร์ GoodReceive
		public ActionResult ViewOrderGoodReceive(string Id)
		{
			if (Id == null) { throw new ArgumentNullException("OrderGuid"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				LoadUser();
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(Id);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				if (!string.IsNullOrEmpty(orderId))
				{
					orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
					orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
					orderData.Order.User = User; //ใส่ค่า User เพื่อไว้เช็คใน Partial
					return View("ViewOrderGoodReceive", orderData);
				}
				ViewData["Error"] = new ResourceString("ErrorMessage.OrderNotFound");
				return View("ErrorOrderNotFound");
			}
		}

	}
}
