﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;

namespace Eprocurement2012.Controllers
{
	public class DepartmentController : AuthenticateRoleController
	{
		private const int defaultPageSize = 120;

		public ActionResult DepartmentList(ParamDeptData paramDeptData, string pRefId, string bRefId, string cRefId, bool? promotion)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				DepartmentRepository repoDept = new DepartmentRepository(connection, User);
				ProductRepository repoProduct = new ProductRepository(connection, User);
				CatalogRepository catalogRepo = new CatalogRepository(connection,User);
				int parentDeptId;
				Int32.TryParse((paramDeptData.Paths ?? String.Empty).Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries).LastOrDefault(), out parentDeptId);
				paramDeptData.CurrentDept = repoDept.GetDepartment(paramDeptData.Id, parentDeptId);

				if (paramDeptData.CurrentDept == null) { return Http404(); }

				//ใส่ค่า theme
				paramDeptData.Theme = SelectTheme(paramDeptData.CurrentDept, paramDeptData.IsBreadCrumbUp);

				if (bRefId != null)
				{
					//Display Brand Filter
					return ProductBrandFilter(paramDeptData, bRefId, repoDept, repoProduct, catalogRepo);
				}

				if (cRefId != null)
				{
					//Display Property Filter
					return ProductPropertyFilter(paramDeptData, cRefId, repoDept, repoProduct, catalogRepo);
				}

				switch (DisplayMode(paramDeptData.CurrentDept, paramDeptData.IsBreadCrumbUp))
				{
					case DepartmentDisplayType.Department:
						//Dept Contain Dept
						return DisplayDepartmentList(paramDeptData, repoDept, repoProduct);

					case DepartmentDisplayType.Product:
						//Dept Contain Product
						return DisplayProductList(paramDeptData, repoDept, repoProduct, catalogRepo);

					case DepartmentDisplayType.Static:
						//Static Page
						return View();

					default:
						break;
				}
				return RedirectToAction("Index", "Home");
			}
		}

		private ActionResult ProductBrandFilter(ParamDeptData paramDeptData, string brandFilterId, DepartmentRepository repoDept, ProductRepository repoProduct, CatalogRepository catalogRepo)
		{
			ProductListData productData = ProductListIntializeData(paramDeptData, repoDept, repoProduct, catalogRepo);
			productData.BrandFilter = productData.BrandFilter.OrderBy(b => b, new BrandRefIdFirst(brandFilterId)).ToArray();
			productData.bRefId = brandFilterId;
			productData.BreadCrumb.BreadCrumbs = productData.BreadCrumb.BreadCrumbs.Concat(new Department[] { new Department() { Name = repoDept.GetBrandName(brandFilterId) } });

			int currentPageIndex = paramDeptData.Page.HasValue ? paramDeptData.Page.Value : 1;
			string refId = paramDeptData.CurrentDept.RefId;
			int totalItemCount;
			int currentItemIndex = ((currentPageIndex - 1) * defaultPageSize) + 1;
			string sortBy = SortByDept(paramDeptData, productData, ProductType.Sku);
			productData.Products = repoProduct.GetBrandFilter(brandFilterId, refId, currentItemIndex, defaultPageSize, sortBy, out totalItemCount);

			productData.PageNumber = currentPageIndex;
			productData.PageSize = defaultPageSize;
			productData.TotalItemCount = totalItemCount;

			return SelectViewThemeProductListFilter(paramDeptData, productData);
		}

		private ActionResult ProductPropertyFilter(ParamDeptData paramDeptData, string propertyRefId, DepartmentRepository repoDept, ProductRepository repoProduct, CatalogRepository catalogRepo)
		{
			ProductListData productData = ProductListIntializeData(paramDeptData, repoDept, repoProduct, catalogRepo);
			productData.cRefId = propertyRefId;
			productData.BreadCrumb.BreadCrumbs = productData.BreadCrumb.BreadCrumbs.Concat(new Department[] { new Department() { Name = repoDept.GetPropertyName(propertyRefId) } });

			int currentPageIndex = paramDeptData.Page.HasValue ? paramDeptData.Page.Value : 1;
			int totalItemCount;
			int currentItemIndex = ((currentPageIndex - 1) * defaultPageSize) + 1;
			string sortBy = SortByDept(paramDeptData, productData, ProductType.Sku);
			productData.Products = repoProduct.GetProductByProperty(propertyRefId, currentItemIndex, defaultPageSize, sortBy, paramDeptData.CurrentDept.RefId, out totalItemCount);

			productData.PageNumber = currentPageIndex;
			productData.PageSize = defaultPageSize;
			productData.TotalItemCount = totalItemCount;

			return SelectViewThemeProductListFilter(paramDeptData, productData);
		}

		private ActionResult SelectViewThemeProductListFilter(ParamDeptData paramDeptData, ProductListData productData)
		{
			Dictionary<string, List<Product>> result = new Dictionary<string, List<Product>>();
			foreach (var item in productData.Products)
			{
				List<Product> product = new List<Product>();
				product.Add(item);
				result.Add(item.Id, product);
			}

			OfficeSupplyProductListData officeSupplyData = (OfficeSupplyProductListData)productData;
			officeSupplyData.SKUs = result.ToDictionary(kp => kp.Key, kp => (IEnumerable<Product>)kp.Value);
			officeSupplyData.OFMSKUs = result.ToDictionary(kp => kp.Key, kp => (IEnumerable<Product>)kp.Value);
			return View("ProductList.OfficeSupply", productData);
		}

		class BrandRefIdFirst : IComparer<Brand>
		{
			private string _refId;
			public BrandRefIdFirst(string refId)
			{
				_refId = refId;
			}
			public int Compare(Brand x, Brand y)
			{
				if (x.RefId == _refId && y.RefId == _refId)
				{
					return 0;
				}
				else if (x.RefId == _refId)
				{
					return -1;
				}
				else if (y.RefId == _refId)
				{
					return 1;
				}
				return 0;
			}
		}

		public ActionResult SiteDirectory()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				DepartmentRepository repo = new DepartmentRepository(connection, User);
				SiteDirectoryData sd = new SiteDirectoryData();
				sd.Departments = repo.GetChildrenAndGrandChildrenDepartments(0);
				return View("Site.Directory", sd);
			}
		}

		private ActionResult DisplayProductList(ParamDeptData paramDeptData, DepartmentRepository repoDept, ProductRepository repoProduct, CatalogRepository catalogRepo)
		{
			bool IsBreadCrumbUp = paramDeptData.IsBreadCrumbUp.HasValue ? paramDeptData.IsBreadCrumbUp.Value : false;
			ProductListData productData = ProductListIntializeData(paramDeptData, repoDept, repoProduct, catalogRepo);
			int currentPageIndex = paramDeptData.Page.HasValue ? paramDeptData.Page.Value : 1;
			int totalItemCount = repoProduct.GetItemCountDeptProductsFilter(paramDeptData.CurrentDept.RefId);
			int currentItemIndex = ((currentPageIndex - 1) * defaultPageSize) + 1;
			string sortBy = SortByDept(paramDeptData, productData, ProductType.Product);

			OfficeSupplyProductListData officeSupplyData = (OfficeSupplyProductListData)productData;

			if (!User.Company.UseOfmCatalog)
			{
				productData.Products = repoProduct.GetDeptProductFilter(paramDeptData.CurrentDept.RefId, currentItemIndex, defaultPageSize, sortBy);
				if (productData.Products.Any())
				{
					officeSupplyData.SKUs = repoProduct.GetProductSKUs(productData.Products);
				}

				if (User.Company.ShowOFMCat)
				{
					productData.ProductsOFM = repoProduct.GetAllDeptProductFilter(paramDeptData.CurrentDept.RefId, currentItemIndex, defaultPageSize, sortBy);
					if (productData.ProductsOFM.Any())
					{
						officeSupplyData.OFMSKUs = repoProduct.GetAllProductSKUs(productData.ProductsOFM);
					}
				}
			}
			else
			{
				productData.Products = repoProduct.GetAllDeptProductFilter(paramDeptData.CurrentDept.RefId, currentItemIndex, defaultPageSize, sortBy);
				if (productData.Products.Any())
				{
					officeSupplyData.SKUs = repoProduct.GetAllProductSKUs(productData.Products);
				}
			}

			if (!IsBreadCrumbUp)
			{
				foreach (var item in productData.Products)
				{
					item.Paths = HtmlHelperExtensions.GenerateDepartmentPaths(paramDeptData.Paths, paramDeptData.Id);
				}
			}

			productData.PageNumber = currentPageIndex;
			productData.PageSize = defaultPageSize;
			productData.TotalItemCount = totalItemCount;

			productData.PromotionCount = repoProduct.GetItemCountProductPromotion(paramDeptData.CurrentDept.RefId);

			if (paramDeptData.Theme == DepartmentThemeType.ListView)
			{
				return View("ProductList.ListView", productData);
			}
			return View("ProductList.OfficeSupply", productData);
		}

		private ProductListData ProductListIntializeData(ParamDeptData paramDeptData, DepartmentRepository repoDept, ProductRepository repoProduct, CatalogRepository catalogRepo)
		{
			ProductListData productData;
			switch (paramDeptData.Theme)
			{
				case DepartmentThemeType.OfficeSupply:
					if ((paramDeptData.GroupByDept.HasValue && paramDeptData.GroupByDept.Value) || (paramDeptData.GroupByDeptSupply.HasValue && paramDeptData.GroupByDeptSupply.Value))
					{
						productData = new GroupPropertyProductListData();
					}
					else
					{
						productData = new OfficeSupplyProductListData();
					}
					break;
				case DepartmentThemeType.ListView:
					productData = new OfficeSupplyProductListData();
					break;
				case DepartmentThemeType.GroupProperty:
				case DepartmentThemeType.GroupPropertyOfficeSupply:
					productData = new GroupPropertyProductListData();
					break;
				case DepartmentThemeType.InkToner:
				case DepartmentThemeType.Printing:
				case DepartmentThemeType.BookPreview:
				case DepartmentThemeType.Football:
				case DepartmentThemeType.ThemeA:
				case DepartmentThemeType.ThemeANonBrand:
				case DepartmentThemeType.ThemeB:
				case DepartmentThemeType.ThemeC:
				case DepartmentThemeType.ThemeD:
				default:
					if ((paramDeptData.GroupByDept.HasValue && paramDeptData.GroupByDept.Value) || (paramDeptData.GroupByDeptSupply.HasValue && paramDeptData.GroupByDeptSupply.Value))
					{
						productData = new GroupPropertyProductListData();
					}
					else
					{
						productData = new OfficeSupplyProductListData();
					}
					break;
			}

			productData.SimilarDepartment = repoDept.GetSimilarDepartment(paramDeptData.CurrentDept);
			if (productData.SimilarDepartment.Any())
			{
				productData.ProductsOfRelateDept = repoProduct.GetProductByDepartment(productData.SimilarDepartment);
			}

			paramDeptData.IsBreadCrumbUp = true;//แสดง Product ต้องเป็นเซตให้เป็นขาขึ้น

			if (paramDeptData.SortBy == null)
			{
				productData.SortBy = paramDeptData.CurrentDept.DefaultSortBy;
			}
			else
			{
				try
				{
					productData.SortBy = (DepartmentSortByType)Enum.Parse(typeof(DepartmentSortByType), paramDeptData.SortBy, true);
				}
				catch (ArgumentException)
				{
					productData.SortBy = DepartmentSortByType.New;
				}
			}

			//Intialize Department View
			DepartmentViewIntializeData(productData, paramDeptData, repoDept, repoProduct);

			if (paramDeptData.View == null)
			{
				productData.ProductListViewType = paramDeptData.CurrentDept.ProductListViewType;
			}
			else
			{
				try
				{
					productData.ProductListViewType = (DepartmentViewType)Enum.Parse(typeof(DepartmentViewType), paramDeptData.View, true);
				}
				catch (ArgumentException)
				{
					productData.ProductListViewType = paramDeptData.CurrentDept.ProductListViewType;
				}

				if (productData.ProductListViewType == DepartmentViewType.List)
				{
					paramDeptData.Theme = DepartmentThemeType.ListView;
				}
				else
				{
					paramDeptData.Theme = DepartmentThemeType.OfficeSupply;
				}
			}

			productData.MyCatalogs = catalogRepo.MyCatalogList(User.CurrentCompanyId);//ดึง MyCatalog

			return productData;
		}

		private static string SortByDept(ParamDeptData paramDeptData, ProductListData productData, ProductType productType)
		{
			string sortBy;
			if (paramDeptData.SortByDept.HasValue && paramDeptData.SortByDept == false)
			{
				productData.SortByDept = false;
				sortBy = ProductSortBy(productData.SortBy, productType);
			}
			else if (paramDeptData.SortByDept.HasValue && paramDeptData.SortByDept == true)
			{
				productData.SortByDept = true;
				sortBy = ProductSortBy(productData.SortBy, productType).Insert(9, "DeptId, ");
			}
			else
			{
				//default
				if (paramDeptData.Theme == DepartmentThemeType.OfficeSupply)
				{
					productData.SortByDept = true;
					sortBy = ProductSortBy(productData.SortBy, productType).Insert(9, "DeptId, ");
				}
				else
				{
					productData.SortByDept = false;
					sortBy = ProductSortBy(productData.SortBy, productType);
				}
			}
			return sortBy;
		}

		public static string ProductSortBy(DepartmentSortByType sortBy, ProductType productType)
		{
			//"Order By " ต้องแบบนี้ห้ามเคาะเกินเด็ดขาด มีผลกับโปรแกรม *----*
			if (DepartmentSortByType.New == sortBy)
			{
				return "Order By IsNew Desc, UploadOn Desc";
			}
			else if (DepartmentSortByType.PriceLowToHigh == sortBy)
			{
				return ProductType.Product == productType ? "Order By MinPrice" : "Order By PriceIncVat";
			}
			else if (DepartmentSortByType.PriceHighToLow == sortBy)
			{
				return ProductType.Product == productType ? "Order By MaxPrice Desc" : "Order By PriceIncVat Desc";
			}
			else if (DepartmentSortByType.Promotion == sortBy)
			{
				return "Order By IsPromotion Desc";
			}
			else if (DepartmentSortByType.Brand == sortBy)
			{
				return "Order By BrandId Desc";
			}
			else
			{
				return "Order By IsNew Desc, UploadOn Desc";
			}
		}

		private ActionResult DisplayDepartmentList(ParamDeptData paramDeptData, DepartmentRepository repoDept, ProductRepository repoProduct)
		{
			DepartmentListData deptData = DepartmentListIntializeData(paramDeptData, repoDept, repoProduct);

			deptData.PromotionCount = repoProduct.GetItemCountProductPromotion(paramDeptData.CurrentDept.RefId);

			if (DepartmentThemeType.ThemeANonBrand == paramDeptData.Theme)
			{
				return View("DepartmentList.ThemeANonBrand", deptData);
			}
			return View("DepartmentList.ThemeA", deptData);
		}

		private DepartmentListData DepartmentListIntializeData(ParamDeptData paramDeptData, DepartmentRepository repoDept, ProductRepository repoProduct)
		{
			DepartmentListData deptData = new DepartmentListData();


			DepartmentViewIntializeData(deptData, paramDeptData, repoDept, repoProduct);

			//Department List
			deptData.Departments = repoDept.GetChildrenAndGrandChildrenDepartments(paramDeptData.Id);

			return deptData;
		}

		private void DepartmentViewIntializeData(DepartmentViewData deptView, ParamDeptData paramDeptData, DepartmentRepository repoDept, ProductRepository repoProduct)
		{
			BreadCrumbData BreadCrumb = new BreadCrumbData();
			deptView.CurrentDepartment = paramDeptData.CurrentDept;
			deptView.Paths = paramDeptData.Paths;
			BreadCrumb.IsBreadCrumbUp = paramDeptData.IsBreadCrumbUp.HasValue && paramDeptData.IsBreadCrumbUp.Value;

			//Relate Department
			deptView.RelateDepartment = repoDept.GetRelateDepartment(paramDeptData.CurrentDept);

			//Sibling Department
			deptView.SiblingDepartment = repoDept.GetSiblingDepartment(paramDeptData.CurrentDept);

			//Navigator Department
			deptView.NavigatorDepartment = repoDept.GetNavigatorList(paramDeptData.CurrentDept);

			//Bread Crumbs
			BreadCrumb.BreadCrumbs = repoDept.GetBreadCrumbs(paramDeptData.CurrentDept.Id, paramDeptData.Paths);
			deptView.BreadCrumb = BreadCrumb;

			//Brand Filter
			IEnumerable<Brand> brandList = repoDept.GetBrandFilter(paramDeptData.CurrentDept.Id).OrderBy(b => b, new NonBrandLast()).ToArray();
			foreach (Brand nonBrand in brandList.Where(b => b.BrandName == "NonBrand"))
			{
				nonBrand.BrandName = "Other Brand";
			}
			deptView.BrandFilter = brandList;

			//Promotion
			deptView.ProductPromotion = repoProduct.GetProductPromotion(paramDeptData.CurrentDept.RefId);

			//Property Department
			deptView.PropertyDepartment = repoDept.GetPropertyByDeptId(paramDeptData.CurrentDept.Id);

			//History
			IEnumerable<string> ProductHistoryList = ProductHistoryProvider.GetExceptCurrent(null);
			deptView.ProductHistory = repoProduct.GetProducts(ProductHistoryList);

		}

		private DepartmentDisplayType DisplayMode(Department currentDept, bool? isBreadCrumbUp)
		{
			bool IsBreadCrumbUp = isBreadCrumbUp.HasValue && isBreadCrumbUp.Value;
			return IsBreadCrumbUp ? currentDept.DisplayTypeUp : currentDept.DisplayTypeDown;
		}

		private DepartmentThemeType SelectTheme(Department currentDept, bool? isBreadCrumbUp)
		{
			bool IsBreadCrumbUp = isBreadCrumbUp.HasValue && isBreadCrumbUp.Value;
			return IsBreadCrumbUp ? currentDept.ThemeTypeUp : currentDept.ThemeTypeDown;
		}

		class NonBrandLast : IComparer<Brand>
		{
			public int Compare(Brand x, Brand y)
			{
				if (x.BrandName == "NonBrand" && y.BrandName == "NonBrand")
				{
					return 0;
				}
				else if (x.BrandName == "NonBrand")
				{
					return 1;
				}
				else if (y.BrandName == "NonBrand")
				{
					return -1;
				}
				return 0;
			}
		}

		public class ParamDeptData
		{
			public int Id { get; set; }
			public string View { get; set; }
			public bool? IsBreadCrumbUp { get; set; }
			public DepartmentThemeType Theme { get; set; }
			public Department CurrentDept { get; set; }
			public string Paths { get; set; }
			public int? Page { get; set; }
			public string SortBy { get; set; }
			public bool? SortByDept { get; set; }
			public bool? GroupByDept { get; set; }
			public bool? GroupByDeptSupply { get; set; }
			public string PropName { get; set; }
		}
	}
}
