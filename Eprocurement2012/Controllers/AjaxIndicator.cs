﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers
{
	[ModelBinder(typeof(AjaxIndicator))]
	public struct AjaxIndicator : IModelBinder
	{
		bool _isAjax;
		public AjaxIndicator(bool isAjax)
		{
			_isAjax = isAjax;
		}

		public override string ToString()
		{
			return _isAjax.ToString();
		}

		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			return new AjaxIndicator(controllerContext.HttpContext.Request.IsAjaxRequest());
		}

		public static implicit operator bool(AjaxIndicator value)
		{
			return value._isAjax;
		}

		public static implicit operator AjaxIndicator(bool value)
		{
			return new AjaxIndicator(value);
		}
	}
}
