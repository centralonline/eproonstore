﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers
{
	public class ProductController : AuthenticateRoleController
	{

		public ActionResult Details(string id, string paths)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Product productDetail = new Product();
				ProductRepository productRepo = new ProductRepository(connection, User);
				productDetail = productRepo.GetProductDetail(id);

				if (productDetail == null)
				{
					Response.StatusCode = 404;
					return View("NotFound");
				}

				IEnumerable<Department> breadCrumbsList = new DepartmentRepository(connection, User).GetProductBreadCrumbs(id, paths);
				BreadCrumbData breadCrumb = new BreadCrumbData();
				breadCrumb.IsBreadCrumbUp = true;
				if (breadCrumbsList.Count() > 0)
				{
					Department currentDeptOfProduct = breadCrumbsList.First();
					breadCrumb.BreadCrumbs = breadCrumbsList.Concat(new Department[] { new Department() { Name = productDetail.Name } });
				}

				ProductData viewPageData = new ProductData();

				viewPageData.ProductDetail = productDetail;

				viewPageData.ProductSku = productRepo.GetProductSKUs(productDetail.CodeId);
				if (productDetail.IsPG && !String.IsNullOrEmpty(productDetail.DefaultSku))
				{
					if (viewPageData.ProductSku.Count() == 0)
					{
						Response.StatusCode = 404;
						return View("NotFound");
					}
					Product SingleProductInGroup = new Product();
					if (viewPageData.ProductSku.Any(w => w.Id == viewPageData.ProductDetail.DefaultSku))
					{
						SingleProductInGroup = viewPageData.ProductSku.Where(w => w.Id == viewPageData.ProductDetail.DefaultSku).Single();
					}
					else
					{
						SingleProductInGroup = viewPageData.ProductSku.First();
					}
					return RedirectToAction("Details", new { Id = SingleProductInGroup.Id, title = SingleProductInGroup.Name });
				}

				viewPageData.BreadCrumb = breadCrumb;
				viewPageData.ProductRelated = productRepo.GetProductRelated(productDetail.Id);

				IEnumerable<string> ProductHistoryList = ProductHistoryProvider.GetExceptCurrent(productDetail.Id);
				viewPageData.ProductHistory = productRepo.GetProducts(ProductHistoryList);
				ProductHistoryProvider.Add(id);

				viewPageData.MyCatalogs = new CatalogRepository(connection, User).MyCatalogList(User.CurrentCompanyId);

				return View("Details.Normal", viewPageData);
			}
		}

		[HttpPost]
		public ActionResult HandleBuy(HandleBuyData data, ImageInputData action, AjaxIndicator isAjax)
		{
			switch (action.Name)
			{
				case "AddToCart":
					return AddToCart(data, isAjax);
				case "AddToWishList":
					return AddToWishList(data, isAjax);
				case "NotifyMe":
					return NotifyMe(new NotifyMeData() { ProductID = data.Id }, isAjax);
				default:
					return RedirectToAction("Details", new { Id = data.Id, title = GetProductName(data.Id) });
			}
		}

		[HttpGet]
		private ActionResult NotifyMe(NotifyMeData tempData, AjaxIndicator isAjax)
		{
			string email = string.Empty;
			if (User.IsLogin)
			{
				email = User.UserId;
			}
			NotifyMeData viewData = new NotifyMeData() { NotifyMeEmail = email, NotifyRemark = string.Empty, ProductID = tempData.ProductID };
			if (isAjax)
			{
				return PartialView("NotifyMe", viewData);
			}
			return View("Product.NotifyMe", viewData);
		}

		[HttpPost]
		public ActionResult NotifyMe(NotifyMeData data, ImageInputData action)
		{
			if ((action.Name == "cancel")) { return RedirectToAction("Details", new { id = data.ProductID }); }
			if (string.IsNullOrEmpty(data.NotifyMeEmail)) { return RedirectToAction("Details", new { id = data.ProductID }); }
			if (string.IsNullOrEmpty(data.NotifyRemark)) { data.NotifyRemark = string.Empty; }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository productRepo = new ProductRepository(connection, User);
				productRepo.AddNotifyMe(data.NotifyMeEmail, data.ProductID, data.NotifyRemark, data.Qty > 0 ? data.Qty : 1, data.MobileNo, User.CurrentCompanyId);
				NotifyMeMailData mailData = new NotifyMeMailData() { ProductDetail = productRepo.GetProductDetail(data.ProductID), NotifyEmail = data.NotifyMeEmail, NotifyRemark = data.NotifyRemark };
				SendMailNotify(new Eprocurement2012.Controllers.Mails.NotifyMeMailAlert(ControllerContext, mailData), MailType.NotifyProduct, mailData.NotifyEmail);
			}
			return RedirectToAction("Details", new { Id = data.ProductID });
		}


		[HttpPost]
		public ActionResult QuickInfoProcessStatus(string status)
		{
			TempData["Status"] = status;
			return View("QuickInfoProcessStatus");
		}

		[HttpPost]
		public ActionResult HandleBuyOfficeSupply(HandleBuyOfficeSupplyData data, ImageInputData action, AjaxIndicator isAjax)
		{
			switch (action.Name)
			{
				case "AddToCartOfficeSupply":
					return AddToCartOfficeSupply(data, isAjax);
				case "AddToWishListOfficeSupply":
					return AddToWishListOfficeSupply(data, isAjax);
				default:
					return RedirectToAction("DepartmentList", "Department", new { Id = data.DepartmentId, Title = data.Title, Paths = data.Paths });
			}
		}

		private LocalizedString GetProductName(string id)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository repo = new ProductRepository(connection, User);
				Product product = repo.GetProductDetail(id);
				return product.Name;
			}
		}

		private ActionResult AddToCartOfficeSupply(HandleBuyOfficeSupplyData data, AjaxIndicator isAjax)
		{
			if (data.Quantity == null || data.ProductId == null || data.ProductId.Length != data.Quantity.Length)
			{
				if (isAjax)
				{
					return Json(new
					{
						Result = false,
						Message = "ระบบเกิดความผิดพลาด กรุณาตรวจเช็คข้อมูลและทำการสั่งซื้อใหม่อีกครั้งค่ะ"
					});
				}
				else
				{
					return RedirectToAction("DepartmentList", "Department", new { Id = data.DepartmentId, Title = data.Title, Paths = data.Paths });
				}
			}
			else
			{
				for (int i = 0; i < data.ProductId.Length; i++)
				{
					int qty;
					if (Int32.TryParse(data.Quantity[i], out qty))
					{
						if (qty > 0)
						{
							ShoppingCartProvider.AddToCart(data.ProductId[i], qty);
						}
						else if (qty < 0)
						{
							ShoppingCartProvider.AddToCart(data.ProductId[i], 1);
						}
					}
					else
					{
						//ถ้ากรอกข้อมูลแต่ไม่ใช่ตัวเลขให้ Default=1
						if (!String.IsNullOrEmpty(data.Quantity[i]))
						{
							ShoppingCartProvider.AddToCart(data.ProductId[i], 1);
						}

					}
				}
				if (isAjax)
				{
					Cart cart = ShoppingCartProvider.LoadMyCart();
					return Json(new
					{
						Result = true,
						ItemCount = cart.ItemCountCart,
						Message = "คุณได้ทำการเลือกสินค้าเรียบร้อยแล้วค่ะ"
					});
				}
				else
				{
					return RedirectToAction("CartDetail", "Cart");
				}
			}
		}

		private ActionResult AddToWishListOfficeSupply(HandleBuyOfficeSupplyData data, AjaxIndicator isAjax)
		{
			if (data.WishListProductId != null && data.WishListProductId.Length > 0)
			{
				string pID = string.Join(",", data.WishListProductId);
				return RedirectToAction("AddProductInCatalog", "Catalog", new { pID = pID, catalogGUID = data.catalogGUID });
			}
			else
			{
				if (isAjax)
				{
					return Json(new
					{
						Result = false,
						Message = "ระบบเกิดความผิดพลาด โปรดดำเนินการใหม่อีกครั้งค่ะ"
					});
				}
				else
				{
					return RedirectToAction("DepartmentList", "Department", new { Id = data.DepartmentId, Title = data.Title, Paths = data.Paths });
				}
			}
		}

		[HttpGet]
		public ActionResult QuickInfo(string id)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository repo = new ProductRepository(connection, User);

				Product product = repo.GetProductDetail(id ?? String.Empty);
				if (product == null)
				{
					Response.StatusCode = 404;
					return View("NotFound");
				}

				ProductData viewdata = new ProductData();
				viewdata.ProductDetail = product;
				viewdata.ProductSku = repo.GetProductSKUs(product.CodeId);
				viewdata.MyCatalogs = new CatalogRepository(connection, User).MyCatalogList(User.CurrentCompanyId);
				if (product.IsPG && !String.IsNullOrEmpty(product.DefaultSku))
				{
					if (viewdata.ProductSku.Count() < 1)
					{
						Response.StatusCode = 404;
						return View("NotFound");
					}
					Product SingleProductInGroup = viewdata.ProductSku.Where(w => w.Id == viewdata.ProductDetail.DefaultSku).Single();
					return RedirectToAction("QuickInfo", new { Id = SingleProductInGroup.Id });
				}
				return View("QuickInfo", viewdata);
			}
		}

		public ActionResult Search(string TextSearch)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository productRepo = new ProductRepository(connection, User);
				SearchResultData search = new SearchResultData();
				search.SearchResult = productRepo.GetProductSearchFilter(TextSearch);
				return View("SearchResult", search);
			}
		}

		public ActionResult SearchProduct(string term, int maxRows)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository productrepo = new ProductRepository(connection, User);
				IEnumerable<ProductJsonData> item = productrepo.GetProductSearch(term);
				return Json(item.Take(maxRows), JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult SearchProductSKU(string pId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository productrepo = new ProductRepository(connection, User);
				Product product = productrepo.GetProductDetail(pId);
				ProductJsonData item = null;
				if (product != null && product.Status != ProductStatusType.Hold && product.Status != ProductStatusType.Deleted)
				{
					item = new ProductJsonData();
					item.ProductThaiName = product.ThaiName;
					item.ProductEngName = product.EngName;
					item.ProductThaiUnit = product.ThaiUnit;
					item.ProductEngUnit = product.EngUnit;
					item.ProductName = product.Name;
					item.ProductUnit = product.Unit;
					item.ProductId = product.Id;
				}
				else
				{
					item = new ProductJsonData();
					if (User.UserLanguage == User.Language.TH) { item.ProductName = "ไม่พบข้อมูลสินค้า"; item.ProductUnit = ""; }
					else { item.ProductName = "Not Found Item"; }

				}
				return Json(item, JsonRequestBehavior.AllowGet);
			}
		}

		private ActionResult AddToCart(HandleBuyData data, AjaxIndicator isAjax)
		{
			int Qty = data.Qty > 0 ? data.Qty : 1;
			if ((!String.IsNullOrEmpty(data.ChildVariationPID))) //กรณีเลือกจาก Select Box
			{
				ShoppingCartProvider.AddToCart(data.ChildVariationPID, Qty);
			}
			else if ((!data.IsPG) && (data.Id != null)) //กรณีไม่ได้เลือกจาก Select Box และ Item ปัจจุบันไม่ใช่ Product Group
			{
				ShoppingCartProvider.AddToCart(data.Id, Qty);
			}
			else
			{
				if (isAjax)
				{
					return Json(new
					{
						Result = false,
						Message = "กรุณาเลือกสินค้าที่ต้องการ ก่อนที่จะ คลิกปุ่ม \"สั่งซื้อสินค้า\""
					});
				}
				else
				{
					TempData["SelectItemError"] = "กรุณาเลือกสินค้าที่ต้องการ ก่อนที่จะ คลิกปุ่ม \"สั่งซื้อสินค้า\"";
					return RedirectToAction("Details", new { Id = data.CodeId, title = GetProductName(data.CodeId) });
				}

			}
			if (isAjax)
			{
				return Json(new
				{
					Result = true,
					//ItemCount = cart.ItemCount,
					Message = "คุณได้ทำการเลือกสินค้าเรียบร้อยแล้วค่ะ..!!"
				});
			}
			else
			{
				return RedirectToAction("CartDetail", "Cart");
			}
		}

		private ActionResult AddToWishList(HandleBuyData data, AjaxIndicator isAjax)
		{

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				return RedirectToAction("AddProductInCatalog", "Catalog", new { pID = data.Id, catalogGUID = data.catalogGUID });
			}

			//if (User.IsLogin)
			//{
			//    using (SqlConnection connection = new SqlConnection(ConnectionString))
			//    {
			//        if ((!String.IsNullOrEmpty(data.ChildVariationPID))) //กรณีเลือกจาก Select Box
			//        {
			//            return RedirectToAction("AddProductInCatalog", "Catalog", new { pID = data.ChildVariationPID, catalogGUID = data.catalogGUID });

			//        }
			//        else if ((!data.IsPG) && (data.Id != null)) //กรณีไม่ได้เลือกจาก Select Box และ Item ปัจจุบันไม่ใช่ Product Group
			//        {

			//            return RedirectToAction("AddProductInCatalog", "Catalog", new { pID = data.Id, catalogGUID = data.catalogGUID });

			//        }
			//        else
			//        {
			//            TempData["SelectItemError"] = "กรุณาเลือกสินค้าที่ต้องการ ก่อนที่จะ Add to Catalog";
			//            return RedirectToAction("Details", new { Id = data.CodeId, title = GetSeoName(data.CodeId) });
			//        }
			//    }
			//}
			//else
			//{
			//    return RedirectToAction("Index", "Home");
			//}
		}

		public class HandleBuyData
		{
			public string ChildVariationPID { get; set; }
			public string CodeId { get; set; }
			public string Id { get; set; }
			public bool IsPG { get; set; }
			public int Qty { get; set; }
			public string Question { get; set; }
			public string catalogGUID { get; set; }
		}

		public class HandleBuyOfficeSupplyData
		{
			public string[] ProductId { get; set; }
			public string[] Quantity { get; set; }
			public string[] WishListProductId { get; set; }
			public int DepartmentId { get; set; }
			public string Title { get; set; }
			public string Paths { get; set; }
			public string catalogGUID { get; set; }
			public bool IsContinueCart { get; set; }
		}
	}
}
