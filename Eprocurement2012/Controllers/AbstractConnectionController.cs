﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Configuration;
using Eprocurement2012.Models;

namespace Eprocurement2012.Controllers
{
    public class AbstractConnectionController : Controller
    {
		public new User User { get; set; }
		protected virtual bool IsProduction
		{
			get { return Boolean.Parse(GetSetting("IsProduction")); }
		}
		protected string GetSetting(string settingKey)
		{
			return WebConfigurationManager.AppSettings[settingKey];
		}
		protected virtual string ConnectionString
		{
			get
			{
				if (IsProduction)
				{
					return WebConfigurationManager.ConnectionStrings["ProductionServer"].ConnectionString;
				}
				else
				{
					return WebConfigurationManager.ConnectionStrings["DevelopmentServer"].ConnectionString;
				}
			}
		}
		

    }
}
