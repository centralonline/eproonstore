﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Controllers.Helpers;
using System.Data.SqlClient;
using Eprocurement2012.Models;
using Eprocurement2012.Models.Repositories;


namespace Eprocurement2012.Controllers
{
	public class CatalogController : AuthenticateRoleController
	{
		public ActionResult ManageCatalog(ImageInputData action, Catalog catalog, string guid, string isDefault)
		{
			switch (action.Name)
			{
				case "CreateCatalog":
					return CreateCatalog();
				case "SaveCatalog":
					return SaveCatalog(catalog);
				case "EditCatalog":
					return EditCatalog(guid);
				case "UpdateCatalog":
					return UpdateCatalog(catalog, guid);
				case "DeleteCatalog":
					return DeleteCatalog(guid, isDefault);
				case "AddProductID":
					return AddProductByPID(catalog.AddByPID, guid);

				default:
					return MyCatalog(guid);
			}
		}

		private ActionResult MyCatalog(string guid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CatalogRepository catalogRepo = new CatalogRepository(connection, User);
				CatalogData data = new CatalogData();
				data.ListCatalog = catalogRepo.GetAllMyCatalog(User.CurrentCompanyId);//สำหรับแสดง List Catalog

				if (!data.ListCatalog.Any()) { return CreateCatalog(); }

				if (!string.IsNullOrEmpty(guid))
				{
					data.Catalog = catalogRepo.GetMyCatalog(guid);
				}
				else
				{
					data.Catalog = catalogRepo.GetMyCatalog(data.ListCatalog.FirstOrDefault().GUID);//ดึงข้อมูล Detail มาแสดง
				}
				return View("ManageCatalog", data);
			}
		}

		private ActionResult AddProductByPID(string addByPID, string guid)
		{
			if (string.IsNullOrEmpty(addByPID))
			{
				TempData["ErrorPIDEmpty"] = "กรุณาป้อนรหัสสินค้าค่ะ";
				return RedirectToAction("ManageCatalog", new { guid = guid });
			}
			if (addByPID.Length < 7)
			{
				TempData["ErrorPIDEmpty"] = "รหัสสินค้าไม่ถูกต้องค่ะ";
				return RedirectToAction("ManageCatalog", new { guid = guid });
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository productRepo = new ProductRepository(connection, User);
				CatalogRepository catalogRepo = new CatalogRepository(connection, User);
				string productId = string.Empty;
				if (CheckPidForAddCartByPid(addByPID, out productId))
				{
					catalogRepo.AddProductByPID(guid, productId, User.UserId);//ดึงฟังก์ชัน AddProductByPID มาใช้
				}
				else
				{
					TempData["ErrorPIDEmpty"] = "รหัสสินค้านี้ไม่มีในระบบค่ะ";
				}
				return RedirectToAction("ManageCatalog", new { guid = guid });
			}
		}

		private bool CheckPidForAddCartByPid(string pid, out string productId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				productId = string.Empty;
				return new ProductRepository(connection, User).IsPidInSystem(pid, out productId);
			}
		}


		private ActionResult CreateCatalog()
		{
			return View("CreateMyCatalog", new Catalog());
		}

		private ActionResult SaveCatalog(Catalog catalog)
		{
			if (string.IsNullOrEmpty(catalog.CatalogName))
			{
				TempData["ErrorCatalogNameEmpty"] = "กรุณากรอกชื่อ Catalog ด้วยค่ะ";
				return View("CreateMyCatalog", catalog);
			}
			if (string.IsNullOrEmpty(catalog.CatalogDesc))
			{
				catalog.CatalogDesc = "";
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CatalogRepository catalogRepo = new CatalogRepository(connection, User);
				if (string.IsNullOrEmpty(catalog.IsDefault))
				{
					catalog.IsDefault = "No";
				}
				else
				{
					catalog.IsDefault = "Yes";
				}
				catalogRepo.CreateMyCatalog(catalog);

				return RedirectToAction("ManageCatalog", catalog);
			}
		}

		private ActionResult EditCatalog(string guid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Catalog catalog = new Catalog();
				catalog = new CatalogRepository(connection, User).GetMyCatalog(guid);
				return View("EditMyCatalog", catalog);
			}
		}

		private ActionResult UpdateCatalog(Catalog catalog, string guid)
		{
			if (string.IsNullOrEmpty(catalog.CatalogName))
			{
				TempData["ErrorCatalogNameEmpty"] = "กรุณากรอกชื่อ Catalog ด้วยค่ะ";
				return View("EditMyCatalog", catalog);
			}
			if (string.IsNullOrEmpty(catalog.CatalogDesc))
			{
				catalog.CatalogDesc = "";
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CatalogRepository catalogRepo = new CatalogRepository(connection, User);

				if (catalog.IsDefault == null)
				{
					catalog.IsDefault = "No";
				}
				else
				{
					catalog.IsDefault = "Yes";
				}

				if (catalogRepo.CheckHasIsDefault(guid) && (catalog.IsDefault == "No"))
				{
					catalog.IsDefault = "Yes";
					TempData["ErrorIsDefault"] = "ไม่สามารถทำการแก้ไข ค่าเริ่มต้นได้ เนื่องจากเป็น Catalog หลักค่ะ";
					return View("EditMyCatalog", catalog);
				}
				else
				{
					catalogRepo.UpdateMyCatalog(guid, catalog.CatalogName, catalog.CatalogDesc, catalog.IsDefault, User.UserId);
					return RedirectToAction("ManageCatalog", new { guid = guid });
				}
			}
		}

		private ActionResult DeleteCatalog(string guid, string isDefault)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CatalogRepository catalogRepo = new CatalogRepository(connection, User);
				if (isDefault == null)
				{
					isDefault = "No";
				}
				else
				{
					isDefault = "Yes";
				}
				string selectGuid = catalogRepo.DeleteMyCatalog(guid, User.UserId, isDefault);
				//if (string.IsNullOrEmpty(selectGuid))
				//{
				//    return CreateCatalog();
				//}
				return View("DeleteMyCatalogSuccess");
			}
		}

		public ActionResult AddProductInCatalog(string pID, string catalogGUID)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CatalogRepository catalogRepo = new CatalogRepository(connection, User);
				Catalog catalog = new Catalog();

				//มี catalog หรือไม่
				bool result = catalogRepo.CheckHasCatalog();

				string[] item = pID.Split(',');
				string[] listPID = pID.Split(',');
				if (result)
				{
					if (string.IsNullOrEmpty(catalogGUID))
					{
						CatalogData catalogData = new CatalogData();
						catalogData.ListCatalog = catalogRepo.GetAllMyCatalog(User.CurrentCompanyId);
						catalogData.pIDs = item;
						if (catalogData.ItemCount == 1)
						{
							return CreateCatalogFromProduct(pID);
						}
					}
					foreach (var pid in listPID)
					{
						catalogRepo.AddToCatalog(pid, catalogGUID);//เพิ่มสินค้าลงใน Catalog
					}
					return RedirectToAction("ManageCatalog", new { guid = catalogGUID });
				}
				else
				{
					return CreateCatalogFromProduct(pID);
				}
			}
		}

		public ActionResult CreateCatalogFromProduct(string pID)
		{
			string result;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CatalogRepository catalogRepo = new CatalogRepository(connection, User);
				Catalog catalog = new Catalog();
				catalog.User = User;

				catalog.CatalogName = "New Catalog";
				catalog.CatalogDesc = "New Catalog";
				catalog.IsDefault = "Yes";

				result = catalogRepo.CreateMyCatalog(catalog);
				if (result != "")
				{
					catalogRepo.AddToCatalog(pID, result);
				}
			}
			return RedirectToAction("ManageCatalog", new { guid = result });
		}


		public ActionResult HandleAction(string guid, string[] checkItem, string[] productId, string[] quantity, ImageInputData Action)
		{
			switch (Action.Name)
			{
				case "DeleteItemCatalog":
					return DeleteItemCatalog(guid, checkItem);
				case "AddToCartOfficeSupply":
					return AddToCart(guid, productId, quantity);
				default:
					return RedirectToAction("ManageCatalog", new { guid = guid });
			}
		}

		private ActionResult DeleteItemCatalog(string guid, string[] item)
		{
			if (item == null)
			{
				return RedirectToAction("ManageCatalog", new { guid = guid });
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				new CatalogRepository(connection, User).DeleteItemMyCatalog(guid, item);
				return RedirectToAction("ManageCatalog", new { guid = guid });
			}
		}

		public ActionResult AddToCart(string guid, string[] productId, string[] quantity)
		{
			if (productId == null || quantity == null || quantity.All(q => string.IsNullOrEmpty(q)) || productId.Length != quantity.Length)
			{
				TempData["SelectItemIsEmpty"] = "กรุณาตรวจเช็คข้อมูลและทำการสั่งซื้อใหม่อีกครั้งค่ะ";
				return RedirectToAction("ManageCatalog", new { guid = guid });
			}
			else
			{
				for (int i = 0; i < productId.Length; i++)
				{
					int qty;
					if (Int32.TryParse(quantity[i], out qty))
					{
						if (qty > 0)
						{
							ShoppingCartProvider.AddToCart(productId[i], qty);
						}
						else if (qty < 0)
						{
							ShoppingCartProvider.AddToCart(productId[i], 1);
						}
					}
					else
					{
						//ถ้ากรอกข้อมูลแต่ไม่ใช่ตัวเลขให้ Default=1
						if (!String.IsNullOrEmpty(quantity[i]))
						{
							ShoppingCartProvider.AddToCart(productId[i], 1);
						}
					}
				}
				Cart cart = ShoppingCartProvider.LoadCartDetail();
				return RedirectToAction("CartDetail", "Cart");
			}
		}




	}
}
