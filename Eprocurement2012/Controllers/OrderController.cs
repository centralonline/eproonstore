﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;
using Eprocurement2012.Models.Repositories;
using System.Data.SqlClient;
using Eprocurement2012.Controllers.Mails;
using OfficeMate.SmsService;
using System.IO;
namespace Eprocurement2012.Controllers
{
	public class OrderController : AuthenticateRoleController
	{

#if !DEBUG
		[RequireHttps]
#endif
		[HttpPost]
		public ActionResult CreateOrder(CartData cartData)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				orderData.User = User;
				User.CurrentCostCenterId = cartData.costcenterId;//ระบบุว่า Cart นี้มาจาก Costcenter อะไร
				GetCartDataForCreate(cartData); //ดึงขอมูลใน cart
				if (!cartData.Cart.ItemCart.Any()) { return RedirectToAction("CartDetail", "Cart"); }
				string orderId = orderRePo.CreateOrder(cartData); //สร้าง Order และ ส่งค่า OrderId ออกมา
				orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
				if (cartData.Cart.SendToAdminAppProd || cartData.Cart.SendToAdminAppBudg)
				{
					orderData.CurrentOrderActivity = SetActivityWaitForAdmin(orderData.Order);// Set ค่าข้อมูล Activity  ของ Order  เพื่อเตรียมส่งไป Save
				}
				else
				{
					orderData.CurrentOrderActivity = SetActivityWaitForApprove(orderData.Order);// Set ค่าข้อมูล Activity  ของ Order  เพื่อเตรียมส่งไป Save
				}

				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);//บันทึก Activity ของ Order
				orderRePo.CreateNewOrderTransactionLog(orderData);//บันทึก OrderTransLog
				orderRePo.CreateOrderDetailTransactionLog(orderData, OrderData.OrderStatusLog.CreateNewOrder);// บันทึก OrderDetailTransaction
				// แยกการส่งเมลล์ในแต่ละ type
				if (cartData.Cart.SendToAdminAppBudg)
				{
					SendMail(new AdminAllowBudgetMail(ControllerContext, orderData), orderId, MailType.RequestAdminAllowBudget, User.UserId);
				}
				else if (cartData.Cart.SendToAdminAppProd)
				{
					SendMail(new AdminAllowProdOutOfOFMMail(ControllerContext, orderData), orderId, MailType.RequestAdminAllowProduct, User.UserId);
				}
				else
				{
					SendMail(new CreateOrderMail(ControllerContext, orderData), orderId, MailType.CreateOrder, User.UserId);
				}

				if (cartData.IsAutoApprove && orderData.Order.Requester.UserId == orderData.Order.CurrentApprover.Approver.UserId)//เช็คว่าเป็น auto approve หรือไม่ ถ้าเป็นต้องส่งไป approve เลย
				{
					if (orderData.Order.GrandTotalAmt < cartData.ListUserApprover.Where(a => a.Approver.UserId == orderData.Order.CurrentApprover.Approver.UserId).FirstOrDefault().ApproveCreditLimit) //ยอดการสั่งซื้อน้อยกว่ายอดเงินของผู้อนุมัติ
					{
						return RedirectToAction("ApprovedOrder", "ApproveOrder", new { orderGuid = orderData.Order.OrderGuid, remark = "Auto Approve", isMultiOrder = false }); //approve ทันที
					}
					else
					{
						return RedirectToAction("PartialApproveOrder", "ApproveOrder", new { orderGuid = orderData.Order.OrderGuid, remark = "Auto Partial Approve", isMultiOrder = false }); //partial approve ส่งต่อให้ผู้อนุมัติคนต่อไป
					}
				}
				//--------------ส่ง SMS หากมีการเลือกรับ SMS แจ้งให้ผู้อนุมัติรับทราบ เพื่อทำการอนุมัติ-----------------------------
				if (cartData.UseSMSFeature.HasValue && cartData.UseSMSFeature.Value)
				{
					if (orderData.Order.CurrentApprover.Approver.UserId != null)
					{
						UserRepository repo = new UserRepository(connection);
						Contact approve = repo.GetUserDataContact(orderData.Order.CurrentApprover.Approver.UserId, User.CurrentCompanyId);
						if (approve.Mobile != null)
						{
							if (approve.Mobile.PhoneNo.Length == 10)
							{
								SendSMS_OFM01(orderData.Order.OrderID, orderData.Order.CostCenter.CostCenterCustID, orderData.Order.CurrentApprover.Approver.UserThaiName, approve);
							}
						}
					}
				}
				return RedirectToAction("ViewOrderDetail", "Order", new { Id = orderData.Order.OrderGuid });
			}
		}

		private void SendSMS_OFM01(string orderId, string custId, string userNameApprover, Contact contact)
		{
			string message = string.Format("ท่านมีรายการสั่งซื้อเลขที่ {0} รอทำการอนุมัติค่ะ", orderId);
			SendSMSTemplate(orderId, custId, userNameApprover, contact, message, "SMS_E-Pro01:รอการอนุมัติ");
		}

		private void SendSMSTemplate(string orderId, string custId, string userNameApprover, Contact contact, string message, string templateName)
		{//orderData.User.UserContact
			SendSmsOrder(contact.Mobile.PhoneNo, message);
			var smsLog = new OfficeMate.Framework.Data.SmsLog();
			smsLog.SMSTemplate = templateName;
			smsLog.SMSTopic = message;
			smsLog.SenderName = "E-Pro";
			smsLog.CustId = custId;
			smsLog.ContactName = userNameApprover;
			smsLog.ContactMobileNo = contact.Mobile.PhoneNo;
			smsLog.WebOrderId = orderId;
			smsLog.InvNo = "";
			smsLog.CreateBy = "System";
			using (SqlConnection connectionMaster = new SqlConnection(ConnectionString))
			{
				new OfficeMate.Framework.Data.Repositories.SmsLogRepository(connectionMaster).InsertLog(smsLog);
			}
		}

		[HttpGet]
		public ActionResult ViewOrderDetail(string Id)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				UserRepository userRepo = new UserRepository(connection);

				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(Id);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
				orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
				orderData.UserApprover = userRepo.GetListUserApprover(orderData.Order.Company.CompanyId, orderData.Order.CostCenter.CostCenterID, User.UserId);//ดึงข้อมูลผู้อนุมัติ ของ Costcenter นี้
				orderData.Order.User = User;//ใส่ User เพื่อให้ Partial  ไว้ใช้ตรวจสอบเปิดปิดปุ่ม
				if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)//หากใช้ campany catalog และแสดง ofm catalog จะต้องเช็คว่า สินค้าใดมาจาก ofm catalog บ้าง
				{
					IEnumerable<string> listId = new ProductRepository(connection, User).IsListPidInProductCatalog();
					foreach (var prod in orderData.Order.ItemOrders)
					{
						if (!listId.Any(p => p == prod.Product.Id))
						{
							prod.IsOfmCatalog = true;
							orderData.Order.SendToAdminAppProd = true;
						}
					}
				}
				if (User.Company.UseGoodReceive && orderData.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Shipped)
				{
					orderData.Order.GoodReceive = orderRePo.GetMyGoodReceive(orderId);
				}
				return View("ViewOrder", orderData);
			}
		}

		private OrderActivity SetActivityWaitForApprove(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ส่งใบสั่งซื้อพิจารณาอนุมัติ";
			orderActivity.ActivityEName = "Wait For Approved";
			orderActivity.Remark = "รอการพิจารณาจากผู้อนุมัติ คุณ " + order.CurrentApprover.Approver.DisplayName;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityWaitForAdmin(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ส่งใบสั่งซื้อพิจารณาอนุมัติเป็นกรณีพิเศษ";
			orderActivity.ActivityEName = "Wait For Admin Allow";
			orderActivity.Remark = "รอการพิจารณาจากผู้ดูแลระบบ คุณ " + order.CurrentApprover.Approver.DisplayName;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		public ActionResult GetDataOrderWaitingProcess()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new OrderRepository(connection).GetDataOrder(User.CurrentCompanyId, User.UserId, Order.OrderStatus.Waiting);
				if (User.IsApprover && User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder && User.Company.ShowMultiApprove)
				{
					foreach (var item in orders)
					{
						Budget budget = new BudgetRepository(connection).GetBudgetByCostcenterID(item.CostCenter.CostCenterID, item.Company.CompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
						item.CurrentBudget = budget.RemainBudgetAmt;
						item.SendToAdminAppBudg = item.GrandTotalAmt > budget.RemainBudgetAmt; // เป็น true แสดงว่า ยอดเงินเกิน budget ของแผนก
					}
				}
				return View("ViewDataOrder", new MasterPageData<IEnumerable<Order>>(orders));
			}
		}
		public ActionResult GetDataOrderReviseProcess()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new OrderRepository(connection).GetDataOrder(User.CurrentCompanyId, User.UserId, Order.OrderStatus.Revise);
				return View("ViewDataOrder", new MasterPageData<IEnumerable<Order>>(orders));
			}
		}
		public ActionResult GetDataOrderApproveProcess()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new OrderRepository(connection).GetDataOrder(User.CurrentCompanyId, User.UserId, Order.OrderStatus.Approved);
				if (User.IsApprover && User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder && User.Company.ShowMultiApprove)
				{
					foreach (var item in orders)
					{
						Budget budget = new BudgetRepository(connection).GetBudgetByCostcenterID(item.CostCenter.CostCenterID, item.Company.CompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
						item.CurrentBudget = budget.RemainBudgetAmt;
						item.SendToAdminAppBudg = item.GrandTotalAmt > budget.RemainBudgetAmt; // เป็น true แสดงว่า ยอดเงินเกิน budget ของแผนก
					}
				}
				return View("ViewDataOrder", new MasterPageData<IEnumerable<Order>>(orders));
			}
		}

		public ActionResult SearchMyOrder(string keyword, string selectedSearch, string redirectUrl)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (string.IsNullOrEmpty(keyword))
				{
					TempData["Error"] = String.Format(new ResourceString("SearchDataOrder.ErrorEmptry"));
					return Redirect(redirectUrl);
				}
				OrderRepository orderRePo = new OrderRepository(connection);
				SearchOrder searchOrder = new SearchOrder();
				searchOrder.Orders = orderRePo.GetSearchMyOrder(keyword, selectedSearch, User.UserId, User.CurrentCompanyId);
				return View("ViewSearchOrderData", searchOrder);
			}
		}

		public ActionResult GetDataOrderForIndex(Order.OrderStatus orderStatus)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new OrderRepository(connection).GetDataOrderStatus(User.CurrentCompanyId, User.UserId, orderStatus, User.IsRequester, User.IsApprover);
				return View("ViewDataOrder", new MasterPageData<IEnumerable<Order>>(orders));
			}
		}

		private CartData GetCartDataForCreate(CartData cartData)
		{
			cartData.Cart = ShoppingCartProvider.LoadMyCart();
			cartData.Cart.ShoppingCartId = ShoppingCartProvider.ShoppingCartId.Value;
			cartData.User = User;
			if (string.IsNullOrEmpty(cartData.CallBackRequestStatus)) { cartData.CallBackRequestStatus = "No"; }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				cartData.ItemCostcenter = costcenterRepo.GetMyCostCenter(User.UserId, User.CurrentCompanyId, User.UserType.Requester);//ดึงข้อมูล ListCostCenter
				cartData.ListUserApprover = userRepo.GetListUserApprover(User.CurrentCompanyId, cartData.costcenterId, User.UserId);//ดึงข้อมูล ListApprover ของ CostCenter ที่เลือก
				string approverId = userRepo.GetUserIdFromUserGuid(cartData.approverId);//ดึง userid จาก guid ที่รับมา
				cartData.CurrentSelectApprover = cartData.ListUserApprover.Where(m => m.Approver.UserId == approverId).Single();//เลือกข้อมูล Approver  ใน List ออกมา เพียงตัวเดียวตามที่ User เลือกมา
				cartData.CurrentUsedCostCenter = costcenterRepo.GetCostcenterDetail(User.CurrentCompanyId, cartData.costcenterId, User.UserId); //ดึงข้อมูล CostCenter  ใน List ออกมา เพียงตัวเดียวตามที่ User เลือกมา

				//เอาข้อมูล Contactor มาแสดงแทนข้อมูล ShipContactor
				cartData.CurrentUsedCostCenter.CostCenterShipping.ShipContactor = cartData.CurrentUsedCostCenter.CostCenterContact.ContactorName;
				cartData.CurrentUsedCostCenter.CostCenterShipping.ShipPhoneNo = cartData.CurrentUsedCostCenter.CostCenterContact.ContactorPhone;
				cartData.CurrentUsedCostCenter.CostCenterShipping.Extension = cartData.CurrentUsedCostCenter.CostCenterContact.ContactorExtension;
				cartData.CurrentUsedCostCenter.CostCenterShipping.ShipMobileNo = cartData.CurrentUsedCostCenter.CostCenterContact.ContactMobileNo;

				if (!cartData.CurrentUsedCostCenter.IsDeliCharge) { cartData.Cart.TotalDeliveryCharge = 0; }
				if (cartData.Cart.GrandTotalAmt > cartData.ListUserApprover.OrderByDescending(c => c.Level).FirstOrDefault().ApproveCreditLimit) { cartData.Cart.IsOverCreditlimit = true; } //กรณีมียอดสั่งซื้อเกินวงเงินของผู้อนุมัติท่านสุดท้าย ไม่ให้สั่ง
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)
				{
					BudgetRepository repo = new BudgetRepository(connection);
					cartData.CurrentBudget = repo.GetBudgetByCostcenterID(cartData.costcenterId, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					cartData.CurrentBudget.WaitingOrderAmt = repo.GetWaitingOrderOnThisBudget(cartData.CurrentBudget.GroupID, User.CurrentCompanyId, User.Company.BudgetLevelType);
					cartData.Cart.SendToAdminAppBudg = cartData.Cart.GrandTotalAmt > cartData.CurrentBudget.RemainBudgetAmt;
				}

				if (User.UserId != approverId || !cartData.CurrentUsedCostCenter.UseAutoApprove) { cartData.AutoApprove = "No"; }//กรณีชื่อผู้สั่งซื้อกับผู้อนุมัติเป็นคนละคนกัน หรือ costcenter นี้ไม่เปิดใช้ auto Approve จะเซ็ท AutoApprove เป็น No
				if (User.UserId == approverId && cartData.CurrentUsedCostCenter.UseAutoApprove && string.IsNullOrEmpty(cartData.AutoApprove)) { cartData.AutoApprove = "Error"; }//กรณีที่เปิดใช้ auto approve และ ผู้สั่งซื้อกับผู้อนุมัติเป็นคนเดียวกันต้องเลือก Yes/No มา ไม่เลือกไม่ให้ผ่าน
				if (User.UserId == approverId && cartData.CurrentUsedCostCenter.UseAutoApprove && cartData.ListUserApprover.Count() == 1 && cartData.Cart.GrandTotalAmt > cartData.ListUserApprover.Single().ApproveCreditLimit) { cartData.Cart.IsOverCreditlimit = true; }//กรณีที่เปิดใช้ auto approve และ ผู้สั่งซื้อกับผู้อนุมัติเป็นคนเดียวกันแต่วงเงินอนุมัติไม่พอ ไม่ให้สั่ง

			}
			using (SqlConnection connectionMaster = new SqlConnection(ConnectionString))
			{
				//ต้องแยก Conection เพราะใน FrameWork ต้องการ Master
				CostCenterRepository costcenterRepo = new CostCenterRepository(connectionMaster);
				cartData.CurrentUsedCostCenter.CostCenterPayment = costcenterRepo.GetMyCostCenterPayment(cartData.CurrentUsedCostCenter.CostCenterCustID);
			}
			return cartData;
		}

		public ActionResult ReceiveAll(string orderGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRepo = new OrderRepository(connection);
				GoodReceive goodReceive = new GoodReceive();
				string orderId = orderRepo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				goodReceive = orderRepo.GetMyGoodReceive(orderId);
				orderRepo.CreateGoodReceiveAll(goodReceive, User.UserId);
				goodReceive = orderRepo.GetMyGoodReceive(orderId);//ดึงข้อมูลหลังจากการ insert GoodReceive เพื่อทำการส่งเมล์
				if (goodReceive.Status == GoodReceive.GoodReceiveStatus.Completed)
				{
					orderRepo.UpdateOrderStatus(Order.OrderStatus.Completed, orderGuid, User.UserId);
				}
				goodReceive.Order = orderRepo.GetMyOrder(orderId);
				goodReceive.CurrentOrderActivity = SetActivityForGoodReceive(orderId);// Set ค่าข้อมูล Activity  ของ Order  เพื่อเตรียมส่งไป Save
				orderRepo.CreateOrderActivity(goodReceive.CurrentOrderActivity);//บันทึก Activity ของ Order
				SendMail(new GoodReceiveMail(ControllerContext, goodReceive), orderId, MailType.GoodReceive, User.UserId);
				return RedirectToAction("ViewOrderDetail", "Order", new { Id = orderGuid });
			}
		}

		public ActionResult PartialReceive(string orderGuid)
		{
			return View("PartialReceive", GetGoodReceiveData(orderGuid));
		}

		[HttpGet]
		public ActionResult SavePartialReceive(string orderGuid)
		{
			GoodReceive goodReceive = GetGoodReceiveData(orderGuid);
			goodReceive.ItemReceiveNow = goodReceive.ItemReceiveDetail.Where(r => r.NumOfAction == goodReceive.NumOfAction && r.RemainQty != 0);
			goodReceive.IsReceive = true;
			return View("PartialReceive", goodReceive);
		}

		[HttpPost]
		public ActionResult SavePartialReceive(string[] pId, int[] actionQty, int[] cancelQty, string actionDate, string orderGuid, ImageInputData action)
		{
			if (action.Name == "cancelReceive") { return RedirectToAction("PartialReceiveSuccess", new { orderGuid = orderGuid }); }
			bool isCheck = true;
			GoodReceive goodReceive = null;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				List<GoodReceiveDetail> listGR = new List<GoodReceiveDetail>();
				DateTime actionDateOn;
				OrderRepository orderRepo = new OrderRepository(connection);
				string orderId = orderRepo.GetOrderIdByGuid(orderGuid); //หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				IEnumerable<GoodReceiveDetail> grDetail = orderRepo.GetMaxGoodReceiveDetail(orderId);
				int index = 0;
				foreach (var item in pId)
				{
					GoodReceiveDetail grd = new GoodReceiveDetail();
					grd.PId = item;
					grd.OriginalQty = grDetail.Where(p => p.PId == item).Single().RemainQty;
					grd.ActionQty = actionQty[index];
					grd.ActionQty_New = actionQty[index];
					grd.CancelQty = cancelQty[index];
					grd.CancelQty_New = cancelQty[index];
					grd.RemainQty = grd.OriginalQty - (grd.ActionQty + grd.CancelQty);
					if (grd.RemainQty < 0)
					{
						TempData["Error" + item] = String.Format(new ResourceString("PartialReceive.ErrorRemainQty")); //แสดง error แต่ละ pid
					}
					grd.NumOfAction = grDetail.First().NumOfAction + 1;
					listGR.Add(grd);
					index++;
				}

				if (actionQty.All(a => a == 0) && cancelQty.All(c => c == 0))
				{
					TempData["Error"] = String.Format(new ResourceString("PartialReceive.ErrorActionQty"));
					isCheck = false;
				}
				if (listGR.Any(o => o.RemainQty < 0))
				{
					isCheck = false;
				}

				if (isCheck)
				{
					if (!String.IsNullOrEmpty(actionDate) && DateTime.TryParseExact(actionDate, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out actionDateOn))
					{
						GoodReceive.GoodReceiveStatus grStatus;
						if (listGR.Any(p => p.RemainQty != 0))
						{
							grStatus = GoodReceive.GoodReceiveStatus.Partial;
						}
						else
						{
							grStatus = GoodReceive.GoodReceiveStatus.Completed;
						}
						orderRepo.CreateGoodPartialReceive(listGR, grStatus, orderId, User.UserId, actionDateOn);
						goodReceive = new GoodReceive();
						goodReceive = orderRepo.GetMyGoodReceive(orderId); // ดึงข้อมูล GoodReceive มาสำหรับแสดงหน้าส่งเมล์
						if (goodReceive.Status == GoodReceive.GoodReceiveStatus.Completed)
						{
							orderRepo.UpdateOrderStatus(Order.OrderStatus.Completed, orderGuid, User.UserId);
						}
						goodReceive.Order = orderRepo.GetMyOrder(orderId); // ดึงข้อมูล order มาสำหรับส่งไปทำ OrderActivity
						goodReceive.CurrentOrderActivity = SetActivityForGoodReceive(orderId);// Set ค่าข้อมูล Activity  ของ Order  เพื่อเตรียมส่งไป Save
						orderRepo.CreateOrderActivity(goodReceive.CurrentOrderActivity);//บันทึก Activity ของ Order

						SendMail(new GoodReceiveMail(ControllerContext, goodReceive), orderId, MailType.GoodReceive, User.UserId);

						return RedirectToAction("PartialReceiveSuccess", new { orderGuid = orderGuid });
					}
					else //ถ้าไม่กรอกวันที่หรือกรอกผิดรูปแบบ
					{
						TempData["Error"] = String.Format(new ResourceString("PartialReceive.ErrorActionDate"));
					}
				}

				/*กรณีที่กรอกข้อมูลไม่ครบ หรือมีข้อมูลใดๆที่ไม่ถูกต้อง*/
				goodReceive = GetGoodReceiveData(orderGuid);
				goodReceive.ItemReceiveNow = goodReceive.ItemReceiveDetail.Where(r => r.NumOfAction == goodReceive.NumOfAction && r.RemainQty != 0);
				foreach (var item in goodReceive.ItemReceiveNow)
				{
					foreach (var prop in listGR)
					{
						if (prop.PId == item.PId)
						{
							item.ActionQty = prop.ActionQty;
							item.ActionQty_New = prop.ActionQty_New;
							item.CancelQty = prop.CancelQty;
							item.CancelQty_New = prop.CancelQty_New;
						}
					}
				}
				goodReceive.ActionDate = actionDate;
				goodReceive.IsReceive = true;
				return View("PartialReceive", goodReceive);
			}
		}

		public ActionResult PartialReceiveSuccess(string orderGuid)
		{
			return View("PartialReceive", GetGoodReceiveData(orderGuid));
		}

		private GoodReceive GetGoodReceiveData(string orderGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRepo = new OrderRepository(connection);
				GoodReceive goodReceive = new GoodReceive();
				string orderId = orderRepo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				goodReceive = orderRepo.GetMyGoodReceive(orderId);
				goodReceive.orderGuid = orderGuid;
				return goodReceive;
			}
		}

		private OrderActivity SetActivityForGoodReceive(string orderid)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = orderid;
			orderActivity.ActivityTName = "ทำรับสินค้า";
			orderActivity.ActivityEName = "GoodReceive";
			orderActivity.Remark = "คุณ " + User.DisplayName + " ได้ทำการรับสินค้าใบสั่งซื้อหมายเลข: " + orderid;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}
		

		/*-----------------------Start download attachFile--------------------------*/
		public FilePathResult DownloadAttachFile(string systemFileName)
		{
			string path = Path.Combine(Server.MapPath("~/AttachFile"), systemFileName);
			string contentType = System.IO.Path.GetExtension(systemFileName);
			return File(path, contentType, systemFileName);
		}



	}
}
