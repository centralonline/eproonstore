﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
namespace Eprocurement2012.Controllers
{
	public class RePurchaseController : AuthenticateRoleController
	{
		//
		// GET: /RePurchase/
		public ActionResult ViewRePurchase()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRepo = new OrderRepository(connection);
				ProductRepository productRepo = new ProductRepository(connection, User);
				RePurchase repurchase = new RePurchase();
				repurchase.Orders = orderRepo.GetDataOrder(User.CurrentCompanyId, User.UserId);
				if (repurchase.Orders.Count() > 0)
				{
					string orderId = orderRepo.GetOrderIdByGuid(repurchase.Orders.Select(o => o.OrderGuid).First());
					repurchase.Products = productRepo.GetProductInOrder(orderId);
					repurchase.OrderId = orderId;
				}
				return View("ProductList.ByOrder", repurchase);
			}

		}
		public ActionResult ViewProductInOrder(string orderGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRepo = new OrderRepository(connection);
				ProductRepository productRepo = new ProductRepository(connection, User);
				RePurchase repurchase = new RePurchase();
				repurchase.Orders = orderRepo.GetDataOrder(User.CurrentCompanyId, User.UserId);
				if (repurchase.Orders != null)
				{
					string orderId = orderRepo.GetOrderIdByGuid(orderGuid);
					repurchase.Products = productRepo.GetProductInOrder(orderId);
					repurchase.OrderId = orderId;
				}
				return View("ProductList.ByOrder", repurchase);
			}
		}

	}
}
