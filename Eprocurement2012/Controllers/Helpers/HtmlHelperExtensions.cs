﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc.Html;
using System.Globalization;
using System.Linq.Expressions;
using System.Web.Routing;
using Textile;
using Eprocurement2012.Models;

namespace Eprocurement2012.Views
{
	public abstract class Resources { }
}

namespace System.Web.Mvc
{
	public static class HtmlHelperExtensions
	{
		public static string GetLocalizedString(string resourceKey)
		{
			if (_resource == null)
			{
				_resource = new System.Resources.ResourceManager(typeof(Eprocurement2012.Views.Resources));
			}
			return _resource.GetString(resourceKey) ?? resourceKey;
		}

		private static System.Resources.ResourceManager _resource;

		public static string ToResourceKey(this Enum e)
		{
			return e.GetType().ToString() + "." + e.ToString();
		}

		public static string _(this HtmlHelper html, string resourceKey) { return GetLocalizedString(resourceKey); }
		public static string _(this HtmlHelper html, string resourceKey, object arg0) { return String.Format(_(html, resourceKey), arg0); }
		public static string _(this HtmlHelper html, string resourceKey, object arg0, object arg1) { return String.Format(_(html, resourceKey), arg0, arg1); }
		public static string _(this HtmlHelper html, string resourceKey, object arg0, object arg1, object arg2) { return String.Format(_(html, resourceKey), arg0, arg1, arg2); }
		public static string _(this HtmlHelper html, string resourceKey, params object[] args) { return String.Format(_(html, resourceKey), args); }

		public static MvcHtmlString LocalizedValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression) { return htmlHelper.LocalizedValidationMessageFor<TModel, TProperty>(expression, null, new RouteValueDictionary()); }
		public static MvcHtmlString LocalizedValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage) { return htmlHelper.LocalizedValidationMessageFor<TModel, TProperty>(expression, validationMessage, new RouteValueDictionary()); }
		public static MvcHtmlString LocalizedValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage, object htmlAttributes) { return htmlHelper.LocalizedValidationMessageFor<TModel, TProperty>(expression, validationMessage, new RouteValueDictionary(htmlAttributes)); }
		public static MvcHtmlString LocalizedValidationMessageFor<TModel, TProperty>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TProperty>> expression, string validationMessage, IDictionary<string, object> htmlAttributes) { return htmlHelper.LocalizedValidationMessageHelper(ModelMetadata.FromLambdaExpression<TModel, TProperty>(expression, htmlHelper.ViewData), ExpressionHelper.GetExpressionText(expression), validationMessage, htmlAttributes); }

		private static MvcHtmlString LocalizedValidationMessageHelper(this HtmlHelper htmlHelper, ModelMetadata modelMetadata, string expression, string validationMessage, IDictionary<string, object> htmlAttributes)
		{
			object errors;
			object item;
			string validationMessageCssClassName;
			string fullHtmlFieldName = htmlHelper.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldName(expression);
			FormContext formContextForClientValidation = htmlHelper.ViewContext.ClientValidationEnabled ? htmlHelper.ViewContext.FormContext : null;
			if (htmlHelper.ViewData.ModelState.ContainsKey(fullHtmlFieldName) || formContextForClientValidation != null)
			{
				ModelState modelState = htmlHelper.ViewData.ModelState[fullHtmlFieldName];
				if (modelState == null)
				{
					errors = null;
				}
				else
				{
					errors = modelState.Errors;
				}
				ModelErrorCollection modelErrorCollection = (ModelErrorCollection)errors;
				if (modelErrorCollection == null || modelErrorCollection.Count == 0)
				{
					item = null;
				}
				else
				{
					item = modelErrorCollection[0];
				}
				ModelError modelError = (ModelError)item;
				if (modelError != null || formContextForClientValidation != null)
				{
					TagBuilder tagBuilder = new TagBuilder("span");
					tagBuilder.MergeAttributes<string, object>(htmlAttributes);
					TagBuilder tagBuilder1 = tagBuilder;
					if (modelError != null)
					{
						validationMessageCssClassName = HtmlHelper.ValidationMessageCssClassName;
					}
					else
					{
						validationMessageCssClassName = HtmlHelper.ValidationMessageValidCssClassName;
					}
					tagBuilder1.AddCssClass(validationMessageCssClassName);
					if (string.IsNullOrEmpty(validationMessage))
					{
						if (modelError != null)
						{
							tagBuilder.SetInnerText(HtmlHelperExtensions.GetUserErrorMessageOrDefault(htmlHelper.ViewContext.HttpContext, modelError, modelState));
						}
					}
					else
					{
						tagBuilder.SetInnerText(validationMessage);
					}
					if (formContextForClientValidation != null)
					{
						tagBuilder.GenerateId(string.Concat(fullHtmlFieldName, ".validationMessage"));
						FieldValidationMetadata fieldValidationMetadatum = HtmlHelperExtensions.ApplyFieldValidationMetadata(htmlHelper, modelMetadata, fullHtmlFieldName);
						fieldValidationMetadatum.ReplaceValidationMessageContents = string.IsNullOrEmpty(validationMessage);
						fieldValidationMetadatum.ValidationMessageId = tagBuilder.Attributes["id"];
					}
					return MvcHtmlString.Create(tagBuilder.ToString(TagRenderMode.Normal));
				}
				else
				{
					return null;
				}
			}
			else
			{
				return null;
			}
		}

		private static FieldValidationMetadata ApplyFieldValidationMetadata(HtmlHelper htmlHelper, ModelMetadata modelMetadata, string modelName)
		{
			FormContext formContext = htmlHelper.ViewContext.FormContext;
			FieldValidationMetadata validationMetadataForField = formContext.GetValidationMetadataForField(modelName, true);
			IEnumerable<ModelValidator> validators = ModelValidatorProviders.Providers.GetValidators(modelMetadata, htmlHelper.ViewContext);
			IEnumerable<ModelValidator> modelValidators = validators;
			foreach (ModelClientValidationRule modelClientValidationRule in modelValidators.SelectMany<ModelValidator, ModelClientValidationRule>((ModelValidator v) => v.GetClientValidationRules()))
			{
				validationMetadataForField.ValidationRules.Add(modelClientValidationRule);
			}
			return validationMetadataForField;
		}

		private static string GetUserErrorMessageOrDefault(HttpContextBase httpContext, ModelError error, ModelState modelState)
		{
			object attemptedValue;
			if (string.IsNullOrEmpty(error.ErrorMessage))
			{
				if (modelState != null)
				{
					if (modelState.Value != null)
					{
						attemptedValue = modelState.Value.AttemptedValue;
					}
					else
					{
						attemptedValue = null;
					}
					string str = (string)attemptedValue;
					object[] objArray = new object[1];
					objArray[0] = str;
					return string.Format(CultureInfo.CurrentCulture, HtmlHelperExtensions.GetInvalidPropertyValueResource(httpContext), objArray);
				}
				else
				{
					return null;
				}
			}
			else
			{
				return GetLocalizedString(error.ErrorMessage);
			}
		}

		private static string GetInvalidPropertyValueResource(HttpContextBase httpContext)
		{
			string globalResourceObject = null;
			if (!string.IsNullOrEmpty(ValidationExtensions.ResourceClassKey) && httpContext != null)
			{
				globalResourceObject = httpContext.GetGlobalResourceObject(ValidationExtensions.ResourceClassKey, "InvalidPropertyValue", CultureInfo.CurrentUICulture) as string;
			}
			string str = globalResourceObject;
			string commonValueNotValidForProperty = str;
			if (str == null)
			{
				commonValueNotValidForProperty = "The value '{0}' is invalid.";
			}
			return commonValueNotValidForProperty;
		}
		
		public static string GenerateDepartmentPaths(this HtmlHelper html, string currentPaths, params int[] departmentIds)
		{
			return GenerateDepartmentPaths(currentPaths, departmentIds);
		}

		public static string BackDepartmentPaths(this HtmlHelper html, string currentPaths)
		{
			String[] deptPaths = (currentPaths ?? string.Empty).Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
			if (deptPaths.Length < 2) { return null; }
			Array.Resize(ref deptPaths, deptPaths.Length - 1);
			return String.Join("-", deptPaths);
		}

		public static string GenerateDepartmentPaths(string currentPaths, params int[] departmentIds)
		{
			string stringIds = String.Join("-", departmentIds.Select(id => id.ToString()).ToArray());
			return String.IsNullOrEmpty(currentPaths) ? stringIds : currentPaths + "-" + stringIds;
		}

		public static string GetMonthName(this HtmlHelper html, int month)
		{
			return System.Threading.Thread.CurrentThread.CurrentUICulture.DateTimeFormat.GetMonthName(month);
		}

		public static void FormatTextile(this HtmlHelper html, string textileString)
		{
			var formatter = new TextileFormatter(new ResponseOutputter(html.ViewContext.HttpContext.Response)) { HeaderOffset = 3 };
			formatter.Format(textileString);
			formatter.Format("p{clear:both}.");
		}
		private class ResponseOutputter : IOutputter
		{
			private HttpResponseBase _response;
			public ResponseOutputter(HttpResponseBase response)
			{
				_response = response;
			}

			public void Begin() { }

			public void End() { }

			public void Write(string text)
			{
				_response.Write(text);
			}

			public void WriteLine(string line)
			{
				_response.Write(line);
				_response.Write(Environment.NewLine);
			}
		}

	}

	public class SubMenuAdmin : System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.IMasterPageData>
	{
		private string _currentAction;
		public string GetTextIfCurrentActionIsInArray(string text, params string[] actions)
		{
			if (_currentAction == null)
			{
				_currentAction =ViewContext.RouteData.Values["action"].ToString();
			}
			return actions.Any(action => action == _currentAction) ? text : string.Empty;
		}
	}

	public class SubMenuOFMAdmin : System.Web.Mvc.ViewUserControl<string>
	{
		private string _currentAction;
		public string GetTextIfCurrentActionIsInArray(string text, params string[] actions)
		{
			if (_currentAction == null)
			{
				_currentAction = ViewContext.RouteData.Values["action"].ToString();
			}
			return actions.Any(action => action == _currentAction) ? text : string.Empty;
		}
	}
}