﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;
using System.Text;

namespace Eprocurement2012.Controllers.Helpers
{
	public class LoginCookieProvider : ILoginProvider
	{
		private const string _hashCookieName = "_EPROAUTH_";
		private const string _idCookieName = "_EPROGUID_";

		private HttpRequestBase _request;
		private HttpResponseBase _response;

		public LoginCookieProvider(HttpRequestBase request, HttpResponseBase response)
		{
			_request = request;
			_response = response;
		}

		public LoginCookieProvider(ControllerContext context) : this(context.HttpContext.Request, context.HttpContext.Response) { }

		public string ReadUserGuid()
		{
			var idCookie = _request.Cookies[_idCookieName];
			var hashCookie = _request.Cookies[_hashCookieName];
			if (idCookie == null || hashCookie == null) { return null; }
			if (String.IsNullOrEmpty(idCookie.Value)) { return null; }
			if (!CompareHash(idCookie.Value, _request.UserAgent, hashCookie.Value)) { return null; }
			return idCookie.Value;
		}

		public void Login(string userGuid, bool remember)
		{
			string domain = _request.Url.Host.TrimStart('w', 'W');
			if (!domain.Equals("localhost", StringComparison.OrdinalIgnoreCase))
			{
				_response.Cookies.Set(new HttpCookie(_hashCookieName, Hash(userGuid, _request.UserAgent)) { HttpOnly = true, Domain = domain, Expires = remember ? DateTime.MaxValue : DateTime.Now.AddHours(3.0) });
				_response.Cookies.Set(new HttpCookie(_idCookieName, userGuid) { HttpOnly = true, Domain = domain, Expires = remember ? DateTime.MaxValue : DateTime.Now.AddHours(3.0) });
			}
			else
			{
				_response.Cookies.Set(new HttpCookie(_hashCookieName, Hash(userGuid, _request.UserAgent)) { HttpOnly = true, Expires = remember ? DateTime.MaxValue : DateTime.Now.AddHours(3.0) });
				_response.Cookies.Set(new HttpCookie(_idCookieName, userGuid) { HttpOnly = true, Expires = remember ? DateTime.MaxValue : DateTime.Now.AddHours(3.0) });
			}
		}

		public void Logout()
		{
			string domain = _request.Url.Host.TrimStart('w', 'W');
			if (!domain.Equals("localhost", StringComparison.OrdinalIgnoreCase))
			{
				_response.Cookies.Set(new HttpCookie(_hashCookieName) { HttpOnly = true, Domain = domain, Expires = DateTime.MinValue.AddDays(1) });
				_response.Cookies.Set(new HttpCookie(_idCookieName) { HttpOnly = true, Domain = domain, Expires = DateTime.MinValue.AddDays(1) });
			}
			else
			{
				_response.Cookies.Set(new HttpCookie(_hashCookieName) { HttpOnly = true, Expires = DateTime.MinValue.AddDays(1) });
				_response.Cookies.Set(new HttpCookie(_idCookieName) { HttpOnly = true, Expires = DateTime.MinValue.AddDays(1) });
			}
		}

		private string Hash(string userGuid, string userAgent)
		{
			byte[] salt = new byte[12];
			new RNGCryptoServiceProvider().GetBytes(salt);
			return Hash(userGuid, userAgent, Convert.ToBase64String(salt));
		}

		private string Hash(string userGuid, string userAgent, string salt)
		{
			SHA256 hasher = SHA256.Create();
			byte[] bytes = Encoding.UTF8.GetBytes(userGuid + "[" + userAgent + "]" + ":" + salt);

			return salt + Convert.ToBase64String(hasher.ComputeHash(bytes));
		}

		private bool CompareHash(string userGuid, string userAgent, string hash)
		{
			return hash == Hash(userGuid, userAgent, hash.Substring(0, 16));
		}
	}
}