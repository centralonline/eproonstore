﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Cryptography;

namespace Eprocurement2012.Controllers.Helpers
{
	public class VerifyHasher
	{
		public string GenerateVerifyKey(string hashedPassword)
		{
			string key = GenerateKey();
			return key + hashedPassword;
		}
		private string GenerateKey()
		{
			byte[] keyBytes = new byte[15];
			RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
			rng.GetBytes(keyBytes);
			return Convert.ToBase64String(keyBytes);
		}
		public string GetHashedPassword(string verifyKey)
		{
			return verifyKey.Substring(20);
		}
	}
}
