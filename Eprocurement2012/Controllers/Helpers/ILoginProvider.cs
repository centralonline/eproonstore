﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Controllers.Helpers
{
	public interface ILoginProvider
	{
		void Login(string userGuid, bool remember);
		void Logout();
		string ReadUserGuid();
	}
}