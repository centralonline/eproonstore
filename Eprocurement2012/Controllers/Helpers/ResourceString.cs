﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers.Helpers
{
	public class ResourceString
	{
		private string _resourceKey;
		public ResourceString(string resouceKey)
		{
			_resourceKey = resouceKey;
		}

		public static implicit operator string(ResourceString resouceString)
		{
			return resouceString.ToString();
		}

		public override string ToString()
		{
			return HtmlHelperExtensions.GetLocalizedString(_resourceKey);
		}
	}
}
