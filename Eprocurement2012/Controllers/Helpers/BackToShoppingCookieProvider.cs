﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers.Helpers
{
	public class BackToShoppingCookieProvider
	{
		private const string _backToShopName = ".BACKTOSHOPEPRO.";
		private HttpRequestBase _request;
		private HttpResponseBase _response;

		public BackToShoppingCookieProvider(HttpRequestBase request, HttpResponseBase response)
		{
			_request = request;
			_response = response;
		}

		public BackToShoppingCookieProvider(ControllerContext context) : this(context.HttpContext.Request, context.HttpContext.Response) { }

		public void Set(string url)
		{
			_response.Cookies.Set(new HttpCookie(_backToShopName, HttpUtility.UrlEncode(url)) { HttpOnly = true, Expires = DateTime.Now.AddDays(1) });
		}

		public string Get()
		{
			var result = _request.Cookies[_backToShopName];
			if (result == null) { return string.Empty; }
			return HttpUtility.UrlDecode(result.Value);
		}
		
	}
}