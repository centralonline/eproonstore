﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers.Helpers
{
	public class ProductHistoryCookieProvider : Eprocurement2012.Controllers.Helpers.IProductHistoryProvider
	{
		private const string _productHistoryCookieName = ".PRODUCTHISTORY.";
		private const int Max = 19;
		private HttpRequestBase _request;
		private HttpResponseBase _response;

		public ProductHistoryCookieProvider(HttpRequestBase request, HttpResponseBase response)
		{
			_request = request;
			_response = response;
		}

		public ProductHistoryCookieProvider(ControllerContext context) : this(context.HttpContext.Request, context.HttpContext.Response) { }

		private IEnumerable<string> ReadProductHistory()
		{
			var productHistoryCookie = _request.Cookies[_productHistoryCookieName];
			if (productHistoryCookie == null) { return new string[0]; }
			return productHistoryCookie.Value.Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
		}

		public void Add(string productId)
		{
			var current = ReadProductHistory().Where(p => p != productId).ToList();
			current.Add(productId);
			_response.Cookies.Set(new HttpCookie(_productHistoryCookieName, String.Join(",", current.Skip(current.Count - (Max + 1)).ToArray())) { HttpOnly = true, Expires = DateTime.MaxValue });
		}

		public IEnumerable<string> GetExceptCurrent(string currentProductId)
		{
			var result = ReadProductHistory().Where(p => p != currentProductId).Reverse();
			return result;
		}
	}
}