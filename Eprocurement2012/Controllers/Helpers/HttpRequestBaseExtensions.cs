﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Controllers.Helpers
{
	public static class HttpRequestBaseExtensions
	{
		public static string IPAddress(this HttpRequestBase request)
		{
			if (String.IsNullOrEmpty(request.ServerVariables["HTTP_X_FORWARDED_FOR"]) || request.ServerVariables["HTTP_X_FORWARDED_FOR"].Contains("unknown"))
			{
				return request.ServerVariables["REMOTE_ADDR"];
			}
			else
			{
				return request.ServerVariables["HTTP_X_FORWARDED_FOR"].Split(new char[] { ';', ',' }, StringSplitOptions.RemoveEmptyEntries).FirstOrDefault();
			}
		}
	}
}