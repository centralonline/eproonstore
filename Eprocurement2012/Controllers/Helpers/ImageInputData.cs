﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers.Helpers
{
	[ModelBinder(typeof(ImageInputData))]
	public sealed class ImageInputData : IModelBinder
	{
		public string Name { get; set; }
		public string Value { get; set; }
		
		public object BindModel(ControllerContext controllerContext, ModelBindingContext bindingContext)
		{
			ImageInputData result = new ImageInputData();
			string imageInputName = controllerContext.HttpContext.Request.Form.AllKeys.FirstOrDefault(key => key.EndsWith(".x", StringComparison.OrdinalIgnoreCase));
			if (imageInputName == null) { return result; }
			string[] imageInputNameParts = imageInputName.Split('.');
			switch (imageInputNameParts.Length)
			{
				case 2:
					result.Name = imageInputNameParts[0];
					break;
				case 3:
					result.Name = imageInputNameParts[0];
					result.Value = imageInputNameParts[1];
					break;
				default:
					break;
			}
			return result;
		}
	}
}