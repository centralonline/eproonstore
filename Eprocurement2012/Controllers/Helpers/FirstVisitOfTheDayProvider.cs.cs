﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Globalization;

namespace Eprocurement2012.Controllers.Helpers
{
	public class FirstVisitOfTheDayProvider
	{
		private const string _lastVisitCookieName = ".LASTVISIT.";

		private HttpRequestBase _request;
		private HttpResponseBase _response;

		public FirstVisitOfTheDayProvider(HttpRequestBase request, HttpResponseBase response)
		{
			_request = request;
			_response = response;
		}

		public FirstVisitOfTheDayProvider(ControllerContext context) : this(context.HttpContext.Request, context.HttpContext.Response) { }

		public bool GetFirstVisitOfTheDay()
		{
			var lastVisitCookie = _request.Cookies[_lastVisitCookieName];
			if (lastVisitCookie != null)
			{
				DateTime lastVisit;
				if (DateTime.TryParseExact(lastVisitCookie.Value, "yyyy-MM-dd", CultureInfo.InvariantCulture, DateTimeStyles.AssumeLocal, out lastVisit))
				{
					SetLastVisitCookie();
					return lastVisit < DateTime.Today;
				}
			}
			SetLastVisitCookie();
			return true;
		}

		private void SetLastVisitCookie()
		{
			_response.Cookies.Set(new HttpCookie(_lastVisitCookieName, DateTime.Today.ToString("yyyy-MM-dd", CultureInfo.InvariantCulture)) { HttpOnly = true, Expires = DateTime.Today.AddDays(1) });
		}
	}
}