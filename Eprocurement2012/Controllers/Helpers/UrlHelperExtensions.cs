﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace System.Web.Mvc
{
	public static class UrlHelperExtensions
	{
		public static string FullUrlAction(this UrlHelper self, string actionName, string controllerName)
		{
			return new Uri(self.RequestContext.HttpContext.Request.Url, self.Action(actionName, controllerName)).AbsoluteUri;
		}
		public static string FullUrlAction(this UrlHelper self, string actionName, string controllerName, object routeValues)
		{
			return new Uri(self.RequestContext.HttpContext.Request.Url, self.Action(actionName, controllerName, routeValues)).AbsoluteUri;
		}
		public static string GetFullUrl(this UrlHelper self, string absoluteUrl)
		{
			return new Uri(self.RequestContext.HttpContext.Request.Url, absoluteUrl).AbsoluteUri;
		}
	}
}