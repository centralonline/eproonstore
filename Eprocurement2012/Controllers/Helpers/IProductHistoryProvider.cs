﻿using System;
namespace Eprocurement2012.Controllers.Helpers
{
	public interface IProductHistoryProvider
	{
		void Add(string productId);
		System.Collections.Generic.IEnumerable<string> GetExceptCurrent(string currentProductId);
	}
}
