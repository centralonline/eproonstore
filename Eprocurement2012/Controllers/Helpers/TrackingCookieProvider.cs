﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers.Helpers
{
	public class TrackingCookieProvider
	{
		private const string _trackingReffererUrlCookieName = ".TRACKING.REFFURL.";
		private const string _trackingLandingPageUrlCookieName = ".TRACKING.LANDURL.";
		private HttpRequestBase _request;
		private HttpResponseBase _response;

		public TrackingCookieProvider(HttpRequestBase request, HttpResponseBase response)
		{
			_request = request;
			_response = response;
		}

		public TrackingCookieProvider(ControllerContext context) : this(context.HttpContext.Request, context.HttpContext.Response) { }

		public string ReffererUrl
		{
			get
			{
				var result = _request.Cookies[_trackingReffererUrlCookieName];
				if (result == null) { return string.Empty; }
				return HttpUtility.UrlDecode(result.Value);
			}

			set
			{
				_response.Cookies.Set(new HttpCookie(_trackingReffererUrlCookieName, HttpUtility.UrlEncode(value)) { HttpOnly = true, Expires = DateTime.MaxValue });
			}
		}

		public string LandingPageUrl
		{
			get
			{
				var result = _request.Cookies[_trackingLandingPageUrlCookieName];
				if (result == null) { return string.Empty; }
				return HttpUtility.UrlDecode(result.Value);
			}

			set
			{
				_response.Cookies.Set(new HttpCookie(_trackingLandingPageUrlCookieName, HttpUtility.UrlEncode(value)) { HttpOnly = true, Expires = DateTime.MaxValue });
			}
		}

		public void Clear()
		{
			ReffererUrl = string.Empty;
			LandingPageUrl = string.Empty;
		}
	}
}