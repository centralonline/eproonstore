﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers.Helpers
{
	public class ExceptionContextRenderer : log4net.ObjectRenderer.IObjectRenderer
	{
		private const string _lineFormat = "{0,-20} : {1}";
		public void RenderObject(log4net.ObjectRenderer.RendererMap rendererMap, object obj, System.IO.TextWriter writer)
		{
			ExceptionContext context = obj as ExceptionContext;
			if (context == null) { return; }
			writer.WriteLine(_lineFormat, "Unhandled Exception", context.Exception.ToString());
			writer.WriteLine(_lineFormat, "Timestamp", DateTime.Now.ToString("u"));
			writer.WriteLine(_lineFormat, "IPAddress", context.HttpContext.Request.IPAddress());
			writer.WriteLine(_lineFormat, "Url", context.HttpContext.Request.Url);
			writer.WriteLine(_lineFormat, "UrlReferrer", context.HttpContext.Request.UrlReferrer);
			foreach (var headerKey in context.HttpContext.Request.Headers.AllKeys)
			{
				writer.WriteLine(_lineFormat, headerKey, context.HttpContext.Request.Headers[headerKey]);
			}
		}
	}
}