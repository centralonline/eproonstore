﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using log4net;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
using System.Web.Mvc;
using System.Data.SqlClient;
using OfficeMate.Framework;

namespace Eprocurement2012.Controllers.Helpers
{
	public sealed class ShoppingCartProvider
	{
		private ILog _log = log4net.LogManager.GetLogger(typeof(ShoppingCartProvider));
		private const string _shoppingCartIdCookieName = ".SCID.";
		private HttpRequestBase _request;
		private HttpResponseBase _response;
		private string _connectionString;
		private User _user;

		public ShoppingCartProvider(ControllerContext context, User user, string connectionString)
			: this(context.HttpContext.Request, context.HttpContext.Response, user, connectionString)
		{ }
		public ShoppingCartProvider(HttpRequestBase request, HttpResponseBase response, User user, string connectionString)
		{
			_request = request;
			_response = response;
			_user = user;
			_connectionString = connectionString;
		}

		private int? UserShoppingCartId
		{
			get
			{
				if (!_user.IsLogin) { throw new InvalidOperationException(); }

				return GetUserShoppingCartId(_user.UserId, _user.CurrentCompanyId);
			}
			set
			{
				if (!_user.IsLogin) { throw new InvalidOperationException(); }

				SetUserShoppongCartId(value, _user.UserId, _user.CurrentCompanyId);
			}
		}
		private int? GetUserShoppingCartId(string userId, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				using (SqlCommand command = connection.CreateCommand())
				{
					command.CommandText = "Select SCID From TBCart Where UserId = @UserId and CompanyId = @CompanyId";
					command.Parameters.AddWithValue("@UserId", userId);
					command.Parameters.AddWithValue("@CompanyId", companyId);
					return (int?)command.ExecuteScalar();
				}
			}
		}
		private void SetUserShoppongCartId(int? scid, string userId, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				using (SqlCommand command = connection.CreateCommand())
				{
					command.CommandText = "Update TBCart Set UserId = '' ,CompanyId ='' Where UserId = @UserId and CompanyId = @CompanyId";
					command.Parameters.AddWithValue("@UserId", userId);
					command.Parameters.AddWithValue("@CompanyId", companyId);
					command.ExecuteNonQuery();
				}

				if (scid.HasValue)
				{
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandText = "Update TBCart Set UserId = @UserId, companyId = @companyId Where SCID = @NewSCID";
						command.Parameters.AddWithValue("@UserId", userId);
						command.Parameters.AddWithValue("@NewSCID", scid.Value);
						command.Parameters.AddWithValue("@companyId", companyId);
						command.ExecuteNonQuery();
					}
				}
			}
		}

		private void UpdateAttachFile(int scid, string userId, string companyId, string custFileName, string systemFileName)
		{
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				using (SqlCommand command = connection.CreateCommand())
				{
					command.CommandText = @"Update	TBCart Set UserId = @UserId, CompanyId = @CompanyId, CustFileName = @CustFileName,SystemFileName = @SystemFileName
											Where	SCID = @SCID";

					command.Parameters.AddWithValue("@UserId",userId);
					command.Parameters.AddWithValue("@CompanyId", companyId);
					command.Parameters.AddWithValue("@CustFileName", custFileName);
					command.Parameters.AddWithValue("@SystemFileName", systemFileName);
					command.Parameters.AddWithValue("@SCID", scid);
					command.ExecuteNonQuery();
				}
			}
		}

		private int? _cookieShoppingCartId;
		private bool _hasSetCookieShoppingCartId;
		private int? CookieShoppingCartId
		{
			get
			{
				if (_hasSetCookieShoppingCartId) { return _cookieShoppingCartId; }
				var shoppingCartCookie = _request.Cookies[_shoppingCartIdCookieName];
				if (shoppingCartCookie == null) { return null; }
				int shoppingCartId;
				if (!Int32.TryParse(shoppingCartCookie.Value, out shoppingCartId)) { return null; }
				return shoppingCartId;
			}
			set
			{
				if (value.HasValue)
				{
					_response.Cookies.Set(new HttpCookie(_shoppingCartIdCookieName, value.Value.ToString()) { HttpOnly = true, Expires = DateTime.MaxValue });
				}
				else
				{
					_response.Cookies.Set(new HttpCookie(_shoppingCartIdCookieName) { HttpOnly = true, Expires = DateTime.MinValue.AddDays(1) });
				}
				_cookieShoppingCartId = value;
				_hasSetCookieShoppingCartId = true;
			}
		}

		public int? ShoppingCartId
		{
			get
			{
				int? shoppingCartId = _user.IsLogin ? UserShoppingCartId : CookieShoppingCartId;
				if (shoppingCartId.HasValue)
				{
					using (SqlConnection connection = new SqlConnection(_connectionString))
					{
						connection.Open();
						using (SqlCommand command = connection.CreateCommand())
						{
							command.CommandText = "Select Count(*) From TBCart Where SCID = @SCID ";
							command.Parameters.AddWithValue("@SCID", shoppingCartId.Value);
							if ((int)command.ExecuteScalar() != 1)
							{
								_log.ErrorFormat("SCID {0} is not found in TBCart.", shoppingCartId.Value);
								shoppingCartId = null;
							}
						}
					}
				}
				return shoppingCartId;
			}
			set
			{
				if (_user.IsLogin)
				{
					UserShoppingCartId = value;
				}
				else
				{
					CookieShoppingCartId = value;
				}
			}
		}

		private IDictionary<string, int> LoadCart(int scid)
		{
			Dictionary<string, int> result = new Dictionary<string, int>();
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				using (SqlCommand command = connection.CreateCommand())
				{
					command.CommandText = "Select PID, pQty From TBCartDetail t1 INNER JOIN TBCart t2 ON t1.SCID=t2.SCID AND t1.RefOrderID=t2.RefOrderID Where t1.SCID = @SCID";
					command.Parameters.AddWithValue("@SCID", scid);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							result.Add(reader.GetString(0), reader.GetInt32(1));
						}
						return result;
					}
				}
			}
		}

		public Cart LoadCartType(Cart cart)
		{
			int? scid = ShoppingCartId;
			if (!scid.HasValue) { return cart; }
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				using (SqlCommand command = connection.CreateCommand())
				{
					command.CommandText = @"Select isnull(ScType,'New') as ScType, isnull(RefOrderID,'') AS RefOrderID From TBCart Where SCID = @SCID";
					command.Parameters.AddWithValue("@SCID", scid);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							cart.RefOrderId = (string)reader["RefOrderID"];
							cart.Type = (Cart.CartType)Enum.Parse(typeof(Cart.CartType), (string)reader["ScType"]);
						}
						return cart;
					}
				}
			}
		}

		public Cart LoadCartDetail()
		{
			Cart cart = null;
			int? scid = ShoppingCartId;
			if (!scid.HasValue) { return cart; }
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				connection.Open();
				using (SqlCommand command = connection.CreateCommand())
				{
					command.CommandText = @"SELECT	DISTINCT t2.pID,t2.ReferenceCode,t4.pTName,t4.pEName,t4.pTunit,t4.pEunit,t3.PriceIncVat,t3.PriceExcVat,t2.pQty,
													t4.Status,t3.Isvat,t3.IsBestDeal,t4.IsPromotion,t4.DeliverFee,@IsNotOfmCatalog as IsOfmCatalog,t3.PriceType,
													t1.CustFileName,t1.SystemFileName
											From	TBCart t1 INNER JOIN TBCartDetail t2 ON (t1.SCID=t2.SCID AND t1.RefOrderID=t2.RefOrderID)
													INNER JOIN TBProductCatalog t3 ON (t2.PID = t3.ProductID AND ((t3.CompanyID=@companyId and t3.CatalogtypeID=@companyId) or (t3.CompanyID=@companyId and t3.CatalogtypeID=@userId )))
													INNER JOIN TBProductCenter t4 ON t2.pID = t4.PID
											WHERE	t1.SCID=@scid
											UNION	ALL
											SELECT	DISTINCT t2.pID,t2.ReferenceCode,t3.pTName,t3.pEName,t3.pTunit,t3.pEunit,t3.PriceIncVat,t3.PriceExcVat,t2.pQty,
													t3.Status,t3.Isvat,t3.IsBestDeal,t3.IsPromotion,t3.DeliverFee,@IsOfmCatalog as IsOfmCatalog,'Float' as PriceType,
													t1.CustFileName,t1.SystemFileName
											From	TBCart t1 INNER JOIN TBCartDetail t2 ON (t1.SCID=t2.SCID AND t1.RefOrderID=t2.RefOrderID)
													INNER JOIN TBProductCenter t3 ON t2.pID = t3.PID
											WHERE	t1.SCID=@scid
													AND NOT EXISTS (select ProductId from TBProductCatalog t4 Where t3.pID=t4.ProductID
													AND ((CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId)))
											";
					command.Parameters.AddWithValue("@scid", scid.Value);
					command.Parameters.AddWithValue("@companyId", _user.CurrentCompanyId);
					command.Parameters.AddWithValue("@userId", _user.UserId);
					command.Parameters.AddWithValue("@IsOfmCatalog", CartDetail.OfmCatalog.Yes.ToString());
					command.Parameters.AddWithValue("@IsNotOfmCatalog", CartDetail.OfmCatalog.No.ToString());
					using (SqlDataReader reader = command.ExecuteReader())
					{
						string CustFileName = string.Empty, SystemFileName = string.Empty;
						List<CartDetail> listCartDetail = new List<CartDetail>();
						CartDetail item = null;
						while (reader.Read())
						{
							item = new CartDetail();
							item.Quantity = (int)reader["pqty"];
							item.Product = new Product();
							item.Product.Id = (string)reader["pID"];
							item.Product.ThaiName = (string)reader["PTname"];
							item.Product.EngName = (string)reader["PEname"];
							item.Product.Name = new LocalizedString((string)reader["PTname"]);
							item.Product.Name["en"] = (string)reader["PEname"];
							item.Product.ThaiUnit = (string)reader["pTunit"];
							item.Product.EngUnit = (string)reader["pEunit"];
							item.Product.Unit = new LocalizedString((string)reader["pTunit"]);
							item.Product.Unit["en"] = (string)reader["pEunit"];
							item.Product.PriceIncVat = (decimal)reader["PriceIncVat"];
							item.Product.PriceExcVat = (decimal)reader["PriceExcVat"];
							item.Product.IsVat = Mapper.ToClass<bool>((string)reader["IsVat"]);
							item.Product.IsBestDeal = Mapper.ToClass<bool>((string)reader["IsBestDeal"]);
							item.Product.IsPromotion = Mapper.ToClass<bool>((string)reader["IsPromotion"]);
							item.Product.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"],true);
							item.Product.TransCharge = (decimal)reader["DeliverFee"];
							item.ReferenceCode = (string)reader["ReferenceCode"];
							item.DiscountRate = _user.Company.CompanyDisCountRate;
							CustFileName = (string)reader["CustFileName"];
							SystemFileName = (string)reader["SystemFileName"];
							if (_user.Company.UseOfmCatalog) //บริษัทที่ใช้ OFM Catalog มีสิทธิสั่งซื้อสินค้าทุกรายการ
							{
								item.IsOfmCatalog = false; //เสมือนว่า OFM Catalog เป็น catalog ของตัวเอง
							}
							else
							{
								item.IsOfmCatalog = Mapper.ToClass<bool>((string)reader["IsOFMCatalog"]);
							}
							listCartDetail.Add(item);
						}
						cart = new Cart();
						cart = LoadCartType(cart);
						cart.CustFileName = CustFileName;
						cart.SystemFileName = SystemFileName;
						cart.ItemCart = listCartDetail;
						return cart;
					}
				}
			}
		}

		public IDictionary<string, int> LoadCart()
		{
			int? scid = ShoppingCartId;
			if (!scid.HasValue) { return new Dictionary<string, int>(StringComparer.OrdinalIgnoreCase); }
			return LoadCart(scid.Value);
		}

		public void RemoveItemsFromCart(IEnumerable<string> productIds)
		{
			IDictionary<string, int> cart = LoadCart();
			int? scid = ShoppingCartId;

			foreach (string productId in productIds)
			{
				cart.Remove(productId);
			}
			SaveCart(cart);
		}

		public void RemoveItemsFromCartRevise(IEnumerable<string> productIds, string refOrderId)
		{
			IDictionary<string, int> cart = LoadCart();
			int? scid = ShoppingCartId;

			foreach (string productId in productIds)
			{
				cart.Remove(productId);
			}
			SaveCartRevise(cart, refOrderId);
		}

		public void CartCalulate(Cart cart)
		{
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				ProductRepository productRepo = new ProductRepository(connection, _user);
				cart.ItemCart = cart.ItemCart.Where(cartDetail => cartDetail.Product != null &&
						(!cartDetail.Product.IsPG) &&
						(cartDetail.Product.Status != ProductStatusType.Deleted) &&
						(cartDetail.Product.Status != ProductStatusType.Hold) &&
						(cartDetail.Product.Status != ProductStatusType.OutOfStock) &&
						(cartDetail.Product.Status != ProductStatusType.New)).ToList();

				cart.DisCountRate = _user.Company.CompanyDisCountRate;

				cart.TotalPriceProductIncVat = Math.Round(cart.ItemCart.Sum(Item => Item.Product.PriceIncVat * Item.Quantity), 2, MidpointRounding.AwayFromZero);
				cart.TotalPriceProductExcVat = Math.Round(cart.ItemCart.Where(item => item.Product.IsVat).Sum(Item => Item.Product.PriceExcVat * Item.Quantity), 2, MidpointRounding.AwayFromZero);
				cart.TotalPriceProductNonVat = Math.Round(cart.ItemCart.Where(item => !item.Product.IsVat).Sum(Item => Item.Product.PriceExcVat * Item.Quantity), 2, MidpointRounding.AwayFromZero);

				cart.TotalAllDiscount = cart.ItemCart.Where(Item => !Item.Product.IsBestDeal && !Item.Product.IsPromotion).Sum(Item => Math.Round((Item.Product.PriceExcVat * cart.DisCountRate) / 100, 2, MidpointRounding.AwayFromZero) * Item.Quantity);
				cart.TotalAllDiscountExcVat = cart.ItemCart.Where(Item => (!Item.Product.IsBestDeal && !Item.Product.IsPromotion) && Item.Product.IsVat).Sum(Item => Math.Round((Item.Product.PriceExcVat * cart.DisCountRate) / 100, 2, MidpointRounding.AwayFromZero) * Item.Quantity);
				cart.TotalAllDiscountNonVat = cart.ItemCart.Where(Item => (!Item.Product.IsBestDeal && !Item.Product.IsPromotion) && !Item.Product.IsVat).Sum(Item => Math.Round((Item.Product.PriceExcVat * cart.DisCountRate) / 100, 2, MidpointRounding.AwayFromZero) * Item.Quantity);
				cart.TotalDeliveryCharge = cart.ItemCart.Sum(Item => Item.Product.TransCharge * Item.Quantity);
				cart.ItemCountCart = cart.ItemCart.Sum(Item => Item.Quantity);
			}
		}

		public int ItemCountQty
		{
			get
			{
				int? scid = ShoppingCartId;
				if (!scid.HasValue) { return 0; }
				using (SqlConnection connection = new SqlConnection(_connectionString))
				{
					connection.Open();
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandText = "Select CountProduct From TBCart Where SCID = @SCID and CompanyID = @CompanyID";
						command.Parameters.AddWithValue("@SCID", scid.Value);
						command.Parameters.AddWithValue("@CompanyID", _user.CurrentCompanyId);
						return (int)command.ExecuteScalar();
					}
				}
			}
		}

		public int ItemCount
		{
			get
			{
				int? scid = ShoppingCartId;
				if (!scid.HasValue) { return 0; }
				using (SqlConnection connection = new SqlConnection(_connectionString))
				{
					connection.Open();
					using (SqlCommand command = connection.CreateCommand())
					{
						command.CommandText = "select count(t2.pid) from  TBCart t1 inner JOIN TBCartDetail t2 ON t1.SCID = t2.SCID AND t1.RefOrderID = t2.RefOrderID WHERE t1.SCID	 = @SCID and CompanyID = @CompanyID";
						command.Parameters.AddWithValue("@SCID", scid.Value);
						command.Parameters.AddWithValue("@CompanyID", _user.CurrentCompanyId);
						return (int)command.ExecuteScalar();
					}
				}
			}
		}

		public void AddToCart(string productId, int quantity)
		{
			IDictionary<string, int> cart = LoadCart();
			if (cart.ContainsKey(productId))
			{
				cart[productId] += quantity;
			}
			else
			{
				cart.Add(productId, quantity);
			}
			SaveCart(cart);
		}

		public Cart LoadMyCart()
		{
			Cart ShoppingCart = new Cart();
			int? scid = ShoppingCartId;
			ShoppingCart = LoadCartDetail();

			if (scid.HasValue) { ShoppingCart.ShoppingCartId = scid.Value; }
			if (ShoppingCart != null)
			{
				CartCalulate(ShoppingCart);

				//หาว่า มีสินค้าตัวใดบ้างมาจาก OFM Catalog
				if (ShoppingCart.ItemCart.Where(o => o.IsOfmCatalog).Any())
				{
					ShoppingCart.SendToAdminAppProd = true;
				}
			}
			return ShoppingCart;
		}

		public void SaveCart(IDictionary<string, int> cart)
		{
			int? scid = ShoppingCartId;
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				if (!scid.HasValue)
				{
					scid = new ShoppingCartRepository(connection).CreateCart(_user, _request.IPAddress());
					ShoppingCartId = scid;
				}
				Cart ShoppingCart = new Cart();
				ShoppingCart = LoadCartType(ShoppingCart);
				if (!ShoppingCart.Type.Equals(Cart.CartType.Revise))
				{
					new ShoppingCartRepository(connection).SaveCart(cart, scid.Value, _user, _request.IPAddress());
				}
				else if (ShoppingCart.Type.Equals(Cart.CartType.Revise) && !string.IsNullOrEmpty(ShoppingCart.RefOrderId))
				{
					new ShoppingCartRepository(connection).SaveCartRevise(cart, scid.Value, _user, ShoppingCart.RefOrderId, _request.IPAddress());
				}
			}
		}

		public void SaveCartRevise(IDictionary<string, int> cartRevise, string refOrderId)
		{
			int? scid = ShoppingCartId;
			using (SqlConnection connection = new SqlConnection(_connectionString))
			{
				if (!scid.HasValue)
				{
					scid = new ShoppingCartRepository(connection).CreateCartRevise(_user, refOrderId, _request.IPAddress());
					ShoppingCartId = scid;
				}
				Cart ShoppingCart = new Cart();
				ShoppingCart = LoadCartType(ShoppingCart);
				if (ShoppingCart.Type.Equals(Cart.CartType.Revise))
				{
					new ShoppingCartRepository(connection).SaveCartRevise(cartRevise, scid.Value, _user, refOrderId, _request.IPAddress());
				}
			}
		}

	}
}