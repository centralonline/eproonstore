﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers.Mails
{
	public class MailTemplateEngine : WebFormViewEngine
	{
		static MailTemplateEngine()
		{
			_instance = new MailTemplateEngine();
		}

		protected MailTemplateEngine()
		{
			MasterLocationFormats = new string[] { "~/MailTemplates/{0}.master" };
			AreaMasterLocationFormats = new string[] { "~/Areas/{2}/MailTemplates/{0}.master" };
			ViewLocationFormats = new string[] { "~/MailTemplates/{0}.aspx", "~/MailTemplates/{0}.ascx" };
			AreaViewLocationFormats = new string[] { "~/Areas/{2}/MailTemplates/{0}.aspx", "~/Areas/{2}/MailTemplates/{0}.ascx" };
			PartialViewLocationFormats = base.ViewLocationFormats;
			AreaPartialViewLocationFormats = base.AreaViewLocationFormats;
		}

		private static MailTemplateEngine _instance;
		public static MailTemplateEngine Instance
		{
			get
			{
				if (_instance == null)
				{
					_instance = new MailTemplateEngine();
				}
				return _instance;
			}
		}

		public static IView FindView(ControllerContext controllerContext, string templateName)
		{
			ViewEngineResult result;
			result = Instance.FindView(controllerContext, templateName, null, true);
			if (result.View != null) { return result.View; }
			result = Instance.FindView(controllerContext, templateName, null, false);
			if (result.View != null) { return result.View; }
			throw new InvalidOperationException(String.Format("MailTemplate {0} was not found. The following locations were searched:{1}{2}", templateName, Environment.NewLine, String.Join(Environment.NewLine, result.SearchedLocations.ToArray())));
		}
	}
}