﻿using System;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;

namespace Eprocurement2012.Controllers.Mails
{
	public class ApproverDeleteOrderMail : AbstractMailTemplate
	{
		public ApproverDeleteOrderMail(ControllerContext controllerContext, OrderData orderData)
		{
			To.Add(orderData.Order.Requester.UserId);
			CC.Add(orderData.Order.PreviousApprover.Approver.UserId);
			Bcc.Add(AddressBook.OrderTransEmail);
			Bcc.Add(AddressBook.OrderGmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.ApproverDeleteOrderMail.Subject"), orderData.Order.OrderID);
			GenerateAlternateView(controllerContext, "ApproverDeleteOrderMail.Alternate", orderData);
		}
	}
}
