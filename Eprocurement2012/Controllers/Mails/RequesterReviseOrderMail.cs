﻿using System;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class RequesterReviseOrderMail : AbstractMailTemplate
	{
		public RequesterReviseOrderMail(ControllerContext controllerContext, OrderData orderData)
		{
			To.Add(orderData.Order.CurrentApprover.Approver.UserId);
			CC.Add(orderData.Order.Contact.Email);
			Bcc.Add(AddressBook.OrderTransEmail);
			Bcc.Add(AddressBook.OrderGmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.RequesterReviseOrderMail.Subject"), orderData.Order.OrderID);
			GenerateAlternateView(controllerContext, "RequesterReviseOrderMail.Alternate", orderData);
		}
	}
}
