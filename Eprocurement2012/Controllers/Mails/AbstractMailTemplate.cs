﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.Mvc;
using System.IO;
using System.Net.Mime;
using System.Text;
using System.Data.SqlClient;

namespace Eprocurement2012.Controllers.Mails
{
	public class AbstractMailTemplate : MailMessage
	{
		public AbstractMailTemplate()
		{
			BodyEncoding = Encoding.UTF8;
			SubjectEncoding = Encoding.UTF8;
		}
		private string GenerateContent(ControllerContext controllerContext, string templateName, object model)
		{
			IView view = MailTemplateEngine.FindView(controllerContext, templateName);
			StringWriter writer = new StringWriter();
			view.Render(new ViewContext(controllerContext, view, new ViewDataDictionary(model), new TempDataDictionary(), writer), writer);
			return writer.ToString();
		}
		protected void GeneratePlainBody(ControllerContext controllerContext, string templateName)
		{
			GeneratePlainBody(controllerContext, templateName, null);
		}
		protected void GeneratePlainBody(ControllerContext controllerContext, string templateName, object model)
		{
			Body = GenerateContent(controllerContext, templateName, model);
		}
		protected string GenerateBody(ControllerContext controllerContext, string templateName, object model)
		{
			Body = GenerateContent(controllerContext, templateName, model);
			return Body;
		}
		protected void GenerateAlternateView(ControllerContext controllerContext, string templateName)
		{
			GenerateAlternateView(controllerContext, templateName, null);
		}
		/* comment ไว้ก่อน ถ้า error ไม่หาย ค่อยแก้ใหม่
		private string GenerateContent(ControllerContext controllerContext, string templateName, object model)
		{
			IView view = MailTemplateEngine.FindView(controllerContext, templateName);
			StringWriter writer = new StringWriter();
			ViewEngineCollection oldCollection = new ViewEngineCollection();
			foreach (var engine in ViewEngines.Engines)
			{
				oldCollection.Add(engine);
			}
			try
			{
				ViewEngines.Engines.Clear();
				ViewEngines.Engines.Add(MailTemplateEngine.Instance);
				view.Render(new ViewContext(controllerContext, view, new ViewDataDictionary(model), new TempDataDictionary(), writer), writer);
			}
			finally
			{
				ViewEngines.Engines.Clear();
				foreach (var engine in oldCollection)
				{
					ViewEngines.Engines.Add(engine);
				}
			}
			return writer.ToString();
		}
		*/
		protected void GenerateAlternateView(ControllerContext controllerContext, string templateName, object model)
		{
			AlternateBody = GenerateContent(controllerContext, templateName, model);
			AlternateView alternateView = AlternateView.CreateAlternateViewFromString(AlternateBody, Encoding.UTF8, MediaTypeNames.Text.Html);
			AlternateViews.Add(alternateView);
		}
		public string AlternateBody { get; private set; }
	}
}