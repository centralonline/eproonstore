﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Mail;
using System.Web.Mvc;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Mails;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class NotifyMeMailAlert : AbstractMailTemplate
	{
		public NotifyMeMailAlert(ControllerContext controllerContext, NotifyMeMailData mailData)
		{
			To.Add(mailData.NotifyEmail);
			From = new MailAddress(AddressBook.ContactEmail);
			Subject = new ResourceString("Mail.NotifyMeMailAlert.Subject") + mailData.ProductDetail.Name.ToString() + "(" + mailData.ProductDetail.Id + ")";
			GenerateAlternateView(controllerContext, "NotifyMeMailAlert.Alternate", mailData);
		}
	}
}