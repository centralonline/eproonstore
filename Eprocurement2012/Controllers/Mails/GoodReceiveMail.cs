﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Models;
using System.Web.Mvc;
using Eprocurement2012.Controllers.Helpers;
using System.Net.Mail;

namespace Eprocurement2012.Controllers.Mails
{
	public class GoodReceiveMail : AbstractMailTemplate
	{
		public GoodReceiveMail(ControllerContext controllerContext, GoodReceive goodReceive)
		{
			To.Add(goodReceive.AppUserId); //สร้าง
			//CC.Add(goodReceive.UserId); //คนรับ
			//Bcc.Add(AddressBook.OrderTransEmail);
			Bcc.Add(AddressBook.OrderGmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.GoodReceiveOrderMail.Subject"), goodReceive.Order.OrderID);
			GenerateAlternateView(controllerContext, "GoodReceiveOrderMail.Alternate", goodReceive);
		}
	}
}