﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class ContactUsSpecialProductMail : AbstractMailTemplate
	{
		public ContactUsSpecialProductMail(ControllerContext controllerContext, ContactUs contactUs)
		{
			To.Add(new MailAddress(contactUs.RootAdminId));
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.ContactUsSpecialProductMail.Subject"), contactUs.UserName);
			GenerateAlternateView(controllerContext, "ContactUsMail.Alternate", contactUs);
		}
	}
}