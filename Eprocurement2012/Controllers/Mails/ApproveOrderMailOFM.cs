﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class ApproveOrderMailOFM : AbstractMailTemplate
	{
		public ApproveOrderMailOFM(ControllerContext controllerContext, OrderData orderData)
		{
			To.Add(orderData.Order.Requester.UserId); //สร้าง
			CC.Add(orderData.Order.PreviousApprover.Approver.UserId); //คนรับ
			//Bcc.Add("eprointernalofm@Officemate.co.th");
			//Bcc.Add("surachart@officemate.int");
			Bcc.Add(AddressBook.OrderGmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.ApproveOrderMail.Subject"), orderData.Order.OrderID);
			GenerateAlternateView(controllerContext, "ApproveOrderMail.Alternate", orderData);
		}
	}
}