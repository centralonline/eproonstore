﻿using System;
using System.Web.Mvc;
using System.Net.Mail;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;

namespace Eprocurement2012.Controllers.Mails
{
	public class ApproveOrderMail : AbstractMailTemplate
	{
		public ApproveOrderMail(ControllerContext controllerContext, OrderData orderData)
		{
			To.Add(orderData.Order.Requester.UserId); //สร้าง
			CC.Add(orderData.Order.PreviousApprover.Approver.UserId); //คนรับ
			Bcc.Add(AddressBook.OrderTransEmail);
			Bcc.Add(AddressBook.OrderGmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.ApproveOrderMail.Subject"), orderData.Order.OrderID);
			GenerateAlternateView(controllerContext, "ApproveOrderMail.Alternate", orderData);
		}
	}
}
