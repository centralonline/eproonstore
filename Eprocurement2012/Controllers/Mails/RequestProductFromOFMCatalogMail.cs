﻿using System;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class RequestProductFromOFMCatalogMail : AbstractMailTemplate
	{
		public RequestProductFromOFMCatalogMail(ControllerContext controllerContext, ContactUs contactUs)
		{
			To.Add(new MailAddress(AddressBook.ContactEmail));
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.ContactUsMail.Subject"), contactUs.ContactTitle);
			GenerateAlternateView(controllerContext, "ContactUsMail.Alternate", contactUs);
		}
	}
}
