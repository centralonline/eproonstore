﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class AutoApproveOrderMail : AbstractMailTemplate
	{
		public AutoApproveOrderMail(ControllerContext controllerContext, OrderData orderData)
		{
			To.Add(orderData.Order.Contact.Email);
			Bcc.Add(AddressBook.OrderTransEmail);
			Bcc.Add(AddressBook.OrderGmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.AutoApproveOrderMail.Subject"), orderData.Order.OrderID);
			GenerateAlternateView(controllerContext, "AutoApproveOrderMail.Alternate", orderData);
		}
	}
}
