﻿using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;

namespace Eprocurement2012.Controllers.Mails
{
	public class ForgotPasswordMail : AbstractMailTemplate
	{
		public ForgotPasswordMail(ControllerContext controllerContext, ForgotPasswordData forgotPasswordData)
		{
			To.Add(forgotPasswordData.UserId);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = new ResourceString("Mail.ForgotPasswordMail.Subject");
			GenerateAlternateView(controllerContext, "ForgotPasswordMail.Alternate", forgotPasswordData);
		}
	}
}
