﻿using System;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class RequesterDeleteOrderMail : AbstractMailTemplate
	{
		public RequesterDeleteOrderMail(ControllerContext controllerContext, OrderData orderData)
		{
			To.Add(orderData.Order.CurrentApprover.Approver.UserId);
			Bcc.Add(AddressBook.OrderTransEmail);
			Bcc.Add(AddressBook.OrderGmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.RequesterDeleteOrderMail.Subject"), orderData.Order.OrderID);
			GenerateAlternateView(controllerContext, "RequesterDeleteOrderMail.Alternate", orderData);
		}
	}
}
