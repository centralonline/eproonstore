﻿using System.Collections.Generic;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class ActivateSiteMail : AbstractMailTemplate
	{
		public ActivateSiteMail(ControllerContext controllerContext, List<string> allUsers)
		{
			foreach (var item in allUsers)
			{
				To.Add(item);
			}
			CC.Add(AddressBook.ContactEmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = new ResourceString("Mail.ActivateSiteMail.Subject");
			GenerateAlternateView(controllerContext, "ActivateSiteMail.Alternate", null);
		}
	}
}
