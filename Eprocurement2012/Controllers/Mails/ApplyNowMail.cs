﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class ApplyNowMail : AbstractMailTemplate
	{
		public ApplyNowMail(ControllerContext controllerContext, ApplyNow applyNow)
		{
			To.Add(new MailAddress(AddressBook.ContactEmail));
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.ApplyNowMail.Subject"), applyNow.CompanyName);
			GenerateAlternateView(controllerContext, "ApplyNowMail.Alternate", applyNow);
		}
	}
}
