﻿using System.Collections.Generic;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers.Mails
{
	public class CloseSiteMail : AbstractMailTemplate
	{
		public CloseSiteMail(ControllerContext controllerContext, List<string> allUsers, Site site)
		{
			foreach (var item in allUsers)
			{
				To.Add(item);
			}
			CC.Add(AddressBook.ContactEmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = new ResourceString("Mail.CloseSiteMail.Subject");
			GenerateAlternateView(controllerContext, "CloseSiteMail.Alternate", site);
		}
	}
}
