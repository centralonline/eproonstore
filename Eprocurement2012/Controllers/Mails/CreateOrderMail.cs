﻿using System;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;
using Eprocurement2012.Controllers.Helpers;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;

namespace Eprocurement2012.Controllers.Mails
{
	public class CreateOrderMail : AbstractMailTemplate 
	{

		public CreateOrderMail(ControllerContext controllerContext, OrderData orderData)
		{
			To.Add(orderData.Order.CurrentApprover.Approver.UserId); //คนรับ
			CC.Add(orderData.Order.Contact.Email);//คนสร้าง
			Bcc.Add(AddressBook.OrderTransEmail);
			Bcc.Add(AddressBook.OrderGmail);
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format(new ResourceString("Mail.CreateOrderMail.Subject"), orderData.Order.OrderID);
			GenerateAlternateView(controllerContext, "CreateOrderMail.Alternate", orderData);
		}
	}
}
