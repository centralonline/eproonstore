﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Net.Mail;

namespace Eprocurement2012.Controllers.Mails
{
    public class CreateNewSiteMail : AbstractMailTemplate
	{
		public CreateNewSiteMail(ControllerContext controllerContext, NewSiteData data)
		{
			To.Add(new MailAddress(AddressBook.ContactEmail));
			From = new MailAddress(AddressBook.ContactEmail, "OfficeMate e-Procurement");
			Subject = String.Format("Officemate Create New Site CompanyID :{0}", data.CompanyId);
			GenerateAlternateView(controllerContext, "CreateNewSiteMail.Alternate", data);
		}
	}
}
