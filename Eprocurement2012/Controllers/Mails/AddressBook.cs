﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.Net.Mail;

namespace Eprocurement2012.Controllers.Mails
{
	public class AddressBook
	{
		public static string StoreManagerEmail
		{
			get
			{
				if (IsProduction)
				{
					return WebConfigurationManager.AppSettings["ProductionStoreManagerEmail"];
				}
				else
				{
					return WebConfigurationManager.AppSettings["DevelopmentStoreManagerEmail"];
				}
			}
		}

		public static string ContactEmail
		{
			get
			{
				if (IsProduction)
				{
					return WebConfigurationManager.AppSettings["ProductionContactEprocurementEmail"];
				}
				else
				{
					return WebConfigurationManager.AppSettings["DevelopmentContactEprocurementEmail"];
				}
			}
		}

		public static string AdminEmail
		{
			get
			{
				if (IsProduction)
				{
					return WebConfigurationManager.AppSettings["ProductionAdminEmail"];
				}
				else
				{
					return WebConfigurationManager.AppSettings["DevelopmentAdminEmail"];
				}
			}
		}

		public static string MonitorServer
		{
			get
			{
				if (IsProduction)
				{
					return WebConfigurationManager.AppSettings["ProductionMonitorserverEmail"];
				}
				else
				{
					return WebConfigurationManager.AppSettings["DevelopmentMonitorserverEmail"];
				}
			}
		}

		public static string OrderTransEmail
		{
			get
			{
				if (IsProduction)
				{
					return WebConfigurationManager.AppSettings["ProductionOrderTransEmail"];
				}
				else
				{
					return WebConfigurationManager.AppSettings["DevelopmentOrderTransEmail"];
				}
			}
		}

		public static string OrderGmail
		{
			get
			{
				if (IsProduction)
				{
					return WebConfigurationManager.AppSettings["ProductionOrderGmail"];
				}
				else
				{
					return WebConfigurationManager.AppSettings["DevelopmentOrderGmail"];
				}
			}
		}

		public static MailAddress NotifyMeMailAlert
		{
			get
			{
				if (IsProduction)
				{
					return new MailAddress(WebConfigurationManager.AppSettings["ProductionNotifyMeMailAdminAlert"]);
				}
				else
				{
					return new MailAddress(WebConfigurationManager.AppSettings["DevelopmentNotifyMeMailAdminAlert"]);
				}
			}
		}

		public static bool IsProduction
		{
			get { return Boolean.Parse(WebConfigurationManager.AppSettings["IsProduction"]); }
		}
	}
}