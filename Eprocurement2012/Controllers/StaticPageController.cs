﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Controllers
{
	public class StaticPageController : AbstractEproController
	{
		public ActionResult Index(string viewName)
		{
			return View(viewName);
		}
	}
}
