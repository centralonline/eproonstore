﻿using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
using System.Data.SqlClient;
using Eprocurement2012.Controllers.Helpers;
using System;

namespace Eprocurement2012.Controllers
{
	[HandleError]
	public class ReportController : AuthenticateRoleController
	{

		public ActionResult Index()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				TrackOrders trackOrders = new TrackOrders();
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				IEnumerable<User> listUser = new UserRepository(connection).GetAllUserReqOrApp(User.CurrentCompanyId);
				trackOrders.UserRequester = listUser.Where(u => u.UserRoleName == User.UserRole.Requester);
				trackOrders.UserApprover = listUser.Where(u => u.UserRoleName == User.UserRole.Approver);
				trackOrders.Costcenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId);
				return View("ConditionalOrderReport", trackOrders);
			}
		}

		public ActionResult ConditionalOrder(int? page, AdvanceSearchField advanceSearchField, string[] requester, string[] approver, string[] costcenter)
		{

			if (advanceSearchField.CheckOrderPrice && advanceSearchField.MinOrderPrice == null)
			{
				advanceSearchField.MinOrderPrice = "0";
			}
			if (advanceSearchField.CheckOrderPrice && advanceSearchField.MaxOrderPrice == null)
			{
				advanceSearchField.MaxOrderPrice = "0";
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);

				TrackOrders trackOrders = new TrackOrders();
				trackOrders.ListOrder = new ListOrder();
				trackOrders.PageNumber = page.HasValue ? page.Value : 1;

				IEnumerable<User> listUser = new UserRepository(connection).GetAllUserReqOrApp(User.CurrentCompanyId);
				trackOrders.UserRequester = listUser.Where(u => u.UserRoleName == User.UserRole.Requester);
				trackOrders.UserApprover = listUser.Where(u => u.UserRoleName == User.UserRole.Approver);
				trackOrders.Costcenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId);

				advanceSearchField.Requester = requester != null ? requester : advanceSearchField.Requester;
				advanceSearchField.Approver = approver != null ? approver : advanceSearchField.Approver;
				advanceSearchField.Costcenter = costcenter != null ? costcenter : advanceSearchField.Costcenter;
				trackOrders.AdvanceSearchField = advanceSearchField;

				trackOrders.TotalOrdersCount = reportRepo.GetCountConditionalOrder(advanceSearchField, User.CurrentCompanyId, "", true);
				trackOrders.ListOrder.Orders = reportRepo.GetConditionalOrder(trackOrders.PageSize, trackOrders.OrderIndex, advanceSearchField, User.CurrentCompanyId, "", true);
				trackOrders.ListOrder.ItemOrders = new List<OrderDetail>();

				foreach (var order in trackOrders.ListOrder.Orders)
				{
					trackOrders.ListOrder.ItemOrders = trackOrders.ListOrder.ItemOrders.Concat(new OrderRepository(connection).GetOrderDetailBySearch(order.OrderID));
				}

				return View("ConditionalOrderReport", trackOrders);
			}
		}

		public ActionResult ConditionalOrderPaging(int? page, AdvanceSearchField advanceSearchField)
		{
			return ConditionalOrder(page, advanceSearchField, advanceSearchField.Requester, advanceSearchField.Approver, advanceSearchField.Costcenter);
		}

		public ActionResult OnProgressOrder(int? page, DateFilter? dateFilter)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;
			trackOrders.DateFilter = dateFilter.HasValue ? dateFilter.Value : DateFilter.Today;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountTrackOrderByStatus(trackOrders.DateFilter, Order.OrderStatus.Waiting, User.CurrentCompanyId, "", true);
				trackOrders.ListOrder.Orders = reportRepo.GetTrackOrderByStatus(trackOrders.PageSize, trackOrders.OrderIndex, trackOrders.DateFilter, Order.OrderStatus.Waiting, User.CurrentCompanyId, "", true);
				return View("OnProgressOrderReport", trackOrders);
			}
		}

		public ActionResult SearchOnProgressOrder(AdvanceSearchField advanceSearchField)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = 1;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = advanceSearchField;
				if (advanceSearchField.CheckOrderDate || advanceSearchField.CheckOrderId || advanceSearchField.CheckUserId)
				{
					trackOrders.ListOrder.Orders = reportRepo.GetSearchOrderByStatus(advanceSearchField, Order.OrderStatus.Waiting, User.CurrentCompanyId);
					trackOrders.TotalOrdersCount = trackOrders.ListOrder.Orders.Count();
				}
				else
				{
					TempData["Message"] = new ResourceString("Report.Search.Error");
				}
				return View("OnProgressOrderReport", trackOrders);
			}
		}

		public ActionResult CompleteOrder(int? page, DateFilter? dateFilter)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;
			trackOrders.DateFilter = dateFilter.HasValue ? dateFilter.Value : DateFilter.Today;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountTrackOrderByStatus(trackOrders.DateFilter, Order.OrderStatus.Approved, User.CurrentCompanyId, "", true);
				trackOrders.ListOrder.Orders = reportRepo.GetTrackOrderByStatus(trackOrders.PageSize, trackOrders.OrderIndex, trackOrders.DateFilter, Order.OrderStatus.Approved, User.CurrentCompanyId, "", true);
				return View("CompleteOrderReport", trackOrders);
			}
		}

		public ActionResult SearchCompleteOrder(AdvanceSearchField advanceSearchField)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = 1;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = advanceSearchField;
				if (advanceSearchField.CheckOrderDate || advanceSearchField.CheckOrderId || advanceSearchField.CheckUserId)
				{
					trackOrders.ListOrder.Orders = reportRepo.GetSearchOrderByStatus(advanceSearchField, Order.OrderStatus.Approved, User.CurrentCompanyId);
					trackOrders.TotalOrdersCount = trackOrders.ListOrder.Orders.Count();
				}
				else
				{
					TempData["Message"] = new ResourceString("Report.Search.Error");
				}
				return View("CompleteOrderReport", trackOrders);
			}
		}

		public ActionResult ExpireOrder(int? page, DateFilter? dateFilter)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;
			trackOrders.DateFilter = dateFilter.HasValue ? dateFilter.Value : DateFilter.Today;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountTrackOrderByStatus(trackOrders.DateFilter, Order.OrderStatus.Expired, User.CurrentCompanyId, "", true);
				trackOrders.ListOrder.Orders = reportRepo.GetTrackOrderByStatus(trackOrders.PageSize, trackOrders.OrderIndex, trackOrders.DateFilter, Order.OrderStatus.Expired, User.CurrentCompanyId, "", true);
				return View("ExpireOrderReport", trackOrders);
			}
		}

		public ActionResult SearchExpireOrder(AdvanceSearchField advanceSearchField)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = 1;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = advanceSearchField;
				if (advanceSearchField.CheckOrderDate || advanceSearchField.CheckOrderId || advanceSearchField.CheckUserId)
				{
					trackOrders.ListOrder.Orders = reportRepo.GetSearchOrderByStatus(advanceSearchField, Order.OrderStatus.Expired, User.CurrentCompanyId);
					trackOrders.TotalOrdersCount = trackOrders.ListOrder.Orders.Count();
				}
				else
				{
					TempData["Message"] = new ResourceString("Report.Search.Error");
				}
				return View("ExpireOrderReport", trackOrders);
			}
		}

		public ActionResult OrderHistory(int? page, MonthFilter? monthFilter)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;
			trackOrders.MonthFilter = monthFilter.HasValue ? monthFilter.Value : MonthFilter.SevenDaysLater;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountOrderHistory(trackOrders.MonthFilter, User.CurrentCompanyId, "", true);
				trackOrders.ListOrder.Orders = reportRepo.GetOrderHistory(trackOrders.PageSize, trackOrders.OrderIndex, trackOrders.MonthFilter, User.CurrentCompanyId, "", true);
				return View("OrderHistoryReport", trackOrders);
			}
		}

		public ActionResult SearchOrderHistory(AdvanceSearchField advanceSearchField)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = 1;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = advanceSearchField;
				if (advanceSearchField.CheckOrderDate || advanceSearchField.CheckOrderId || advanceSearchField.CheckUserId)
				{
					trackOrders.ListOrder.Orders = reportRepo.GetSearchOrderHistory(advanceSearchField, User.CurrentCompanyId, "", true);
					trackOrders.TotalOrdersCount = trackOrders.ListOrder.Orders.Count();
				}
				else
				{
					TempData["Message"] = new ResourceString("Report.Search.Error");
				}
				return View("OrderHistoryReport", trackOrders);
			}
		}

		public ActionResult CostcenterOrder(RankFilter? rankFilter)
		{
			TrackCostCenter trackCostCenter = new TrackCostCenter();
			trackCostCenter.RankFilter = rankFilter.HasValue ? rankFilter.Value : RankFilter.TopTen;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				trackCostCenter.ListSummaryOrder = new ReportRepository(connection).GetSummaryOrderCostCenter(trackCostCenter, User.CurrentCompanyId);
				return View("CostcenterOrderReport", trackCostCenter);
			}
		}

		public ActionResult SearchCostcenterOrder(TrackCostCenter trackCostCenter, int[] months, ImageInputData action)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackCostCenter.Months = months == null ? trackCostCenter.Months : months;
				if (action.Name == "CheckDetail")
				{
					trackCostCenter.CostcenterId = action.Value;
					trackCostCenter.ListOrder = new ListOrder();
					trackCostCenter.ListOrder.Orders = reportRepo.GetListOrderFromSummaryOrderCostCenter(trackCostCenter, User.CurrentCompanyId);
					return View("ListOrderFromSummaryOrder", trackCostCenter);
				}
				else
				{
					if (trackCostCenter.Quarters1 || trackCostCenter.Quarters2 || trackCostCenter.Quarters3 || trackCostCenter.Quarters4 || trackCostCenter.Months.Any())
					{
						trackCostCenter.ListSummaryOrder = reportRepo.GetSummaryOrderCostCenter(trackCostCenter, User.CurrentCompanyId);
					}
					else
					{
						TempData["Message"] = new ResourceString("Report.Search.Error");
					}
					return View("CostcenterOrderReport", trackCostCenter);
				}
			}
		}

		[HttpGet]
		public ActionResult ProductSummary()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				TrackOrders trackOrders = new TrackOrders();
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.Costcenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId);
				trackOrders.CompanyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(User.CurrentCompanyId);
				return View("ProductSummary", trackOrders);
			}
		}

		[HttpPost]
		public ActionResult ProductSummary(AdvanceSearchField advanceSearchField, string[] department, string[] costcenter)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				TrackOrders trackOrders = new TrackOrders();

				advanceSearchField.Costcenter = costcenter != null ? costcenter : advanceSearchField.Costcenter;
				advanceSearchField.Department = department != null ? department : advanceSearchField.Department;
				trackOrders.AdvanceSearchField = advanceSearchField;

				trackOrders.Costcenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId);
				trackOrders.CompanyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(User.CurrentCompanyId);
				trackOrders.ProductSummary = new ReportRepository(connection).GetProductSummary(advanceSearchField, User.CurrentCompanyId, User.UserId, true);
				return View("ProductSummary", trackOrders);
			}
		}

		public ActionResult PurchaseIndex()
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.ListOrder = new ListOrder();
			trackOrders.AdvanceSearchField = new AdvanceSearchField();
			return View("Purchase.ConditionalOrderReport", trackOrders);
		}

		public ActionResult PurchaseConditionalOrder(int? page, AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				TrackOrders trackOrders = new TrackOrders();
				trackOrders.ListOrder = new ListOrder();
				trackOrders.PageNumber = page.HasValue ? page.Value : 1;
				trackOrders.AdvanceSearchField = advanceSearchField;

				trackOrders.TotalOrdersCount = reportRepo.GetCountConditionalOrder(advanceSearchField, User.CurrentCompanyId, User.UserId, false);
				trackOrders.ListOrder.Orders = reportRepo.GetConditionalOrder(trackOrders.PageSize, trackOrders.OrderIndex, advanceSearchField, User.CurrentCompanyId, User.UserId, false);

				trackOrders.ListOrder.ItemOrders = new List<OrderDetail>();

				foreach (var order in trackOrders.ListOrder.Orders)
				{
					trackOrders.ListOrder.ItemOrders = trackOrders.ListOrder.ItemOrders.Concat(new OrderRepository(connection).GetOrderDetailBySearch(order.OrderID));
				}

				return View("Purchase.ConditionalOrderReport", trackOrders);
			}
		}

		public ActionResult PurchaseOnProgressOrder(int? page)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountTrackOrderByStatus(null, Order.OrderStatus.Waiting, User.CurrentCompanyId, User.UserId, false);
				trackOrders.ListOrder.Orders = reportRepo.GetTrackOrderByStatus(trackOrders.PageSize, trackOrders.OrderIndex, null, Order.OrderStatus.Waiting, User.CurrentCompanyId, User.UserId, false);
				return View("Purchase.OnProgressOrderReport", trackOrders);
			}
		}

		public ActionResult PurchaseCompleteOrder(int? page)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountTrackOrderByStatus(null, Order.OrderStatus.Approved, User.CurrentCompanyId, User.UserId, false);
				trackOrders.ListOrder.Orders = reportRepo.GetTrackOrderByStatus(trackOrders.PageSize, trackOrders.OrderIndex, null, Order.OrderStatus.Approved, User.CurrentCompanyId, User.UserId, false);
				return View("Purchase.CompleteOrderReport", trackOrders);
			}
		}

		public ActionResult PurchaseExpireOrder(int? page)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountTrackOrderByStatus(null, Order.OrderStatus.Expired, User.CurrentCompanyId, User.UserId, false);
				trackOrders.ListOrder.Orders = reportRepo.GetTrackOrderByStatus(trackOrders.PageSize, trackOrders.OrderIndex, null, Order.OrderStatus.Expired, User.CurrentCompanyId, User.UserId, false);
				return View("Purchase.ExpireOrderReport", trackOrders);
			}
		}

		public ActionResult PurchaseOrderHistory(int? page, MonthFilter? monthFilter)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;
			trackOrders.MonthFilter = monthFilter.HasValue ? monthFilter.Value : MonthFilter.SevenDaysLater;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountOrderHistory(trackOrders.MonthFilter, User.CurrentCompanyId, User.UserId, false);
				trackOrders.ListOrder.Orders = reportRepo.GetOrderHistory(trackOrders.PageSize, trackOrders.OrderIndex, trackOrders.MonthFilter, User.CurrentCompanyId, User.UserId, false);
				return View("Purchase.OrderHistoryReport", trackOrders);
			}
		}

		public ActionResult SearchPurchaseOrderHistory(AdvanceSearchField advanceSearchField)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = 1;

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = advanceSearchField;
				if (advanceSearchField.CheckOrderDate || advanceSearchField.CheckOrderId)
				{
					trackOrders.ListOrder.Orders = reportRepo.GetSearchOrderHistory(advanceSearchField, User.CurrentCompanyId, User.UserId, false);
					trackOrders.TotalOrdersCount = trackOrders.ListOrder.Orders.Count();
				}
				else
				{
					TempData["Message"] = new ResourceString("Report.Search.Error");
				}
				return View("Purchase.OrderHistoryReport", trackOrders);
			}
		}

		[HttpGet]
		public ActionResult PurchaseProductSummary()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				TrackOrders trackOrders = new TrackOrders();
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.DeptProduct = new DepartmentRepository(connection, User).GetChildrenAndGrandChildrenDepartments(0).Where(dept => dept.ParentId == 0);
				return View("Purchase.ProductSummary", trackOrders);
			}
		}

		[HttpPost]
		public ActionResult PurchaseProductSummary(AdvanceSearchField advanceSearchField, string[] deptProduct)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				TrackOrders trackOrders = new TrackOrders();
				advanceSearchField.DeptProduct = deptProduct != null ? deptProduct : advanceSearchField.DeptProduct;
				trackOrders.AdvanceSearchField = advanceSearchField;
				trackOrders.DeptProduct = new DepartmentRepository(connection, User).GetChildrenAndGrandChildrenDepartments(0).Where(dept => dept.ParentId == 0);
				trackOrders.ProductSummary = new ReportRepository(connection).GetProductSummary(advanceSearchField, User.CurrentCompanyId, User.UserId, false);
				return View("Purchase.ProductSummary", trackOrders);
			}
		}

		public ActionResult ViewOrderByStatus(Order.OrderStatus status, bool thisMonth)
		{
			AdvanceSearchField advanceSearchField = new AdvanceSearchField();
			switch (status)
			{
				case Order.OrderStatus.Waiting:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusWaiting = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.Approved:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusApproved = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.Partial:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusPartial = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.Revise:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusRevise = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.WaitingAdmin:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusWaitingAdmin = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.AdminAllow:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusAdminAllow = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.Shipped:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusShipped = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.Deleted:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusDeleted = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.Expired:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusExpired = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				case Order.OrderStatus.Completed:
					advanceSearchField.CheckOrderStatus = true;
					advanceSearchField.StatusCompleted = true;
					advanceSearchField.CheckOrderDate = true;
					if (thisMonth)
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, DateTime.Today.Day).ToString("d/M/yyyy");
					}
					else
					{
						advanceSearchField.OrderDateFrom = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1).ToString("d/M/yyyy");
						advanceSearchField.OrderDateTo = new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1).ToString("d/M/yyyy");
					}
					break;
				default:
					return Index(); //กันเคสที่ order status ไม่ถูกต้อง
			}
			return ConditionalOrder(1, advanceSearchField, null, null, null);
		}

		public ActionResult ReportBudget()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				ReportBudget budget = new ReportBudget();
				budget.Company = new CompanyRepository(connection).GetCompanyDetail(User.CurrentCompanyId);
				if (budget.Company.BudgetLevelType == Company.BudgetLevel.Company)
				{
					budget.Budgets = reportRepo.GetReportBudget(User.CurrentCompanyId, Company.BudgetLevel.Company);
				}
				else if (budget.Company.BudgetLevelType == Company.BudgetLevel.Department)
				{
					budget.Budgets = reportRepo.GetReportBudget(User.CurrentCompanyId, Company.BudgetLevel.Department);
				}
				else if (budget.Company.BudgetLevelType == Company.BudgetLevel.Costcenter)
				{
					budget.Budgets = reportRepo.GetReportBudget(User.CurrentCompanyId, Company.BudgetLevel.Costcenter);
				}
				else
				{

				}
				return View("ViewReportBudget", budget);
			}
		}

		public ActionResult ReportBudgetDetail(string groupId, string periodNo, string year)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				ReportBudget budget = new ReportBudget();
				budget.Company = new CompanyRepository(connection).GetCompanyDetail(User.CurrentCompanyId);
				budget.Budgets = reportRepo.GetReportBudgetDetail(User.CurrentCompanyId, groupId, periodNo, year);
				return View("ViewReportBudgetDetail", budget);
			}
		}

		public ActionResult ConditionalOrderReportPaging(int? page, bool checkAppDate, bool checkOrderDate, bool checkOrderStatus, bool checkApprover, bool checkCostcenter, bool checkOrderPrice, bool checkRequester, bool waiting, bool approved, bool partial, bool revise, bool waitingAdmin, bool adminAllow, bool shipped, bool deleted, bool expired, bool completed, string orderDateFrom, string orderDateTo, string appDateFrom, string appDateTo, string minOrderPrice, string maxOrderPrice, string[] requester, string[] approver, string[] costcenter)
		{
			AdvanceSearchField advanceSearchField = new AdvanceSearchField();
			advanceSearchField.StatusWaiting = waiting;
			advanceSearchField.StatusApproved = approved;
			advanceSearchField.StatusPartial = partial;
			advanceSearchField.StatusRevise = revise;
			advanceSearchField.StatusWaitingAdmin = waitingAdmin;
			advanceSearchField.StatusAdminAllow = adminAllow;
			advanceSearchField.StatusShipped = shipped;
			advanceSearchField.StatusDeleted = deleted;
			advanceSearchField.StatusExpired = expired;
			advanceSearchField.StatusCompleted = completed;
			advanceSearchField.OrderDateFrom = orderDateFrom;
			advanceSearchField.OrderDateTo = orderDateTo;
			advanceSearchField.AppDateFrom = appDateFrom;
			advanceSearchField.AppDateTo = appDateTo;
			advanceSearchField.MinOrderPrice = minOrderPrice;
			advanceSearchField.MaxOrderPrice = maxOrderPrice;
			advanceSearchField.CheckAppDate = checkAppDate;
			advanceSearchField.CheckOrderDate = checkOrderDate;
			advanceSearchField.CheckOrderStatus = checkOrderStatus;
			advanceSearchField.CheckApprover = checkApprover;
			advanceSearchField.CheckCostcenter = checkCostcenter;
			advanceSearchField.CheckOrderPrice = checkOrderPrice;
			advanceSearchField.CheckRequester = checkRequester;
			return ConditionalOrder(page, advanceSearchField, requester, approver, costcenter);
		}

		#region Good Receive Order
		public ActionResult PurchaseGoodReceiveOrder(int? page)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				trackOrders.TotalOrdersCount = reportRepo.GetCountTrackOrderByGoodReceive(null, Order.OrderStatus.Shipped, User.CurrentCompanyId, User.UserId, false);
				trackOrders.ListOrder.Orders = reportRepo.GetTrackOrderByGoodReceive(trackOrders.PageSize, trackOrders.OrderIndex, null, Order.OrderStatus.Shipped, User.CurrentCompanyId, User.UserId, false);
				return View(trackOrders);
			}
		}

		public ActionResult GoodReceiveOrder(int? page)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				TrackOrders trackOrders = new TrackOrders();
				trackOrders.ListOrder = new ListOrder();
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				IEnumerable<User> listUser = new UserRepository(connection).GetAllUserReqOrApp(User.CurrentCompanyId);
				trackOrders.UserRequester = listUser.Where(u => u.UserRoleName == User.UserRole.Requester);
				trackOrders.UserApprover = listUser.Where(u => u.UserRoleName == User.UserRole.Approver);
				trackOrders.Costcenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId);
				return View("GoodReceiveOrder", trackOrders);
			}
		}

		public ActionResult ConditionalGoodReceiveOrder(int? page, AdvanceSearchField advanceSearchField, string[] requester, string[] approver, string[] costcenter)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);

				TrackOrders trackOrders = new TrackOrders();
				trackOrders.ListOrder = new ListOrder();
				trackOrders.PageNumber = page.HasValue ? page.Value : 1;

				IEnumerable<User> listUser = new UserRepository(connection).GetAllUserReqOrApp(User.CurrentCompanyId);
				trackOrders.UserRequester = listUser.Where(u => u.UserRoleName == User.UserRole.Requester);
				trackOrders.UserApprover = listUser.Where(u => u.UserRoleName == User.UserRole.Approver);
				trackOrders.Costcenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId);

				advanceSearchField.Requester = requester != null ? requester : advanceSearchField.Requester;
				advanceSearchField.Approver = approver != null ? approver : advanceSearchField.Approver;
				advanceSearchField.Costcenter = costcenter != null ? costcenter : advanceSearchField.Costcenter;
				trackOrders.AdvanceSearchField = advanceSearchField;

				trackOrders.TotalOrdersCount = reportRepo.GetCountGoodReceiveOrder(advanceSearchField, User.CurrentCompanyId, "", true);
				trackOrders.ListOrder.Orders = reportRepo.GetGoodReceiveOrder(trackOrders.PageSize, trackOrders.OrderIndex, advanceSearchField, User.CurrentCompanyId, "", true);

				return View("GoodReceiveOrder", trackOrders);
			}
		}

		public ActionResult ConditionalGoodReceiveOrderPaging(int? page, bool checkAppDate, bool checkOrderDate, bool checkOrderStatus, bool checkApprover, bool checkCostcenter, bool checkRequester, bool waiting, bool partial, bool completed, string orderDateFrom, string orderDateTo, string appDateFrom, string appDateTo, string[] requester, string[] approver, string[] costcenter)
		{
			AdvanceSearchField advanceSearchField = new AdvanceSearchField();
			advanceSearchField.Waiting = waiting;
			advanceSearchField.Partial = partial;
			advanceSearchField.Completed = completed;
			advanceSearchField.OrderDateFrom = orderDateFrom;
			advanceSearchField.OrderDateTo = orderDateTo;
			advanceSearchField.AppDateFrom = appDateFrom;
			advanceSearchField.AppDateTo = appDateTo;
			advanceSearchField.CheckAppDate = checkAppDate;
			advanceSearchField.CheckOrderDate = checkOrderDate;
			advanceSearchField.CheckOrderStatus = checkOrderStatus;
			advanceSearchField.CheckApprover = checkApprover;
			advanceSearchField.CheckCostcenter = checkCostcenter;
			advanceSearchField.CheckRequester = checkRequester;
			return ConditionalGoodReceiveOrder(page, advanceSearchField, requester, approver, costcenter);
		}
		#endregion

		[HttpGet]
		public ActionResult ReportProductBySupplier()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				TrackOrders orders = new TrackOrders();
				orders.AdvanceSearchField = new AdvanceSearchField();
				orders.AdvanceSearchField.ListSuppliers = new ReportRepository(connection).GetSuppliers();
				orders.AdvanceSearchField.CompanyId = User.CurrentCompanyId;
				return View(orders);
			}
		}

		[HttpPost]
		public ActionResult ReportProductBySupplier(AdvanceSearchField advanceSearchField, string[] supplier,string companyId)
		{

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				TrackOrders orders = new TrackOrders();
				orders.AdvanceSearchField = new AdvanceSearchField();
				if (advanceSearchField.CheckSupplier || advanceSearchField.CheckOrderDate)
				{
					advanceSearchField.Supplier = supplier != null ? supplier : advanceSearchField.Supplier;
					orders.AdvanceSearchField = advanceSearchField;
					orders.AdvanceSearchField.ListProductSuppliers = new ReportRepository(connection).GetProductSupplier(advanceSearchField,companyId);
				}
				else
				{
					TempData["MessageError"] = "ไม่พบข้อมูล";
				}
				orders.AdvanceSearchField.ListSuppliers = new ReportRepository(connection).GetSuppliers();
				return View(orders);
			}
		}

	}
}
