﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using NPOI.HSSF.UserModel;
using OfficeMate.Framework.Formatting;
using System.IO;
namespace Eprocurement2012.Controllers
{
	public class SeminarController : AbstractEproController
	{

		[HttpGet]
		public ActionResult SignUp()
		{
			Seminar seminar = new Seminar();
			seminar.IsCustOFM = true;
			seminar.NewsChannel = "0";
			seminar.CustType = "";
			seminar.ListBusinessType = new UserRepository(new SqlConnection(ConnectionString)).GetBusinessDetail();
			return View(seminar);
		}
		[HttpPost]
		public ActionResult SignUp(Seminar seminar)
		{
			bool IsSave = true;
			decimal capital = 0;
			int custEmpNum = 0;

			if (string.IsNullOrEmpty(seminar.CustType))
			{
				TempData["ErrorCustType"] = "<div class='alert-box warning'><span>กรุณาเลือกประเภทลูกค้าค่ะ</span></div>";
				IsSave = false;
			}

			if (seminar.CustType == "Corporate")
			{
				if (string.IsNullOrEmpty(seminar.InvAddr1))
				{
					TempData["ErrorCompName"] = "<div class='alert-box warning'><span>กรุณาระบุชื่อบริษัทค่ะ</span></div>";
					IsSave = false;
				}
				if (string.IsNullOrEmpty(seminar.InvAddr2))
				{
					TempData["ErrorInvAddr"] = "<div class='alert-box warning'><span>กรุณาระบุที่อยู่ให้ครบทุกช่องค่ะ</span></div>";
					IsSave = false;
				}
				if (string.IsNullOrEmpty(seminar.PhoneNo))
				{
					TempData["ErrorPhoneNo"] = "<div class='alert-box warning'><span>กรุณาระบุหมายเลขติดต่อค่ะ</span></div>";
					IsSave = false;
				}
				if (seminar.IsCustOFM && string.IsNullOrEmpty(seminar.CustID))
				{
					TempData["ErrorCustID"] = "<div class='alert-box warning'><span>กรุณาระบุรหัสลูกค้าค่ะ</span></div>";
					IsSave = false;
				}
				if (seminar.CustEmpNum == "0" || !Int32.TryParse(seminar.CustEmpNum, out custEmpNum))
				{
					TempData["ErrorCustEmpNum"] = "<div class='alert-box warning'><span>กรุณาระบุจำนวนวพนักงานให้ถูกต้องด้วยค่ะ</span></div>";
					IsSave = false;
				}
				if (seminar.Capital == "0" || !Decimal.TryParse(seminar.Capital, out capital))
				{
					TempData["ErrorCapital"] = "<div class='alert-box warning'><span>กรุณาระบุทุนจดทะเบียนให้ถูกต้องด้วยค่ะ</span></div>";
					IsSave = false;
				}
				if (string.IsNullOrEmpty(seminar.BusinessType))
				{
					TempData["ErrorBusinessType"] = "<div class='alert-box warning'><span>กรุณาระบุประเภทธุรกิจด้วยค่ะ</span></div>";
					IsSave = false;
				}
			}

			if (string.IsNullOrEmpty(seminar.ContactName1))
			{
				TempData["ErrorContactName1"] = "<div class='alert-box warning'><span>กรุณาระบุชื่อ-นามสกุลค่ะ</span></div>";
				IsSave = false;
			}
			if (string.IsNullOrEmpty(seminar.ContactPosition1))
			{
				TempData["ErrorContactPosition1"] = "<div class='alert-box warning'><span>กรุณาระบุตำแหน่งค่ะ</span></div>";
				IsSave = false;
			}
			if (string.IsNullOrEmpty(seminar.ContactPhone1))
			{
				TempData["ErrorContactPhone1"] = "<div class='alert-box warning'><span>กรุณาระบุเบอร์ติดต่อค่ะ</span></div>";
				IsSave = false;
			}
			if (string.IsNullOrEmpty(seminar.ContactMobile1))
			{
				TempData["ErrorContactMobile1"] = "<div class='alert-box warning'><span>กรุณาระบุเบอร์ติดต่อค่ะ</span></div>";
				IsSave = false;
			}
			if (string.IsNullOrEmpty(seminar.ContactMail1))
			{
				TempData["ErrorContactMail1"] = "<div class='alert-box warning'><span>กรุณาระบุอีเมล์ค่ะ</span></div>";
				IsSave = false;
			}
			if (!string.IsNullOrEmpty(seminar.ContactMail1) && !IsValidEmail(seminar.ContactMail1))
			{
				TempData["ErrorFormatContactMail1"] = "<div class='alert-box warning'><span>กรุณาระบุรูปแบบอีเมล์ให้ถูกต้องค่ะ</span></div>";
				IsSave = false;
			}
			if (string.IsNullOrEmpty(seminar.NewsChannel))
			{
				TempData["ErrorNewsChannel"] = "<div class='alert-box warning'><span>กรุณาระบุช่องทางการได้รับข้อมูลของคุณค่ะ</span></div>";
				IsSave = false;
			}
			if (!string.IsNullOrEmpty(seminar.NewsChannel) && seminar.NewsChannel.Equals("4") && string.IsNullOrEmpty(seminar.NewsChannelOther))
			{
				TempData["ErrorNewsChannelOther"] = "<div class='alert-box warning'><span>กรุณาระบุช่องทางอื่นๆค่ะ</span></div>";
				IsSave = false;
			}
			if (seminar.HaveSystem && string.IsNullOrEmpty(seminar.ERPSystem))
			{
				TempData["ErrorERPSystem"] = "<div class='alert-box warning'><span>กรุณาชื่อระบบภายในของบริษัทคุณค่ะ</span></div>";
				IsSave = false;
			}
			if (!string.IsNullOrEmpty(seminar.ContactMail2) && !IsValidEmail(seminar.ContactMail2))
			{
				TempData["ErrorFormatContactMail2"] = "<div class='alert-box warning'><span>กรุณาระบุรูปแบบอีเมล์ให้ถูกต้องค่ะ</span></div>";
				IsSave = false;
			}

			if (IsSave)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					SeminarRepository simenarRepo = new SeminarRepository(connection);
					simenarRepo.SaveSeminarRegister(seminar, capital, custEmpNum);
					return RedirectToAction("SeminarSuccess");
				}
			}

			seminar.ListBusinessType = new UserRepository(new SqlConnection(ConnectionString)).GetBusinessDetail();
			return View(seminar);
		}
		[HttpGet]
		public ActionResult SeminarSuccess()
		{
			return View();
		}
		[HttpGet]
		public ActionResult InformationRegister()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				SeminarRepository simenarRepo = new SeminarRepository(connection);
				IEnumerable<Seminar> seminar = simenarRepo.GetSeminarRegister();
				return View(seminar);
			}
		}
		[HttpGet]
		public ActionResult ExportSeminarRegister()
		{
			IEnumerable<Seminar> seminar = new SeminarRepository(new SqlConnection(ConnectionString)).GetSeminarRegister();

			var workbook = new HSSFWorkbook();
			var sheet = workbook.CreateSheet("SeminarRegister");
			var rowIndex = 0;

			var row = sheet.CreateRow(rowIndex);
			row.CreateCell(0).SetCellValue("ลำดับ");
			row.CreateCell(1).SetCellValue("วันที่ลงทะเบียน");
			row.CreateCell(2).SetCellValue("ข้อมูลองค์กร");
			row.CreateCell(3).SetCellValue("");
			row.CreateCell(4).SetCellValue("");
			row.CreateCell(5).SetCellValue("");
			row.CreateCell(6).SetCellValue("");
			row.CreateCell(7).SetCellValue("");
			row.CreateCell(8).SetCellValue("");
			row.CreateCell(9).SetCellValue("");
			row.CreateCell(10).SetCellValue("");
			row.CreateCell(11).SetCellValue("");
			row.CreateCell(12).SetCellValue("ข้อมูลผู้เข้าร่วมสัมมนาท่านที่ 1");
			row.CreateCell(13).SetCellValue("");
			row.CreateCell(14).SetCellValue("");
			row.CreateCell(15).SetCellValue("");
			row.CreateCell(16).SetCellValue("ข้อมูลผู้เข้าร่วมสัมมนาท่านที่ 2");
			row.CreateCell(17).SetCellValue("");
			row.CreateCell(18).SetCellValue("");
			row.CreateCell(19).SetCellValue("");

			rowIndex++;
			row = sheet.CreateRow(rowIndex);
			row.CreateCell(0).SetCellValue("");
			row.CreateCell(1).SetCellValue("");
			row.CreateCell(2).SetCellValue("ประเภท");
			row.CreateCell(3).SetCellValue("ชื่อบริษัท [รหัสลูกค้า]");
			row.CreateCell(4).SetCellValue("ประเภทธุรกิจ");
			row.CreateCell(5).SetCellValue("ทุนจดทะเบียนบริษัท");
			row.CreateCell(6).SetCellValue("จำนวนพนักงาน");
			row.CreateCell(7).SetCellValue("ที่อยู่");
			row.CreateCell(8).SetCellValue("โทรศัพท์/FAX");
			row.CreateCell(9).SetCellValue("website");
			row.CreateCell(10).SetCellValue("ทราบข่าวจาก");
			row.CreateCell(11).SetCellValue("ERPSystem");
			row.CreateCell(12).SetCellValue("ชื่อ-นามสกุล");
			row.CreateCell(13).SetCellValue("ตำแหน่ง");
			row.CreateCell(14).SetCellValue("โทรศัพท์/mobile");
			row.CreateCell(15).SetCellValue("e-mail");
			row.CreateCell(16).SetCellValue("ชื่อ-นามสกุล");
			row.CreateCell(17).SetCellValue("ตำแหน่ง");
			row.CreateCell(18).SetCellValue("โทรศัพท์/mobile");
			row.CreateCell(19).SetCellValue("e-mail");

			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 0, 0));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 1, 1, 1));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 2, 11));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 12, 15));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 16, 19));

			foreach (var item in seminar)
			{
				rowIndex++;
				row = sheet.CreateRow(rowIndex);
				row.CreateCell(0).SetCellValue(rowIndex - 1);
				row.CreateCell(1).SetCellValue(item.CreateOn.ToString(new DateTimeFormat()));
				row.CreateCell(2).SetCellValue(item.CustType);
				row.CreateCell(3).SetCellValue(item.CompName + (!string.IsNullOrEmpty(item.CustID) ? "[" + item.CustID + "]" : ""));
				row.CreateCell(4).SetCellValue(item.BusinessType);
				row.CreateCell(5).SetCellValue(Convert.ToDouble(item.Capital));
				row.CreateCell(6).SetCellValue(item.CustEmpNum);
				row.CreateCell(7).SetCellValue(item.InvAddr2 + " " + item.InvAddr3 + " " + item.InvAddr4);
				row.CreateCell(8).SetCellValue(item.PhoneNo + " / " + item.FaxNo);
				row.CreateCell(9).SetCellValue(item.Website);
				row.CreateCell(10).SetCellValue(item.NewsChannel);
				row.CreateCell(11).SetCellValue(item.ERPSystem);
				row.CreateCell(12).SetCellValue(item.ContactName1);
				row.CreateCell(13).SetCellValue(item.ContactPosition1);
				row.CreateCell(14).SetCellValue(item.ContactPhone1 + " / " + item.ContactMobile1);
				row.CreateCell(15).SetCellValue(item.ContactMail1);
				row.CreateCell(16).SetCellValue(item.ContactName2);
				row.CreateCell(17).SetCellValue(item.ContactPosition2);
				row.CreateCell(18).SetCellValue(item.ContactPhone2 + " / " + item.ContactMobile2);
				row.CreateCell(19).SetCellValue(item.ContactMail2);
			}

			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "SeminarRegister.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
		}
		public bool IsValidEmail(string email)
		{
			try
			{
				var addr = new System.Net.Mail.MailAddress(email);
				return true;
			}
			catch
			{
				return false;
			}
		}
		public ActionResult SeminarContent()
		{
			return View();
		}

	}
}
