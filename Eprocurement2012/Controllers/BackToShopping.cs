﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Controllers
{
		[global::System.AttributeUsage(AttributeTargets.Method, Inherited = false, AllowMultiple = false)]
		public class BackToShoppingAttribute : Attribute { }
}