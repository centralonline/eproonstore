﻿using System.Collections.Generic;
using System.Web.Mvc;
using Eprocurement2012.Models;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Controllers.Helpers;
using OfficeMate.Framework.Formatting;
using System.IO;
using System.Linq;
using System.Data;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using CrystalDecisions.CrystalReports.Engine;
using System.Web.Hosting;
using CrystalDecisions.Shared;
using System;
using System.Web.Configuration;
using System.Globalization;


namespace Eprocurement2012.Controllers
{
	[HandleError]
	public class ExportExcelController : AuthenticateRoleController
	{

		public ActionResult ExportConditionalOrder(AdvanceSearchField advanceSearchField, ImageInputData action)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (action.Name == "ExportDetail")
				{
					ListOrder listOrder = new ListOrder();
					listOrder.ItemOrders = new List<OrderDetail>();
					listOrder.Orders = new ReportRepository(connection).GetConditionalOrderWithoutPaging(advanceSearchField, User.CurrentCompanyId, "", true);

					foreach (var order in listOrder.Orders)
					{
						listOrder.ItemOrders = listOrder.ItemOrders.Concat(new OrderRepository(connection).GetOrderDetailBySearch(order.OrderID));
					}
					return ExportOrderDetail(listOrder);
				}
				else
				{
					IEnumerable<Order> orders = new ReportRepository(connection).GetConditionalOrderWithoutPaging(advanceSearchField, User.CurrentCompanyId, "", true);
					return ExportOrder(orders);
				}
			}
		}

		public ActionResult ExportCompleteOrder(DateFilter? dateFilter, AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new List<Order>();
				if (dateFilter.HasValue)
				{
					orders = new ReportRepository(connection).GetTrackOrderByStatusWithoutPaging(dateFilter, Order.OrderStatus.Approved, User.CurrentCompanyId, "", true);
				}
				else
				{
					orders = new ReportRepository(connection).GetSearchOrderByStatus(advanceSearchField, Order.OrderStatus.Approved, User.CurrentCompanyId);
				}
				return ExportOrder(orders);
			}
		}

		public ActionResult ExportOnProgressOrder(DateFilter? dateFilter, AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new List<Order>();
				if (dateFilter.HasValue)
				{
					orders = new ReportRepository(connection).GetTrackOrderByStatusWithoutPaging(dateFilter, Order.OrderStatus.Waiting, User.CurrentCompanyId, "", true);
				}
				else
				{
					orders = new ReportRepository(connection).GetSearchOrderByStatus(advanceSearchField, Order.OrderStatus.Waiting, User.CurrentCompanyId);
				}
				return ExportOrder(orders);
			}
		}

		public ActionResult ExportExpireOrder(DateFilter? dateFilter, AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new List<Order>();
				if (dateFilter.HasValue)
				{
					orders = new ReportRepository(connection).GetTrackOrderByStatusWithoutPaging(dateFilter, Order.OrderStatus.Expired, User.CurrentCompanyId, "", true);
				}
				else
				{
					orders = new ReportRepository(connection).GetSearchOrderByStatus(advanceSearchField, Order.OrderStatus.Expired, User.CurrentCompanyId);
				}
				return ExportOrder(orders);
			}
		}

		public ActionResult ExportOrderHistory(MonthFilter? monthFilter, AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new List<Order>();
				if (monthFilter.HasValue)
				{
					orders = new ReportRepository(connection).GetOrderHistoryWithoutPaging(monthFilter, User.CurrentCompanyId, "", true);
				}
				else
				{
					orders = new ReportRepository(connection).GetSearchOrderHistory(advanceSearchField, User.CurrentCompanyId, "", true);
				}

				return ExportOrder(orders);
			}
		}

		private ActionResult ExportOrder(IEnumerable<Order> Orders)
		{
			//----------------Export to Excel-------------------
			var workbook = new HSSFWorkbook();
			var sheet = workbook.CreateSheet("dataOrder");
			var rowIndex = 0;
			var row = sheet.CreateRow(rowIndex);
			row.CreateCell(0).SetCellValue(new ResourceString("Order.ViewDataOrder.CustID"));
			row.CreateCell(1).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderId"));
			row.CreateCell(2).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderStatus"));
			row.CreateCell(3).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderDate"));
			row.CreateCell(4).SetCellValue(new ResourceString("Order.ViewDataOrder.ApprovrOrderDate"));
			row.CreateCell(5).SetCellValue(new ResourceString("Order.ViewOrder.RequesterName"));
			row.CreateCell(6).SetCellValue(new ResourceString("Order.ViewOrder.ApproverName"));
			row.CreateCell(7).SetCellValue(new ResourceString("Order.ViewOrder.CostCenterName"));
			row.CreateCell(8).SetCellValue(new ResourceString("Order.ConfirmOrder.Quantity"));
			row.CreateCell(9).SetCellValue(new ResourceString("Order.ConfirmOrder.GrandTotalAmt"));


			rowIndex++;
			foreach (var item in Orders)
			{
				row = sheet.CreateRow(rowIndex);
				row.CreateCell(0).SetCellValue(item.CustId);
				row.CreateCell(1).SetCellValue(item.OrderID);
				row.CreateCell(2).SetCellValue(new ResourceString("Eprocurement2012.Models.Order+OrderStatus." + item.Status.ToString()));
				row.CreateCell(3).SetCellValue(item.OrderDate.ToString(new DateTimeFormat()));
				row.CreateCell(4).SetCellValue(item.DisplayApproveOrderDate);
				row.CreateCell(5).SetCellValue(item.Requester.DisplayName);

				if (item.PreviousApprover != null)
				{
					row.CreateCell(6).SetCellValue(item.PreviousApprover.Approver.DisplayName);
				}
				else
				{
					row.CreateCell(6).SetCellValue(item.CurrentApprover.Approver.DisplayName);
				}
				row.CreateCell(7).SetCellValue(item.CostCenter.CostCenterName);
				row.CreateCell(8).SetCellValue(item.ItemCountOrder);
				row.CreateCell(9).SetCellValue(item.GrandTotalAmt.ToString(new MoneyFormat()));
				rowIndex++;
			}

			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "exportOrder.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		private ActionResult ExportOrderDetail(ListOrder listOrder)
		{
			//----------------Export to Excel-------------------
			var workbook = new HSSFWorkbook();
			var sheet = workbook.CreateSheet("OrderDetail");
			var rowIndex = 0;
			var row = sheet.CreateRow(rowIndex);
			row.CreateCell(0).SetCellValue(new ResourceString("Order.ViewDataOrder.CustID"));
			row.CreateCell(1).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderId"));
			row.CreateCell(2).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderStatus"));
			row.CreateCell(3).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderDate"));
			row.CreateCell(4).SetCellValue(new ResourceString("Order.ViewDataOrder.ApprovrOrderDate"));
			row.CreateCell(5).SetCellValue(new ResourceString("Order.ViewOrder.RequesterName"));
			row.CreateCell(6).SetCellValue(new ResourceString("Order.ViewOrder.ApproverName"));
			row.CreateCell(7).SetCellValue(new ResourceString("Order.ViewOrder.CostCenterName"));
			row.CreateCell(8).SetCellValue(new ResourceString("Order.ConfirmOrder.Quantity"));
			row.CreateCell(9).SetCellValue(new ResourceString("Order.ConfirmOrder.GrandTotalAmt"));


			rowIndex++;
			foreach (var item in listOrder.Orders)
			{
				row = sheet.CreateRow(rowIndex);
				row.CreateCell(0).SetCellValue(item.CustId);
				row.CreateCell(1).SetCellValue(item.OrderID);
				row.CreateCell(2).SetCellValue(new ResourceString("Eprocurement2012.Models.Order+OrderStatus." + item.Status.ToString()));
				row.CreateCell(3).SetCellValue(item.OrderDate.ToString(new DateTimeFormat()));
				row.CreateCell(4).SetCellValue(item.DisplayApproveOrderDate);
				row.CreateCell(5).SetCellValue(item.Requester.DisplayName);

				if (item.PreviousApprover != null)
				{
					row.CreateCell(6).SetCellValue(item.PreviousApprover.Approver.DisplayName);
				}
				else
				{
					row.CreateCell(6).SetCellValue(item.CurrentApprover.Approver.DisplayName);
				}
				row.CreateCell(7).SetCellValue(item.CostCenter.CostCenterName);
				row.CreateCell(8).SetCellValue(item.ItemCountOrder);
				row.CreateCell(9).SetCellValue(Convert.ToDouble(item.GrandTotalAmt));

				++rowIndex;
				row = sheet.CreateRow(rowIndex);
				row.CreateCell(1).SetCellValue(new ResourceString("Report.ListOrderPartial.ProductId"));
				row.CreateCell(2).SetCellValue(new ResourceString("Report.ListOrderPartial.ProductName"));
				row.CreateCell(3).SetCellValue(new ResourceString("Report.ListOrderPartial.PriceIncVat"));
				row.CreateCell(4).SetCellValue(new ResourceString("Report.ListOrderPartial.PriceExcVat"));
				row.CreateCell(5).SetCellValue(new ResourceString("Report.ListOrderPartial.Quantity"));
				row.CreateCell(6).SetCellValue(new ResourceString("Report.ListOrderPartial.Unit"));
				row.CreateCell(7).SetCellValue(new ResourceString("Report.ListOrderPartial.PriceNet"));


				++rowIndex;
				foreach (var orderDetail in listOrder.ItemOrders.Where(o => o.OrderId == item.OrderID))
				{
					row = sheet.CreateRow(rowIndex);
					row.CreateCell(1).SetCellValue(orderDetail.Product.Id);
					row.CreateCell(2).SetCellValue(orderDetail.Product.Name);
					row.CreateCell(3).SetCellValue(Convert.ToDouble(orderDetail.Product.PriceIncVat));
					row.CreateCell(4).SetCellValue(Convert.ToDouble(orderDetail.Product.PriceExcVat));
					row.CreateCell(5).SetCellValue(orderDetail.Quantity);
					row.CreateCell(6).SetCellValue(orderDetail.Product.Unit);
					row.CreateCell(7).SetCellValue(Convert.ToDouble(orderDetail.ItemPriceNet));
					rowIndex++;
				}

				rowIndex++;
			}

			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "exportOrderDetail.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportCostcenterOrder(TrackCostCenter trackCostCenter)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				trackCostCenter.ListSummaryOrder = new ReportRepository(connection).GetSummaryOrderCostCenter(trackCostCenter, User.CurrentCompanyId);
			}

			//----------------Export to Excel-------------------
			var workbook = new HSSFWorkbook();
			var sheet = workbook.CreateSheet("dataOrder");
			var rowIndex = 0;
			var row = sheet.CreateRow(rowIndex);
			row.CreateCell(0).SetCellValue(new ResourceString("Order.ViewDataOrder.CostCenterId"));
			row.CreateCell(1).SetCellValue(new ResourceString("Order.ViewOrder.CostCenterName"));
			row.CreateCell(2).SetCellValue(new ResourceString("Order.ViewDataOrder.GrandTotalAmount"));
			row.CreateCell(3).SetCellValue(new ResourceString("Order.ViewDataOrder.CountOrder"));

			rowIndex++;
			foreach (var item in trackCostCenter.ListSummaryOrder)
			{
				row = sheet.CreateRow(rowIndex);
				row.CreateCell(0).SetCellValue(item.CostcenterId);
				row.CreateCell(1).SetCellValue(item.CostcenterName);
				row.CreateCell(2).SetCellValue(item.SumGrandTotalAmt.ToString(new MoneyFormat()));
				row.CreateCell(3).SetCellValue(item.CountOrder);
				rowIndex++;
			}

			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "exportCostcenterOrder.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportPurchaseConditionalOrder(AdvanceSearchField advanceSearchField, ImageInputData action)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (action.Name == "ExportDetail")
				{
					ListOrder listOrder = new ListOrder();
					listOrder.ItemOrders = new List<OrderDetail>();
					listOrder.Orders = new ReportRepository(connection).GetConditionalOrderWithoutPaging(advanceSearchField, User.CurrentCompanyId, User.UserId, false);

					foreach (var order in listOrder.Orders)
					{
						listOrder.ItemOrders = listOrder.ItemOrders.Concat(new OrderRepository(connection).GetOrderDetailBySearch(order.OrderID));
					}
					return ExportOrderDetail(listOrder);
				}
				else
				{
					IEnumerable<Order> orders = new ReportRepository(connection).GetConditionalOrderWithoutPaging(advanceSearchField, User.CurrentCompanyId, User.UserId, false);
					return ExportOrder(orders);
				}

				//IEnumerable<Order> orders = new ReportRepository(connection).GetConditionalOrderWithoutPaging(advanceSearchField, User.CurrentCompanyId, User.UserId, false);
				//return ExportOrder(orders);
			}
		}

		public ActionResult ExportPurchaseOnProgressOrder()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new ReportRepository(connection).GetTrackOrderByStatusWithoutPaging(null, Order.OrderStatus.Waiting, User.CurrentCompanyId, User.UserId, false);
				return ExportOrder(orders);
			}
		}

		public ActionResult ExportPurchaseCompleteOrder()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new ReportRepository(connection).GetTrackOrderByStatusWithoutPaging(null, Order.OrderStatus.Approved, User.CurrentCompanyId, User.UserId, false);
				return ExportOrder(orders);
			}
		}

		public ActionResult ExportPurchaseExpireOrder()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new ReportRepository(connection).GetTrackOrderByStatusWithoutPaging(null, Order.OrderStatus.Expired, User.CurrentCompanyId, User.UserId, false);
				return ExportOrder(orders);
			}
		}

		public ActionResult ExportPurchaseOrderHistory(MonthFilter? monthFilter, AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new List<Order>();
				if (monthFilter.HasValue)
				{
					orders = new ReportRepository(connection).GetOrderHistoryWithoutPaging(monthFilter, User.CurrentCompanyId, User.UserId, false);
				}
				else
				{
					orders = new ReportRepository(connection).GetSearchOrderHistory(advanceSearchField, User.CurrentCompanyId, User.UserId, false);
				}

				return ExportOrder(orders);
			}
		}



		public ActionResult ExportFinalReview()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository companyRepo = new CompanyRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				FinalReview finalReview = new FinalReview();
				finalReview.FinalCompany = companyRepo.GetCompanyDetail(User.CurrentCompanyId);
				finalReview.FinalCompany.CompanyInvoice = companyRepo.GetCompanyInvoice(User.CustId);
				finalReview.FinalCompany.ListShipAddress = companyRepo.GetCompanyShipping(User.CurrentCompanyId).Where(s => s.CustId == finalReview.FinalCompany.DefaultCustId && s.IsDefault.Equals("Yes"));
				finalReview.CompanyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(User.CurrentCompanyId);
				finalReview.CostCenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId);
				finalReview.UserAdmin = userRepo.GetAllUser(User.CurrentCompanyId, true);
				finalReview.UserPurchase = userRepo.GetAllUser(User.CurrentCompanyId, false);
				finalReview.RequesterLineDetail = companyRepo.GetAllCompanyRequesterLine(User.CurrentCompanyId);
				return ExportToExcel(finalReview);
			}
		}

		private ActionResult ExportToExcel(FinalReview finalReview)
		{
			//----------------Export to Excel-------------------
			HSSFWorkbook workbook = new HSSFWorkbook();
			Sheet sheet = workbook.CreateSheet("CompanyInfo");

			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;

			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;


			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;


			int rowIndex = 0;

			Cell cell_company;
			Row row;

			row = sheet.CreateRow(rowIndex);

			cell_company = row.CreateCell(0);
			int cell_width = 0;
			cell_company.Sheet.SetColumnWidth(0, cell_width);
			cell_company.SetCellValue(new ResourceString("Admin.AllSetting.CompanyInfo"));
			cell_width = cell_company.RichStringCellValue.Length * 300;
			cell_company.RichStringCellValue.ApplyFont(font);


			rowIndex += 1;
			Cell cell_headerCompany;
			Row row_headerCompany;
			row_headerCompany = sheet.CreateRow(rowIndex);


			cell_headerCompany = row_headerCompany.CreateCell(0);
			cell_headerCompany.SetCellValue(new ResourceString("Admin.CompanyInfo.CompanyId"));
			cell_width = cell_headerCompany.RichStringCellValue.Length * 300;
			cell_headerCompany.RichStringCellValue.ApplyFont(font); cell_headerCompany.CellStyle = cellstyle;
			cell_headerCompany.Sheet.SetColumnWidth(0, cell_width);


			cell_headerCompany = row_headerCompany.CreateCell(1);
			cell_headerCompany.SetCellValue(new ResourceString("Admin.CompanyInfo.CompanyThaiName"));
			cell_width = cell_headerCompany.RichStringCellValue.Length * 400;
			cell_headerCompany.RichStringCellValue.ApplyFont(font); cell_headerCompany.CellStyle = cellstyle;
			cell_headerCompany.Sheet.SetColumnWidth(1, cell_width);

			cell_headerCompany = row_headerCompany.CreateCell(2);
			cell_headerCompany.SetCellValue(new ResourceString("Admin.CompanyInfo.CompanyEngName"));
			cell_width = cell_headerCompany.RichStringCellValue.Length * 400;
			cell_headerCompany.RichStringCellValue.ApplyFont(font); cell_headerCompany.CellStyle = cellstyle;
			cell_headerCompany.Sheet.SetColumnWidth(2, cell_width);

			cell_headerCompany = row_headerCompany.CreateCell(3);
			cell_headerCompany.SetCellValue(new ResourceString("Admin.CompanyInfo.DefaultCustId"));
			cell_width = cell_headerCompany.RichStringCellValue.Length * 250;
			cell_headerCompany.RichStringCellValue.ApplyFont(font); cell_headerCompany.CellStyle = cellstyle;
			cell_headerCompany.Sheet.SetColumnWidth(3, cell_width);

			cell_headerCompany = row_headerCompany.CreateCell(4);
			cell_headerCompany.SetCellValue(new ResourceString("Admin.CompanyInfo.InvoiceAddress") + "(" + new ResourceString("Admin.FinalReview.Default") + ")");
			cell_width = cell_headerCompany.RichStringCellValue.Length * 250;
			cell_headerCompany.RichStringCellValue.ApplyFont(font); cell_headerCompany.CellStyle = cellstyle;
			cell_headerCompany.Sheet.SetColumnWidth(4, cell_width);

			cell_headerCompany = row_headerCompany.CreateCell(5);
			cell_headerCompany.SetCellValue(new ResourceString("Admin.CompanyInfo.ShippingAddress") + "(" + new ResourceString("Admin.FinalReview.Default") + ")");
			cell_width = cell_headerCompany.RichStringCellValue.Length * 250;
			cell_headerCompany.RichStringCellValue.ApplyFont(font); cell_headerCompany.CellStyle = cellstyle;
			cell_headerCompany.Sheet.SetColumnWidth(5, cell_width);




			Cell cell_DetailCompany;
			Row row_DetailCompany;
			row_DetailCompany = sheet.CreateRow(++rowIndex);


			cell_DetailCompany = row_DetailCompany.CreateCell(0);
			cell_DetailCompany.SetCellValue(finalReview.FinalCompany.CompanyId);
			cell_DetailCompany.CellStyle = cellstyle_detail;

			cell_DetailCompany = row_DetailCompany.CreateCell(1);
			cell_DetailCompany.SetCellValue(finalReview.FinalCompany.CompanyThaiName);
			cell_DetailCompany.CellStyle = cellstyle_detail;


			cell_DetailCompany = row_DetailCompany.CreateCell(2);
			cell_DetailCompany.SetCellValue(finalReview.FinalCompany.CompanyEngName);
			cell_DetailCompany.CellStyle = cellstyle_detail;


			cell_DetailCompany = row_DetailCompany.CreateCell(3);
			cell_DetailCompany.SetCellValue(finalReview.FinalCompany.DefaultCustId);
			cell_DetailCompany.CellStyle = cellstyle_detail;


			cell_DetailCompany = row_DetailCompany.CreateCell(4);
			row.CreateCell(4).SetCellValue(finalReview.FinalCompany.CompanyInvoice.Address1 + finalReview.FinalCompany.CompanyInvoice.Address2 +
			finalReview.FinalCompany.CompanyInvoice.Address3 + finalReview.FinalCompany.CompanyInvoice.Address4);
			cell_DetailCompany.CellStyle = cellstyle_detail;
			cell_width = cell_DetailCompany.RichStringCellValue.Length * 250;
			cell_DetailCompany.Sheet.SetColumnWidth(4, cell_width);


			cell_DetailCompany = row_DetailCompany.CreateCell(5);
			var resultAddress = "";
			foreach (var item in finalReview.FinalCompany.ListShipAddress)
			{
				resultAddress += item.Address1 + item.Address2 + item.Address3 + item.Address4;
			}
			cell_DetailCompany.SetCellValue(resultAddress);
			cell_DetailCompany.CellStyle = cellstyle_detail;
			cell_width = cell_DetailCompany.RichStringCellValue.Length * 250;
			cell_DetailCompany.Sheet.SetColumnWidth(5, cell_width);



			Cell cell_DepartmentHeader;
			Row row_DetailDepartmentHeader;
			Sheet sheet_department = workbook.CreateSheet("Department");

			rowIndex = 0;
			row_DetailDepartmentHeader = sheet_department.CreateRow(rowIndex);
			cell_DepartmentHeader = row_DetailDepartmentHeader.CreateCell(0);
			cell_DepartmentHeader.SetCellValue(new ResourceString("Admin.Department.DeptInfo"));
			cell_DepartmentHeader.RichStringCellValue.ApplyFont(font);
			cell_width = cell_DepartmentHeader.RichStringCellValue.Length * 300;




			row_DetailDepartmentHeader = sheet_department.CreateRow(++rowIndex);

			if (finalReview.CompanyDepartment.Any())
			{
				cell_DepartmentHeader = row_DetailDepartmentHeader.CreateCell(0);
				cell_DepartmentHeader.SetCellValue(new ResourceString("Admin.Department.DeptID"));
				cell_DepartmentHeader.RichStringCellValue.ApplyFont(font);
				cell_DepartmentHeader.CellStyle = cellstyle;
				cell_width = cell_DepartmentHeader.RichStringCellValue.Length * 300;
				cell_DepartmentHeader.Sheet.SetColumnWidth(0, cell_width);

				cell_DepartmentHeader = row_DetailDepartmentHeader.CreateCell(1);
				cell_DepartmentHeader.SetCellValue(new ResourceString("Admin.Department.ThaiName"));
				cell_DepartmentHeader.RichStringCellValue.ApplyFont(font);
				cell_DepartmentHeader.CellStyle = cellstyle;
				cell_width = cell_DepartmentHeader.RichStringCellValue.Length * 350;
				cell_DepartmentHeader.Sheet.SetColumnWidth(1, cell_width);

				cell_DepartmentHeader = row_DetailDepartmentHeader.CreateCell(2);
				cell_DepartmentHeader.SetCellValue(new ResourceString("Admin.Department.EngName"));
				cell_DepartmentHeader.RichStringCellValue.ApplyFont(font);
				cell_DepartmentHeader.CellStyle = cellstyle;
				cell_width = cell_DepartmentHeader.RichStringCellValue.Length * 350;
				cell_DepartmentHeader.Sheet.SetColumnWidth(2, cell_width);

				cell_DepartmentHeader = row_DetailDepartmentHeader.CreateCell(3);
				cell_DepartmentHeader.SetCellValue(new ResourceString("Admin.Department.Status"));
				cell_DepartmentHeader.RichStringCellValue.ApplyFont(font);
				cell_DepartmentHeader.CellStyle = cellstyle;
				cell_width = cell_DepartmentHeader.RichStringCellValue.Length * 350;
				cell_DepartmentHeader.Sheet.SetColumnWidth(3, cell_width);

				++rowIndex;

				Cell cell_DepartmentDetail;
				Row row_DetailDepartment;
				foreach (var item in finalReview.CompanyDepartment)
				{
					row_DetailDepartment = sheet_department.CreateRow(rowIndex);

					cell_DepartmentDetail = row_DetailDepartment.CreateCell(0);
					cell_DepartmentDetail.SetCellValue(item.DepartmentID);
					cell_DepartmentDetail.CellStyle = cellstyle_detail;

					cell_DepartmentDetail = row_DetailDepartment.CreateCell(1);
					cell_DepartmentDetail.SetCellValue(item.DepartmentThaiName);
					cell_DepartmentDetail.CellStyle = cellstyle_detail;

					cell_DepartmentDetail = row_DetailDepartment.CreateCell(2);
					cell_DepartmentDetail.SetCellValue(item.DepartmentEngName);
					cell_DepartmentDetail.CellStyle = cellstyle_detail;

					cell_DepartmentDetail = row_DetailDepartment.CreateCell(3);
					cell_DepartmentDetail.SetCellValue(item.DisplayDepartmentStatus);
					cell_DepartmentDetail.CellStyle = cellstyle_detail;
					rowIndex++;
				}
			}


			rowIndex = 0;
			Sheet sheet_costcenter = workbook.CreateSheet("CostCenter");
			Cell cell_CostcenterHeader;
			Row row_DetailCostcenterHeader;


			row_DetailCostcenterHeader = sheet_costcenter.CreateRow(rowIndex);
			cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(0);
			cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.CostCenter.ContentInfo"));
			cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
			cell_width = cell_DepartmentHeader.RichStringCellValue.Length * 300;


			row_DetailCostcenterHeader = sheet_costcenter.CreateRow(++rowIndex);

			if (finalReview.CostCenter.Any())
			{


				cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(0);
				cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.Department.DeptID"));
				cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
				cell_CostcenterHeader.CellStyle = cellstyle;
				cell_width = cell_CostcenterHeader.RichStringCellValue.Length * 600;
				cell_CostcenterHeader.Sheet.SetColumnWidth(0, cell_width);

				cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(1);
				cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.CostCenter.CostCenterID"));
				cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
				cell_CostcenterHeader.CellStyle = cellstyle;
				cell_width = cell_CostcenterHeader.RichStringCellValue.Length * 400;
				cell_CostcenterHeader.Sheet.SetColumnWidth(1, cell_width);

				cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(2);
				cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.CostCenter.ThaiName"));
				cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
				cell_CostcenterHeader.CellStyle = cellstyle;
				cell_width = cell_CostcenterHeader.RichStringCellValue.Length * 600;
				cell_CostcenterHeader.Sheet.SetColumnWidth(2, cell_width);


				cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(3);
				cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.CostCenter.EngName"));
				cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
				cell_CostcenterHeader.CellStyle = cellstyle;
				cell_width = cell_CostcenterHeader.RichStringCellValue.Length * 600;
				cell_CostcenterHeader.Sheet.SetColumnWidth(3, cell_width);


				cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(4);
				cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.CostCenter.CustID"));
				cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
				cell_CostcenterHeader.CellStyle = cellstyle;
				cell_width = cell_CostcenterHeader.RichStringCellValue.Length * 400;
				cell_CostcenterHeader.Sheet.SetColumnWidth(4, cell_width);


				cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(5);
				cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.CompanyInfo.InvoiceAddress"));
				cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
				cell_CostcenterHeader.CellStyle = cellstyle;


				cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(6);
				cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.CompanyInfo.ShippingAddress"));
				cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
				cell_CostcenterHeader.CellStyle = cellstyle;

				cell_CostcenterHeader = row_DetailCostcenterHeader.CreateCell(7);
				cell_CostcenterHeader.SetCellValue(new ResourceString("Admin.CostCenter.Status"));
				cell_CostcenterHeader.RichStringCellValue.ApplyFont(font);
				cell_CostcenterHeader.CellStyle = cellstyle;



				++rowIndex;
				Cell cell_CostcenterDetail;
				Row row_DetailCostcenter;
				foreach (var item in finalReview.CostCenter)
				{
					row_DetailCostcenter = sheet_costcenter.CreateRow(rowIndex);

					cell_CostcenterDetail = row_DetailCostcenter.CreateCell(0);
					cell_CostcenterDetail.SetCellValue(item.CostCenterDepartment.DepartmentID);
					cell_CostcenterDetail.CellStyle = cellstyle_detail;


					cell_CostcenterDetail = row_DetailCostcenter.CreateCell(1);
					cell_CostcenterDetail.SetCellValue(item.CostCenterID);
					cell_CostcenterDetail.CellStyle = cellstyle_detail;


					cell_CostcenterDetail = row_DetailCostcenter.CreateCell(2);
					cell_CostcenterDetail.SetCellValue(item.CostCenterThaiName);
					cell_CostcenterDetail.CellStyle = cellstyle_detail;


					cell_CostcenterDetail = row_DetailCostcenter.CreateCell(3);
					cell_CostcenterDetail.SetCellValue(item.CostCenterEngName);
					cell_CostcenterDetail.CellStyle = cellstyle_detail;


					cell_CostcenterDetail = row_DetailCostcenter.CreateCell(4);
					cell_CostcenterDetail.SetCellValue(item.CostCenterCustID);
					cell_CostcenterDetail.CellStyle = cellstyle_detail;


					cell_CostcenterDetail = row_DetailCostcenter.CreateCell(5);
					cell_CostcenterDetail.SetCellValue(item.CostCenterInvoice.Address1 + item.CostCenterInvoice.Address2 + item.CostCenterInvoice.Address3 + item.CostCenterInvoice.Address4);
					cell_CostcenterDetail.CellStyle = cellstyle_detail;
					cell_width = cell_CostcenterDetail.RichStringCellValue.Length * 300;
					cell_CostcenterDetail.Sheet.SetColumnWidth(5, cell_width);

					cell_CostcenterDetail = row_DetailCostcenter.CreateCell(6);
					cell_CostcenterDetail.SetCellValue(item.CostCenterShipping.Address1 + item.CostCenterShipping.Address2 + item.CostCenterShipping.Address3 + item.CostCenterShipping.Address4);
					cell_CostcenterDetail.CellStyle = cellstyle_detail;
					cell_width = cell_CostcenterDetail.RichStringCellValue.Length * 300;
					cell_CostcenterDetail.Sheet.SetColumnWidth(6, cell_width);

					cell_CostcenterDetail = row_DetailCostcenter.CreateCell(7);
					cell_CostcenterDetail.SetCellValue(item.DisplayCostCenterStatus);
					cell_CostcenterDetail.CellStyle = cellstyle_detail;
					cell_width = cell_CostcenterDetail.RichStringCellValue.Length * 300;
					cell_CostcenterDetail.Sheet.SetColumnWidth(7, cell_width);

					rowIndex++;
				}
			}
			rowIndex = 0;

			Sheet sheet_UserInfo = workbook.CreateSheet("UserInfo");
			Cell cell_UserInfoHeader;
			Row row_DetailUserInfoHeader;

			row_DetailUserInfoHeader = sheet_UserInfo.CreateRow(rowIndex);

			cell_UserInfoHeader = row_DetailUserInfoHeader.CreateCell(0);
			cell_UserInfoHeader.SetCellValue(new ResourceString("Admin.FinalReview.UserData"));
			cell_UserInfoHeader.RichStringCellValue.ApplyFont(font);
			cell_width = cell_UserInfoHeader.RichStringCellValue.Length * 400;


			row_DetailUserInfoHeader = sheet_UserInfo.CreateRow(++rowIndex);

			cell_UserInfoHeader = row_DetailUserInfoHeader.CreateCell(0);
			cell_UserInfoHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.Email"));
			cell_UserInfoHeader.RichStringCellValue.ApplyFont(font);
			cell_UserInfoHeader.CellStyle = cellstyle;
			cell_width = cell_UserInfoHeader.RichStringCellValue.Length * 600;
			cell_UserInfoHeader.Sheet.SetColumnWidth(0, cell_width);

			cell_UserInfoHeader = row_DetailUserInfoHeader.CreateCell(1);
			cell_UserInfoHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ThaiName"));
			cell_UserInfoHeader.RichStringCellValue.ApplyFont(font);
			cell_UserInfoHeader.CellStyle = cellstyle;
			cell_width = cell_UserInfoHeader.RichStringCellValue.Length * 600;
			cell_UserInfoHeader.Sheet.SetColumnWidth(1, cell_width);

			cell_UserInfoHeader = row_DetailUserInfoHeader.CreateCell(2);
			cell_UserInfoHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.Role"));
			cell_UserInfoHeader.RichStringCellValue.ApplyFont(font);
			cell_UserInfoHeader.CellStyle = cellstyle;
			cell_width = cell_UserInfoHeader.RichStringCellValue.Length * 600;
			cell_UserInfoHeader.Sheet.SetColumnWidth(2, cell_width);

			cell_UserInfoHeader = row_DetailUserInfoHeader.CreateCell(3);
			cell_UserInfoHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.UserStatus"));
			cell_UserInfoHeader.RichStringCellValue.ApplyFont(font);
			cell_UserInfoHeader.CellStyle = cellstyle;
			cell_width = cell_UserInfoHeader.RichStringCellValue.Length * 600;
			cell_UserInfoHeader.Sheet.SetColumnWidth(3, cell_width);


			++rowIndex;


			Cell cell_UserInfo;
			Row row_DetailUserInfo;
			foreach (var item in finalReview.UserAdmin.GroupBy(u => u.UserId))
			{
				row_DetailUserInfo = sheet_UserInfo.CreateRow(rowIndex);

				string[] displayrole = new string[item.Count()];
				int index = 0;
				foreach (var role in item)
				{
					displayrole[index] = role.DisplayUserRoleName;
					index++;
				}
				cell_UserInfo = row_DetailUserInfo.CreateCell(0);
				cell_UserInfo.SetCellValue(item.First().UserId);
				cell_UserInfo.CellStyle = cellstyle_detail;
				cell_UserInfo = row_DetailUserInfo.CreateCell(1);
				cell_UserInfo.SetCellValue(item.First().DisplayName);
				cell_UserInfo.CellStyle = cellstyle_detail;
				cell_UserInfo = row_DetailUserInfo.CreateCell(2);
				cell_UserInfo.SetCellValue(string.Join(",", displayrole));
				cell_UserInfo.CellStyle = cellstyle_detail;
				cell_UserInfo = row_DetailUserInfo.CreateCell(3);
				cell_UserInfo.SetCellValue(item.First().DisplayUserStatus);
				cell_UserInfo.CellStyle = cellstyle_detail;
				rowIndex++;
			}
			foreach (var item in finalReview.UserPurchase.GroupBy(u => u.UserId))
			{
				row_DetailUserInfo = sheet_UserInfo.CreateRow(rowIndex);

				string[] displayrole = new string[item.Count()];
				int index = 0;
				foreach (var role in item)
				{
					displayrole[index] = role.DisplayUserRoleName;
					index++;
				}
				cell_UserInfo = row_DetailUserInfo.CreateCell(0);
				cell_UserInfo.SetCellValue(item.First().UserId);
				cell_UserInfo.CellStyle = cellstyle_detail;
				cell_UserInfo = row_DetailUserInfo.CreateCell(1);
				cell_UserInfo.SetCellValue(item.First().DisplayName);
				cell_UserInfo.CellStyle = cellstyle_detail;
				cell_UserInfo = row_DetailUserInfo.CreateCell(2);
				cell_UserInfo.SetCellValue(string.Join(",", displayrole));
				cell_UserInfo.CellStyle = cellstyle_detail;
				cell_UserInfo = row_DetailUserInfo.CreateCell(3);
				cell_UserInfo.SetCellValue(item.First().DisplayUserStatus);
				cell_UserInfo.CellStyle = cellstyle_detail;
				rowIndex++;
			}




			rowIndex = 0;

			Sheet sheet_RoleUser = workbook.CreateSheet("RequesterLine");
			Cell cell_RoleUserHeader;
			Row row_DetailRoleUserHeader;

			row_DetailRoleUserHeader = sheet_RoleUser.CreateRow(rowIndex);
			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(0);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.RequesterLineGuide"));
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 400;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);


			row_DetailRoleUserHeader = sheet_RoleUser.CreateRow(++rowIndex);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(0);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.Department.DeptID"));
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(0, cell_width);



			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(1);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.CostCenter.DepartmentName"));
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 600;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(1, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(2);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.CostCenter.CostCenterID"));
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(2, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(3);
			cell_RoleUserHeader.SetCellValue(new ResourceString("DataUserPermission.CostcenterName"));
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 500;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(3, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(4);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.FinalReview.RequesterLogin"));
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 400;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(4, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(5);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.FinalReview.RequesterName"));
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 500;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(5, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(6);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverLogin") + " (" + new ResourceString("Admin.FinalReview.Level1") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(6, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(7);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverName") + " (" + new ResourceString("Admin.FinalReview.Level1") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(7, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(8);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverCreditBudget") + " (" + new ResourceString("Admin.FinalReview.Level1") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(8, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(9);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverLogin") + " (" + new ResourceString("Admin.FinalReview.Level2") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 500;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(9, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(10);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverName") + " (" + new ResourceString("Admin.FinalReview.Level2") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(10, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(11);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverCreditBudget") + " (" + new ResourceString("Admin.FinalReview.Level2") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(11, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(12);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverLogin") + " (" + new ResourceString("Admin.FinalReview.Level3") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(12, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(13);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverName") + " (" + new ResourceString("Admin.FinalReview.Level3") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(13, cell_width);

			cell_RoleUserHeader = row_DetailRoleUserHeader.CreateCell(14);
			cell_RoleUserHeader.SetCellValue(new ResourceString("Admin.ExportFinalReview.ApproverCreditBudget") + new ResourceString("Admin.FinalReview.Level3") + ")");
			cell_width = cell_RoleUserHeader.RichStringCellValue.Length * 300;
			cell_RoleUserHeader.RichStringCellValue.ApplyFont(font);
			cell_RoleUserHeader.CellStyle = cellstyle;
			cell_RoleUserHeader.Sheet.SetColumnWidth(14, cell_width);


			++rowIndex;

			Cell cell_RoleUser;
			Row row_DetailRoleUser;
			foreach (var costcenter in finalReview.RequesterLineDetail.GroupBy(c => c.CostcenterID))
			{
				foreach (var user in costcenter.GroupBy(u => u.RequesterId))
				{
					row_DetailRoleUser = sheet_RoleUser.CreateRow(rowIndex);
					cell_RoleUser = row_DetailRoleUser.CreateCell(0);
					cell_RoleUser.SetCellValue(user.First().DepartmentID);
					cell_RoleUser.CellStyle = cellstyle_detail;
					cell_RoleUser = row_DetailRoleUser.CreateCell(1);
					cell_RoleUser.SetCellValue(user.First().DepartmentName);
					cell_RoleUser.CellStyle = cellstyle_detail;
					cell_RoleUser = row_DetailRoleUser.CreateCell(2);
					cell_RoleUser.SetCellValue(user.First().CostcenterID);
					cell_RoleUser.CellStyle = cellstyle_detail;
					cell_RoleUser = row_DetailRoleUser.CreateCell(3);
					cell_RoleUser.SetCellValue(user.First().CostcenterName);
					cell_RoleUser.CellStyle = cellstyle_detail;
					cell_RoleUser = row_DetailRoleUser.CreateCell(4);
					cell_RoleUser.SetCellValue(user.First().RequesterId);
					cell_RoleUser.CellStyle = cellstyle_detail;
					cell_RoleUser = row_DetailRoleUser.CreateCell(5);
					cell_RoleUser.SetCellValue(user.First().RequesterName);
					cell_RoleUser.CellStyle = cellstyle_detail;
					cell_RoleUser = row_DetailRoleUser.CreateCell(6);
					cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 1).ApproverId);
					cell_RoleUser.CellStyle = cellstyle_detail;
					cell_RoleUser = row_DetailRoleUser.CreateCell(7);
					cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 1).ApproverName);
					cell_RoleUser.CellStyle = cellstyle_detail;
					cell_RoleUser = row_DetailRoleUser.CreateCell(8);
					cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 1).ApproverCreditBudget.ToString(new MoneyFormat()));
					cell_RoleUser.CellStyle = cellstyle_detail;


					if (user.Any(a => a.ApproverLevel == 2))
					{
						cell_RoleUser = row_DetailRoleUser.CreateCell(9);
						cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 2).ApproverId);
						cell_RoleUser.CellStyle = cellstyle_detail;
						cell_RoleUser = row_DetailRoleUser.CreateCell(10);
						cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 2).ApproverName);
						cell_RoleUser.CellStyle = cellstyle_detail;
						cell_RoleUser = row_DetailRoleUser.CreateCell(11);
						cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 2).ApproverCreditBudget.ToString(new MoneyFormat()));
						cell_RoleUser.CellStyle = cellstyle_detail;

					}
					else
					{
						cell_RoleUser = row_DetailRoleUser.CreateCell(9);
						cell_RoleUser.SetCellValue("-");
						cell_RoleUser.CellStyle = cellstyle_detail;
						cell_RoleUser = row_DetailRoleUser.CreateCell(10);
						cell_RoleUser.SetCellValue("-");
						cell_RoleUser.CellStyle = cellstyle_detail;
						cell_RoleUser = row_DetailRoleUser.CreateCell(11);
						cell_RoleUser.SetCellValue("-");
						cell_RoleUser.CellStyle = cellstyle_detail;

					}

					if (user.Any(a => a.ApproverLevel == 3))
					{
						cell_RoleUser = row_DetailRoleUser.CreateCell(12);
						cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 3).ApproverId);
						cell_RoleUser.CellStyle = cellstyle_detail;
						cell_RoleUser = row_DetailRoleUser.CreateCell(13);
						cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 3).ApproverName);
						cell_RoleUser.CellStyle = cellstyle_detail;
						cell_RoleUser = row_DetailRoleUser.CreateCell(14);
						cell_RoleUser.SetCellValue(user.First(a => a.ApproverLevel == 3).ApproverCreditBudget.ToString(new MoneyFormat()));
						cell_RoleUser.CellStyle = cellstyle_detail;

					}
					else
					{
						cell_RoleUser = row_DetailRoleUser.CreateCell(12);
						cell_RoleUser.SetCellValue("-");
						cell_RoleUser.CellStyle = cellstyle_detail;
						cell_RoleUser = row_DetailRoleUser.CreateCell(13);
						cell_RoleUser.SetCellValue("-");
						cell_RoleUser.CellStyle = cellstyle_detail;
						cell_RoleUser = row_DetailRoleUser.CreateCell(14);
						cell_RoleUser.SetCellValue("-");
						cell_RoleUser.CellStyle = cellstyle_detail;

					}

					rowIndex++;
				}
			}


			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "CompanyInfo_Data.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}


		public Row SetAllAutoSizeColumn(Row row)
		{
			for (int indexRow = 0; indexRow < row.LastCellNum; indexRow++)
			{
				row.Sheet.AutoSizeColumn(indexRow);
			}
			return row;
		}



		/*-----------------------Start Export OFMAdmin--------------------------*/
		public ActionResult OFMAdminExportFinalReview(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				FinalReview finalReview = new FinalReview();
				finalReview.FinalCompany = compRepo.GetCompanyDetail(companyId);
				finalReview.FinalCompany.CompanyInvoice = compRepo.GetCompanyInvoice(finalReview.FinalCompany.DefaultCustId);
				finalReview.FinalCompany.ListShipAddress = compRepo.GetCompanyShipping(companyId).Where(s => s.CustId == finalReview.FinalCompany.DefaultCustId && s.IsDefault.Equals("Yes"));
				finalReview.CompanyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(companyId);
				finalReview.CostCenter = new CostCenterRepository(connection).GetAllCostCenter(companyId);
				finalReview.UserAdmin = userRepo.GetAllUser(companyId, true);
				finalReview.UserPurchase = userRepo.GetAllUser(companyId, false);
				finalReview.RequesterLineDetail = compRepo.GetAllCompanyRequesterLine(companyId);
				return ExportToExcel(finalReview);
			}
		}
		/*-----------------------End Export OFMAdmin--------------------------*/



		/*-----------------------Start Print file PDF--------------------------*/
		public ActionResult PrintPo(string orderGuid, string ActionName)
		{
			if (ActionName == "PrintEnPO")
			{
				return PrintEnPO(orderGuid);
			}
			else if (ActionName == "PrintThQU")
			{
				return PrintThQU(orderGuid);
			}
			else if (ActionName == "PrintEnQU")
			{
				return PrintEnQU(orderGuid);
			}
			else if (ActionName == "PrintExcel")
			{
				return ExportViewOrder(orderGuid);
			}
			else
			{
				return PrintThPO(orderGuid);
			}
		}

		private ActionResult PrintEnPO(string orderGuid)
		{
			ReportClass rptH = new ReportClass();
			rptH.FileName = HostingEnvironment.MapPath("~/App_Data/ofmEnPo_COL.rpt");
			rptH.Load();
			SetConnection(rptH, orderGuid);
			return File(rptH.ExportToStream(ExportFormatType.PortableDocFormat), "application/pdf");
		}

		private ActionResult PrintThPO(string orderGuid)
		{
			ReportClass rptH = new ReportClass();
			rptH.FileName = HostingEnvironment.MapPath("~/App_Data/ofmThPo_COL.rpt");
			rptH.Load();
			SetConnection(rptH, orderGuid);
			return File(rptH.ExportToStream(ExportFormatType.PortableDocFormat), "application/pdf");
		}

		private ActionResult PrintThQU(string orderGuid)
		{
			ReportClass rptH = new ReportClass();
			rptH.FileName = HostingEnvironment.MapPath("~/App_Data/ofmThQu_COL.rpt");
			rptH.Load();
			SetConnection(rptH, orderGuid);
			return File(rptH.ExportToStream(ExportFormatType.PortableDocFormat), "application/pdf");
		}

		private ActionResult PrintEnQU(string orderGuid)
		{
			ReportClass rptH = new ReportClass();
			rptH.FileName = HostingEnvironment.MapPath("~/App_Data/ofmEnQu_COL.rpt");
			rptH.Load();
			SetConnection(rptH, orderGuid);
			return File(rptH.ExportToStream(ExportFormatType.PortableDocFormat), "application/pdf");
		}

		private static void SetConnection(ReportDocument report, string id)
		{
			for (var i = 0; i < report.DataSourceConnections.Count; i++)
			{
				if (Boolean.Parse(WebConfigurationManager.AppSettings["IsProduction"]))
				{
					// Production
					report.DataSourceConnections[i].SetConnection("dbofm.officemate.co.th", "DBStoreSupplyUse", "SVC_EPROSTORE", "TS53PkH5");
				}
				else
				{
					//Dev-server
					report.DataSourceConnections[i].SetConnection("Dev-Server", "DBStroeSupplyUse_New", "sa", "ofm@dev");
				}
			}
			List<string> ThisCRParamName = new List<string>();
			List<string> ThisCRParamValue = new List<string>();
			ThisCRParamName.Add("@OrderGUID");
			ThisCRParamValue.Add("'" + id + "'");

			for (var i = 0; i < ThisCRParamName.Count; i++)
			{
				ParameterDiscreteValue pdvParam = new ParameterDiscreteValue();
				ParameterValues pvParam = new ParameterValues();
				pdvParam.Value = ThisCRParamValue[i].ToString();
				pvParam.Add(pdvParam);
				report.DataDefinition.ParameterFields[ThisCRParamName[i]].ApplyCurrentValues(pvParam);
			}
		}
		/*-----------------------End Print file PDF--------------------------*/




		/*-----------------------Start Export Product--------------------------*/

		public ActionResult ExportProduct(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductCatalogRepository productCatalogRepo = new ProductCatalogRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				productCatalog.ItemProductCatalog = productCatalogRepo.GetProductCatalogForCompany(User.CurrentCompanyId);
				return ExportProductToExcel(productCatalog);
			}
		}

		private ActionResult ExportProductToExcel(ProductCatalog productCatalog)
		{
			//----------------Export to Excel-------------------
			var workbook = new HSSFWorkbook();
			var sheet = workbook.CreateSheet("ProductCompany");

			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;

			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;

			Cell cell_ProductHeader;
			Row row_DetailProcuctHeader;
			int cell_width = 0;

			var rowIndex = 0;
			row_DetailProcuctHeader = sheet.CreateRow(rowIndex);
			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(0);
			cell_ProductHeader.SetCellValue(new ResourceString("ProductCatalog.HeadProductId"));
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(0, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(1);
			cell_ProductHeader.SetCellValue(new ResourceString("ProductCatalog.HeadProductName"));
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 1000;
			cell_ProductHeader.Sheet.SetColumnWidth(1, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(2);
			cell_ProductHeader.SetCellValue(new ResourceString("ProductCatalog.HeadProductPrice"));
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(2, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(3);
			cell_ProductHeader.SetCellValue(new ResourceString("ProductCatalog.HeadProductUnit"));
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 600;
			cell_ProductHeader.Sheet.SetColumnWidth(3, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(4);
			cell_ProductHeader.SetCellValue(new ResourceString("ProductCatalog.HeadValidFrom"));
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(4, cell_width);

			cell_ProductHeader = row_DetailProcuctHeader.CreateCell(5);
			cell_ProductHeader.SetCellValue(new ResourceString("ProductCatalog.HeadValidTo"));
			cell_ProductHeader.RichStringCellValue.ApplyFont(font);
			cell_ProductHeader.CellStyle = cellstyle;
			cell_width = cell_ProductHeader.RichStringCellValue.Length * 300;
			cell_ProductHeader.Sheet.SetColumnWidth(5, cell_width);

			rowIndex++;
			Cell cell_ProductDetail;
			Row row_DetailProduct;

			foreach (var item in productCatalog.ItemProductCatalog)
			{
				row_DetailProduct = sheet.CreateRow(rowIndex);

				cell_ProductDetail = row_DetailProduct.CreateCell(0);
				cell_ProductDetail.SetCellValue(item.Id);
				cell_ProductDetail.CellStyle = cellstyle_detail;

				cell_ProductDetail = row_DetailProduct.CreateCell(1);
				cell_ProductDetail.SetCellValue(item.Name);
				cell_ProductDetail.CellStyle = cellstyle_detail;

				cell_ProductDetail = row_DetailProduct.CreateCell(2);
				cell_ProductDetail.SetCellValue(item.PriceIncVat.ToString());
				cell_ProductDetail.CellStyle = cellstyle_detail;

				cell_ProductDetail = row_DetailProduct.CreateCell(3);
				cell_ProductDetail.SetCellValue(item.Unit);
				cell_ProductDetail.CellStyle = cellstyle_detail;

				cell_ProductDetail = row_DetailProduct.CreateCell(4);
				cell_ProductDetail.SetCellValue(item.ValidFrom.ToString(new DateFormat()));
				cell_ProductDetail.CellStyle = cellstyle_detail;

				cell_ProductDetail = row_DetailProduct.CreateCell(5);
				cell_ProductDetail.SetCellValue(item.ValidTo.ToString(new DateFormat()));
				cell_ProductDetail.CellStyle = cellstyle_detail;
				rowIndex++;
			}

			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "ProductCompany.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		/*-----------------------End Export Product--------------------------*/

		public ActionResult ExportTrackOrder(JobStatus? jobStatus, DateFilter? dateFilter)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.JobStatus = jobStatus.HasValue ? jobStatus.Value : JobStatus.Open;
			IEnumerable<Order> orders = new List<Order>();
			if (trackOrders.JobStatus == JobStatus.Open)
			{
				trackOrders.DateFilter = dateFilter.HasValue ? dateFilter.Value : DateFilter.Today;
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				orders = reportRepo.GetTrackOrdersWithoutPaging(trackOrders.JobStatus, trackOrders.DateFilter);
				return OFMAdminExportTrackOrder(orders);
			}
		}

		public ActionResult ExportSearchOrderAdvance(JobStatus jobStatus, AdvanceSearchField advanceSearchField)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.JobStatus = jobStatus;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				IEnumerable<Order> orders = reportRepo.GetAdvanceSearchOrders(trackOrders.JobStatus, advanceSearchField);
				return OFMAdminExportTrackOrder(orders);
			}
		}

		private ActionResult OFMAdminExportTrackOrder(IEnumerable<Order> orders)
		{
			//----------------Export to Excel-------------------
			var workbook = new HSSFWorkbook();
			var sheet = workbook.CreateSheet("dataOrder");

			var rowIndex = 0;
			var row = sheet.CreateRow(rowIndex);
			row.CreateCell(0).SetCellValue(new ResourceString("Order.ViewDataOrder.CustID"));
			row.CreateCell(1).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderId"));
			row.CreateCell(2).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderStatus"));
			row.CreateCell(3).SetCellValue(new ResourceString("Order.ViewDataOrder.OrderDate"));
			row.CreateCell(4).SetCellValue(new ResourceString("Order.ViewDataOrder.ApprovrOrderDate"));
			row.CreateCell(5).SetCellValue(new ResourceString("Order.ViewOrder.Requester"));
			row.CreateCell(6).SetCellValue(new ResourceString("Order.ViewOrder.RequesterName"));
			row.CreateCell(7).SetCellValue(new ResourceString("Order.ViewOrder.Phone"));
			row.CreateCell(8).SetCellValue(new ResourceString("Order.ViewOrder.CurrentApprover"));
			row.CreateCell(9).SetCellValue(new ResourceString("Order.ViewOrder.CurrentApproverName"));
			row.CreateCell(10).SetCellValue(new ResourceString("Order.ViewOrder.PreviousApprover"));
			row.CreateCell(11).SetCellValue(new ResourceString("Order.ViewOrder.PreviousApproverName"));


			rowIndex++;
			foreach (var item in orders)
			{
				row = sheet.CreateRow(rowIndex);
				row.CreateCell(0).SetCellValue(item.Requester.CustId);
				row.CreateCell(1).SetCellValue(item.OrderID);
				row.CreateCell(2).SetCellValue(new ResourceString("Eprocurement2012.Models.Order+OrderStatus." + item.Status.ToString()));
				row.CreateCell(3).SetCellValue(item.OrderDate.ToString(new DateTimeFormat()));
				row.CreateCell(4).SetCellValue(item.DisplayApproveOrderDate);
				row.CreateCell(5).SetCellValue(item.Requester.UserId);
				row.CreateCell(6).SetCellValue(item.Requester.DisplayName);
				row.CreateCell(7).SetCellValue(item.Requester.TelephoneNumber);

				if (!string.IsNullOrEmpty(item.CurrentApprover.Approver.UserId))
				{
					row.CreateCell(8).SetCellValue(item.CurrentApprover.Approver.UserId);
				}
				else
				{
					row.CreateCell(8).SetCellValue("-");
				}

				if (!string.IsNullOrEmpty(item.CurrentApprover.Approver.UserId))
				{
					row.CreateCell(9).SetCellValue(item.CurrentApprover.Approver.DisplayName);
				}
				else
				{
					row.CreateCell(9).SetCellValue("-");
				}

				if (!string.IsNullOrEmpty(item.PreviousApprover.Approver.UserId))
				{
					row.CreateCell(10).SetCellValue(item.PreviousApprover.Approver.UserId);
				}
				else
				{
					row.CreateCell(10).SetCellValue("-");
				}

				if (!string.IsNullOrEmpty(item.PreviousApprover.Approver.UserId))
				{
					row.CreateCell(11).SetCellValue(item.PreviousApprover.Approver.DisplayName);
				}
				else
				{
					row.CreateCell(11).SetCellValue("-");
				}
				rowIndex++;
			}

			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "exportOrder.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportPurchaseProductSummary(AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<ProductSummary> productSummarys = new ReportRepository(connection).GetProductSummary(advanceSearchField, User.CurrentCompanyId, User.UserId, false);
				return ExportProuctSummary(productSummarys);
			}
		}

		public ActionResult ExportProductSummary(AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<ProductSummary> productSummarys = new ReportRepository(connection).GetProductSummary(advanceSearchField, User.CurrentCompanyId, User.UserId, true);
				return ExportProuctSummary(productSummarys);
			}
		}

		private ActionResult ExportProuctSummary(IEnumerable<ProductSummary> productSummarys)
		{
			//----------------Export to Excel-------------------
			var workbook = new HSSFWorkbook();
			var sheet = workbook.CreateSheet("ProductSummary");

			int rowIndex = 0;

			Cell cell_headProductSummary;
			Row row_headProductSummary;
			row_headProductSummary = sheet.CreateRow(++rowIndex);

			++rowIndex;

			cell_headProductSummary = row_headProductSummary.CreateCell(0);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.ParentName"));

			cell_headProductSummary = row_headProductSummary.CreateCell(1);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.DeptName"));

			cell_headProductSummary = row_headProductSummary.CreateCell(2);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.OrderId"));

			cell_headProductSummary = row_headProductSummary.CreateCell(3);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.ProductId"));

			cell_headProductSummary = row_headProductSummary.CreateCell(4);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.ProductName"));

			//cell_headProductSummary = row_headProductSummary.CreateCell(5);
			//cell_headProductSummary.SetCellValue(new ResourceString("ชื่อ Detp"));

			cell_headProductSummary = row_headProductSummary.CreateCell(5);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.OrderDate"));

			cell_headProductSummary = row_headProductSummary.CreateCell(6);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.RequesterName"));

			cell_headProductSummary = row_headProductSummary.CreateCell(7);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.Qty"));

			cell_headProductSummary = row_headProductSummary.CreateCell(8);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.Average"));

			cell_headProductSummary = row_headProductSummary.CreateCell(9);
			cell_headProductSummary.SetCellValue(new ResourceString("ProductSummary.Total"));

			Cell cell_detailParent;
			Row row_detailParent;

			foreach (var parents in productSummarys.GroupBy(g => g.DepartmentProduct.ParentId))
			{
				row_detailParent = sheet.CreateRow(rowIndex);

				cell_detailParent = row_detailParent.CreateCell(0);
				cell_detailParent.SetCellValue(parents.First().DepartmentProduct.ParentName);
				rowIndex++;

				foreach (var depts in parents.GroupBy(g => g.DepartmentProduct.DeptId))
				{
					row_detailParent = sheet.CreateRow(rowIndex);

					cell_detailParent = row_detailParent.CreateCell(1);
					cell_detailParent.SetCellValue(depts.First().DepartmentProduct.DeptName);
					rowIndex++;

					foreach (var item in depts)
					{
						row_detailParent = sheet.CreateRow(rowIndex);

						cell_detailParent = row_detailParent.CreateCell(2);
						cell_detailParent.SetCellValue(item.Order.OrderID);

						cell_detailParent = row_detailParent.CreateCell(3);
						cell_detailParent.SetCellValue(item.OrderDetail.Product.Id);

						cell_detailParent = row_detailParent.CreateCell(4);
						//cell_detailParent.SetCellValue(item.OrderDetail.Product.Name);

						if (item.OrderDetail.IsOfmCatalog)
						{
							cell_detailParent.SetCellValue(item.OrderDetail.Product.Name + " [" + new ResourceString("Order.ConfirmOrder.NotProductCatalog") + "]");
						}
						else
						{
							cell_detailParent.SetCellValue(item.OrderDetail.Product.Name);
						}


						cell_detailParent = row_detailParent.CreateCell(5);
						cell_detailParent.SetCellValue(item.Order.OrderDate.ToString(new DateTimeFormat()));

						cell_detailParent = row_detailParent.CreateCell(6);
						cell_detailParent.SetCellValue(item.Order.Requester.DisplayName);

						cell_detailParent = row_detailParent.CreateCell(7);
						cell_detailParent.SetCellValue(item.OrderDetail.Quantity);

						cell_detailParent = row_detailParent.CreateCell(8);
						cell_detailParent.SetCellValue(Convert.ToDouble(item.OrderDetail.ItemExcVatPrice));

						cell_detailParent = row_detailParent.CreateCell(9);
						cell_detailParent.SetCellValue(Convert.ToDouble(item.OrderDetail.ItemExcVatPrice * item.OrderDetail.Quantity));
						rowIndex++;
					}
				}
			}

			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "productSummary.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}


		public ActionResult ExportPurchaseGoodReceive()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new ReportRepository(connection).GetTrackOrderByGoodReceiveWithoutPaging(null, Order.OrderStatus.Shipped, User.CurrentCompanyId, User.UserId, false);
				return ExportOrderGoodReceive(orders);
			}
		}

		private ActionResult ExportOrderGoodReceive(IEnumerable<Order> Orders)
		{
			//----------------Export to Excel-------------------
			HSSFWorkbook workbook = new HSSFWorkbook();
			Sheet sheet = workbook.CreateSheet("GoodReceive");

			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			CellStyle cellstyle_left = workbook.CreateCellStyle();

			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;

			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;

			int rowIndex = 0;

			Cell cell_headerGoodReceive;
			Row row_headerGoodReceive;
			row_headerGoodReceive = sheet.CreateRow(++rowIndex);

			int cell_width = 0;

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(0);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.CustID"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(0, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(1);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.OrderId"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 600;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(1, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(2);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.OrderStatus"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 600;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(2, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(3);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.OrderDate"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(3, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(4);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.ApprovrOrderDate"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(4, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(5);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewOrder.RequesterName"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(5, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(6);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewOrder.EmailRequester"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(6, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(7);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewOrder.ApproverName"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(7, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(8);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Report.GoodReceiveOrder.EmailApprover"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(8, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(9);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewOrder.CostCenterName"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(9, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(10);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ConfirmOrder.Quantity"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 500;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(10, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(11);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.PartialReceive.ProductAction"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(11, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(12);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.PartialReceive.ProductCancel"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(12, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(13);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.PartialReceive.ProductRemain"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(13, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(14);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ConfirmOrder.GrandTotalAmt"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 500;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(14, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(15);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Report.GoodReceiveOrder.UpdateOn"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 600;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(15, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(16);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Report.GoodReceiveOrder.Status"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 600;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(16, cell_width);

			++rowIndex;

			Cell cell_detailGoodReceive;
			Row row_detailGoodReceive;

			foreach (var item in Orders)
			{
				row_detailGoodReceive = sheet.CreateRow(rowIndex);

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(0);
				cell_detailGoodReceive.SetCellValue(item.CustId);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(1);
				cell_detailGoodReceive.SetCellValue(item.OrderID);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(2);
				cell_detailGoodReceive.SetCellValue(new ResourceString("Eprocurement2012.Models.Order+OrderStatus." + item.Status.ToString()));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(3);
				cell_detailGoodReceive.SetCellValue(item.OrderDate.ToString(new DateTimeFormat()));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(4);
				cell_detailGoodReceive.SetCellValue(item.DisplayApproveOrderDate);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(5);
				cell_detailGoodReceive.SetCellValue(item.Requester.DisplayName);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(6);
				cell_detailGoodReceive.SetCellValue(item.Requester.UserId);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				if (item.PreviousApprover != null)
				{
					cell_detailGoodReceive = row_detailGoodReceive.CreateCell(7);
					cell_detailGoodReceive.SetCellValue(item.PreviousApprover.Approver.DisplayName);
					cell_detailGoodReceive.CellStyle = cellstyle_detail;
				}
				else
				{
					cell_detailGoodReceive = row_detailGoodReceive.CreateCell(7);
					cell_detailGoodReceive.SetCellValue(item.CurrentApprover.Approver.DisplayName);
					cell_detailGoodReceive.CellStyle = cellstyle_detail;
				}
				if (item.PreviousApprover != null)
				{
					cell_detailGoodReceive = row_detailGoodReceive.CreateCell(8);
					cell_detailGoodReceive.SetCellValue(item.PreviousApprover.Approver.UserId);
					cell_detailGoodReceive.CellStyle = cellstyle_detail;
				}
				else
				{
					cell_detailGoodReceive = row_detailGoodReceive.CreateCell(8);
					cell_detailGoodReceive.SetCellValue(item.CurrentApprover.Approver.UserId);
					cell_detailGoodReceive.CellStyle = cellstyle_detail;
				}

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(9);
				cell_detailGoodReceive.SetCellValue("[ " + item.CostCenter.CostCenterID + " ] " + item.CostCenter.CostCenterName);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(10);
				cell_detailGoodReceive.SetCellValue(item.ItemCountOrder);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(11);
				cell_detailGoodReceive.SetCellValue(Convert.ToDouble(item.GoodReceiveDetail.ActionQty));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(12);
				cell_detailGoodReceive.SetCellValue(Convert.ToDouble(item.GoodReceiveDetail.CancelQty));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(13);
				cell_detailGoodReceive.SetCellValue(Convert.ToDouble(item.ItemCountOrder - (item.GoodReceiveDetail.ActionQty + item.GoodReceiveDetail.CancelQty)));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(14);
				cell_detailGoodReceive.SetCellValue(Convert.ToDouble(item.GrandTotalAmt));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(15);
				cell_detailGoodReceive.SetCellValue(item.GoodReceive.UpdateOn.ToString(new DateTimeFormat()));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(16);
				cell_detailGoodReceive.SetCellValue(new ResourceString("Eprocurement2012.Models.GoodReceive+GoodReceiveStatus." + item.GoodReceive.Status.ToString()));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;
				rowIndex++;
			}


			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "GoodReceive.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportGoodReceive(/*string[]Requester,string[] Approver,string[] Costcenter,*/AdvanceSearchField advanceSearchField)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new ReportRepository(connection).GetGoodReceiveOrderWithoutPaging(advanceSearchField, User.CurrentCompanyId, "", true);
				return ExportOrderGoodReceiveToExcel(orders);
			}
		}

		private ActionResult ExportOrderGoodReceiveToExcel(IEnumerable<Order> Orders)
		{
			//----------------Export to Excel-------------------
			HSSFWorkbook workbook = new HSSFWorkbook();
			Sheet sheet = workbook.CreateSheet("GoodReceive");

			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			CellStyle cellstyle_left = workbook.CreateCellStyle();

			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;

			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;

			int rowIndex = 0;

			Cell cell_headerGoodReceive;
			Row row_headerGoodReceive;
			row_headerGoodReceive = sheet.CreateRow(++rowIndex);

			int cell_width = 0;

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(0);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.CustID"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(0, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(1);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.OrderId"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(1, cell_width);


			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(2);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.OrderStatus"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 600;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(2, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(3);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.OrderDate"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(3, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(4);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewDataOrder.ApprovrOrderDate"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(4, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(5);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewOrder.RequesterName"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(5, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(6);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewOrder.EmailRequester"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(6, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(7);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewOrder.ApproverName"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(7, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(8);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Report.GoodReceiveOrder.EmailApprover"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(8, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(9);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ViewOrder.CostCenterName"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 800;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(9, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(10);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ConfirmOrder.Quantity"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 500;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(10, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(11);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.PartialReceive.ProductAction"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(11, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(12);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.PartialReceive.ProductCancel"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(12, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(13);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.PartialReceive.ProductRemain"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 300;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(13, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(14);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Order.ConfirmOrder.GrandTotalAmt"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 500;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(14, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(15);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Report.GoodReceiveOrder.UpdateOn"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 600;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(15, cell_width);

			cell_headerGoodReceive = row_headerGoodReceive.CreateCell(16);
			cell_headerGoodReceive.SetCellValue(new ResourceString("Report.GoodReceiveOrder.Status"));
			cell_width = cell_headerGoodReceive.RichStringCellValue.Length * 600;
			cell_headerGoodReceive.RichStringCellValue.ApplyFont(font);
			cell_headerGoodReceive.CellStyle = cellstyle;
			cell_headerGoodReceive.Sheet.SetColumnWidth(16, cell_width);

			++rowIndex;

			Cell cell_detailGoodReceive;
			Row row_detailGoodReceive;

			foreach (var item in Orders)
			{
				row_detailGoodReceive = sheet.CreateRow(rowIndex);
				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(0);
				cell_detailGoodReceive.SetCellValue(item.CustId);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(1);
				cell_detailGoodReceive.SetCellValue(item.OrderID);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(2);
				cell_detailGoodReceive.SetCellValue(new ResourceString("Eprocurement2012.Models.Order+OrderStatus." + item.Status.ToString()));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(3);
				cell_detailGoodReceive.SetCellValue(item.OrderDate.ToString(new DateTimeFormat()));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(4);
				cell_detailGoodReceive.SetCellValue(item.DisplayApproveOrderDate);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(5);
				cell_detailGoodReceive.SetCellValue(item.Requester.DisplayName);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(6);
				cell_detailGoodReceive.SetCellValue(item.Requester.UserId);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				if (item.PreviousApprover != null)
				{
					cell_detailGoodReceive = row_detailGoodReceive.CreateCell(7);
					cell_detailGoodReceive.SetCellValue(item.PreviousApprover.Approver.DisplayName);
					cell_detailGoodReceive.CellStyle = cellstyle_detail;
				}
				else
				{
					cell_detailGoodReceive = row_detailGoodReceive.CreateCell(7);
					cell_detailGoodReceive.SetCellValue(item.CurrentApprover.Approver.DisplayName);
					cell_detailGoodReceive.CellStyle = cellstyle_detail;
				}

				if (item.PreviousApprover != null)
				{
					cell_detailGoodReceive = row_detailGoodReceive.CreateCell(8);
					cell_detailGoodReceive.SetCellValue(item.PreviousApprover.Approver.UserId);
					cell_detailGoodReceive.CellStyle = cellstyle_detail;
				}
				else
				{
					cell_detailGoodReceive = row_detailGoodReceive.CreateCell(8);
					cell_detailGoodReceive.SetCellValue(item.CurrentApprover.Approver.UserId);
					cell_detailGoodReceive.CellStyle = cellstyle_detail;
				}
				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(9);
				cell_detailGoodReceive.SetCellValue("[ " + item.CostCenter.CostCenterID + " ] " + item.CostCenter.CostCenterName);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(10);
				cell_detailGoodReceive.SetCellValue(item.ItemCountOrder);
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(11);
				cell_detailGoodReceive.SetCellValue(Convert.ToDouble(item.GoodReceiveDetail.ActionQty));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(12);
				cell_detailGoodReceive.SetCellValue(Convert.ToDouble(item.GoodReceiveDetail.CancelQty));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(13);
				cell_detailGoodReceive.SetCellValue(Convert.ToDouble(item.ItemCountOrder - (item.GoodReceiveDetail.ActionQty + item.GoodReceiveDetail.CancelQty)));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(14);
				cell_detailGoodReceive.SetCellValue(Convert.ToDouble(item.GrandTotalAmt));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(15);
				cell_detailGoodReceive.SetCellValue(item.GoodReceive.UpdateOn.ToString(new DateTimeFormat()));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;

				cell_detailGoodReceive = row_detailGoodReceive.CreateCell(16);
				cell_detailGoodReceive.SetCellValue(new ResourceString("Eprocurement2012.Models.GoodReceive+GoodReceiveStatus." + item.GoodReceive.Status.ToString()));
				cell_detailGoodReceive.CellStyle = cellstyle_detail;
				rowIndex++;
			}


			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "GoodReceive.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportReportBudget()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				ReportBudget budget = new ReportBudget();
				budget.Company = new CompanyRepository(connection).GetCompanyDetail(User.CurrentCompanyId);
				if (budget.Company.BudgetLevelType == Company.BudgetLevel.Company)
				{
					budget.Budgets = reportRepo.GetReportBudget(User.CurrentCompanyId, Company.BudgetLevel.Company);
				}
				else if (budget.Company.BudgetLevelType == Company.BudgetLevel.Department)
				{
					budget.Budgets = reportRepo.GetReportBudget(User.CurrentCompanyId, Company.BudgetLevel.Department);
				}
				else if (budget.Company.BudgetLevelType == Company.BudgetLevel.Costcenter)
				{
					budget.Budgets = reportRepo.GetReportBudget(User.CurrentCompanyId, Company.BudgetLevel.Costcenter);
				}
				else
				{

				}
				return ExportBudgetToExcel(budget);
			}
		}

		private ActionResult ExportBudgetToExcel(ReportBudget budget)
		{
			//----------------Export to Excel-------------------
			HSSFWorkbook workbook = new HSSFWorkbook();
			Sheet sheet = workbook.CreateSheet("ReportBudget");

			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			CellStyle cellstyle_left = workbook.CreateCellStyle();

			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;

			cellstyle_left.Alignment = HorizontalAlignment.LEFT;
			cellstyle_left.BorderBottom = CellBorderType.THIN;
			cellstyle_left.BorderLeft = CellBorderType.THIN;
			cellstyle_left.BorderRight = CellBorderType.THIN;
			cellstyle_left.BorderTop = CellBorderType.THIN;


			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;


			int rowIndex = 0;

			Cell cell_company;
			Row row;

			row = sheet.CreateRow(rowIndex);

			cell_company = row.CreateCell(0);
			int cell_width = 0;
			cell_company.Sheet.SetColumnWidth(0, cell_width);
			cell_company.SetCellValue(new ResourceString("ReportBudget.CompanyBudget"));
			cell_width = cell_company.RichStringCellValue.Length * 800;
			cell_company.RichStringCellValue.ApplyFont(font);
			++rowIndex;

			rowIndex += 1;
			Cell cell_CompanyID;
			Row row_CompanyID;
			row_CompanyID = sheet.CreateRow(rowIndex);

			Cell cell_CompanyModel;
			Row row_CompanyModel;
			row_CompanyModel = sheet.CreateRow(++rowIndex);

			Cell cell_BudgetType;
			Row row_BudgetType;
			row_BudgetType = sheet.CreateRow(++rowIndex);

			Cell cell_PeroidType;
			Row row_PeroidType;
			row_PeroidType = sheet.CreateRow(++rowIndex);


			cell_CompanyID = row_CompanyID.CreateCell(0);
			cell_CompanyID.SetCellValue(new ResourceString("ReportBudget.CompanyId"));
			cell_CompanyID.CellStyle = cellstyle_detail;
			cell_CompanyID.RichStringCellValue.ApplyFont(font); cell_CompanyID.CellStyle = cellstyle_left;
			cell_CompanyID.Sheet.SetColumnWidth(0, cell_width);

			cell_CompanyID = row_CompanyID.CreateCell(1);
			cell_CompanyID.SetCellValue(budget.Company.CompanyId);
			cell_width = cell_CompanyID.RichStringCellValue.Length * 800;
			cell_CompanyID.CellStyle = cellstyle_detail;
			cell_CompanyID.Sheet.SetColumnWidth(1, cell_width);

			cell_CompanyModel = row_CompanyModel.CreateCell(0);
			cell_CompanyModel.SetCellValue(new ResourceString("ReportBudget.CompanyModel"));
			cell_CompanyModel.CellStyle = cellstyle_detail;
			cell_CompanyModel.RichStringCellValue.ApplyFont(font); cell_CompanyModel.CellStyle = cellstyle_left;

			cell_CompanyModel = row_CompanyModel.CreateCell(1);
			cell_CompanyModel.SetCellValue(budget.Company.IsCompModelThreeLevel ? new ResourceString("ReportBudget.Company.3Level") : new ResourceString("ReportBudget.Company.2Level"));
			cell_CompanyModel.CellStyle = cellstyle_detail;

			cell_BudgetType = row_BudgetType.CreateCell(0);
			cell_BudgetType.SetCellValue(new ResourceString("ReportBudget.BudgetLevelType"));
			cell_BudgetType.CellStyle = cellstyle_detail;
			cell_BudgetType.RichStringCellValue.ApplyFont(font); cell_BudgetType.CellStyle = cellstyle_left;

			cell_BudgetType = row_BudgetType.CreateCell(1);
			cell_BudgetType.SetCellValue(budget.Company.BudgetLevelType.ToString());
			cell_BudgetType.CellStyle = cellstyle_detail;

			cell_PeroidType = row_PeroidType.CreateCell(0);
			cell_PeroidType.SetCellValue(new ResourceString("ReportBudget.BudgetPeriod"));
			cell_PeroidType.CellStyle = cellstyle_detail;
			cell_PeroidType.RichStringCellValue.ApplyFont(font); cell_PeroidType.CellStyle = cellstyle_left;

			cell_PeroidType = row_PeroidType.CreateCell(1);
			cell_PeroidType.SetCellValue(budget.Company.BudgetPeriodType.ToString());
			cell_PeroidType.CellStyle = cellstyle_detail;
			++rowIndex;


			Cell cell_headerBudgetCompany;
			Row row_headerBudgetCompany;
			row_headerBudgetCompany = sheet.CreateRow(++rowIndex);


			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Company)
			{
				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(0);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CompanyId"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(0, cell_width);

				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(1);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CompanyThaiName"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(1, cell_width);

				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(2);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CompanyEngName"));
				cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(2, cell_width);
			}

			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Costcenter)
			{
				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(0);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CostCenterID"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(0, cell_width);

				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(1);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CostCenterThaiName"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(1, cell_width);

				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(2);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CostCenterEngName"));
				cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(2, cell_width);
			}

			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Department)
			{
				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(0);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.DepartmentID"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(0, cell_width);

				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(1);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.DepartmentThaiName"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(1, cell_width);

				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(2);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.DepartmentEngName"));
				cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(2, cell_width);
			}

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(3);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.BudgetPeriod"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 500;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(3, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(4);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.BudgetYear"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 800;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(4, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(5);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.OriginalAmt"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(5, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(6);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.BudgetAmt"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 500;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(6, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(7);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.UsedAmt"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(7, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(8);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.RemainBudgetAmt"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(8, cell_width);

			++rowIndex;

			Cell cell_detailBudgetCompany;
			Row row_detailBudgetCompany;
			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Company)
			{
				foreach (var item in budget.Budgets)
				{
					row_detailBudgetCompany = sheet.CreateRow(rowIndex);

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(0);
					cell_detailBudgetCompany.SetCellValue(budget.Company.CompanyId);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(1);
					cell_detailBudgetCompany.SetCellValue(budget.Company.CompanyThaiName);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(2);
					cell_detailBudgetCompany.SetCellValue(budget.Company.CompanyEngName);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(3);
					cell_detailBudgetCompany.SetCellValue(item.Budget.DisplayPeriodNo);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(4);
					cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.Year));
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(5);
					cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.OriginalAmt));
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(6);
					cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.BudgetAmt));
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(7);
					cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.UsedAmt));
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(8);
					cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.RemainBudgetAmt));
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;
					rowIndex++;
				}
			}


			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Costcenter)
			{
				foreach (var item in budget.Budgets.GroupBy(g => g.CostCenter.CostCenterID))
				{
					row_detailBudgetCompany = sheet.CreateRow(rowIndex);
					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(0);
					cell_detailBudgetCompany.SetCellValue(item.First().CostCenter.CostCenterID);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(1);
					cell_detailBudgetCompany.SetCellValue(item.First().CostCenter.CostCenterThaiName);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(2);
					cell_detailBudgetCompany.SetCellValue(item.First().CostCenter.CostCenterEngName);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;
					rowIndex++;

					foreach (var budgetCostCenter in item)
					{
						row_detailBudgetCompany = sheet.CreateRow(rowIndex);
						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(3);
						cell_detailBudgetCompany.SetCellValue(budgetCostCenter.Budget.DisplayPeriodNo);
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(4);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetCostCenter.Budget.Year));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(5);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetCostCenter.Budget.OriginalAmt));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(6);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetCostCenter.Budget.BudgetAmt));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(7);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetCostCenter.Budget.UsedAmt));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(8);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetCostCenter.Budget.RemainBudgetAmt));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;
						rowIndex++;
					}
				}
			}

			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Department)
			{
				foreach (var item in budget.Budgets.GroupBy(g => g.Department.DepartmentID))
				{
					row_detailBudgetCompany = sheet.CreateRow(rowIndex);
					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(0);
					cell_detailBudgetCompany.SetCellValue(item.First().Department.DepartmentID);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(1);
					cell_detailBudgetCompany.SetCellValue(item.First().Department.DepartmentThaiName);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;

					cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(2);
					cell_detailBudgetCompany.SetCellValue(item.First().Department.DepartmentEngName);
					cell_detailBudgetCompany.CellStyle = cellstyle_detail;
					rowIndex++;

					foreach (var budgetDepartment in item)
					{
						row_detailBudgetCompany = sheet.CreateRow(rowIndex);
						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(3);
						cell_detailBudgetCompany.SetCellValue(budgetDepartment.Budget.DisplayPeriodNo);
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(4);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetDepartment.Budget.Year));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(5);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetDepartment.Budget.OriginalAmt));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(6);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetDepartment.Budget.BudgetAmt));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(7);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetDepartment.Budget.UsedAmt));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;

						cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(8);
						cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(budgetDepartment.Budget.RemainBudgetAmt));
						cell_detailBudgetCompany.CellStyle = cellstyle_detail;
						rowIndex++;
					}
				}
			}


			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "ReportBudget.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportReportBudgetDetail(string groupId, string periodNo, string year)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				ReportBudget budget = new ReportBudget();
				budget.Company = new CompanyRepository(connection).GetCompanyDetail(User.CurrentCompanyId);
				budget.Budgets = reportRepo.GetReportBudgetDetail(User.CurrentCompanyId, groupId, periodNo, year);
				return ExportReportBudgetDetailToExcel(budget);
			}
		}

		private ActionResult ExportReportBudgetDetailToExcel(ReportBudget budget)
		{
			//----------------Export to Excel-------------------
			HSSFWorkbook workbook = new HSSFWorkbook();
			Sheet sheet = workbook.CreateSheet("ReportBudgetDetail");

			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();

			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;


			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;


			int rowIndex = 0;

			Cell cell_company;
			Row row;

			row = sheet.CreateRow(rowIndex);

			cell_company = row.CreateCell(0);
			int cell_width = 0;
			cell_company.Sheet.SetColumnWidth(0, cell_width);
			cell_company.SetCellValue(new ResourceString("ReportBudget.DetailBudget"));
			cell_width = cell_company.RichStringCellValue.Length * 300;
			cell_company.RichStringCellValue.ApplyFont(font);
			++rowIndex;

			Cell cell_headerBudgetCompany;
			Row row_headerBudgetCompany;
			row_headerBudgetCompany = sheet.CreateRow(++rowIndex);


			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Company)
			{
				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(0);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CompanyId"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(0, cell_width);
			}

			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Costcenter)
			{
				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(0);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CostCenterID"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(0, cell_width);
			}

			if (budget.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Department)
			{
				cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(0);
				cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.DepartmentID"));
				cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
				cell_headerBudgetCompany.Sheet.SetColumnWidth(0, cell_width);
			}

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(1);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.BudgetPeriod"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 500;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(1, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(2);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.BudgetYear"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 800;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(2, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(3);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.BudgetAmt"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(3, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(4);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.BudgetIncAmt"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 500;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(4, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(5);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.UsedAmt"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(5, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(6);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.RemainBudgetAmt"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(6, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(7);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.OrderId"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(7, cell_width);

			cell_headerBudgetCompany = row_headerBudgetCompany.CreateCell(8);
			cell_headerBudgetCompany.SetCellValue(new ResourceString("ReportBudget.CreateOn"));
			cell_width = cell_headerBudgetCompany.RichStringCellValue.Length * 300;
			cell_headerBudgetCompany.RichStringCellValue.ApplyFont(font); cell_headerBudgetCompany.CellStyle = cellstyle;
			cell_headerBudgetCompany.Sheet.SetColumnWidth(8, cell_width);

			++rowIndex;

			Cell cell_detailBudgetCompany;
			Row row_detailBudgetCompany;

			foreach (var item in budget.Budgets)
			{
				row_detailBudgetCompany = sheet.CreateRow(rowIndex);

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(0);
				cell_detailBudgetCompany.SetCellValue(item.Budget.GroupID);
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(1);
				cell_detailBudgetCompany.SetCellValue(item.Budget.DisplayPeriodNo);
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(2);
				cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.Year));
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(3);
				cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.OriginalAmt));
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(4);
				cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.BudgetAmt));
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(5);
				cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.UsedAmt));
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(6);
				cell_detailBudgetCompany.SetCellValue(Convert.ToDouble(item.Budget.RemainBudgetAmt));
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(7);
				cell_detailBudgetCompany.SetCellValue(item.Order.OrderID);
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				cell_detailBudgetCompany = row_detailBudgetCompany.CreateCell(8);
				cell_detailBudgetCompany.SetCellValue(item.Budget.CreateOn.ToString(new DateFormat()));
				cell_detailBudgetCompany.CellStyle = cellstyle_detail;

				rowIndex++;
			}



			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "ReportBudgetDetail.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportSearchProduct(SearchProduct searchProduct)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<ProductCatalog> searchProductData = new ProductCatalogRepository(connection).GetAllProductBySearch(searchProduct);
				return ExportSeachProductToExcel(searchProductData);
			}
		}

		private ActionResult ExportSeachProductToExcel(IEnumerable<ProductCatalog> searchProductData)
		{
			//----------------Export to Excel-------------------
			HSSFWorkbook workbook = new HSSFWorkbook();
			Sheet sheet = workbook.CreateSheet("Product");

			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			CellStyle cellstyle_left = workbook.CreateCellStyle();

			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;

			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;

			int rowIndex = 0;

			Cell cell_headerProduct;
			Row row_headerProduct;
			row_headerProduct = sheet.CreateRow(++rowIndex);

			int cell_width = 0;

			cell_headerProduct = row_headerProduct.CreateCell(0);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.CompanyId"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(0, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(1);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.ProductId"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(1, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(2);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.ThaiName"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 600;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(2, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(3);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.EngName"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 600;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(3, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(4);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.ThaiUnit"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(4, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(5);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.EngUnit"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(5, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(6);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.PriceExcVat"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(6, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(7);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.PriceIncVat"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(7, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(8);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.PriceType"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(8, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(9);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.IsBestDeal"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(9, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(10);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.IsVat"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(10, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(11);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.AddProdcut"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 500;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(11, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(12);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.PeriodOrder"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(12, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(13);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.CreateOn"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 300;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(13, cell_width);

			cell_headerProduct = row_headerProduct.CreateCell(14);
			cell_headerProduct.SetCellValue(new ResourceString("OFMAdmin.SearchProduct.CreateBy"));
			cell_width = cell_headerProduct.RichStringCellValue.Length * 500;
			cell_headerProduct.RichStringCellValue.ApplyFont(font);
			cell_headerProduct.CellStyle = cellstyle;
			cell_headerProduct.Sheet.SetColumnWidth(14, cell_width);

			++rowIndex;

			Cell cell_detailProduct;
			Row row_detailProduct;

			foreach (var item in searchProductData)
			{
				row_detailProduct = sheet.CreateRow(rowIndex);

				cell_detailProduct = row_detailProduct.CreateCell(0);
				cell_detailProduct.SetCellValue(item.Company.CompanyId);
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(1);
				cell_detailProduct.SetCellValue(item.ProductDetail.Id);
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(2);
				cell_detailProduct.SetCellValue(item.ProductDetail.ThaiName);
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(3);
				cell_detailProduct.SetCellValue(item.ProductDetail.EngName);
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(4);
				cell_detailProduct.SetCellValue(item.ProductDetail.ThaiUnit);
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(5);
				cell_detailProduct.SetCellValue(item.ProductDetail.EngUnit);
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(6);
				cell_detailProduct.SetCellValue(Convert.ToDouble(item.ProductDetail.PriceExcVat));
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(7);
				cell_detailProduct.SetCellValue(Convert.ToDouble(item.ProductDetail.PriceIncVat));
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(8);
				cell_detailProduct.SetCellValue(item.ProductDetail.PriceType.ToString());
				cell_detailProduct.CellStyle = cellstyle_detail;

				if (item.ProductDetail.IsBestDeal)
				{
					cell_detailProduct = row_detailProduct.CreateCell(9);
					cell_detailProduct.SetCellValue("Yes");
					cell_detailProduct.CellStyle = cellstyle_detail;
				}
				else
				{
					cell_detailProduct = row_detailProduct.CreateCell(9);
					cell_detailProduct.SetCellValue("No");
					cell_detailProduct.CellStyle = cellstyle_detail;
				}

				if (item.ProductDetail.IsVat)
				{
					cell_detailProduct = row_detailProduct.CreateCell(10);
					cell_detailProduct.SetCellValue("Yes");
					cell_detailProduct.CellStyle = cellstyle_detail;
				}
				else
				{
					cell_detailProduct = row_detailProduct.CreateCell(10);
					cell_detailProduct.SetCellValue("No");
					cell_detailProduct.CellStyle = cellstyle_detail;
				}


				cell_detailProduct = row_detailProduct.CreateCell(11);
				cell_detailProduct.SetCellValue(item.CatalogTypeId);
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(12);
				cell_detailProduct.SetCellValue(item.ProductDetail.ValidFrom.ToString(new DateFormat()) + " - " + item.ProductDetail.ValidTo.ToString(new DateFormat()));
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(13);
				cell_detailProduct.SetCellValue(item.CreateOn.ToString(new DateFormat()));
				cell_detailProduct.CellStyle = cellstyle_detail;

				cell_detailProduct = row_detailProduct.CreateCell(14);
				cell_detailProduct.SetCellValue(item.CreateBy);
				cell_detailProduct.CellStyle = cellstyle_detail;

				rowIndex++;
			}

			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "product.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportViewOrder(string orderGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
				return ExportOrderToExcel(orderData);
			}
		}

		private ActionResult ExportOrderToExcel(OrderData orderData)
		{
			//----------------Export to Excel-------------------
			var workbook = new HSSFWorkbook();
			var sheet = workbook.CreateSheet("Order");
			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			CellStyle cellstyle_left = workbook.CreateCellStyle();
			CellStyle cellstyle_right = workbook.CreateCellStyle();
			CellStyle cellstyle_center = workbook.CreateCellStyle();
			CellStyle cellstyle_textcenter = workbook.CreateCellStyle();
			CellStyle cellstyle_textright = workbook.CreateCellStyle();

			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;


			cellstyle_right.BorderBottom = CellBorderType.THIN;
			cellstyle_right.BorderLeft = CellBorderType.THIN;
			cellstyle_right.BorderRight = CellBorderType.THIN;
			cellstyle_right.BorderTop = CellBorderType.THIN;
			cellstyle_right.WrapText = false;
			cellstyle_right.Alignment = HorizontalAlignment.RIGHT;

			cellstyle_textright.WrapText = false;
			cellstyle_textright.Alignment = HorizontalAlignment.RIGHT;

			cellstyle_center.BorderBottom = CellBorderType.THIN;
			cellstyle_center.BorderLeft = CellBorderType.THIN;
			cellstyle_center.BorderRight = CellBorderType.THIN;
			cellstyle_center.BorderTop = CellBorderType.THIN;
			cellstyle_center.WrapText = false;
			cellstyle_center.Alignment = HorizontalAlignment.CENTER;

			cellstyle_left.BorderBottom = CellBorderType.THIN;
			cellstyle_left.BorderLeft = CellBorderType.THIN;
			cellstyle_left.BorderRight = CellBorderType.THIN;
			cellstyle_left.BorderTop = CellBorderType.THIN;
			cellstyle_left.WrapText = false;
			cellstyle_left.Alignment = HorizontalAlignment.LEFT;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;

			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;

			int rowIndex = 0;
			int cell_width = 0;

			Cell cell_header;
			Row row_header;

			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("เลขที่ใบสั่งซื้อ :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.OrderID);
			cell_width = cell_header.RichStringCellValue.Length * 800;

			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("รหัสลูกค้า :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.CostCenter.CostCenterCustID);
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString("รหัสองค์กร :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(orderData.Order.Company.CompanyId);
			cell_width = cell_header.RichStringCellValue.Length * 800;


			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("ติดต่อ :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.Contact.ContactorName);
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString("ชื่อองค์กร :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(orderData.Order.Company.CompanyName);
			cell_width = cell_header.RichStringCellValue.Length * 800;


			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("รหัสผู้ใช้งาน :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.Requester.UserId);
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString("ฝ่าย :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(string.IsNullOrEmpty(orderData.Order.Department.DepartmentID) ? "-" : "[" + orderData.Order.Department.DepartmentID + "]" + orderData.Order.Department.DepartmentName);
			cell_width = cell_header.RichStringCellValue.Length * 800;


			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("เบอร์โทรศัพท์ :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.Contact.ContactorPhone + (string.IsNullOrEmpty(orderData.Order.Contact.ContactorExtension) ? "" : "#" + orderData.Order.Contact.ContactorExtension));
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString("หน่วยงาน/แผนก :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue("[" + orderData.Order.CostCenter.CostCenterID + "]" + orderData.Order.CostCenter.CostCenterName);
			cell_width = cell_header.RichStringCellValue.Length * 800;

			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("เบอร์โทรสาร :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.Contact.ContactorFax);
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString("วันที่สั่งซื้อ :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(orderData.Order.OrderDate.ToString(new DateTimeFormat()));
			cell_width = cell_header.RichStringCellValue.Length * 800;


			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("ที่อยู่ใบกำกับภาษี :"));
			cell_width = cell_header.RichStringCellValue.Length * 300;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.Invoice.Address1 + orderData.Order.Invoice.Address2 + orderData.Order.Invoice.Address3 + orderData.Order.Invoice.Address4);
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString("ข้อมูลสถานที่จัดส่ง :"));
			cell_width = cell_header.RichStringCellValue.Length * 300;
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(orderData.Order.Shipping.Address1 + orderData.Order.Shipping.Address2 + orderData.Order.Shipping.Address3 + orderData.Order.Shipping.Address4);
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header = row_header.CreateCell(5);


			rowIndex = rowIndex + 2;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("รายการสินค้าที่สั่งซื้อ"));


			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("รหัสสินค้า"));
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header.RichStringCellValue.ApplyFont(font);
			cell_header.CellStyle = cellstyle;
			cell_header.Sheet.SetColumnWidth(0, cell_width);

			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(new ResourceString("รายการ"));
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header.RichStringCellValue.ApplyFont(font);
			cell_header.CellStyle = cellstyle;
			cell_header.Sheet.SetColumnWidth(1, cell_width);

			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString("ราคาสินค้า(รวมภาษี)"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header.RichStringCellValue.ApplyFont(font);
			cell_header.CellStyle = cellstyle;
			cell_header.Sheet.SetColumnWidth(2, cell_width);

			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString("ราคาสินค้า(ไม่รวมภาษี)"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header.RichStringCellValue.ApplyFont(font);
			cell_header.CellStyle = cellstyle;
			cell_header.Sheet.SetColumnWidth(3, cell_width);

			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(new ResourceString("จำนวน"));
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header.RichStringCellValue.ApplyFont(font);
			cell_header.CellStyle = cellstyle;
			cell_header.Sheet.SetColumnWidth(4, cell_width);

			cell_header = row_header.CreateCell(5);
			cell_header.SetCellValue(new ResourceString("หน่วย"));
			cell_width = cell_header.RichStringCellValue.Length * 1000;
			cell_header.RichStringCellValue.ApplyFont(font);
			cell_header.CellStyle = cellstyle;
			cell_header.Sheet.SetColumnWidth(5, cell_width);

			cell_header = row_header.CreateCell(6);
			cell_header.SetCellValue(new ResourceString("ราคาสุทธิ"));
			cell_width = cell_header.RichStringCellValue.Length * 800;
			cell_header.RichStringCellValue.ApplyFont(font);
			cell_header.CellStyle = cellstyle;
			cell_header.Sheet.SetColumnWidth(6, cell_width);


			rowIndex++;
			foreach (var item in orderData.Order.ItemOrders)
			{
				row_header = sheet.CreateRow(rowIndex);
				cell_header = row_header.CreateCell(0);
				cell_header.SetCellValue(item.Product.Id);
				cell_width = cell_header.RichStringCellValue.Length * 500;
				cell_header.CellStyle = cellstyle_left;

				cell_header = row_header.CreateCell(1);
				cell_header.SetCellValue(item.Product.Name);
				cell_header.CellStyle = cellstyle_left;

				cell_header = row_header.CreateCell(2);
				cell_header.SetCellValue(Convert.ToDouble(item.ItemIncVatPrice));
				cell_header.CellStyle = cellstyle_right;

				cell_header = row_header.CreateCell(3);
				cell_header.CellStyle = cellstyle_right;
				cell_header.SetCellValue(Convert.ToDouble(item.ItemExcVatPrice));
				cell_header.CellStyle = cellstyle_right;

				cell_header = row_header.CreateCell(4);
				cell_header.SetCellValue(item.Quantity);
				cell_header.CellStyle = cellstyle_center;

				cell_header = row_header.CreateCell(5);
				cell_header.SetCellValue(item.Product.Unit);
				cell_header.CellStyle = cellstyle_center;

				cell_header = row_header.CreateCell(6);
				cell_header.SetCellValue(Convert.ToDouble(item.ItemPriceNet));
				cell_header.CellStyle = cellstyle_right;
				rowIndex++;
			}

			int rowTotal = 0;
			row_header = sheet.CreateRow(rowIndex);
			rowTotal = rowIndex;
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("ราคาสุทธิ "));
			cell_header.CellStyle = cellstyle_right;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header.CellStyle = cellstyle_center;
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header.CellStyle = cellstyle_center;
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header.CellStyle = cellstyle_center;
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(orderData.Order.ItemCountOrder);
			cell_header.CellStyle = cellstyle_center;
			cell_header = row_header.CreateCell(5);
			cell_header.SetCellValue(new ResourceString("ชิ้น"));
			cell_header.CellStyle = cellstyle_center;
			cell_header = row_header.CreateCell(6);
			cell_header.SetCellValue(Convert.ToDouble(orderData.Order.TotalNetAmt));
			cell_header.CellStyle = cellstyle_right;

			rowIndex = rowIndex + 2;

			if (orderData.Order.TotalDeliveryFee > 0)
			{
				rowIndex++;
				row_header = sheet.CreateRow(rowIndex);
				cell_header = row_header.CreateCell(0);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(1);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(2);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(3);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(4);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(5);
				cell_header.SetCellValue(new ResourceString("ค่าขนส่งพิเศษ"));
				cell_width = cell_header.RichStringCellValue.Length * 500;
				cell_header = row_header.CreateCell(6);
				cell_header.SetCellValue(Convert.ToDouble(orderData.Order.TotalDeliveryFee));
				//cell_width = cell_header.RichStringCellValue.Length * 500;
				cell_header.CellStyle = cellstyle_textright;
			}

			if (orderData.Order.TotalDeliveryCharge > 0)
			{
				rowIndex++;
				row_header = sheet.CreateRow(rowIndex);
				cell_header = row_header.CreateCell(0);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(1);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(2);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(3);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(4);
				cell_header.SetCellValue(new ResourceString(""));
				cell_header = row_header.CreateCell(5);
				cell_header.SetCellValue(new ResourceString("ค่าขนส่งพิเศษ"));
				cell_width = cell_header.RichStringCellValue.Length * 500;
				cell_header = row_header.CreateCell(6);
				cell_header.SetCellValue(Convert.ToDouble(orderData.Order.TotalDeliveryCharge));
				//cell_width = cell_header.RichStringCellValue.Length * 500;
				cell_header.CellStyle = cellstyle_textright;
			}

			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(5);
			cell_header.SetCellValue(new ResourceString("ราคาสุทธิสินค้ายกเว้นภาษี"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(6);
			cell_header.SetCellValue(Convert.ToDouble(orderData.Order.TotalPriceProductNoneVatAmount));
			//cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header.CellStyle = cellstyle_textright;

			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(5);
			cell_header.SetCellValue(new ResourceString("ราคาสุทธิสินค้าเสียภาษี"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(6);
			cell_header.SetCellValue(Convert.ToDouble(orderData.Order.TotalPriceProductExcVatAmount));
			//cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header.CellStyle = cellstyle_textright;

			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(5);
			cell_header.SetCellValue(new ResourceString("ภาษีมูลค่าเพิ่ม 7%"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(6);
			cell_header.SetCellValue(Convert.ToDouble(orderData.Order.TotalVatAmt));
			//cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header.CellStyle = cellstyle_textright;


			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(5);
			cell_header.SetCellValue(new ResourceString("จำนวนรวมทั้งสิ้น"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(6);
			cell_header.SetCellValue(Convert.ToDouble(orderData.Order.GrandTotalAmt));
			//cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header.CellStyle = cellstyle_textright;

			rowIndex = rowIndex + 2;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("หมายเหตุอื่นๆ"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(2);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(3);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(4);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(5);
			cell_header.SetCellValue(new ResourceString(""));
			cell_header = row_header.CreateCell(6);
			cell_header.SetCellValue(new ResourceString(""));

			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("ข้อความถึงผู้อนุมัติ :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.ApproverRemark);
			cell_width = cell_header.RichStringCellValue.Length * 500;


			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("ข้อความถึงออฟฟิศเมท :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.OFMRemark);
			cell_width = cell_header.RichStringCellValue.Length * 500;


			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("เลขที่เอกสารอ้างอิง(กรณีที่มี) :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(orderData.Order.ReferenceRemark);
			cell_width = cell_header.RichStringCellValue.Length * 500;



			rowIndex++;
			row_header = sheet.CreateRow(rowIndex);
			cell_header = row_header.CreateCell(0);
			cell_header.SetCellValue(new ResourceString("เอกสารแนบ :"));
			cell_width = cell_header.RichStringCellValue.Length * 500;
			cell_header = row_header.CreateCell(1);
			cell_header.SetCellValue(string.IsNullOrEmpty(orderData.Order.CustFileName) ? "-" : orderData.Order.CustFileName);
			cell_width = cell_header.RichStringCellValue.Length * 500;


			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(0, 0, 1, 6));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(1, 1, 1, 2));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(1, 1, 4, 6));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(2, 2, 1, 2));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(2, 2, 4, 6));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(3, 3, 1, 2));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(3, 3, 4, 6));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(4, 4, 1, 2));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(4, 4, 4, 6));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(5, 5, 1, 2));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(5, 5, 4, 6));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(6, 6, 1, 2));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(6, 6, 4, 6));
			sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(rowTotal, rowTotal, 0, 1));



			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "exportOrder.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

		public ActionResult ExportSupplier(AdvanceSearchField advanceSearchField,string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<ProductSupplier> ListProductSuppliers = new ReportRepository(connection).GetProductSupplier(advanceSearchField, companyId);
				return ExportSupplierToExcel(ListProductSuppliers);
			}
		}


		private ActionResult ExportSupplierToExcel(IEnumerable<ProductSupplier> ListProductSuppliers)
		{//----------------Export to Excel-------------------
			var workbook = new HSSFWorkbook();

			CellStyle cellstyle = workbook.CreateCellStyle();
			CellStyle cellstyle_detail = workbook.CreateCellStyle();
			CellStyle cellstyle_left = workbook.CreateCellStyle();
			CellStyle cellstyle_right = workbook.CreateCellStyle();
			CellStyle cellstyle_center = workbook.CreateCellStyle();
			CellStyle cellstyle_textcenter = workbook.CreateCellStyle();
			CellStyle cellstyle_textright = workbook.CreateCellStyle();
			CellStyle cellstyle_middle = workbook.CreateCellStyle();

			cellstyle.Alignment = HorizontalAlignment.CENTER_SELECTION;
			cellstyle.BorderBottom = CellBorderType.THIN;
			cellstyle.BorderLeft = CellBorderType.THIN;
			cellstyle.BorderRight = CellBorderType.THIN;
			cellstyle.BorderTop = CellBorderType.THIN;

			cellstyle_middle.BorderBottom = CellBorderType.THIN;
			cellstyle_middle.BorderLeft = CellBorderType.THIN;
			cellstyle_middle.BorderRight = CellBorderType.THIN;
			cellstyle_middle.BorderTop = CellBorderType.THIN;
			cellstyle_middle.Alignment = HorizontalAlignment.CENTER;
			cellstyle_middle.VerticalAlignment = VerticalAlignment.CENTER;

			cellstyle_right.BorderBottom = CellBorderType.THIN;
			cellstyle_right.BorderLeft = CellBorderType.THIN;
			cellstyle_right.BorderRight = CellBorderType.THIN;
			cellstyle_right.BorderTop = CellBorderType.THIN;
			cellstyle_right.WrapText = false;
			cellstyle_right.Alignment = HorizontalAlignment.RIGHT;

			cellstyle_textright.WrapText = false;
			cellstyle_textright.Alignment = HorizontalAlignment.RIGHT;

			cellstyle_center.BorderBottom = CellBorderType.THIN;
			cellstyle_center.BorderLeft = CellBorderType.THIN;
			cellstyle_center.BorderRight = CellBorderType.THIN;
			cellstyle_center.BorderTop = CellBorderType.THIN;
			cellstyle_center.WrapText = false;
			cellstyle_center.Alignment = HorizontalAlignment.CENTER;

			cellstyle_left.BorderBottom = CellBorderType.THIN;
			cellstyle_left.BorderLeft = CellBorderType.THIN;
			cellstyle_left.BorderRight = CellBorderType.THIN;
			cellstyle_left.BorderTop = CellBorderType.THIN;
			cellstyle_left.WrapText = false;
			cellstyle_left.Alignment = HorizontalAlignment.LEFT;

			cellstyle_detail.BorderBottom = CellBorderType.THIN;
			cellstyle_detail.BorderLeft = CellBorderType.THIN;
			cellstyle_detail.BorderRight = CellBorderType.THIN;
			cellstyle_detail.BorderTop = CellBorderType.THIN;
			cellstyle_detail.WrapText = false;

			Font font = workbook.CreateFont();
			font.Boldweight = (int)FontBoldWeight.BOLD;


			//Head
			foreach (var ListProduct in ListProductSuppliers.GroupBy(s => s.SupplierID))
			{
				int ratio = 3;
				int countProduct = 0;

				var sheet = workbook.CreateSheet(ListProduct.FirstOrDefault().SupplierID == string.Empty ? "ไม่มีรหัส" : ListProduct.FirstOrDefault().SupplierID);
				var rowIndex = 0;
				var row = sheet.CreateRow(rowIndex);
				//int cell_width = 0;

				Cell cell_header;
				Row row_header;

				int column = 0;

				row_header = sheet.CreateRow(rowIndex);
				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue(ListProduct.FirstOrDefault().DisplaySupplier);
				cell_header.RichStringCellValue.ApplyFont(font);


				rowIndex = rowIndex + 2;//สร้าง row เปล่า 1 row

				row_header = sheet.CreateRow(rowIndex);
				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("BRANCH");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				cell_header.CellStyle = cellstyle_middle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(2, 4, 0, 3));


				foreach (var pid in ListProduct.GroupBy(p => p.OrderDetail.Product.Id).OrderBy(p => p.Key))
				{
					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue(pid.Key);
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;

					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue("");
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;

					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue("");
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;

					sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(2, 2, 4 + (ratio * countProduct), 6 + (ratio * countProduct)));
					countProduct++;
				}

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("Grand Total");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				cell_header.CellStyle = cellstyle_middle;


				sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(2, 4, 4 + (ratio * countProduct), 4 + (ratio * countProduct)));


				rowIndex++;
				column = 0; //รีเซ็ตค่า
				countProduct = 0; //รีเซ็ตค่า Merge

				row_header = sheet.CreateRow(rowIndex);
				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				foreach (var pid in ListProduct.GroupBy(p => p.OrderDetail.Product.Id).OrderBy(p => p.Key))
				{
					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue(pid.FirstOrDefault().OrderDetail.Product.Name);
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;

					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue("");
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;


					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue("");
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;

					sheet.AddMergedRegion(new NPOI.SS.Util.CellRangeAddress(3, 3, 4 + (ratio * countProduct), 6 + (ratio * countProduct)));
					countProduct++;

				}

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;

				rowIndex++;

				column = 0; //รีเซ็ตค่า

				row_header = sheet.CreateRow(rowIndex);
				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;
				column++;

				foreach (var pid in ListProduct.GroupBy(p => p.OrderDetail.Product.Id).OrderBy(p => p.Key))
				{
					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue("Cost");
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;

					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue("Qty");
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;

					cell_header = row_header.CreateCell(column);
					cell_header.SetCellValue("Amount");
					cell_header.RichStringCellValue.ApplyFont(font);
					cell_header.CellStyle = cellstyle;
					column++;

				}

				cell_header = row_header.CreateCell(column);
				cell_header.SetCellValue("");
				cell_header.RichStringCellValue.ApplyFont(font);
				cell_header.CellStyle = cellstyle;

				rowIndex++;

				//Body
				Cell cell_detail;
				Row row_detail;
				int count = 0;
				


				foreach (var branch in ListProduct.GroupBy(p => p.Order.CostCenter.CostCenterID).OrderBy(p => p.Key))
				{
					int seq = 0;
					column = 0;
					row_detail = sheet.CreateRow(rowIndex);
					cell_detail = row_detail.CreateCell(column);
					cell_detail.SetCellValue(++count);
					cell_detail.CellStyle = cellstyle_detail;
					cell_detail.CellStyle = cellstyle_center;
					column++;

					cell_detail = row_detail.CreateCell(column);
					cell_detail.SetCellValue(branch.Key);
					cell_detail.CellStyle = cellstyle_detail;
					cell_detail.CellStyle = cellstyle_center;
					column++;

					cell_detail = row_detail.CreateCell(column);
					cell_detail.SetCellValue(branch.FirstOrDefault().Order.CostCenter.OracleCode);
					cell_detail.CellStyle = cellstyle_detail;
					cell_detail.CellStyle = cellstyle_left;
					column++;

					cell_detail = row_detail.CreateCell(column);
					cell_detail.SetCellValue(branch.FirstOrDefault().Order.CostCenter.CostCenterName);
					cell_detail.CellStyle = cellstyle_detail;
					cell_detail.CellStyle = cellstyle_left;
					column++;


					foreach (var pid in ListProduct.GroupBy(p => p.OrderDetail.Product.Id).OrderBy(p => p.Key))
					{
						cell_detail = row_detail.CreateCell(column);
						cell_detail.SetCellValue(Convert.ToDouble(pid.FirstOrDefault().OrderDetail.ItemExcVatPrice));
						cell_detail.CellStyle = cellstyle_detail;
						cell_detail.CellStyle = cellstyle_center;
						column++;

						if (pid.Any(c => c.Order.CostCenter.CostCenterID == branch.Key))
						{
							cell_detail = row_detail.CreateCell(column);
							cell_detail.SetCellValue(Convert.ToDouble(pid.Where(p => p.Order.CostCenter.CostCenterID == branch.Key).Sum(p => p.OrderDetail.Quantity)));
							cell_detail.CellStyle = cellstyle_detail;
							cell_detail.CellStyle = cellstyle_center;
							column++;

							cell_detail = row_detail.CreateCell(column);
							cell_detail.SetCellValue(Convert.ToDouble(pid.Where(p => p.Order.CostCenter.CostCenterID == branch.Key).Sum(p => p.OrderDetail.ItemExcVatPrice * p.OrderDetail.Quantity)));

							cell_detail.CellStyle = cellstyle_detail;
							cell_detail.CellStyle = cellstyle_right;
							column++;
						}
						else
						{
							cell_detail = row_detail.CreateCell(column);
							cell_detail.SetCellValue("-");
							cell_detail.CellStyle = cellstyle_detail;
							cell_detail.CellStyle = cellstyle_center;
							column++;

							cell_detail = row_detail.CreateCell(column);
							cell_detail.SetCellValue("-");
							cell_detail.CellStyle = cellstyle_detail;
							cell_detail.CellStyle = cellstyle_center;
							column++;
						}
					}

					cell_detail = row_detail.CreateCell(column);
					cell_detail.SetCellValue(Convert.ToDouble(branch.Sum(p => p.OrderDetail.ItemExcVatPrice * p.OrderDetail.Quantity)));
					cell_detail.CellStyle = cellstyle_detail;
					cell_detail.CellStyle = cellstyle_right;
					column++;
					seq++;
					rowIndex++;
				}
				rowIndex++;
			}




			using (var exportData = new MemoryStream())
			{
				workbook.Write(exportData);
				string saveAsFileName = "Supplier.xls";
				Response.ContentType = "application/vnd.ms-excel";
				Response.AddHeader("Content-Disposition", string.Format("attachment;filename={0}", saveAsFileName));
				Response.Clear();
				Response.BinaryWrite(exportData.GetBuffer());
				Response.End();
				return new EmptyResult();
			}
			//--------------------End Export---------------------------
		}

	}
}
