﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Controllers.Helpers;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
using System.IO;

namespace Eprocurement2012.Controllers
{
	public class ReviseController : AuthenticateRoleController
	{

		public ActionResult ViewReviseOrder(string Id)
		{
			if (string.IsNullOrEmpty(Id)) { throw new ArgumentNullException("Id"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				CartRevise cartRevise = new CartRevise();
				string orderId = orderRePo.GetOrderIdByGuid(Id); // หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				cartRevise.Order = orderRePo.GetMyOrder(orderId); // นำ OrderId ส่งไปดึงข้อมูล Order และส่งไปแสดงที่ ViewOrder
				cartRevise.OrderActivity = orderRePo.GetOrderActivity(orderId); // ดึงข้อมูล Activity ของ Order
				cartRevise.Cart = ShoppingCartProvider.LoadMyCart(); // ดึงข้อมูลสินค้าหลังจากสร้าง Cart จาก Order

				cartRevise.ApproverRemark = cartRevise.Order.ApproverRemark; // remark จาก approver
				cartRevise.OFMRemark = cartRevise.Order.OFMRemark; // remark ถึง officemate
				cartRevise.ReferenceRemark = cartRevise.Order.ReferenceRemark; // remark อื่นๆ

				if (cartRevise.Cart == null)//ถ้าไม่เคยมี Cart มาก่อน ต้องสร้าง Cart
				{
					UpdateItemInCart(cartRevise.Order.ItemOrders.Select(o => o.Product.Id).ToArray(), cartRevise.Order.ItemOrders.Select(o => o.Quantity).ToArray(), cartRevise.Order.OrderID, false);
					return RedirectToAction("ViewReviseOrder", new { id = Id });
				}

				if (!cartRevise.Cart.RefOrderId.Equals(orderId))//ถ้า Cart เดิม เป็น NEW -> RefOrderId จะเป็นช่องว่าง หรือถ้ามี RefOrderId จาก order อื่น ต้องทำเป็น Cart Revise
				{
					CreateCartTypeReviseFormOrder(cartRevise.Order);//นำข้อมูลจาก Order ไปสร้างเป็น Cart
					return RedirectToAction("ViewReviseOrder", new { id = Id });
				}

				cartRevise.CurrentUsedCostCenter = GetMyCostcenterAddress(User.CurrentCompanyId, cartRevise.Order.CostCenter.CostCenterID, User.UserId); //ดึงข้อมูล CostCenter ที่เลือก
				if (!cartRevise.CurrentUsedCostCenter.IsDeliCharge) { cartRevise.Cart.TotalDeliveryCharge = 0; }
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder) //องค์กรที่คุมด้วย budget ต้องดึง budget มาด้วย
				{
					BudgetRepository repo = new BudgetRepository(connection);
					cartRevise.CurrentBudget = repo.GetBudgetByCostcenterID(cartRevise.Order.CostCenter.CostCenterID, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					cartRevise.CurrentBudget.WaitingOrderAmt = repo.GetWaitingOrderOnThisBudget(cartRevise.CurrentBudget.GroupID, User.CurrentCompanyId, User.Company.BudgetLevelType);
					cartRevise.Cart.SendToAdminAppBudg = cartRevise.Cart.GrandTotalAmt > cartRevise.CurrentBudget.RemainBudgetAmt;
				}
				return View(cartRevise);
			}
		}
		public ActionResult ViewReviseOrderByScId()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				return RedirectToAction("ViewReviseOrder", new { Id = orderRePo.GetGuidByOrderId(ShoppingCartProvider.LoadCartType(new Cart()).RefOrderId) });
			}
		}

		public ActionResult HandleActionRevise(ImageInputData Action, CartRevise cartRevise,HttpPostedFileBase file)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				cartRevise.Cart = ShoppingCartProvider.LoadMyCart();//ดึงข้อมูลสินค้าหลังจากสร้าง Cart จาก Order
				string guid = new OrderRepository(connection).GetGuidByOrderId(cartRevise.Cart.RefOrderId);
				switch (Action.Name)
				{
					case "UploadFile":
						AttachFileOrder(file, cartRevise.Cart.ShoppingCartId,guid);
						return RedirectToAction("ViewReviseOrder", new { Id = guid});
					case "DeleteAttachFile":
						DeleteAttachFile(guid);
						return RedirectToAction("ViewReviseOrder", new { Id = guid });
					case "NewOrder":
						ChangeCartReviseToNew(cartRevise.Cart.ShoppingCartId);
						return RedirectToAction("CartDetail", "Cart");
					case "CancelReviseOrder":
						ClearCart(cartRevise.Cart.ShoppingCartId);
						return RedirectToAction("ViewCancelOrderRevise");
					case "SendToApprover":
						OrderRepository orderRePo = new OrderRepository(connection);
						cartRevise.Order = orderRePo.GetMyOrder(cartRevise.Cart.RefOrderId);
						cartRevise.User = User; //Set ค่า User
						if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)
						{
							BudgetRepository repo = new BudgetRepository(connection);
							cartRevise.CurrentBudget = repo.GetBudgetByCostcenterID(cartRevise.Order.CostCenter.CostCenterID, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
							cartRevise.CurrentBudget.WaitingOrderAmt = repo.GetWaitingOrderOnThisBudget(cartRevise.CurrentBudget.GroupID, User.CurrentCompanyId, User.Company.BudgetLevelType);
							cartRevise.Cart.SendToAdminAppBudg = cartRevise.Cart.GrandTotalAmt > cartRevise.CurrentBudget.RemainBudgetAmt;
						}
						if (cartRevise.Order.NextApprover.ParkDay == 0) { cartRevise.Order.NextApprover.ParkDay = User.Company.DefaultParkDay; } //ถ้า ParkDay เป็น 0 ให้ใช้ DefaultParkDay แทน
						if (!new CostCenterRepository(connection).IsCostcenterDeliveryFee(cartRevise.Order.Company.CompanyId, cartRevise.Order.CostCenter.CostCenterID))
						{
							cartRevise.Cart.TotalDeliveryCharge = 0;
						}
						string orderId = orderRePo.UpdateOrderEditByRequester(cartRevise);// อัพเดตสถานะของ Order ใบนี้

						OrderData orderData = new OrderData();
						orderData.User = User;
						orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
						orderData.CurrentOrderActivity = SetActivityForWaitingOrder(orderData.Order);
						orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);//บันทึก Activity ของ Order
						CreateOrderLog(orderData);
						SendMail(new Eprocurement2012.Controllers.Mails.RequesterReviseOrderMail(ControllerContext, orderData), orderId, MailType.RequesterSentReviseOrder, User.UserId);
						return RedirectToAction("ViewReviseOrderSuccess");
					default:
						return View("ViewReviseOrder", cartRevise);
				}
			}
		}
		private void CreateOrderLog(OrderData orderData)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				switch (orderData.Order.Status)
				{
					case Order.OrderStatus.Waiting:
						orderRePo.CreateWatingOrderTransactionLog(orderData);
						break;
					case Order.OrderStatus.WaitingAdmin:
						orderRePo.CreateAllowWattingOrderTransactionLog(orderData);
						break;
					default:
						break;
				}
				orderRePo.CreateOrderDetailTransactionLog(orderData, OrderData.OrderStatusLog.Revise);
			}
		}
		public ActionResult ViewReviseOrderSuccess()
		{
			return View();
		}
		private void CreateCartTypeReviseFormOrder(Order orderData)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				new ShoppingCartRepository(connection).ChangeCartTypeToRevise(ShoppingCartProvider.ShoppingCartId.Value, orderData.OrderID);
				UpdateItemInCart(orderData.ItemOrders.Select(o => o.Product.Id).ToArray(), orderData.ItemOrders.Select(o => o.Quantity).ToArray(), orderData.OrderID, false);
			}
		}
		private OrderActivity SetActivityForWaitingOrder(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ส่งใบสั่งซื้อพิจารณาอนุมัติ";
			orderActivity.ActivityEName = "Wait For Approved";
			orderActivity.Remark = "รอการพิจารณาจากผู้อนุมัติ คุณ " + order.CurrentApprover.Approver.DisplayName;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}
		private void ClearCart(int scid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ShoppingCartRepository shoppingCartrepo = new ShoppingCartRepository(connection);
				shoppingCartrepo.ClearCartRevise(scid);
			}
		}
		private void ChangeCartReviseToNew(int scid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				new ShoppingCartRepository(connection).ChangeCartReviseToNew(scid);
			}
		}
		public ActionResult ViewCancelOrderRevise()
		{
			return View("ReviseOrderCancel");
		}
		//Method ดึงข้อมูล CostCenter
		private CostCenter GetMyCostcenterAddress(string companyId, string costcenterId, string userId)
		{
			CostCenter costcenter = null;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				costcenter = costcenterRepo.GetCostcenterDetail(companyId, costcenterId, userId);

				//เอาข้อมูล Contactor มาแสดงแทนข้อมูล ShipContactor
				costcenter.CostCenterShipping.ShipContactor = costcenter.CostCenterContact.ContactorName;
				costcenter.CostCenterShipping.ShipPhoneNo = costcenter.CostCenterContact.ContactorPhone;
				costcenter.CostCenterShipping.Extension = costcenter.CostCenterContact.ContactorExtension;
				costcenter.CostCenterShipping.ShipMobileNo = costcenter.CostCenterContact.ContactMobileNo;
			}
			using (SqlConnection connectionMaster = new SqlConnection(ConnectionString))
			{
				//ต้องแยก Conection เพราะใน FrameWork ต้องการ Master
				CostCenterRepository costcenterRepo = new CostCenterRepository(connectionMaster);
				costcenter.CostCenterPayment = costcenterRepo.GetMyCostCenterPayment(costcenter.CostCenterCustID);
			}
			return costcenter;
		}
#if !DEBUG
		[RequireHttps]
#endif
		//Action สำหรับแก้ไขสินค้าใน Cart
		public void UpdateItemInCart(string[] productId, int[] quantity, string refOrderId, bool isAddbyProductId)
		{
			IDictionary<string, int> shoppingcart = ShoppingCartProvider.LoadCart();
			bool IsValid = true;

			if (isAddbyProductId) //เช็คว่าใส่รหัสสินค้าและจำนวนเรียบร้อย
			{
				string pID = string.Empty;
				if (quantity[0] <= 0 || string.IsNullOrEmpty(productId[0]))
				{
					TempData["ErrorAddProductByPid"] = new ResourceString("ErrorMessage.AddProductByPid");
				}
				else if (CheckPidForAddCartByPid(productId[0], out pID)) //เช็คว่ารหัสสินค้าถูกต้อง
				{
					if (shoppingcart.ContainsKey(pID))
					{
						shoppingcart[pID] += quantity[0];
					}
					else
					{
						shoppingcart.Add(pID, quantity[0]);
					}
					ShoppingCartProvider.SaveCart(shoppingcart);
				}
				else
				{
					TempData["ErrorAddProductByPid"] = new ResourceString("ErrorMessage.ProductNotFound");
				}
			}
			else
			{
				for (int i = 0; i < productId.Length; i++)
				{
					if (quantity[i] <= 0)
					{
						TempData["QuantityErrorPid" + productId[i]] = new ResourceString("ErrorMessage.Quantity") + quantity[i] + new ResourceString("ErrorMessage.Incorrect");
						IsValid = false;
						continue;
					}
					if (shoppingcart.ContainsKey(productId[i]))
					{
						shoppingcart[productId[i]] = quantity[i];
					}
					else
					{
						shoppingcart.Add(productId[i], quantity[i]);
					}
				}
				if (IsValid) { ShoppingCartProvider.SaveCart(shoppingcart); }
			}
		}

#if !DEBUG
		[RequireHttps]
#endif
		public ActionResult AddToCartByProductId(string[] productID, int[] quantity, string refOrderId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRepo = new OrderRepository(connection);
				string pID = string.Empty;
				if (CheckPidForAddCartByPid(productID[0], out pID)) //เช็คว่ารหัสสินค้าถูกต้อง
				{
					UpdateItemInCart(productID, quantity, refOrderId, true);
				}
				else
				{
					TempData["ErrorAddProductByPid"] = new ResourceString("ErrorMessage.ProductNotFound");
				}
				return RedirectToAction("ViewReviseOrder", new { Id = orderRepo.GetGuidByOrderId(refOrderId) });
			}
		}
#if !DEBUG
				[RequireHttps]
#endif
		//Action สำหรับเลือกทำ ลบ หรือ แก้ไขสินค้าใน Cart
		public ActionResult HandleAction(string[] productID, int[] quantity, string[] DeleteCartItem, string refOrderId, ImageInputData Action)
		{
			switch (Action.Name)
			{
				case "UpdateCart":
					UpdateItemInCart(productID, quantity, refOrderId, false);
					break;
				case "DeleteCart":
					if (DeleteCartItem == null) { TempData["DeleteError"] = new ResourceString("ErrorMessage.DeleteItem"); }
					else if (productID.Count() == DeleteCartItem.Count()) { TempData["DeleteError"] = new ResourceString("ErrorMessage.NotDeleteItem"); }
					else { DeleteItemInCart(DeleteCartItem, refOrderId); }
					break;
				default:
					break;
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRepo = new OrderRepository(connection);
				return RedirectToAction("ViewReviseOrder", new { Id = orderRepo.GetGuidByOrderId(refOrderId) });
			}
		}

		private void DeleteItemInCart(string[] DeleteCartItem, string refOrderId)
		{
			if (DeleteCartItem != null)
			{
				ShoppingCartProvider.RemoveItemsFromCartRevise(DeleteCartItem, refOrderId);
			}
		}
		
		private bool CheckPidForAddCartByPid(string pid, out string productId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				productId = string.Empty;
				return new ProductRepository(connection, User).IsPidInSystem(pid, out productId);
			}
		}


		private void AttachFileOrder(HttpPostedFileBase file, int scid,string guid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Order order = new Order();
				string fileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);
				if (file == null)
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.InvalidFile");
				}
				else if (file.ContentLength > 2097152)//แปลงจาก 2MB เป็น Byte
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.FileSizeMore");
				}
				else if (order.Files.Any(m => m.Name == fileExtension))
				{
					var fileName = Path.GetFileName(file.FileName);
					//var path = Path.Combine(Server.MapPath("~/AttachFile"), fileName);
					//file.SaveAs(path);
					string systemFileName = new ShoppingCartRepository(connection).UpdateAttachFileByRevise(scid, fileName, guid, fileExtension);
					var path = Path.Combine(Server.MapPath("~/AttachFile"), systemFileName);
					file.SaveAs(path);
				}
				else
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.SupportFile");
				}
			}
		}

		private void DeleteAttachFile(string guid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				new ShoppingCartRepository(connection).DeleteAttachFileByRevise(guid);
			}
		}

	}
}
