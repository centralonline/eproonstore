﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;
using Eprocurement2012.Models.Repositories;
using System.IO;


namespace Eprocurement2012.Controllers
{
	public class CartController : AuthenticateRoleController
	{

#if !DEBUG
		[RequireHttps]
#endif
		//Action View CartDetail  ตอนเข้ามาครั้งแรก
		public ActionResult CartDetail()
		{
			if (!User.IsLogin) { return RedirectToAction("LogIn", "Account", new LogIn()); }
			CartData cartData = new CartData();
			cartData.Cart = ShoppingCartProvider.LoadMyCart();//ดึงข้อมูลสินค้าในเลือกซื้อ

			if (cartData.Cart == null)
			{
				cartData.Cart = new Cart();
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costCenterRepo = new CostCenterRepository(connection);
				OrderRepository orderRePo = new OrderRepository(connection);
				cartData.ItemCostcenter = costCenterRepo.GetMyCostCenter(User.UserId, User.CurrentCompanyId, User.UserType.Requester);//ดึงข้อมูล ListCostCenter
				cartData.IsGoodReceivePeriod = orderRePo.GetGoodReceivePeriod(User.Company.GoodReceivePeriod, User.CurrentCompanyId, User.UserId); //ดึงข้อมูล GoodReceivePeriod สำหรับแสดงการแจ้งเตือน

				if (cartData.Cart.Type.Equals(Cart.CartType.Revise) && !string.IsNullOrEmpty(cartData.Cart.RefOrderId) && User.IsRequester) //ถ้า Cart ที่ได้มาเป็น Revise
				{
					Order order = orderRePo.GetMyOrder(cartData.Cart.RefOrderId);
					if (order.Status != Eprocurement2012.Models.Order.OrderStatus.Deleted)
					{
						return RedirectToAction("ViewReviseOrderByScId", "Revise");
					}
					else
					{
						new ShoppingCartRepository(connection).ChangeCartReviseToNew(cartData.Cart.ShoppingCartId);
					}
				}
			}

			if (cartData.ItemCostcenter.Count() == 1)
			{
				cartData = GetApproverAndCostCenterDetail(cartData.ItemCostcenter.FirstOrDefault().CostCenterID, cartData);
			}
			return View("CartDetail", cartData);
		}

#if !DEBUG
		[RequireHttps]
#endif
		/* Action ดึงข้อมูลผู้อนุมัติและCostcenter หลังจาก User เลือก CostCenter และ กดปุ่มแสดงรายละเอียดข้อมูล */
		public ActionResult ShowApproverAndCostCenter(string costcenterId, CartData cartData)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costCenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				//CartData cartData = new CartData();
				cartData.Cart = ShoppingCartProvider.LoadMyCart();//ดึงข้อมูลสินค้าในเลือกซื้อ
				cartData.ItemCostcenter = costCenterRepo.GetMyCostCenter(User.UserId, User.CurrentCompanyId, User.UserType.Requester);//ดึงข้อมูล ListCostCenter
				if (string.IsNullOrEmpty(costcenterId))
				{
					TempData["ErrorCostCenter"] = new ResourceString("CartDetail.SelectedCostcenter");
					return View("CartDetail", cartData);
				}
				cartData = GetApproverAndCostCenterDetail(costcenterId, cartData);
				return View("CartDetail", cartData);
			}
		}

		public ActionResult HandleActionCart(CartData cartData, HttpPostedFileBase file, ImageInputData Action)
		{
			if (Action.Name == "CheckLogIn")
			{
				return ConfirmOrderDetail(cartData);
			}
			if (Action.Name == "AttachFile")
			{
				AttachFileOrder(file, cartData.Cart.ShoppingCartId);
			}
			if (Action.Name == "DeleteAttachFile")
			{
				DeleteAttachFile(cartData.Cart.ShoppingCartId);
			}
			return ShowApproverAndCostCenter(cartData.costcenterId, cartData);
		}

		private CartData GetApproverAndCostCenterDetail(string costcenterId, CartData cartData)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)
				{
					BudgetRepository repo = new BudgetRepository(connection);
					cartData.CurrentBudget = repo.GetBudgetByCostcenterID(costcenterId, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					cartData.CurrentBudget.WaitingOrderAmt = repo.GetWaitingOrderOnThisBudget(cartData.CurrentBudget.GroupID, User.CurrentCompanyId, User.Company.BudgetLevelType);
					cartData.IsNotSetBudget = cartData.CurrentBudget.OriginalAmt <= 0;
					cartData.Cart.SendToAdminAppBudg = cartData.Cart.GrandTotalAmt > cartData.CurrentBudget.RemainBudgetAmt;
				}
				cartData.ListUserApprover = GetMyApprover(costcenterId); //ดึงข้อมูล Approver ของ CostCenter ที่เลือก
				cartData.CurrentUsedCostCenter = GetMyCostcenterAddress(User.CurrentCompanyId, costcenterId, User.UserId); //ดึงข้อมูล CostCenter ที่เลือก
				if (!cartData.CurrentUsedCostCenter.IsDeliCharge) { cartData.Cart.TotalDeliveryCharge = 0; }
				if (cartData.Cart.GrandTotalAmt > cartData.ListUserApprover.OrderByDescending(c => c.Level).FirstOrDefault().ApproveCreditLimit) { cartData.Cart.IsOverCreditlimit = true; }
			}
			return cartData;

		}

		//Method ดึงข้อมูล Approver
		private IEnumerable<UserApprover> GetMyApprover(string costcenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				return userRepo.GetListUserApprover(User.CurrentCompanyId, costcenterId, User.UserId);
			}
		}

		//Method ดึงข้อมูล CostCenter
		private CostCenter GetMyCostcenterAddress(string companyId, string costcenterId, string userId)
		{
			CostCenter costcenter = null;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				costcenter = costcenterRepo.GetCostcenterDetail(companyId, costcenterId, userId);

				//เอาข้อมูล Contactor มาแสดงแทนข้อมูล ShipContactor
				costcenter.CostCenterShipping.ShipContactor = costcenter.CostCenterContact.ContactorName;
				costcenter.CostCenterShipping.ShipPhoneNo = costcenter.CostCenterContact.ContactorPhone;
				costcenter.CostCenterShipping.Extension = costcenter.CostCenterContact.ContactorExtension;
				costcenter.CostCenterShipping.ShipMobileNo = costcenter.CostCenterContact.ContactMobileNo;
			}
			using (SqlConnection connectionMaster = new SqlConnection(ConnectionString))
			{
				//ต้องแยก Conection เพราะใน FrameWork ต้องการ Master
				CostCenterRepository costcenterRepo = new CostCenterRepository(connectionMaster);
				costcenter.CostCenterPayment = costcenterRepo.GetMyCostCenterPayment(costcenter.CostCenterCustID);
			}
			return costcenter;
		}

#if !DEBUG
		[RequireHttps]
#endif
		public ActionResult AddToCartByProductId(string[] productID, int[] quantity)
		{
			return UpdateItemInCart(productID, quantity, true);
		}

#if !DEBUG
		[RequireHttps]
#endif
		//Action สำหรับเลือกทำ ลบ หรือ แก้ไขสินค้าใน Cart
		public ActionResult HandleAction(string[] productID, int[] quantity, string[] DeleteCartItem, ImageInputData Action)
		{
			switch (Action.Name)
			{
				case "AddToCartByPId":
					return UpdateItemInCart(productID, quantity, true);
				case "UpdateCart":
					return UpdateItemInCart(productID, quantity, false);
				case "DeleteCart":
					if (DeleteCartItem != null)
					{
						return DeleteItemInCart(DeleteCartItem);
					}
					goto default;
				default:
					return UpdateItemInCart(productID, quantity, false);
			}
		}

#if !DEBUG
		[RequireHttps]
#endif
		//Action สำหรับแก้ไขสินค้าใน Cart
		public ActionResult UpdateItemInCart(string[] productId, int[] quantity, bool isAddbyProductId)
		{
			IDictionary<string, int> shoppingcart = ShoppingCartProvider.LoadCart();
			bool IsValid = true;


			if (isAddbyProductId) //เช็คว่าใส่รหัสสินค้าและจำนวนเรียบร้อย
			{
				string pID = string.Empty;
				if (quantity[0] <= 0 || string.IsNullOrEmpty(productId[0]))
				{
					TempData["ErrorAddProductByPid"] = new ResourceString("ErrorMessage.AddProductByPid");
				}
				else if (CheckPidForAddCartByPid(productId[0], out pID)) //เช็คว่ารหัสสินค้าถูกต้อง
				{
					if (shoppingcart.ContainsKey(pID))
					{
						shoppingcart[pID] += quantity[0];
					}
					else
					{
						shoppingcart.Add(pID, quantity[0]);
					}
					ShoppingCartProvider.SaveCart(shoppingcart);
				}
				else
				{
					TempData["ErrorAddProductByPid"] = new ResourceString("ErrorMessage.ProductNotFound");
				}
			}
			else
			{
				for (int i = 0; i < productId.Length; i++)
				{
					if (quantity[i] <= 0)
					{
						TempData["QuantityErrorPid" + productId[i]] = new ResourceString("ErrorMessage.Quantity") + quantity[i] + new ResourceString("ErrorMessage.Incorrect");
						IsValid = false;
						continue;
					}
					if (shoppingcart.ContainsKey(productId[i]))
					{
						shoppingcart[productId[i]] = quantity[i];
					}
					else
					{
						shoppingcart.Add(productId[i], quantity[i]);
					}
				}
				if (IsValid) { ShoppingCartProvider.SaveCart(shoppingcart); }
			}
			return RedirectToAction("CartDetail");
		}

#if !DEBUG
		[RequireHttps]
#endif
		//Action สำหรับลบสินค้าใน Cart
		public ActionResult DeleteItemInCart(string[] DeleteCartItem)
		{
			if (DeleteCartItem != null)
			{
				ShoppingCartProvider.RemoveItemsFromCart(DeleteCartItem);
			}
			else
			{
				TempData["DeleteError"] = new ResourceString("ErrorMessage.SelectItem");
			}
			return RedirectToAction("CartDetail");
		}

		//เช็คว่ารหัสนี้มีอยู่ในระบบและมีสิทธิสั่งซื้อหรือไม่
		private bool CheckPidForAddCartByPid(string pid, out string productId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				productId = string.Empty;
				return new ProductRepository(connection, User).IsPidInSystem(pid, out productId);
			}
		}

		private void AttachFileOrder(HttpPostedFileBase file, int scid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Order order = new Order();
				string fileExtension = System.IO.Path.GetExtension(Request.Files["file"].FileName);
				if (file == null)
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.InvalidFile");
				}
				else if (file.ContentLength > 2097152)//แปลงจาก 2MB เป็น Byte
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.FileSizeMore");
				}
				else if (order.Files.Any(m => m.Name == fileExtension))
				{
					var fileName = Path.GetFileName(file.FileName);
					string systemFileName = new ShoppingCartRepository(connection).UpdateAttachFile(scid, User.UserId, User.CurrentCompanyId, fileName, fileExtension);
					var path = Path.Combine(Server.MapPath("~/AttachFile"), systemFileName);
					file.SaveAs(path);
				}
				else
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.SupportFile");
				}
			}
		}

		private void DeleteAttachFile(int scid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				new ShoppingCartRepository(connection).DeleteAttachFile(scid);
			}
		}

		private ActionResult ConfirmOrderDetail(CartData cartData)
		{
			GetCartDataForCreate(cartData); //ดึงขอมูลใน cart
			if (cartData.AutoApprove == "Error")
			{
				TempData["Error"] = new ResourceString("CartDetail.SelectedRedio");
				return CartDetail();
				//return RedirectToAction("CartDetail", "Cart");
			}
			if (!cartData.Cart.ItemCart.Any()) { return CartDetail(); }
			return View("ConfirmOrder", cartData);
		}

		private CartData GetCartDataForCreate(CartData cartData)
		{
			cartData.Cart = ShoppingCartProvider.LoadMyCart();
			cartData.Cart.ShoppingCartId = ShoppingCartProvider.ShoppingCartId.Value;
			cartData.User = User;
			if (string.IsNullOrEmpty(cartData.CallBackRequestStatus)) { cartData.CallBackRequestStatus = "No"; }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				cartData.ItemCostcenter = costcenterRepo.GetMyCostCenter(User.UserId, User.CurrentCompanyId, User.UserType.Requester);//ดึงข้อมูล ListCostCenter
				cartData.ListUserApprover = userRepo.GetListUserApprover(User.CurrentCompanyId, cartData.costcenterId, User.UserId);//ดึงข้อมูล ListApprover ของ CostCenter ที่เลือก
				string approverId = userRepo.GetUserIdFromUserGuid(cartData.approverId);//ดึง userid จาก guid ที่รับมา
				cartData.CurrentSelectApprover = cartData.ListUserApprover.Where(m => m.Approver.UserId == approverId).Single();//เลือกข้อมูล Approver  ใน List ออกมา เพียงตัวเดียวตามที่ User เลือกมา
				cartData.CurrentUsedCostCenter = costcenterRepo.GetCostcenterDetail(User.CurrentCompanyId, cartData.costcenterId, User.UserId); //ดึงข้อมูล CostCenter  ใน List ออกมา เพียงตัวเดียวตามที่ User เลือกมา

				//เอาข้อมูล Contactor มาแสดงแทนข้อมูล ShipContactor
				cartData.CurrentUsedCostCenter.CostCenterShipping.ShipContactor = cartData.CurrentUsedCostCenter.CostCenterContact.ContactorName;
				cartData.CurrentUsedCostCenter.CostCenterShipping.ShipPhoneNo = cartData.CurrentUsedCostCenter.CostCenterContact.ContactorPhone;
				cartData.CurrentUsedCostCenter.CostCenterShipping.Extension = cartData.CurrentUsedCostCenter.CostCenterContact.ContactorExtension;
				cartData.CurrentUsedCostCenter.CostCenterShipping.ShipMobileNo = cartData.CurrentUsedCostCenter.CostCenterContact.ContactMobileNo;

				if (!cartData.CurrentUsedCostCenter.IsDeliCharge) { cartData.Cart.TotalDeliveryCharge = 0; }
				if (cartData.Cart.GrandTotalAmt > cartData.ListUserApprover.OrderByDescending(c => c.Level).FirstOrDefault().ApproveCreditLimit) { cartData.Cart.IsOverCreditlimit = true; } //กรณีมียอดสั่งซื้อเกินวงเงินของผู้อนุมัติท่านสุดท้าย ไม่ให้สั่ง
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)
				{
					BudgetRepository repo = new BudgetRepository(connection);
					cartData.CurrentBudget = repo.GetBudgetByCostcenterID(cartData.costcenterId, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					cartData.CurrentBudget.WaitingOrderAmt = repo.GetWaitingOrderOnThisBudget(cartData.CurrentBudget.GroupID, User.CurrentCompanyId, User.Company.BudgetLevelType);
					cartData.Cart.SendToAdminAppBudg = cartData.Cart.GrandTotalAmt > cartData.CurrentBudget.RemainBudgetAmt;
				}

				if (User.UserId != approverId || !cartData.CurrentUsedCostCenter.UseAutoApprove) { cartData.AutoApprove = "No"; }//กรณีชื่อผู้สั่งซื้อกับผู้อนุมัติเป็นคนละคนกัน หรือ costcenter นี้ไม่เปิดใช้ auto Approve จะเซ็ท AutoApprove เป็น No
				if (User.UserId == approverId && cartData.CurrentUsedCostCenter.UseAutoApprove && string.IsNullOrEmpty(cartData.AutoApprove)) { cartData.AutoApprove = "Error"; }//กรณีที่เปิดใช้ auto approve และ ผู้สั่งซื้อกับผู้อนุมัติเป็นคนเดียวกันต้องเลือก Yes/No มา ไม่เลือกไม่ให้ผ่าน
				if (User.UserId == approverId && cartData.CurrentUsedCostCenter.UseAutoApprove && cartData.ListUserApprover.Count() == 1 && cartData.Cart.GrandTotalAmt > cartData.ListUserApprover.Single().ApproveCreditLimit) { cartData.Cart.IsOverCreditlimit = true; }//กรณีที่เปิดใช้ auto approve และ ผู้สั่งซื้อกับผู้อนุมัติเป็นคนเดียวกันแต่วงเงินอนุมัติไม่พอ ไม่ให้สั่ง

			}
			using (SqlConnection connectionMaster = new SqlConnection(ConnectionString))
			{
				//ต้องแยก Conection เพราะใน FrameWork ต้องการ Master
				CostCenterRepository costcenterRepo = new CostCenterRepository(connectionMaster);
				cartData.CurrentUsedCostCenter.CostCenterPayment = costcenterRepo.GetMyCostCenterPayment(cartData.CurrentUsedCostCenter.CostCenterCustID);
			}
			return cartData;
		}

	}

}