﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;
namespace Eprocurement2012.Controllers
{
	public class SiteController : AuthenticateRoleController
	{

		[HttpPost]
		public ActionResult OpenSite(ImageInputData Action)
		{
			if (User.UserId == null) { throw new ArgumentNullException("userId"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				SiteRepository siteRepo = new SiteRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				Site Site = new Site();
				Site.AdminSiteUserId = User.UserId;
				Site.CompanyID = User.CurrentCompanyId;
				Site.AdminRemark = "";
				Site.AdminRemarkMail = "";
				siteRepo.SetSite(User.CurrentCompanyId, Site.OpenSite.Yes); //เปิดใช้งาน Site
				siteRepo.InsertLogOpenSite(Site);//บันทึก Log

				if (Action.Name == "openSiteMail")
				{
					List<string> AllUser = userRepo.GetAllUserReqOrApp(User.Company.CompanyId).Select(u => u.UserId).Distinct().ToList();
					SendMail(new Eprocurement2012.Controllers.Mails.ActivateSiteMail(ControllerContext, AllUser));
				}
				if (Action.Name == "changeSite")//ปุ่มเปลี่ยนไซต์ สำหรับคนที่ดูแลหลายไซต์และเป็น Admin ระบบจะเปิดไซต์ Auto
				{
					return RedirectToAction("LogIn", "Account");
				}
				if (userRepo.IsOnlyAdmin(User.UserId, User.CurrentCompanyId)) // ตรวจสอบว่า เป็น Only Admin (เกิด 1 Row และ เป็น RootAdmin เท่านั้น)
				{
					return RedirectToAction("Logout", "Account");
				}
				return RedirectToAction("Index", "Home");
			}
		}

		[HttpPost]
		public ActionResult CloseSite(string adminRemark, string adminRemarkMail , ImageInputData Action)
		{
			if (User.UserId == null) { throw new ArgumentNullException("userId"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				SiteRepository siteRepo = new SiteRepository(connection);
				Site Site = new Site();
				Site.AdminSiteUserId = User.UserId;
				Site.AdminRemark = string.IsNullOrEmpty(adminRemark) ? "" : adminRemark;
				Site.AdminRemarkMail = string.IsNullOrEmpty(adminRemarkMail) ? "" : adminRemarkMail;
				Site.CompanyID = User.CurrentCompanyId;
				siteRepo.Closesite(User.CurrentCompanyId); //ปิด Site
				siteRepo.InsertLogCloseSite(Site);//บันทึก Log CloseSite
				if (Action.Name == "closeSiteMail")
				{
					List<string> AllUser = new UserRepository(connection).GetAllUserReqOrApp(User.Company.CompanyId).Select(u => u.UserId).Distinct().ToList();
					SendMail(new Eprocurement2012.Controllers.Mails.CloseSiteMail(ControllerContext, AllUser, Site));
				}
				return RedirectToAction("Index", "Admin");
			}
		}

	}
}
