﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Models;
using System.IO;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Controllers
{

	[HandleError]
	public class UserProfileController : AuthenticateRoleController
	{

		// เปลี่ยนภาษที่ใช้งาน
		public ActionResult ChangeTypeLanguage(string language, string redirectUrl)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				User.Language lang = (User.Language)Enum.Parse(typeof(User.Language), language, true);
				userRepo.UpdateUserLanguage(User.UserId, lang);
				return Redirect(redirectUrl);
			}
		}

		// เปลี่ยนภาษาที่ใช้งาน ในหน้า EditProfile
		public ActionResult ChangeLanguage(string userLanguage)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				User.Language lang = (User.Language)Enum.Parse(typeof(User.Language), userLanguage, true);
				userRepo.UpdateUserLanguage(User.UserId, lang);
				return RedirectToAction("GoToEditProfileUser");
			}
		}

		// ไปสู่หน้า แก้ไขข้อมูลส่วนตัวผู้ใช้งาน
		public ActionResult GoToEditProfileUser()
		{
			return View("SetProfileUser", SetInformationUserProfile());
		}

		// บันทึกข้อมูลส่วนตัวพนักงงานที่แก้ไข
		[HttpPost]
		public ActionResult EditMyProfile(EditProfileUser editUser)
		{
			editUser.CustId = User.CustId;
			if (ModelState.IsValid)
			{
				Contact contact = new UserRepository(new SqlConnection(ConnectionString)).GetUserDataContact(User.UserId, User.CurrentCompanyId);
				editUser.PhoneId = contact.Phone != null ? contact.Phone.Id : 0;
				editUser.MobileId = contact.Mobile != null ? contact.Mobile.Id : 0;
				editUser.FaxId = contact.Fax != null ? contact.Fax.Id : 0;
				if (!String.IsNullOrEmpty(editUser.ThaiName) && !String.IsNullOrEmpty(editUser.EngName)) { UpdateUserName(editUser); } //อัพเดตชิ่อไทยและอังกฤษ
				if (!String.IsNullOrEmpty(editUser.Phone)) { UpdatePhoneUser(editUser); }//อัพเดตข้อมูลเบอร์โทรศัพท์
				if (!String.IsNullOrEmpty(editUser.Mobile)) { UpdateMobileUser(editUser); }//อัพเดตข้อมูลมือถือ
				if (!String.IsNullOrEmpty(editUser.Fax)) { UpdateFaxUser(editUser); }//อัพเดตข้อมูลแฟกซ์
				TempData["Notify"] = "<span class='uiIcon'><span class='iconCorrectSmall'></span> บันทึกการเปลี่ยนแปลงของคุณแล้ว</span>";
				return RedirectToAction("GoToEditProfileUser");
			}
			return View("SetProfileUser", editUser);
		}

		private void UpdateUserName(EditProfileUser editUser)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				userRepo.UpdateUserName(editUser, User.UserId);
			}
		}

		private void UpdatePhoneUser(EditProfileUser editUser)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repo = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(connection);
				OfficeMate.Framework.CustomerData.PhoneNumber phone;

				phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
				if (editUser.PhoneId == 0) //new phone number
				{
					phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
					phone.CustomerId = editUser.CustId;
					phone.SequenceId = editUser.SequenceId;
					phone.PhoneNo = editUser.Phone;
					phone.Extension = string.IsNullOrEmpty(editUser.PhoneExt) ? "" : editUser.PhoneExt;
					repo.InsertPhoneNumber(phone, "EproV5");
					//repo.setDefaultPhoneNumber(phone, "EproV5");
				}
				else //edit phone number
				{
					phone = repo.SelectPhone(editUser.PhoneId);
					phone.PhoneNo = editUser.Phone;
					phone.Extension = string.IsNullOrEmpty(editUser.PhoneExt) ? "" : editUser.PhoneExt;
					repo.UpdatePhoneNumber(phone, "EproV5");
				}
			}
		}

		private void UpdateMobileUser(EditProfileUser editUser)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repo = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(connection);
				OfficeMate.Framework.CustomerData.PhoneNumber phone;

				phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
				if (editUser.MobileId == 0) //new phone number
				{
					phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.Mobile;
					phone.CustomerId = editUser.CustId;
					phone.SequenceId = editUser.SequenceId;
					phone.PhoneNo = editUser.Mobile;
					phone.Extension = "";
					repo.InsertPhoneNumber(phone, "EproV5");
					//repo.setDefaultPhoneNumber(phone, "EproV5");
				}
				else //edit phone number
				{
					phone = repo.SelectPhone(editUser.MobileId);
					phone.PhoneNo = editUser.Mobile;
					phone.Extension = "";
					repo.UpdatePhoneNumber(phone, "EproV5");
				}
			}
		}

		private void UpdateFaxUser(EditProfileUser editUser)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repo = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(connection);
				OfficeMate.Framework.CustomerData.PhoneNumber phone;

				phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
				if (editUser.FaxId == 0) //new phone number
				{
					phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.FaxOut;
					phone.CustomerId = editUser.CustId;
					phone.SequenceId = editUser.SequenceId;
					phone.PhoneNo = editUser.Fax;
					phone.Extension = "";
					repo.InsertPhoneNumber(phone, "EproV5");
					//repo.setDefaultPhoneNumber(phone, "EproV5");
				}
				else //edit phone number
				{
					phone = repo.SelectPhone(editUser.FaxId);
					phone.PhoneNo = editUser.Fax;
					phone.Extension = "";
					repo.UpdatePhoneNumber(phone, "EproV5");
				}
			}
		}

		// เปลี่ยน pasword จากหน้า edit profile
		public ActionResult ForceChangePasswordInProfile(Password password)
		{
			EditProfileUser editprofile = SetInformationUserProfile();
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					UserRepository userRepo = new UserRepository(connection);
					if (userRepo.IsPasswordCorrect(User.UserId, password.OldPassword))
					{
						userRepo.UpdateChangPassword(User.UserId, password.NewPassword);
						TempData["Notify"] = "<span class='uiIcon'><span class='iconCorrectSmall'></span> บันทึกการเปลี่ยนแปลงของคุณแล้ว</span>";
					}
					else
					{
						ModelState.AddModelError("OldPassword", "Shared.LogInCenter.ErrorOldPasswordNotCorrect");
					}
				}
			}
			return View("SetProfileUser", editprofile);
		}

		//ดึงข้อมูลผู้ใช้งาน
		private EditProfileUser SetInformationUserProfile()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				EditProfileUser user = new EditProfileUser();
				user = userRepo.GetUserDataForEdit(User.UserGuid, User.CurrentCompanyId);
				return user;
			}
		}

		// แสดงรูป profile ของผู้ใช้งาน
		[HttpGet]
#if !DEBUG
				[AcceptBothHttpHttps]
#endif
		public ActionResult ProfileImage(string userGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ImageData profileImage = new UserRepository(connection).GetProfilePicture(userGuid);
				if (string.IsNullOrEmpty(userGuid) || profileImage.Bytes == null || profileImage.Bytes.Length.Equals(4) || profileImage.Bytes.Length.Equals(0))
				{
					return new FilePathResult("/images/icon-profile.gif", "image/gif");
				}
				else
				{
					return new FileContentResult(profileImage.Bytes, "image/png");
				}
			}
		}

		// upload รูป profile ของผู้ใช้งาน
		[HttpPost]
		public ActionResult UploadImage(HttpPostedFileBase file)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (file == null)
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.Image");
				}
				else if (!file.ContentType.StartsWith("image"))
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.ImageFile");
				}
				else if (file.ContentLength > (512 * 1024))
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.ImageLarger");
				}
				else
				{
					UserRepository userRepo = new UserRepository(connection);
					System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);
					userRepo.UpdateUserImage(ConvertImageToByteArray(image, System.Drawing.Imaging.ImageFormat.Jpeg), User.UserId);
				}
				return RedirectToAction("GoToEditProfileUser");
			}
		}

		// เปลี่ยนไฟล์รูปเป็น ByteArray  เพื่อเก็บลง Database
		private byte[] ConvertImageToByteArray(System.Drawing.Image imageToConvert, System.Drawing.Imaging.ImageFormat formatOfImage)
		{
			byte[] Ret;
			try
			{
				using (MemoryStream ms = new MemoryStream())
				{
					imageToConvert.Save(ms, formatOfImage);
					Ret = ms.ToArray();
				}
			}
			catch (Exception) { throw; }
			return Ret;
		}

		// ลบรูป profile ของผู้ใช้งาน
		[HttpPost]
		public ActionResult RemoveProfileImage()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				userRepo.RemoveUserImage(User.UserId);
			}
			return RedirectToAction("GoToEditProfileUser");
		}

		// ดึงข้อมูลผู้ดูแลระบบ
		[HttpPost]
		public ActionResult GetAdminUser()
		{
			HomePageData hp = new HomePageData();
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				hp.UserAdmin = userRepo.GetAllUserAdmin(User.CurrentCompanyId);
				hp.RootAdmin = hp.UserAdmin.Where(a => a.UserRoleName == Models.User.UserRole.RootAdmin).Single();
				for (int i = 0; i < hp.UserAdmin.ToList().Count(); i++)
				{
					hp.UserAdmin.ToList()[i].UserContact = userRepo.GetUserDataContact(hp.UserAdmin.ToList()[i].UserId, hp.UserAdmin.ToList()[i].CurrentCompanyId); //นำรายชื่อ Admin มาหาข้อมูลการติดต่อของตัวเอง
				}
				NewsRepository newsRepo = new NewsRepository(connection);
				IEnumerable<string> ProductHistoryList = ProductHistoryProvider.GetExceptCurrent(null);
				hp.CompanyNews = newsRepo.GetAllNewsCompany(User.CurrentCompanyId).Take(2);
				hp.OFMNews = newsRepo.GetAllOFMNews().Take(2);
				hp.SiteDirectory = new DepartmentRepository(connection, User).GetChildrenAndGrandChildrenDepartments(0);
				return View("AdminUser", hp);
			}
		}

		//  ไปสู่หน้า Profile ผู้ใช้งาน
		public ActionResult GoToMyProfile()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserProfile userProfile = new UserProfile();
				UserRepository userRepo = new UserRepository(connection);
				userProfile.ItemUserPermissions = userRepo.GetUserReqOrAppPermission(User.UserId, User.CurrentCompanyId); // ดึงข้อมูล Role สิทธิของ User
				User.UserContact = userRepo.GetUserDataContact(User.UserId, User.CurrentCompanyId); //ดึงข้อมูลการติดต่อสำหรับตัวเอง
				return View("MyProfile", userProfile);
			}
		}

		//  ไปสู่หน้า Profile ผู้ใช้งานฝั่ง Admin
		public ActionResult GoToMyProfileAdmin()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserProfile userProfile = new UserProfile();
				UserRepository userRepo = new UserRepository(connection);
				userProfile.ItemUserPermissions = userRepo.GetUserReqOrAppPermission(User.UserId, User.CurrentCompanyId); // ดึงข้อมูล Role สิทธิของ User
				User.UserContact = userRepo.GetUserDataContact(User.UserId, User.CurrentCompanyId); //ดึงข้อมูลการติดต่อสำหรับตัวเอง
				return View("MyProfileAdmin", userProfile);
			}
		}

		// ไปสู่หน้า แก้ไขข้อมูลส่วนตัวผู้ใช้งาน ฝั่ง Admin
		public ActionResult GoToEditProfileAdmin()
		{
			return View("SetProfileAdmin", SetInformationUserProfile());
		}

		// เปลี่ยน pasword จากหน้า edit profile ฝั่ง Admin
		public ActionResult ForceChangePasswordInProfileAdmin(Password password)
		{
			EditProfileUser editprofile = SetInformationUserProfile();
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					UserRepository userRepo = new UserRepository(connection);
					if (userRepo.IsPasswordCorrect(User.UserId, password.OldPassword))
					{
						userRepo.UpdateChangPassword(User.UserId, password.NewPassword);
						TempData["Notify"] = "<span class='uiIcon'><span class='iconCorrectSmall'></span> บันทึกการเปลี่ยนแปลงของคุณแล้ว</span>";
					}
					else
					{
						ModelState.AddModelError("OldPassword", "Shared.LogInCenter.ErrorOldPasswordNotCorrect");
					}
				}
			}
			return View("SetProfileAdmin", editprofile);
		}

		// เปลี่ยนภาษาที่ใช้งาน ในหน้า EditProfile ฝั่ง Admin
		public ActionResult ChangeLanguageAdmin(string userLanguage)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				User.Language lang = (User.Language)Enum.Parse(typeof(User.Language), userLanguage, true);
				userRepo.UpdateUserLanguage(User.UserId, lang);
				return RedirectToAction("GoToEditProfileAdmin");
			}
		}

		// ลบรูป profile ของผู้ใช้งาน ฝั่ง Admin
		[HttpPost]
		public ActionResult RemoveProfileImageAdmin()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				userRepo.RemoveUserImage(User.UserId);
			}
			return RedirectToAction("GoToEditProfileAdmin");
		}

		// upload รูป profile ของผู้ใช้งาน ฝั่ง Admin
		[HttpPost]
		public ActionResult UploadImageAdmin(HttpPostedFileBase file)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (file == null)
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.Image");
				}
				else if (!file.ContentType.StartsWith("image"))
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.ImageFile");
				}
				else if (file.ContentLength > (512 * 1024))
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.ImageLarger");
				}
				else
				{
					UserRepository userRepo = new UserRepository(connection);
					System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);
					userRepo.UpdateUserImage(ConvertImageToByteArray(image, System.Drawing.Imaging.ImageFormat.Jpeg), User.UserId);
				}
				return RedirectToAction("GoToEditProfileAdmin");
			}
		}

		// บันทึกข้อมูลส่วนตัวพนักงงานที่แก้ไข
		[HttpPost]
		public ActionResult EditMyProfileAdmin(EditProfileUser editUser)
		{
			editUser.CustId = User.CustId;
			if (ModelState.IsValid)
			{
				Contact contact = new UserRepository(new SqlConnection(ConnectionString)).GetUserDataContact(User.UserId, User.CurrentCompanyId);
				editUser.PhoneId = contact.Phone != null ? contact.Phone.Id : 0;
				editUser.MobileId = contact.Mobile != null ? contact.Mobile.Id : 0;
				editUser.FaxId = contact.Fax != null ? contact.Fax.Id : 0;
				if (!String.IsNullOrEmpty(editUser.ThaiName) && !String.IsNullOrEmpty(editUser.EngName)) { UpdateUserName(editUser); } //อัพเดตชิ่อไทยและอังกฤษ
				if (!String.IsNullOrEmpty(editUser.Phone)) { UpdatePhoneUser(editUser); }//อัพเดตข้อมูลเบอร์โทรศัพท์
				if (!String.IsNullOrEmpty(editUser.Mobile)) { UpdateMobileUser(editUser); }//อัพเดตข้อมูลมือถือ
				if (!String.IsNullOrEmpty(editUser.Fax)) { UpdateFaxUser(editUser); }//อัพเดตข้อมูลแฟกซ์
				TempData["Notify"] = "<span class='uiIcon'><span class='iconCorrectSmall'></span> บันทึกการเปลี่ยนแปลงของคุณแล้ว</span>";
				return RedirectToAction("GoToEditProfileAdmin");
			}
			return View("SetProfileAdmin", editUser);
		}

	}
}
