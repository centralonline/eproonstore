﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models.Repositories;
using System.Data.SqlClient;
using Eprocurement2012.Models;
using System.IO;
using Eprocurement2012.Controllers.Helpers;
using System.Text.RegularExpressions;
using System.Globalization;

namespace Eprocurement2012.Controllers
{
	public class AdminController : AuthenticateRoleController
	{

		public ActionResult Index(bool? thisMonth)
		{
			if (!User.IsObserve && User.Company.IsSiteActive) { return RedirectToAction("GoToCloseSitePage", "Account"); }
			AdminPageData admin = new AdminPageData();
			bool valMonth = thisMonth.HasValue ? thisMonth.Value : true;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				admin.SearchOrder = new SearchOrder();
				NewsRepository newsRepo = new NewsRepository(connection);
				admin.CompanyNews = newsRepo.GetAllNewsCompany(User.CurrentCompanyId).Where(o => o.NewsStatus == "Active").Take(2);
				foreach (var item in admin.CompanyNews)
				{
					item.NewsDetail = Regex.Replace(item.NewsDetail, @"<.*?>", String.Empty);
				}

				admin.OFMNews = newsRepo.GetAllOFMNews().Where(o => o.NewsStatus == "Active").Take(2);
				foreach (var item in admin.OFMNews)
				{
					item.NewsDetail = Regex.Replace(item.NewsDetail, @"<.*?>", String.Empty);
				}

				admin.IsNoLogo = new CompanyRepository(connection).GetCompanyLogo(User.CurrentCompanyId).Bytes.All(b => b == 0);
				UserRepository repo = new UserRepository(connection);
				admin.CostNotReqLine = new CostCenterRepository(connection).GetCountCostcenterWithoutRequesterLine(User.CurrentCompanyId);
				admin.UserNotVerify = repo.GetAllUser(User.CurrentCompanyId, true).Select(u => new { key = u.UserId, status = u.IsVerified }).Distinct().Count(u => !u.status) + repo.GetAllUser(User.CurrentCompanyId, false).Select(u => new { key = u.UserId, status = u.IsVerified }).Distinct().Count(u => !u.status);
				admin.ItemOrderProcess = CountOrderProcess(valMonth);
			}
			return View(admin);
		}
		private CountOrderProcess CountOrderProcess(bool thisMonth)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository repoOrder = new OrderRepository(connection);
				CountOrderProcess countOrderProcess = new CountOrderProcess();
				countOrderProcess.CountOrderAllowAdmin = repoOrder.GetCountOrderWaitingAdminAllow(User.UserId, User.CurrentCompanyId);
				countOrderProcess.CountStatusWaiting = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.Waiting);
				countOrderProcess.CountStatusWaitingAdmin = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.WaitingAdmin);
				countOrderProcess.CountStatusRevise = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.Revise);
				countOrderProcess.CountStatusApproved = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.Approved);
				countOrderProcess.CountStatusShipped = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.Shipped);
				countOrderProcess.CountStatusCompleted = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.Completed);
				countOrderProcess.CountStatusExpired = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.Expired);
				countOrderProcess.CountStatusDeleted = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.Deleted);
				countOrderProcess.CountStatusPartial = repoOrder.GetCountAllOrderByStatus(User.CurrentCompanyId, thisMonth, Order.OrderStatus.Partial);
				countOrderProcess.thisMonth = thisMonth;
				return countOrderProcess;
			}
		}
		public ActionResult SearchResult(string keyWord, string selectedSearch, string userId, string redirectUrl)
		{
			if (string.IsNullOrEmpty(keyWord))
			{
				TempData["Error"] = String.Format(new ResourceString("SearchDataOrder.ErrorEmptry"));
				return Redirect(redirectUrl);
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRepo = new OrderRepository(connection);
				SearchOrder order = new SearchOrder();
				order.Orders = orderRepo.GetSearchMyOrder(keyWord, selectedSearch, null, User.CurrentCompanyId);
				return View("ViewSearchOrder", order);
			}
		}

		[HttpGet]
		public ActionResult ViewOrderDetail(string Id)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				UserRepository userRepo = new UserRepository(connection);

				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(Id);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
				orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
				orderData.UserApprover = userRepo.GetListUserApprover(orderData.Order.Company.CompanyId, orderData.Order.CostCenter.CostCenterID, User.UserId);//ดึงข้อมูลผู้อนุมัติ ของ Costcenter นี้
				orderData.Order.User = User;//ใส่ User เพื่อให้ Partial  ไว้ใช้ตรวจสอบเปิดปิดปุ่ม
				return View("ViewOrderDetail", orderData);
			}
		}

		public ActionResult CompanyHome()
		{
			if (User.Company.IsSiteActive) { return RedirectToAction("GoToCloseSitePage", "Account"); }
			return View();
		}
		public ActionResult CompanyGuide()
		{
			return View();
		}
		[HttpGet]
		public ActionResult CompanyInformation()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Company company = new CompanyRepository(connection).GetCompany(User.CurrentCompanyId);
				return View("CompanyInformation", company);
			}
		}
		[HttpPost]
		public ActionResult CompanyInformation(Company company)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository companyRepo = new CompanyRepository(connection);
				if (ModelState.IsValid)
				{
					companyRepo.UpdateCompanyInfo(company, User.UserId);
					company.ListCompanyAddress = companyRepo.GetAllCompanyAddress(User.CurrentCompanyId);
					TempData["CompanyInfoComplete"] = new ResourceString("Admin.CompanySetting.EditComplete");
					return View("CompanyInformation", company);
				}
				company.ListCompanyAddress = companyRepo.GetAllCompanyAddress(User.CurrentCompanyId);
			}
			return View("CompanyInformation", company);
		}
		public ActionResult ViewShippingInformation(string custId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository companyRepo = new CompanyRepository(connection);
				Company company = companyRepo.GetCompany(User.CurrentCompanyId);
				company.CompanyInvoice = companyRepo.GetCompanyInvoice(custId);
				company.ListShipAddress = companyRepo.GetCompanyShipping(User.CurrentCompanyId).Where(s => s.CustId == custId);
				int count_ship = company.ListShipAddress.Count();
				return View("CompanyInformation", company);
			}
		}
		[HttpGet]
		public ActionResult CompanyLogo()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Company company = new CompanyRepository(connection).GetCompanyDetail(User.CurrentCompanyId);
				return View(company);
			}
		}
		[HttpGet]
		public ActionResult CompanyLogoImage(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ImageData companyLogo = new CompanyRepository(connection).GetCompanyLogo(companyId);
				if (companyLogo.Bytes.All(b => b == 0))
				{
					return new FilePathResult("/images/logocompany.jpg", "image/gif");
				}
				else
				{
					return new FileContentResult(companyLogo.Bytes, "image/png");
				}
			}
		}
		[HttpPost]
		public ActionResult UploadLogo(HttpPostedFileBase file)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (file == null)
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.Image");
				}
				else if (!file.ContentType.StartsWith("image"))
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.ImageFile");
				}
				else if (file.ContentLength > (512 * 1024))
				{
					TempData["errormessage"] = new ResourceString("ErrorMessage.ImageLarger");
				}
				else
				{
					CompanyRepository companyRepo = new CompanyRepository(connection);
					System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);
					companyRepo.UpdateCompanyLogo(ConvertImageToByteArray(image, System.Drawing.Imaging.ImageFormat.Jpeg), User.CurrentCompanyId, User.UserId);
					Company company = companyRepo.GetCompanyDetail(User.CurrentCompanyId);
					TempData["errormessage"] = new ResourceString("Admin.CompanySetting.EditComplete");
					return View("CompanyLogo", company);
				}
				return RedirectToAction("CompanyLogo");
			}
		}

		public ActionResult RemoveCompanyLogo()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				new CompanyRepository(connection).RemoveCompanyLogo(User.CurrentCompanyId);
			}
			return RedirectToAction("CompanyLogo");
		}

		// เปลี่ยนไฟล์รูปเป็น ByteArray เพื่อเก็บลง Database
		private byte[] ConvertImageToByteArray(System.Drawing.Image imageToConvert, System.Drawing.Imaging.ImageFormat formatOfImage)
		{
			byte[] Ret;
			try
			{
				using (MemoryStream ms = new MemoryStream())
				{
					imageToConvert.Save(ms, formatOfImage);
					Ret = ms.ToArray();
				}
			}
			catch (Exception) { throw; }
			return Ret;
		}
		[HttpGet]
		public ActionResult CompanySetting()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Company company = new CompanyRepository(connection).GetCompanyDetail(User.CurrentCompanyId);
				return View(company);
			}
		}
		[HttpPost]
		public ActionResult CompanySetting(Company company)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				new CompanyRepository(connection).UpdateCompanySetting(company, User.UserId);
				TempData["CompanySettingComplete"] = new ResourceString("Admin.CompanySetting.EditComplete");
			}
			return View("CompanySetting", company);
		}
		public ActionResult CompanyInfoReview()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Company company = new Company();
				company.CompanyLogoReview = new CompanyRepository(connection).GetCompanyLogo(User.CurrentCompanyId);
				return View(company);
			}
		}

		public ActionResult DepartmentGuide()
		{
			return View();
		}
		public ActionResult ViewAllDepartment()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<CompanyDepartment> companyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(User.Company.CompanyId);
				return View(new MasterPageData<IEnumerable<CompanyDepartment>>(companyDepartment));
			}
		}
		[HttpGet]
		public ActionResult EditDepartment(string departmentId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartment companyDepartment = new CompanyDepartmentRepository(connection).GetCompanyDepartment(User.Company.CompanyId, departmentId);
				return View(companyDepartment);
			}
		}
		[HttpPost]
		public ActionResult EditDepartment(CompanyDepartment companyDepartment)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (ModelState.IsValid)
				{
					new CompanyDepartmentRepository(connection).UpdateCompanyDepartment(companyDepartment, User.UserId);
					return RedirectToAction("ViewAllDepartment");
				}
			}
			return View(companyDepartment);
		}
		public ActionResult RemoveDepartment(string companyId, string departmentId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (new OrderRepository(connection).HaveOrderWaitingProcess(companyId, departmentId, string.Empty, string.Empty))
				{
					TempData["deleteError"] = new ResourceString("Error.DeleteDepartment");
				}
				else
				{
					new CompanyDepartmentRepository(connection).RemoveCompanyDepartment(companyId, departmentId, User.UserId);
				}
				return RedirectToAction("ViewAllDepartment");
			}
		}
		[HttpGet]
		public ActionResult CreateDepartment()
		{
			CompanyDepartment department = new CompanyDepartment();
			department.CompanyId = User.Company.CompanyId;
			return View(department);
		}
		[HttpPost]
		public ActionResult CreateDepartment(CompanyDepartment department)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartmentRepository companyDepRepo = new CompanyDepartmentRepository(connection);
				CompanyRepository compRepo = new CompanyRepository(connection);
				if (ModelState.IsValid)
				{
					if (!companyDepRepo.IsDepartmentIdInSystem(department.CompanyId, department.DepartmentID))
					{
						department.DepartmentStatus = CompanyDepartment.DeptStatus.Active;
						companyDepRepo.CreateCompanyDepartment(department, User.UserId);
						if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder && User.Company.BudgetLevelType == Company.BudgetLevel.Department) // ถ้าเป็นกรณีคุม Budget
						{
							compRepo.CreateBudget(User.Company.BudgetLevelType.ToString(), User.Company.BudgetPeriodType.ToString(), User.CurrentCompanyId);
						}
						return RedirectToAction("ViewAllDepartment");
					}
					else
					{
						ModelState.AddModelError("DepartmentID", "Shared.DepartmentID.Error");
					}
				}
			}
			return View(department);
		}

		public ActionResult CostCenterGuide()
		{
			return View();
		}
		public ActionResult ViewAllCostCenter()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<CostCenter> costcenter = new CostCenterRepository(connection).GetAllCostCenter(User.Company.CompanyId);
				return View(new MasterPageData<IEnumerable<CostCenter>>(costcenter));
			}
		}
		[HttpGet]
		public ActionResult EditCostCenter(string costCenterID)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				CostCenter costCenter = costRepo.GetCostcenterDetailForAdmin(User.CurrentCompanyId, costCenterID);
				costCenter.SelectShipID = costCenter.CostCenterShipping.ShipID.ToString();
				costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(User.Company.CompanyId);
				costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.CostCenterCustID);
				return View(costCenter);
			}
		}
		[HttpPost]
		public ActionResult EditCostCenter(CostCenter costCenter, ImageInputData action)
		{
			switch (action.Name)
			{
				case "SaveEditCostCenter":
					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						CostCenterRepository costRepo = new CostCenterRepository(connection);
						if (ModelState.IsValid)
						{
							if (String.IsNullOrEmpty(costCenter.SelectShipID))
							{
								TempData["ErrorSelectShipID"] = "กรุณาเลือกสถานที่จัดส่งด้วยค่ะ";
							}
							else
							{
								if (!User.Company.IsCompModelThreeLevel) { costCenter.DepartmentID = ""; }
								if (!User.Company.IsByPassAdmin) { costCenter.UseByPassAdmin = false; }
								if (!User.Company.IsAutoApprove) { costCenter.UseAutoApprove = false; }
								if (!User.Company.IsByPassApprover) { costCenter.UseByPassApprover = false; }
								costRepo.UpdateCostcenter(costCenter, User.UserId);
								return RedirectToAction("ViewAllCostCenter");
							}
						}
						costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(User.Company.CompanyId);
						if (string.IsNullOrEmpty(costCenter.SelectInvoice))
						{
							costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.ListInvoiceAddress.First().CustId);
						}
						else
						{
							costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.SelectInvoice);
						}
					}
					ShowInvAndShipAddress(costCenter, false);
					return View(costCenter);
				case "ShowAddressForEdit":
					return ShowInvAndShipAddress(costCenter, false);
				default:
					return EditCostCenter(costCenter.CostCenterID);
			}
		}
		public ActionResult RemoveCostCenter(string companyId, string costCenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (new OrderRepository(connection).HaveOrderWaitingProcess(companyId, string.Empty, costCenterId, string.Empty))
				{
					TempData["deleteError"] = new ResourceString("Error.DeleteCostCenter");
				}
				else
				{
					new CostCenterRepository(connection).RemoveCostCenter(companyId, costCenterId, User.UserId);
				}
				return RedirectToAction("ViewAllCostCenter");
			}
		}
		[HttpGet]
		public ActionResult CreateCostCenter()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				CostCenter costCenter = new CostCenter();
				if (User.Company.IsCompModelThreeLevel)
				{
					costCenter.ListCompanyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(User.Company.CompanyId);
				}
				costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(User.Company.CompanyId);
				costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.ListInvoiceAddress.First().CustId);
				costCenter.CompanyId = User.Company.CompanyId;
				return View(costCenter);
			}
		}
		[HttpPost]
		public ActionResult CreateCostCenter(CostCenter costCenter, ImageInputData action)
		{
			switch (action.Name)
			{
				case "CreateCostCenter":
					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						CompanyDepartmentRepository companydepRepo = new CompanyDepartmentRepository(connection);
						CostCenterRepository costRepo = new CostCenterRepository(connection);
						CompanyRepository compRepo = new CompanyRepository(connection);
						if (User.Company.IsCompModelThreeLevel && string.IsNullOrEmpty(costCenter.DepartmentID))
						{
							ModelState.AddModelError("DepartmentID", "Admin.EditDepartment.ErrorDepartmentIDEmptry");
						}
						else if (ModelState.IsValid)
						{
							if (String.IsNullOrEmpty(costCenter.SelectShipID))
							{
								TempData["ErrorSelectShipID"] = "กรุณาเลือกสถานที่จัดส่งด้วยค่ะ";
							}
							else if (!costRepo.IsCostcenterIdInSystem(costCenter.CompanyId, costCenter.CostCenterID))
							{
								if (!User.Company.IsCompModelThreeLevel) { costCenter.DepartmentID = ""; }
								if (!User.Company.IsByPassAdmin) { costCenter.UseByPassAdmin = false; }
								if (!User.Company.IsAutoApprove) { costCenter.UseAutoApprove = false; }
								if (!User.Company.IsByPassApprover) { costCenter.UseByPassApprover = false; }
								costRepo.CreateCostcenter(costCenter, User.UserId);
								if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder && User.Company.BudgetLevelType == Company.BudgetLevel.Costcenter) // ถ้าเป็นกรณีคุม Budget
								{
									compRepo.CreateBudget(User.Company.BudgetLevelType.ToString(), User.Company.BudgetPeriodType.ToString(), User.CurrentCompanyId);
								}
								return RedirectToAction("ViewAllCostCenter");
							}
							else
							{
								ModelState.AddModelError("CostCenterID", "Shared.CostCenterID.Error");
							}
						}
						costCenter.ListCompanyDepartment = companydepRepo.GetMyCompanyDepartment(User.Company.CompanyId);
						costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(User.Company.CompanyId);
						if (string.IsNullOrEmpty(costCenter.SelectInvoice))
						{
							costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.SelectInvoice);
						}
						else
						{
							costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.ListInvoiceAddress.First().CustId);
						}
						costCenter.CompanyId = User.Company.CompanyId;
						ShowInvAndShipAddress(costCenter, true);
					}
					return View(costCenter);
				case "ShowAddressForCreate":
					return ShowInvAndShipAddress(costCenter, true);
				case "AddNewShipAddress":
					return Shipping();
				default:
					return CreateCostCenter();
			}
		}
		private ActionResult ShowInvAndShipAddress(CostCenter costCenter, bool isCreate)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartmentRepository deptRepo = new CompanyDepartmentRepository(connection);
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				costCenter.ListCompanyDepartment = deptRepo.GetMyCompanyDepartment(costCenter.CompanyId);
				costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(costCenter.CompanyId);
				if (!string.IsNullOrEmpty(costCenter.SelectInvoice))
				{
					costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.SelectInvoice);
					costCenter.CostCenterInvoice = new Eprocurement2012.Models.Invoice();
					costCenter.CostCenterCustID = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().CustId;
					costCenter.CostCenterInvoice.Address1 = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().Address1;
					costCenter.CostCenterInvoice.Address2 = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().Address2;
					costCenter.CostCenterInvoice.Address3 = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().Address3;
					costCenter.CostCenterInvoice.Address4 = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().Address4;
				}
				else
				{
					costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.ListInvoiceAddress.First().CustId);
					costCenter.CostCenterInvoice = null;
				}
				if (!string.IsNullOrEmpty(costCenter.SelectShipID) && costCenter.ListShipAddress.Any(s => s.ShipID.ToString() == costCenter.SelectShipID))
				{
					costCenter.CostCenterShipping = new Shipping();
					costCenter.CostCenterShipping.ShipID = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().ShipID;
					costCenter.CostCenterShipping.Address1 = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().Address1;
					costCenter.CostCenterShipping.Address2 = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().Address2;
					costCenter.CostCenterShipping.Address3 = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().Address3;
					costCenter.CostCenterShipping.Province = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().Province;
					costCenter.CostCenterShipping.ZipCode = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().ZipCode;
				}
				else
				{
					costCenter.CostCenterShipping = null;
				}
				if (isCreate)
				{
					return View("CreateCostCenter", costCenter);
				}
				return View("EditCostCenter", costCenter);
			}
		}

		public ActionResult UserGuide()
		{
			return View();
		}
		public ActionResult ViewAllUser()
		{
			return ViewAllUser(false);
		}
		public ActionResult ViewAllAdmin()
		{
			return ViewAllUser(true);
		}
		private ActionResult ViewAllUser(bool IsAdmin)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				ModelViewListUserData data = new ModelViewListUserData();
				if (IsAdmin)
				{
					data.UserData = userRepo.GetAllUser(User.CurrentCompanyId, IsAdmin);
					return View("ViewAllAdmin", data);
				}
				else
				{
					data.UserData = userRepo.GetAllUser(User.CurrentCompanyId, IsAdmin);
					return View("ViewAllUser", data);
				}
			}
		}
		public ActionResult ManageUser(ImageInputData action)
		{
			switch (action.Name)
			{
				case "EditUser":
					return RedirectToAction("EditUser", new { userGuid = action.Value });
				case "SetUserReq":
					return RedirectToAction("SetRequesterLineForUser", new { userGuid = action.Value, companyId = User.CurrentCompanyId });
				case "RemoveUser":
					return RemoveUser(action.Value);
				case "SetRootAdmin":
					return SetRootAdmin(action.Value, User.CurrentCompanyId);
				default:
					return ViewAllUser();
			}
		}
		public ActionResult ViewUserDetail(string userGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				EditProfileUser user = new UserRepository(connection).GetUserDataForEdit(userGuid, User.CurrentCompanyId);
				if (user == null) { return ViewAllUser(); }
				return View(user);
			}
		}
		[HttpGet]
		public ActionResult EditUser(string userGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				EditProfileUser user = new UserRepository(connection).GetUserDataForEdit(userGuid, User.CurrentCompanyId);
				if (user == null) { return ViewAllUser(); }
				return View("EditUser", user);
			}
		}
		[HttpPost]
		public ActionResult EditUser(EditProfileUser userData)
		{
			userData.CurrentCompanyId = User.CurrentCompanyId;
			if (ModelState.IsValid)
			{
				if (!userData.IsRequester && !userData.IsApprover && !userData.IsAssistantAdmin && !userData.IsObserve)
				{
					ModelState.AddModelError("IsApprover", "CreateUser.RoleEmptry");
				}
				else
				{
					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						UserRepository userRepo = new UserRepository(connection);
						if (userData.Status == Eprocurement2012.Models.User.UserStatus.Cancel && userRepo.GetUserReqOrAppPermission(userData.Email, userData.CurrentCompanyId).Any())
						{
							TempData["EditUserCancel"] = new ResourceString("EditUser.ErrorEditUserCancel");
						}
						else
						{
							OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repo = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(connection);
							userRepo.UpdateUserLanguage(userData.Email, (User.Language)Enum.Parse(typeof(User.Language), userData.DefaultLang, true));
							if (userData.Status == Eprocurement2012.Models.User.UserStatus.Cancel)
							{
								userRepo.UpdateUserStatus(userData.UserGuid, userData.Status, User.UserId, User.CurrentCompanyId);
								//if (userRepo.IsUserMultiCompany(User.UserId))
								//{
								userRepo.UpdateIsDefaultUserStatus(userData.UserGuid, userData.Status, User.UserId, User.CurrentCompanyId);
								//}
							}
							else
							{
								userRepo.UpdateUserStatus(userData.UserGuid, userData.Status, User.UserId, User.CurrentCompanyId);
								userRepo.UpdateIsDefaultUserStatus(userData.UserGuid, userData.Status, User.UserId, User.CurrentCompanyId);
							}


							OfficeMate.Framework.CustomerData.PhoneNumber phone;

							//ดึงข้อมูล CustId ที่ผูกอยู่กับองค์กรทั้งหมด
							IEnumerable<string> itemCustId = new CompanyRepository(connection).GetAllCompanyAddress(userData.CurrentCompanyId).Select(c => c.CustId);
							//ดึงข้อมูล Userid นี้ที่ผูกอยู่กับองค์กรทั้งหมด
							IEnumerable<CreateNewUser> ListUser = new UserRepository(connection).GetUserInfoAndPhoneInCompany(userData.CurrentCompanyId, userData.Email);

							foreach (var custId in itemCustId)
							{
								if (ListUser.Any(id => id.DefaultCustId == custId))//check ว่า custid ที่รับมามีอยู่ในข้อมูลของ user เพื่อใช้ในการ edit
								{
									int SequenceId = ListUser.FirstOrDefault(id => id.DefaultCustId == custId).ContactId;
									int PhoneId = ListUser.FirstOrDefault(id => id.DefaultCustId == custId).PhoneId;
									int MobileId = ListUser.FirstOrDefault(id => id.DefaultCustId == custId).MobileId;
									int FaxId = ListUser.FirstOrDefault(id => id.DefaultCustId == custId).FaxId;

									/*------------------------------ User Name -------------------------*/
									userData.SequenceId = SequenceId;
									userRepo.UpdateUserName(userData, User.UserId);

									/*------------------------------ phone number -------------------------*/
									phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
									if (ListUser.Any(id => id.DefaultCustId == custId && id.PhoneId == 0)) //new phone number
									{
										phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
										phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
										phone.CustomerId = custId;
										phone.SequenceId = SequenceId;
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Phone) ? userData.Phone : "";
										phone.Extension = !string.IsNullOrEmpty(userData.PhoneExt) ? userData.PhoneExt : "";
										repo.InsertPhoneNumber(phone, "EproV5");
									}
									else //edit phone number
									{
										phone = repo.SelectPhone(PhoneId);
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Phone) ? userData.Phone : "";
										phone.Extension = !string.IsNullOrEmpty(userData.PhoneExt) ? userData.PhoneExt : "";
										repo.UpdatePhoneNumber(phone, "EproV5");
									}

									/*-------------------------- mobile number -----------------------------*/
									phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
									if (ListUser.Any(id => id.DefaultCustId == custId && id.MobileId == 0)) //new phone number
									{
										phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
										phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.Mobile;
										phone.CustomerId = custId;
										phone.SequenceId = SequenceId;
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Mobile) ? userData.Mobile : "";
										phone.Extension = "";
										repo.InsertPhoneNumber(phone, "EproV5");
									}
									else //edit phone number
									{
										phone = repo.SelectPhone(MobileId);
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Mobile) ? userData.Mobile : "";
										phone.Extension = "";
										repo.UpdatePhoneNumber(phone, "EproV5");
									}

									/*------------------------ fax number -------------------------------*/
									phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
									if (ListUser.Any(id => id.DefaultCustId == custId && id.FaxId == 0)) //new phone number
									{
										phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
										phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.FaxOut;
										phone.CustomerId = custId;
										phone.SequenceId = SequenceId;
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Fax) ? userData.Fax : "";
										phone.Extension = "";
										repo.InsertPhoneNumber(phone, "EproV5");
									}
									else //edit phone number
									{
										phone = repo.SelectPhone(FaxId);
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Fax) ? userData.Fax : "";
										phone.Extension = "";
										repo.UpdatePhoneNumber(phone, "EproV5");
									}
									/*--------------------------end-----------------------------*/
								}
							}
							userRepo.EditRoleUserFromAdmin(userData, User.UserId);
							TempData["EditUserComplete"] = new ResourceString("Admin.CompanySetting.EditComplete");
						}
						return RedirectToAction("EditUser", new { userGuid = userData.UserGuid });
					}
				}
			}
			return View("EditUser", userData);
		}
		private ActionResult RemoveUser(string userGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				new UserRepository(connection).UpdateUserStatus(userGuid, Models.User.UserStatus.Cancel, User.UserId, User.CurrentCompanyId);
				return RedirectToAction("ViewAllUser");
			}
		}
		[HttpGet]
		public ActionResult CreateNewUser()
		{
			return View(new CreateNewUser());
		}
		[HttpPost]
		public ActionResult CreateNewUser(CreateNewUser createNewUser)
		{
			if (!createNewUser.IsInitialPassword) //Check ก่อนว่า ต้องการให้ระบบตั้ง Password ให้ไหม
			{
				if (string.IsNullOrEmpty(createNewUser.Password))
				{
					ModelState.AddModelError("Password", "CreateUser.PasswordEmptry");
					return View("CreateNewUser", createNewUser);
				}
				if (!createNewUser.Password.Equals(createNewUser.RePassword))
				{
					ModelState.AddModelError("Password", "CreateUser.PasswordNotEqual");
					return View("CreateNewUser", createNewUser);
				}
			}

			if (!createNewUser.IsApprover && !createNewUser.IsRequester && !createNewUser.IsAssistantAdmin && !createNewUser.IsObserve) //Check ไม่ได้เลือก Role มาเลย
			{
				ModelState.AddModelError("IsApprover", "CreateUser.RoleEmptry");
				return View("CreateNewUser", createNewUser);
			}

			if (!ModelState.IsValid) { return View("CreateNewUser", createNewUser); }

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				if (userRepo.IsUserInSystem(createNewUser.Email, User.CurrentCompanyId))
				{
					ModelState.AddModelError("Email", "CreateUser.EmailInSystem");
					return View("CreateNewUser", createNewUser);
				}

				//แจ้งเตือน กรณีลูกค้าเคยเป็นลูกค้า OFM
				//if (userRepo.IsUserInOFMSystem(createNewUser.Email) && !createNewUser.IsUserOFM)
				//{
				//    TempData["ErrorOFMSystem"] = new ResourceString("ErrorMessage.OFMSystem");
				//    return View("CreateNewUser", createNewUser);
				//}
				//else
				//{
					//แก้ไขปัญหากรณี 1 User สามารถใช้งานได้แค่ 1 Web เท่านั้น
					//if (userRepo.IsUserInOFMSystem(createNewUser.Email))
					//{
					//    ModelViewListUserData data = new ModelViewListUserData();
					//    data.UserData = userRepo.GetAllUser(User.CurrentCompanyId, true);
					//    userRepo.DeleteUserInOFM(createNewUser.Email);
					//    data.ProcessLog = SetUserOFMForAdmin(data, createNewUser.Email);
					//    userRepo.AddRootAdminProcessLog(data.ProcessLog, User.UserId);
					//}

				OfficeMate.Framework.CustomerData.Contact contact = new OfficeMate.Framework.CustomerData.Contact();
				OfficeMate.Framework.CustomerData.PhoneNumber phone;
				List<OfficeMate.Framework.CustomerData.PhoneNumber> itemPhone = new List<OfficeMate.Framework.CustomerData.PhoneNumber>();
					createNewUser.DefaultCompanyId = User.CurrentCompanyId;

					//ดึงข้อมูล CustId ที่ผูกอยู่กับองค์กรทั้งหมด
					IEnumerable<string> itemCustId = new CompanyRepository(connection).GetAllCompanyAddress(createNewUser.DefaultCompanyId).Select(c => c.CustId);

					if (!userRepo.UserExistMasterUser(createNewUser.Email)) //สร้างข้อมูลฝั่ง master
					{
						if (createNewUser.IsInitialPassword)
						{
							createNewUser.Password = "officemate";
							createNewUser.RePassword = createNewUser.Password;
						}
						userRepo.InsertUserMaster(createNewUser.Email, createNewUser.Password);
					}

					foreach (var custId in itemCustId)
					{
						//create contact
						contact.Email = createNewUser.Email;
						contact.CustomerId = custId;
						if (createNewUser.IsThaiLanguage)
						{
							contact.Name = createNewUser.ThaiName;
						}
						else
						{
							contact.Name = createNewUser.EngName;
						}
						contact.FaxNumber = createNewUser.Fax;
						contact.MobileNumber = createNewUser.Mobile;

						phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
						phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
						phone.PhoneNo = createNewUser.Phone;
						phone.Extension = !string.IsNullOrEmpty(createNewUser.PhoneExt) ? createNewUser.PhoneExt : "";
						phone.IsDefault = true;
						itemPhone.Add(phone);

						if (!string.IsNullOrEmpty(createNewUser.Mobile))
						{
							phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
							phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.Mobile;
							phone.PhoneNo = createNewUser.Mobile;
							phone.IsDefault = true;
							itemPhone.Add(phone);
						}

						if (!string.IsNullOrEmpty(createNewUser.Fax))
						{
							phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
							phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.FaxOut;
							phone.PhoneNo = createNewUser.Fax;
							phone.IsDefault = true;
							itemPhone.Add(phone);
						}

						contact.Phones = itemPhone;

						new OfficeMate.Framework.CustomerData.Repositories.ContactRepository(connection).InsertContact(contact, "EproV5"); //Insert Contact
					//}

					userRepo.CreateNewUser(createNewUser, User.UserId);//สร้างข้อมูลฝั่ง epro

					if (!User.Company.UseOfmCatalog)//หากคุมสินค้าด้วย (มีสินค้าเป็นของตัวเอง) ต้องทำการ GentDeptStructure
					{
						new GenerateDeptStructureControl(connection).GentDeptStructure(User.CurrentCompanyId);
					}

					string verifyKey;
					if (userRepo.TryVerifyUser(createNewUser.Email, out verifyKey))
					{
						if (createNewUser.SetVerifyNow)
						{
							ForgotPasswordData forgot = userRepo.GetForgotPasswordData(createNewUser.Email);//ใช้ Model เหมือนกัน
							SendMail(new Eprocurement2012.Controllers.Mails.VerifyUserMail(ControllerContext, forgot), "", MailType.VerifyUser, User.UserId);
						}
					}
				}
			}
			return RedirectToAction("ViewAllUser");
		}
		private SetRequesterLine GetUserRequesterLineDetail(string userGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				SetRequesterLine setReq = new SetRequesterLine();
				setReq.UserReqDetail = userRepo.GetUser(userGuid, User.UserStatus.Active);
				setReq.ListCostCenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId).Where(c => c.CostStatus == CostCenter.CostCenterStatus.Active);
				setReq.CostCurrentPermission = new CostCenterRepository(connection).GetMyCostCenter(setReq.UserReqDetail.UserId, User.CurrentCompanyId, User.UserType.Requester);
				setReq.UserApprover = userRepo.GetAllUserReqOrApp(User.CurrentCompanyId).Where(u => u.UserRoleName == User.UserRole.Approver && u.UserStatusName == Models.User.UserStatus.Active);
				setReq.InformationRequesterLine = userRepo.GetDataRequesterlineForUserSide(User.CurrentCompanyId, setReq.UserReqDetail.UserId);
				return setReq;
			}
		}

		public ActionResult SetVerifyUser(string userGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				string verifyKey, userId;
				userId = userRepo.GetUserIdFromUserGuid(userGuid);
				if (userRepo.TryVerifyUser(userId, out verifyKey))
				{
					ForgotPasswordData forgot = userRepo.GetForgotPasswordData(userId);//ใช้ Model เหมือนกัน
					SendMail(new Eprocurement2012.Controllers.Mails.VerifyUserMail(ControllerContext, forgot), "", MailType.VerifyUser, User.UserId);
				}
			}
			return RedirectToAction("ViewAllUser");
		}
		[HttpGet]
		public ActionResult SetRequesterLineForUser(string userGuid, string companyId)
		{
			if (!User.CurrentCompanyId.ToUpper().Equals(companyId.ToUpper()))//ตรวจสอบสิทธิว่ากำลังแก้ไข CompanyId ที่ตัวเองมีสิทธิใช้
			{
				return Http403();
			}
			SetRequesterLine setReq = GetUserRequesterLineDetail(userGuid);
			setReq.SelectCostcenter = new string[] { };
			setReq.SelectApprover = new string[3];
			setReq.SelectParkday = new int[3];
			setReq.SelectBudget = new decimal[3];
			return View(setReq);
		}
		[HttpPost]
		public ActionResult SetRequesterLineForUser(string[] costcenterId, string[] userApprover, int[] parkday, decimal[] approverBudget, string userGuid, string[] costcenterName)
		{
			SetRequesterLine reqLineDetail = GetUserRequesterLineDetail(userGuid);
			reqLineDetail.SelectCostcenter = costcenterId;
			reqLineDetail.SelectApprover = userApprover;
			reqLineDetail.SelectParkday = parkday;
			reqLineDetail.SelectBudget = approverBudget;

			if (reqLineDetail.SelectCostcenter == null)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.SelectCostcenter");
				return View(reqLineDetail);
			}
			else
			{
				int count = costcenterId.Count();
				List<string[,]> costcenters = new List<string[,]>();
				for (int i = 0; i < count; i++)
				{
					costcenters.Add(new string[,] { { costcenterId[i], costcenterName[i] } });
				}
				TempData["costcenterId"] = costcenters;//ส่ง UserId กลับไป Display ที่หน้า View กรณีที่กรอกข้อมุลไม่ครบ
			}

			if (reqLineDetail.SelectApprover.All(a => a == ""))
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
				return View(reqLineDetail);
			}

			bool IsPassCheckBudget = true;
			decimal budget = 0;
			int level = 0;
			foreach (var item in reqLineDetail.SelectBudget)
			{
				if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[level]) && item == 0) { break; }
				if (budget >= item)
				{
					IsPassCheckBudget = false;
					break;
				}
				budget = item;
				level++;
			}

			if (!IsPassCheckBudget)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorBudget");
				return View(reqLineDetail);
			}

			bool IsPassCheckApp = true;
			for (int i = 0; i < reqLineDetail.SelectApprover.Count(); i++)
			{
				if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[i])) { break; }
				for (int j = i + 1; j < reqLineDetail.SelectApprover.Count(); j++)
				{
					if (reqLineDetail.SelectApprover[i] == reqLineDetail.SelectApprover[j])
					{
						IsPassCheckApp = false;
						break;
					}
				}
			}

			if (!IsPassCheckApp)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
				return View(reqLineDetail);
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				if (!string.IsNullOrEmpty(reqLineDetail.SelectApprover[0]))
				{
					foreach (var item in reqLineDetail.SelectCostcenter)
					{
						int i = 0;
						foreach (var itemApp in reqLineDetail.SelectApprover)
						{
							SetRequesterLine reqLineForSave = null;
							if (!string.IsNullOrEmpty(itemApp))
							{
								reqLineForSave = new SetRequesterLine();
								reqLineForSave.CompanyId = User.CurrentCompanyId;
								reqLineForSave.CostcenterId = item;
								reqLineForSave.RequesterUserId = reqLineDetail.UserReqDetail.UserId;
								reqLineForSave.ApproverUserId = itemApp;
								reqLineForSave.AppCreditlimit = reqLineDetail.SelectBudget[i];
								reqLineForSave.Parkday = Convert.ToInt32(reqLineDetail.SelectParkday[i]);
								reqLineForSave.CreateBy = User.UserId;
								reqLineForSave.UpdateBy = User.UserId;
								reqLineForSave.AppLevel = i + 1;
								userRepo.SetUserReqLineAndApp(reqLineForSave);
								i += 1;
							}
							else
							{
								break;
							}
						}
					}
				}
				TempData["Error"] = new ResourceString("Admin.CompanySetting.EditComplete");
				TempData["costcenterId"] = null;
				return RedirectToAction("SetRequesterLineForUser", new { userGuid = userGuid, companyId = User.CurrentCompanyId });
			}
		}
		public ActionResult ResetRequesterLineForUser(string costcenterId, string userGuid)
		{
			return RedirectToAction("SetRequesterLineByUser", new { costcenterId = costcenterId, userGuid = userGuid });	//สำหรับการเซ็ท RequesterLine ให้ไปหน้า Edit
		}

		[HttpGet]
		public ActionResult SetRequesterLineByUser(string costcenterId, string userGuid)
		{
			SetRequesterLine setReq = null;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				string requesterId = userRepo.GetUserIdFromUserGuid(userGuid);
				setReq = SetDataRequestLine(User.CurrentCompanyId, costcenterId, requesterId);


				List<string> ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == requesterId).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
				TempData["ErrorResetReqLine"] = "";
				foreach (var approverId in ItemUser)
				{
					if (costcenterRepo.IsReqAndAppInOrder(User.CurrentCompanyId, costcenterId, requesterId, approverId))
					{
						TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
					}
				}
			}
			return View(setReq);
		}

		[HttpPost]
		public ActionResult SetRequesterLineByUser(string[] costcenterId, string[] userApprover, int[] parkday, decimal[] approverBudget, string userGuid, string userRequester, string SetRequesterLineBack, string DeleteRequesterLine)
		{
			if (SetRequesterLineBack != null)
			{
				return RedirectToAction("SetRequesterLineForUser", new { userGuid = userGuid, companyId = User.CurrentCompanyId });
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (DeleteRequesterLine != null)
				{
					SetRequesterLine setReq = SetDataRequestLine(User.CurrentCompanyId, costcenterId[0], userRequester);
					UserRepository userRepo = new UserRepository(connection);
					userRepo.ClearReqLineAndAppByUser(User.CurrentCompanyId, costcenterId[0], userRequester);
					TempData["Error"] = new ResourceString("SetRequesterLine.DeleteSuccess");
					return RedirectToAction("SetRequesterLineForUser", new { userGuid = userGuid, companyId = User.CurrentCompanyId });
				}

				else
				{

					SetRequesterLine setReq = SetDataRequestLine(User.CurrentCompanyId, costcenterId[0], userRequester);
					SetRequesterLine reqLineDetail = GetUserRequesterLineDetail(userGuid);
					reqLineDetail.SelectCostcenter = costcenterId;
					reqLineDetail.SelectApprover = userApprover;
					reqLineDetail.SelectParkday = parkday;
					reqLineDetail.SelectBudget = approverBudget;

					//เช็คว่ามีการผูกสิทธิ์ กับ Order ที่ยังดำเนินการไม่เสร็จหรือไม่
					List<string> ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
					int count = ItemUser.Count();
					for (int i = 0; i < count; i++)
					{
						if (ItemUser[i] != userApprover[i])
						{
							if (new CostCenterRepository(connection).IsReqAndAppInOrder(User.CurrentCompanyId, costcenterId[0], userRequester, ItemUser[i]))
							{
								TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
								return View(setReq);
							}
						}
					}

					if (reqLineDetail.SelectCostcenter == null)
					{
						TempData["Error"] = new ResourceString("SetRequesterLine.SelectCostcenter");
						return View("SetRequesterLineForUser", reqLineDetail);
					}

					if (reqLineDetail.SelectApprover.All(a => a == ""))
					{
						TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
						return View("SetRequesterLineForUser", reqLineDetail);
					}

					bool IsPassCheckBudget = true;
					decimal budget = 0;
					int level = 0;
					foreach (var item in reqLineDetail.SelectBudget)
					{
						if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[level]) && item == 0) { break; }
						if (budget >= item)
						{
							IsPassCheckBudget = false;
							break;
						}
						budget = item;
						level++;
					}

					if (!IsPassCheckBudget)
					{
						TempData["Error"] = new ResourceString("SetRequesterLine.ErrorBudget");
						return View("SetRequesterLineForUser", reqLineDetail);
					}

					bool IsPassCheckApp = true;
					for (int i = 0; i < reqLineDetail.SelectApprover.Count(); i++)
					{
						if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[i])) { break; }
						for (int j = i + 1; j < reqLineDetail.SelectApprover.Count(); j++)
						{
							if (reqLineDetail.SelectApprover[i] == reqLineDetail.SelectApprover[j])
							{
								IsPassCheckApp = false;
								break;
							}
						}
					}

					if (!IsPassCheckApp)
					{
						TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
						return View("SetRequesterLineForUser", reqLineDetail);
					}

					CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
					UserRepository userRepo = new UserRepository(connection);
					userRepo.ClearReqLineAndAppByUser(User.CurrentCompanyId, costcenterId[0], userRequester);
					if (!string.IsNullOrEmpty(reqLineDetail.SelectApprover[0]))
					{
						foreach (var item in reqLineDetail.SelectCostcenter)
						{
							int i = 0;
							foreach (var itemApp in reqLineDetail.SelectApprover)
							{
								SetRequesterLine reqLineForSave = null;
								if (!string.IsNullOrEmpty(itemApp))
								{
									reqLineForSave = new SetRequesterLine();
									reqLineForSave.CompanyId = User.CurrentCompanyId;
									reqLineForSave.CostcenterId = item;
									reqLineForSave.RequesterUserId = reqLineDetail.UserReqDetail.UserId;
									reqLineForSave.ApproverUserId = itemApp;
									reqLineForSave.AppCreditlimit = reqLineDetail.SelectBudget[i];
									reqLineForSave.Parkday = Convert.ToInt32(reqLineDetail.SelectParkday[i]);
									reqLineForSave.CreateBy = User.UserId;
									reqLineForSave.UpdateBy = User.UserId;
									reqLineForSave.AppLevel = i + 1;
									userRepo.SetUserReqLineAndApp(reqLineForSave);
									i += 1;
								}
								else
								{
									break;
								}
							}
						}
					}
					setReq = SetDataRequestLine(User.CurrentCompanyId, costcenterId[0], userRequester);
					ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
					TempData["ErrorResetReqLine"] = "";
					foreach (var approverId in ItemUser)
					{
						if (costcenterRepo.IsReqAndAppInOrder(User.CurrentCompanyId, costcenterId[0], userRequester, approverId))
						{
							TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
						}
					}
					TempData["Success"] = new ResourceString("SetRequesterLine.Complete");
					return View(setReq);
				}
			}
		}

		public ActionResult LearnAboutApproval()
		{
			return View();
		}
		public ActionResult UserFinalReview()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				FinalReview finalReview = new FinalReview();
				finalReview.UserPurchase = new UserRepository(connection).GetAllUser(User.CurrentCompanyId, false);
				finalReview.RequesterLineDetail = new CompanyRepository(connection).GetAllCompanyRequesterLine(User.CurrentCompanyId);
				return View(finalReview);
			}
		}

		public ActionResult RequesterLineGuide()
		{
			return View();
		}
		private SetRequesterLine GetRequesterLineDetail(string costcenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				SetRequesterLine setReq = new SetRequesterLine();
				setReq.CostCenter = costcenterRepo.GetCostcenterDetailForAdmin(User.CurrentCompanyId, costcenterId);
				setReq.UserCurrentPermission = userRepo.GetUserReqOrAppPermission(User.CurrentCompanyId);
				IEnumerable<User> listUser = userRepo.GetAllUserReqOrApp(User.CurrentCompanyId).Where(u => u.UserStatusName == Models.User.UserStatus.Active);
				setReq.UserRequester = listUser.Where(u => u.UserRoleName == User.UserRole.Requester);
				setReq.UserApprover = listUser.Where(u => u.UserRoleName == User.UserRole.Approver);
				setReq.InformationRequesterLine = userRepo.GetDataRequesterline(User.CurrentCompanyId, costcenterId);
				return setReq;
			}
		}
		[HttpGet]
		public ActionResult SetRequesterLine(string costcenterId, string companyId)
		{
			if (!User.CurrentCompanyId.ToUpper().Equals(companyId.ToUpper()))//ตรวจสอบสิทธิว่ากำลังแก้ไข CompanyId ที่ตัวเองมีสิทธิใช้
			{
				return Http403();
			}
			SetRequesterLine setReq = GetRequesterLineDetail(costcenterId);
			setReq.SelectRequester = new string[] { };
			setReq.SelectApprover = new string[3];
			setReq.SelectParkday = new int[3];
			setReq.SelectBudget = new decimal[3];
			return View(setReq);
		}

		public ActionResult AutoCompleteSetRequesterLine(string companyId, int rows, string term)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				IEnumerable<string> data = userRepo.GetAllRequester(companyId, term);
				return Json(data.Take(rows), JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult AutoCompleteSetRequesterLineForUser(string companyId, int rows, string term)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				IEnumerable<string> data = costcenterRepo.GetAllCostCenterForRequesterLine(companyId, term);
				return Json(data.Take(rows), JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult CheckHasRequesterLine(string companyId, string costcenterId, string reqUserId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				var data = userRepo.GetDefaultRequester(companyId, costcenterId, reqUserId);
				return Json(data, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult CheckHasCostCenterInSystem(string companyId, string costcenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				var data = costcenterRepo.IsCostcenterIdInSystem(companyId, costcenterId);
				return Json(data, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult CheckHasUserIdInSystem(string companyId,string userId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				var data = userRepo.IsUserInSystem(userId,companyId);
				return Json(data, JsonRequestBehavior.AllowGet);
			}
		}



		[HttpGet]
		public ActionResult SetRequesterLineByOwner(string costcenterId, string companyId, string requesterGuid)
		{
			if (companyId == null) { return Http403(); }
			if (!User.CurrentCompanyId.ToUpper().Equals(companyId.ToUpper()))//ตรวจสอบสิทธิว่ากำลังแก้ไข CompanyId ที่ตัวเองมีสิทธิใช้
			{
				return Http403();
			}
			SetRequesterLine setReq = null;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				string requesterId = userRepo.GetUserIdFromUserGuid(requesterGuid);
				setReq = SetDataRequestLine(companyId, costcenterId, requesterId);

				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);

				List<string> ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == requesterId).Select(a => a.ApproverId).ToList();
				TempData["ErrorResetReqLine"] = "";
				TempData["Error"] = "";
				foreach (var approverId in ItemUser)
				{
					if (costcenterRepo.IsReqAndAppInOrder(companyId, costcenterId, requesterId, approverId))
					{
						TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
					}
				}
			}
			return View(setReq);
		}


		private SetRequesterLine SetDataRequestLine(string companyId, string costcenterId, string userRequester)
		{
			SetRequesterLine setReq = GetRequesterLineDetail(costcenterId);
			setReq.UserReqDetail = new Models.User();
			setReq.CompanyId = companyId;
			setReq.UserReqDetail.UserEngName = setReq.UserRequester.Where(o => o.UserId == userRequester).Select(o => o.UserEngName).SingleOrDefault();
			setReq.UserReqDetail.UserThaiName = setReq.UserRequester.Where(o => o.UserId == userRequester).Select(o => o.UserThaiName).SingleOrDefault();
			setReq.UserReqDetail.UserId = setReq.UserRequester.Where(o => o.UserId == userRequester).Select(o => o.UserId).SingleOrDefault();
			setReq.UserReqDetail.UserGuid = setReq.UserRequester.Where(o => o.UserId == userRequester).Select(o => o.UserGuid).SingleOrDefault();
			setReq.UserReqDetail.DisplayName = new LocalizedString(setReq.UserReqDetail.UserThaiName);
			setReq.UserReqDetail.DisplayName["en"] = setReq.UserReqDetail.UserEngName;

			int countApprover = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).Count();

			setReq.SelectApprover = new string[3];
			setReq.SelectParkday = new int[3];
			setReq.SelectBudget = new decimal[3];

			for (int i = 0; i < countApprover; i++)
			{
				setReq.SelectApprover[i] = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester && o.ApproverLevel == i + 1).Select(o => o.ApproverId).SingleOrDefault();
				setReq.SelectParkday[i] = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester && o.ApproverLevel == i + 1).Select(o => o.Parkday).SingleOrDefault();
				setReq.SelectBudget[i] = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester && o.ApproverLevel == i + 1).Select(o => o.ApproverCreditBudget).SingleOrDefault();
			}

			return setReq;
		}

		[HttpPost]
		public ActionResult SetRequesterLineByOwner(string userRequester, string[] userApprover, int[] parkday, decimal[] approverBudget, string costcenterId, string companyId, string SetRequesterLineBack, string DeleteRequesterLine)
		{

			if (SetRequesterLineBack != null)
			{
				return RedirectToAction("SetRequesterLine", new { costcenterId = costcenterId, companyId = companyId });
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (DeleteRequesterLine != null)
				{
					SetRequesterLine setReq = SetDataRequestLine(companyId, costcenterId, userRequester);
					UserRepository userRepo = new UserRepository(connection);
					userRepo.ClearReqLineAndAppByUser(companyId, costcenterId, userRequester);
					TempData["Error"] = new ResourceString("SetRequesterLine.DeleteSuccess");
					return RedirectToAction("SetRequesterLine", new { costcenterId = costcenterId, companyId = User.CurrentCompanyId });
				}
				else
				{
					SetRequesterLine setReq = SetDataRequestLine(companyId, costcenterId, userRequester);
					SetRequesterLine reqLineDetail = GetRequesterLineDetail(costcenterId);
					reqLineDetail.SelectRequester = new string[] { userRequester, null, null };
					reqLineDetail.SelectApprover = userApprover;
					reqLineDetail.SelectParkday = parkday;
					reqLineDetail.SelectBudget = approverBudget;

					//เช็คว่ามีการผูกสิทธิ์ กับ Order ที่ยังดำเนินการไม่เสร็จหรือไม่
					List<string> ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
					int count = ItemUser.Count();
					for (int i = 0; i < count; i++)
					{
						if (ItemUser[i] != userApprover[i])
						{
							if (new CostCenterRepository(connection).IsReqAndAppInOrder(companyId, costcenterId, userRequester, ItemUser[i]))
							{
								TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
								return View(setReq);
							}
						}
					}

					if (reqLineDetail.SelectRequester == null)
					{
						TempData["Error"] = new ResourceString("SetRequesterLine.SelectRequester");
						TempData["ErrorResetReqLine"] = "";
						return View(setReq);
					}

					if (reqLineDetail.SelectApprover.All(a => a == ""))
					{
						TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
						TempData["ErrorResetReqLine"] = "";
						return View(setReq);
					}

					bool IsPassCheckBudget = true;
					decimal budget = 0;
					int level = 0;
					foreach (var item in reqLineDetail.SelectBudget)
					{
						if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[level]) && item == 0) { break; }
						if (budget >= item)
						{
							IsPassCheckBudget = false;
							break;
						}
						budget = item;
						level++;
					}

					if (!IsPassCheckBudget)
					{
						TempData["Error"] = new ResourceString("SetRequesterLine.ErrorBudget");
						TempData["ErrorResetReqLine"] = "";
						return View(setReq);
					}

					bool IsPassCheckApp = true;
					for (int i = 0; i < reqLineDetail.SelectApprover.Count(); i++)
					{
						if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[i])) { break; }
						for (int j = i + 1; j < reqLineDetail.SelectApprover.Count(); j++)
						{
							if (reqLineDetail.SelectApprover[i] == reqLineDetail.SelectApprover[j])
							{
								IsPassCheckApp = false;
								break;
							}
						}
					}

					if (!IsPassCheckApp)
					{
						TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
						TempData["ErrorResetReqLine"] = "";
						return View(setReq);
					}

					UserRepository userRepo = new UserRepository(connection);
					userRepo.ResetRequestline(costcenterId, User.CurrentCompanyId, userRequester);

					CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
					if (!string.IsNullOrEmpty(reqLineDetail.SelectApprover[0]))
					{
						int i = 0;
						foreach (var itemApp in reqLineDetail.SelectApprover)
						{
							SetRequesterLine reqLineForSave = null;
							if (!string.IsNullOrEmpty(itemApp))
							{
								reqLineForSave = new SetRequesterLine();
								reqLineForSave.CompanyId = User.CurrentCompanyId;
								reqLineForSave.CostcenterId = costcenterId;
								reqLineForSave.RequesterUserId = reqLineDetail.SelectRequester[0];
								reqLineForSave.ApproverUserId = itemApp;
								reqLineForSave.AppCreditlimit = reqLineDetail.SelectBudget[i];
								reqLineForSave.Parkday = Convert.ToInt32(reqLineDetail.SelectParkday[i]);
								reqLineForSave.CreateBy = User.UserId;
								reqLineForSave.UpdateBy = User.UserId;
								reqLineForSave.AppLevel = i + 1;
								userRepo.SetUserReqLineAndApp(reqLineForSave);
								i += 1;
							}
							else
							{
								break;
							}
						}
					}

					setReq = SetDataRequestLine(companyId, costcenterId, userRequester);
					ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
					TempData["ErrorResetReqLine"] = "";
					TempData["Error"] = "";
					foreach (var approverId in ItemUser)
					{
						if (costcenterRepo.IsReqAndAppInOrder(companyId, costcenterId, userRequester, approverId))
						{
							TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
						}
					}
					TempData["Success"] = new ResourceString("SetRequesterLine.Complete");
					return View(setReq);
				}
			}
		}

		[HttpPost]
		public ActionResult SetRequesterLine(string[] userRequester, string[] userApprover, int[] parkday, decimal[] approverBudget, string costcenterId, string[] userName)
		{
			SetRequesterLine reqLineDetail = GetRequesterLineDetail(costcenterId);
			reqLineDetail.SelectRequester = userRequester;
			reqLineDetail.SelectApprover = userApprover;
			reqLineDetail.SelectParkday = parkday;
			reqLineDetail.SelectBudget = approverBudget;


			if (reqLineDetail.SelectRequester == null)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.SelectRequester");
				return View(reqLineDetail);
			}
			else
			{
				int count = userRequester.Count();
				List<string[,]> users = new List<string[,]>();
				for (int i = 0; i < count; i++)
				{
					users.Add(new string[,] { { userName[i].ToString(), userRequester[i].ToString() } });

				}
				TempData["userRequester"] = users;//ส่ง userName และ UserId กลับไป Display ที่หน้า View กรณีที่กรอกข้อมุลไม่ครบ
			}

			if (reqLineDetail.SelectApprover.All(a => a == ""))
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
				return View(reqLineDetail);
			}

			bool IsPassCheckBudget = true;
			decimal budget = 0;
			int level = 0;
			foreach (var item in reqLineDetail.SelectBudget)
			{
				if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[level]) && item == 0) { break; }
				if (budget >= item)
				{
					IsPassCheckBudget = false;
					break;
				}
				budget = item;
				level++;
			}

			if (!IsPassCheckBudget)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorBudget");
				return View(reqLineDetail);
			}

			bool IsPassCheckApp = true;
			for (int i = 0; i < reqLineDetail.SelectApprover.Count(); i++)
			{
				if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[i])) { break; }
				for (int j = i + 1; j < reqLineDetail.SelectApprover.Count(); j++)
				{
					if (reqLineDetail.SelectApprover[i] == reqLineDetail.SelectApprover[j])
					{
						IsPassCheckApp = false;
						break;
					}
				}
			}

			if (!IsPassCheckApp)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
				return View(reqLineDetail);
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				if (!string.IsNullOrEmpty(reqLineDetail.SelectApprover[0]))
				{
					foreach (var item in reqLineDetail.SelectRequester)
					{
						int i = 0;
						foreach (var itemApp in reqLineDetail.SelectApprover)
						{
							SetRequesterLine reqLineForSave = null;
							if (!string.IsNullOrEmpty(itemApp))
							{
								reqLineForSave = new SetRequesterLine();
								reqLineForSave.CompanyId = User.CurrentCompanyId;
								reqLineForSave.CostcenterId = costcenterId;
								reqLineForSave.RequesterUserId = item;
								reqLineForSave.ApproverUserId = itemApp;
								reqLineForSave.AppCreditlimit = reqLineDetail.SelectBudget[i];
								reqLineForSave.Parkday = Convert.ToInt32(reqLineDetail.SelectParkday[i]);
								reqLineForSave.CreateBy = User.UserId;
								reqLineForSave.UpdateBy = User.UserId;
								reqLineForSave.AppLevel = i + 1;
								userRepo.SetUserReqLineAndApp(reqLineForSave);
								i += 1;
							}
							else
							{
								break;
							}
						}
					}
				}
				TempData["Error"] = new ResourceString("Admin.CompanySetting.EditComplete");
				TempData["userRequester"] = null; //เคลียร์ค่าไม่ให้แสดงหน้า View
				return RedirectToAction("SetRequesterLine", new { costcenterId = costcenterId, companyId = User.CurrentCompanyId });
			}
		}
		public ActionResult ResetRequesterLine(string costcenterId, string userGuid)
		{
			return RedirectToAction("SetRequesterLineByOwner", new { costcenterId = costcenterId, companyId = User.CurrentCompanyId, requesterGuid = userGuid });
		}



		public ActionResult FinalReview(string name)
		{
			if (name == "Department")
			{
				return FinalReviewDepartment();
			}

			if (name == "CostCenter")
			{
				return FinalReviewCostCenter();
			}

			if (name == "User")
			{
				return FinalReviewUser();
			}

			if (name == "RequesterLine")
			{
				return FinalReviewRequesterLine();
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository companyRepo = new CompanyRepository(connection);
				CostCenterRepository costCenterRepo = new CostCenterRepository(connection);
				FinalReview finalReview = new FinalReview();
				finalReview.FinalCompany = companyRepo.GetCompanyDetail(User.CurrentCompanyId);
				finalReview.FinalCompany.ListShipAddress = companyRepo.GetCompanyShipping(User.CurrentCompanyId);
				finalReview.Invoice = costCenterRepo.GetAllInvoidAddress(User.CurrentCompanyId);
				return View(finalReview);
			}
		}

		private ActionResult FinalReviewCostCenter()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				FinalReview finalReview = new FinalReview();
				finalReview.CostCenter = new CostCenterRepository(connection).GetAllCostCenter(User.CurrentCompanyId);
				return View("FinalReviewForCostCenter", finalReview);
			}
		}

		private ActionResult FinalReviewUser()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				FinalReview finalReview = new FinalReview();
				finalReview.UserAdmin = userRepo.GetAllUser(User.CurrentCompanyId, true);
				finalReview.UserPurchase = userRepo.GetAllUser(User.CurrentCompanyId, false);
				return View("FinalReviewForUser", finalReview);
			}
		}

		private ActionResult FinalReviewRequesterLine()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				FinalReview finalReview = new FinalReview();
				finalReview.RequesterLineDetail = new CompanyRepository(connection).GetAllCompanyRequesterLine(User.CurrentCompanyId);
				return View("FinalReviewForRequesterLine", finalReview);
			}
		}

		private ActionResult FinalReviewDepartment()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				FinalReview finalReview = new FinalReview();
				finalReview.CompanyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(User.CurrentCompanyId);
				return View("FinalReviewForDepartment", finalReview);
			}
		}


		public ActionResult AllSetting()
		{
			return View();
		}

		public ActionResult ViewProductCatalog()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductCatalogRepository productcatRepo = new ProductCatalogRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				productCatalog.CountCompanyCatalog = productcatRepo.GetCountProductCatalogType(User.CurrentCompanyId, ProductCatalog.ProductCatalogType.Company);
				productCatalog.CountUserCatalog = productcatRepo.GetCountProductCatalogType(User.CurrentCompanyId, ProductCatalog.ProductCatalogType.User);
				return View(productCatalog);
			}
		}
		[HttpPost]
		public ActionResult ViewProductCatalog(string showProduct)
		{
			if (!string.IsNullOrEmpty(showProduct))
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					ProductCatalogRepository productcatRepo = new ProductCatalogRepository(connection);
					ProductCatalog productCatalog = new ProductCatalog();
					productCatalog.CountCompanyCatalog = productcatRepo.GetCountProductCatalogType(User.CurrentCompanyId, ProductCatalog.ProductCatalogType.Company);
					productCatalog.CountUserCatalog = productcatRepo.GetCountProductCatalogType(User.CurrentCompanyId, ProductCatalog.ProductCatalogType.User);
					productCatalog.ItemProductCatalog = productcatRepo.GetProductCatalog(User.CurrentCompanyId);
					return View(productCatalog);
				}
			}
			return View(new ProductCatalog());
		}
		[HttpGet]
		public ActionResult AddProductCatalogByPID()
		{
			if (User.Company.UseOfmCatalog) { return Index(true); }
			return View("AddProductCatalogByPID", GetProductCatalogRequest(User.CurrentCompanyId));
		}
		[HttpPost]
		public ActionResult AddProductCatalogByPID(string productId, ImageInputData action)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (action.Name == "AddToCartByPId")
				{
					ProductCatalogRepository catalogRepo = new ProductCatalogRepository(connection);
					if (string.IsNullOrEmpty(productId))
					{
						TempData["Error"] = new ResourceString("ProductCatalog.AddProductErrorPID0");
					}
					else if (catalogRepo.HaveProductInYourRequestList(productId, User.CurrentCompanyId))
					{
						TempData["Error"] = String.Format(new ResourceString("ProductCatalog.AddProductErrorPID1"), productId);
					}
					else if (catalogRepo.HaveProductInYourCompanyList(productId, User.CurrentCompanyId))
					{
						TempData["Error"] = String.Format(new ResourceString("ProductCatalog.AddProductErrorPID2"), productId);
					}
					else
					{
						catalogRepo.AddProductCatalogRequest(productId, User.CurrentCompanyId, User.UserId);
					}
				}
				else if (action.Name == "deleteProduct" && !string.IsNullOrEmpty(action.Value))
				{
					ProductCatalogRepository procatalogRepo = new ProductCatalogRepository(connection);
					procatalogRepo.DeleteProductCatalogRequest(action.Value, User.CurrentCompanyId);
				}
				return View("AddProductCatalogByPID", GetProductCatalogRequest(User.CurrentCompanyId));
			}
		}

		[HttpGet]
		public ActionResult AddProductCatalogByTemplate()
		{
			if (User.Company.UseOfmCatalog) { return Index(true); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductTemplateData templateData = new ProductTemplateData();
				templateData.ProductTemplate = GetTemplateCatalog();
				return View("AddProductCatalogByTemplate", templateData);
			}
		}
		[HttpPost]
		public ActionResult AddProductCatalogByTemplate(string[] listproductId, string templateId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				ProductCatalogRepository catalogRepo = new ProductCatalogRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				if (listproductId == null || !listproductId.Any())
				{
					ProductTemplateData templateData = new ProductTemplateData();
					TempData["Error"] = new ResourceString("ProductCatalog.EmptrySelectProduct");
					templateData.ProductTemplate = GetTemplateCatalog();
					templateData.ItemProduct = catalogRepo.GetItemProductTemplate(templateId);
					templateData.CurrentTemplateId = templateId;
					return View("AddProductCatalogByTemplate", templateData);
				}

				foreach (var itemId in listproductId)
				{
					if (!catalogRepo.HaveProductInYourRequestList(itemId, User.CurrentCompanyId) && !catalogRepo.HaveProductInYourCompanyList(itemId, User.CurrentCompanyId))
					{
						catalogRepo.AddProductCatalogRequest(itemId, User.CurrentCompanyId, User.UserId);
					}
				}
				productCatalog = GetProductCatalogRequest(User.CurrentCompanyId);
				if (productCatalog.ItemProductCatalog.Count() == 0)
				{
					TempData["Error"] = new ResourceString("ProductCatalog.ErrorProductCatalogByTemplate");
					return RedirectToAction("AddProductCatalogByTemplate");
				}
				return RedirectToAction("SetProductCatalogRequest");
			}
		}
		public ActionResult GetDatailProductTemplate(string templateId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (templateId == null)
				{
					TempData["Error"] = new ResourceString("ProductCatalog.ErrorAddProductCatalogByTemplate");
					return RedirectToAction("AddProductCatalogByTemplate");
				}
				ProductCatalogRepository productcatRepo = new ProductCatalogRepository(connection);
				ProductTemplateData templateData = new ProductTemplateData();
				templateData.ProductTemplate = GetTemplateCatalog();
				templateData.ItemProduct = productcatRepo.GetItemProductTemplate(templateId);
				templateData.CurrentTemplateId = templateId;
				return View("AddProductCatalogByTemplate", templateData);
			}
		}
		private IEnumerable<ProductTemplate> GetTemplateCatalog()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductCatalogRepository productcatRepo = new ProductCatalogRepository(connection);
				return productcatRepo.GetProductTemplate();
			}
		}
		public ActionResult SetProductCatalogRequest(ImageInputData action)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				if (action != null && action.Name == "deleteProduct" && !string.IsNullOrEmpty(action.Value))
				{
					new ProductCatalogRepository(connection).DeleteProductCatalogRequest(action.Value, User.CurrentCompanyId);
				}
				productCatalog = GetProductCatalogRequest(User.CurrentCompanyId);
				productCatalog.UserRequester = userRepo.GetAllUserReqOrApp(User.CurrentCompanyId).Where(r => r.UserRoleName == Models.User.UserRole.Requester);
				return View("SetProductCatalogRequest", productCatalog);
			}
		}
		public ActionResult ViewProductAddCatalogSuccess()
		{
			return View("ProductAddCatalogSuccess");
		}
		public ActionResult AddProductCatalog(string[] listuserId, bool IsByCompany, string StartDate, string EndDate)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				DateTime ValidFrom = new DateTime();
				DateTime ValidTo = new DateTime();
				if (string.IsNullOrEmpty(StartDate)) //ไม่ได้ Setค่า วันที่เริ่มต้นมากำหนดเป็นเวลาปัจจุบัน
				{

					ValidFrom = DateTime.Now;
				}
				else
				{
					ValidFrom = DateTime.ParseExact(StartDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
				}
				if (string.IsNullOrEmpty(EndDate)) //ไม่ได้ Setค่า วันที่สุดท้ายมากำหนดเป็นเวลาปัจจุบัน+50 ปี
				{
					ValidTo = DateTime.Now.AddYears(50);
				}
				else
				{
					ValidTo = DateTime.ParseExact(EndDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
				}
				ProductCatalogRepository catalogRepo = new ProductCatalogRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				productCatalog = GetProductCatalogRequest(User.CurrentCompanyId);
				if (IsByCompany)//เพิ่มทั้ง Company
				{
					foreach (var itempId in productCatalog.ItemProductCatalog)
					{
						if (!catalogRepo.HaveProductInYourCompanyList(itempId.Id, User.CurrentCompanyId))
						{
							catalogRepo.AddProductCatalog(itempId.Id, User.CurrentCompanyId, ValidFrom, ValidTo, User.UserId);
						}
					}
					new GenerateDeptStructureControl(connection).GentDeptStructure(User.CurrentCompanyId);
				}
				else//เพิ่มเฉพาะคน
				{
					if (listuserId == null)  //ไม่ได้เลือก UserId มา
					{
						TempData["ErrorSelectUser"] = String.Format(new ResourceString("ProductCatalog.ErrorSelectUser"));
						return SetProductCatalogRequest(null);
					}
					foreach (var itemUser in listuserId)
					{
						foreach (var itempId in productCatalog.ItemProductCatalog)
						{
							if (!catalogRepo.HaveProductInYourCompanyList(itempId.Id, User.CurrentCompanyId))
							{
								if (!catalogRepo.HaveProductInYourUserList(itempId.Id, User.CurrentCompanyId, itemUser))
								{
									catalogRepo.AddProductCatalogToRequester(itempId.Id, itemUser, User.CurrentCompanyId, ValidFrom, ValidTo, User.UserId);
								}
							}
						}
					}
					new GenerateDeptStructureControl(connection).GentDeptStructure(User.CurrentCompanyId);
				}
				return RedirectToAction("ViewProductAddCatalogSuccess");
			}
		}
		private ProductCatalog GetProductCatalogRequest(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductCatalogRepository procatalogRepo = new ProductCatalogRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				productCatalog.ItemProductCatalog = procatalogRepo.GetProductCatalogRequestWaitForSetting(companyId);
				return productCatalog;
			}
		}

		public ActionResult NewsCompany()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<News> itemNews = new NewsRepository(connection).GetAllNewsCompany(User.CurrentCompanyId);
				return View(new MasterPageData<IEnumerable<News>>(itemNews));
			}
		}
		public ActionResult NewsCompanyDetail(string newsGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				News news = new NewsRepository(connection).GetNewsDetail(newsGuid, User.CurrentCompanyId);
				return View(news);
			}
		}
		[HttpGet]
		public ActionResult ManageNews(string newsGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				News news = new NewsRepository(connection).GetNewsDetail(newsGuid, User.CurrentCompanyId);
				return View(news);
			}
		}
		[HttpPost]
		public ActionResult ManageNews(News news)
		{
			if (ModelState.IsValid)
			{
				DateTime endYear = new DateTime(DateTime.Now.Year, 12, 31);
				if (news.Period == "Now")
				{
					news.ValidFrom = DateTime.Today;
					news.ValidTo = endYear;
				}
				else
				{
					if (String.IsNullOrEmpty(news.ValidFromDisplay) || String.IsNullOrEmpty(news.ValidToDisplay))
					{
						ModelState.AddModelError("ValidFrom", "Admin.EditNewsCompany.ErrorNewsValidFromOrValidToEmptry");
						return View("ManageNews", news);
					}

					DateTime fromDateTime, toDateTime;
					if (!String.IsNullOrEmpty(news.ValidFromDisplay) && DateTime.TryParseExact(news.ValidFromDisplay, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateTime))
					{
						news.ValidFrom = fromDateTime;
					}
					if (!String.IsNullOrEmpty(news.ValidToDisplay) && DateTime.TryParseExact(news.ValidToDisplay, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDateTime))
					{
						news.ValidTo = toDateTime;
					}
				}
				if (news.ValidFrom < DateTime.Today) { news.ValidFrom = DateTime.Today; }
				if (news.ValidTo > endYear) { news.ValidTo = endYear; }

				if (news.ValidFrom > news.ValidTo)
				{
					ModelState.AddModelError("ValidFrom", "Admin.CreateNewsCompany.ErrorValid");
					return View("ManageNews", news);
				}
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					NewsRepository newsRepo = new NewsRepository(connection);
					news.UpdateBy = User.UserId;
					news.CompanyID = User.CurrentCompanyId;
					newsRepo.UpdateNewsCompany(news);
					TempData["NewsComplete"] = new ResourceString("Admin.NewsCompany.EditNewsComplete");
				}
			}
			return View("ManageNews", news);
		}

		[ValidateInput(false)]
		public ActionResult HandleAction(News news, ImageInputData Action)
		{
			switch (Action.Name)
			{
				case "AddNews":
					return CreateNewsCompany(news);
				case "Save":
					return ManageNews(news);
				case "NewsBack":
					return RedirectToAction("NewsCompany");
				default:
					return View("ManageNews");
			}
		}
		[HttpGet]
		public ActionResult CreateNewsCompany()
		{
			News news = new News();
			news.CompanyID = User.CurrentCompanyId;
			return View("ManageNews", news);
		}
		[HttpPost]
		public ActionResult CreateNewsCompany(News news)
		{
			if (ModelState.IsValid)
			{
				DateTime endYear = new DateTime(DateTime.Now.Year, 12, 31);
				if (news.Period == "Now")
				{
					news.ValidFrom = DateTime.Today;
					news.ValidTo = endYear;
				}
				else
				{
					if (String.IsNullOrEmpty(news.ValidFromDisplay) || String.IsNullOrEmpty(news.ValidToDisplay))
					{
						ModelState.AddModelError("ValidFrom", "Admin.EditNewsCompany.ErrorNewsValidFromOrValidToEmptry");
						return View("ManageNews", news);
					}

					DateTime fromDateTime, toDateTime;
					if (!String.IsNullOrEmpty(news.ValidFromDisplay) && DateTime.TryParseExact(news.ValidFromDisplay, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateTime))
					{
						news.ValidFrom = fromDateTime;
					}
					if (!String.IsNullOrEmpty(news.ValidToDisplay) && DateTime.TryParseExact(news.ValidToDisplay, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDateTime))
					{
						news.ValidTo = toDateTime;
					}
				}
				if (news.ValidFrom < DateTime.Today) { news.ValidFrom = DateTime.Today; }
				if (news.ValidTo > endYear) { news.ValidTo = endYear; }

				if (news.ValidFrom > news.ValidTo)
				{
					ModelState.AddModelError("ValidFrom", "Admin.CreateNewsCompany.ErrorValid");
					return View("ManageNews", news);
				}

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					NewsRepository newsRepo = new NewsRepository(connection);
					news.CreateBy = User.UserId;
					news.CompanyID = User.CurrentCompanyId;
					news.NewsType = "Private";
					newsRepo.InsertNewsCompany(news);
					return RedirectToAction("NewsCompany");
				}
			}
			return View("ManageNews", news);
		}

		public ActionResult AdminNewsCompany(string newsType)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				NewsRepository newsRepo = new NewsRepository(connection);
				IEnumerable<News> itemNews = null;
				if (newsType == "Private")
				{
					itemNews = newsRepo.GetAllNewsCompany(User.CurrentCompanyId);
				}
				else
				{
					itemNews = newsRepo.GetAllOFMNews();
				}
				return View(new MasterPageData<IEnumerable<News>>(itemNews));
			}
		}
		public ActionResult AdminNewsCompanyDetail(string newsGuid, string newsType)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				NewsRepository newsRepo = new NewsRepository(connection);
				News news = new News();
				if (newsType == "Private")
				{
					news = newsRepo.GetNewsDetail(newsGuid, User.CurrentCompanyId);
				}
				else
				{
					news = newsRepo.GetNewsDetail(newsGuid, "OfficeMate");
				}
				if (news == null) { return Index(true); }
				return View(news);
			}
		}

		public ActionResult RemoveProduct(string guid, string productId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductCatalogRepository productCatalogRepo = new ProductCatalogRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				string userId = new UserRepository(connection).GetUserIdFromUserGuid(guid);
				productCatalogRepo.DeleteProductCatalog(User.CurrentCompanyId, ProductCatalog.ProductCatalogType.User, userId, productId, User.UserId);
				productCatalog.CountCompanyCatalog = productCatalogRepo.GetCountProductCatalogType(User.CurrentCompanyId, ProductCatalog.ProductCatalogType.Company);
				productCatalog.CountUserCatalog = productCatalogRepo.GetCountProductCatalogType(User.CurrentCompanyId, ProductCatalog.ProductCatalogType.User);
				productCatalog.ItemProductCatalog = productCatalogRepo.GetProductCatalog(User.CurrentCompanyId);
				return View("ViewProductCatalog", productCatalog);
			}
		}

		public ActionResult Shipping()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ShippingData shippingData = new ShippingData();
				CompanyRepository companyRepo = new CompanyRepository(connection);
				shippingData.OtherShippings = companyRepo.GetCompanyShipping(User.CurrentCompanyId);
				shippingData.ListCustId = companyRepo.GetAllCompanyAddress(User.CurrentCompanyId).Select(c => c.CustId);
				shippingData.ProvinceList = new ProvinceEproRepository(connection).GetProvince();
				return View("ManageShipping", shippingData);
			}
		}

		public ActionResult ShippingInfo(ShippingData shippingData, ImageInputData action)
		{
			switch (action.Name)
			{
				case "CreateShipping":
					return CreateShipping(shippingData);
				case "SaveShipping":
					return EditShipping(shippingData);
				default:
					return RedirectToAction("Shipping");
			}
		}

		private ActionResult CreateShipping(ShippingData shippingData)
		{
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					OfficeMate.Framework.CustomerData.Repositories.ShippingRepository shippingRepo = new OfficeMate.Framework.CustomerData.Repositories.ShippingRepository(connection);
					OfficeMate.Framework.CustomerData.CustomerShipping shipping = new OfficeMate.Framework.CustomerData.CustomerShipping();

					List<OfficeMate.Framework.CustomerData.PhoneNumber> phones = new List<OfficeMate.Framework.CustomerData.PhoneNumber>();
					OfficeMate.Framework.CustomerData.PhoneNumber phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
					phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
					phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Shipping;
					phone.PhoneNo = shippingData.ShipPhoneNumber;
					phone.Extension = shippingData.ShipPhoneNumberExtension ?? String.Empty;
					phone.CustomerId = shippingData.CustIdSelected;
					phone.SequenceId = shipping.Id;
					phones.Add(phone);

					shipping.Phones = phones;

					shipping.CustomerId = shippingData.CustIdSelected;
					shipping.Contactor = shippingData.ShipAddress1;
					shipping.PhoneNo = shippingData.ShipPhoneNumber;
					shipping.ShipAddress1 = shippingData.ShipAddress1;
					shipping.ShipAddress2 = shippingData.ShipAddress2;
					shipping.ShipAddress3 = shippingData.ShipAddress3 ?? String.Empty;
					shipping.ShipAddress4 = shippingData.ProvinceSelected + " " + shippingData.ShipAddress4;
					shipping.Province = shippingData.ProvinceSelected;
					shipping.ZipCode = shippingData.ShipAddress4;
					shipping.IsDeliveryFee = new ProvinceEproRepository(connection).GetDeliverFee(shipping.Province);
					shippingRepo.InsertShipping(shipping, "EproV5");
					shippingData.ShippingIdSelected = shipping.Id;
				}
				return RedirectToAction("Shipping");
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ShippingData Shipping = new ShippingData();
				CompanyRepository companyRepo = new CompanyRepository(connection);
				Shipping.ProvinceList = new ProvinceEproRepository(connection).GetProvince();
				Shipping.ListCustId = companyRepo.GetAllCompanyAddress(User.CurrentCompanyId).Select(c => c.CustId);
				Shipping.OtherShippings = companyRepo.GetCompanyShipping(User.CurrentCompanyId);
				return View("ManageShipping", Shipping);
			}
		}

		public ActionResult GetShipping(string custId, string shipId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ShippingData shippingData = new ShippingData();
				shippingData.ProvinceList = new ProvinceEproRepository(connection).GetProvinceWithNoDefault();
				shippingData.ListCustId = new CompanyRepository(connection).GetAllCompanyAddress(User.CurrentCompanyId).Select(c => c.CustId);

				IEnumerable<Shipping> shippings = new CompanyRepository(connection).GetCompanyShipping(User.CurrentCompanyId);
				Shipping selectShip = shippings.Where(s => s.ShipID.ToString() == shipId).First();
				shippingData.ShipAddress1 = selectShip.ShipContactor;
				shippingData.PhoneID = selectShip.PhoneID;
				shippingData.ShipPhoneNumber = selectShip.ShipPhoneNo;
				shippingData.ShipPhoneNumberExtension = selectShip.Extension;
				shippingData.ShipAddress2 = selectShip.Address2;
				shippingData.ShipAddress3 = selectShip.Address3;
				shippingData.ProvinceSelected = selectShip.Province;
				shippingData.ShipAddress4 = selectShip.ZipCode;
				shippingData.CustIdSelected = custId;
				shippingData.OtherShippings = shippings.Where(s => s.ShipID.ToString() != shipId);
				shippingData.ShipID = Convert.ToInt32(shipId);
				return View("ManageShipping", shippingData);
			}
		}

		private ActionResult EditShipping(ShippingData shippingData)
		{
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					OfficeMate.Framework.CustomerData.Repositories.ShippingRepository shippingRepo = new OfficeMate.Framework.CustomerData.Repositories.ShippingRepository(connection);
					OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repo = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(connection);
					OfficeMate.Framework.CustomerData.CustomerShipping selectShipping = new OfficeMate.Framework.CustomerData.CustomerShipping();
					OfficeMate.Framework.CustomerData.PhoneNumber phone;

					if (shippingData.PhoneID == 0) //new phone number
					{
						phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
						phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Shipping;
						phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
						phone.CustomerId = shippingData.CustIdSelected;
						phone.SequenceId = shippingData.ShipID;
						phone.PhoneNo = shippingData.ShipPhoneNumber;
						phone.Extension = string.IsNullOrEmpty(shippingData.ShipPhoneNumberExtension) ? "" : shippingData.ShipPhoneNumberExtension;
						repo.InsertPhoneNumber(phone, "EproV5");
						//repo.setDefaultPhoneNumber(phone, "EproV5");
					}
					else //edit phone number
					{
						phone = repo.SelectPhone(shippingData.PhoneID);
						phone.PhoneNo = shippingData.ShipPhoneNumber;
						phone.Extension = string.IsNullOrEmpty(shippingData.ShipPhoneNumberExtension) ? "" : shippingData.ShipPhoneNumberExtension;
						repo.UpdatePhoneNumber(phone, "EproV5");
					}

					selectShipping = shippingRepo.SelectShipping(shippingData.ShipID);
					selectShipping.CustomerId = shippingData.CustIdSelected;
					selectShipping.Contactor = shippingData.ShipAddress1;
					selectShipping.PhoneNo = shippingData.ShipPhoneNumber;
					selectShipping.ShipAddress1 = shippingData.ShipAddress1;
					selectShipping.ShipAddress2 = shippingData.ShipAddress2;
					selectShipping.ShipAddress3 = shippingData.ShipAddress3 ?? String.Empty;
					selectShipping.ShipAddress4 = shippingData.ProvinceSelected + " " + shippingData.ShipAddress4;
					selectShipping.Province = shippingData.ProvinceSelected;
					selectShipping.ZipCode = shippingData.ShipAddress4;
					selectShipping.IsDeliveryFee = new ProvinceEproRepository(connection).GetDeliverFee(selectShipping.Province);

					shippingRepo.UpdateShipping(selectShipping, "EproV5");
				}
				return RedirectToAction("Shipping");
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ShippingData Shipping = new ShippingData();
				CompanyRepository companyRepo = new CompanyRepository(connection);
				Shipping.OtherShippings = companyRepo.GetCompanyShipping(User.CurrentCompanyId);
				Shipping.ListCustId = companyRepo.GetAllCompanyAddress(User.CurrentCompanyId).Select(c => c.CustId);
				Shipping.ProvinceList = new ProvinceEproRepository(connection).GetProvince();
				return View("ManageShipping", Shipping);
			}
		}

		public ActionResult GetDataOrderApproveProcess()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				IEnumerable<Order> orders = new OrderRepository(connection).GetDataOrder(User.CurrentCompanyId, User.UserId, Order.OrderStatus.Approved);
				return View("ViewDataOrder", new MasterPageData<IEnumerable<Order>>(orders));
			}
		}

		public ActionResult ViewOrderForApprover(string Id) //เข้าจากหน้าเว็บ
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(Id);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
				if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)//หากใช้ campany catalog และแสดง ofm catalog จะต้องเช็คว่า สินค้าใดมาจาก ofm catalog บ้าง
				{
					IEnumerable<string> listId = new ProductRepository(connection, User).IsListPidInProductCatalog();
					foreach (var prod in orderData.Order.ItemOrders)
					{
						if (!listId.Any(p => p == prod.Product.Id))
						{
							prod.IsOfmCatalog = true;
							orderData.Order.SendToAdminAppProd = true;
						}
					}
				}
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)//หากมีการคุม budget จะต้องเอายอดใบสั่งซื้อไปลบออกจาก budget
				{
					BudgetRepository repo = new BudgetRepository(connection);
					orderData.CurrentBudget = repo.GetBudgetByCostcenterID(orderData.Order.CostCenter.CostCenterID, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					orderData.CurrentBudget.WaitingOrderAmt = repo.GetWaitingOrderOnThisBudget(orderData.CurrentBudget.GroupID, User.CurrentCompanyId, User.Company.BudgetLevelType);
					orderData.Order.SendToAdminAppBudg = orderData.Order.GrandTotalAmt > orderData.CurrentBudget.RemainBudgetAmt;
				}
				orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
				orderData.Order.User = User; //ใส่ค่า User  เพื่อไว้เช็คใน Partial
				return View("ViewOrderForApprover", orderData);
			}
		}

		public ActionResult ConfirmHandleOrder(ImageInputData action, string orderGuid)
		{
			ConfirmHandelOrder data = new ConfirmHandelOrder();
			data.InputActionName = action.Name;
			data.OrderGuid = orderGuid;
			return View("ConfirmHandleOrder", data);
		}

		public ActionResult HandleOrder(ConfirmHandelOrder ConfirmHandleOrder)
		{
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					if (!new UserRepository(connection).IsPasswordCorrect(User.UserId, ConfirmHandleOrder.Password))
					{
						TempData["Error"] = new ResourceString("Approver.Login.ErrorPasswordEmptry");
						return View("ConfirmHandleOrder", ConfirmHandleOrder);
					}
				}
				if (string.IsNullOrEmpty(ConfirmHandleOrder.Remark)) { ConfirmHandleOrder.Remark = ""; }
				switch (ConfirmHandleOrder.InputActionName)
				{
					case "PartialApprove":
						return PartialApproveOrder(ConfirmHandleOrder.OrderGuid, ConfirmHandleOrder.Remark);
					case "ApproveOrder":
						return ApprovedOrder(ConfirmHandleOrder.OrderGuid, ConfirmHandleOrder.Remark);
					case "DeleteOrder":
						return DeleteOrder(ConfirmHandleOrder.OrderGuid, ConfirmHandleOrder.Remark);
					case "ReviseOrder":
						return ReviseOrder(ConfirmHandleOrder.OrderGuid, ConfirmHandleOrder.Remark);
					case "AdminAllow":
						return AdminPartialApproveOrder(ConfirmHandleOrder.OrderGuid, ConfirmHandleOrder.Remark);
					default:
						return ViewOrderForApprover(ConfirmHandleOrder.OrderGuid);
				}
			}
			return View("ConfirmHandleOrder", ConfirmHandleOrder);
		}

		public ActionResult ApprovedOrder(string orderGuid, string remark)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderRePo.ApproveOrder(orderGuid, User.UserId, remark);
				orderData.Order = orderRePo.GetMyOrder(orderId);
				if (User.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)//หากมีการคุม budget จะต้องเอายอดใบสั่งซื้อไปลบออกจาก budget
				{
					BudgetRepository repo = new BudgetRepository(connection);
					orderData.CurrentBudget = repo.GetBudgetByCostcenterID(orderData.Order.CostCenter.CostCenterID, User.CurrentCompanyId, User.Company.BudgetLevelType, User.Company.BudgetPeriodType);
					repo.UpdateBudgetAfterApproveOrder(orderData.CurrentBudget, orderData.Order.OrderID, orderData.Order.GrandTotalAmt, User.UserId);
				}
				orderData.CurrentOrderActivity = SetActivityForApprove(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreateApprovedOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.ApproveOrderMail(ControllerContext, orderData), orderId, MailType.ApproveOrder, User.UserId);
				//--------------ส่ง SMS หากมีการเลือกรับ SMS แจ้งให้ผู้สั่งซื้อรับทราบ-----------------------------
				if (orderData.Order.UseSMSFeature)
				{
					if (orderData.Order.Requester.UserId != null)
					{
						UserRepository repo = new UserRepository(connection);
						Contact requester = repo.GetUserDataContact(orderData.Order.Requester.UserId, orderData.Order.Company.CompanyId);
						if (requester.Mobile != null)
						{
							if (requester.Mobile.PhoneNo.Length == 10)
							{
								SendSMS_OFM02(orderData.Order.OrderID, orderData.Order.CostCenter.CostCenterCustID, orderData.Order.Requester.UserThaiName, requester);
							}
						}
					}
				}
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}

		public ActionResult PartialApproveOrder(string orderGuid, string remark)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid); //หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);
				orderRePo.PartialApproveOrder(orderGuid, orderData.Order.Requester.UserId, User.UserId, orderData.Order.Company.CompanyId, orderData.Order.CostCenter.CostCenterID, orderData.Order.ParkDay, remark);
				orderData.Order = orderRePo.GetMyOrder(orderId); // ดึงค่า Order ใหม่หลังจาก Status เปลี่ยนแล้ว
				orderData.CurrentOrderActivity = SetActivityForPartialApprove(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreatePartialApproveOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.ApproveAndForwardMail(ControllerContext, orderData), orderId, MailType.ApproveAndForwardOrder, User.UserId);
				//--------------ส่ง SMS หากมีการเลือกรับ SMS แจ้งให้ผู้สั่งซื้อรับทราบ-----------------------------
				if (orderData.Order.UseSMSFeature)
				{
					if (orderData.Order.Requester.UserId != null)
					{
						UserRepository repo = new UserRepository(connection);
						Contact requester = repo.GetUserDataContact(orderData.Order.Requester.UserId, orderData.Order.Company.CompanyId);
						if (requester.Mobile != null)
						{
							if (requester.Mobile.PhoneNo.Length == 10)
							{
								SendSMS_OFM02(orderData.Order.OrderID, orderData.Order.CostCenter.CostCenterCustID, orderData.Order.Requester.UserThaiName, requester);
							}
						}
					}
				}
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}

		public ActionResult AdminPartialApproveOrder(string orderGuid, string remark)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid); //หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);
				orderRePo.AdminPartialApproveOrder(orderGuid, orderData.Order.Requester.UserId, orderData.Order.CurrentApprover.Approver.UserId, User.UserId, orderData.Order.Company.CompanyId, orderData.Order.CostCenter.CostCenterID, orderData.Order.ParkDay, remark);
				orderData.Order = orderRePo.GetMyOrder(orderId); // ดึงค่า Order ใหม่หลังจาก Status เปลี่ยนแล้ว
				orderData.CurrentOrderActivity = SetActivityForAdminPartialApprove(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreateAdminPartialApproveOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.AdminAllowBudgetToRequesterMail(ControllerContext, orderData), orderId, MailType.AdminAllowBudgetToRequester, User.UserId);
				SendMail(new Eprocurement2012.Controllers.Mails.AdminAllowBudgetToApproveMail(ControllerContext, orderData), orderId, MailType.AdminAllowBudgetToApprover, User.UserId);
				//--------------ส่ง SMS หากมีการเลือกรับ SMS แจ้งให้ผู้สั่งซื้อรับทราบ-----------------------------
				if (orderData.Order.UseSMSFeature)
				{
					if (orderData.Order.Requester.UserId != null)
					{
						UserRepository repo = new UserRepository(connection);
						Contact requester = repo.GetUserDataContact(orderData.Order.Requester.UserId, orderData.Order.Company.CompanyId);
						if (requester.Mobile != null)
						{
							if (requester.Mobile.PhoneNo.Length == 10)
							{
								SendSMS_OFM02(orderData.Order.OrderID, orderData.Order.CostCenter.CostCenterCustID, orderData.Order.Requester.UserThaiName, requester);
							}
						}
					}
				}
				if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)//หากใช้ campany catalog และแสดง ofm catalog จะต้องเช็คว่า สินค้าใดมาจาก ofm catalog บ้าง
				{
					IEnumerable<string> listId = new ProductRepository(connection, User).IsListPidInProductCatalog();
					foreach (var prod in orderData.Order.ItemOrders)
					{
						if (!listId.Any(p => p == prod.Product.Id))
						{
							return RedirectToAction("AddProductFromOrder", new { guid = orderGuid });
						}
					}
				}

			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}

		private void SendSMS_OFM02(string orderId, string custId, string userName, Contact contact)
		{
			string message = string.Format("ใบสั่งซื้อเลขที่ {0} ของคุณเรียบร้อยแล้วค่ะ", orderId);
			SendSMSTemplate(orderId, custId, userName, contact, message, "SMS_E-Pro02:ยืนยันการอนุมัติ");
		}

		private void SendSMSTemplate(string orderId, string custId, string userName, Contact contact, string message, string templateName)
		{
			SendSmsOrder(contact.Mobile.PhoneNo, message);
			var smsLog = new OfficeMate.Framework.Data.SmsLog();
			smsLog.SMSTemplate = templateName;
			smsLog.SMSTopic = message;
			smsLog.SenderName = "E-Pro";
			smsLog.CustId = custId;
			smsLog.ContactName = userName;
			smsLog.ContactMobileNo = contact.Mobile.PhoneNo;
			smsLog.WebOrderId = orderId;
			smsLog.InvNo = "";
			smsLog.CreateBy = "System";
			using (SqlConnection connectionMaster = new SqlConnection(ConnectionString))
			{
				new OfficeMate.Framework.Data.Repositories.SmsLogRepository(connectionMaster).InsertLog(smsLog);
			}
		}
		public ActionResult DeleteOrder(string orderGuid, string remark)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderRePo.DeleteOrder(orderGuid, User.UserId, remark);
				orderData.Order = orderRePo.GetMyOrder(orderId);
				orderData.CurrentOrderActivity = SetActivityForDelete(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreateDeleteOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.ApproverDeleteOrderMail(ControllerContext, orderData), orderId, MailType.ApproverDeleteOrder, User.UserId);
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}

		public ActionResult ReviseOrder(string orderGuid, string remark)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderRePo.ReviseOrder(orderGuid, User.UserId, remark); // Update Order เป็น Status Revise
				orderData.Order = orderRePo.GetMyOrder(orderId); // ดึงค่า Order ใหม่หลังจาก Status เปลี่ยนแล้ว
				orderData.CurrentOrderActivity = SetActivityForRevise(orderData.Order);
				orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
				orderRePo.CreateReviseOrderTransactionLog(orderData);
				SendMail(new Eprocurement2012.Controllers.Mails.ApproverSentReviseOrderMail(ControllerContext, orderData), orderId, MailType.ApproverSentReviseOrder, User.UserId);
			}
			return RedirectToAction("ViewOrderForApprover", new { id = orderGuid });
		}
		[HttpGet]
		public ActionResult AddProductFromOrder(string guid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository prodRepo = new ProductRepository(connection, User);
				RePurchase order = new RePurchase();
				List<Product> product = new List<Product>();
				order.OrderId = new OrderRepository(connection).GetOrderIdByGuid(guid);
				IEnumerable<string> productId = new OrderRepository(connection).GetMyOrder(order.OrderId).ItemOrders.Select(p => p.Product.Id).ToList();
				IEnumerable<string> listId = new ProductRepository(connection, User).IsListPidInProductCatalog();
				foreach (var id in productId)
				{
					if (!listId.Any(p => p == id))
					{
						product.Add(prodRepo.GetProductDetail(id));
					}
				}
				order.Products = product;
				order.OrderGuid = guid;
				return View("AddProductFromOrder", order);
			}
		}

		[HttpPost]
		public ActionResult AddProductFromOrder(string[] productId, string guid, ImageInputData action)
		{
			if (action.Name == "AddToCatalog")
			{
				if (productId != null && productId.Any())
				{
					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						ProductCatalogRepository catalogRepo = new ProductCatalogRepository(connection);
						foreach (var Id in productId)
						{
							if (!catalogRepo.HaveProductInYourCompanyList(Id, User.CurrentCompanyId))
							{
								catalogRepo.AddProductCatalog(Id, User.CurrentCompanyId, DateTime.Now, DateTime.Now.AddYears(50), User.UserId);
							}
						}
					}
				}
				else
				{
					TempData["errormsg"] = "กรุณาเลือกสินค้าที่ต้องการเพิ่มลงในแคตตาล็อกองค์กรด้วยค่ะ";
					return AddProductFromOrder(guid);
				}
			}
			return RedirectToAction("ViewOrderForApprover", new { id = guid });
		}

		private OrderActivity SetActivityForApprove(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อผ่านการอนุมัติ";
			orderActivity.ActivityEName = "Approved Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการอนุมัติใบสั่งซื้อหมายเลข: " + order.OrderID;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityForPartialApprove(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อผ่านการอนุมัติแล้วเบื้องต้น";
			orderActivity.ActivityEName = "Partial Approved Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการอนุมัติใบสั่งซื้อหมายเลข: " + order.OrderID + " แล้วเบื้องต้น";
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityForAdminPartialApprove(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อผ่านการอนุมัติเบื้องต้นจากผู้ดูแลระบบแล้ว";
			orderActivity.ActivityEName = "Admin Partial Approved Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการอนุมัติใบสั่งซื้อหมายเลข: " + order.OrderID + " เบื้องต้นแล้ว";
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityForRevise(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อถุกส่งกลับไปแก้ไข";
			orderActivity.ActivityEName = "Revise Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการส่งใบสั่งซื้อหมายเลข: " + order.OrderID + " กลับไปแก้ไข";
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		private OrderActivity SetActivityForDelete(Order order)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อถูกยกเลิก";
			orderActivity.ActivityEName = "Deleted Order";
			orderActivity.Remark = "คุณ " + order.PreviousApprover.Approver.DisplayName + " ได้ทำการอยกเลิกใบสั่งซื้อหมายเลข: " + order.OrderID;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		//ฟังก์ชัน ค้นหา Product เพื่อใช้ในการลบรหัสสินค้าออกจาก ProductCatalog ขององค์กร
		public ActionResult SearchProductCatalog(string keywords)
		{
			if (string.IsNullOrEmpty(keywords))
			{
				TempData["Error"] = new ResourceString("Admin.ViewProductCatalogByPID.EmptyKeyword");
				IEnumerable<ProductCatalog> prop = new List<ProductCatalog>();
				return View("ViewProductCatalogByPID", new MasterPageData<IEnumerable<ProductCatalog>>(prop));
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductCatalogRepository catalogRepo = new ProductCatalogRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				IEnumerable<ProductCatalog> productCatalogs = catalogRepo.GetSearchProductCatalog(keywords, User.CurrentCompanyId);
				if (productCatalogs.Count() == 0)
				{
					TempData["Error"] = new ResourceString("Admin.ViewProductCatalogByPID.ErrorKeyword");
				}
				return View("ViewProductCatalogByPID", new MasterPageData<IEnumerable<ProductCatalog>>(productCatalogs));
			}
		}

		public ActionResult ViewProductCatalogByPID()
		{
			IEnumerable<ProductCatalog> prop = new List<ProductCatalog>();
			return View(new MasterPageData<IEnumerable<ProductCatalog>>(prop));
		}

		public ActionResult DeleteProductCatalog(ProductCatalog.ProductCatalogType PropType, string catalogTypeID, string productId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductCatalogRepository catalogRepo = new ProductCatalogRepository(connection);
				ProductCatalog productCatalog = new ProductCatalog();
				catalogRepo.DeleteProductCatalog(User.CurrentCompanyId, PropType, catalogTypeID, productId, User.UserId);
				return View("ViewDeleteProductCatalogSuccess", productCatalog);
			}
		}

		public ActionResult QuickInfoForAdmin(string id)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ProductRepository repo = new ProductRepository(connection, User);
				Product product = repo.GetProductDetail(id ?? String.Empty);

				if (product == null)
				{
					Response.StatusCode = 404;
					return View("NotFound");
				}

				ProductData viewdata = new ProductData();
				viewdata.ProductDetail = product;
				viewdata.ProductSku = repo.GetProductSKUs(product.CodeId);
				if (product.IsPG && !String.IsNullOrEmpty(product.DefaultSku))
				{
					if (viewdata.ProductSku.Count() < 1)
					{
						Response.StatusCode = 404;
						return View("NotFound");
					}
					Product SingleProductInGroup = viewdata.ProductSku.Where(w => w.Id == viewdata.ProductDetail.DefaultSku).Single();
					return RedirectToAction("QuickInfoAdmin", viewdata);
				}
				return View("QuickInfoAdmin", viewdata);
			}
		}

		private ActionResult SetRootAdmin(string guid, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				ModelViewListUserData data = new ModelViewListUserData();
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				string userId = userRepo.GetUserIdFromUserGuid(guid);

				data.UserData = userRepo.GetAllUser(User.CurrentCompanyId, true);

				string userAdmin = data.UserData.Where(m => m.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin).FirstOrDefault().UserId;
				if (costcenterRepo.IsAdminApproveInOrder(companyId, userAdmin))
				{
					TempData["ErrorSetRootAdmin"] = new ResourceString("ErrorMessage.SetRootAdmin");
				}
				else
				{
					userRepo.UpdateRootAdmin(data, companyId, userId);
					data.ProcessLog = SetRootAdminForAdmin(data, userId);
					userRepo.AddRootAdminProcessLog(data.ProcessLog, User.UserId);
				}
				return ViewAllUser(true);
			}
		}

		private ProcessLog SetRootAdminForAdmin(ModelViewListUserData data, string userId)
		{
			ProcessLog process = new ProcessLog();
			process.CompanyId = data.UserData.First().CurrentCompanyId;
			process.ProcessRemark = "Change RootAdmin";
			process.Remark = "เปลี่ยนจาก " + data.UserData.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin).FirstOrDefault().UserId + " เป็น " + userId;
			process.IPAddress = IPAddress;
			process.CreateBy = User.UserId;
			return process;
		}

		private ProcessLog SetUserOFMForAdmin(ModelViewListUserData data, string userId)
		{
			ProcessLog process = new ProcessLog();
			process.CompanyId = data.UserData.First().CurrentCompanyId;
			process.ProcessRemark = "Delete UserOFM";
			process.Remark = "ลบข้อมูลผู้ใช้งาน " + userId + " ออกจากระบบ Officemate";
			process.IPAddress = IPAddress;
			process.CreateBy = User.UserId;
			return process;
		}

	}
}
