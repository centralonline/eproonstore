﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using System.Drawing.Text;
using System.Drawing.Drawing2D;
using System.Text;

namespace Eprocurement2012.Controllers
{
	public class CaptchaController : Controller
	{
		//
		// GET: /Captcha/

		public ActionResult CaptchaImage(string prefix, bool noisy = true)
		{
			var rand = new Random((int)DateTime.Now.Ticks);

			//generate new question
			//int a = rand.Next(10, 99);
			//int b = rand.Next(0, 9);
			// var captcha = string.Format("{0} + {1} = ?", a, b);

			var captcha = string.Format("{0}", RandomString(5, false));

			//store answer
			//Session["Captcha" + prefix] = a + b;
			Session["Captcha" + prefix] = captcha;

			//image stream
			FileContentResult img = null;

			using (var mem = new MemoryStream())
			using (var bmp = new Bitmap(200, 50))
			using (var gfx = Graphics.FromImage((Image)bmp))
			{
				gfx.TextRenderingHint = TextRenderingHint.ClearTypeGridFit;
				gfx.SmoothingMode = SmoothingMode.AntiAlias;
				gfx.FillRectangle(Brushes.Ivory, new Rectangle(0, 0, bmp.Width, bmp.Height));

				//add noise
				if (noisy)
				{
					int i, r, x, y;
					var pen = new Pen(Color.Yellow);
					for (i = 1; i < 10; i++)
					{
						pen.Color = Color.FromArgb(
						(rand.Next(0, 255)),
						(rand.Next(0, 255)),
						(rand.Next(0, 255)));

						r = rand.Next(0, (130 / 3));
						x = rand.Next(0, 130);
						y = rand.Next(0, 30);

						int gx = x - r;
						int gy = y - r;
						gfx.DrawEllipse(pen, gx, gy, r, r);
					}
				}

				//add question
				gfx.DrawString(captcha, new Font("Tahoma", 20), Brushes.Gray, 4, 5);

				//render as Jpeg
				bmp.Save(mem, System.Drawing.Imaging.ImageFormat.Jpeg);
				img = this.File(mem.GetBuffer(), "image/Jpeg");
			}

			return img;
		}
		private string RandomString(int size, bool lowerCase)
		{
			StringBuilder builder = new StringBuilder();
			Random random = new Random();
			char ch;
			for (int i = 0; i < size; i++)
			{
				ch = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * random.NextDouble() + 65)));
				builder.Append(ch);
			}
			if (lowerCase)
				return builder.ToString().ToLower();
			return builder.ToString();
		}
	}
}
