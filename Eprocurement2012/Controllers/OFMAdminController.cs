﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Eprocurement2012.Models;
using Eprocurement2012.Controllers.Helpers;
using Eprocurement2012.Models.Repositories;
using System.Data.SqlClient;
using System.IO;
using System.Data;
using Excel;
using OfficeMate.Framework.Formatting;
using Eprocurement2012.Controllers.Mails;
using System.Transactions;


namespace Eprocurement2012.Controllers
{
	public class OFMAdminController : AuthenticateRoleController
	{

		[HttpGet]
		public ActionResult Index()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				IEnumerable<Company> allCompany = compRepo.GetAllCompany();
				return View(new MasterPageData<IEnumerable<Company>>(allCompany));
			}
		}

		[HttpGet]
		public ActionResult SearchCompany()
		{
			return View(new SearchCompany());
		}
		[HttpPost]
		public ActionResult SearchCompany(SearchCompany searchCompany, string fromDate, string toDate)
		{
			DateTime fromDateTime, toDateTime;
			if (!String.IsNullOrEmpty(fromDate) && DateTime.TryParseExact(fromDate, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateTime))
			{
				searchCompany.CreateDateFrom = fromDateTime;
			}
			if (!String.IsNullOrEmpty(toDate) && DateTime.TryParseExact(toDate, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDateTime))
			{
				searchCompany.CreateDateTo = toDateTime;
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				searchCompany.ShearchData = new CompanyRepository(connection).GetAllCompanyBySearch(searchCompany);
				return View(searchCompany);
			}
		}

		[HttpGet]
		public ActionResult SearchUser()
		{
			return View(new SearchUser());
		}

		[HttpPost]
		public ActionResult SearchUser(SearchUser searchUser)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (searchUser.CheckUserId || searchUser.CheckUserName || searchUser.CheckCompanyId || searchUser.CheckUserStatusName || searchUser.CheckUserRoleName || searchUser.CheckCreateBy)
				{
					searchUser.ShearchUserData = new UserRepository(connection).GetAllUserBySearch(searchUser);
					if (!searchUser.ShearchUserData.Any())
					{
						TempData["ErrorMessage"] = "ไม่พบข้อมูลที่ค้นหาค่ะ";
					}
				}
				else
				{
					TempData["ErrorMessage"] = "ไม่พบข้อมูลที่ค้นหาค่ะ";
				}
				return View(searchUser);
			}
		}

		[HttpGet]
		public ActionResult SearchProduct()
		{
			return View(new SearchProduct());
		}

		[HttpPost]
		public ActionResult SearchProduct(SearchProduct searchProduct, string fromDate, string toDate)
		{
			DateTime fromDateTime, toDateTime;
			if (!String.IsNullOrEmpty(fromDate) && DateTime.TryParseExact(fromDate, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateTime))
			{
				searchProduct.CreateDateFrom = fromDateTime;
			}
			if (!String.IsNullOrEmpty(toDate) && DateTime.TryParseExact(toDate, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDateTime))
			{
				searchProduct.CreateDateTo = toDateTime;
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (searchProduct.CheckCatalogTypeId || searchProduct.CheckCompanyId || searchProduct.CheckCreateProduct || searchProduct.CheckProductId || searchProduct.CheckCreateBy)
				{
					searchProduct.SearchProductData = new ProductCatalogRepository(connection).GetAllProductBySearch(searchProduct);
					if (!searchProduct.SearchProductData.Any())
					{
						TempData["ErrorMessage"] = "ไม่พบข้อมูลที่ค้นหาค่ะ";
					}
				}
				else
				{
					TempData["ErrorMessage"] = "ไม่พบข้อมูลที่ค้นหาค่ะ";
				}
				return View(searchProduct);
			}
		}

		[HttpGet]
		public ActionResult SearchRequesterLine()
		{
			return View(new SearchRequesterLine());
		}

		[HttpPost]
		public ActionResult SearchRequesterLine(SearchRequesterLine searchRequesterLine)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (searchRequesterLine.CheckCompanyId || searchRequesterLine.CheckCostcenterId || searchRequesterLine.CheckRequesterUserId || searchRequesterLine.CheckApproverUserId)
				{
					searchRequesterLine.SearchRequesterLineData = new UserRepository(connection).GetAllRequesterLineBySearch(searchRequesterLine);
					if (!searchRequesterLine.SearchRequesterLineData.Any())
					{
						TempData["ErrorMessage"] = "ไม่พบข้อมูลที่ค้นหาค่ะ";
					}
				}
				else
				{
					TempData["ErrorMessage"] = "ไม่พบข้อมูลที่ค้นหาค่ะ";
				}
				return View(searchRequesterLine);
			}

		}

		[HttpGet]
		public ActionResult CreateNewSite()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				return View(new NewSiteData());
			}
		}
		[HttpPost]
		public ActionResult CreateNewSite(string SearchCustID, NewSiteData newSiteData, ImageInputData action)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);

				IEnumerable<UserOFM> users = new List<UserOFM>();
				if (action.Name == "SearchCustID")
				{
					if (string.IsNullOrEmpty(SearchCustID))
					{
						TempData["ErrorMessage"] = "กรุณาระบุ CustID ด้วยค่ะ";
						newSiteData = new NewSiteData();
					}
					else if (SearchCustID.IndexOf('T') == 0 || SearchCustID.Count() > 6)
					{
						TempData["ErrorMessage"] = "รหัสลูกค้านี้เป็นรหัสชั่วคราว กรุณากรอกรหัสลูกค้าจริงด้วยค่ะ";
						newSiteData = new NewSiteData();
					}
					else
					{
						//users = userRepo.GetUserIdInOFMSystem(SearchCustID);
						newSiteData = userRepo.GetCustDataFromMasterByCustID(SearchCustID);
						if (newSiteData == null)
						{
							TempData["ErrorMessage"] = "รหัสลูกค้านี้ไม่มีข้อมูลในระบบค่ะ";
							newSiteData = new NewSiteData();
						}
						else if (compRepo.IsCustIdInEproSystem(SearchCustID))
						{
							TempData["ErrorMessage"] = "รหัสลูกค้านี้มีข้อมูลในระบบ e-Procurement แล้วค่ะ";
							newSiteData = new NewSiteData();
						}
						//else if (users.Any() && compRepo.IsOrderIdInOFMSystem(SearchCustID))
						//{
						//    TempData["ErrorMessage"] = "ไม่สามารถสร้างไซต์ได้ ให้ติดต่อเจ้าหน้าที่ IS Consult ค่ะ";
						//    newSiteData = new NewSiteData();
						//}
						//else if (users.Any() && !compRepo.IsOrderIdInOFMSystem(SearchCustID))
						//{
						//    TempData["ErrorMessage"] = "รหัสลูกค้านี้มีสิทธิการใช้งานที่เว็บออฟฟิศเมท";
						//}
					}
				}
				else if (action.Name == "SaveData")
				{
					if (ModelState.IsValid)
					{
						if (newSiteData.IsMember)//เป็นลูกค้าออนไลน์อยู่ก่อนแล้ว
						{
							if (!compRepo.IsCompanyIdInSystem(newSiteData.CompanyId))
							{
								//users = userRepo.GetUserIdInOFMSystem(newSiteData.CustId);
								//if (DeleteUserInfoForCreateNewSite(users))
								//{
								//    CreateNewSiteData(newSiteData);
								//    compRepo.InsertCustCompany(newSiteData.CustId, newSiteData.CompanyId, User.UserId, User.UserCompanyDefault.Yes);
								//    newSiteData.CreateBy = User.UserId;
								//    SendMail(new CreateNewSiteMail(ControllerContext, newSiteData), newSiteData.CompanyId, MailType.CreateNewSite, User.UserId);
								//    return RedirectToAction("ViewNewSiteDetail", new { companyId = newSiteData.CompanyId });
								//}
								//else
								//{
								//    TempData["ErrorMessage"] = "ไม่สามารถสร้างไซต์ได้ ให้ติดต่อเจ้าหน้าที่ออฟฟิศเมทค่ะ";
								//}

								CreateNewSiteData(newSiteData);
								compRepo.InsertCustCompany(newSiteData.CustId, newSiteData.CompanyId, User.UserId, User.UserCompanyDefault.Yes);
								newSiteData.CreateBy = User.UserId;
								SendMail(new CreateNewSiteMail(ControllerContext, newSiteData), newSiteData.CompanyId, MailType.CreateNewSite, User.UserId);
								return RedirectToAction("ViewNewSiteDetail", new { companyId = newSiteData.CompanyId });
							}
							else
							{
								TempData["ErrorMessage"] = "รหัสองค์กรนี้มีข้อมูลในระบบแล้วค่ะ";
							}
						}
						else //ไม่ใช่ลูกค้าออนไลน์ รอปรึษาพี่แจ็ก
						{
							TempData["ErrorMessage"] = "ข้อมูลลูกค้านี้ยังไม่เคยมีอยู่ในระบบ กรุณาเพิ่มข้อมมูลก่อนค่ะ";
						}
					}
				}
				return View(newSiteData);
			}
		}
		//private bool DeleteUserInfoForCreateNewSite(IEnumerable<UserOFM> users)
		//{
		//    UserRepository userRepo = new UserRepository(new SqlConnection(ConnectionString));
		//    ModelViewListUserData data = new ModelViewListUserData();
		//    try
		//    {
		//        using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
		//        {
		//            foreach (var item in users)
		//            {
		//                userRepo.DeleteUserInOFM(item.UserID);
		//                data.ProcessLog = SetDeleteUserOFMForCreateNewSiteAdmin(data, item.UserID);
		//                userRepo.AddRootAdminProcessLog(data.ProcessLog, User.UserId);
		//            }
		//            scope.Complete();
		//            return true;
		//        }
		//    }
		//    catch (Exception)
		//    {
		//        return false;
		//    }
		//}
		private void CreateNewSiteData(NewSiteData newSiteData)
		{
			if (newSiteData.OrderControlType == "ByOrder")
			{
				newSiteData.BudgetLevelType = Company.BudgetLevel.Not.ToString();
				newSiteData.BudgetPeriodType = Company.BudgetPeriod.Not.ToString();
			}

			CompanyRepository compRepo = new CompanyRepository(new SqlConnection(ConnectionString));
			compRepo.CreateNewCompany(newSiteData, User.UserId);

			if (newSiteData.OrderControlType == "ByBudgetAndOrder") // ถ้าเป็นกรณีคุม Budget
			{
				compRepo.CreateBudget(newSiteData.BudgetLevelType, newSiteData.BudgetPeriodType, newSiteData.CompanyId);
			}

			if (newSiteData.FileCompanyLogo != null)
			{
				System.Drawing.Image image = System.Drawing.Image.FromStream(newSiteData.FileCompanyLogo.InputStream);
				compRepo.UpdateCompanyLogo(ConvertImageToByteArray(image, System.Drawing.Imaging.ImageFormat.Jpeg), newSiteData.CompanyId, User.UserId);
			}
			new UserRepository(new SqlConnection(ConnectionString)).CreateUserForNewSite(newSiteData, User.UserId);

			//------------------------------------เพิ่มเบอร์โทรศัพท์------------------------------------------//
			OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repo = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(new SqlConnection(ConnectionString));
			OfficeMate.Framework.CustomerData.PhoneNumber phone;
			if (!string.IsNullOrEmpty(newSiteData.PhoneNumber))
			{
				phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
				if (newSiteData.PhoneId == 0) //new phone number
				{
					phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
					phone.CustomerId = newSiteData.CustId;
					phone.SequenceId = newSiteData.SequenceId;
					phone.PhoneNo = newSiteData.PhoneNumber;
					phone.Extension = !string.IsNullOrEmpty(newSiteData.ExtentionPhoneNumber) ? newSiteData.ExtentionPhoneNumber : "";
					repo.InsertPhoneNumber(phone, "EproV5");
					//repo.setDefaultPhoneNumber(phone, "EproV5");
				}
				else //edit phone number
				{
					phone = repo.SelectPhone(newSiteData.PhoneId);
					phone.PhoneNo = newSiteData.PhoneNumber;
					phone.Extension = !string.IsNullOrEmpty(newSiteData.ExtentionPhoneNumber) ? newSiteData.ExtentionPhoneNumber : "";
					repo.UpdatePhoneNumber(phone, "EproV5");
				}
			}
			if (!string.IsNullOrEmpty(newSiteData.MobileNumber))
			{
				phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
				if (newSiteData.MobileId == 0) //new phone number
				{
					phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.Mobile;
					phone.CustomerId = newSiteData.CustId;
					phone.SequenceId = newSiteData.SequenceId;
					phone.PhoneNo = newSiteData.MobileNumber;
					phone.Extension = "";
					repo.InsertPhoneNumber(phone, "EproV5");
					//repo.setDefaultPhoneNumber(phone, "EproV5");
				}
				else //edit phone number
				{
					phone = repo.SelectPhone(newSiteData.MobileId);
					phone.PhoneNo = newSiteData.MobileNumber;
					phone.Extension = "";
					repo.UpdatePhoneNumber(phone, "EproV5");
				}
			}
			if (!string.IsNullOrEmpty(newSiteData.FaxNumber))
			{
				phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
				if (newSiteData.FaxId == 0) //new phone number
				{
					phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.FaxOut;
					phone.CustomerId = newSiteData.CustId;
					phone.SequenceId = newSiteData.SequenceId;
					phone.PhoneNo = newSiteData.FaxNumber;
					phone.Extension = "";
					repo.InsertPhoneNumber(phone, "EproV5");
					//repo.setDefaultPhoneNumber(phone, "EproV5");
				}
				else //edit phone number
				{
					phone = repo.SelectPhone(newSiteData.FaxId);
					phone.PhoneNo = newSiteData.FaxNumber;
					phone.Extension = "";
					repo.UpdatePhoneNumber(phone, "EproV5");
				}
			}
		}

		public ActionResult ImportExcel(string companyId)
		{
			Company comp = new Company();
			comp.CompanyId = companyId;
			return View("ViewImportExcel", comp);
		}
		[HttpPost]
		public ActionResult ImportExcel(HttpPostedFileBase uploadFile, string companyId)
		{
			if (uploadFile != null)
			{
				string[] fileName = uploadFile.FileName.Split('.');

				try
				{
					//Reading from a OpenXml Excel file ('97-2003 format; *.xls)
					IExcelDataReader excelReader = ExcelReaderFactory.CreateBinaryReader(uploadFile.InputStream);

					//DataSet - The result of each spreadsheet will be created in the result.Tables
					DataSet result = excelReader.AsDataSet();

					//Data Reader methods
					foreach (DataTable table in result.Tables)
					{
						foreach (DataRow row in table.Rows)
						{
							TempData["error"] += "//" + row.ItemArray[0] + "|" + row.ItemArray[1] + "|" + row.ItemArray[2] + "//";
						}
					}
					excelReader.Close();
				}
				catch (Exception ex)
				{
					TempData["error"] = "ERROR: " + ex.Message.ToString();
				}
			}
			else
			{
				TempData["error"] = "You have not specified a file.";
			}
			Company comp = new Company();
			comp.CompanyId = companyId;
			return View("ViewImportExcel", comp);
		}
		[HttpGet]
		public ActionResult ViewNewSiteDetail(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				NewSiteData newSiteData = new NewSiteData();
				newSiteData = compRepo.GetNewSiteData(companyId);
				if (newSiteData != null)
				{
					UserRepository repo = new UserRepository(connection);
					Contact contact = repo.GetUserDataContact(newSiteData.UserID, companyId);
					newSiteData.PhoneNumber = contact.Phone != null ? contact.Phone.PhoneNo : "";
					newSiteData.ExtentionPhoneNumber = contact.Phone != null ? contact.Phone.Extension : "";
					newSiteData.MobileNumber = contact.Mobile != null ? contact.Mobile.PhoneNo : "";
					newSiteData.FaxNumber = contact.Fax != null ? contact.Fax.PhoneNo : "";

					//ดึงสิทธิการใช้งานอื่นๆ
					IEnumerable<UserPermission> permission = repo.GetUserPermission(newSiteData.UserID, companyId);
					newSiteData.IsApprover = permission.Any(o => o.RoleName == Models.User.UserRole.Approver);
					newSiteData.IsRequester = permission.Any(o => o.RoleName == Models.User.UserRole.Requester);
				}
				return View(newSiteData);
			}
		}

		public ActionResult CompanyHome(string companyId, ImageInputData action)
		{
			if (string.IsNullOrEmpty(action.Value) && string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company company = new Company();
				if (!string.IsNullOrEmpty(companyId))
				{
					company = compRepo.GetCompanyDetail(companyId);
				}
				else
				{
					company = compRepo.GetCompanyDetail(action.Value);
				}
				return View(company);
			}
		}

		public ActionResult CompanyInfo(string companyId, ImageInputData action, Company company)
		{
			if (string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }
			switch (action.Name)
			{
				case "CompanyInfo":
					return CompanyInformation(companyId);
				case "UpdateInfo":
					return UpdateCompanyInformation(company);
				case "CheckShipping":
					return CheckShippingFromCustID(companyId, action.Value);
				case "CompanyLogo":
					return CompanyLogo(companyId);
				case "UploadLogo":
					return UploadLogo(company.CompanyLogo, company.CompanyId);
				case "CompanySetting":
					return CompanySetting(companyId);
				case "UpdateSetting":
					return UpdateCompanySetting(company);
				case "CompanyReview":
					return CompanyReview(companyId);
				default:
					return CompanyGuide(companyId);
			}
		}
		private ActionResult CompanyGuide(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company company = new Company();
				company = compRepo.GetCompany(companyId);
				return View("CompanyInfo.Guide", company);
			}
		}
		private ActionResult CompanyInformation(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company company = new Company();
				company = compRepo.GetCompany(companyId);
				return View("CompanyInfo.Info", company);
			}
		}
		private ActionResult CheckShippingFromCustID(string companyId, string custId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company company = compRepo.GetCompany(companyId);
				company.CompanyInvoice = compRepo.GetCompanyInvoice(custId);
				company.ListShipAddress = compRepo.GetCompanyShipping(companyId).Where(s => s.CustId == custId);
				return View("CompanyInfo.Info", company);
			}
		}
		private ActionResult UpdateCompanyInformation(Company company)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				if (ModelState.IsValid)
				{
					compRepo.UpdateCompanyInfo(company, User.UserId);
					return CompanyInformation(company.CompanyId);
				}
				else
				{
					company.ListCompanyAddress = compRepo.GetAllCompanyAddress(company.CompanyId);
					return View("CompanyInfo.Info", company);
				}
			}
		}
		private ActionResult CompanyLogo(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company company = compRepo.GetCompanyDetail(companyId);
				return View("CompanyInfo.Logo", company);
			}
		}
		private ActionResult UploadLogo(HttpPostedFileBase file, string companyId)
		{
			if (file == null || !file.ContentType.StartsWith("image") || file.ContentLength > (512 * 1024))
			{
				TempData["errormessage"] = "กรุณาเลือกรูปภาพด้วยค่ะ";
			}
			else
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					CompanyRepository compRepo = new CompanyRepository(connection);
					System.Drawing.Image image = System.Drawing.Image.FromStream(file.InputStream);
					compRepo.UpdateCompanyLogo(ConvertImageToByteArray(image, System.Drawing.Imaging.ImageFormat.Jpeg), companyId, User.UserId);
				}
			}
			return CompanyLogo(companyId);
		}
		public ActionResult CompanyLogoImage(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ImageData companyLogo = new CompanyRepository(connection).GetCompanyLogo(companyId);
				if (companyLogo.Bytes.All(b => b == 0))
				{
					return new FilePathResult("/images/logocompany.jpg", "image/gif");
				}
				else
				{
					return new FileContentResult(companyLogo.Bytes, "image/png");
				}
			}
		}
		// เปลี่ยนไฟล์รูปเป็น ByteArray  เพื่อเก็บลง Database
		private byte[] ConvertImageToByteArray(System.Drawing.Image imageToConvert, System.Drawing.Imaging.ImageFormat formatOfImage)
		{
			byte[] Ret;
			try
			{
				using (MemoryStream ms = new MemoryStream())
				{
					imageToConvert.Save(ms, formatOfImage);
					Ret = ms.ToArray();
				}
			}
			catch (Exception) { throw; }
			return Ret;
		}
		private ActionResult CompanySetting(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company company = compRepo.GetCompanyDetail(companyId);
				return View("CompanyInfo.Setting", company);
			}
		}
		private ActionResult UpdateCompanySetting(Company company)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				compRepo.UpdateCompanySettingFormAdmin(company, User.UserId);
				TempData["CompanySettingComplete"] = new ResourceString("Admin.CompanySetting.EditComplete");
				return CompanySetting(company.CompanyId);
			}
		}
		private ActionResult CompanyReview(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company company = compRepo.GetCompanyDetail(companyId);
				company.CompanyLogoReview = compRepo.GetCompanyLogo(companyId);
				return View("CompanyInfo.Review", company);
			}
		}

		public ActionResult Department(string companyId, ImageInputData action, CompanyDepartment department)
		{
			if (string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }
			switch (action.Name)
			{
				case "CreateNewDepartment":
					return CreateDepartment(companyId);
				case "SaveNewDepartment":
					if (ModelState.IsValid)
					{
						return SaveNewDepartment(department);
					}
					else
					{
						return View("Department.Create", department);
					}
				case "ViewAllDepartment":
					return ViewAllDepartment(companyId);
				case "EditDepartment":
					return EditDepartment(companyId, action.Value);
				case "UpdateDepartment":
					if (ModelState.IsValid)
					{
						return UpdateDepartment(department);
					}
					else
					{
						return View("Department.Edit", department);
					}
				case "RemoveDepartment":
					return RemoveDepartment(companyId, action.Value);
				default:
					return DepartmentGuide(companyId);
			}
		}
		private ActionResult DepartmentGuide(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Company company = new CompanyRepository(connection).GetCompanyDetail(companyId);
				return View("Department.Guide", company);
			}
		}
		private ActionResult CreateDepartment(string companyId)
		{
			CompanyDepartment department = new CompanyDepartment();
			department.CompanyId = companyId;
			return View("Department.Create", department);
		}
		private ActionResult SaveNewDepartment(CompanyDepartment department)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartmentRepository deptRepo = new CompanyDepartmentRepository(connection);
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company comp = new CompanyRepository(connection).GetCompanyDetail(department.CompanyId);
				if (!deptRepo.IsDepartmentIdInSystem(department.CompanyId, department.DepartmentID))
				{
					department.DepartmentStatus = CompanyDepartment.DeptStatus.Active;
					deptRepo.CreateCompanyDepartment(department, User.UserId);
					if (comp.OrderControlType == Company.OrderControl.ByBudgetAndOrder && comp.BudgetLevelType == Company.BudgetLevel.Department) // ถ้าเป็นกรณีคุม Budget
					{
						compRepo.CreateBudget(comp.BudgetLevelType.ToString(), comp.BudgetPeriodType.ToString(), comp.CompanyId);
					}
					return ViewAllDepartment(department.CompanyId);
				}
				else
				{
					ModelState.AddModelError("DepartmentID", "Shared.DepartmentID.Error");
					return View("Department.Create", department);
				}
			}
		}

		private ActionResult ViewAllDepartment(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ViewAllDepartmentData deptData = new ViewAllDepartmentData();
				deptData.CompanyId = companyId;
				deptData.IsCompModelThreeLevel = new CompanyRepository(connection).GetCompanyDetail(companyId).IsCompModelThreeLevel;
				deptData.Departments = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(companyId);
				return View("Department.OverView", deptData);
			}
		}
		private ActionResult EditDepartment(string companyId, string departmentId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartmentRepository deptRepo = new CompanyDepartmentRepository(connection);
				CompanyDepartment department = new CompanyDepartment();
				department = deptRepo.GetCompanyDepartment(companyId, departmentId);
				return View("Department.Edit", department);
			}
		}
		private ActionResult UpdateDepartment(CompanyDepartment department)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartmentRepository deptRepo = new CompanyDepartmentRepository(connection);
				deptRepo.UpdateCompanyDepartment(department, User.UserId);
				return ViewAllDepartment(department.CompanyId);
			}
		}
		private ActionResult RemoveDepartment(string companyId, string departmentId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartmentRepository deptRepo = new CompanyDepartmentRepository(connection);
				deptRepo.RemoveCompanyDepartment(companyId, departmentId, User.UserId);
				return ViewAllDepartment(companyId);
			}
		}

		public ActionResult CostCenter(string companyId, ImageInputData action, CostCenter costCenter)
		{
			if (string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }
			switch (action.Name)
			{
				case "CreateNewCostCenter":
					return CreateCostCenter(companyId);
				case "ShowAddressForCreate":
					return ShowInvAndShipAddress(costCenter, true);
				case "SaveNewCostCenter":
					return SaveNewCostCenter(costCenter);
				case "ViewAllCostCenter":
					return ViewAllCostCenter(companyId);
				case "EditCostCenter":
					return EditCostCenter(companyId, action.Value);
				case "ShowAddressForEdit":
					return ShowInvAndShipAddress(costCenter, false);
				case "UpdateCostCenter":
					return UpdateCostCenter(costCenter);
				case "RemoveCostCenter":
					return RemoveCostCenter(companyId, action.Value);
				case "SetRequesterLine":
					return SetRequesterLine(companyId, action.Value);
				case "AddNewShipAddress":
					return ManageShipping(companyId, string.Empty, string.Empty);
				default:
					return CostCenterGuide(companyId);
			}
		}
		private ActionResult CostCenterGuide(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Company company = new CompanyRepository(connection).GetCompanyDetail(companyId);
				return View("CostCenter.Guide", company);
			}
		}
		private ActionResult CreateCostCenter(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartmentRepository deptRepo = new CompanyDepartmentRepository(connection);
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				CostCenter costCenter = new CostCenter();
				costCenter.ListCompanyDepartment = deptRepo.GetMyCompanyDepartment(companyId);
				costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(companyId);
				costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.ListInvoiceAddress.First().CustId);
				costCenter.CompanyId = companyId;
				Company comp = new CompanyRepository(connection).GetCompanyDetail(companyId);
				costCenter.IsCompModelThreeLevel = comp.IsCompModelThreeLevel;
				costCenter.IsAutoApprove = comp.IsAutoApprove;
				costCenter.IsByPassAdmin = comp.IsByPassAdmin;
				costCenter.IsByPassApprover = comp.IsByPassApprover;
				costCenter.IsDefaultDeliCharge = comp.IsDefaultDeliCharge;
				costCenter.OrderIDFormat = comp.OrderIDFormat.ToString();
				return View("CostCenter.Create", costCenter);
			}
		}
		private ActionResult ShowInvAndShipAddress(CostCenter costCenter, bool isCreate)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyDepartmentRepository deptRepo = new CompanyDepartmentRepository(connection);
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				costCenter.ListCompanyDepartment = deptRepo.GetMyCompanyDepartment(costCenter.CompanyId);
				costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(costCenter.CompanyId);
				costCenter.CompanyId = costCenter.CompanyId;
				Company comp = new CompanyRepository(connection).GetCompanyDetail(costCenter.CompanyId);
				costCenter.IsCompModelThreeLevel = comp.IsCompModelThreeLevel;
				costCenter.IsAutoApprove = comp.IsAutoApprove;
				costCenter.IsByPassAdmin = comp.IsByPassAdmin;
				costCenter.IsByPassApprover = comp.IsByPassApprover;
				costCenter.IsDefaultDeliCharge = comp.IsDefaultDeliCharge;
				costCenter.OrderIDFormat = comp.OrderIDFormat.ToString();
				if (!string.IsNullOrEmpty(costCenter.SelectInvoice))
				{
					costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.SelectInvoice);
					costCenter.CostCenterInvoice = new Invoice();
					costCenter.CostCenterCustID = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().CustId;
					costCenter.CostCenterInvoice.Address1 = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().Address1;
					costCenter.CostCenterInvoice.Address2 = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().Address2;
					costCenter.CostCenterInvoice.Address3 = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().Address3;
					costCenter.CostCenterInvoice.Address4 = costCenter.ListInvoiceAddress.Where(i => i.CustId == costCenter.SelectInvoice).First().Address4;
				}
				else
				{
					costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.ListInvoiceAddress.First().CustId);
					costCenter.CostCenterInvoice = null;
				}

				if (!string.IsNullOrEmpty(costCenter.SelectShipID) && costCenter.ListShipAddress.Any(s => s.ShipID.ToString() == costCenter.SelectShipID))
				{
					costCenter.CostCenterShipping = new Shipping();
					costCenter.CostCenterShipping.ShipID = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().ShipID;
					costCenter.CostCenterShipping.Address1 = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().Address1;
					costCenter.CostCenterShipping.Address2 = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().Address2;
					costCenter.CostCenterShipping.Address3 = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().Address3;
					costCenter.CostCenterShipping.Province = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().Province;
					costCenter.CostCenterShipping.ZipCode = costCenter.ListShipAddress.Where(s => s.ShipID.ToString() == costCenter.SelectShipID).First().ZipCode;
				}
				else
				{
					costCenter.CostCenterShipping = null;
				}
				if (isCreate)
				{
					return View("CostCenter.Create", costCenter);
				}
				return View("CostCenter.Edit", costCenter);
			}
		}
		private ActionResult SaveNewCostCenter(CostCenter costCenter)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				CompanyRepository compRepo = new CompanyRepository(connection);
				Company comp = new CompanyRepository(connection).GetCompanyDetail(costCenter.CompanyId);
				if (comp.IsCompModelThreeLevel && string.IsNullOrEmpty(costCenter.DepartmentID))
				{
					ModelState.AddModelError("DepartmentID", "Admin.EditDepartment.ErrorDepartmentIDEmptry");
				}
				else if (ModelState.IsValid)
				{
					if (String.IsNullOrEmpty(costCenter.SelectShipID))
					{
						TempData["ErrorSelectShipID"] = "กรุณาเลือกสถานที่จัดส่งด้วยค่ะ";
					}
					else if (!costRepo.IsCostcenterIdInSystem(costCenter.CompanyId, costCenter.CostCenterID))
					{
						if (!comp.IsCompModelThreeLevel) { costCenter.DepartmentID = ""; }
						if (!comp.IsByPassAdmin) { costCenter.UseByPassAdmin = false; }
						if (!comp.IsAutoApprove) { costCenter.UseAutoApprove = false; }
						if (!comp.IsByPassApprover) { costCenter.UseByPassApprover = false; }

						costRepo.CreateCostcenter(costCenter, User.UserId);
						if (comp.OrderControlType == Company.OrderControl.ByBudgetAndOrder && comp.BudgetLevelType == Company.BudgetLevel.Costcenter) // ถ้าเป็นกรณีคุม Budget
						{
							compRepo.CreateBudget(comp.BudgetLevelType.ToString(), comp.BudgetPeriodType.ToString(), comp.CompanyId);
						}
						return ViewAllCostCenter(costCenter.CompanyId);
					}
					else
					{
						ModelState.AddModelError("CostCenterID", "Shared.CostCenterID.Error");
					}
				}

				costCenter.ListCompanyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(costCenter.CompanyId);
				costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(costCenter.CompanyId);
				costCenter.CompanyId = costCenter.CompanyId;
				costCenter.IsCompModelThreeLevel = comp.IsCompModelThreeLevel;
				costCenter.IsAutoApprove = comp.IsAutoApprove;
				costCenter.IsByPassAdmin = comp.IsByPassAdmin;
				costCenter.IsByPassApprover = comp.IsByPassApprover;
				if (string.IsNullOrEmpty(costCenter.SelectInvoice))
				{
					costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.SelectInvoice);
				}
				else
				{
					costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.ListInvoiceAddress.First().CustId);
				}
				ShowInvAndShipAddress(costCenter, true);
				return View("CostCenter.Create", costCenter);
			}
		}

		private ActionResult ViewAllCostCenter(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ViewAllCostCenterData costcenter = new ViewAllCostCenterData();
				costcenter.CompanyId = companyId;
				costcenter.IsCompModelThreeLevel = new CompanyRepository(connection).GetCompanyDetail(companyId).IsCompModelThreeLevel;
				costcenter.CostCenters = new CostCenterRepository(connection).GetAllCostCenter(companyId);
				return View("CostCenter.OverView", costcenter);
			}
		}
		private ActionResult EditCostCenter(string companyId, string costCenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				CostCenter costCenter = costRepo.GetCostcenterDetailForAdmin(companyId, costCenterId);
				costCenter.SelectShipID = costCenter.CostCenterShipping.ShipID.ToString();
				costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(companyId);
				costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.CostCenterCustID);
				Company comp = new CompanyRepository(connection).GetCompanyDetail(companyId);
				costCenter.IsCompModelThreeLevel = comp.IsCompModelThreeLevel;
				costCenter.IsAutoApprove = comp.IsAutoApprove;
				costCenter.IsByPassAdmin = comp.IsByPassAdmin;
				costCenter.IsByPassApprover = comp.IsByPassApprover;
				costCenter.IsDefaultDeliCharge = comp.IsDefaultDeliCharge;
				return View("CostCenter.Edit", costCenter);
			}
		}
		private ActionResult UpdateCostCenter(CostCenter costCenter)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				Company comp = new CompanyRepository(connection).GetCompanyDetail(costCenter.CompanyId);
				if (ModelState.IsValid)
				{
					if (String.IsNullOrEmpty(costCenter.SelectShipID))
					{
						TempData["ErrorSelectShipID"] = "กรุณาเลือกสถานที่จัดส่งด้วยค่ะ";
					}
					else
					{
						if (!comp.IsCompModelThreeLevel) { costCenter.DepartmentID = ""; }
						if (!comp.IsByPassAdmin) { costCenter.UseByPassAdmin = false; }
						if (!comp.IsAutoApprove) { costCenter.UseAutoApprove = false; }
						if (!comp.IsByPassApprover) { costCenter.UseByPassApprover = false; }

						costRepo.UpdateCostcenter(costCenter, User.UserId);
						return ViewAllCostCenter(costCenter.CompanyId);
					}
				}
				costCenter.ListInvoiceAddress = costRepo.GetAllInvoidAddress(costCenter.CompanyId);
				if (string.IsNullOrEmpty(costCenter.SelectInvoice))
				{
					costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.SelectInvoice);
				}
				else
				{
					costCenter.ListShipAddress = costRepo.GetAllShipAddress(costCenter.ListInvoiceAddress.First().CustId);
				}
				costCenter.CompanyId = costCenter.CompanyId;
				costCenter.IsCompModelThreeLevel = comp.IsCompModelThreeLevel;
				costCenter.IsAutoApprove = comp.IsAutoApprove;
				costCenter.IsByPassAdmin = comp.IsByPassAdmin;
				costCenter.IsByPassApprover = comp.IsByPassApprover;
				ShowInvAndShipAddress(costCenter, false);
				return View("CostCenter.Edit", costCenter);
			}
		}
		private ActionResult RemoveCostCenter(string companyId, string costCenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				costRepo.RemoveCostCenter(companyId, costCenterId, User.UserId);
				return ViewAllCostCenter(companyId);
			}
		}

		public ActionResult UserInfo(string companyId, ImageInputData action, CreateNewUser newUserData, EditProfileUser editUser)
		{
			if (string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }
			switch (action.Name)
			{
				case "CreateNewUser":
					return CreateUser(companyId);
				case "SaveNewUser":
					return SaveNewUser(newUserData);
				case "ViewAllUser":
					return ViewAllUser(companyId);
				case "ViewAllAdmin":
					return ViewAllAdmin(companyId);
				case "EditUser":
					return EditUser(action.Value, companyId);
				case "UpdateUser":
					return UpdateUser(editUser, companyId);
				case "RemoveUser":
					return RemoveUser(action.Value, companyId);
				case "SetUserReq":
					return SetRequesterLineForUser(action.Value, companyId);
				case "SetRootAdmin":
					return SetRootAdmin(action.Value, companyId);
				default:
					return UserInfoGuide(companyId);
			}
		}
		private ActionResult UserInfoGuide(string companyId)
		{
			CreateNewUser user = new CreateNewUser();
			user.DefaultCompanyId = companyId;
			return View("UserInfo.Guide", user);
		}
		private ActionResult CreateUser(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CreateNewUser user = new CreateNewUser();
				Company comp = new CompanyRepository(connection).GetCompanyDetail(companyId);
				user.DefaultCompanyId = comp.CompanyId;
				user.DefaultCustId = comp.DefaultCustId;
				return View("UserInfo.Create", user);
			}
		}
		private ActionResult SaveNewUser(CreateNewUser createNewUser)
		{
			if (!createNewUser.IsInitialPassword) //Check ก่อนว่า ต้องการให้ระบบตั้ง Password ให้ไหม
			{
				if (string.IsNullOrEmpty(createNewUser.Password))
				{
					ModelState.AddModelError("Password", "CreateUser.PasswordEmptry");
					return View("UserInfo.Create", createNewUser);
				}
				if (!createNewUser.Password.Equals(createNewUser.RePassword))
				{
					ModelState.AddModelError("Password", "CreateUser.PasswordNotEqual");
					return View("UserInfo.Create", createNewUser);
				}
			}
			if (!createNewUser.IsApprover && !createNewUser.IsRequester && !createNewUser.IsAssistantAdmin && !createNewUser.IsObserve) //Check ไม่ได้เลือก Role มาเลย
			{
				ModelState.AddModelError("IsApprover", "CreateUser.RoleEmptry");
				return View("UserInfo.Create", createNewUser);
			}
			if (!ModelState.IsValid) { return View("UserInfo.Create", createNewUser); }

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);

				if (userRepo.IsUserInSystem(createNewUser.Email, createNewUser.DefaultCompanyId))
				{
					ModelState.AddModelError("Email", "CreateUser.EmailInSystem");
					return View("UserInfo.Create", createNewUser);
				}

				//แจ้งเตือน กรณีลูกค้าเคยเป็นลูกค้า OFM
				//if (userRepo.IsUserInOFMSystem(createNewUser.Email) && !createNewUser.IsUserOFM)
				//{
				//    TempData["ErrorOFMSystem"] = new ResourceString("ErrorMessage.OFMSystem");
				//    return View("UserInfo.Create", createNewUser);
				//}
				//else
				//{
					//แก้ไขปัญหากรณี 1 User สามารถใช้งานได้แค่ 1 Web เท่านั้น
					//if (userRepo.IsUserInOFMSystem(createNewUser.Email))
					//{
					//    ModelViewListUserData data = new ModelViewListUserData();
					//    data.UserData = userRepo.GetAllUser(createNewUser.DefaultCompanyId, true);
					//    userRepo.DeleteUserInOFM(createNewUser.Email);
					//    data.ProcessLog = SetDeleteUserOFMForCreateNewSiteAdmin(data, createNewUser.Email);
					//    userRepo.AddRootAdminProcessLog(data.ProcessLog, User.UserId);
					//}

				OfficeMate.Framework.CustomerData.Contact contact = new OfficeMate.Framework.CustomerData.Contact();
				OfficeMate.Framework.CustomerData.PhoneNumber phone;
				List<OfficeMate.Framework.CustomerData.PhoneNumber> itemPhone = new List<OfficeMate.Framework.CustomerData.PhoneNumber>();

					//ดึงข้อมูล CustId ที่ผูกอยู่กับองค์กรทั้งหมด
					IEnumerable<string> itemCustId = new CompanyRepository(connection).GetAllCompanyAddress(createNewUser.DefaultCompanyId).Select(c => c.CustId);

					if (!userRepo.UserExistMasterUser(createNewUser.Email)) //สร้าง users,password ใน master
					{
						if (createNewUser.IsInitialPassword)
						{
							createNewUser.Password = "officemate";
							createNewUser.RePassword = createNewUser.Password;
						}
						userRepo.InsertUserMaster(createNewUser.Email, createNewUser.Password);
					}

					foreach (var custId in itemCustId)
					{
						//create contact
						contact.Email = createNewUser.Email;
						contact.CustomerId = custId;
						if (createNewUser.IsThaiLanguage)
						{
							contact.Name = createNewUser.ThaiName;
						}
						else
						{
							contact.Name = createNewUser.EngName;
						}
						contact.FaxNumber = createNewUser.Fax;
						contact.MobileNumber = createNewUser.Mobile;

						phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
						phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
						phone.PhoneNo = createNewUser.Phone;
						phone.Extension = !string.IsNullOrEmpty(createNewUser.PhoneExt) ? createNewUser.PhoneExt : "";
						phone.IsDefault = true;
						itemPhone.Add(phone);

						if (!string.IsNullOrEmpty(createNewUser.Mobile))
						{
							phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
							phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.Mobile;
							phone.PhoneNo = createNewUser.Mobile;
							phone.IsDefault = true;
							itemPhone.Add(phone);
						}

						if (!string.IsNullOrEmpty(createNewUser.Fax))
						{
							phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
							phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.FaxOut;
							phone.PhoneNo = createNewUser.Fax;
							phone.IsDefault = true;
							itemPhone.Add(phone);
						}

						contact.Phones = itemPhone;

						new OfficeMate.Framework.CustomerData.Repositories.ContactRepository(connection).InsertContact(contact, "EproV5");//Insert Contact

					//}

					userRepo.CreateNewUser(createNewUser, User.UserId); //สร้างข้อมูลฝั่ง epro

					if (!new CompanyRepository(connection).GetCompanyDetail(createNewUser.DefaultCompanyId).UseOfmCatalog)//หากคุมสินค้าด้วย (มีสินค้าเป็นของตัวเอง) ต้องทำการ GentDeptStructure
					{
						new GenerateDeptStructureControl(connection).GentDeptStructure(createNewUser.DefaultCompanyId);
					}

					string verifyKey;
					if (userRepo.TryVerifyUser(createNewUser.Email, out verifyKey))
					{
						if (createNewUser.SetVerifyNow)
						{
							ForgotPasswordData forgot = userRepo.GetForgotPasswordData(createNewUser.Email);//ใช้ Model เหมือนกัน
							SendMail(new Eprocurement2012.Controllers.Mails.VerifyUserMail(ControllerContext, forgot), "", MailType.VerifyUser, User.UserId);
						}
					}
				}
			}
			return ViewAllUser(createNewUser.DefaultCompanyId);
		}
		private ActionResult ViewAllUser(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				ModelViewListUserData userData = new ModelViewListUserData();
				userData.CompanyId = companyId;
				userData.UserData = userRepo.GetAllUser(companyId, false);
				return View("UserInfo.AllUser", userData);
			}
		}
		private ActionResult ViewAllAdmin(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				ModelViewListUserData userData = new ModelViewListUserData();
				userData.CompanyId = companyId;
				userData.UserData = userRepo.GetAllUser(companyId, true);
				return View("UserInfo.AllAdmin", userData);
			}
		}
		private ActionResult EditUser(string userGuid, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				EditProfileUser user = new EditProfileUser();
				user = userRepo.GetUserDataForEdit(userGuid, companyId);
				if (user == null) { return ViewAllUser(companyId); }
				return View("UserInfo.Edit", user);
			}
		}
		private ActionResult UpdateUser(EditProfileUser userData, string companyId)
		{
			userData.CurrentCompanyId = companyId;
			if (ModelState.IsValid)
			{
				if (!userData.IsRequester && !userData.IsApprover && !userData.IsAssistantAdmin && !userData.IsObserve)
				{
					ModelState.AddModelError("IsApprover", "CreateUser.RoleEmptry");
				}
				else
				{
					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						UserRepository userRepo = new UserRepository(connection);
						if (userData.Status == Eprocurement2012.Models.User.UserStatus.Cancel && userRepo.GetUserReqOrAppPermission(userData.Email, companyId).Any())
						{
							TempData["EditUserCancel"] = new ResourceString("EditUser.ErrorEditUserCancel");
						}
						else
						{
							OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repo = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(connection);
							userRepo.UpdateUserLanguage(userData.Email, (User.Language)Enum.Parse(typeof(User.Language), userData.DefaultLang, true));
							if (userData.Status == Eprocurement2012.Models.User.UserStatus.Cancel)
							{
								userRepo.UpdateUserStatus(userData.UserGuid, userData.Status, User.UserId, companyId);
								//if (userRepo.IsUserMultiCompany(userData.Email))
								//{
								userRepo.UpdateIsDefaultUserStatus(userData.UserGuid, userData.Status, User.UserId, companyId);
								//}
							}
							else
							{
								userRepo.UpdateUserStatus(userData.UserGuid, userData.Status, User.UserId, companyId);
								userRepo.UpdateIsDefaultUserStatus(userData.UserGuid, userData.Status, User.UserId, companyId);
							}
							OfficeMate.Framework.CustomerData.PhoneNumber phone;

							//ดึงข้อมูล CustId ที่ผูกอยู่กับองค์กรทั้งหมด
							IEnumerable<string> itemCustId = new CompanyRepository(connection).GetAllCompanyAddress(companyId).Select(c => c.CustId);
							//ดึงข้อมูล Userid นี้ที่ผูกอยู่กับองค์กรทั้งหมด
							IEnumerable<CreateNewUser> ListUser = new UserRepository(connection).GetUserInfoAndPhoneInCompany(companyId, userData.Email);

							foreach (var custId in itemCustId)
							{
								if (ListUser.Any(id => id.DefaultCustId == custId)) //check ว่า custid ที่รับมามีอยู่ในข้อมูลของ user เพื่อใช้ในการ edit
								{
									int SequenceId = ListUser.FirstOrDefault(id => id.DefaultCustId == custId).ContactId;
									int PhoneId = ListUser.FirstOrDefault(id => id.DefaultCustId == custId).PhoneId;
									int MobileId = ListUser.FirstOrDefault(id => id.DefaultCustId == custId).MobileId;
									int FaxId = ListUser.FirstOrDefault(id => id.DefaultCustId == custId).FaxId;

									/*------------------------------ User Name -------------------------*/
									userData.SequenceId = SequenceId;
									userRepo.UpdateUserName(userData, User.UserId);

									/*------------------------------ phone number -------------------------*/
									phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
									if (ListUser.Any(id => id.DefaultCustId == custId && id.PhoneId == 0))
									{
										phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
										phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
										phone.CustomerId = custId;
										phone.SequenceId = SequenceId;
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Phone) ? userData.Phone : "";
										phone.Extension = !string.IsNullOrEmpty(userData.PhoneExt) ? userData.PhoneExt : "";
										repo.InsertPhoneNumber(phone, "EproV5");
									}
									else //edit phone number
									{
										phone = repo.SelectPhone(PhoneId);
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Phone) ? userData.Phone : "";
										phone.Extension = !string.IsNullOrEmpty(userData.PhoneExt) ? userData.PhoneExt : "";
										repo.UpdatePhoneNumber(phone, "EproV5");
									}

									/*-------------------------- mobile number -----------------------------*/
									phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
									if (ListUser.Any(id => id.DefaultCustId == custId && id.MobileId == 0))
									{
										phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
										phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.Mobile;
										phone.CustomerId = custId;
										phone.SequenceId = SequenceId;
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Mobile) ? userData.Mobile : "";
										phone.Extension = "";
										repo.InsertPhoneNumber(phone, "EproV5");

									}
									else //edit phone number
									{
										phone = repo.SelectPhone(MobileId);
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Mobile) ? userData.Mobile : "";
										phone.Extension = "";
										repo.UpdatePhoneNumber(phone, "EproV5");
									}

									/*------------------------ fax number -------------------------------*/
									phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
									if (ListUser.Any(id => id.DefaultCustId == custId && id.FaxId == 0))
									{
										phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
										phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.FaxOut;
										phone.CustomerId = custId;
										phone.SequenceId = SequenceId;
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Fax) ? userData.Fax : "";
										phone.Extension = "";
										repo.InsertPhoneNumber(phone, "EproV5");
									}
									else //edit phone number
									{
										phone = repo.SelectPhone(FaxId);
										phone.PhoneNo = !string.IsNullOrEmpty(userData.Fax) ? userData.Fax : "";
										phone.Extension = "";
										repo.UpdatePhoneNumber(phone, "EproV5");
									}
									/*--------------------------end-----------------------------*/
								}
							}
							userRepo.EditRoleUserFromAdmin(userData, User.UserId);
							TempData["EditUserComplete"] = new ResourceString("Admin.CompanySetting.EditComplete");

						}
						return EditUser(userData.UserGuid, companyId);
					}
				}
			}
			return View("UserInfo.Edit", userData);
		}
		private ActionResult RemoveUser(string userGuid, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				userRepo.UpdateUserStatus(userGuid, Models.User.UserStatus.Cancel, User.UserId, companyId);
				return ViewAllUser(companyId);
			}
		}
		public ActionResult SetVerifyUser(string userGuid, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				string verifyKey, userId;
				userId = userRepo.GetUserIdFromUserGuid(userGuid);
				if (userRepo.TryVerifyUser(userId, out verifyKey))
				{
					ForgotPasswordData forgot = userRepo.GetForgotPasswordData(userId);//ใช้ Model เหมือนกัน
					SendMail(new Eprocurement2012.Controllers.Mails.VerifyUserMail(ControllerContext, forgot), "", MailType.VerifyUser, User.UserId);
				}
			}
			return ViewAllUser(companyId);
		}
		private SetRequesterLine GetUserRequesterLineDetail(string companyId, string userGuid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				SetRequesterLine setReq = new SetRequesterLine();
				setReq.CompanyId = companyId;
				setReq.UserReqDetail = userRepo.GetUser(userGuid, User.UserStatus.Active);
				setReq.ListCostCenter = new CostCenterRepository(connection).GetAllCostCenter(companyId).Where(c => c.CostStatus == Models.CostCenter.CostCenterStatus.Active);
				setReq.CostCurrentPermission = new CostCenterRepository(connection).GetMyCostCenter(setReq.UserReqDetail.UserId, companyId, User.UserType.Requester);
				setReq.UserApprover = userRepo.GetAllUserReqOrApp(companyId).Where(u => u.UserRoleName == User.UserRole.Approver);
				setReq.InformationRequesterLine = userRepo.GetDataRequesterlineForUserSide(companyId, setReq.UserReqDetail.UserId);
				return setReq;
			}
		}
		private ActionResult SetRequesterLineForUser(string userGuid, string companyId)
		{
			SetRequesterLine setReq = GetUserRequesterLineDetail(companyId, userGuid);
			setReq.SelectCostcenter = new string[] { };
			setReq.SelectApprover = new string[3];
			setReq.SelectParkday = new int[3];
			setReq.SelectBudget = new decimal[3];
			return View("UserInfo.SetRequesterLine", setReq);
		}
		public ActionResult RequesterLineForUser(string companyId, ImageInputData action, string[] costcenterId, string[] userApprover, int[] parkday, decimal[] approverBudget, string userGuid, string userRequester, string[] costcenterName)
		{
			if (string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }
			switch (action.Name)
			{
				case "CancelSet":
					return ViewAllUser(companyId);
				case "SetUserReq":
					return SetRequesterLineForUser(companyId, costcenterId, userApprover, parkday, approverBudget, userGuid, costcenterName);
				case "ResetUserReq":
					return ResetRequesterLineForUser(companyId, action.Value, userGuid);
				case "SaveRequesterLine":
					return SetRequesterLineByUser(companyId, costcenterId, userApprover, parkday, approverBudget, userGuid, userRequester);
				case "DeleteRequesterLine":
					return DeleteRequesterLineByUser(companyId, costcenterId, userGuid, userRequester);
				default:
					return SetRequesterLineForUser(userGuid, companyId);
			}
		}
		private ActionResult SetRequesterLineForUser(string companyId, string[] costcenterId, string[] userApprover, int[] parkday, decimal[] approverBudget, string userGuid, string[] costcenterName)
		{
			SetRequesterLine reqLineDetail = GetUserRequesterLineDetail(companyId, userGuid);
			reqLineDetail.SelectCostcenter = costcenterId;
			reqLineDetail.SelectApprover = userApprover;
			reqLineDetail.SelectParkday = parkday;
			reqLineDetail.SelectBudget = approverBudget;

			if (reqLineDetail.SelectCostcenter == null)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.SelectCostcenter");
				return View("UserInfo.SetRequesterLine", reqLineDetail);
			}
			else
			{
				int count = costcenterId.Count();
				List<string[,]> costcenters = new List<string[,]>();
				for (int i = 0; i < count; i++)
				{
					costcenters.Add(new string[,] { { costcenterId[i], costcenterName[i] } });
				}
				TempData["costcenterId"] = costcenters;//ส่ง UserId กลับไป Display ที่หน้า View กรณีที่กรอกข้อมุลไม่ครบ
			}

			if (reqLineDetail.SelectApprover.All(a => a == ""))
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
				return View("UserInfo.SetRequesterLine", reqLineDetail);
			}

			bool IsPassCheckBudget = true;
			decimal budget = 0;
			int level = 0;
			foreach (var item in reqLineDetail.SelectBudget)
			{
				if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[level]) && item == 0) { break; }
				if (budget >= item)
				{
					IsPassCheckBudget = false;
					break;
				}
				budget = item;
				level++;
			}

			if (!IsPassCheckBudget)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorBudget");
				return View("UserInfo.SetRequesterLine", reqLineDetail);
			}

			bool IsPassCheckApp = true;
			for (int i = 0; i < reqLineDetail.SelectApprover.Count(); i++)
			{
				if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[i])) { break; }
				for (int j = i + 1; j < reqLineDetail.SelectApprover.Count(); j++)
				{
					if (reqLineDetail.SelectApprover[i] == reqLineDetail.SelectApprover[j])
					{
						IsPassCheckApp = false;
						break;
					}
				}
			}

			if (!IsPassCheckApp)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
				return View("UserInfo.SetRequesterLine", reqLineDetail);
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				if (!string.IsNullOrEmpty(reqLineDetail.SelectApprover[0]))
				{
					foreach (var item in reqLineDetail.SelectCostcenter)
					{
						int i = 0;
						foreach (var itemApp in reqLineDetail.SelectApprover)
						{
							SetRequesterLine reqLineForSave = null;
							if (!string.IsNullOrEmpty(itemApp))
							{
								reqLineForSave = new SetRequesterLine();
								reqLineForSave.CompanyId = companyId;
								reqLineForSave.CostcenterId = item;
								reqLineForSave.RequesterUserId = reqLineDetail.UserReqDetail.UserId;
								reqLineForSave.ApproverUserId = itemApp;
								reqLineForSave.AppCreditlimit = reqLineDetail.SelectBudget[i];
								reqLineForSave.Parkday = Convert.ToInt32(reqLineDetail.SelectParkday[i]);
								reqLineForSave.CreateBy = User.UserId;
								reqLineForSave.UpdateBy = User.UserId;
								reqLineForSave.AppLevel = i + 1;
								userRepo.SetUserReqLineAndApp(reqLineForSave);
								i += 1;
							}
							else
							{
								break;
							}
						}
					}
				}
				TempData["Error"] = new ResourceString("Admin.CompanySetting.EditComplete");
				TempData["costcenterId"] = null;
				return SetRequesterLineForUser(userGuid, companyId);
			}
		}
		private ActionResult ResetRequesterLineForUser(string companyId, string costcenterId, string userGuid)
		{
			return SetRequesterLineByUser(companyId, costcenterId, userGuid);
		}
		private ActionResult SetRequesterLineByUser(string companyId, string costcenterId, string userGuid)
		{
			SetRequesterLine setReq = null;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				string requesterId = userRepo.GetUserIdFromUserGuid(userGuid);
				setReq = SetDataRequesterLine(companyId, costcenterId, requesterId);

				List<string> ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == requesterId).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
				TempData["ErrorResetReqLine"] = "";
				foreach (var approverId in ItemUser)
				{
					if (costcenterRepo.IsReqAndAppInOrder(companyId, costcenterId, requesterId, approverId))
					{
						TempData["ErrorResetReqLine"] = new ResourceString("ไม่สามารถตั้งค่าการสั่งซื้อได้ เนื่องจากยังมีใบสั่งซื้อที่ยังรอการอนุมัติอยู่ค่ะ");
					}
				}
			}
			return View("SetRequesterLineByUser", setReq);
		}
		private ActionResult DeleteRequesterLineByUser(string companyId, string[] costcenterId, string userGuid, string userRequester)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				SetRequesterLine setReq = SetDataRequesterLine(companyId, costcenterId[0], userRequester);
				UserRepository userRepo = new UserRepository(connection);
				userRepo.ClearReqLineAndAppByUser(companyId, costcenterId[0], userRequester);
				TempData["Error"] = new ResourceString("ลบข้อมูลเรียบร้อยแล้วค่ะ");
				return SetRequesterLineForUser(userGuid, companyId);
			}
		}

		private ActionResult SetRequesterLineByUser(string companyId, string[] costcenterId, string[] userApprover, int[] parkday, decimal[] approverBudget, string userGuid, string userRequester)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				SetRequesterLine setReq = SetDataRequesterLine(companyId, costcenterId[0], userRequester);
				SetRequesterLine reqLineDetail = GetUserRequesterLineDetail(companyId, userGuid);
				reqLineDetail.SelectCostcenter = costcenterId;
				reqLineDetail.SelectApprover = userApprover;
				reqLineDetail.SelectParkday = parkday;
				reqLineDetail.SelectBudget = approverBudget;

				//เช็คว่ามีการผูกสิทธิ์ กับ Order ที่ยังดำเนินการไม่เสร็จหรือไม่
				List<string> ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
				int count = ItemUser.Count();
				for (int i = 0; i < count; i++)
				{
					if (ItemUser[i] != userApprover[i])
					{
						if (new CostCenterRepository(connection).IsReqAndAppInOrder(companyId, costcenterId[0], userRequester, ItemUser[i]))
						{
							TempData["ErrorResetReqLine"] = new ResourceString("ไม่สามารถตั้งค่าการสั่งซื้อได้ เนื่องจากยังมีใบสั่งซื้อที่ยังรอการอนุมัติอยู่ค่ะ");
							return View("SetRequesterLineByUser", setReq);
						}
					}
				}

				if (reqLineDetail.SelectCostcenter == null)
				{
					TempData["Error"] = new ResourceString("SetRequesterLine.SelectCostcenter");
					return View("UserInfo.SetRequesterLine", reqLineDetail);
				}

				if (reqLineDetail.SelectApprover.All(a => a == ""))
				{
					TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
					return View("UserInfo.SetRequesterLine", reqLineDetail);
				}

				bool IsPassCheckBudget = true;
				decimal budget = 0;
				int level = 0;
				foreach (var item in reqLineDetail.SelectBudget)
				{
					if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[level]) && item == 0) { break; }
					if (budget >= item)
					{
						IsPassCheckBudget = false;
						break;
					}
					budget = item;
					level++;
				}

				if (!IsPassCheckBudget)
				{
					TempData["Error"] = new ResourceString("SetRequesterLine.ErrorBudget");
					return View("UserInfo.SetRequesterLine", reqLineDetail);
				}

				bool IsPassCheckApp = true;
				for (int i = 0; i < reqLineDetail.SelectApprover.Count(); i++)
				{
					if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[i])) { break; }
					for (int j = i + 1; j < reqLineDetail.SelectApprover.Count(); j++)
					{
						if (reqLineDetail.SelectApprover[i] == reqLineDetail.SelectApprover[j])
						{
							IsPassCheckApp = false;
							break;
						}
					}
				}

				if (!IsPassCheckApp)
				{
					TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
					return View("SetRequesterLineByUser", setReq);
				}

				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				userRepo.ClearReqLineAndAppByUser(companyId, costcenterId[0], userRequester);
				if (!string.IsNullOrEmpty(reqLineDetail.SelectApprover[0]))
				{
					foreach (var item in reqLineDetail.SelectCostcenter)
					{
						int i = 0;
						foreach (var itemApp in reqLineDetail.SelectApprover)
						{
							SetRequesterLine reqLineForSave = null;
							if (!string.IsNullOrEmpty(itemApp))
							{
								reqLineForSave = new SetRequesterLine();
								reqLineForSave.CompanyId = companyId;
								reqLineForSave.CostcenterId = item;
								reqLineForSave.RequesterUserId = reqLineDetail.UserReqDetail.UserId;
								reqLineForSave.ApproverUserId = itemApp;
								reqLineForSave.AppCreditlimit = reqLineDetail.SelectBudget[i];
								reqLineForSave.Parkday = Convert.ToInt32(reqLineDetail.SelectParkday[i]);
								reqLineForSave.CreateBy = User.UserId;
								reqLineForSave.UpdateBy = User.UserId;
								reqLineForSave.AppLevel = i + 1;
								userRepo.SetUserReqLineAndApp(reqLineForSave);
								i += 1;
							}
							else
							{
								break;
							}
						}
					}
				}
				setReq = SetDataRequesterLine(companyId, costcenterId[0], userRequester);
				ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
				TempData["ErrorResetReqLine"] = "";
				foreach (var approverId in ItemUser)
				{
					if (costcenterRepo.IsReqAndAppInOrder(User.CurrentCompanyId, costcenterId[0], userRequester, approverId))
					{
						TempData["ErrorResetReqLine"] = new ResourceString("ไม่สามารถตั้งค่าการสั่งซื้อได้ เนื่องจากยังมีใบสั่งซื้อที่ยังรอการอนุมัติอยู่ค่ะ");
					}
				}

				TempData["Success"] = new ResourceString("SetRequesterLine.Complete");
				return View("SetRequesterLineByUser", setReq);
			}
		}
		public ActionResult RequesterLine(string companyId, ImageInputData action, string costcenterId, string[] userRequester, string[] userApprover, int[] parkday, decimal[] approverBudget, string[] userName)
		{
			if (string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }
			switch (action.Name)
			{
				case "SelectCostcenter":
					return SelectCostcenter(companyId);
				case "EditReqLine":
					return SetRequesterLine(companyId, action.Value);
				case "SaveReqLine":
					return SaveRequesterLine(companyId, action.Value, userRequester, userApprover, parkday, approverBudget, userName);
				case "ResetReqLine":
					return ResetRequesterLine(companyId, costcenterId, action.Value);
				case "SaveRequesterLine":
					return SetRequesterLineByOwner(companyId, costcenterId, userRequester[0], userApprover, parkday, approverBudget);
				case "SetRequesterLineBack":
					return SetRequesterLine(companyId, costcenterId);
				case "DeleteRequesterLine":
					return DeleteRequesterLine(companyId, costcenterId, userRequester[0]);
				default:
					return RequesterLineGuide(companyId);
			}
		}
		private ActionResult RequesterLineGuide(string companyId)
		{
			SetRequesterLine data = new SetRequesterLine();
			data.CompanyId = companyId;
			return View("RequesterLine.Guide", data);
		}
		private ActionResult SelectCostcenter(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				IEnumerable<CostCenter> costcenter = costRepo.GetAllCostCenter(companyId);
				return View("RequesterLine.View", new MasterPageData<IEnumerable<CostCenter>>(costcenter));
			}
		}
		private SetRequesterLine GetRequesterLineDetail(string companyId, string costcenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				SetRequesterLine setReq = new SetRequesterLine();
				setReq.CompanyId = companyId;
				setReq.CostCenter = costRepo.GetCostcenterDetailForAdmin(companyId, costcenterId);
				setReq.UserCurrentPermission = userRepo.GetUserReqOrAppPermission(companyId);
				IEnumerable<User> listUser = userRepo.GetAllUserReqOrApp(companyId);
				setReq.UserRequester = listUser.Where(u => u.UserRoleName == User.UserRole.Requester);
				setReq.UserApprover = listUser.Where(u => u.UserRoleName == User.UserRole.Approver);
				setReq.InformationRequesterLine = userRepo.GetDataRequesterline(companyId, costcenterId);
				return setReq;
			}
		}
		private ActionResult SetRequesterLine(string companyId, string costcenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				SetRequesterLine setReq = GetRequesterLineDetail(companyId, costcenterId);
				setReq.SelectRequester = new string[] { };
				setReq.SelectApprover = new string[3];
				setReq.SelectParkday = new int[3];
				setReq.SelectBudget = new decimal[3];
				return View("RequesterLine.Set", setReq);
			}
		}
		private ActionResult SaveRequesterLine(string companyId, string costcenterId, string[] userRequester, string[] userApprover, int[] parkday, decimal[] approverBudget, string[] userName)
		{
			SetRequesterLine setReq = GetRequesterLineDetail(companyId, costcenterId);
			setReq.SelectRequester = userRequester;
			setReq.SelectApprover = userApprover;
			setReq.SelectParkday = parkday;
			setReq.SelectBudget = approverBudget;

			if (setReq.SelectRequester == null)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.SelectRequester");
				return View("RequesterLine.Set", setReq);
			}
			else
			{
				int count = userRequester.Count();
				List<string[,]> users = new List<string[,]>();
				for (int i = 0; i < count; i++)
				{
					users.Add(new string[,] { { userName[i].ToString(), userRequester[i].ToString() } });

				}
				TempData["userRequester"] = users;//ส่ง userName และ UserId กลับไป Display ที่หน้า View กรณีที่กรอกข้อมุลไม่ครบ
			}

			if (setReq.SelectApprover.All(a => a == ""))
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
				return View("RequesterLine.Set", setReq);
			}

			bool IsPassCheckBudget = true;
			decimal budget = 0;
			int level = 0;
			foreach (var item in setReq.SelectBudget)
			{
				if (string.IsNullOrEmpty(setReq.SelectApprover[level]) && item == 0) { break; }
				if (budget >= item)
				{
					IsPassCheckBudget = false;
					break;
				}
				budget = item;
				level++;
			}

			if (!IsPassCheckBudget)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorBudget");
				return View("RequesterLine.Set", setReq);
			}

			bool IsPassCheckApp = true;
			for (int i = 0; i < setReq.SelectApprover.Count(); i++)
			{
				if (string.IsNullOrEmpty(setReq.SelectApprover[i])) { break; }
				for (int j = i + 1; j < setReq.SelectApprover.Count(); j++)
				{
					if (setReq.SelectApprover[i] == setReq.SelectApprover[j])
					{
						IsPassCheckApp = false;
						break;
					}
				}
			}

			if (!IsPassCheckApp)
			{
				TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
				return View("RequesterLine.Set", setReq);
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				if (!string.IsNullOrEmpty(setReq.SelectApprover[0]))
				{
					foreach (var item in setReq.SelectRequester)
					{
						int i = 0;
						foreach (var itemApp in setReq.SelectApprover)
						{
							SetRequesterLine reqLineForSave = null;
							if (!string.IsNullOrEmpty(itemApp))
							{
								reqLineForSave = new SetRequesterLine();
								reqLineForSave.CompanyId = companyId;
								reqLineForSave.CostcenterId = costcenterId;
								reqLineForSave.RequesterUserId = item;
								reqLineForSave.ApproverUserId = itemApp;
								reqLineForSave.AppCreditlimit = setReq.SelectBudget[i];
								reqLineForSave.Parkday = Convert.ToInt32(setReq.SelectParkday[i]);
								reqLineForSave.CreateBy = User.UserId;
								reqLineForSave.UpdateBy = User.UserId;
								reqLineForSave.AppLevel = i + 1;
								userRepo.SetUserReqLineAndApp(reqLineForSave);
								i += 1;
							}
							else
							{
								break;
							}
						}
					}
				}
				TempData["Error"] = new ResourceString("Admin.CompanySetting.EditComplete");
				TempData["userRequester"] = null; //เคลียร์ค่าไม่ให้แสดงหน้า View
				return SetRequesterLine(companyId, costcenterId);
			}
		}
		private ActionResult ResetRequesterLine(string companyId, string costcenterId, string userGuid)
		{
			return SetRequesterLineByOwner(costcenterId, companyId, userGuid);
			//using (SqlConnection connection = new SqlConnection(ConnectionString))
			//{
			//    UserRepository userRepo = new UserRepository(connection);
			//    userRepo.ResetRequestline(costcenterId, companyId, userRepo.GetUserIdFromUserGuid(userGuId));
			//    return SetRequesterLine(companyId, costcenterId);
			//}
		}


		private ActionResult SetRequesterLineByOwner(string costcenterId, string companyId, string requesterGuid)
		{
			if (companyId == null) { return Http403(); }
			SetRequesterLine setReq = null;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				string requesterId = userRepo.GetUserIdFromUserGuid(requesterGuid);
				setReq = SetDataRequesterLine(companyId, costcenterId, requesterId);
				//setReq = SetDataRequesterLine(companyId, costcenterId, userRepo.GetUserIdFromUserGuid(requesterGuid));

				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				List<string> ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == requesterId).Select(a => a.ApproverId).ToList();
				TempData["ErrorResetReqLine"] = "";
				TempData["Error"] = "";
				foreach (var approverId in ItemUser)
				{
					if (costcenterRepo.IsReqAndAppInOrder(companyId, costcenterId, requesterId, approverId))
					{
						TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
					}
				}
			}
			return View("SetRequesterLineByOwner", setReq);
		}

		private ActionResult DeleteRequesterLine(string companyId, string costcenterId, string userRequester)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				SetRequesterLine setReq = SetDataRequesterLine(companyId, costcenterId, userRequester);
				UserRepository userRepo = new UserRepository(connection);
				userRepo.ClearReqLineAndAppByUser(companyId, costcenterId, userRequester);
				TempData["Error"] = new ResourceString("Button.DeleteRequesterLine");
				return SetRequesterLine(companyId, costcenterId);
			}
		}

		private ActionResult SetRequesterLineByOwner(string companyId, string costcenterId, string userRequester, string[] userApprover, int[] parkday, decimal[] approverBudget)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				SetRequesterLine setReq = SetDataRequesterLine(companyId, costcenterId, userRequester);
				SetRequesterLine reqLineDetail = GetRequesterLineDetail(companyId, costcenterId);
				reqLineDetail.SelectRequester = new string[] { userRequester, null, null };
				reqLineDetail.SelectApprover = userApprover;
				reqLineDetail.SelectParkday = parkday;
				reqLineDetail.SelectBudget = approverBudget;

				//เช็คว่ามีการผูกสิทธิ์ กับ Order ที่ยังดำเนินการไม่เสร็จหรือไม่
				List<string> ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
				int count = ItemUser.Count();
				for (int i = 0; i < count; i++)
				{
					if (ItemUser[i] != userApprover[i])
					{
						if (new CostCenterRepository(connection).IsReqAndAppInOrder(companyId, costcenterId, userRequester, ItemUser[i]))
						{
							TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
							return View("SetRequesterLineByOwner", setReq);
						}
					}
				}

				if (reqLineDetail.SelectRequester == null)
				{
					TempData["Error"] = new ResourceString("SetRequesterLine.SelectRequester");
					TempData["ErrorResetReqLine"] = "";
					return View("SetRequesterLineByOwner", setReq);
				}

				if (reqLineDetail.SelectApprover.All(o => o == ""))
				{
					TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
					TempData["ErrorResetReqLine"] = "";
					return View("SetRequesterLineByOwner", setReq);
				}

				bool IsPassCheckBudget = true;
				decimal budget = 0;
				int level = 0;
				foreach (var item in reqLineDetail.SelectBudget)
				{
					if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[level]) && item == 0) { break; }
					if (budget >= item)
					{
						IsPassCheckBudget = false;
						break;
					}
					budget = item;
					level++;
				}

				if (!IsPassCheckBudget)
				{
					TempData["Error"] = new ResourceString("SetRequesterLine.ErrorBudget");
					TempData["ErrorResetReqLine"] = "";
					return View("SetRequesterLineByOwner", setReq);
				}

				bool IsPassCheckApp = true;
				for (int i = 0; i < reqLineDetail.SelectApprover.Count(); i++)
				{
					if (string.IsNullOrEmpty(reqLineDetail.SelectApprover[i])) { break; }
					for (int j = i + 1; j < reqLineDetail.SelectApprover.Count(); j++)
					{
						if (reqLineDetail.SelectApprover[i] == reqLineDetail.SelectApprover[j])
						{
							IsPassCheckApp = false;
							break;
						}
					}
				}

				if (!IsPassCheckApp)
				{
					TempData["Error"] = new ResourceString("SetRequesterLine.ErrorApprover");
					TempData["ErrorResetReqLine"] = "";
					return View("SetRequesterLineByOwner", setReq);
				}
				UserRepository userRepo = new UserRepository(connection);
				userRepo.ResetRequestline(costcenterId, companyId, userRequester);

				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				if (!string.IsNullOrEmpty(reqLineDetail.SelectApprover[0]))
				{
					int i = 0;
					foreach (var itemApp in reqLineDetail.SelectApprover)
					{
						SetRequesterLine reqLineForSave = null;
						if (!string.IsNullOrEmpty(itemApp))
						{
							reqLineForSave = new SetRequesterLine();
							reqLineForSave.CompanyId = companyId;
							reqLineForSave.CostcenterId = costcenterId;
							reqLineForSave.RequesterUserId = reqLineDetail.SelectRequester[0];
							reqLineForSave.ApproverUserId = itemApp;
							reqLineForSave.AppCreditlimit = reqLineDetail.SelectBudget[i];
							reqLineForSave.Parkday = Convert.ToInt32(reqLineDetail.SelectParkday[i]);
							reqLineForSave.CreateBy = User.UserId;
							reqLineForSave.UpdateBy = User.UserId;
							reqLineForSave.AppLevel = i + 1;
							userRepo.SetUserReqLineAndApp(reqLineForSave);
							i += 1;
						}
						else
						{
							break;
						}
					}
				}
				setReq = SetDataRequesterLine(companyId, costcenterId, userRequester);
				ItemUser = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).OrderBy(a => a.ApproverLevel).Select(a => a.ApproverId).ToList();
				TempData["ErrorResetReqLine"] = "";
				TempData["Error"] = "";
				foreach (var approverId in ItemUser)
				{
					if (costcenterRepo.IsReqAndAppInOrder(companyId, costcenterId, userRequester, approverId))
					{
						TempData["ErrorResetReqLine"] = new ResourceString("SetRequesterLine.ErrorResetReqLine");
					}
				}

				TempData["Success"] = new ResourceString("SetRequesterLine.Complete");
				return View("SetRequesterLineByOwner", setReq);

			}
		}

		private SetRequesterLine SetDataRequesterLine(string companyId, string costcenterId, string userRequester)
		{
			SetRequesterLine setReq = GetRequesterLineDetail(companyId, costcenterId);
			setReq.UserReqDetail = new Models.User();
			setReq.CompanyId = companyId;
			setReq.UserReqDetail.UserEngName = setReq.UserRequester.Where(o => o.UserId == userRequester).Select(o => o.UserEngName).SingleOrDefault();
			setReq.UserReqDetail.UserThaiName = setReq.UserRequester.Where(o => o.UserId == userRequester).Select(o => o.UserThaiName).SingleOrDefault();
			setReq.UserReqDetail.UserId = setReq.UserRequester.Where(o => o.UserId == userRequester).Select(o => o.UserId).SingleOrDefault();
			setReq.UserReqDetail.UserGuid = setReq.UserRequester.Where(o => o.UserId == userRequester).Select(o => o.UserGuid).SingleOrDefault();
			setReq.UserReqDetail.DisplayName = new LocalizedString(setReq.UserReqDetail.UserThaiName);
			setReq.UserReqDetail.DisplayName["en"] = setReq.UserReqDetail.UserEngName;

			int countApprover = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester).Count();
			setReq.SelectApprover = new string[3];
			setReq.SelectParkday = new int[3];
			setReq.SelectBudget = new decimal[3];

			for (int i = 0; i < countApprover; i++)
			{
				setReq.SelectApprover[i] = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester && o.ApproverLevel == i + 1).Select(o => o.ApproverId).SingleOrDefault();
				setReq.SelectParkday[i] = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester && o.ApproverLevel == i + 1).Select(o => o.Parkday).SingleOrDefault();
				setReq.SelectBudget[i] = setReq.InformationRequesterLine.Where(o => o.RequesterId == userRequester && o.ApproverLevel == i + 1).Select(o => o.ApproverCreditBudget).SingleOrDefault();
			}
			return setReq;
		}


		public ActionResult FinalReview(string companyId, ImageInputData action, string name)
		{
			if (string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }

			if (name == "Department")
			{
				return FinalReviewDepartment(companyId);
			}

			if (name == "CostCenter")
			{
				return FinalReviewCostCenter(companyId);
			}

			if (name == "User")
			{
				return FinalReviewUser(companyId);
			}

			if (name == "RequesterLine")
			{
				return FinalReviewRequesterLine(companyId);
			}


			switch (action.Name)
			{
				default:
					return FinalReview(companyId);
			}
		}
		private ActionResult FinalReview(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CompanyRepository compRepo = new CompanyRepository(connection);
				CostCenterRepository costCenterRepo = new CostCenterRepository(connection);
				FinalReview reviewData = new FinalReview();
				reviewData.FinalCompany = compRepo.GetCompanyDetail(companyId);
				reviewData.FinalCompany.ListShipAddress = compRepo.GetCompanyShipping(companyId);
				reviewData.Invoice = costCenterRepo.GetAllInvoidAddress(companyId);
				return View("FinalReview", reviewData);
			}
		}

		private ActionResult FinalReviewCostCenter(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				FinalReview finalReview = new FinalReview();
				finalReview.CostCenter = new CostCenterRepository(connection).GetAllCostCenter(companyId);
				return View("FinalReviewForCostCenter", finalReview);
			}
		}

		private ActionResult FinalReviewUser(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				FinalReview finalReview = new FinalReview();
				finalReview.UserAdmin = userRepo.GetAllUser(companyId, true);
				finalReview.UserPurchase = userRepo.GetAllUser(companyId, false);
				return View("FinalReviewForUser", finalReview);
			}
		}

		private ActionResult FinalReviewRequesterLine(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				FinalReview finalReview = new FinalReview();
				finalReview.RequesterLineDetail = new CompanyRepository(connection).GetAllCompanyRequesterLine(companyId);
				return View("FinalReviewForRequesterLine", finalReview);
			}
		}

		private ActionResult FinalReviewDepartment(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				FinalReview finalReview = new FinalReview();
				finalReview.CompanyDepartment = new CompanyDepartmentRepository(connection).GetMyCompanyDepartment(companyId);
				return View("FinalReviewForDepartment", finalReview);
			}
		}



		public ActionResult AllSetting(string companyId, ImageInputData action)
		{
			if (string.IsNullOrEmpty(companyId)) { return RedirectToAction("Index"); }
			switch (action.Name)
			{
				case "CompanyInfo":
					return CompanyInformation(companyId);
				case "Department":
					return ViewAllDepartment(companyId);
				case "CostCenter":
					return ViewAllCostCenter(companyId);
				case "UserInfo":
					return ViewAllUser(companyId);
				case "RequesterLines":
					return FinalReview(companyId);
				case "CompanyLogo":
					return CompanyLogo(companyId);
				case "RoleUsers":
					return ViewAllUser(companyId);
				case "Approvals":
					return ViewAllCostCenter(companyId);
				case "CompanySetting":
					return CompanySetting(companyId);
				//case "Dashboard":
				//    return Dashboard(companyId);
				//case "BudgetReport":
				//    return BudgetReport(companyId);
				default:
					return ViewAllSetting(companyId);
			}
		}
		private ActionResult ViewAllSetting(string companyId)
		{
			Company company = new Company();
			company.CompanyId = companyId;
			return View("AllSetting", company);
		}

		public ActionResult ManageNewsSearch(ImageInputData Action, string companyId)
		{
			switch (Action.Name)
			{
				case "ViewDetail":
					return ViewNewsDetail(Action.Value, "ViewDetail");
				case "CreateNews":
					return View("ManageNews", new News());
				case "EditNews":
					return ViewNewsDetail(Action.Value, "EditNews");
				case "Search":
					if (!string.IsNullOrEmpty(companyId)) { return ViewAllNews(companyId); }
					else { return ViewAllNews(""); }
				default:
					return ViewAllNews("");
			}
		}
		[HttpPost]
		[ValidateInput(false)]
		public ActionResult ManageNews(ImageInputData Action, News news)
		{
			switch (Action.Name)
			{
				case "SaveNews":
					return CreateNews(news);
				case "SaveEditNews":
					return EditNews(news);
				default:
					return ViewAllNews("");
			}
		}
		public ActionResult ViewAllNews(string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				NewsRepository newsRepo = new NewsRepository(connection);
				IEnumerable<News> itemNews = newsRepo.GetAllNews(companyId);
				return View("ViewAllNews", new MasterPageData<IEnumerable<News>>(itemNews));
			}
		}
		private ActionResult ViewNewsDetail(string newsGuid, string action)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				NewsRepository newsRepo = new NewsRepository(connection);
				News news = new News();
				news = newsRepo.GetNewsDetail(newsGuid, "");
				if (action == "EditNews")
				{
					return View("ManageNews", news);
				}
				else
				{
					return View("ViewNewsDetail", news);
				}
			}
		}
		private ActionResult CreateNews(News news)
		{
			if (ModelState.IsValid)
			{
				DateTime endYear = new DateTime(DateTime.Now.Year, 12, 31);
				if (news.Period == "Now")
				{
					news.ValidFrom = DateTime.Today;
					news.ValidTo = endYear;
				}
				else
				{
					if (String.IsNullOrEmpty(news.ValidFromDisplay) || String.IsNullOrEmpty(news.ValidToDisplay))
					{
						ModelState.AddModelError("ValidFrom", "Admin.EditNewsCompany.ErrorNewsValidFromOrValidToEmptry");
						return View("ManageNews", news);
					}

					DateTime fromDateTime, toDateTime;
					if (!String.IsNullOrEmpty(news.ValidFromDisplay) && DateTime.TryParseExact(news.ValidFromDisplay, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateTime))
					{
						news.ValidFrom = fromDateTime;
					}
					if (!String.IsNullOrEmpty(news.ValidToDisplay) && DateTime.TryParseExact(news.ValidToDisplay, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDateTime))
					{
						news.ValidTo = toDateTime;
					}
				}
				if (news.ValidFrom < DateTime.Today) { news.ValidFrom = DateTime.Today; }
				if (news.ValidTo > endYear) { news.ValidTo = endYear; }

				if (news.ValidFrom > news.ValidTo)
				{
					ModelState.AddModelError("ValidFrom", "Admin.CreateNewsCompany.ErrorValid");
					return View("ManageNews", news);
				}

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					NewsRepository newsRepo = new NewsRepository(connection);
					news.CreateBy = User.UserId;
					news.CompanyID = "OfficeMate";
					news.NewsType = "Public";
					newsRepo.InsertNewsCompany(news);
					return RedirectToAction("ViewAllNews", new { companyId = news.CompanyID });
				}
			}
			return View("ManageNews", news);
		}
		private ActionResult EditNews(News news)
		{
			if (ModelState.IsValid)
			{
				DateTime endYear = new DateTime(DateTime.Now.Year, 12, 31);
				if (news.Period == "Now")
				{
					news.ValidFrom = DateTime.Today;
					news.ValidTo = endYear;
				}
				else
				{
					if (String.IsNullOrEmpty(news.ValidFromDisplay) || String.IsNullOrEmpty(news.ValidToDisplay))
					{
						ModelState.AddModelError("ValidFrom", "Admin.EditNewsCompany.ErrorNewsValidFromOrValidToEmptry");
						return View("ManageNews", news);
					}

					DateTime fromDateTime, toDateTime;
					if (!String.IsNullOrEmpty(news.ValidFromDisplay) && DateTime.TryParseExact(news.ValidFromDisplay, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateTime))
					{
						news.ValidFrom = fromDateTime;
					}
					if (!String.IsNullOrEmpty(news.ValidToDisplay) && DateTime.TryParseExact(news.ValidToDisplay, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDateTime))
					{
						news.ValidTo = toDateTime;
					}
				}
				if (news.ValidFrom < DateTime.Today) { news.ValidFrom = DateTime.Today; }
				if (news.ValidTo > endYear) { news.ValidTo = endYear; }

				if (news.ValidFrom > news.ValidTo)
				{
					ModelState.AddModelError("ValidFrom", "Admin.CreateNewsCompany.ErrorValid");
					return View("ManageNews", news);
				}

				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					NewsRepository newsRepo = new NewsRepository(connection);
					news.UpdateBy = User.UserId;
					news.CompanyID = string.IsNullOrEmpty(news.CompanyID) ? "" : news.CompanyID;
					newsRepo.UpdateNewsCompany(news);
					TempData["NewsComplete"] = new ResourceString("Admin.NewsCompany.EditNewsComplete");
				}
			}
			return View("ManageNews", news);
		}

		public ActionResult TrackOrders(int? page, JobStatus? jobStatus, DateFilter? dateFilter)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = page.HasValue ? page.Value : 1;
			trackOrders.JobStatus = jobStatus.HasValue ? jobStatus.Value : JobStatus.Open;
			if (trackOrders.JobStatus == JobStatus.Open)
			{
				trackOrders.DateFilter = dateFilter.HasValue ? dateFilter.Value : DateFilter.Today;
			}

			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.TotalOrdersCount = reportRepo.GetCountTrackOrders(trackOrders.JobStatus, trackOrders.DateFilter);
				trackOrders.ListOrder.Orders = reportRepo.GetTrackOrders(trackOrders.PageSize, trackOrders.OrderIndex, trackOrders.JobStatus, trackOrders.DateFilter);
				trackOrders.AdvanceSearchField = new AdvanceSearchField();
				return View("TrackOrders", trackOrders);
			}
		}
		public ActionResult SearchOrderAdvance(JobStatus jobStatus, AdvanceSearchField advanceSearchField)
		{
			TrackOrders trackOrders = new TrackOrders();
			trackOrders.PageNumber = 1;
			trackOrders.JobStatus = jobStatus;
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ReportRepository reportRepo = new ReportRepository(connection);
				trackOrders.ListOrder = new ListOrder();
				trackOrders.ListOrder.Orders = reportRepo.GetAdvanceSearchOrders(trackOrders.JobStatus, advanceSearchField);
				trackOrders.TotalOrdersCount = trackOrders.ListOrder.Orders.Count();
				trackOrders.AdvanceSearchField = advanceSearchField;
				return View("TrackOrders", trackOrders);
			}
		}
		public ActionResult ViewOrderDetail(string guid)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				UserRepository userRepo = new UserRepository(connection);
				OrderData orderData = new OrderData();
				string orderId = orderRePo.GetOrderIdByGuid(guid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
				orderData.Order = orderRePo.GetMyOrder(orderId);// นำ OrderId ส่งไปดึงข้อมูล Order  และส่งไปแสดงที่ ViewOrder
				orderData.OrderActivity = orderRePo.GetOrderActivity(orderId);//ดึงข้อมูล Activity ของ Order
				orderData.UserApprover = userRepo.GetListUserApprover(orderData.Order.Company.CompanyId, orderData.Order.CostCenter.CostCenterID, orderData.Order.Requester.UserId);//ดึงข้อมูลผู้อนุมัติ ของ Costcenter นี้
				orderData.ProcessLog = orderRePo.GetOrderProcessLog(orderId);//ดึงข้อมูล ProcessLog ของ Order
				orderData.MailTransLog = orderRePo.GetOrderMailTransLog(orderId);//ดึงข้อมูล Mail TransLog
				orderData.Order.User = User;//สร้างไว้ใช้ใน order detail partial view
				return View(orderData);
			}
		}

		public ActionResult ViewOrderMail(string guid, int logId)
		{
			if (!string.IsNullOrEmpty(guid))
			{
				MailTransLog maillog = GetMailTransLogByLogId(guid, logId);
				return View(new MasterPageData<MailTransLog>(maillog));
			}
			else
			{
				MailTransLog maillog = GetMailLog(logId);
				return View(new MasterPageData<MailTransLog>(maillog));
			}
		}

		public ActionResult ReSendOrderMail(string guid, int logId, string remark)
		{
			MailTransLog maillog = GetMailTransLogByLogId(guid, logId);

			System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
			System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

			smtpClient.Host = IsProduction ? GetSetting("ProductionSmtpServer") : GetSetting("DevelopmentSmtpServer");
			if (IsProduction)
			{
				smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("ProductionSmtpServerUsername"), GetSetting("ProductionSmtpServerPassword"));
			}
			else
			{
				smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("DevelopmentSmtpServerUsername"), GetSetting("DevelopmentSmtpServerPassword"));
			}

			mailMessage.From = new System.Net.Mail.MailAddress(maillog.EmailFrom);
			mailMessage.To.Add(new System.Net.Mail.MailAddress(maillog.EmailTo));

			if (!string.IsNullOrEmpty(maillog.EmailCC))
			{
				List<string> mailCC = maillog.EmailCC.Split(',').ToList();
				foreach (var item in mailCC)
				{
					mailMessage.CC.Add(new System.Net.Mail.MailAddress(item));
				}
			}

			if (!string.IsNullOrEmpty(maillog.EmailBCC))
			{
				List<string> mailBCC = maillog.EmailBCC.Split(',').ToList();
				foreach (var item in mailBCC)
				{
					mailMessage.Bcc.Add(new System.Net.Mail.MailAddress(item));
				}
			}

			mailMessage.Subject = maillog.Subject;
			mailMessage.IsBodyHtml = true;
			mailMessage.Body = maillog.MailContent;

			try
			{
				smtpClient.Send(mailMessage);
				new EmailRepository(new SqlConnection(ConnectionString)).AddOrderProcessLog(maillog.OrderId, "resend mail", remark, IPAddress, User.UserId);
				TempData["SendComplete"] = "ส่งเมล์เรียบร้อยแล้วค่ะ";
			}
			catch (System.Net.Mail.SmtpFailedRecipientException)
			{
				Log.Warn("System.Net.Mail.SmtpFailedRecipientException Swallowed");
				TempData["SendComplete"] = "ไม่สามารถส่งเมล์ได้ค่ะ";
			}
			return RedirectToAction("ViewOrderMail", new { guid, logId });
		}

		public ActionResult ReSendSearchMail(string[] listLogId, SearchMail searchMail)
		{

			if (listLogId == null || !listLogId.Any())
			{
				TempData["MessageSendMail"] = "กรุณาเลือกรายการส่งเมล์ด้วยค่ะ";
				return SearchMail(searchMail, "", "", "", "");
			}

			foreach (string logId in listLogId)
			{
				var result = logId.Split('_');
				int result_logId = Convert.ToInt32(result[0]);
				string result_guid = result[1];

				MailTransLog maillog = GetMailTransLogByLogId(result_guid, result_logId);

				System.Net.Mail.SmtpClient smtpClient = new System.Net.Mail.SmtpClient();
				System.Net.Mail.MailMessage mailMessage = new System.Net.Mail.MailMessage();

				smtpClient.Host = IsProduction ? GetSetting("ProductionSmtpServer") : GetSetting("DevelopmentSmtpServer");
				if (IsProduction)
				{
					smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("ProductionSmtpServerUsername"), GetSetting("ProductionSmtpServerPassword"));
				}
				else
				{
					smtpClient.Credentials = new System.Net.NetworkCredential(GetSetting("DevelopmentSmtpServerUsername"), GetSetting("DevelopmentSmtpServerPassword"));
				}


				mailMessage.From = new System.Net.Mail.MailAddress(maillog.EmailFrom);
				mailMessage.To.Add(new System.Net.Mail.MailAddress(maillog.EmailTo));


				if (!string.IsNullOrEmpty(maillog.EmailCC))
				{
					List<string> mailCC = maillog.EmailCC.Split(',').ToList();
					foreach (var item in mailCC)
					{
						mailMessage.CC.Add(new System.Net.Mail.MailAddress(item));
					}
				}

				if (!string.IsNullOrEmpty(maillog.EmailBCC))
				{
					List<string> mailBCC = maillog.EmailBCC.Split(',').ToList();
					foreach (var item in mailBCC)
					{
						mailMessage.Bcc.Add(new System.Net.Mail.MailAddress(item));
					}
				}

				mailMessage.Subject = maillog.Subject;
				mailMessage.IsBodyHtml = true;
				mailMessage.Body = maillog.MailContent;

				try
				{
					smtpClient.Send(mailMessage);
					new EmailRepository(new SqlConnection(ConnectionString)).AddOrderProcessLog(maillog.OrderId, "resend mail", "ส่งจากเจ้าหน้าที่ officemate", IPAddress, User.UserId);
					TempData["SendComplete"] = "ส่งเมล์เรียบร้อยแล้วค่ะ";
				}
				catch (System.Net.Mail.SmtpFailedRecipientException)
				{
					Log.Warn("System.Net.Mail.SmtpFailedRecipientException Swallowed");
					TempData["SendComplete"] = "ไม่สามารถส่งเมล์ได้ค่ะ";
				}
			}

			return RedirectToAction("SearchMail", searchMail);
		}

		private MailTransLog GetMailTransLogByLogId(string guid, int logId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository repo = new OrderRepository(connection);
				string orderId = repo.GetOrderIdByGuid(guid);
				MailTransLog mailTrans = repo.GetOrderMailTransLog(orderId).FirstOrDefault(log => log.LogID == logId);
				mailTrans.Guid = guid;
				return mailTrans;
			}
		}

		private MailTransLog GetMailLog(int logId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				EmailRepository mailRepo = new EmailRepository(connection);
				MailTransLog mailTrans = mailRepo.GetMailTransLog(logId).FirstOrDefault(log => log.LogID == logId);
				return mailTrans;
			}
		}

		public ActionResult EditProcessRemark(string guid, string processRemark, string Remark)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				orderRePo.UpdateProcessRemark(orderRePo.GetOrderIdByGuid(guid), processRemark, Remark, User.UserId, IPAddress);
				return RedirectToAction("ViewOrderDetail", new { guid = guid });
			}
		}
		public ActionResult EditShippingType(string guid, string shippingType, string Remark)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				OrderRepository orderRePo = new OrderRepository(connection);
				orderRePo.UpdateShippingType(orderRePo.GetOrderIdByGuid(guid), shippingType, Remark, User.UserId, IPAddress);
				return RedirectToAction("ViewOrderDetail", new { guid = guid });
			}
		}

		[HttpGet]
		public ActionResult ProductControl()
		{
			return View(new ProductCatalog());
		}
		[HttpGet]
		public ActionResult ProductControlImport()
		{
			return View(new ProductCatalog());
		}
		[HttpPost]
		public ActionResult ProductControl(ImageInputData Action, ProductCatalog productCatalog, string[] listuserId)
		{
			if (string.IsNullOrEmpty(Action.Name)) { return RedirectToAction("Index"); }
			switch (Action.Name)
			{
				case "CheckCompany":
					return View("ProductControl", GetDataForAddProductCatalog(productCatalog));
				case "CheckProductId":
					if (string.IsNullOrEmpty(productCatalog.ProductId))  //ไม่ได้เลือก pId มา
					{
						TempData["ErrorSelectProduct"] = new ResourceString("ProductCatalog.AddProductErrorPID0");
					}
					return View("ProductControl", GetDataForAddProductCatalog(productCatalog));
				case "SaveProduct":
					if (!productCatalog.IsByCompany && listuserId == null)  //ไม่ได้เลือก UserId มา
					{
						TempData["Error"] = new ResourceString("ProductCatalog.ErrorSelectUser");
						return View("ProductControl", GetDataForAddProductCatalog(productCatalog));
					}
					return AddProductCatalog(GetDataForAddProductCatalog(productCatalog), listuserId);
				default:
					return View(new ProductCatalog());
			}
		}

		[HttpPost]
		public ActionResult ProductControlImport(ImageInputData Action, ProductCatalog productCatalog)
		{
			if (string.IsNullOrEmpty(Action.Name)) { return RedirectToAction("Index"); }
			switch (Action.Name)
			{
				case "CheckCompany":
					return View("ProductControlImport", GetDataForAddProductCatalog(productCatalog));
				default:
					return View(new ProductCatalog());
			}
		}

		[HttpPost]
		public ActionResult UploadExcel(HttpPostedFileBase file, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (file == null)
				{
					TempData["errormessage"] = "กรุณาเลือกรูปภาพด้วยค่ะ";
				}
				else if (!file.ContentType.StartsWith("application/vnd.ms-excel"))
				{
					TempData["errormessage"] = "กรุณาเลือกไฟล์นามสกุล .xls ค่ะ";
				}
				else
				{
					NPOI.HSSF.UserModel.HSSFWorkbook hssfwb;
					hssfwb = new NPOI.HSSF.UserModel.HSSFWorkbook(file.InputStream);
					NPOI.SS.UserModel.Sheet sheet = hssfwb.GetSheet("Sheet1");
					List<ExportProduct> itemProduct = new List<ExportProduct>();
					ExportProduct product = null;
					for (int row = 0; row <= sheet.LastRowNum; row++)
					{
						if (sheet.GetRow(row) != null) //null is when the row only contains empty cells 
						{
							if (row > 0)
							{
								product = new ExportProduct();
								product.Pid = sheet.GetRow(row).GetCell(0).ToString();
								product.StatusBaseDeal = sheet.GetRow(row).GetCell(1).ToString().ToLower() == "yes" ? "Yes" : "No";
								product.CompanyId = companyId;
								product.UserId = User.UserId;
								itemProduct.Add(product);
							}
						}
					}

					ProductCatalogRepository productRepository = new ProductCatalogRepository(connection);
					productRepository.ImportDataProductCatalog(itemProduct, companyId, User.UserId);

				}
				TempData["ImportComplete"] = "ระบบได้ทำการเพิ่มข้อมูลเรียบร้อยแล้วค่ะ";
				return View("ProductControlImport", new ProductCatalog());
			}
		}

		public FilePathResult GetImportFile()
		{
			return File(Path.Combine(Server.MapPath("~/App_Data"), "ImportProduct.xls"), "application/vnd.ms-excel");
		}

		private ProductCatalog GetDataForAddProductCatalog(ProductCatalog productCatalog)
		{
			if (string.IsNullOrEmpty(productCatalog.CompanyId))
			{
				TempData["ErrorSelectCompany"] = "กรุณาระบุรหัสองค์กรด้วยค่ะ";
				return new ProductCatalog();
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				Company company = new CompanyRepository(connection).GetCompany(productCatalog.CompanyId);
				if (company == null)
				{
					TempData["ErrorSelectCompany"] = "ไม่พบรหัสองค์กรนี้ค่ะ";
					return new ProductCatalog();
				}
				if (company.UseOfmCatalog)
				{
					TempData["ErrorSelectCompany"] = string.Format("รหัสองค์กร {0} ใช้งาน officemate catalog อยู่ ไม่สามารถเพิ่มสินค้าได้ค่ะ", company.CompanyId);
				}
				else
				{
					productCatalog.Company = company;
					productCatalog.UserRequester = new UserRepository(connection).GetAllUserReqOrApp(productCatalog.CompanyId).Where(r => r.UserRoleName == Models.User.UserRole.Requester);
				}
				if (!string.IsNullOrEmpty(productCatalog.ProductId))
				{
					User.Company = company;
					productCatalog.ProductDetail = new ProductRepository(connection, User).GetProductDetailByPidForOFMAdmin(productCatalog.ProductId);
					if (productCatalog.ProductDetail == null) { TempData["ErrorSelectProduct"] = string.Format("ไม่พบข้อมูลสินค้ารหัส {0} ค่ะ", productCatalog.ProductId); }
				}
				return productCatalog;
			}
		}
		private ActionResult AddProductCatalog(ProductCatalog productCatalog, string[] listuserId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				DateTime ValidFrom = new DateTime();
				DateTime ValidTo = new DateTime();
				if (string.IsNullOrEmpty(productCatalog.StartDate)) //ไม่ได้ Setค่า วันที่เริ่มต้นมากำหนดเป็นเวลาปัจจุบัน
				{
					ValidFrom = DateTime.Now;
				}
				else
				{
					ValidFrom = DateTime.ParseExact(productCatalog.StartDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
				}
				if (string.IsNullOrEmpty(productCatalog.EndDate)) //ไม่ได้ Setค่า วันที่สุดท้ายมากำหนดเป็นเวลาปัจจุบัน+50 ปี
				{
					ValidTo = DateTime.Now.AddYears(50);
				}
				else
				{
					ValidTo = DateTime.ParseExact(productCatalog.EndDate, "dd-MM-yyyy", System.Globalization.CultureInfo.InvariantCulture);
				}
				productCatalog.listUser = listuserId;
				ProductCatalogRepository catalogRepo = new ProductCatalogRepository(connection);
				if (!catalogRepo.HaveProductInYourCompanyList(productCatalog.ProductDetail.Id, productCatalog.Company.CompanyId)) //เช็คว่ารหัสสินค้านี้ มีอยู่ใน company catalog แล้วหรือไม่
				{
					if (productCatalog.IsByCompany)//เพิ่มทั้ง Company
					{
						catalogRepo.AddProductCatalog(productCatalog.ProductDetail.Id, productCatalog.Company.CompanyId, ValidFrom, ValidTo, User.UserId);
						new GenerateDeptStructureControl(connection).GentDeptStructure(productCatalog.Company.CompanyId);
					}
					else//เพิ่มเฉพาะคน
					{
						foreach (var itemUser in listuserId)
						{
							if (!catalogRepo.HaveProductInYourUserList(productCatalog.ProductDetail.Id, productCatalog.Company.CompanyId, itemUser))
							{
								catalogRepo.AddProductCatalogToRequester(productCatalog.ProductDetail.Id, itemUser, productCatalog.Company.CompanyId, ValidFrom, ValidTo, User.UserId);
							}
						}
						new GenerateDeptStructureControl(connection).GentDeptStructure(productCatalog.Company.CompanyId);
					}
					return RedirectToAction("ProductAddCatalogSuccess", new { companyId = productCatalog.Company.CompanyId });
				}
				else
				{
					TempData["Error"] = String.Format(new ResourceString("ProductCatalog.AddProductErrorPID2"), productCatalog.ProductId);
					return View("ProductControl", GetDataForAddProductCatalog(productCatalog));
				}
			}
		}

		public ActionResult ProductAddCatalogSuccess(string companyId)
		{
			TempData["ErrorSelectCompany"] = new ResourceString("Admin.CompanySetting.EditComplete");
			ProductCatalog productCatalog = new ProductCatalog();
			productCatalog.CompanyId = companyId;
			return View("ProductControl", GetDataForAddProductCatalog(productCatalog));
		}

		[HttpGet]
		public ActionResult GentDeptStructureByCompanyId() //ใช้ในการสร้าง DeptStructure ของแต่ละ CompanyId
		{
			return View();
		}
		[HttpPost]
		public ActionResult GentDeptStructureByCompanyId(string companyId, string siteType) //ใช้ในการสร้าง DeptStructure ของแต่ละ CompanyId
		{
			if (!string.IsNullOrEmpty(companyId))
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					if (siteType.CompareTo("OCI") == 0)
					{
						TempData["data"] = new GenerateDeptStructureControl(connection).GentDeptStructureForOCI(companyId);
					}
					else
					{
						TempData["data"] = new GenerateDeptStructureControl(connection).GentDeptStructure(companyId);
					}

				}
			}
			return View();
		}

		[HttpGet]
		public ActionResult GentDeptStructureForOFMCatalog() //ใช้ในการสร้าง DeptStructure ของ OFMCatalog
		{
			return View();
		}

		[HttpPost]
		public ActionResult GentDeptStructureForOFMCatalog(string password) //ใช้ในการสร้าง DeptStructure ของ OFMCatalog
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				if (new UserRepository(connection).IsPasswordCorrect(User.UserId, password))
				{
					new GenerateDeptStructureControl(connection).GentDeptStructureForOFM();
					TempData["data"] = "ดำเนินการเรียบแล้วค่ะ";
				}
				else
				{
					TempData["data"] = "กรุณากรอกรหัสผ่านให้ถูกต้องด้วยค่ะ";
				}
			}
			return View();
		}

		public ActionResult Help()//หน้า Help OfficemateAdmin
		{
			return View();
		}

		public ActionResult ViewUserDetail(string userGuid, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				EditProfileUser user = new EditProfileUser();
				user = userRepo.GetUserDataForEdit(userGuid, companyId);
				if (user == null) { return ViewAllUser(User.CurrentCompanyId); }
				return View(user);
			}
		}

		[HttpGet]
		public ActionResult ManageShipping(string companyId, string custId, string shipId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ShippingData shippingData = new ShippingData();
				shippingData.CompanyId = companyId;
				shippingData.ListCustId = new CompanyRepository(connection).GetAllCompanyAddress(companyId).Select(c => c.CustId);
				IEnumerable<Shipping> shippings = new CompanyRepository(connection).GetCompanyShipping(companyId);
				if (!string.IsNullOrEmpty(custId) && !string.IsNullOrEmpty(shipId)) //edit shipping
				{
					shippingData.ProvinceList = new ProvinceEproRepository(connection).GetProvinceWithNoDefault();
					Shipping selectShip = shippings.Where(s => s.ShipID.ToString() == shipId).First();
					shippingData.ShipAddress1 = selectShip.ShipContactor;
					shippingData.PhoneID = selectShip.PhoneID;
					shippingData.ShipPhoneNumber = selectShip.ShipPhoneNo;
					shippingData.ShipPhoneNumberExtension = selectShip.Extension;
					shippingData.ShipAddress2 = selectShip.Address2;
					shippingData.ShipAddress3 = selectShip.Address3;
					shippingData.ProvinceSelected = selectShip.Province;
					shippingData.ShipAddress4 = selectShip.ZipCode;
					shippingData.CustIdSelected = custId;
					shippingData.OtherShippings = shippings.Where(s => s.ShipID.ToString() != shipId);
					shippingData.ShipID = Convert.ToInt32(shipId);
				}
				else //create shipping
				{
					shippingData.ProvinceList = new ProvinceEproRepository(connection).GetProvince();
					shippingData.OtherShippings = shippings;
				}
				return View("ManageShipping", shippingData);
			}
		}

		[HttpPost]
		public ActionResult ManageShipping(ShippingData shippingData, ImageInputData action, string companyId)
		{
			switch (action.Name)
			{
				case "CreateShipping":
					return CreateShipping(shippingData, companyId);
				case "SaveShipping":
					return EditShipping(shippingData, companyId);
				default:
					return RedirectToAction("ManageShipping", new { companyId = companyId, custId = string.Empty, shipId = string.Empty });
			}
		}

		private ActionResult CreateShipping(ShippingData shippingData, string companyId)
		{
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					OfficeMate.Framework.CustomerData.Repositories.ShippingRepository shippingRepo = new OfficeMate.Framework.CustomerData.Repositories.ShippingRepository(connection);
					OfficeMate.Framework.CustomerData.CustomerShipping shipping = new OfficeMate.Framework.CustomerData.CustomerShipping();

					List<OfficeMate.Framework.CustomerData.PhoneNumber> phones = new List<OfficeMate.Framework.CustomerData.PhoneNumber>();
					OfficeMate.Framework.CustomerData.PhoneNumber phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
					phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Shipping;
					phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
					phone.PhoneNo = shippingData.ShipPhoneNumber;
					phone.Extension = shippingData.ShipPhoneNumberExtension ?? String.Empty;
					phone.CustomerId = shippingData.CustIdSelected;
					phone.SequenceId = shipping.Id;
					phones.Add(phone);
					shipping.Phones = phones;

					shipping.CustomerId = shippingData.CustIdSelected;
					shipping.Contactor = shippingData.ShipAddress1;
					shipping.PhoneNo = shippingData.ShipPhoneNumber;
					shipping.ShipAddress1 = shippingData.ShipAddress1;
					shipping.ShipAddress2 = shippingData.ShipAddress2;
					shipping.ShipAddress3 = shippingData.ShipAddress3 ?? String.Empty;
					shipping.ShipAddress4 = shippingData.ProvinceSelected + " " + shippingData.ShipAddress4;
					shipping.Province = shippingData.ProvinceSelected;
					shipping.ZipCode = shippingData.ShipAddress4;
					shipping.IsDeliveryFee = new ProvinceEproRepository(connection).GetDeliverFee(shipping.Province);
					shippingRepo.InsertShipping(shipping, "EproV5");
				}
				return RedirectToAction("ManageShipping", new { companyId = companyId, custId = string.Empty, shipId = string.Empty });
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ShippingData Shipping = new ShippingData();
				CompanyRepository companyRepo = new CompanyRepository(connection);
				Shipping.CompanyId = companyId;
				Shipping.OtherShippings = companyRepo.GetCompanyShipping(companyId);
				Shipping.ListCustId = companyRepo.GetAllCompanyAddress(companyId).Select(c => c.CustId);
				Shipping.ProvinceList = new ProvinceEproRepository(connection).GetProvinceWithNoDefault();
				return View("ManageShipping", Shipping);
			}
		}

		private ActionResult EditShipping(ShippingData shippingData, string companyId)
		{
			if (ModelState.IsValid)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					OfficeMate.Framework.CustomerData.Repositories.ShippingRepository shippingRepo = new OfficeMate.Framework.CustomerData.Repositories.ShippingRepository(connection);
					OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repo = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(connection);
					OfficeMate.Framework.CustomerData.CustomerShipping selectShipping = new OfficeMate.Framework.CustomerData.CustomerShipping();
					OfficeMate.Framework.CustomerData.PhoneNumber phone;

					if (shippingData.PhoneID == 0) //new phone number
					{
						phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
						phone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Shipping;
						phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
						phone.CustomerId = shippingData.CustIdSelected;
						phone.SequenceId = shippingData.ShipID;
						phone.PhoneNo = shippingData.ShipPhoneNumber;
						phone.Extension = string.IsNullOrEmpty(shippingData.ShipPhoneNumberExtension) ? "" : shippingData.ShipPhoneNumberExtension;
						repo.InsertPhoneNumber(phone, "EproV5");
						//repo.setDefaultPhoneNumber(phone, "EproV5");
					}
					else //edit phone number
					{
						phone = repo.SelectPhone(shippingData.PhoneID);
						phone.PhoneNo = shippingData.ShipPhoneNumber;
						phone.Extension = string.IsNullOrEmpty(shippingData.ShipPhoneNumberExtension) ? "" : shippingData.ShipPhoneNumberExtension;
						repo.UpdatePhoneNumber(phone, "EproV5");
					}

					selectShipping = shippingRepo.SelectShipping(shippingData.ShipID);
					selectShipping.CustomerId = shippingData.CustIdSelected;
					selectShipping.Contactor = shippingData.ShipAddress1;
					selectShipping.PhoneNo = shippingData.ShipPhoneNumber;
					selectShipping.ShipAddress1 = shippingData.ShipAddress1;
					selectShipping.ShipAddress2 = shippingData.ShipAddress2;
					selectShipping.ShipAddress3 = shippingData.ShipAddress3 ?? String.Empty;
					selectShipping.ShipAddress4 = shippingData.ProvinceSelected + " " + shippingData.ShipAddress4;
					selectShipping.Province = shippingData.ProvinceSelected;
					selectShipping.ZipCode = shippingData.ShipAddress4;
					selectShipping.IsDeliveryFee = new ProvinceEproRepository(connection).GetDeliverFee(selectShipping.Province);

					shippingRepo.UpdateShipping(selectShipping, "EproV5");
				}
				return RedirectToAction("ManageShipping", new { companyId = companyId, custId = string.Empty, shipId = string.Empty });
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				ShippingData Shipping = new ShippingData();
				CompanyRepository companyRepo = new CompanyRepository(connection);
				Shipping.CompanyId = companyId;
				Shipping.OtherShippings = companyRepo.GetCompanyShipping(companyId);
				Shipping.ListCustId = companyRepo.GetAllCompanyAddress(companyId).Select(c => c.CustId);
				Shipping.ProvinceList = new ProvinceEproRepository(connection).GetProvinceWithNoDefault();
				return View("ManageShipping", Shipping);
			}
		}
		[HttpGet]
		public ActionResult CreateCustomer()
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				NewCustMasterData data = new NewCustMasterData();
				data.ProvinceList = new ProvinceEproRepository(connection).GetProvince();
				data.PaymentTypeList = new UserRepository(connection).GetPaymentType();
				return View(data);
			}
		}
		[HttpPost]
		public ActionResult CreateCustomer(string customerId, string email, NewCustMasterData data, ImageInputData action)
		{
			if (action.Name == "CheckData")
			{
				if (string.IsNullOrEmpty(customerId) || string.IsNullOrEmpty(email))
				{
					TempData["ErrorMessage"] = "กรุณาระบุรหัสลูกค้าและ e-Mail ด้วยค่ะ";
					data = new NewCustMasterData();
				}
				else
				{
					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						UserRepository userRepo = new UserRepository(connection);
						if (userRepo.CustIdExistMasterUser(customerId))
						{
							TempData["ErrorMessage"] = "รหัสลูกค้า นี้มีข้อมูลใน master แล้วค่ะ";
							data = new NewCustMasterData();
						}
						else
						{
							TempData["ErrorMessage"] = "รหัสลูกค้า และ e-mail นี้ไม่มีข้อมูลใน master สามารถใช้ได้ค่ะ";
							data = new NewCustMasterData();
							data.CustomerId = customerId;
							data.ContactEmail = email;
						}
					}
				}
			}
			else if (action.Name == "SaveData")
			{
				if (ModelState.IsValid && !string.IsNullOrEmpty(data.CustomerId) && !string.IsNullOrEmpty(data.ContactEmail))
				{
					int contactid;
					int shippingid;
					InsertCustomer(data, ConnectionString, out contactid, out shippingid);
					if (contactid > 0 && shippingid > 0)
					{
						InsertPhoneNewCustomer(data, contactid, shippingid, ConnectionString);
						UserRepository userRepo = new UserRepository(new SqlConnection(ConnectionString));
						if (!userRepo.UserExistMasterUser(data.ContactEmail))
						{
							userRepo.InsertUserMaster(data.ContactEmail, "officemate");
						}
						return RedirectToAction("CreateCustomerSuccess", new { customerId = data.CustomerId });
					}
				}
				TempData["ErrorMessage"] = "กรุณาระบุรหัสลูกค้าและ e-Mail แล้วกด check ก่อนค่ะ";
			}

			data.ProvinceList = new ProvinceEproRepository(new SqlConnection(ConnectionString)).GetProvince();
			data.PaymentTypeList = new UserRepository(new SqlConnection(ConnectionString)).GetPaymentType();
			return View(data);
		}
		private void InsertCustomer(NewCustMasterData data, string connectionString, out int contactid, out int shippingid)
		{
			using (SqlConnection connection = new SqlConnection(connectionString))
			{
				OfficeMate.Framework.CustomerData.Customer c = new OfficeMate.Framework.CustomerData.Customer();
				c.Name = data.InvAddr1;
				c.InvoiceAddress1 = data.InvAddr1;
				c.InvoiceAddress2 = data.InvAddr2 ?? string.Empty;
				c.InvoiceAddress3 = data.InvAddr3 ?? string.Empty;
				c.InvoiceAddress4 = data.InvAddr4 ?? string.Empty;
				c.ActiveDate = DateTime.Now;
				c.CreateOn = DateTime.Now;
				c.CreditDiscount = data.DiscountRate;
				c.PaymentType = (OfficeMate.Framework.CustomerData.PaymentType)Enum.Parse(typeof(OfficeMate.Framework.CustomerData.PaymentType), data.PaymentSelected, true);
				c.PaymentCode = new OfficeMate.Framework.CustomerData.Repositories.PaymentRepository(connection).GetPaymentCode(c.PaymentType);

				OfficeMate.Framework.CustomerData.Contact co = new OfficeMate.Framework.CustomerData.Contact();
				co.Name = data.ContactName;
				co.MobileNumber = data.ContactMobileNumber;
				co.Email = data.ContactEmail;
				co.IsSubscribeNewsletter = true;
				co.IsDefault = true;

				OfficeMate.Framework.CustomerData.CustomerShipping s = new OfficeMate.Framework.CustomerData.CustomerShipping();
				s.Contactor = data.ShipContactor;
				s.ShipAddress1 = data.ShipAddress1;
				s.ShipAddress2 = data.ShipAddress2;
				s.ShipAddress3 = data.ShipAddress3 ?? String.Empty;
				s.ShipAddress4 = data.ProvinceSelected + " " + data.ShipAddress4;
				s.Province = data.ProvinceSelected;
				s.ZipCode = data.ShipAddress4;
				s.Area = "";
				s.PhoneNo = data.ShipPhoneNumber;
				s.IsDefault = true;

				new OfficeMate.Framework.CustomerData.Repositories.CustomerRepository(connection).InsertCustomerTemp(c, s, co, "EproV5", data.CustomerId);

				// out
				shippingid = s.Id;
				contactid = co.Id;
			}
		}
		private void InsertPhoneNewCustomer(NewCustMasterData data, int contactId, int shippingId, string connectionString)
		{
			using (SqlConnection conn = new SqlConnection(connectionString))
			{
				OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository repoPhone = new OfficeMate.Framework.CustomerData.Repositories.PhoneNumberRepository(conn);

				if (!string.IsNullOrEmpty(data.ContactPhoneNumber))
				{
					OfficeMate.Framework.CustomerData.PhoneNumber contactPhone = new OfficeMate.Framework.CustomerData.PhoneNumber();
					contactPhone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					contactPhone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
					contactPhone.CustomerId = data.CustomerId;
					contactPhone.SequenceId = contactId;
					contactPhone.PhoneNo = data.ContactPhoneNumber;
					contactPhone.Extension = !string.IsNullOrEmpty(data.ContactPhoneNumberExtension) ? data.ContactPhoneNumberExtension : string.Empty;
					repoPhone.InsertPhoneNumber(contactPhone, "EproV5");
				}

				if (!string.IsNullOrEmpty(data.ContactMobileNumber))
				{
					OfficeMate.Framework.CustomerData.PhoneNumber contactMobile = new OfficeMate.Framework.CustomerData.PhoneNumber();
					contactMobile.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					contactMobile.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.Mobile;
					contactMobile.CustomerId = data.CustomerId;
					contactMobile.SequenceId = contactId;
					contactMobile.PhoneNo = data.ContactMobileNumber;
					contactMobile.Extension = string.Empty;
					repoPhone.InsertPhoneNumber(contactMobile, "EproV5");
				}

				if (!string.IsNullOrEmpty(data.ContactFaxNumber))
				{
					OfficeMate.Framework.CustomerData.PhoneNumber contactFax = new OfficeMate.Framework.CustomerData.PhoneNumber();
					contactFax.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Contact;
					contactFax.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.FaxOut;
					contactFax.CustomerId = data.CustomerId;
					contactFax.SequenceId = contactId;
					contactFax.PhoneNo = data.ContactFaxNumber;
					contactFax.Extension = string.Empty;
					repoPhone.InsertPhoneNumber(contactFax, "EproV5");
				}

				if (!string.IsNullOrEmpty(data.ShipPhoneNumber))
				{
					OfficeMate.Framework.CustomerData.PhoneNumber shippingPhone = new OfficeMate.Framework.CustomerData.PhoneNumber();
					shippingPhone.TypeDataSource = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneDataSourceType.Shipping;
					shippingPhone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
					shippingPhone.CustomerId = data.CustomerId;
					shippingPhone.SequenceId = shippingId;
					shippingPhone.PhoneNo = data.ShipPhoneNumber;
					shippingPhone.Extension = !string.IsNullOrEmpty(data.ShipPhoneNumberExtension) ? data.ShipPhoneNumberExtension : string.Empty;
					repoPhone.InsertPhoneNumber(shippingPhone, "EproV5");
				}
			}
		}
		public ActionResult CreateCustomerSuccess(string customerId)
		{
			NewSiteData data = new UserRepository(new SqlConnection(ConnectionString)).GetCustDataFromMasterByCustID(customerId);
			return View("CreateNewSite", data);
		}

		public ActionResult EditOrderStatus(string orderGuid, string remark, bool? isOrderDeleted)
		{
			if (isOrderDeleted.HasValue)
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					OrderRepository orderRePo = new OrderRepository(connection);
					OrderData orderData = new OrderData();
					string orderId = orderRePo.GetOrderIdByGuid(orderGuid);//หาค่า OrderID จาก GuId ที่ส่งมาจาก Query String
					orderRePo.UpdateOrderEditByOFMAdmin(orderId, User.UserId);
					orderData.Order = orderRePo.GetMyOrder(orderId);
					orderData.CurrentOrderActivity = SetActivityForDelete(orderData.Order, remark);
					orderRePo.CreateOrderActivity(orderData.CurrentOrderActivity);
					new EmailRepository(new SqlConnection(ConnectionString)).AddOrderProcessLog(orderId, "Change OrderStatus", "เปลี่ยนจากสถานะ " + orderData.Order.OldStatus + " เป็น " + orderData.Order.Status, IPAddress, User.UserId);
				}
			}
			return RedirectToAction("ViewOrderDetail", new { guid = orderGuid });
		}

		private OrderActivity SetActivityForDelete(Order order, string remark)
		{
			OrderActivity orderActivity = new OrderActivity();
			orderActivity.OrderId = order.OrderID;
			orderActivity.ActivityTName = "ใบสั่งซื้อถูกยกเลิก";
			orderActivity.ActivityEName = "Deleted Order";
			orderActivity.Remark = remark;
			orderActivity.IPAddress = IPAddress;
			orderActivity.CreateBy = User.UserId;
			return orderActivity;
		}

		[HttpGet]
		public ActionResult CreateCustCompany()
		{
			return View(new NewCustCompany());
		}

		[HttpPost]
		public ActionResult CreateCustCompany(string companyId, string[] custId, NewCustCompany data, ImageInputData action)
		{
			if (action.Name == "Search")
			{
				if (string.IsNullOrEmpty(companyId))
				{
					TempData["ErrorCompanyId"] = "กรุณากรอกรหัสองค์กรด้วยค่ะ";
				}
				else
				{
					using (SqlConnection connection = new SqlConnection(ConnectionString))
					{
						CompanyRepository compRepo = new CompanyRepository(connection);
						if (compRepo.IsCompanyIdInSystem(companyId))
						{
							data = new UserRepository(connection).GetCustCompany(companyId);
						}
						else
						{
							TempData["ErrorCompanyId"] = "รหัสองค์กรไม่ถูกต้องค่ะ";
						}
					}
				}
			}
			else if (action.Name == "CreateCustId")
			{
				using (SqlConnection connection = new SqlConnection(ConnectionString))
				{
					UserRepository userRepo = new UserRepository(connection);
					CompanyRepository compRepo = new CompanyRepository(connection);
					data = userRepo.GetCustCompany(companyId);
					data.CustId = custId;
					if (!custId.Any())
					{
						TempData["ErrorCustId"] = "กรุณากรอกรหัสลูกค้าด้วยค่ะ";
					}
					else if (custId.Any(c => !userRepo.CustIdExistMasterUser(c)))
					{
						TempData["ErrorCustId"] = "รหัสลูกค้านี้ไม่มีข้อมูลในระบบค่ะ";
					}
					else if (custId.Any(c => !userRepo.IsDefaultEmailForCustId(c, data.UserId)))
					{
						TempData["ErrorCustId"] = "email ของผู้ดูแลระบบไม่ถูกต้องค่ะ";
					}
					else if (custId.Any(c => compRepo.IsCustExistCustCompany(c)))
					{
						TempData["ErrorCustId"] = "รหัสลูกค้านี้มีข้อมูลในระบบแล้วค่ะ";
					}
					else
					{
						data.ListUser = userRepo.GetUserForCustId(data.DefaultCustId, companyId, data.UserId);
						foreach (var item in custId)
						{
							try
							{
								compRepo.InsertCustCompany(item, companyId, User.UserId, User.UserCompanyDefault.No);
								foreach (var createNewUser in data.ListUser)
								{
									if (!userRepo.IsEmailInCustId(item, createNewUser.Email)) //custid ที่ได้มา มี default email เดียวกันกับ rootadmin หรือไม่
									{
										OfficeMate.Framework.CustomerData.Contact contact = new OfficeMate.Framework.CustomerData.Contact();
										OfficeMate.Framework.CustomerData.PhoneNumber phone;
										List<OfficeMate.Framework.CustomerData.PhoneNumber> itemPhone = new List<OfficeMate.Framework.CustomerData.PhoneNumber>();

										//Create Contact
										contact.Email = createNewUser.Email;
										contact.CustomerId = item;
										contact.Name = createNewUser.ThaiName;
										contact.FaxNumber = createNewUser.Fax;
										contact.MobileNumber = createNewUser.Mobile;

										phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
										phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.PhoneOut;
										phone.PhoneNo = createNewUser.Phone;
										phone.Extension = !string.IsNullOrEmpty(createNewUser.PhoneExt) ? createNewUser.PhoneExt : "";
										phone.IsDefault = true;
										itemPhone.Add(phone);

										if (!string.IsNullOrEmpty(createNewUser.Mobile))
										{
											phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
											phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.Mobile;
											phone.PhoneNo = createNewUser.Mobile;
											phone.IsDefault = true;
											itemPhone.Add(phone);
										}

										if (!string.IsNullOrEmpty(createNewUser.Fax))
										{
											phone = new OfficeMate.Framework.CustomerData.PhoneNumber();
											phone.PhoneNumberType = OfficeMate.Framework.CustomerData.PhoneNumber.PhoneType.FaxOut;
											phone.PhoneNo = createNewUser.Fax;
											phone.IsDefault = true;
											itemPhone.Add(phone);
										}

										contact.Phones = itemPhone;
										new OfficeMate.Framework.CustomerData.Repositories.ContactRepository(connection).InsertContact(contact, "EproV5"); //Insert Contact
									}
								}
							}
							catch (Exception)
							{
								throw;
							}

						}
						TempData["ErrorCustId"] = "บันทึกข้อมูลเรียบร้อยแล้วค่ะ";
					}
				}
			}
			return View(data);
		}

		public ActionResult ManageBudget(ImageInputData action, EditBudget budget)
		{
			switch (action.Name)
			{
				case "SearchBudget":
					return ShowBudget(budget);
				case "EditBudget":
					return EditBudget(budget);
				case "SaveEditBudget":
					return SaveEditBudget(budget);
				default:
					return BudgetGuide();
			}
		}

		private ActionResult BudgetGuide()
		{
			return View("Budget.Guid", new EditBudget());
		}

		private ActionResult ShowBudget(EditBudget budget)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				BudgetRepository budgetRepo = new BudgetRepository(connection);
				CompanyRepository companyRepo = new CompanyRepository(connection);
				EditBudget editBudget = new EditBudget();
				if (string.IsNullOrEmpty(budget.CompanyId))
				{
					TempData["ErrorCompanyId"] = "กรุณากรอกรหัสองค์กรด้วยค่ะ";
				}
				else
				{
					if (companyRepo.IsCompanyIdInSystem(budget.CompanyId))
					{
						editBudget.Company = companyRepo.GetCompany(budget.CompanyId);
						if (editBudget.Company.OrderControlType == Eprocurement2012.Models.Company.OrderControl.ByBudgetAndOrder)
						{
							editBudget.ItemBudget = budgetRepo.GetBudgetByCompanyIdAndGroupId(editBudget.Company.BudgetLevelType, editBudget.Company.BudgetPeriodType, budget.CompanyId, budget.GroupId);
						}
						else
						{
							TempData["ErrorMessage"] = "รหัสองค์กรนี้ไม่ได้คุม Budget";
						}
					}
					else
					{
						TempData["ErrorMessage"] = "รหัสองค์กรนี้ไม่มีข้อมูลในระบบค่ะ";
					}
				}
				return View("Budget.Guid", editBudget);
			}
		}

		private ActionResult EditBudget(EditBudget budget)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				BudgetRepository budgetRepo = new BudgetRepository(connection);
				CompanyRepository companyRepo = new CompanyRepository(connection);
				EditBudget editBudget = new EditBudget();
				editBudget.Company = companyRepo.GetCompany(budget.CompanyId);
				editBudget.Budget = budgetRepo.GetBudgetByGroupId(editBudget.Company.BudgetLevelType, editBudget.Company.BudgetPeriodType, budget.CompanyId, budget.GroupId);
				return View("EditBudget", editBudget);
			}
		}

		private ActionResult SaveEditBudget(EditBudget budget)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				BudgetRepository budgetRepo = new BudgetRepository(connection);
				CompanyRepository companyRepo = new CompanyRepository(connection);
				EditBudget editBudget = new EditBudget();
				editBudget.Company = companyRepo.GetCompany(budget.CompanyId);
				editBudget.Budget = budgetRepo.GetBudgetByGroupId(editBudget.Company.BudgetLevelType, editBudget.Company.BudgetPeriodType, budget.CompanyId, budget.GroupId);
				editBudget.NewBudgetAmt = budget.NewBudgetAmt;
				editBudget.IsCreateBudgetAmt = budget.IsCreateBudgetAmt;
				budgetRepo.UpdateBudgetByAddRemainAmt(editBudget, User.UserId);
				return ShowBudget(budget);
			}
		}

		private ActionResult SetRootAdmin(string guid, string companyId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				ModelViewListUserData data = new ModelViewListUserData();
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				string userId = userRepo.GetUserIdFromUserGuid(guid);

				data.UserData = userRepo.GetAllUser(companyId, true);

				string userAdmin = data.UserData.Where(m => m.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin).FirstOrDefault().UserId;
				if (costcenterRepo.IsAdminApproveInOrder(companyId, userAdmin))
				{
					TempData["ErrorSetRootAdmin"] = new ResourceString("ErrorMessage.SetRootAdmin");
				}
				else
				{
					userRepo.UpdateRootAdmin(data, companyId, userId);
					data.ProcessLog = SetRootAdminForAdmin(data, userId);
					userRepo.AddRootAdminProcessLog(data.ProcessLog, User.UserId);
				}
				return ViewAllAdmin(companyId);
			}
		}

		private ProcessLog SetRootAdminForAdmin(ModelViewListUserData data, string userId)
		{
			ProcessLog process = new ProcessLog();
			process.CompanyId = data.UserData.First().CurrentCompanyId;
			process.ProcessRemark = "Change RootAdmin";
			process.Remark = "เปลี่ยนจาก " + data.UserData.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin).FirstOrDefault().UserId + " เป็น " + userId;
			process.IPAddress = IPAddress;
			process.CreateBy = User.UserId;
			return process;
		}

		private ProcessLog SetDeleteUserOFMForCreateNewSiteAdmin(ModelViewListUserData data, string userId)
		{
			ProcessLog process = new ProcessLog();
			process.CompanyId = data.UserData != null ? data.UserData.First().CurrentCompanyId : "";
			process.ProcessRemark = "Delete UserOFM";
			process.Remark = "ลบข้อมูลผู้ใช้งาน " + userId + " ออกจากระบบ Officemate";
			process.IPAddress = IPAddress;
			process.CreateBy = User.UserId;
			return process;
		}

		public ActionResult SearchMail(SearchMail searchMail, string fromDate, string toDate, string timeFrom, string timeTo)
		{
			if (searchMail == null) { return View(new SearchMail()); }
			DateTime fromDateTime, toDateTime;
			if (!String.IsNullOrEmpty(fromDate) && DateTime.TryParseExact(fromDate + " " + timeFrom, "d/M/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out fromDateTime))
			{
				searchMail.DateFrom = fromDateTime;
			}
			if (!String.IsNullOrEmpty(toDate) && DateTime.TryParseExact(toDate + " " + timeTo, "d/M/yyyy HH:mm", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out toDateTime))
			{
				searchMail.DateTo = toDateTime;
			}
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				EmailRepository mailRepo = new EmailRepository(connection);
				if (searchMail.CheckEmailTo || searchMail.CheckEmailCC || searchMail.CheckOrderId || searchMail.CheckMailCategory || searchMail.CheckDate || searchMail.CheckMailSuccess)
				{
					searchMail.MailTransLog = mailRepo.GetAdvanceSearchMail(searchMail);
				}
				else
				{
					return View(new SearchMail());
				}
				if (!searchMail.MailTransLog.Any())
				{
					TempData["ErrorMessage"] = "ไม่พบข้อมูลที่ต้องการ";
				}
				return View("SearchMail", searchMail);
			}
		}

		public ActionResult AutoCompleteSetRequesterLine(string companyId, int rows, string term)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				IEnumerable<string> data = userRepo.GetAllRequester(companyId, term);
				return Json(data.Take(rows),JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult CheckHasRequesterLine(string companyId, string costcenterId, string reqUserId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				var data = userRepo.GetDefaultRequester(companyId, costcenterId, reqUserId);
				return Json(data, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult CheckHasCostCenterInSystem(string companyId, string costcenterId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				var data = costcenterRepo.IsCostcenterIdInSystem(companyId, costcenterId);
				return Json(data, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult CheckHasUserIdInSystem(string companyId, string userId)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				UserRepository userRepo = new UserRepository(connection);
				var data = userRepo.IsUserInSystem(userId, companyId);
				return Json(data, JsonRequestBehavior.AllowGet);
			}
		}

		public ActionResult AutoCompleteSetRequesterLineForUser(string companyId, int rows, string term)
		{
			using (SqlConnection connection = new SqlConnection(ConnectionString))
			{
				CostCenterRepository costcenterRepo = new CostCenterRepository(connection);
				IEnumerable<string> data = costcenterRepo.GetAllCostCenterForRequesterLine(companyId, term);
				return Json(data.Take(rows), JsonRequestBehavior.AllowGet);
			}
		}



	}
}
