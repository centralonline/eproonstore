﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class ForgotPasswordData : MasterPageData
	{
		public string UserId { get; set; }
		public LocalizedString UserName { get; set; }
		public string UserGuid { get; set; }
		public string VerifyKey { get; set; }
	}
}