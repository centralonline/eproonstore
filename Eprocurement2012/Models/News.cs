﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using OfficeMate.Framework.Formatting;
using Eprocurement2012.Controllers.Helpers;
namespace Eprocurement2012.Models
{
	public class News : MasterPageData
	{
		public int NewsID { get; set; }
		public string NewsGUID { get; set; }
		public string CompanyID { get; set; }
		[Required(ErrorMessage = "Admin.EditNewsCompany.ErrorNewsTitleEmptry")]
		public string NewsTitle { get; set; }
		[Required(ErrorMessage = "Admin.EditNewsCompany.ErrorNewsDetailEmptry")]
		public string NewsDetail { get; set; }
		public string NewsType { get; set; }

		public DateTime ValidFrom { get; set; }
		public string ValidFromDisplay { get; set; }

		public DateTime ValidTo { get; set; }
		public string ValidToDisplay { get; set; }

		[Required(ErrorMessage = "Admin.EditNewsCompany.ErrorNewsPriorityEmptry")]
		public string Priority { get; set; }
		[Required(ErrorMessage = "Admin.EditNewsCompany.ErrorNewsNewsStatusEmptry")]
		public string NewsStatus { get; set; }
		public string CreateBy { get; set; }
		public string CreateGuid { get; set; }
		public string UpdateBy { get; set; }

		//ใช้แสดงข้อมูลหน้า View
		[Required(ErrorMessage = "Admin.EditNewsCompany.ErrorNewsPeriodEmptry")]
		public string Period { get; set; }
		public string DisplayNewsStatus
		{
			get
			{
				if (NewsStatus == "Active") { return new ResourceString("News.NewsStatus"); }
				return new ResourceString("News.Cancel");
			}
		}
		public string DisplayPriority
		{
			get
			{
				if (Priority == "Low") { return new ResourceString("News.PriorityLow"); }
				return new ResourceString("News.PriorityHight");
			}
		}
		public string DisplayNewsType
		{
			get
			{
				if (NewsType == "Public") { return new ResourceString("News.Officemate"); }
				return new ResourceString("News.Company");
			}
		}

	}
}

