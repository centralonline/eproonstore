﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class ProductSupplier
	{
		public Order Order { get; set; }
		public ProductDetail OrderDetail { get; set; }
		public string SupplierID { get; set; }
		public string SupplierName { get; set; }

		public string DisplaySupplier
		{
			get
			{
				return SupplierID + " " + SupplierName;
			}
		}
	}

	public class ProductDetail
	{
		public Product Product { get; set; }
		public decimal DiscountRate { get; set; }
		public int Quantity { get; set; }
		//public int OldQuantity { get; set; }
		public decimal IncVatPrice { get; set; }
		public decimal ExcVatPrice { get; set; }
		//public bool IsOfmCatalog { get; set; }

		//แสดงราคาสินค้าที่ลบส่วนลดแล้ว แต่ยังไม่ได้คูณ Quantity
		public decimal PriceDiscount
		{
			get
			{
				if (Product.IsBestDeal || Product.IsPromotion)
				{
					return 0;
				}
				else
				{
					return (Math.Round((ExcVatPrice * DiscountRate) / 100, 2, MidpointRounding.AwayFromZero));
				}
			}
		}

		public decimal ItemIncVatPrice
		{
			get
			{
				if (Product.IsVat)
				{
					return (Math.Round((ItemExcVatPrice * Convert.ToDecimal(1.07)), 2, MidpointRounding.AwayFromZero));
				}
				else
				{
					return ExcVatPrice - PriceDiscount;
				}
			}
		}

		public decimal ItemExcVatPrice
		{
			get
			{
				return ExcVatPrice - PriceDiscount;
			}
		}

		public decimal GrandTotal
		{
			get {
				return IncVatPrice * Quantity;
			}
		}

	}
}