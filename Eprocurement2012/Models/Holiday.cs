﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
    public class Holiday
    {
        public DateTime HolidayDate { get; set; }
        public string HolidayName { get; set; }
    }
}