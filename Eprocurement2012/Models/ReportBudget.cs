﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class ReportBudget : MasterPageData
	{
		public Company Company { get; set; }
		public IEnumerable<ReportBudgetData> Budgets { get; set; }
	}
	public class ReportBudgetData
	{
		public CompanyDepartment Department { get; set; }
		public CostCenter CostCenter { get; set; }
		public Budget Budget { get; set; }
		public Order Order { get; set; }
	}
}