﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class Budget
	{
		public string CompanyID { get; set; }
		public string GroupID { get; set; }
		public string Year { get; set; }
		public Period PeriodNo { get; set; }
		public decimal OriginalAmt { get; set; }
		public decimal BudgetAmt { get; set; }
		public decimal UsedAmt { get; set; }
		public decimal RemainBudgetAmt { get; set; }
		public decimal WaitingOrderAmt { get; set; }
		public decimal WaitingCurrentUserAmt { get; set; } //ใช้ใน View ExpandBudget แสดงจำนวนเงินที่รอการอนุมัติของ User ที่ใช้งานอยู่ในขณะนั้น
		public decimal CurrentUserApproveAmt { get; set; } //ใช้ใน View ExpandBudget แสดงจำนวนเงินที่อนุมัติแล้ว User ที่ใช้งานอยู่ในขณะนั้น
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }
		public DateTime UpdateOn { get; set; }
		public string UpdateBy { get; set; }
		//ใช้ค่าจาก User.CompanyInfo
		public Company.BudgetLevel BudgetLevel { get; set; }
		public Company.BudgetPeriod BudgetPeriod { get; set; }

		public string DisplayPeriodNo
		{
			get
			{
				switch (PeriodNo)
				{
					case Period.H1:
						return "ครึ่งปีแรก";
					case Period.H2:
						return "ครึ่งปีหลัง";
					case Period.Q1:
						return "ไตรมาศที่ 1";
					case Period.Q2:
						return "ไตรมาศที่ 2";
					case Period.Q3:
						return "ไตรมาศที่ 3";
					case Period.Q4:
						return "ไตรมาศที่ 4";
					case Period.M1:
						return "เดือนมกราคม";
					case Period.M2:
						return "เดือนกุมภาพันธ์";
					case Period.M3:
						return "เดือนมีนาคม";
					case Period.M4:
						return "เดือนเมษายน";
					case Period.M5:
						return "เดือนพฤษภาคม";
					case Period.M6:
						return "เดือนมิถุนายน";
					case Period.M7:
						return "เดือนกรกฎาคม";
					case Period.M8:
						return "เดือนสิงหาคม";
					case Period.M9:
						return "เดือนกันยายน";
					case Period.M10:
						return "เดือนตุลาคม";
					case Period.M11:
						return "เดือนพฤศจิกายน";
					case Period.M12:
						return "เดือนธันวาคม";
					default:
						return "ปี";
				}
			}
		}
		public enum Period
		{
			H1,
			H2,
			Q1,
			Q2,
			Q3,
			Q4,
			M1,
			M2,
			M3,
			M4,
			M5,
			M6,
			M7,
			M8,
			M9,
			M10,
			M11,
			M12,
			Y
		}
	}
}