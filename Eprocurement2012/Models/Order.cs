﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Controllers.Helpers;
using OfficeMate.Framework.Formatting;


namespace Eprocurement2012.Models
{
	public class Order : MasterPageData
	{

		public IEnumerable<OrderDetail> ItemOrders { get; set; }
		public string OrderID { get; set; }
		public string OrderGuid { get; set; }
		public string CustId { get; set; }
		public DateTime OrderDate { get; set; }
		public DateTime ApproveOrderDate { get; set; }
		public OrderStatus Status { get; set; }
		public OrderStatus OldStatus { get; set; }
		public Company Company { get; set; }
		public CompanyDepartment Department { get; set; }
		public CostCenter CostCenter { get; set; }
		public Shipping Shipping { get; set; }
		public Invoice Invoice { get; set; }
		public Contact Contact { get; set; }
		public User Requester { get; set; }
		public UserApprover CurrentApprover { get; set; }
		public UserApprover PreviousApprover { get; set; }
		public UserApprover NextApprover { get; set; }
		public GoodReceive GoodReceive { get; set; }
		public GoodReceiveDetail GoodReceiveDetail { get; set; }
		public int ItemCountOrder { get; set; }
		public string AttachFile { get; set; }
		public string ShipType { get; set; }
		public string RefInvNo { get; set; }
		public bool SendToAdminAppProd { get; set; } // ถ้าเป็น True แสดงว่า Cart นี้มีสินค้านอก ProductCatalog
		public bool SendToAdminAppBudg { get; set; } // ถ้าเป็น True แสดงว่า Cart นี้มียอดรวมเกิน Budget แผนก
		public bool UseSMSFeature { get; set; }// ส่ง SMS แจ้งเตือนการสั่งซื้อสินค้า
		public decimal CurrentBudget { get; set; } // ยอด budget คงเหลือที่ใช้ในการอนุมัติใบสั่งซื้อนี้ ใช้ในหน้า ViewDataOrder

		//สำหรับดึงไฟล์เอกสารแนบ
		public string CustFileName { get; set; }
		public string SystemFileName { get; set; }

		public decimal DiscountRate { get; set; }
		public decimal TotalAllDiscount { get; set; }
		public decimal TotalAllDiscountNonVat { get; set; }
		public decimal TotalAllDiscountExcVat { get; set; }

		public decimal TotalDeliveryFee { get; set; } //ราคาค่าขนส่งต่อ Order < 499  คิด 50 บาท  ลงที่ @OrdDeliverfee
		public decimal TotalDeliveryCharge { get; set; } //ราคาค่าขนส่งพิเศษ ByItem รับมาจาก IOrder ItemDeliveryFee ลงที่ @DeliverFee

		public decimal TotalPriceProductNonVat { get; set; } //ราคาสินค่าไม่คิด Vat
		public decimal TotalPriceProductNoneVatAmount { get; set; } //ราคาสินค่าไม่คิด Vat - ส่วนลด
		public decimal TotalPriceProductExcVat { get; set; } //ราคาสินค้าก่อน Vat
		public decimal TotalPriceProductExcVatAmount { get; set; } //ราคาสินค้าก่อน Vat - ส่วนลด

		public decimal TotalAmt { get; set; } // ลง ที่ TotAmt
		public decimal TotalNetAmt { get; set; } //ลงที่ NetAmt
		public decimal TotalVatAmt { get; set; } //ลงที่ VatAmt
		public decimal GrandTotalAmt { get; set; } // ลงที่ GrandTotalAmt ค่าใช้จ่ายทั้งหมดที่ต้องชำระ

		public int NumOfAdjust { get; set; }
		public string ApproverRemark { get; set; }
		public string OFMRemark { get; set; }
		public string ReferenceRemark { get; set; }
		public string StatusRemark { get; set; }
		public string CallBackRequest { get; set; }
		public string CallBackRequestText
		{
			get
			{
				if (CallBackRequest.ToUpper() == "NO") { return ""; }
				else if (CallBackRequest.ToUpper() == "YES") { return new ResourceString("CartDetail.CallBackRequestStatus"); }
				else { return ""; }
			}
		}
		public int ParkDay { get; set; }
		public bool IsEdit { get; set; }
		public bool AdminAllowFlag { get; set; }
		public DateTime ExpireDate { get; set; }
		public DateTime WarningExpireDate { get; set; }
		public DateTime CompleteMailDate { get; set; }

		//สำหรับแสดงหน้า view
		public string DisplayApproveOrderDate
		{
			get
			{
				if (ApproveOrderDate.ToString(new DateTimeFormat()) == "01-01-1900 00:00")
				{
					return "-";
				}
				else
				{
					return ApproveOrderDate.ToString(new DateTimeFormat());
				}

			}
		}
		
		public enum OrderStatus
		{
			Waiting,
			Approved,
			Partial,
			Revise,
			WaitingAdmin,
			AdminAllow,
			Shipped,
			Deleted,
			Expired,
			Completed
		}

		public enum DownloadStatus
		{
			Complete,
			Waiting,
			Process,
			Cancel,
			Download
		}

		public List<AttachFileList> Files = new List<AttachFileList>
		{
			new AttachFileList{Id="file1",Name = ".xls"},
			new AttachFileList{Id="file2",Name = ".xlsx"},
			new AttachFileList{Id="file3",Name = ".doc"},
			new AttachFileList{Id="file4",Name = ".docx"},
			new AttachFileList{Id="file5",Name = ".pdf"}
		};

	}

	public class AttachFileList
	{
		public string Id { get; set; }
		public string Name { get; set; }
	}
}