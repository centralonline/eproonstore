﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Eprocurement2012.Models
{
	public class CartDetail : MasterPageData
	{
		public Product Product { get; set; }
		public int Quantity { get; set; }
		public decimal DiscountRate { get; set; }
		public string ReferenceCode { get; set; }
		public bool IsOfmCatalog { get; set; }
		public decimal ItemPriceVat
		{
			get
			{
				return Math.Round((((Product.PriceExcVat * Quantity) - ItemPriceDiscount) * 7) / 100, 2);
			}
		}
		public decimal ItemPriceDiscount
		{
			get
			{
				if (Product.IsBestDeal || Product.IsPromotion)
				{
					return 0;
				}
				else
				{
					return (Math.Round((Product.PriceExcVat * DiscountRate) / 100, 2, MidpointRounding.AwayFromZero)) * Quantity;
				}
			}

		}
		public decimal ItemPriceNet
		{
			get
			{
				return (Product.PriceExcVat * Quantity) - ItemPriceDiscount;
			}
		}
		public enum OfmCatalog
		{
			Yes,
			No
		}

		//แสดงราคาสินค้าที่ลบส่วนลดแล้ว แต่ยังไม่ได้คูณ Quantity
		public decimal PriceDiscount
		{
			get
			{
				if (Product.IsBestDeal || Product.IsPromotion)
				{
					return 0;
				}
				else
				{
					return (Math.Round((Product.PriceExcVat * DiscountRate) / 100, 2, MidpointRounding.AwayFromZero));
				}
			}
		}

		public decimal ItemIncVatPrice
		{
			get
			{
				if (Product.IsVat)
				{
					return (Math.Round((ItemExcVatPrice * Convert.ToDecimal(1.07)), 2, MidpointRounding.AwayFromZero));
				}
				else
				{
					return ItemExcVatPrice;
				}
			}
		}

		public decimal ItemExcVatPrice
		{
			get
			{
				return Product.PriceExcVat - PriceDiscount;
			}
		}
	}
}