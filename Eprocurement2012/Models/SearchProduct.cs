﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class SearchProduct : MasterPageData
	{
		public DateTime CreateDateFrom { get; set; }
		public DateTime CreateDateTo { get; set; }
		public string ProductIdFrom { get; set; }
		public string ProductIdTo{ get; set; }
		public string CompanyId { get; set; }
		public string CreateBy { get; set; }
		public string CatalogTypeId { get; set; }
		public IEnumerable<ProductCatalog> SearchProductData { get; set; }

		//Check Box
		public bool CheckCreateProduct { get; set; }
		public bool CheckProductId { get; set; }
		public bool CheckCompanyId { get; set; }
		public bool CheckCreateBy { get; set; }
		public bool CheckCatalogTypeId { get; set; }
	}
}