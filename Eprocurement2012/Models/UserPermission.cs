﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class UserPermission
	{
		public CostCenter CostCenter { get; set; }
		public string CustId { get; set; }
		public User.UserRole RoleName { get; set; }
		public LocalizedString DisplayRoleName { get; set; }
		public LocalizedString DisplayUserName { get; set; }
		public string UserId { get; set; }
		public DateTime CreateOn { get; set; }
		public int AppLevel { get; set; }
		public int Parkday { get; set; }
		public decimal AppCreditlimit { get; set; }
	}
}