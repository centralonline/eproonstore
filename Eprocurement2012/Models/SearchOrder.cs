﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class SearchOrder : MasterPageData
	{
		public IEnumerable<Order> Orders { get; set; }
		public IEnumerable<StringForSelectList> ListOrderSearch = new List<StringForSelectList>
		{
			new StringForSelectList {Id="OrderID",Name=new ResourceString("Order.ViewDataOrder.OrderId")},
			new StringForSelectList {Id="ReqName",Name=new ResourceString("Order.ViewOrder.Requester")},
			new StringForSelectList {Id="AppName",Name=new ResourceString("Order.ViewOrder.Approver")},
			new StringForSelectList {Id="CostName",Name=new ResourceString("Order.ViewOrder.CostCenterName")}
		};
	}
}