﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class EditBudget : MasterPageData
	{
		public Budget Budget { get; set; }
		public string CompanyId { get; set; }
		public string GroupId { get; set; }
		public decimal NewBudgetAmt { get; set; }
		public bool IsCreateBudgetAmt { get; set; }
		public IEnumerable<Budget> ItemBudget { get; set; }
		//สำหรับดึงข้อมูล บริษัท เพื่อนำ BudgetLevel มาใช้งาน
		public Company Company { get; set; }
	}
}