﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class OrderActivityTransLog : MasterPageData
	{
		public IEnumerable<OrderTransLog> OrderTransLogs { get; set; }
		public IEnumerable<OrderDetailTransLog> OrderDetailTransLogs { get; set; }
	}
	public class OrderTransLog
	{
		public string OrderID { get; set; }
		public DateTime ActionDate { get; set; }
		public string ActionUserID { get; set; }
		public LocalizedString ActionName { get; set; }
		public string ActionTDesc { get; set; }
		public string ActionEDesc { get; set; }
		public string BeforeStatus { get; set; }
		public string AfterStatus { get; set; }
		public string ActionRemark { get; set; }

		//แสดงหน้า view
		public string DisplayBeforeStatus
		{
			get
			{
				if (BeforeStatus == "Waiting")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Waiting");
				}
				else if (BeforeStatus == "Revise")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Revise");
				}
				else if (BeforeStatus == "Approved")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Approved");
				}
				else if (BeforeStatus == "AdminAllow")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.AdminAllow");
				}
				else if (BeforeStatus == "Completed")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Completed");
				}
				else if (BeforeStatus == "Deleted")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Deleted");
				}
				else if (BeforeStatus == "WaitingAdmin")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.WaitingAdmin");
				}
				else if (BeforeStatus == "Partial")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Partial");
				}
				else if (BeforeStatus == "Shipped")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Shipped");
				}
				else
				{
					return "-";
				}
				
			}
		}
		public string DisplayAfterStatus
		{
			get
			{
				if (AfterStatus == "Waiting")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Waiting");
				}
				else if (AfterStatus == "Revise")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Revise");
				}
				else if (AfterStatus == "Approved")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Approved");
				}
				else if (AfterStatus == "AdminAllow")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.AdminAllow");
				}
				else if (AfterStatus == "Completed")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Completed");
				}
				else if (AfterStatus == "Deleted")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Deleted");
				}
				else if (AfterStatus == "WaitingAdmin")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.WaitingAdmin");
				}
				else if (AfterStatus == "Partial")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Partial");
				}
				else if (AfterStatus == "Shipped")
				{
					return new ResourceString("Eprocurement2012.Models.Order+OrderStatus.Shipped");
				}
				else
				{
					return "-";
				}

			}
		}
	}

	public class OrderDetailTransLog
	{
		public string OrderID { get; set; }
		public string EditBy { get; set; }
		public string PID { get; set; }
		public DateTime EditOn { get; set; }
		public int OldQty { get; set; }
		public int EditQty { get; set; }
		public int NewQty { get; set; }
		public LocalizedString ActionEditName { get; set; }
		public string EditTReason { get; set; }
		public string EditEReason { get; set; }
		public string EditRemark { get; set; }
	}
}