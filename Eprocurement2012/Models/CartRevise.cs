﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class CartRevise : MasterPageData
	{
		public Cart Cart{ get; set; }
		public Order Order { get; set; }
		public IEnumerable<OrderActivity> OrderActivity { get; set; }
		public OrderActivity CurrentOrderActivity { get; set; }
		public CostCenter CurrentUsedCostCenter { get; set; }
		public UserApprover CurrentSelectApprover { get; set; }
		public Budget CurrentBudget { get; set; }
		public string ApproverRemark { get; set; }
		public string OFMRemark { get; set; }
		public string ReferenceRemark { get; set; }
	}
}