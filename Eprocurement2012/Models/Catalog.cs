﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class Catalog: MasterPageData
	{
		public IEnumerable<Product> CatalogDetail { get; set; }
		public CatalogData CatalogData { get; set; }
		public string CatalogID { get; set; }
		public string UserID { get; set; }
		public string CompanyID { get; set; }
		public string CatalogName { get; set; }
		public string CatalogDesc { get; set; }
		public int CountProduct { get; set; }
		public string GUID { get; set; }
		public string IsDefault { get; set; }
		public CatalogStatus Status { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }
		public DateTime UpdateOn { get; set; }
		public string UpdateBy { get; set; }


		public string AddByPID { get; set; }

		public enum CatalogStatus
		{
			Active,
			Delete
		}

		public string IsCheck
		{
			get
			{
				if (IsDefault=="Yes")
				{
					return "checked";
				}
				return "";
			}
		}

	}

	//public class CatalogDetail
	//{
	//    public string CatalogID { get; set; }
	//    //public int SeqNo { get; set; }
	//    public string ProductID { get; set; }
	//    public string CreateOn { get; set; }
	//    public string CreateBy { get; set; }
	//    public Product Product { get; set; }
	//}
}