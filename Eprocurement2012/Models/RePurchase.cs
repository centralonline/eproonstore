﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class RePurchase : MasterPageData
	{
		public string OrderGuid { get; set; }
		public string OrderId { get; set; }
		public IEnumerable<Order> Orders { get; set; }
		public IEnumerable<Product> Products { get; set; }
	}
}