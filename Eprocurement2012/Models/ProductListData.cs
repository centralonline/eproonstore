﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Models
{
	public class ProductListData : DepartmentViewData
	{
		public IEnumerable<Product> Products { get; set; }
		public IEnumerable<Product> ProductsOFM { get; set; }
		public DepartmentSortByType SortBy { get; set; }
		public IEnumerable<Department> SimilarDepartment { get; set; }
		public IDictionary<string, IEnumerable<Product>> ProductsOfRelateDept { get; set; }
		public bool SortByDept { get; set; }
		public bool IsPromotion { get; set; }
		public bool GroupByDeptSupply { get; set; }
		public int PageSize { get; set; }
		public int PageNumber { get; set; }
		public int TotalItemCount { get; set; }
		public IEnumerable<SelectListItem> MyCatalogs { get; set; }
	}

	public class OfficeSupplyProductListData : ProductListData
	{
		public IDictionary<string, IEnumerable<Product>> SKUs { get; set; }
		public IDictionary<string, IEnumerable<Product>> OFMSKUs { get; set; }
	}

	public class GroupPropertyProductListData : ProductListData
	{
		public IDictionary<string, IEnumerable<Product>> PropertyProducts { get; set; }
		public IEnumerable<PropertyDept> PropertyDepartmentSelected { get; set; }
	}
}