﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class Contact
	{
		public string Email { get; set; }
		public int ContactID { get; set; }
		public int OfflineContactID { get; set; }
		public int ContactSeqNo { get; set; }
		public string ContactorName { get; set; }

		public string ContactorPhone { get; set; }
		public string ContactorExtension { get; set; }
		public string ContactorFax { get; set; }
		public string ContactMobileNo { get; set; }

		public Phone Phone { get; set; }
		public Phone Mobile { get; set; }
		public Phone Fax { get; set; }

		public enum TypedataSource
		{
			Contact,
			Prospect,
			Shipping
		}
		public enum Isdefault
		{
			Yes,
			No
		}
		public enum TypeNumber
		{
			FaxOut,
			FaxIn,
			PhoneIn,
			Mobile,
			PhoneOut
		}
	}
}