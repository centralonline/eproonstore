﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Eprocurement2012.Models
{
	public class Cart : MasterPageData
	{
		
		public IEnumerable<CartDetail> ItemCart { get; set; }
		public int ItemCountCart { get; set; }
		public int ShoppingCartId { get; set; }
		public string RefOrderId { get; set; }
		public CartType Type { get; set; }
		public string CustFileName { get; set; }
		public string SystemFileName { get; set; }
		public decimal TotalPriceProductIncVat { get; set; }	//PriceIncVat(Product) * Qty
		public decimal TotalPriceProductExcVat { get; set; }	//PriceExcVat(Product) * Qty  and  isvat = Ture
		public decimal TotalPriceProductNonVat { get; set; }	//PriceExcVat(Product) * Qty  and  isvat = False
		public decimal TotalAdditionalAmount { get { return 0; } } //ค่าใช้จ่ายพิเศษต่างๆที่เพิ่มมา เช่น ค่าห่อของขวัญ
		public decimal TotalAmt // ลง ที่ TotAmt
		{
			get
			{
				return Math.Round(TotalPriceProductExcVat + TotalPriceProductNonVat + TotalDeliveryAmount, 2);
			}
		}
		public decimal TotalPriceNet //ลงที่ NetAmt
		{
			get
			{
				return Math.Round(TotalPriceProductExcVat + TotalPriceProductNonVat + TotalDeliveryAmount - TotalAllDiscount, 2);
			}
		}
		public decimal TotalVatAmt //ลงที่ VatAmt
		{
			get
			{
				if (TotalPriceProductExcVat <= 0) { return 0; }
				return Math.Round(((TotalPriceProductExcVat - Math.Abs(TotalAllDiscountExcVat)) * 7) / 100, 2);
			}
		}
		public decimal VatRate { get { return 7; } } // Default ภาษี = 7% ลงที่ VatRate
		public decimal GrandTotalAmt // ลงที่ GrandTotalAmt ค่าใช้จ่ายทั้งหมดที่ต้องชำระ
		{
			get
			{
				return Math.Round((TotalPriceProductExcVat + TotalPriceProductNonVat + TotalVatAmt + TotalDeliveryAmount) - Math.Abs(TotalAllDiscount) + TotalAdditionalAmount, 2);
			}

		}
		public decimal OriginalTotalNetExcVatAmt
		{
			get
			{
				return Math.Round((TotalPriceProductExcVat + TotalPriceProductNonVat + TotalDeliveryAmount) - Math.Abs(TotalAllDiscount) + TotalAdditionalAmount, 2);
			}
		}
		public decimal OriginalTotalNetIncVatAmt
		{
			get
			{
				return Math.Round((TotalPriceProductExcVat + TotalPriceProductNonVat + TotalVatAmt + TotalDeliveryAmount) - Math.Abs(TotalAllDiscount) + TotalAdditionalAmount, 2);
			}
		}

		public decimal TotalAllDiscountExcVat { get; set; }

		private decimal _TotalAllDiscount;
		public decimal TotalAllDiscount
		{
			get
			{
				return _TotalAllDiscount;
			}
			set
			{
				_TotalAllDiscount = value;
			}
		}
		public decimal TotalAllDiscountNonVat { get; set; }

		public decimal DisCountRate { get; set; } // %ส่วนลดของลูกค้า

		public decimal TotalDeliveryFee //ราคาค่าขนส่งต่อ Order < 499  คิด 50 บาท  ลงที่ @OrdDeliverfee
		{
			get
			{
				return 0;
			}
		}
		public decimal TotalDeliveryCharge { get; set; }//ราคาค่าขนส่งพิเศษ ByItem รับมาจาก IOrder ItemDeliveryFee ลงที่ @DeliverFee
		public decimal TotalDeliveryAmount
		{
			get
			{
				return Math.Round(TotalDeliveryFee + TotalDeliveryCharge, 2);
			}
		}

		public bool SendToAdminAppProd { get; set; } // ถ้าเป็น True แสดงว่า Cart นี้มีสินค้านอก ProductCatalog => Order จะถูกส่งไปหา Admin
		public bool SendToAdminAppBudg { get; set; } // ถ้าเป็น True แสดงว่า Cart นี้มียอดรวมเกิน Budget แผนก => Order จะถูกส่งไปหา Admin

		public bool IsOverCreditlimit { get; set; } // ถ้าเป็น True แสดงว่า Cart นี้มียอดรวมเกินวงเงินของผู้อนุมัติท่านสุดท้าย => ต้องเพิ่มวงเงินก่อนจึงจะสร้าง Order ได้

		public int NumOfAdjust { get; set; }

		public DateTime WarningExpireDate { get; set; }
		public DateTime ExpireDate { get; set; }

		public decimal TotalPriceProductWithDisCountAmount
		{
			get
			{
				return TotalPriceProductExcVat + TotalPriceProductNonVat - TotalAllDiscount;
			}

		}
		public decimal TotalPriceProductExcVatWithDisCountAmount
		{
			get
			{
				if (TotalPriceProductExcVat <= 0) { return 0; }
				return TotalPriceProductExcVat - TotalAllDiscountExcVat;
			}

		}
		public decimal TotalPriceProductNoneVatWithDisCountAmount
		{
			get
			{
				if (TotalPriceProductNonVat <= 0) { return 0; }
				return TotalPriceProductNonVat - TotalAllDiscountNonVat;
			}

		}

		public enum CartType
		{
			New,
			Revise,
			Repurchase
		}

	}

}