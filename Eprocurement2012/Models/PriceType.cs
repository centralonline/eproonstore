﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeMate.Framework;

namespace Eprocurement2012.Models
{
	public enum PriceType
	{
		[EnumValue("fix")]
		Fix,
		[EnumValue("float")]
		Float,
		[EnumValue("")]
		None //ป้องกัน error ในกรณีที่มี columns แต่ ไม่มีการส่งค่า
	}
}