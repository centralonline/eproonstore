﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class FinalReview : MasterPageData
	{
		public Company FinalCompany { get; set; }
		public IEnumerable<CompanyDepartment> CompanyDepartment { get; set; }
		public IEnumerable<CostCenter> CostCenter { get; set; }
		public IEnumerable<User> UserPurchase { get; set; }
		public IEnumerable<User> UserAdmin { get; set; }
		public IEnumerable<InformationRequesterLine> RequesterLineDetail { get; set; }
		public IEnumerable<Invoice> Invoice { get; set; }
	}
}