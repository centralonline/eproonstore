﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public sealed class ImageData
	{
		public byte[] Bytes { get; set; }
		public string ContentType { get; set; }
	}
}