﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class EprocurementMasterPageData : MasterPageData
	{
		public IEnumerable<Department> SiteDirectory { get; set; }
	}
}