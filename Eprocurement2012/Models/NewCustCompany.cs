﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class NewCustCompany : MasterPageData
	{
		public NewCustCompany()
		{
			CustId = new string[0] { };
		}

		public string[] CustId { get; set; }
		public IEnumerable<CreateNewUser> ListUser { get; set; }

		//Company
		public string CompanyId { get; set; }
		public LocalizedString CompanyName { get; set; }
		public string CompanyThaiName { get; set; }
		public string CompanyEngName { get; set; }
		public string DefaultCustId { get; set; }

		//User
		public string UserId { get; set; }
		public LocalizedString DisplayName { get; set; }
		public string UserThaiName { get; set; }
		public string UserEngName { get; set; }

		//Invoice
		public string InvAddr1 { get; set; }
		public string InvAddr2 { get; set; }
		public string InvAddr3 { get; set; }
		public string InvAddr4 { get; set; }
	}
}