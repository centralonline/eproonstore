﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;

namespace Eprocurement2012.Models
{
	public class NotifyMeData : MasterPageData
	{
		public int NotifyID { get; set; }
		public string ProductID { get; set; }
		public string ProductName { get; set; }
		public int Qty { get; set; }
		[Required(ErrorMessage = "NotifyMeData.ErrorEmail")]
		[RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "NotifyMeData.NonRegularEmail")]
		//RegularExpression From http://www.markussipila.info/pub/emailvalidator.php?action=validate
		public string NotifyMeEmail { get; set; }

		[RegularExpression(@"^[0]{1}[8-9]{1}[0-9]{8}$", ErrorMessage = "NotifyMeData.NonRegularMobile")]
		public string MobileNo { get; set; }
		public string NotifyRemark { get; set; }
	}
}