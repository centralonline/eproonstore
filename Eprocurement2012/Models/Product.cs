﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Models.Repositories;
using OfficeMate.Framework.Formatting;

namespace Eprocurement2012.Models
{
	public sealed class Product
	{
		private const string _imageProductDefaultLargePath = "/images/image-product-deafult-Large.jpg";
		private const string _imageProductDefaultSmallPath = "/images/image-product-deafult-small.jpg";

		private const string _imageLargePath = "/images/lpimage/{0}.jpg";
		private const string _imageZoomPath = "/images/zpimage/{0}.jpg";
		private const string _imageSmallPath = "/images/tpimage/{0}.jpg";

		private const string _imageGalleryLargePath = "/images/lpimage/{0}_x{1}.jpg";
		private const string _imageGalleryZoomPath = "/images/zpimage/{0}_x{1}.jpg";
		private const string _imageGallerySmallPath = "/images/tpimage/{0}_x{1}.jpg";

		private const string _premiumGalleryLargePath = "/images/lpimage/{0}_f{1}.jpg";
		private const string _premiumGalleryZoomPath = "/images/zpimage/{0}_f{1}.jpg";
		private const string _premiumGallerySmallPath = "/images/tpimage/{0}_f{1}.jpg";

		public string Id { get; set; }
		public string CodeId { get; set; }
		public string DefaultSku { get; set; }
		public string RefId { get; set; }
		public LocalizedString Name { get; set; }
		public string ThaiName { get; set; }
		public string EngName { get; set; }
		public int DeptId { get; set; }
		public string Title { get; set; }
		private ProductStatusType _status;
		public ProductStatusType Status
		{
			get
			{
				if (_status == ProductStatusType.OutOfStock && User.Company.ShowOutofStock) { return ProductStatusType.Normal; }
				return _status;
			}
			set
			{
				_status = value;
			}
		}
		public PriceType PriceType { get; set; }

		public User User { get; set; }

		public string DisplayPrice
		{
			get
			{
				if ((PriceBegin > 0) && (PriceEnd > 0) && (PriceBegin != PriceEnd))
				{
					return String.Format("{0} - {1}", PriceBegin.ToString(new MoneyFormat()), PriceEnd.ToString(new MoneyFormat()));
				}
				else if ((PriceBegin > 0) && (PriceEnd > 0) && (PriceBegin == PriceEnd))
				{
					return PriceBegin.ToString(new MoneyFormat());
				}
				else
				{
					return PriceIncVatDiscount.ToString(new MoneyFormat());
				}
			}
		}

		//ราคาสินค้าลบส่วนลดบวกภาษี
		public decimal PriceIncVatDiscount
		{
			get
			{
				if (IsVat) //คิด Vat
				{
					if (IsBestDeal || IsPromotion) //ลดไม่ได้แล้ว
					{
						return PriceIncVat;
					}
					else
					{
						decimal PriceDiscount = (Math.Round(PriceExcVat - ((PriceExcVat * User.Company.CompanyDisCountRate) / 100), 2, MidpointRounding.AwayFromZero));
						return (Math.Round(PriceDiscount * Convert.ToDecimal(1.07), 2, MidpointRounding.AwayFromZero));
					}
				}
				else //ไม่คิด Vat
				{
					if (IsBestDeal || IsPromotion) //ลดไม่ได้แล้ว
					{
						return PriceExcVat;
					}
					else
					{
						return (Math.Round(PriceExcVat - ((PriceExcVat * User.Company.CompanyDisCountRate) / 100), 2, MidpointRounding.AwayFromZero));
					}
				}
			}
		}

		public decimal PriceExcVat { get; set; }
		public decimal PriceIncVat { get; set; }
		public decimal PriceBegin { get; set; }
		public decimal PriceEnd { get; set; }
		public decimal FullPriceIncVat { get; set; }

		public decimal Discount
		{
			get
			{
				decimal discount;
				discount = FullPriceIncVat - PriceIncVat;
				if (discount < 0)
				{
					discount = 0;
				}
				return discount;
			}
		}
		public decimal DiscountPercent
		{
			get
			{
				decimal discountPercent = 0m;
				if (Discount > 0)
				{
					discountPercent = (Discount * 100) / FullPriceIncVat;
				}
				return discountPercent;
			}
		}
		public LocalizedString Unit { get; set; }

		public LocalizedString Description { get; set; }
		public string DescriptionThai { get; set; }
		public string DescriptionEng { get; set; }

		public LocalizedString DescriptionShort { get; set; }
		public string DescriptionShortThai { get; set; }
		public string DescriptionShortEng { get; set; }


		public string RewriteUrl { get; set; }
		public string ImageSmallUrl
		{
			get
			{
				string strPath = String.Format(_imageSmallPath, IsPG ? DefaultSku : Id);
				if (CachedIO.PathExists(strPath))
				{
					return strPath;
				}
				return _imageProductDefaultSmallPath;
			}
		}
		public string ImageLargeUrl
		{
			get
			{
				string strPath = String.Format(_imageLargePath, IsPG ? DefaultSku : Id);
				if (CachedIO.PathExists(strPath))
				{
					return strPath;
				}
				return _imageProductDefaultLargePath;
			}
		}
		public string ImageZoomUrl
		{
			get
			{
				string strPath = String.Format(_imageZoomPath, IsPG ? DefaultSku : Id);
				if (CachedIO.PathExists(strPath))
				{
					return strPath;
				}
				return ImageLargeUrl;
			}
		}
		public string ProdType { get; set; }
		public decimal NumOfRating { get; set; }
		public bool IsPG { get { return !String.IsNullOrEmpty(Id) && (Id.Length != 7) ? true : false; } }
		public bool IsOneProduct { get; set; }
		public bool IsDelivery { get; set; }
		public bool IsInstock { get; set; }
		//public bool IsPromotion { get; set; }
		private bool _isPromotion;
		public bool IsPromotion
		{
			get
			{
				if (PriceType == Models.PriceType.Fix) { return false; }
				return _isPromotion;
			}
			set
			{
				_isPromotion = value;
			}
		}
		public bool IsNew { get; set; }
		public bool IsReccommend { get; set; }
		public bool IsHot { get; set; }
		public bool IsPin { get; set; }
		public bool IsBestDeal { get; set; }
		public bool IsVat { get; set; }
		public bool IsPremium { get; set; }
		public bool IsOfmCatalog { get; set; }
		public bool IsContactForDelivery
		{
			get { return String.Equals(pcatId, "A", StringComparison.OrdinalIgnoreCase); }
		}
		public bool IsContactAndNotOrder
		{
			get { return String.Equals(pcatId, "A", StringComparison.OrdinalIgnoreCase) && String.Equals(psubcatId, "56", StringComparison.OrdinalIgnoreCase); }
		}
		public string BrandId { get; set; }
		public string BrandName { get; set; }
		public string PropName { get; set; }
		public string PropValue { get; set; }
		public decimal TransCharge { get; set; }
		public string pcatId { get; set; }
		public string psubcatId { get; set; }
		public string supplierId { get; set; }
		public string supplierName { get; set; }
		public string PriceFlag { get; set; }
		public string ThaiUnit { get; set; }
		public string EngUnit { get; set; }
		private string _promotionText;
		public DateTime ValidFrom { get; set; }
		public DateTime ValidTo { get; set; }

		public string PromotionText
		{
			get
			{
				return IsPromotion ? _promotionText : "";
			}
			set { _promotionText = value; }
		}

		public DateTime UploadOn { get; set; }
		public string DisplayBestDeal
		{
			get { return IsBestDeal ? "<img src='/images/icon/icon_specialprice.gif' alt='special price' />*ราคาพิเศษสุด งดส่วนลดทุกชนิด" : ""; }
		}

		public string DisplayStatusText
		{
			get
			{
				switch (Status)
				{
					case ProductStatusType.NonStock:
						return "<img src='/images/icon/product_bt_3-7days.gif' alt='สินค้าจัดส่งภายใน 3-7 วัน' />";
					case ProductStatusType.OutOfStock:
						return "<img src='/images/icon/product_bt_out_of_stock.gif' alt='ขออภัยสินค้าขาดชั่วคราวค่ะ' />";
					case ProductStatusType.New:
						return "<img src='/images/icon/product_bt_comming_soon.gif' alt='Comming soon!' />";
					case ProductStatusType.PreOrder:
					case ProductStatusType.Prepaid:
						return "สินค้า PreOrder";
					case ProductStatusType.Limited:
						return "สินค้า Limited";
					case ProductStatusType.Deleted:
						return "<img src='/images/icon/product_bt_cancel.gif' alt='สินค้านี้ยกเลิกการขายแล้วค่ะ' />";
					case ProductStatusType.Hold:
						return "<img src='/images/icon/product_bt_stock_wait.gif' alt='สินค้ากำลังรอเข้าสต็อก' />";
					case ProductStatusType.Normal:
					case ProductStatusType.InStock:
						return "<img src='/images/icon/product_bt_stock.gif' alt='มีสินค้าในสต็อก พร้อมจัดส่ง' />";
					case ProductStatusType.ByOrder:
						return "<img src='/images/icon/product_bt_byorder.jpg' alt='สินค้าสั่งพิเศษ' />";
					default:
						return "";
				}
			}
		}

		public bool IsDisplayBuyButton
		{
			get
			{
				switch (Status)
				{
					case ProductStatusType.Normal:
					case ProductStatusType.NonStock:
					case ProductStatusType.PreOrder:
					case ProductStatusType.Prepaid:
					case ProductStatusType.InStock:
					case ProductStatusType.Limited:
					case ProductStatusType.ByOrder:
						//return true && !IsContactForDelivery;
						return true && !IsContactAndNotOrder;
					case ProductStatusType.OutOfStock:
					case ProductStatusType.New://comming soon
					case ProductStatusType.Deleted:
					case ProductStatusType.Hold:
						return false;
					default:
						return false;
				}
			}
		}

		public IEnumerable<ProductGalleryImage> GalleryImages
		{
			get
			{
				string smallFormat = _imageGallerySmallPath;
				string largeFormat = _imageGalleryLargePath;
				string zoomFormat = _imageGalleryZoomPath;
				List<ProductGalleryImage> galleryImages = new List<ProductGalleryImage>();
				string smallPath = ImageSmallUrl;
				string largePath = ImageLargeUrl;
				string zoomPath = ImageZoomUrl;
				int galleryImageIndex = 2;

				do
				{
					galleryImages.Add(new ProductGalleryImage { ImageSmallUrl = smallPath, ImageLargeUrl = largePath, ImageZoomUrl = zoomPath });
					smallPath = String.Format(smallFormat, Id, galleryImageIndex);
					largePath = String.Format(largeFormat, Id, galleryImageIndex);
					zoomPath = String.Format(zoomFormat, Id, galleryImageIndex);
					if (!CachedIO.PathExists(zoomPath))
					{
						zoomPath = largePath;
					}
					galleryImageIndex++;
				} while (CachedIO.PathExists(smallPath) && CachedIO.PathExists(largePath));

				if (IsPremium)
				{
					smallFormat = _premiumGallerySmallPath;
					largeFormat = _premiumGalleryLargePath;
					zoomFormat = _premiumGalleryZoomPath;

					galleryImageIndex = 2;

					smallPath = String.Format(smallFormat, Id, galleryImageIndex);
					largePath = String.Format(largeFormat, Id, galleryImageIndex);
					zoomPath = String.Format(zoomFormat, Id, galleryImageIndex);

					while (CachedIO.PathExists(smallPath) && CachedIO.PathExists(largePath))
					{
						if (!CachedIO.PathExists(zoomPath))
						{
							zoomPath = largePath;
						}
						galleryImages.Add(new ProductGalleryImage { ImageSmallUrl = smallPath, ImageLargeUrl = largePath, ImageZoomUrl = zoomPath });
						galleryImageIndex++;
						smallPath = String.Format(smallFormat, Id, galleryImageIndex);
						largePath = String.Format(largeFormat, Id, galleryImageIndex);
						zoomPath = String.Format(zoomFormat, Id, galleryImageIndex);
					}
				}
				return galleryImages.Count > 1 ? galleryImages : new List<ProductGalleryImage>();
			}
		}

		public string Paths { get; set; }

		public class ProductGalleryImage
		{
			public string ImageSmallUrl { get; set; }
			public string ImageLargeUrl { get; set; }
			public string ImageZoomUrl { get; set; }
		}
	}
}