﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class SiteDirectoryData : MasterPageData
	{
		public IEnumerable<Department> Departments { get; set; }
	}
}