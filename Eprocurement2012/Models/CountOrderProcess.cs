﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Net.Json;
using System.Reflection;

namespace Eprocurement2012.Models
{
	public class CountOrderProcess
	{
		//Purchase
		public int CountOrderWaiting { get; set; }
		public int CountOrderRevise { get; set; }
		public int CountOrderApprove { get; set; }
		public int CountOrderAllowAdmin { get; set; }

		//Admin
		public int CountStatusApproved { get; set; }
		public int CountStatusWaiting { get; set; }
		public int CountStatusExpired { get; set; }
		public int CountStatusRevise { get; set; }
		public int CountStatusWaitingAdmin { get; set; }
		public int CountStatusCompleted { get; set; }
		public int CountStatusShipped { get; set; }
		public int CountStatusDeleted { get; set; }
		public int CountStatusPartial { get; set; }

		//เก็บค่า เดือนที่ต้องการทราบจาก admin
		public bool thisMonth { get; set; }

		public string FormatPieChart { get; set; }
	}
	public class CountOrderProcessChart
	{
		public string FormatJSONCountOrderProcess(CountOrderProcess countOrderProcess)
		{
			List<JsonObjectCollection> item = new List<JsonObjectCollection>();
			JsonObjectCollection collection = null;
			string Result ="[";
			foreach (PropertyInfo p in typeof(CountOrderProcess).GetProperties())
			{
				if (!p.Name.Equals("FormatPieChart"))
				{
					collection = new JsonObjectCollection();
					collection.Add(new JsonStringValue("status", p.Name));
					collection.Add(new JsonNumericValue("val", Convert.ToInt32(p.GetValue(countOrderProcess, null))));
					item.Add(collection);
				}
			}
			JsonUtility.GenerateIndentedJsonText = true;
			foreach (var itemobj in item)
			{
				Result = Result + itemobj;
			}
			Result += "]";
			return Result;
		}
	}
}