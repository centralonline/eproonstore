﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class SearchUser : MasterPageData
	{
		public string UserId { get; set; }
		public string UserName { get; set; }
		public string CompanyId { get; set; }
		public string UserRoleName { get; set; }
		public string UserStatusName { get; set; }
		public string CreateBy { get; set; }
		public IEnumerable<User> ShearchUserData { get; set; }

		//Check Box
		public bool CheckUserId { get; set; }
		public bool CheckUserName { get; set; }
		public bool CheckCompanyId { get; set; }
		public bool CheckUserRoleName { get; set; }
		public bool CheckUserStatusName { get; set; }
		public bool CheckCreateBy { get; set; }
	}
}