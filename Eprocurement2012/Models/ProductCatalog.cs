﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class ProductCatalog : MasterPageData
	{
		public int CountUserCatalog { get; set; }
		public int CountCompanyCatalog { get; set; }

		public Company Company { get; set; }
		public IEnumerable<User> UserRequester { get; set; }
		public IEnumerable<Product> ItemProductCatalog { get; set; }

		public Product ProductDetail { get; set; }
		public ProductCatalogType PropType { get; set; }
		public string CatalogTypeId { get; set; }
		public string ProductId { get; set; }
		public string CompanyId { get; set; }
		public bool IsByCompany { get; set; }
		public string[] listUser { get; set; }
		public string StartDate { get; set; }
		public string EndDate { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }

		public enum ProductCatalogType
		{
			Company,
			User,
			Hold
		}
	}
}