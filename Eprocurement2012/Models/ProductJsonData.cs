﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class ProductJsonData
	{
		public string ProductThaiName { get; set; }
		public string ProductEngName { get; set; }
		public string ProductThaiUnit { get; set; }
		public string ProductEngUnit { get; set; }
		public string ProductName { get; set; }
		public string ProductUnit { get; set; }
		public string ProductId { get; set; }
	}
}