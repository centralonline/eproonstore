﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel;
using System.Globalization;
using System.Web.Security;


namespace Eprocurement2012.Models
{
	[PropertiesMustMatch("NewPassword", "ConfirmNewPassword", ErrorMessage = "Shared.ChangePassword.PasswordNotEqual")]
	public class Password : MasterPageData
	{
		[Required(ErrorMessage = "Shared.LogInCenter.ErrorOldPasswordEmptry")]
		public string OldPassword { get; set; }
		[RegularExpression(@"(^.{8,}$)$", ErrorMessage = "Shared.ChangePassword.PasswordLength")]
		[Required(ErrorMessage = "Shared.LogInCenter.ErrorNewPasswordEmptry")]
		[DataType(DataType.Password)]
		public string NewPassword { get; set; }
		[DataType(DataType.Password)]
		[Required(ErrorMessage = "Shared.LogInCenter.ErrorConfirmNewPasswordEmptry")]
		public string ConfirmNewPassword { get; set; }
		public string UserGuid { get; set; }
		public string VerifyKey { get; set; }
		public bool IsFirstPassword { get; set; }	
	}

	[AttributeUsage(AttributeTargets.Class, AllowMultiple = true, Inherited = true)]
	public sealed  class PropertiesMustMatchAttribute : ValidationAttribute
	{
		private const string _defaultErrorMessage = "'{0}' and '{1}' do not match.";
		private readonly object _typeId = new object();
		public PropertiesMustMatchAttribute(string originalProperty, string confirmProperty) : base(_defaultErrorMessage)
		{
			OriginalProperty = originalProperty;
			ConfirmProperty = confirmProperty;
		}
		public string ConfirmProperty { get; private set; }
		public string OriginalProperty { get; private set; }
		public override object TypeId
		{
			get
			{
				return _typeId;
			}
		}
		public override string FormatErrorMessage(string name)
		{
			return String.Format(CultureInfo.CurrentUICulture, ErrorMessageString,OriginalProperty, ConfirmProperty);
		}
		public override bool IsValid(object value)
		{
			PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(value);
			object originalValue = properties.Find(OriginalProperty, true /* ignoreCase */).GetValue(value);
			object confirmValue = properties.Find(ConfirmProperty, true /* ignoreCase */).GetValue(value);
			return Object.Equals(originalValue, confirmValue);
		}
	}

	[PropertiesMustMatch("NewPassword", "ConfirmNewPassword", ErrorMessage = "Shared.ChangePassword.PasswordNotEqual")]
	public class SetPassword : MasterPageData
	{
		[RegularExpression(@"(^.{8,}$)$", ErrorMessage = "Shared.ChangePassword.PasswordLength")]
		[Required(ErrorMessage = "Shared.LogInCenter.ErrorNewPasswordEmptry")]
		[DataType(DataType.Password)]
		public string NewPassword { get; set; }
		[RegularExpression(@"(^.{8,}$)$", ErrorMessage = "Shared.ChangePassword.PasswordLength")]
		[Required(ErrorMessage = "Shared.LogInCenter.ErrorConfirmNewPasswordEmptry")]
		[DataType(DataType.Password)]
		public string ConfirmNewPassword { get; set; }
		public string UserGuid { get; set; }
		public string VerifyKey { get; set; }
		public bool IsFirstPassword { get; set; }
	}

}