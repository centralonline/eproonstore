﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class SearchCompany : MasterPageData
	{
		public string CompanyId { get; set; }
		public string CompanyName { get; set; }
		public string CustId { get; set; }
		public string CompanyStatus { get; set; }
		public string PriceFormat { get; set; }
		public string OrderFormat { get; set; }
		public string CatalogFormat { get; set; }
		public string SiteStatus { get; set; }
		public DateTime CreateDateFrom { get; set; }
		public DateTime CreateDateTo { get; set; }
		public string CreateBy { get; set; }

		//Check Box
		public bool CheckCompId { get; set; }
		public bool CheckCompName { get; set; }
		public bool CheckCustId { get; set; }
		public bool CheckCompStatus { get; set; }
		public bool CheckPriceFormat { get; set; }
		public bool CheckOrderFormat { get; set; }
		public bool CheckCatalogFormat { get; set; }
		public bool CheckSiteStatus { get; set; }
		public bool CheckCreateDate { get; set; }
		public bool CheckCreateBy { get; set; }

		public IEnumerable<Company> ShearchData { get; set; }
	}
}