﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class UserProfile : MasterPageData //ใช้ในหน้า Profile ผู้ใช้งาน
	{
		public IEnumerable<UserPermission> ItemUserPermissions { get; set; }
	}

	public class ModelViewListUserData : MasterPageData //สร้างไว้ใช้ในหน้า UserInfo.AllUser และ UserInfo.AllAdmin
	{
		public string CompanyId { get; set; }
		public IEnumerable<User> UserData { get; set; }
		public ProcessLog ProcessLog { get; set; }
	}
}