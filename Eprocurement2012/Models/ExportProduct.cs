﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class ExportProduct
	{
		public string Pid { get; set; }
		public string StatusBaseDeal { get; set; }
		public string CompanyId { get; set; }
		public string UserId { get; set; }
	}
}