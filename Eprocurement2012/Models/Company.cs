﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Controllers;
using Eprocurement2012.Controllers.Helpers;
using System.ComponentModel.DataAnnotations;

namespace Eprocurement2012.Models
{
	public class Company : MasterPageData
	{
		public string CompanyId { get; set; }
		public LocalizedString CompanyName { get; set; }
		[Required(ErrorMessage = "Admin.EditCompanyInfo.ErrorCompanyThaiNameEmptry")]
		public string CompanyThaiName { get; set; }
		[Required(ErrorMessage = "Admin.EditCompanyInfo.ErrorCompanyEngNameEmptry")]
		public string CompanyEngName { get; set; }
		public string DefaultCustId { get; set; }
		public CompanyStatus CompStatus { get; set; }
		public bool IsCompModelThreeLevel { get; set; }
		public decimal CompanyDisCountRate { get; set; }
		public HttpPostedFileBase CompanyLogo { get; set; }
		public OrderControl OrderControlType { get; set; }
		public BudgetLevel BudgetLevelType { get; set; }
		public BudgetPeriod BudgetPeriodType { get; set; }
		public PriceType PriceType { get; set; }
		public OrderIDFormatType OrderIDFormat { get; set; }
		public User.Language DefaultLang { get; set; }
		public Invoice CompanyInvoice { get; set; }
		public Shipping CompanyShipping { get; set; }
		public IEnumerable<Shipping> ListShipAddress { get; set; }
		public IEnumerable<CompanyAddress> ListCompanyAddress { get; set; }
		public CompanyAddress CompanyAddress { get; set; }
		public bool AllowAddProduct { get; set; }
		public bool IsDefaultDeliCharge { get; set; }
		public int DefaultParkDay { get; set; }
		public bool IsCompanyFixPrice { get; set; }
		public bool ShowOFMCat { get; set; }
		public bool ShowOutofStock { get; set; }
		public bool IsByPassAdmin { get; set; }
		public bool IsByPassApprover { get; set; }
		public bool IsAutoApprove { get; set; }
		public bool UseCompanyNews { get; set; }
		public bool UseOfmNews { get; set; }
		public bool UseSMSFeature { get; set; }// ส่ง SMS การสั่งซื้อสินค้า
		public bool UseOfmCatalog { get; set; }
		public bool ShowContactUs { get; set; }
		public bool ShowSpecialProd { get; set; }
		public bool UseReferenceCode { get; set; }
		public bool IsSiteActive { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }
		public ImageData CompanyLogoReview { get; set; }
		public bool UseGoodReceive { get; set; }
		public int GoodReceivePeriod { get; set; }
		public string CompanyModel { get; set; }
		public string SaleRequest { get; set; }
		public bool ShowMultiApprove { get; set; }

		//นำข้อมมูลในหน้า view มาแปลงเป็นตัวเลย เพื่อนเก็บลง data base
		public int RePasswordPeriod
		{
			get
			{
				switch (RePasswordText)
				{
					case "XMonth":
						return Convert.ToInt32(RePasswordPeriodFromUser);
					case "1Month":
						return 30;
					case "2Month":
						return 60;
					default:
						return 90;
				}
			}
		}

		//ใช้แสดงข้อมูลหน้า View
		public decimal DiscountRate { get; set; }
		public string RePasswordPeriodFromUser { get; set; }
		public string RePasswordText { get; set; }
		public string DisplayCompanyModel
		{
			get
			{
				if (CompanyModel == "3Level")
				{
					return new ResourceString("CompanyModel.3Level");
				}
				else
				{
					return new ResourceString("CompanyModel.2Level");
				}
			}
		}
		public string DisplayBudgetPeriodType
		{
			get
			{
				if (BudgetPeriodType == BudgetPeriod.Half) { return new ResourceString("NewSiteData.ListBudgetPeriodType.Half"); }
				else if (BudgetPeriodType == BudgetPeriod.Month) { return new ResourceString("NewSiteData.ListBudgetPeriodType.Month"); }
				else if (BudgetPeriodType == BudgetPeriod.Quarter) { return new ResourceString("NewSiteData.ListBudgetPeriodType.Quarter"); }
				return new ResourceString("NewSiteData.ListBudgetPeriodType.Year");
			}
		}
		public string DisplayBudgetLevelType
		{
			get
			{
				if (BudgetLevelType == BudgetLevel.Company) { return new ResourceString("Admin.CompanySettingMenu.CompanyInfo"); }
				else if (BudgetLevelType == BudgetLevel.Costcenter) { return new ResourceString("Admin.CompanySettingMenu.CostCenter"); }
				return new ResourceString("Admin.CompanySettingMenu.Department");
			}
		}
		public string DisplayControlTypeName
		{
			get
			{
				if (OrderControlType == OrderControl.ByOrder) { return new ResourceString("Model.Company.OrderTypeByOrder"); }
				return new ResourceString("Model.Company.OrderTypeByBudget");
			}
		}
		public string DisplayUseCompanyCatalog
		{
			get
			{
				if (UseOfmCatalog) { return new ResourceString("Model.Company.NotUseCatalog"); }
				return new ResourceString("Model.Company.UseCatalog");
			}
		}
		public string DisplayPriceType
		{
			get
			{
				if (PriceType == PriceType.Fix) { return new ResourceString("Model.Company.FixPrice"); }
				return new ResourceString("Model.Company.FloatPrice");
			}
		}
		public string DisplayOrderIDFormat
		{
			get
			{
				if (OrderIDFormat == OrderIDFormatType.Standard) { return new ResourceString("Model.Company.StandardOrderID"); }
				return new ResourceString("Model.Company.ManualOrderID");
			}
		}
		public IEnumerable<StringForSelectList> ListOrderControlType = new List<StringForSelectList>
		{
			new StringForSelectList { Id="ByOrder", Name = new ResourceString("NewSiteData.OrderControlType.ByOrder")},
			new StringForSelectList { Id="ByBudgetAndOrder", Name = new ResourceString("NewSiteData.OrderControlType.ByBudgetAndOrder")}
		};
		public IEnumerable<StringForSelectList> ListBudgetLevelType = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Company", Name = new ResourceString("NewSiteData.ListBudgetLevelType.Company")},
			new StringForSelectList { Id="Department", Name = new ResourceString("NewSiteData.ListBudgetLevelType.Department")},
			new StringForSelectList { Id="Costcenter", Name = new ResourceString("NewSiteData.ListBudgetLevelType.Costcenter")}
		};
		public IEnumerable<StringForSelectList> ListBudgetPeriodType = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Year", Name = new ResourceString("NewSiteData.ListBudgetPeriodType.Year")},
			new StringForSelectList { Id="Half", Name = new ResourceString("NewSiteData.ListBudgetPeriodType.Half")},
			new StringForSelectList { Id="Quarter", Name = new ResourceString("NewSiteData.ListBudgetPeriodType.Quarter")},
			new StringForSelectList { Id="Month", Name = new ResourceString("NewSiteData.ListBudgetPeriodType.Month")}
		};
		public IEnumerable<StringForSelectList> ListPriceType = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Float", Name = new ResourceString("NewSiteData.ListPriceType.Float")},
			new StringForSelectList { Id="Fix", Name = new ResourceString("NewSiteData.ListPriceType.Fix")}
		};
		public IEnumerable<StringForSelectList> ListParkDay = new List<StringForSelectList>
		{
			new StringForSelectList {Id="3",Name = new ResourceString("NewSiteData.ListParkDay.3")},
			new StringForSelectList {Id="5",Name = new ResourceString("NewSiteData.ListParkDay.5")},
			new StringForSelectList {Id="7",Name = new ResourceString("NewSiteData.ListParkDay.7")}
		};
		public string DisplayCompanyStatus
		{
			get 
			{
				if (CompStatus == CompanyStatus.Active) { return new ResourceString("OFMAdmin.Index.Active"); }
				return new ResourceString("OFMAdmin.Index.Cancel");
			}
		}

		public string DisplayIsSiteActive 
		{
			get 
			{
				if (IsSiteActive)
				{
					return new ResourceString("OFMAdmin.SearchCompany.Active");
				}
				return new ResourceString("OFMAdmin.SearchCompany.Cancel");
			}
		}
		//enum
		public enum CompanyStatus
		{
			Active,
			Cancel
		}
		public enum CompanyActive
		{
			Yes,
			No
		}
		public enum OrderControl
		{
			ByOrder,
			ByBudgetAndOrder,
		}
		public enum BudgetLevel
		{
			Company,
			Department,
			Costcenter,
			Not // ByOrder ไม่คุม Budget
		}
		public enum BudgetPeriod
		{
			Year,
			Half,
			Quarter,
			Month,
			Not // ByOrder ไม่คุม Budget
		}
		public enum OrderIDFormatType
		{
			Standard, // CompID-Y2M2-Running(5)
			TypeA, // TypeA CompID-CostID-Y2M2-Running(5) - OFM-001-120100001
			TypeB, // TypeB CompID-DeptID-Y2M2-Running(5) - OFM-MK-120100001
			TypeC, // TypeC DeptID-CostID-Y2M2-Running(5) - MK-001-120100001
			TypeD, // TypeD CostID-Y2M2-Running(5) - 001-120100001
			Modify //แบบเฉพาะของ company
		}
	}

	public class CompanyAddress
	{
		public string CustId { get; set; }
		public string InvoiceAddress1 { get; set; }
		public int CountShip { get; set; }
	}

	public class CustCompany
	{
		public string CustId { get; set; }
		public string CompanyId { get; set; }
		public string IsDefault { get; set; }
	}
}