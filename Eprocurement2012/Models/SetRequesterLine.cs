﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class SetRequesterLine : MasterPageData
	{
		public CostCenter CostCenter { get; set; }
		public IEnumerable<User> UserRequester { get; set; }
		public IEnumerable<User> UserApprover { get; set; }
		public IEnumerable<UserPermission> UserCurrentPermission { get; set; }
		public IEnumerable<InformationRequesterLine> InformationRequesterLine { get; set; }
		public User UserReqDetail { get; set; }
		public IEnumerable<CostCenter> ListCostCenter { get; set; }
		public IEnumerable<CostCenter> CostCurrentPermission { get; set; }
		public string CompanyId { get; set; }
		public string CostcenterId { get; set; }
		public string CreateBy { get; set; }
		public string UpdateBy { get; set; }
		public string RequesterUserId { get; set; }
		public string ApproverUserId { get; set; }
		public int AppLevel { get; set; }
		public int Parkday { get; set; }
		public decimal AppCreditlimit { get; set; }
		public DateTime CreateOn { get; set; }
		public DateTime UpdateOn { get; set; }
		public LocalizedString ApproverName { get; set; }
		public string ApproverThaiName { get; set; }
		public string ApproverEngName { get; set; }

		//รับค่าจากหน้า view
		public string[] SelectCostcenter { get; set; }
		public string[] SelectRequester { get; set; }
		public string[] SelectApprover { get; set; }
		public int[] SelectParkday { get; set; }
		public decimal[] SelectBudget { get; set; }

		public IEnumerable<StringForSelectList> ListParkDay = new List<StringForSelectList>
		{
			new StringForSelectList { Id="3", Name = new ResourceString("NewSiteData.ListParkDay.3")},
			new StringForSelectList { Id="5", Name = new ResourceString("NewSiteData.ListParkDay.5")},
			new StringForSelectList { Id="7", Name = new ResourceString("NewSiteData.ListParkDay.7")}
		};
	}
}