﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class CompanyDepartment : MasterPageData
	{
		public string CompanyId { get; set; }
		public string DepartmentGuID { get; set; }
		public LocalizedString DepartmentName { get; set; }
		public string DepartmentDisplayname {
			get
			{
				return "[" + DepartmentID + "] " + DepartmentName;
			}
			
			}
		public DeptStatus DepartmentStatus { get; set; }
		[RegularExpression(@"^[A-Za-z0-9_\-]*$", ErrorMessage = "FormatNotValid")]
		[Required(ErrorMessage = "Admin.EditDepartment.ErrorDepartmentIDEmptry")]
		public string DepartmentID { get; set; }
		[Required(ErrorMessage = "Admin.EditDepartment.ErrorThaiNameEmptry")]
		public string DepartmentThaiName { get; set; }
		[Required(ErrorMessage = "Admin.EditDepartment.ErrorEngNameEmptry")]
		public string DepartmentEngName { get; set; }
		public string CreateBy { get; set; }
		public string CreateOn { get; set; }
		public string UpdateBy { get; set; }
		public string UpdateOn { get; set; }
		public IEnumerable<StringForSelectList> ListDepartmentStatus = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Active", Name = new ResourceString("Admin.ListDepartmentStatus.Active")},
			new StringForSelectList { Id="Cancel", Name = new ResourceString("Admin.ListDepartmentStatus.Cancel")}
		};
		public enum DeptStatus
		{
			Active,
			Cancel
		}

		public string DisplayDepartmentStatus
		{
			get
			{
				if (DepartmentStatus == DeptStatus.Active) { return new ResourceString("Admin.Status.Active"); }
				return new ResourceString("Admin.Status.Cancel");
			}
		}

	}

	/*สร้างมาใช้กับ ViewAllDepartment เท่านั้น*/
	public class ViewAllDepartmentData : MasterPageData
	{
		public string CompanyId { get; set; }
		public bool IsCompModelThreeLevel { get; set; }
		public IEnumerable<CompanyDepartment> Departments { get; set; }
	}

}