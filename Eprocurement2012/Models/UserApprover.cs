﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class UserApprover
	{
		public User Approver { get; set; }
		public int ParkDay { get; set; }
		public int Level { get; set; }
		public decimal ApproveCreditLimit { get; set; }
		public string ApproverRemark { get; set; }
		public bool IsByPassApprover { get; set; }
	}
}