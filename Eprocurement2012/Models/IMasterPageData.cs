﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
namespace Eprocurement2012.Models
{
	public interface IMasterPageData
	{
		User User { get; set; }
		string RedirectUrl { get; set; }
		string CurrentUrl { get; set; }
		string BackToShoppingUrl { get; set; }
		bool IsSecureConnection { get; set; }
		bool IsProduction { get; set; }
		int ItemCount { get; set; }
		int ItemCountQty { get; set; }
	}
}