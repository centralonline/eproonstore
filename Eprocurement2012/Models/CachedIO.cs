﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Caching;
using System.IO;

namespace Eprocurement2012.Models
{
	public static class CachedIO
	{
		public static bool PathExists(string path)
		{
			bool result;
			string cacheKey = "PathExist/" + path;
			object cacheObject = HostingEnvironment.Cache.Get(cacheKey);
			if (cacheObject == null)
			{
				string localPath = HostingEnvironment.MapPath(path);
				result = File.Exists(localPath);
				HostingEnvironment.Cache.Insert(cacheKey, result, new CacheDependency(Path.GetDirectoryName(localPath)));
			}
			else
			{
				result = (bool)cacheObject;
			}
			return result;
		}
	}
}