﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public sealed class PropertyDept
	{
		public int DeptId { get; set; }
		public string Name { get; set; }
		public IEnumerable<PropertyChoice> Choices { get; set; }
	}
}