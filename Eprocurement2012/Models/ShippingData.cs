﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;


namespace Eprocurement2012.Models
{
	public class ShippingData : MasterPageData
	{
		//shipping
		public int ShipID { get; set; }
		public string ShipContactor { get; set; }
		public int PhoneID { get; set; }
		[Required(ErrorMessage = "Error.PhoneNumber")]
		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string ShipPhoneNumber { get; set; }
		public string ShipPhoneNumberExtension { get; set; }
		[Required(ErrorMessage = "Error.ContactName")]
		public string ShipAddress1 { get; set; }
		[Required(ErrorMessage = "Error.ShippingAddress")]
		public string ShipAddress2 { get; set; }
		public string ShipAddress3 { get; set; }
		[Required(ErrorMessage = "Error.Province")]
		[RegularExpression(@"^\d{5}$", ErrorMessage = "Error.NotFormatProvince")]
		public string ShipAddress4 { get; set; }
		public string ProvinceSelected { get; set; }
		public IEnumerable<SelectListItem> ProvinceList { get; set; }
		public string CustIdSelected { get; set; }
		public IEnumerable<string> ListCustId { get; set; }
		public IEnumerable<Shipping> OtherShippings { get; set; }
		public int ShippingIdSelected { get; set; }
		public string ZipCode { get; set; }
		public string CompanyId { get; set; } //ใช้ในหน้า admin ofm
	}
}