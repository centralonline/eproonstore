﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Models.Repositories;

namespace Eprocurement2012.Models
{
	public sealed class Department
	{
		private const string _imageDepartmentPath = "/images/department/{0}.jpg";
		private const string _imageDepartmentDefaultPath = "/images/department/image_default.jpg";

		public int Id { get; set; }
		public int ParentId { get; set; }
		public string RefId { get; set; }
		public string ParentRefId { get; set; }
		public int ChildrenCount { get; set; }
		public LocalizedString Name { get; set; }
		public string MetaDescription { get; set; }
		public string MetaKeyword { get; set; }
		public string Title { get; set; }
		public string RewriteUrl { get; set; }
		public bool HasSubDept { get; set; }
		public DepartmentDisplayType DisplayTypeDown { get; set; }
		public DepartmentDisplayType DisplayTypeUp { get; set; }
		public DepartmentThemeType ThemeTypeDown { get; set; }
		public DepartmentThemeType ThemeTypeUp { get; set; }
		public DepartmentViewType ProductListViewType { get; set; }
		public DepartmentSortByType DefaultSortBy { get; set; }
		public bool IsDefault { get; set; }
		public string ImageDepartmentUrl
		{
			get
			{
				string strPath = String.Format(_imageDepartmentPath, Id);
				if (CachedIO.PathExists(strPath))
				{
					return strPath;
				}
				return _imageDepartmentDefaultPath;
			}
		}
	}
	public sealed class Child
	{
		public Department ChildDepartment { get; set; }
		public IEnumerable<Department> GrandChild { get; set; }
	}
	public sealed class Ancestor
	{
		public Department ParentDept { get; set; }
		public Department AncestorDepartment { get; set; }
		public IEnumerable<Child> ChildDept { get; set; }
	}
}