﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class ContactUs : MasterPageData
	{
		[Required(ErrorMessage = "Home.ContactUs.ErrorUserNameEmpty")]
		public string UserName { get; set; }

		[Required(ErrorMessage = "Home.ContactUs.ErrorEmailEmpty")]
		[RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "Home.ContactUs.ErrorEmailFalse")]
		public string UserID { get; set; }

		public string CompanyId { get; set; }
		public string CompanyName { get; set; }

		[Required(ErrorMessage = "Home.ContactUs.ErrorPhoneNoEmpty")]
		public string PhoneNo { get; set; }

		[Required(ErrorMessage = "Home.ContactUs.ErrorContactTitleEmpty")]
		public string ContactTitle { get; set; }

		[Required(ErrorMessage = "Home.ContactUs.ErrorContactDetailEmpty")]
		public string ContactDetail { get; set; }

		public IEnumerable<StringForSelectList> ListContactTitle = new List<StringForSelectList>
		{
			new StringForSelectList {Id="ContactTitle1",Name = new ResourceString("Home.ContactUs.ListContactTitle1")},
			new StringForSelectList {Id="ContactTitle2",Name = new ResourceString("Home.ContactUs.ListContactTitle2")},
			new StringForSelectList {Id="ContactTitle3",Name = new ResourceString("Home.ContactUs.ListContactTitle3")},
			new StringForSelectList {Id="ContactTitle4",Name = new ResourceString("Home.ContactUs.ListContactTitle4")},
			new StringForSelectList {Id="ContactTitle5",Name = new ResourceString("Home.ContactUs.ListContactTitle5")}
		};

		public IEnumerable<User> ListMyAdmin { get; set; }

		public string RootAdminName
		{
			get 
			{
				return ListMyAdmin.Where(o => o.UserRoleName == User.UserRole.RootAdmin).Single().DisplayName;
			}
		}

		public string RootAdminId
		{
			get
			{
				return ListMyAdmin.Where(o => o.UserRoleName == User.UserRole.RootAdmin).Single().UserId;
			}
		}
	}

	public class StringForSelectList
	{
		public string Id { get; set; }
		public ResourceString Name { get; set; }
	}
}
