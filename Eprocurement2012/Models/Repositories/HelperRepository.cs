﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models.Repositories
{
	public class HelperRepository
	{
		public string GenerateSeoName(string name)
		{
			char[] charsToTrim = { ',', '.', ' ', '-', };
			name = name.Replace("**", "");
			name = name.Replace(" ", "-");
			name = name.Replace("&", "and");
			name = name.Replace("/", "-");
			name = name.Replace("\n", "");
			name = name.Replace("\r", "");
			name = name.Replace("'", "");
			name = name.Replace("\"", "");
			name = name.Replace("\\", "");
			name = name.Replace(":", "-");
			name = name.Replace("%", "-เปอร์เซ็นต์");
			name = name.Replace(".", "-");
			name = name.Replace(",", "-");
			name = name.Replace("+", "");
			while (name.Contains("--"))
			{
				name = name.Replace("--", "-");
			}
			name = name.TrimEnd(charsToTrim).Trim();
			return name;
		}
	}
}