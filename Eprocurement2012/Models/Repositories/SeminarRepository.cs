﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class SeminarRepository : RepositoryBase
	{
		public SeminarRepository(SqlConnection connection) : base(connection) { }
		public void SaveSeminarRegister(Seminar siminar, decimal capital, int custEmpNum)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"INSERT INTO [TBSeminar]
										   ([CustType]
										   ,[CompName]
										   ,[CustID]
										   ,[InvAddr1]
										   ,[InvAddr2]
										   ,[InvAddr3]
										   ,[InvAddr4]
										   ,[PhoneNo]
										   ,[FaxNo]
										   ,[Website]
										   ,[ContactName1]
										   ,[ContactPosition1]
										   ,[ContactPhone1]
										   ,[ContactMobile1]
										   ,[ContactMail1]
										   ,[ContactName2]
										   ,[ContactPosition2]
										   ,[ContactPhone2]
										   ,[ContactMobile2]
										   ,[ContactMail2]
										   ,[NewsChannel]
										   ,[ERPSystem]
										   ,[CreateOn]
											,[BusinessTypeName]
											,[CustEmpnum]
											,[Capital]
											)
									 VALUES
										(
											@CustType
										   ,@CompName
										   ,@CustID
										   ,@InvAddr1
										   ,@InvAddr2
										   ,@InvAddr3
										   ,@InvAddr4
										   ,@PhoneNo
										   ,@FaxNo
										   ,@Website
										   ,@ContactName1
										   ,@ContactPosition1
										   ,@ContactPhone1
										   ,@ContactMobile1
										   ,@ContactMail1
										   ,@ContactName2
										   ,@ContactPosition2
										   ,@ContactPhone2
										   ,@ContactMobile2
										   ,@ContactMail2
										   ,@NewsChannel
										   ,@ERPSystem
										   ,getdate()
											,@BusinessTypeName
											,@CustEmpnum
											,@Capital
											)";

				command.Parameters.AddWithValue("@CustType", siminar.CustType);
				command.Parameters.AddWithValue("@CompName", !string.IsNullOrEmpty(siminar.CompName) ? siminar.CompName : !string.IsNullOrEmpty(siminar.InvAddr1) ? siminar.InvAddr1 : "");
				command.Parameters.AddWithValue("@CustID", !string.IsNullOrEmpty(siminar.CustID) ? siminar.CustID : "");
				command.Parameters.AddWithValue("@InvAddr1", !string.IsNullOrEmpty(siminar.InvAddr1) ? siminar.InvAddr1 : !string.IsNullOrEmpty(siminar.CompName) ? siminar.CompName : "");
				command.Parameters.AddWithValue("@InvAddr2", !string.IsNullOrEmpty(siminar.InvAddr2) ? siminar.InvAddr2 : "");
				command.Parameters.AddWithValue("@InvAddr3", !string.IsNullOrEmpty(siminar.InvAddr3) ? siminar.InvAddr3 : "");
				command.Parameters.AddWithValue("@InvAddr4", !string.IsNullOrEmpty(siminar.InvAddr4) ? siminar.InvAddr4 : "");
				command.Parameters.AddWithValue("@PhoneNo", !string.IsNullOrEmpty(siminar.PhoneNo) ? siminar.PhoneNo : "");
				command.Parameters.AddWithValue("@FaxNo", !string.IsNullOrEmpty(siminar.FaxNo) ? siminar.FaxNo : "");
				command.Parameters.AddWithValue("@Website", !string.IsNullOrEmpty(siminar.Website) ? siminar.Website : "");
				command.Parameters.AddWithValue("@ContactName1", siminar.ContactName1);
				command.Parameters.AddWithValue("@ContactPosition1", siminar.ContactPosition1);
				command.Parameters.AddWithValue("@ContactPhone1", siminar.ContactPhone1);
				command.Parameters.AddWithValue("@ContactMobile1", !string.IsNullOrEmpty(siminar.ContactMobile1) ? siminar.ContactMobile1 : "");
				command.Parameters.AddWithValue("@ContactMail1", siminar.ContactMail1);
				command.Parameters.AddWithValue("@ContactName2", !string.IsNullOrEmpty(siminar.ContactName2) ? siminar.ContactName2 : "");
				command.Parameters.AddWithValue("@ContactPosition2", !string.IsNullOrEmpty(siminar.ContactPosition2) ? siminar.ContactPosition2 : "");
				command.Parameters.AddWithValue("@ContactPhone2", !string.IsNullOrEmpty(siminar.ContactPhone2) ? siminar.ContactPhone2 : "");
				command.Parameters.AddWithValue("@ContactMobile2", !string.IsNullOrEmpty(siminar.ContactMobile2) ? siminar.ContactMobile2 : "");
				command.Parameters.AddWithValue("@ContactMail2", !string.IsNullOrEmpty(siminar.ContactMail2) ? siminar.ContactMail2 : "");
				command.Parameters.AddWithValue("@NewsChannel", siminar.NewsChannelToDB);
				command.Parameters.AddWithValue("@ERPSystem", !string.IsNullOrEmpty(siminar.ERPSystem) ? siminar.ERPSystem : "-");
				command.Parameters.AddWithValue("@BusinessTypeName",siminar.BusinessType ?? string.Empty);
				//command.Parameters.AddWithValue("@CustEmpnum",!string.IsNullOrEmpty(siminar.CustEmpNum.ToString()) ? siminar.CustEmpNum : 0);
				//command.Parameters.AddWithValue("@Capital",!string.IsNullOrEmpty(siminar.Capital.ToString()) ? siminar.Capital : 0);
				command.Parameters.AddWithValue("@CustEmpnum", custEmpNum);
				command.Parameters.AddWithValue("@Capital", capital);
				command.ExecuteNonQuery();
			}
		}
		public IEnumerable<Seminar> GetSeminarRegister()
		{
			List<Seminar> item = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "SELECT * FROM [TBSeminar] ORDER BY CreateOn";

				using (SqlDataReader reader = command.ExecuteReader())
				{
					item = new List<Seminar>();
					while (reader.Read())
					{
						Seminar seminar = new Seminar();
						seminar.InvAddr1 = (string)reader["InvAddr1"];
						seminar.InvAddr2 = (string)reader["InvAddr2"];
						seminar.InvAddr3 = (string)reader["InvAddr3"];
						seminar.InvAddr4 = (string)reader["InvAddr4"];
						seminar.CustType = (string)reader["CustType"];
						seminar.CompName = (string)reader["CompName"];
						seminar.CustID = (string)reader["CustID"];
						seminar.PhoneNo = (string)reader["PhoneNo"];
						seminar.FaxNo = (string)reader["FaxNo"];
						seminar.Website = (string)reader["Website"];
						seminar.ContactName1 = (string)reader["ContactName1"];
						seminar.ContactPosition1 = (string)reader["ContactPosition1"];
						seminar.ContactPhone1 = (string)reader["ContactPhone1"];
						seminar.ContactMobile1 = (string)reader["ContactMobile1"];
						seminar.ContactMail1 = (string)reader["ContactMail1"];
						seminar.ContactName2 = (string)reader["ContactName2"];
						seminar.ContactPosition2 = (string)reader["ContactPosition2"];
						seminar.ContactPhone2 = (string)reader["ContactPhone2"];
						seminar.ContactMobile2 = (string)reader["ContactMobile2"];
						seminar.ContactMail2 = (string)reader["ContactMail2"];
						seminar.NewsChannel = (string)reader["NewsChannel"];
						seminar.ERPSystem = (string)reader["ERPSystem"];
						seminar.CreateOn = (DateTime)reader["CreateOn"];
						seminar.BusinessType = (string)reader["BusinessTypeName"] ?? string.Empty ;
						//seminar.CustEmpNum = (int)reader["CustEmpNum"];
						//seminar.Capital = (decimal)reader["Capital"];
						seminar.CustEmpNum = reader["CustEmpNum"].ToString();
						seminar.Capital = reader["Capital"].ToString();
						item.Add(seminar);
					}
				}
				return item;
			}
		}
	}
}