﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Transactions;
using Eprocurement2012.Models.Repositories;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class OrderRepository : RepositoryBase
	{
		public OrderRepository(SqlConnection connection) : base(connection) { }

		public string CreateOrder(CartData cartData)
		{
			using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
			{

				using (SqlCommand command = CreateCommandEnsureConnectionOpen())
				{
					command.CommandText = @"
											Declare @OrderID varchar(20) Set @OrderID = ''
											exec spc_GetMaxIDInc 'TBOrder','OrderID',@DocType,@Part1,@Part2,@Part3,@OrderID output

											insert into TBOrder 
											(
												OrderID,OrderGUID,OrderDate,OrderStatus,OrderBeforeStatus,
												CompanyID,DepartmentID,CostcenterID,CustID,CustName,
												UserID,UserTName,UserEName,
												InvAddr1,InvAddr2,InvAddr3,InvAddr4,
												ShipID,OfflineShipID,ShipContactor,ShipPhoneNo,ShipMobileNo,ShippingSeqNo,ShipAddr1,ShipAddr2,
												ShipAddr3,ShipAddr4,ShipProvince,ShipRemark,ShipType,
												ContactID,OfflineContactID,ContactorSeqNo,ContactorName,ContactorPhone,ContactorExtension,
												ContactorFax,ContactorEmail,ContactMobileNo,CountProduct,
												GrandTotalAmt,TotAmt,NetAmt,VatAmt,VatRate,OriginalTotalNetExcVatAmt,OriginalTotalNetIncVatAmt,VatProdNetAmt,
												NonVatProdNetAmt,NumOfAdjust,DiscountAmt,DiscountRate,OrdDeliverFee,DeliveryFee,
												PaymentCode,PaymentType,RefInvNo,TrackingNo,CallbackRequest,IPAddress,
												ApproverRemark,OFMRemark,ReferenceRemark,AttachFile,UseSMSFeature,
												CurrentAppID,NextAppID,ParkDay,WarningExpireDate,
												ExpireDate,AdminAllowFlag,DownloadStatus,DownloadBy,
												CreateOn,CreateBy,UpdateOn,UpdateBy,CustFileName,SystemFileName
											)
											values
											(
												@OrderID,newid(),getdate(),@OrderStatus,@OrderBeforeStatus,
												@CompanyID,@DepartmentID,@CostcenterID,@CustID,@CustName,
												@UserID,@UserTName,@UserEName,
												@InvAddr1,@InvAddr2,@InvAddr3,@InvAddr4,
												@ShipID,@OfflineShipID,@ShipContactor,@ShipPhoneNo,@ShipMobileNo,@ShippingSeqNo,@ShipAddr1,@ShipAddr2,
												@ShipAddr3,@ShipAddr4,@ShipProvince,@ShipRemark,'',
												@ContactID,@OfflineContactID,@ContactorSeqNo,@ContactorName,@ContactorPhone,@ContactorExtension,
												@ContactorFax,@ContactorEmail,@ContactMobileNo,@CountProduct,
												@GrandTotalAmt,@TotAmt,@NetAmt,@VatAmt,@VatRate,@OriginalTotalNetExcVatAmt,@OriginalTotalNetIncVatAmt,@VatProdNetAmt,
												@NonVatProdNetAmt,0,@DiscountAmt,@DiscountRate,@OrdDeliverFee,@DeliveryCharge,
												@PaymentCode,@PaymentType,'','',@CallbackRequest,@IPAddress,
												@ApproverRemark,@OFMRemark,@ReferenceRemark,@AttachFile,@UseSMSFeature,
												@CurrentAppID,@NextAppID,@ParkDay,@WarningExpireDate,
												@ExpireDate,@AdminAllowFlag,@DownloadStatus,'',
												getdate(),@CreateBy,getdate(),@UpdateBy,@CustFileName,@SystemFileName
											)

											Select @OrderID as OrderID ";

					switch (cartData.User.Company.OrderIDFormat)//check ว่าลูกค้าต้องการใช้ format orderid อะไร
					{
						case Company.OrderIDFormatType.TypeA: //TypeA CompID-CostID-Y2M2Running(5) - OFM-001-120100001
							command.Parameters.AddWithValue("@DocType", cartData.User.Company.OrderIDFormat.ToString());
							command.Parameters.AddWithValue("@Part1", cartData.User.CurrentCompanyId);
							command.Parameters.AddWithValue("@Part2", cartData.CurrentUsedCostCenter.CostCenterID);
							command.Parameters.AddWithValue("@Part3", string.Empty);
							break;
						case Company.OrderIDFormatType.TypeB: //TypeB CompID-DeptID-Y2M2-Running(5) - OFM-MK-120100001
							command.Parameters.AddWithValue("@DocType", cartData.User.Company.OrderIDFormat.ToString());
							command.Parameters.AddWithValue("@Part1", cartData.User.CurrentCompanyId);
							if (cartData.CurrentUsedCostCenter.CostCenterDepartment != null)
							{
								command.Parameters.AddWithValue("@Part2", cartData.CurrentUsedCostCenter.CostCenterDepartment.DepartmentID);
							}
							else
							{
								command.Parameters.AddWithValue("@Part2", string.Empty);
							}
							command.Parameters.AddWithValue("@Part3", string.Empty);
							break;
						case Company.OrderIDFormatType.TypeC: //TypeC DeptID-CostID-Y2M2Running(5) - MK-001-120100001
							command.Parameters.AddWithValue("@DocType", cartData.User.Company.OrderIDFormat.ToString());
							if (cartData.CurrentUsedCostCenter.CostCenterDepartment != null)
							{
								command.Parameters.AddWithValue("@Part1", cartData.CurrentUsedCostCenter.CostCenterDepartment.DepartmentID);
							}
							else
							{
								command.Parameters.AddWithValue("@Part1", string.Empty);
							}
							command.Parameters.AddWithValue("@Part2", cartData.CurrentUsedCostCenter.CostCenterID);
							command.Parameters.AddWithValue("@Part3", string.Empty);
							break;
						case Company.OrderIDFormatType.TypeD: //TypeD CostID-Y2M2Running(5) - 001-120100001
							command.Parameters.AddWithValue("@DocType", cartData.User.Company.OrderIDFormat.ToString());
							command.Parameters.AddWithValue("@Part1", cartData.CurrentUsedCostCenter.CostCenterID);
							command.Parameters.AddWithValue("@Part2", string.Empty);
							command.Parameters.AddWithValue("@Part3", string.Empty);
							break;
						case Company.OrderIDFormatType.Modify:
							command.Parameters.AddWithValue("@DocType", cartData.User.CurrentCompanyId);
							command.Parameters.AddWithValue("@Part1", cartData.User.CurrentCompanyId);
							if (cartData.CurrentUsedCostCenter.CostCenterDepartment != null)
							{
								command.Parameters.AddWithValue("@Part2", cartData.CurrentUsedCostCenter.CostCenterDepartment.DepartmentID);
							}
							else
							{
								command.Parameters.AddWithValue("@Part2", string.Empty);
							}
							command.Parameters.AddWithValue("@Part3", cartData.CurrentUsedCostCenter.CostCenterID);
							break;
						default: //Standard CompID-Y2M2-Running(5) - OFM-120100001
							command.Parameters.AddWithValue("@DocType", Company.OrderIDFormatType.Standard.ToString());
							command.Parameters.AddWithValue("@Part1", cartData.User.CurrentCompanyId);
							command.Parameters.AddWithValue("@Part2", string.Empty);
							command.Parameters.AddWithValue("@Part3", string.Empty);
							break;
					}

					command.Parameters.AddWithValue("@SCID", cartData.Cart.ShoppingCartId);
					command.Parameters.AddWithValue("@OrderBeforeStatus", GetOrderStatus(cartData.Cart.SendToAdminAppProd || cartData.Cart.SendToAdminAppBudg));
					command.Parameters.AddWithValue("@OrderStatus", GetOrderStatus(cartData.Cart.SendToAdminAppProd || cartData.Cart.SendToAdminAppBudg));
					command.Parameters.AddWithValue("@DownloadStatus", Order.DownloadStatus.Waiting.ToString());
					command.Parameters.AddWithValue("@CompanyID", cartData.User.CurrentCompanyId);
					if (cartData.CurrentUsedCostCenter.CostCenterDepartment != null)
					{
						command.Parameters.AddWithValue("@DepartmentID", cartData.CurrentUsedCostCenter.CostCenterDepartment.DepartmentID);
					}
					else
					{
						command.Parameters.AddWithValue("@DepartmentID", "");
					}
					command.Parameters.AddWithValue("@CostcenterID", cartData.CurrentUsedCostCenter.CostCenterID);
					command.Parameters.AddWithValue("@CustID", cartData.CurrentUsedCostCenter.CostCenterCustID);
					command.Parameters.AddWithValue("@CustName", cartData.CurrentUsedCostCenter.CostCenterCustName);

					command.Parameters.AddWithValue("@UserID", cartData.User.UserId);
					command.Parameters.AddWithValue("@UserTName", cartData.User.UserThaiName);// น่าจะลงแค่ UserName ไม่ต้องแยก ไทย/อังกฤษ
					command.Parameters.AddWithValue("@UserEName", cartData.User.UserEngName);// น่าจะลงแค่ UserName ไม่ต้องแยก ไทย/อังกฤษ

					command.Parameters.AddWithValue("@InvAddr1", cartData.CurrentUsedCostCenter.CostCenterInvoice.Address1);
					command.Parameters.AddWithValue("@InvAddr2", cartData.CurrentUsedCostCenter.CostCenterInvoice.Address2);
					command.Parameters.AddWithValue("@InvAddr3", cartData.CurrentUsedCostCenter.CostCenterInvoice.Address3);
					command.Parameters.AddWithValue("@InvAddr4", cartData.CurrentUsedCostCenter.CostCenterInvoice.Address4);

					command.Parameters.AddWithValue("@ShipID", cartData.CurrentUsedCostCenter.CostCenterShipping.ShipID);
					command.Parameters.AddWithValue("@OfflineShipID", cartData.CurrentUsedCostCenter.CostCenterShipping.OfflineShipID);
					command.Parameters.AddWithValue("@ShipContactor", cartData.CurrentUsedCostCenter.CostCenterShipping.ShipContactor);
					command.Parameters.AddWithValue("@ShipPhoneNo", cartData.CurrentUsedCostCenter.CostCenterShipping.ShipPhoneNo);
					command.Parameters.AddWithValue("@ShipMobileNo", cartData.CurrentUsedCostCenter.CostCenterShipping.ShipMobileNo);
					command.Parameters.AddWithValue("@ShippingSeqNo", cartData.CurrentUsedCostCenter.CostCenterShipping.ShippingSeqNo);
					command.Parameters.AddWithValue("@ShipAddr1", cartData.CurrentUsedCostCenter.CostCenterShipping.Address1);
					command.Parameters.AddWithValue("@ShipAddr2", cartData.CurrentUsedCostCenter.CostCenterShipping.Address2);
					command.Parameters.AddWithValue("@ShipAddr3", cartData.CurrentUsedCostCenter.CostCenterShipping.Address3);
					command.Parameters.AddWithValue("@ShipAddr4", cartData.CurrentUsedCostCenter.CostCenterShipping.Address4);
					command.Parameters.AddWithValue("@ShipProvince", cartData.CurrentUsedCostCenter.CostCenterShipping.Province);
					command.Parameters.AddWithValue("@ShipRemark", cartData.CurrentUsedCostCenter.CostCenterShipping.Remark);

					command.Parameters.AddWithValue("@ContactID", cartData.CurrentUsedCostCenter.CostCenterContact.ContactID);
					command.Parameters.AddWithValue("@OfflineContactID", cartData.CurrentUsedCostCenter.CostCenterContact.OfflineContactID);
					command.Parameters.AddWithValue("@ContactorSeqNo", cartData.CurrentUsedCostCenter.CostCenterContact.ContactSeqNo);
					command.Parameters.AddWithValue("@ContactorName", cartData.CurrentUsedCostCenter.CostCenterContact.ContactorName);
					command.Parameters.AddWithValue("@ContactorPhone", cartData.CurrentUsedCostCenter.CostCenterContact.ContactorPhone);
					command.Parameters.AddWithValue("@ContactorExtension", cartData.CurrentUsedCostCenter.CostCenterContact.ContactorExtension);
					command.Parameters.AddWithValue("@ContactorFax", cartData.CurrentUsedCostCenter.CostCenterContact.ContactorFax);
					command.Parameters.AddWithValue("@ContactorEmail", cartData.CurrentUsedCostCenter.CostCenterContact.Email);
					command.Parameters.AddWithValue("@ContactMobileNo", cartData.CurrentUsedCostCenter.CostCenterContact.ContactMobileNo);

					command.Parameters.AddWithValue("@CountProduct", cartData.Cart.ItemCountCart);
					command.Parameters.AddWithValue("@GrandTotalAmt", cartData.Cart.GrandTotalAmt);
					command.Parameters.AddWithValue("@TotAmt", cartData.Cart.TotalAmt);
					command.Parameters.AddWithValue("@NetAmt", cartData.Cart.TotalPriceNet);
					command.Parameters.AddWithValue("@VatAmt", cartData.Cart.TotalVatAmt);
					command.Parameters.AddWithValue("@VatRate", cartData.Cart.VatRate);

					command.Parameters.AddWithValue("@OriginalTotalNetExcVatAmt", cartData.Cart.OriginalTotalNetExcVatAmt);
					command.Parameters.AddWithValue("@OriginalTotalNetIncVatAmt", cartData.Cart.OriginalTotalNetIncVatAmt);
					command.Parameters.AddWithValue("@VatProdNetAmt", cartData.Cart.TotalPriceProductExcVatWithDisCountAmount);
					command.Parameters.AddWithValue("@NonVatProdNetAmt", cartData.Cart.TotalPriceProductNoneVatWithDisCountAmount);
					command.Parameters.AddWithValue("@DiscountAmt", cartData.Cart.TotalAllDiscount);
					command.Parameters.AddWithValue("@DiscountRate", cartData.Cart.DisCountRate);
					command.Parameters.AddWithValue("@OrdDeliverFee", cartData.Cart.TotalDeliveryFee);
					command.Parameters.AddWithValue("@DeliveryCharge", cartData.Cart.TotalDeliveryCharge);
					command.Parameters.AddWithValue("@PaymentCode", Mapper.ToDatabase(cartData.CurrentUsedCostCenter.CostCenterPayment.Code));
					command.Parameters.AddWithValue("@PaymentType", Mapper.ToDatabase(cartData.CurrentUsedCostCenter.CostCenterPayment.Type));
					command.Parameters.AddWithValue("@CallbackRequest", Mapper.ToDatabase(cartData.IsCallBackRequestStatus));
					command.Parameters.AddWithValue("@IPAddress", !String.IsNullOrEmpty(cartData.User.IPAddress) ? cartData.User.IPAddress : "");
					command.Parameters.AddWithValue("@ApproverRemark", !String.IsNullOrEmpty(cartData.ApproverRemark) ? cartData.ApproverRemark : "");
					command.Parameters.AddWithValue("@OFMRemark", !String.IsNullOrEmpty(cartData.OFMRemark) ? cartData.OFMRemark : "");
					command.Parameters.AddWithValue("@ReferenceRemark", !String.IsNullOrEmpty(cartData.ReferenceRemark) ? cartData.ReferenceRemark : "");
					command.Parameters.AddWithValue("@AttachFile", String.Empty);
					command.Parameters.AddWithValue("@ParkDay", cartData.CurrentSelectApprover.ParkDay);
					command.Parameters.AddWithValue("@CreateBy", cartData.User.UserId);
					command.Parameters.AddWithValue("@UpdateBy", cartData.User.UserId);
					command.Parameters.AddWithValue("@WarningExpireDate", CalculateWarningExpireDate(DateTime.Now, cartData.CurrentSelectApprover.ParkDay).ToString("yyyy/MM/dd HH:mm"));
					command.Parameters.AddWithValue("@ExpireDate", CalculateExprieDate(DateTime.Now, cartData.CurrentSelectApprover.ParkDay).ToString("yyyy/MM/dd HH:mm"));
					command.Parameters.AddWithValue("@AdminAllowFlag", "No");
					command.Parameters.AddWithValue("@CurrentAppID", GetApproverID(cartData.CurrentSelectApprover.Approver.UserId, cartData.User.CurrentCompanyId, cartData.Cart.SendToAdminAppProd || cartData.Cart.SendToAdminAppBudg)); // หาว่าจะส่งไปที่ Approver คนไหน
					command.Parameters.AddWithValue("@NextAppID", GetNextApproverID(cartData.CurrentSelectApprover.Approver.UserId, cartData.User.CurrentCompanyId, cartData.costcenterId, cartData.Cart.SendToAdminAppProd || cartData.Cart.SendToAdminAppBudg, cartData.User.UserId)); //ตรวจสอบตาม Logic ใหม่
					command.Parameters.AddWithValue("@UseSMSFeature", cartData.UseSMSFeature.HasValue ? Mapper.ToDatabase(cartData.UseSMSFeature.Value) : Mapper.ToDatabase(false));
					command.Parameters.AddWithValue("@CustFileName", !String.IsNullOrEmpty(cartData.Cart.CustFileName) ? cartData.Cart.CustFileName : "");
					command.Parameters.AddWithValue("@SystemFileName", !String.IsNullOrEmpty(cartData.Cart.CustFileName) ? cartData.Cart.SystemFileName : "");

					string orderId = (string)command.ExecuteScalar();

					/*----------------------------- สร้าง OrderDetail--------------------------------------*/
					int rowIndex = 0;
					command.CommandText = @"Insert Into TBOrderDetail
											(
												OrderID,SeqNo,PID,pTName,pEName,Qty,OldQty,
												pTUnit,pEUnit,IsFree,IncVatPrice,ExcVatPrice,UnitPriceIncVat,
												DiscountRate,DiscAmt,DeliverFeeItem,Isvat,PriceFlag,
												ProdType,BestDealFlag,CreateOn,CreateBy,UpdateOn,UpdateBy
											)
											Values
											(
												@OrderID,@SeqNo,@Pid,@ProductThaiName,@ProductEngName,@Qty,@Qty,
												@SaleThaiUnit,@SaleEngUnit,@IsFree,@IncVatPrice,@ExcVatPrice,@UnitPriceIncVat,
												@DiscountRate,@DiscAmt,@DeliverFeeItem,@Isvat,@PriceFlag,
												@ProdType,@BestDealFlag,getdate(),@CreateBy,getdate(),@UpdateBy
											)";

					foreach (var item in cartData.Cart.ItemCart)
					{
						command.Parameters.Clear();
						command.Parameters.AddWithValue("@SeqNo", rowIndex + 1);
						command.Parameters.AddWithValue("@Pid", item.Product.Id);
						command.Parameters.AddWithValue("@ProductThaiName", item.Product.ThaiName);
						command.Parameters.AddWithValue("@ProductEngName", item.Product.EngName);
						command.Parameters.AddWithValue("@SaleThaiUnit", item.Product.ThaiUnit);
						command.Parameters.AddWithValue("@SaleEngUnit", item.Product.EngUnit);
						command.Parameters.AddWithValue("@Qty", item.Quantity);
						command.Parameters.AddWithValue("@IsFree", "No");
						command.Parameters.AddWithValue("@IncVatPrice", item.ItemIncVatPrice);
						command.Parameters.AddWithValue("@UnitPriceIncVat", item.Product.PriceIncVat);
						command.Parameters.AddWithValue("@ExcVatPrice", item.Product.PriceExcVat);
						command.Parameters.AddWithValue("@DiscountRate", (item.Product.IsBestDeal || item.Product.IsPromotion) ? 0 : cartData.Cart.DisCountRate);
						command.Parameters.AddWithValue("@DiscAmt", item.ItemPriceDiscount);
						command.Parameters.AddWithValue("@DeliverFeeItem", cartData.CurrentUsedCostCenter.IsDeliCharge ? item.Product.TransCharge : 0);
						command.Parameters.AddWithValue("@Isvat", Mapper.ToDatabase(item.Product.IsVat));
						command.Parameters.AddWithValue("@PriceFlag", Mapper.ToDatabase(item.Product.PriceFlag));
						command.Parameters.AddWithValue("@ProdType", Mapper.ToDatabase(item.Product.PriceFlag));
						command.Parameters.AddWithValue("@BestDealFlag", Mapper.ToDatabase(item.Product.IsBestDeal || item.Product.IsPromotion));
						command.Parameters.AddWithValue("@CreateBy", cartData.User.UserId);
						command.Parameters.AddWithValue("@UpdateBy", cartData.User.UserId);
						command.Parameters.AddWithValue("@OrderID", orderId);

						command.ExecuteNonQuery();
						rowIndex++;
					}

					command.Parameters.Clear();
					command.CommandText = @"Delete From TBCartDetail Where SCID = @SCID
											Update TBCart Set CountProduct=0 , CustFileName='',SystemFileName='' Where SCID = @SCID";
					command.Parameters.AddWithValue("@SCID", cartData.Cart.ShoppingCartId);
					command.ExecuteNonQuery();

					// commit transaction
					scope.Complete();
					return orderId;
				}
			}
		}

		public string UpdateOrderEditByRequester(CartRevise cartData)
		{
			using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
			{
				using (SqlCommand command = CreateCommandEnsureConnectionOpen())
				{
					StringBuilder sqlBuilder = new StringBuilder(2048);

					command.CommandText = @"DECLARE @OrderBeforeStatus as varchar(max) SELECT @OrderBeforeStatus=OrderStatus FROM TBOrder Where OrderID=@OrderId

											UPDATE	TBOrder Set
													CountProduct=@CountProducts,GrandTotalAmt=@GrandTotalAmt,TotAmt=@TotAmt,NetAmt=@NetAmt,VatAmt=@VatAmt,
													VatRate=@VatRate,VatProdNetAmt=@VatProdNetAmt,NonVatProdNetAmt=@NonVatProdNetAmt,
													DiscountAmt=@DiscountAmt,DiscountRate=@DiscountRate,OrdDeliverFee=@OrdDeliverFee,DeliveryFee=@DeliveryCharge,
													NumOfAdjust=NumOfAdjust+1,IPAddress=@IPAddress,UpdateBy=@UpdateBy,UpdateOn=getdate(),
													ApproverRemark=@ApproverRemark,OFMRemark=@OFMRemark,ReferenceRemark=@ReferenceRemark,OrderBeforeStatus=@OrderBeforeStatus,OrderStatus=@OrderStatus,
													CurrentAppID=@CurrentAppID,NextAppID=@NextAppID,ParkDay=@ParkDay,WarningExpireDate=@WarningExpireDate,ExpireDate=@ExpireDate
											Where	OrderID = @OrderId

											Delete TBOrderDetail Where OrderID = @OrderId";

					command.Parameters.AddWithValue("@OrderId", cartData.Cart.RefOrderId);
					command.Parameters.AddWithValue("@SCID", cartData.Cart.ShoppingCartId);
					command.Parameters.AddWithValue("@CountProducts", cartData.Cart.ItemCountCart);
					command.Parameters.AddWithValue("@GrandTotalAmt", cartData.Cart.GrandTotalAmt);
					command.Parameters.AddWithValue("@TotAmt", cartData.Cart.TotalAmt);
					command.Parameters.AddWithValue("@NetAmt", cartData.Cart.TotalPriceNet);
					command.Parameters.AddWithValue("@VatAmt", cartData.Cart.TotalVatAmt);
					command.Parameters.AddWithValue("@VatRate", cartData.Cart.VatRate);
					command.Parameters.AddWithValue("@VatProdNetAmt", cartData.Cart.TotalPriceProductExcVatWithDisCountAmount);
					command.Parameters.AddWithValue("@NonVatProdNetAmt", cartData.Cart.TotalPriceProductNoneVatWithDisCountAmount);
					command.Parameters.AddWithValue("@DiscountAmt", cartData.Cart.TotalAllDiscount);
					command.Parameters.AddWithValue("@DiscountRate", cartData.Cart.DisCountRate);
					command.Parameters.AddWithValue("@OrdDeliverFee", cartData.Cart.TotalDeliveryFee);
					command.Parameters.AddWithValue("@DeliveryCharge", cartData.Cart.TotalDeliveryCharge);
					command.Parameters.AddWithValue("@IPAddress", !String.IsNullOrEmpty(cartData.User.IPAddress) ? cartData.User.IPAddress : "");
					command.Parameters.AddWithValue("@UpdateBy", cartData.User.UserId);
					command.Parameters.AddWithValue("@ApproverRemark", !String.IsNullOrEmpty(cartData.ApproverRemark) ? cartData.ApproverRemark : "");
					command.Parameters.AddWithValue("@OFMRemark", !String.IsNullOrEmpty(cartData.OFMRemark) ? cartData.OFMRemark : "");
					command.Parameters.AddWithValue("@ReferenceRemark", !String.IsNullOrEmpty(cartData.ReferenceRemark) ? cartData.ReferenceRemark : "");
					command.Parameters.AddWithValue("@OrderStatus", GetOrderStatus(cartData.Cart.SendToAdminAppProd || cartData.Cart.SendToAdminAppBudg || cartData.Order.OldStatus == Order.OrderStatus.WaitingAdmin));
					command.Parameters.AddWithValue("@ParkDay", cartData.Order.NextApprover.ParkDay);
					command.Parameters.AddWithValue("@WarningExpireDate", CalculateWarningExpireDate(DateTime.Now, cartData.Order.NextApprover.ParkDay));
					command.Parameters.AddWithValue("@ExpireDate", CalculateExprieDate(DateTime.Now, cartData.Order.NextApprover.ParkDay));
					command.Parameters.AddWithValue("@CurrentAppID", GetApproverID(cartData.Order.NextApprover.Approver.UserId, cartData.User.CurrentCompanyId, cartData.Cart.SendToAdminAppProd || cartData.Cart.SendToAdminAppBudg)); // หาว่าจะส่งไปที่ Approver คนไหน
					command.Parameters.AddWithValue("@NextAppID", GetNextApproverID(cartData.Order.NextApprover.Approver.UserId, cartData.User.CurrentCompanyId, cartData.Order.CostCenter.CostCenterID, cartData.Cart.SendToAdminAppProd || cartData.Cart.SendToAdminAppBudg, cartData.User.UserId)); //ตรวจสอบตาม Logic ใหม่
					command.ExecuteNonQuery();

					/*----------------------------- Create OrderDetail--------------------------------------*/
					int rowIndex = 0;
					command.CommandText = @"
											Insert Into TBOrderDetail
											(
												OrderID,SeqNo,PID,pTName,pEName,Qty,
												pTUnit,pEUnit,IsFree,IncVatPrice,ExcVatPrice,
												DiscountRate,DiscAmt,DeliverFeeItem,Isvat,PriceFlag,
												ProdType,BestDealFlag,CreateOn,CreateBy,UpdateOn,UpdateBy
											)
											Values
											(
												@OrderId,@SeqNo,@Pid,@ProductThaiName,@ProductEngName,@Qty,
												@SaleThaiUnit,@SaleEngUnit,@IsFree,@IncVatPrice,@ExcVatPrice,
												@DiscountRate,@DiscAmt,@DeliverFeeItem,@Isvat,@PriceFlag,
												@ProdType,@BestDealFlag,getdate(),@CreateBy,getdate(),@UpdateBy
											)";
					foreach (var item in cartData.Cart.ItemCart)
					{
						command.Parameters.Clear();
						command.Parameters.AddWithValue("@OrderId", cartData.Cart.RefOrderId);
						command.Parameters.AddWithValue("@SeqNo", rowIndex + 1);
						command.Parameters.AddWithValue("@Pid", item.Product.Id);
						command.Parameters.AddWithValue("@ProductThaiName", item.Product.ThaiName);
						command.Parameters.AddWithValue("@ProductEngName", item.Product.EngName);
						command.Parameters.AddWithValue("@SaleThaiUnit", item.Product.ThaiUnit);
						command.Parameters.AddWithValue("@SaleEngUnit", item.Product.EngUnit);
						command.Parameters.AddWithValue("@Qty", item.Quantity);
						command.Parameters.AddWithValue("@IsFree", "No");
						command.Parameters.AddWithValue("@IncVatPrice", item.Product.PriceIncVat);
						command.Parameters.AddWithValue("@ExcVatPrice", item.Product.PriceExcVat);
						command.Parameters.AddWithValue("@DiscountRate", (item.Product.IsBestDeal || item.Product.IsPromotion) ? 0 : cartData.Cart.DisCountRate);
						command.Parameters.AddWithValue("@DiscAmt", item.ItemPriceDiscount);
						command.Parameters.AddWithValue("@DeliverFeeItem", cartData.Cart.TotalDeliveryCharge > 0 ? item.Product.TransCharge : 0);
						command.Parameters.AddWithValue("@Isvat", Mapper.ToDatabase(item.Product.IsVat));
						command.Parameters.AddWithValue("@PriceFlag", Mapper.ToDatabase(item.Product.PriceFlag));
						command.Parameters.AddWithValue("@ProdType", Mapper.ToDatabase(item.Product.PriceFlag));
						command.Parameters.AddWithValue("@BestDealFlag", Mapper.ToDatabase(item.Product.IsBestDeal || item.Product.IsPromotion));
						command.Parameters.AddWithValue("@UpdateBy", cartData.User.UserId);
						command.Parameters.AddWithValue("@CreateBy", cartData.User.UserId);

						command.ExecuteNonQuery();
						rowIndex++;
					}

					/*----------------------------- Update OldQty in OrderDetail--------------------------------------*/
					command.CommandText = "Update TBOrderDetail set OldQty = @oldQty Where OrderID = @OrderId and pid = @pid";
					foreach (var item in cartData.Order.ItemOrders)
					{
						command.Parameters.Clear();
						command.Parameters.AddWithValue("@OrderId", cartData.Cart.RefOrderId);
						command.Parameters.AddWithValue("@pid", item.Product.Id);
						command.Parameters.AddWithValue("@oldQty", item.OldQuantity);

						command.ExecuteNonQuery();
					}

					command.CommandText = @"Delete From TBCartDetail Where SCID = @SCID
											Update TBCart Set CountProduct=0,scType='New',RefOrderId='' Where SCID = @SCID";
					command.Parameters.AddWithValue("@SCID", cartData.Cart.ShoppingCartId);
					command.ExecuteNonQuery();
				}
				scope.Complete();
			}
			return cartData.Cart.RefOrderId;
		}

		public string UpdateOrderEditbyApprover(Order order)
		{
			using (TransactionScope scope = new TransactionScope(TransactionScopeOption.RequiresNew))
			{
				using (SqlCommand command = CreateCommandEnsureConnectionOpen())
				{
					StringBuilder sqlBuilder = new StringBuilder(2048);

					sqlBuilder.AppendLine("UPDATE TBOrder Set ");
					sqlBuilder.AppendLine("CountProduct=@CountProduct,GrandTotalAmt=@GrandTotalAmt,TotAmt=@TotAmt,NetAmt=@NetAmt,");
					sqlBuilder.AppendLine("VatAmt=@VatAmt,VatProdNetAmt=@VatProdNetAmt,NonVatProdNetAmt=@NonVatProdNetAmt,");
					sqlBuilder.AppendLine("DiscountAmt=@DiscountAmt,DeliveryFee=@DeliveryCharge,OrdDeliverFee=@OrdDeliverFee,");
					sqlBuilder.AppendLine("NumOfAdjust=NumOfAdjust+1,LastAdjustOn=getdate(),LastAdjustBy=@LastAdjustBy,");
					sqlBuilder.AppendLine("IPAddress=@IPAddress,UpdateOn=getdate(),UpdateBy=@UpdateBy");
					sqlBuilder.AppendLine("Where OrderID = @OrderId ");

					int rowIndex = 0;

					foreach (var item in order.ItemOrders)
					{
						sqlBuilder.AppendFormat("UPDATE TBOrderDetail SET OldQty=Qty, Qty=@Qty_{0}, DiscAmt=@DiscAmt_{0}, UpdateBy=@UpdateBy, UpdateOn=getdate() Where OrderID=@OrderId AND pID=@pID_{0}", rowIndex);
						sqlBuilder.AppendLine();

						command.Parameters.AddWithValue("@Pid_" + rowIndex, item.Product.Id);
						command.Parameters.AddWithValue("@Qty_" + rowIndex, item.Quantity);
						command.Parameters.AddWithValue("@DiscAmt_" + rowIndex, item.DiscAmt);
						rowIndex++;
					}

					sqlBuilder.AppendLine("DELETE FROM TBOrderDetail WHERE OrderID = @OrderId AND Qty = 0");

					command.Parameters.AddWithValue("@OrderId", order.OrderID);
					command.Parameters.AddWithValue("@CountProduct", order.ItemCountOrder);
					command.Parameters.AddWithValue("@GrandTotalAmt", order.GrandTotalAmt);
					command.Parameters.AddWithValue("@TotAmt", order.TotalAmt);
					command.Parameters.AddWithValue("@NetAmt", order.TotalNetAmt);
					command.Parameters.AddWithValue("@VatAmt", order.TotalVatAmt);
					command.Parameters.AddWithValue("@VatProdNetAmt", order.TotalPriceProductExcVatAmount);
					command.Parameters.AddWithValue("@NonVatProdNetAmt", order.TotalPriceProductNoneVatAmount);
					command.Parameters.AddWithValue("@DiscountAmt", order.TotalAllDiscount);
					command.Parameters.AddWithValue("@OrdDeliverFee", 0);
					command.Parameters.AddWithValue("@DeliveryCharge", order.TotalDeliveryCharge);
					command.Parameters.AddWithValue("@LastAdjustBy", order.User.UserId);
					command.Parameters.AddWithValue("@IPAddress", order.User.IPAddress);
					command.Parameters.AddWithValue("@UpdateBy", order.User.UserId);

					command.CommandText = sqlBuilder.ToString();
					command.ExecuteNonQuery();
				}
				scope.Complete();
			}
			return order.OrderID;
		}

		public void CreateOrderActivity(OrderActivity orderActivity)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBOrderActivity (OrderID,ActivityTName,ActivityEName,Remark,IPAddress,CreateOn,CreateBy)
										values (@OrderID,@ActivityTName,@ActivityEName,@Remark,@IPAddress,getdate(),@CreateBy)";

				command.Parameters.AddWithValue("@OrderID", orderActivity.OrderId);
				command.Parameters.AddWithValue("@ActivityTName", orderActivity.ActivityTName);
				command.Parameters.AddWithValue("@ActivityEName", orderActivity.ActivityEName);
				command.Parameters.AddWithValue("@Remark", orderActivity.Remark);
				command.Parameters.AddWithValue("@IPAddress", orderActivity.IPAddress);
				command.Parameters.AddWithValue("@CreateBy", orderActivity.CreateBy);
				command.ExecuteNonQuery();
			}
		}

		public IEnumerable<OrderActivity> GetOrderActivity(string orderId)
		{
			List<OrderActivity> item = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBOrderActivity Where orderId = @orderId ORDER BY CreateOn DESC ";
				command.Parameters.AddWithValue("@orderId", orderId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					OrderActivity orderActivity = null;
					item = new List<OrderActivity>();
					while (reader.Read())
					{
						orderActivity = new OrderActivity();
						orderActivity.OrderId = (string)reader["OrderId"];
						orderActivity.CreateOn = (DateTime)reader["CreateOn"];
						orderActivity.ActivityName = new LocalizedString((string)reader["ActivityTName"]);
						orderActivity.ActivityName["en"] = (string)reader["ActivityEName"];
						orderActivity.Remark = (string)reader["Remark"];
						orderActivity.CreateBy = (string)reader["CreateBy"];
						item.Add(orderActivity);
					}
				}
				return item;
			}
		}

		public IEnumerable<ProcessLog> GetOrderProcessLog(string orderId)
		{
			List<ProcessLog> logs = new List<ProcessLog>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBOrderProcessLog with (nolock) WHERE OrderID=@orderId ORDER BY CreateOn DESC";
				command.Parameters.AddWithValue("@orderId", orderId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					ProcessLog log = null;
					while (reader.Read())
					{
						log = new ProcessLog();
						log.LogID = (int)reader["LogID"];
						log.OrderId = (string)reader["OrderID"];
						log.ProcessRemark = (string)reader["ProcessRemark"];
						log.Remark = (string)reader["Remark"];
						log.IPAddress = (string)reader["IPAddress"];
						log.CreateBy = (string)reader["CreateBy"];
						log.CreateOn = (DateTime)reader["CreateOn"];
						logs.Add(log);
					}
				}
				return logs;
			}
		}

		public IEnumerable<MailTransLog> GetOrderMailTransLog(string orderId)
		{
			List<MailTransLog> logs = new List<MailTransLog>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBMailTransLog with (nolock) WHERE OrderID=@orderId ORDER BY CreateOn DESC";
				command.Parameters.AddWithValue("@orderId", orderId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					MailTransLog log = null;
					while (reader.Read())
					{
						log = new MailTransLog();
						log.LogID = (int)reader["LogID"];
						log.OrderId = (string)reader["OrderID"];
						log.EmailFrom = (string)reader["EmailFrom"];
						log.EmailTo = (string)reader["EmailTo"];
						log.EmailCC = (string)reader["EmailCC"];
						log.EmailBCC = (string)reader["EmailBCC"];
						log.Subject = (string)reader["Subject"];
						log.MailContent = (string)reader["MailContent"];
						log.MailCategory = (MailType)Enum.Parse(typeof(MailType), (string)reader["MailCategory"]);
						log.SendMailSuccess = (string)reader["SendMailSuccess"];
						log.ErrorDesc = (string)reader["ErrorDesc"];
						log.CreateBy = (string)reader["CreateBy"];
						log.CreateOn = (DateTime)reader["CreateOn"];
						logs.Add(log);
					}
				}
				return logs;
			}
		}

		public void UpdateProcessRemark(string orderId, string processRemark, string remark, string userId, string ipAddress)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Insert Into TBOrderActivity(OrderID, Activity, Remark, ipAddress, ActionBy, ActionDate)
										Values(@orderId, 'ออฟฟิศเมทดำเนินการ', @processRemark, @ipAddress, @userId, getdate())

										Insert Into TBProcessLog
										Values(@orderId, 'Officemate Process', @processRemark + @remarkOfProcessRemark, @ipAddress, @userId, getdate())";

				command.Parameters.AddWithValue("@processRemark", processRemark);
				command.Parameters.AddWithValue("@remarkOfProcessRemark", !String.IsNullOrEmpty(remark) ? ": " + remark : String.Empty);
				command.Parameters.AddWithValue("@orderId", orderId);
				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@ipAddress", ipAddress);
				command.ExecuteNonQuery();
			}
		}

		public void UpdateShippingType(string orderId, string shippingType, string remark, string userId, string ipAddress)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string sqlCommand = @"Update TBOrder
									Set ShipType = @shippingType, UpdateOn = getdate(), UpdateBy = @userId
									Where OrderID = @orderId

									Insert Into TBProcessLog
									Values(@orderId, 'Shipping', 'ส่งสินค้าโดย {0} {1}', @ipAddress, @userId, getdate())";

				command.CommandText = String.Format(sqlCommand, shippingType, !String.IsNullOrEmpty(remark) ? ": " + remark : String.Empty);

				command.Parameters.AddWithValue("@shippingType", shippingType);
				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@orderId", orderId);
				command.Parameters.AddWithValue("@ipAddress", ipAddress);

				command.ExecuteNonQuery();
			}
		}

		public void CreateNewOrderTransactionLog(OrderData orderData)
		{
			CreateOrderTransactionLog(orderData, OrderData.OrderStatusLog.CreateNewOrder);
		}
		public void CreateWatingOrderTransactionLog(OrderData orderData)
		{
			CreateOrderTransactionLog(orderData, OrderData.OrderStatusLog.Waiting);
		}
		public void CreateAllowWattingOrderTransactionLog(OrderData orderData)
		{
			CreateOrderTransactionLog(orderData, OrderData.OrderStatusLog.WaitingAdmin);
		}
		public void CreateReviseOrderTransactionLog(OrderData orderData)
		{
			CreateOrderTransactionLog(orderData, OrderData.OrderStatusLog.Revise);
		}
		public void CreateApprovedOrderTransactionLog(OrderData orderData)
		{
			CreateOrderTransactionLog(orderData, OrderData.OrderStatusLog.Approved);
		}
		public void CreatePartialApproveOrderTransactionLog(OrderData orderData)
		{
			CreateOrderTransactionLog(orderData, OrderData.OrderStatusLog.Partial);
		}
		public void CreateAdminPartialApproveOrderTransactionLog(OrderData orderData)
		{
			CreateOrderTransactionLog(orderData, OrderData.OrderStatusLog.AdminAllow);
		}
		public void CreateDeleteOrderTransactionLog(OrderData orderData)
		{
			CreateOrderTransactionLog(orderData, OrderData.OrderStatusLog.Deleted);
		}

		private void CreateOrderTransactionLog(OrderData orderData, OrderData.OrderStatusLog OrderStatusLog)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert	TBOrderTransLog
												(OrderID,ActionDate,ActionUserID,ActionTDesc,ActionEDesc,BeforeStatus,AfterStatus,
												APPUserID,APPLevel,APPValue,NextAPPUserID,NextAPPLevel,NextAPPValue,ActionRemark,
												RemarkExpire,CreateOn,CreateBy)
										VALUES	(@OrderID,getdate(),@ActionUserID,@ActionTDesc,@ActionEDesc,@BeforeStatus,@AfterStatus,
												@APPUserID,@APPLevel,@APPValue,@NextAPPUserID,@NextAPPLevel,@NextAPPValue,@ActionRemark,
												@RemarkExpire,getdate(),@CreateBy)";

				command.Parameters.AddWithValue("@OrderID", orderData.Order.OrderID);
				command.Parameters.AddWithValue("@AfterStatus", orderData.Order.Status.ToString());
				command.Parameters.AddWithValue("@ActionUserID", orderData.CurrentOrderActivity.CreateBy);
				command.Parameters.AddWithValue("@ActionTDesc", orderData.CurrentOrderActivity.ActivityTName);
				command.Parameters.AddWithValue("@ActionEDesc", orderData.CurrentOrderActivity.ActivityEName);
				command.Parameters.AddWithValue("@ActionRemark", orderData.Order.ApproverRemark);
				command.Parameters.AddWithValue("@CreateBy", orderData.CurrentOrderActivity.CreateBy);
				command.Parameters.AddWithValue("@RemarkExpire", "");

				switch (OrderStatusLog)
				{
					case OrderData.OrderStatusLog.CreateNewOrder:
						command.Parameters.AddWithValue("@BeforeStatus", "");
						command.Parameters.AddWithValue("@APPUserID", orderData.Order.CurrentApprover.Approver.UserId);
						command.Parameters.AddWithValue("@APPLevel", orderData.Order.CurrentApprover.Level);
						command.Parameters.AddWithValue("@APPValue", orderData.Order.CurrentApprover.ApproveCreditLimit);
						if (orderData.Order.NextApprover == null)
						{
							command.Parameters.AddWithValue("@NextAPPUserID", "");
							command.Parameters.AddWithValue("@NextAPPLevel", "");
							command.Parameters.AddWithValue("@NextAPPValue", 0);
						}
						else
						{
							command.Parameters.AddWithValue("@NextAPPUserID", orderData.Order.NextApprover.Approver.UserId);
							command.Parameters.AddWithValue("@NextAPPLevel", orderData.Order.NextApprover.Level);
							command.Parameters.AddWithValue("@NextAPPValue", orderData.Order.NextApprover.ApproveCreditLimit);
						}
						break;
					case OrderData.OrderStatusLog.Waiting:
						command.Parameters.AddWithValue("@BeforeStatus", orderData.Order.OldStatus.ToString());
						command.Parameters.AddWithValue("@APPUserID", orderData.Order.CurrentApprover.Approver.UserId);
						command.Parameters.AddWithValue("@APPLevel", orderData.Order.CurrentApprover.Level);
						command.Parameters.AddWithValue("@APPValue", orderData.Order.CurrentApprover.ApproveCreditLimit);
						if (orderData.Order.NextApprover == null)
						{
							command.Parameters.AddWithValue("@NextAPPUserID", "");
							command.Parameters.AddWithValue("@NextAPPLevel", "");
							command.Parameters.AddWithValue("@NextAPPValue", 0);
						}
						else
						{
							command.Parameters.AddWithValue("@NextAPPUserID", orderData.Order.NextApprover.Approver.UserId);
							command.Parameters.AddWithValue("@NextAPPLevel", orderData.Order.NextApprover.Level);
							command.Parameters.AddWithValue("@NextAPPValue", orderData.Order.NextApprover.ApproveCreditLimit);
						}
						break;
					case OrderData.OrderStatusLog.Partial:
					case OrderData.OrderStatusLog.WaitingAdmin:
					case OrderData.OrderStatusLog.AdminAllow:
						command.Parameters.AddWithValue("@BeforeStatus", orderData.Order.OldStatus.ToString());
						command.Parameters.AddWithValue("@APPUserID", orderData.Order.PreviousApprover.Approver.UserId);
						command.Parameters.AddWithValue("@APPLevel", orderData.Order.PreviousApprover.Level);
						command.Parameters.AddWithValue("@APPValue", orderData.Order.PreviousApprover.ApproveCreditLimit);
						command.Parameters.AddWithValue("@NextAPPUserID", orderData.Order.CurrentApprover.Approver.UserId);
						command.Parameters.AddWithValue("@NextAPPLevel", orderData.Order.CurrentApprover.Level);
						command.Parameters.AddWithValue("@NextAPPValue", orderData.Order.CurrentApprover.ApproveCreditLimit);
						break;
					case OrderData.OrderStatusLog.Revise:
						command.Parameters.AddWithValue("@BeforeStatus", orderData.Order.OldStatus.ToString());
						command.Parameters.AddWithValue("@APPUserID", orderData.Order.PreviousApprover.Approver.UserId);
						command.Parameters.AddWithValue("@APPLevel", orderData.Order.PreviousApprover.Level);
						command.Parameters.AddWithValue("@APPValue", orderData.Order.PreviousApprover.ApproveCreditLimit);
						command.Parameters.AddWithValue("@NextAPPUserID", orderData.Order.NextApprover.Approver.UserId);
						command.Parameters.AddWithValue("@NextAPPLevel", orderData.Order.NextApprover.Level);
						command.Parameters.AddWithValue("@NextAPPValue", orderData.Order.NextApprover.ApproveCreditLimit);
						break;
					case OrderData.OrderStatusLog.Approved:
					case OrderData.OrderStatusLog.Deleted:
						command.Parameters.AddWithValue("@BeforeStatus", orderData.Order.OldStatus.ToString());
						command.Parameters.AddWithValue("@APPUserID", orderData.Order.PreviousApprover.Approver.UserId);
						command.Parameters.AddWithValue("@APPLevel", orderData.Order.PreviousApprover.Level);
						command.Parameters.AddWithValue("@APPValue", orderData.Order.PreviousApprover.ApproveCreditLimit);
						command.Parameters.AddWithValue("@NextAPPUserID", "");
						command.Parameters.AddWithValue("@NextAPPLevel", "");
						command.Parameters.AddWithValue("@NextAPPValue", 0);
						break;
					case OrderData.OrderStatusLog.Expired:
						break;
					case OrderData.OrderStatusLog.Shipped:
						break;
					case OrderData.OrderStatusLog.Completed:
						break;
					default:
						break;
				}
				command.ExecuteNonQuery();
			}
		}

		public void CreateOrderDetailTransactionLog(OrderData orderData, OrderData.OrderStatusLog OrderStatusLog)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				int rowIndex = 0;

				StringBuilder sqlBuilder = new StringBuilder();
				List<OrderDetail> OrderDetail = orderData.Order.ItemOrders.ToList();
				foreach (var item in orderData.Order.ItemOrders)
				{

					sqlBuilder.AppendFormat(@"insert	TBOrderDetailTransLog
											(
												OrderID,EditBy,pID,EditOn,OldQty,EditQty,NewQty,
												EditTReason,EditEReason,EditFlag,EditRemark,CreateOn,CreateBy
											)
											VALUES
											(
												@OrderID_{0},@EditBy_{0},@pID_{0},getdate(),@OldQty_{0},@EditQty_{0},@NewQty_{0},
												@EditTReason_{0},@EditEReason_{0},@EditFlag_{0},@EditRemark_{0},getdate(),@CreateBy_{0}
											)", rowIndex);

					sqlBuilder.AppendLine();
					command.Parameters.AddWithValue("@OrderID_" + rowIndex, orderData.Order.OrderID);
					command.Parameters.AddWithValue("@EditBy_" + rowIndex, orderData.User.UserId);
					command.Parameters.AddWithValue("@pID_" + rowIndex, item.Product.Id);
					command.Parameters.AddWithValue("@OldQty_" + rowIndex, item.OldQuantity);
					command.Parameters.AddWithValue("@EditQty_" + rowIndex, Math.Abs(item.OldQuantity - item.Quantity));
					command.Parameters.AddWithValue("@NewQty_" + rowIndex, item.Quantity);
					command.Parameters.AddWithValue("@EditRemark_" + rowIndex, orderData.Order.ReferenceRemark);
					command.Parameters.AddWithValue("@EditTReason_" + rowIndex, orderData.OrderStatusLogThaiName(OrderStatusLog));
					command.Parameters.AddWithValue("@EditEReason_" + rowIndex, orderData.OrderStatusLogEngName(OrderStatusLog));
					command.Parameters.AddWithValue("@CreateBy_" + rowIndex, orderData.User.UserId);

					if (OrderStatusLog.Equals(OrderData.OrderStatusLog.Revise)) { command.Parameters.AddWithValue("@EditFlag_" + rowIndex, "Yes"); }
					else { command.Parameters.AddWithValue("@EditFlag_" + rowIndex, "No"); }
					command.CommandText = sqlBuilder.ToString();
					rowIndex++;

				}

				if (OrderStatusLog.Equals(OrderData.OrderStatusLog.Revise))
				{
					sqlBuilder = new StringBuilder();
					sqlBuilder.Append(@"delete	from TBOrderDetailTransLog
										where	PID in(SELECT PID  from TBOrderDetailTransLog where orderId = @orderID AND EditFlag ='No')
												AND orderId = @orderID  AND EditFlag ='Yes' AND EditQty = 0");
					command.Parameters.AddWithValue("@orderID", orderData.Order.OrderID);
					command.CommandText += sqlBuilder.ToString();
				}
				command.ExecuteNonQuery();
			}
		}

		public void UpdateOrderActivity(OrderActivity orderActivity)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"update TBOrderActivity 
										set ActivityTName = @ActivityTName, ActivityEName = @ActivityEName, Remark = @Remark,UpdateBy = @UserId, UpdateOn = getdate()
										where LogID = @LogID";
				command.Parameters.AddWithValue("@ActivityTName", orderActivity.ActivityTName);
				command.Parameters.AddWithValue("@ActivityEName", orderActivity.ActivityEName);
				command.Parameters.AddWithValue("@Remark", orderActivity.Remark);
				command.Parameters.AddWithValue("@UserId", orderActivity.CreateBy);
				command.Parameters.AddWithValue("@LogID", orderActivity.LogID);
				command.ExecuteNonQuery();
			}
		}

		public Order GetMyOrder(string orderId)
		{
			Order order = null;
			order = GetOrder(orderId);
			if (order != null)
			{
				order.ItemOrders = GetOrderDetail(orderId);
			}
			return order;
		}

		public IEnumerable<Order> GetSearchMyOrder(string keywords, string selectedSearch, string userId, string companyId)
		{
			List<Order> item = null;

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{

				command.CommandText = @"Select	Distinct T1.OrderID,T1.OrderGuid,T1.OrderDate,T3.UserID,T3.UserTName,T3.UserEName,
												isnull(T4.UserID,'') as ApproverID,isnull(T4.UserTName,'') as ApproverTName,isnull(T4.UserEName,'') as ApproverEName,
												T2.CostcenterID,T2.CostTName,T2.CostEName,T1.CountProduct,T1.GrandTotalAmt,T1.OrderStatus
										from	TBOrder T1 with (nolock)
												INNER Join TBCostCenter T2 with (nolock) On T1.CostCenterID = T2.CostCenterID and T1.CompanyID=T2.CompanyID
												Left Join TBUserInfo T3 with (nolock) on T1.UserID=T3.UserID
												Left Join TBUserInfo T4 with (nolock) on T1.CurrentAppID=T4.UserID
										Where	T1.CompanyID=@CompanyID
										";

				if (!string.IsNullOrEmpty(userId))
				{
					command.CommandText += " and (T1.UserID=@UserID OR T4.UserId=@UserID)";
					command.Parameters.AddWithValue("@userId", @userId);
				}
				switch (selectedSearch)
				{
					case "OrderID":
						command.CommandText += " and T1.OrderID like @keywords ";
						break;
					case "ReqName":
						command.CommandText += " and (T3.UserId LIKE @keywords OR T3.UserTName LIKE @keywords OR T3.UserEName LIKE @keywords) ";
						break;
					case "AppName":
						command.CommandText += " and (T4.UserId LIKE @keywords OR T4.UserTName LIKE @keywords OR T4.UserEName LIKE @keywords) ";
						break;
					default:
						command.CommandText += " and (T2.CostcenterID LIKE @keywords OR T2.CostTName LIKE @keywords OR T2.CostEName LIKE @keywords) ";
						break;
				}
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@keywords", "%" + keywords + "%");

				using (SqlDataReader reader = command.ExecuteReader())
				{
					item = new List<Order>();
					Order order = null;
					while (reader.Read())
					{
						order = new Order();
						order.OrderID = (string)reader["OrderID"];
						order.OrderGuid = (string)reader["OrderGuid"];
						order.OrderDate = (DateTime)reader["OrderDate"];
						order.CostCenter = new CostCenter();
						order.CostCenter.CostCenterID = (string)reader["CostcenterID"];
						order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
						order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];
						order.Requester = new User();
						order.Requester.UserId = (string)reader["UserID"];
						order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
						order.Requester.DisplayName["en"] = (string)reader["UserEName"];
						order.CurrentApprover = new UserApprover();
						order.CurrentApprover.Approver = new User();
						order.CurrentApprover.Approver.UserId = (string)reader["ApproverID"];
						order.CurrentApprover.Approver.DisplayName = new LocalizedString((string)reader["ApproverTName"]);
						order.CurrentApprover.Approver.DisplayName["en"] = (string)reader["ApproverEName"];
						order.ItemCountOrder = (int)reader["CountProduct"];
						order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						item.Add(order);
					}
				}
			}
			return item;
		}

		public void ApproveOrder(string orderGuid, string userId, string remark)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBOrder set OrderBeforeStatus=Orderstatus, Orderstatus=@OrderStatus, DownloadStatus=@DownloadStatus, PreviousAppID=@PreviousAppID, CurrentAppID = '', NextAppID='', ApproveOrderDate = getdate(),
										ApproverRemark = @ApproverRemark, Updateby=@Updateby, UpdateOn = getdate() where orderGuid = @orderGuid";
				command.Parameters.AddWithValue("@OrderStatus", Order.OrderStatus.Approved.ToString());
				command.Parameters.AddWithValue("@DownloadStatus", Order.DownloadStatus.Download.ToString());
				command.Parameters.AddWithValue("@PreviousAppID", userId);
				command.Parameters.AddWithValue("@ApproverRemark", remark);
				command.Parameters.AddWithValue("@Updateby", userId);
				command.Parameters.AddWithValue("@orderGuid", orderGuid);
				command.ExecuteNonQuery();
			}
		}

		public void ApproveOrderOFM(string orderGuid, string userId, string remark)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBOrder set OrderBeforeStatus=Orderstatus, Orderstatus=@OrderStatus, DownloadStatus='No',IsDownload='No', PreviousAppID=@PreviousAppID, CurrentAppID = '', NextAppID='', ApproveOrderDate = getdate(),
										ApproverRemark = @ApproverRemark, Updateby=@Updateby, UpdateOn = getdate() where orderGuid = @orderGuid";
				command.Parameters.AddWithValue("@OrderStatus", Order.OrderStatus.Approved.ToString());
				command.Parameters.AddWithValue("@PreviousAppID", userId);
				command.Parameters.AddWithValue("@ApproverRemark", remark);
				command.Parameters.AddWithValue("@Updateby", userId);
				command.Parameters.AddWithValue("@orderGuid", orderGuid);
				command.ExecuteNonQuery();
			}
		}

		public int GetCountOrderOnProcess(string userId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(OrderId) as OrderID from TBOrder where companyId=@companyId and userId=@UserId and OrderStatus in (@Waiting,@Partial,@WaitingAdmin,@AdminAllow)";

				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@Waiting", Order.OrderStatus.Waiting.ToString());
				command.Parameters.AddWithValue("@WaitingAdmin", Order.OrderStatus.WaitingAdmin.ToString());
				command.Parameters.AddWithValue("@AdminAllow", Order.OrderStatus.AdminAllow.ToString());
				command.Parameters.AddWithValue("@Partial", Order.OrderStatus.Partial.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (int)reader["OrderID"];
					}
					return 0;
				}
			}
		}
		public int GetCountOrderRevise(string userId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(OrderId) as OrderID from TBOrder where  companyId=@companyId and userId=@UserId and OrderStatus = @Revise ";

				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@Revise", Order.OrderStatus.Revise.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (int)reader["OrderID"];
					}
					return 0;
				}
			}
		}
		public int GetCountOrderWaitingApproved(string userId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(OrderId) as OrderID from TBOrder where companyId=@companyId and CurrentAppId=@UserId and OrderStatus in (@Waiting,@WaitingAdmin,@AdminAllow,@Partial)";

				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@Waiting", Order.OrderStatus.Waiting.ToString());
				command.Parameters.AddWithValue("@WaitingAdmin", Order.OrderStatus.WaitingAdmin.ToString());
				command.Parameters.AddWithValue("@AdminAllow", Order.OrderStatus.AdminAllow.ToString());
				command.Parameters.AddWithValue("@Partial", Order.OrderStatus.Partial.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (int)reader["OrderID"];
					}
					return 0;
				}
			}
		}
		public int GetCountOrderWaitingAdminAllow(string userId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(OrderId) as OrderID from TBOrder where  companyId=@companyId and CurrentAppID=@userId and OrderStatus = @OrderStatus ";

				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@OrderStatus", Order.OrderStatus.WaitingAdmin.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (int)reader["OrderID"];
					}
					return 0;
				}
			}
		}
		public int GetCountAllOrderByStatus(string companyId, bool thisMonth, Order.OrderStatus orderStatus)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select count(OrderId) as OrderID from TBOrder t1
										INNER JOIN TBCostcenter t2 ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID
										where  t1.companyId=@companyId and OrderStatus=@orderStatus and (orderdate >=@dateFrom  and orderdate < @dateTo)";

				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@orderStatus", orderStatus.ToString());
				if (thisMonth)
				{
					command.Parameters.AddWithValue("@dateFrom", new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1));
					command.Parameters.AddWithValue("@dateTo", new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(1));
				}
				else
				{
					command.Parameters.AddWithValue("@dateFrom", new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddMonths(-1));
					command.Parameters.AddWithValue("@dateTo", new DateTime(DateTime.Today.Year, DateTime.Today.Month, 1).AddDays(-1));
				}
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (int)reader["OrderID"];
					}
					return 0;
				}
			}
		}

		public void AdminPartialApproveOrder(string orderGuid, string orderuserId, string currentAppID, string userId, string companyId, string costcenterId, int parkday, string remark)
		{

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				// เนื่องจาก เป็น Admin Partial Approved ผู้อนุมัติคนปัจจุบันเป็น admin ระบบ (userId)
				currentAppID = GetNextApproverID(currentAppID, companyId, costcenterId, true, orderuserId); // currentAppID ที่ส่งมาให้ เป็น admin ต้องเอาไปหา approver คนแรกก่อน
				string nextAppID = GetNextApproverID(currentAppID, companyId, costcenterId, false, orderuserId); // หลังจากนั้น เอา (currentAppID) ที่ได้มาไปผ่าน Method GetNextApproverID เพื่อดูว่ามีผู้อนุมัติลำดับถัดไปอีกหรือไม่ (NextAppID)

				command.CommandText = @"Update	TBOrder SET OrderBeforeStatus = OrderStatus where OrderGUID = @orderGuid

										Update	TBOrder SET OrderStatus = @OrderStatus, PreviousAppID = @PreviousAppID, CurrentAppID = @CurrentAppID, NextAppID = @NextAppID, ApproverRemark = @ApproverRemark,
												WarningExpireDate = @WarningExpireDate, ExpireDate = @ExpireDate, AdminAllowFlag = @AdminAllowFlag, UpdateBy = @Updateby, UpdateOn = getdate() 
										WHERE	OrderGUID = @orderGuid";
				command.Parameters.AddWithValue("@OrderStatus", Order.OrderStatus.AdminAllow.ToString());
				command.Parameters.AddWithValue("@PreviousAppID", userId);
				command.Parameters.AddWithValue("@CurrentAppID", currentAppID);
				command.Parameters.AddWithValue("@NextAppID", nextAppID);
				command.Parameters.AddWithValue("@ApproverRemark", remark);
				command.Parameters.AddWithValue("@WarningExpireDate", CalculateWarningExpireDate(DateTime.Now, parkday));
				command.Parameters.AddWithValue("@ExpireDate", CalculateExprieDate(DateTime.Now, parkday));
				command.Parameters.AddWithValue("@AdminAllowFlag", "Yes");
				command.Parameters.AddWithValue("@Updateby", userId);
				command.Parameters.AddWithValue("@orderGuid", orderGuid);
				command.ExecuteNonQuery();
			}
		}

		public void PartialApproveOrder(string orderGuid, string orderuserId, string userId, string companyId, string costcenterId, int parkday, string remark)
		{

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				//	เนื่องจาก เป็น Partial Approved ผู้อนุมัติคนปัจจุบันไม่สามารถอนุมัติได้เนื่องจากวงเงินไม่พอ
				//	ดังนั้น ต้องเอา UserID คนที่ Login เข้ามาไปผ่าน Method GetNextApproverID เพื่อหาว่าผู้อนุมัติคนต่อไปจะไปถึงใคร (CurrentAppID)
				//	หลังจากนั้น เอา (CurrentAppID) ที่ได้มาไปผ่าน Method GetNextApproverID เพื่อดูว่ามีผู้อนุมัติลำดับถัดไปอีกหรือไม่ (NextAppID
				string currentAppID = GetNextApproverID(userId, companyId, costcenterId, false, orderuserId);
				string nextAppID = GetNextApproverID(currentAppID, companyId, costcenterId, false, orderuserId);

				command.CommandText = @"Update	TBOrder SET OrderBeforeStatus = OrderStatus where orderGuid = @orderGuid

										Update	TBOrder SET	orderstatus = @OrderStatus, PreviousAppID = @PreviousAppID, CurrentAppID = @CurrentAppID, NextAppID = @NextAppID,
												ApproverRemark = @ApproverRemark, WarningExpireDate = @WarningExpireDate, ExpireDate = @ExpireDate, updateby = @Updateby, UpdateOn = getdate()
										WHERE	OrderGUID = @orderGuid";
				command.Parameters.AddWithValue("@OrderStatus", Order.OrderStatus.Partial.ToString());
				command.Parameters.AddWithValue("@PreviousAppID", userId);
				command.Parameters.AddWithValue("@CurrentAppID", currentAppID);
				command.Parameters.AddWithValue("@NextAppID", nextAppID);
				command.Parameters.AddWithValue("@ApproverRemark", remark);
				command.Parameters.AddWithValue("@WarningExpireDate", CalculateWarningExpireDate(DateTime.Now, parkday));
				command.Parameters.AddWithValue("@ExpireDate", CalculateExprieDate(DateTime.Now, parkday));
				command.Parameters.AddWithValue("@Updateby", userId);
				command.Parameters.AddWithValue("@orderGuid", orderGuid);
				command.ExecuteNonQuery();
			}
		}

		public void DeleteOrder(string orderGuid, string userId, string remark)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"
										Update TBOrder set OrderBeforeStatus = OrderStatus where orderGuid = @orderGuid
										Update TBOrder set orderstatus = @OrderStatus, PreviousAppID = @PreviousAppID, CurrentAppID = '', NextAppID = '', ApproverRemark = @ApproverRemark, Updateby = @Updateby, UpdateOn = getdate() where orderGuid = @orderGuid ";
				command.Parameters.AddWithValue("@OrderStatus", Order.OrderStatus.Deleted.ToString());
				command.Parameters.AddWithValue("@ApproverRemark", remark);
				command.Parameters.AddWithValue("@PreviousAppID", userId);
				command.Parameters.AddWithValue("@Updateby", userId);
				command.Parameters.AddWithValue("@orderGuid", orderGuid);
				command.ExecuteNonQuery();
			}
		}

		public void ReviseOrder(string orderGuid, string userId, string remark)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{

				command.CommandText = @"
										Update TBOrder set OrderBeforeStatus = OrderStatus where orderGuid = @orderGuid
										Update TBOrder set OrderStatus = @orderStatus, PreviousAppID = @previousAppID, CurrentAppID = '', NextAppID = @nextAppID, ApproverRemark = @ApproverRemark, Updateby=@userId, UpdateOn = getdate() where orderGuid = @orderGuid ";
				command.Parameters.AddWithValue("@orderStatus", Order.OrderStatus.Revise.ToString());
				command.Parameters.AddWithValue("@ApproverRemark", remark);
				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@orderGuid", orderGuid);
				command.Parameters.AddWithValue("@previousAppID", userId);
				command.Parameters.AddWithValue("@nextAppID", userId);
				command.ExecuteNonQuery();
			}
		}

		private Order GetOrder(string orderId)
		{
			Order order = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.*, t2.CompTName, t2.CompEName, isnull(t3.DeptTName,'') as DeptTName, isnull(t3.DeptEName,'') as DeptEName, t4.CostTName, t4.CostEName,
												isnull(t5.UserTName,'') as AppTName, isnull(t5.UserEName,'') as AppEName, isnull(t5.UserLang,'') as AppLang,
												isnull(t6.UserTName,'') as PreAppTName, isnull(t6.UserEName,'') as PreAppEName, isnull(t6.UserLang,'') as PreAppLang,
												isnull(t7.UserTName,'') as NextAppTName, isnull(t7.UserEName,'') as NextAppEName, isnull(t7.UserLang,'') as NextAppLang,
												isnull(t8.AppCreditlimit,0) as AppCreditlimit, isnull(t8.ParkDay,0) as ParkDay, isnull(t8.APPLevel,0) as APPLevel,
												isnull(t9.APPCreditlimit,0) as PreAppCreditlimit, isnull(t9.ParkDay,0) as PreParkday, isnull(t9.APPLevel,0) as PreAppLevel,
												isnull(t10.APPCreditlimit,0)as NextAppCreditlimit, isnull(t10.ParkDay,0) as NextParkday,isnull(t10.APPLevel,0) as  NextAPPLevel,
												isnull(t1.CustFileName,'') as CustFileName,isnull(t1.SystemFileName,'') as SystemFileName,t2.UseGoodReceive
										from	TBOrder t1 
												inner join TBCompanyInfo t2 on t1.CompanyID=t2.CompanyID
												left join TBDepartment t3 on t1.DepartmentID=t3.DepartmentID and t1.CompanyID=t3.CompanyID
												inner join TBCostcenter t4 on t1.CostcenterID=t4.CostcenterID and t1.CompanyID=t4.CompanyID
												left join TBUserinfo t5 on t1.CurrentAppID=t5.UserID
												left join TBUserinfo t6 on t1.PreviousAppID=t6.UserID
												left join TBUserinfo t7 on t1.NextAppID=t7.UserID
												left Join TBRequesterLine t8 on t1.CurrentAppID = t8.AppuserId and t1.CompanyID=t8.CompanyID and t1.CostcenterID=t8.CostcenterID AND t1.UserID = t8.ReqUserID
												left Join TBRequesterLine t9 on t1.PreviousAppID = t9.AppuserId and t1.CompanyID=t9.CompanyID and t1.CostcenterID=t9.CostcenterID AND t1.UserID = t9.ReqUserID
												left Join TBRequesterLine t10 on t1.NextAppID = t10.AppuserId and t1.CompanyID=t10.CompanyID and t1.CostcenterID=t10.CostcenterID AND t1.UserID = t10.ReqUserID
										where	orderId=@orderId
										";
				command.Parameters.AddWithValue("@orderId", orderId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						order = new Order();

						order.OrderID = (string)reader["OrderID"];
						order.OrderGuid = (string)reader["OrderGuid"];
						order.OrderDate = (DateTime)reader["OrderDate"];
						order.ApproveOrderDate = (DateTime)reader["ApproveOrderDate"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						order.OldStatus = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderBeforeStatus"]);
						order.ItemCountOrder = (int)reader["CountProduct"];
						order.TotalAmt = (decimal)reader["TotAmt"];
						order.TotalNetAmt = (decimal)reader["NetAmt"];
						order.DiscountRate = (decimal)reader["DiscountRate"];
						order.TotalAllDiscount = (decimal)reader["DiscountAmt"];
						order.TotalDeliveryCharge = (decimal)reader["OrdDeliverFee"];
						order.TotalDeliveryFee = (decimal)reader["DeliveryFee"];
						order.TotalPriceProductExcVatAmount = (decimal)reader["VatProdNetAmt"];
						order.TotalPriceProductNoneVatAmount = (decimal)reader["NonVatProdNetAmt"];
						order.TotalVatAmt = (decimal)reader["VatAmt"];
						order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];
						order.ParkDay = (int)reader["ParkDay"];
						order.WarningExpireDate = (DateTime)reader["WarningExpireDate"];
						order.ExpireDate = (DateTime)reader["ExpireDate"];
						order.ApproverRemark = (string)reader["ApproverRemark"];
						order.OFMRemark = (string)reader["OFMRemark"];
						order.ReferenceRemark = (string)reader["ReferenceRemark"];
						order.AdminAllowFlag = Mapper.ToClass<bool>((string)reader["AdminAllowFlag"]);
						order.AttachFile = (string)reader["AttachFile"];
						order.CallBackRequest = (string)reader["CallBackRequest"];
						order.NumOfAdjust = (int)reader["NumOfAdjust"];
						order.RefInvNo = (string)reader["RefInvNo"];
						order.ShipType = (string)reader["ShipType"];
						order.UseSMSFeature = Mapper.ToClass<bool>((string)reader["UseSMSFeature"]);
						order.CustFileName = (string)reader["CustFileName"];
						order.SystemFileName = (string)reader["SystemFileName"];

						order.Company = new Company();
						order.Company.CompanyId = (string)reader["CompanyID"];
						order.Company.CompanyThaiName = (string)reader["CompTName"];
						order.Company.CompanyEngName = (string)reader["CompEName"];
						order.Company.CompanyName = new LocalizedString((string)reader["CompTName"]);
						order.Company.CompanyName["en"] = (string)reader["CompEName"];
						order.Company.UseGoodReceive = Mapper.ToClass<bool>((string)reader["UseGoodReceive"]);

						order.Department = new CompanyDepartment();
						order.Department.DepartmentID = (string)reader["DepartmentID"];
						order.Department.DepartmentThaiName = (string)reader["DeptTName"];
						order.Department.DepartmentEngName = (string)reader["DeptEName"];
						order.Department.DepartmentName = new LocalizedString((string)reader["DeptTName"]);
						order.Department.DepartmentName["en"] = (string)reader["DeptEName"];

						order.CostCenter = new CostCenter();
						order.CostCenter.CostCenterCustID = (string)reader["CustId"];
						order.CostCenter.CostCenterID = (string)reader["CostcenterID"];
						order.CostCenter.CostCenterThaiName = (string)reader["CostTName"];
						order.CostCenter.CostCenterEngName = (string)reader["CostEName"];
						order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
						order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];

						order.Invoice = new Invoice();
						order.Invoice.Address1 = (string)reader["InvAddr1"];
						order.Invoice.Address2 = (string)reader["InvAddr2"];
						order.Invoice.Address3 = (string)reader["InvAddr3"];
						order.Invoice.Address4 = (string)reader["InvAddr4"];

						order.Shipping = new Shipping();
						order.Shipping.ShipContactor = (string)reader["ShipContactor"];
						order.Shipping.Address1 = (string)reader["ShipAddr1"];
						order.Shipping.Address2 = (string)reader["ShipAddr2"];
						order.Shipping.Address3 = (string)reader["ShipAddr3"];
						order.Shipping.Address4 = (string)reader["ShipAddr4"];
						order.Shipping.Province = (string)reader["ShipProvince"];
						order.Shipping.ShipPhoneNo = (string)reader["ShipPhoneNo"];
						order.Shipping.ShipMobileNo = (string)reader["ShipMobileNo"];
						order.Shipping.Remark = (string)reader["ShipRemark"];

						order.Contact = new Contact();
						order.Contact.ContactorName = (string)reader["ContactorName"];
						order.Contact.ContactorPhone = (string)reader["ContactorPhone"];
						order.Contact.ContactorExtension = (string)reader["ContactorExtension"];
						order.Contact.ContactMobileNo = (string)reader["ContactMobileNo"];
						order.Contact.ContactorFax = (string)reader["ContactorFax"];
						order.Contact.Email = (string)reader["ContactorEmail"];

						order.Requester = new User();
						order.Requester.UserId = (string)reader["UserID"];
						order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
						order.Requester.DisplayName["en"] = (string)reader["UserEName"];
						order.Requester.UserThaiName = (string)reader["UserTName"];
						order.Requester.UserEngName = (string)reader["UserEName"];


						if (!string.IsNullOrEmpty(reader["AppTName"].ToString()))
						{
							order.CurrentApprover = new UserApprover();
							order.CurrentApprover.Approver = new User();
							order.CurrentApprover.Approver.UserId = (string)reader["CurrentAppID"];
							order.CurrentApprover.Approver.DisplayName = new LocalizedString((string)reader["AppTName"]);
							order.CurrentApprover.Approver.DisplayName["en"] = (string)reader["AppEName"];
							order.CurrentApprover.Approver.UserThaiName = (string)reader["AppTName"];
							order.CurrentApprover.Approver.UserEngName = (string)reader["AppEName"];
							order.CurrentApprover.Approver.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["AppLang"], true);
							order.CurrentApprover.ParkDay = (int)reader["ParkDay"];
							order.CurrentApprover.ApproveCreditLimit = (decimal)reader["AppCreditlimit"];
							order.CurrentApprover.Level = (int)reader["APPLevel"];
						}

						if (!string.IsNullOrEmpty(reader["PreAppTName"].ToString()))
						{
							order.PreviousApprover = new UserApprover();
							order.PreviousApprover.Approver = new User();
							order.PreviousApprover.Approver.UserId = (string)reader["PreviousAppID"];
							order.PreviousApprover.Approver.DisplayName = new LocalizedString((string)reader["PreAppTName"]);
							order.PreviousApprover.Approver.DisplayName["en"] = (string)reader["PreAppEName"];
							order.PreviousApprover.Approver.UserThaiName = (string)reader["PreAppTName"];
							order.PreviousApprover.Approver.UserEngName = (string)reader["PreAppEName"];
							order.PreviousApprover.Approver.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["PreAppLang"], true);
							order.PreviousApprover.ParkDay = (int)reader["PreParkDay"];
							order.PreviousApprover.ApproveCreditLimit = (decimal)reader["PreAppCreditlimit"];
							order.PreviousApprover.Level = (int)reader["PreAPPLevel"];
						}

						if (!string.IsNullOrEmpty(reader["NextAppTName"].ToString()))
						{
							order.NextApprover = new UserApprover();
							order.NextApprover.Approver = new User();
							order.NextApprover.Approver.UserId = (string)reader["NextAppID"];
							order.NextApprover.Approver.DisplayName = new LocalizedString((string)reader["NextAppTName"]);
							order.NextApprover.Approver.DisplayName["en"] = (string)reader["NextAppEName"];
							order.NextApprover.Approver.UserThaiName = (string)reader["NextAppTName"];
							order.NextApprover.Approver.UserEngName = (string)reader["NextAppEName"];
							order.NextApprover.Approver.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["NextAppLang"], true);
							order.NextApprover.ParkDay = (int)reader["NextParkDay"];
							order.NextApprover.ApproveCreditLimit = (decimal)reader["NextAppCreditlimit"];
							order.NextApprover.Level = (int)reader["NextAPPLevel"];
						}
					}
				}
			}
			return order;
		}
		public string GetOrderIdByGuid(string orderguId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	OrderID from TBOrder Where OrderGuid = @OrderGuid";
				command.Parameters.AddWithValue("@OrderGuid", orderguId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (string)reader["OrderID"];
					}
					return string.Empty;
				}
			}
		}
		public string GetGuidByOrderId(string orderId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	OrderGuID from TBOrder Where OrderId = @OrderId";
				command.Parameters.AddWithValue("@OrderId", orderId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (string)reader["OrderGuID"];
					}
					return string.Empty;
				}
			}
		}
		private IEnumerable<OrderDetail> GetOrderDetail(string orderId)
		{
			List<OrderDetail> item = null;

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	SeqNo,pID,pTName,pEName,pTUnit,pEUnit,Qty,OldQty,
												IsFree,IncVatPrice,ExcVatPrice,DiscountRate,
												DiscAmt,DeliverFeeItem,IsVat,PriceFlag,ProdType,
												BestDealFlag
										from	TBOrderDetail
										where	orderId = @orderId";
				command.Parameters.AddWithValue("@orderId", orderId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					item = new List<OrderDetail>();
					while (reader.Read())
					{
						OrderDetail orderDetail = new OrderDetail();
						orderDetail.Seq = (int)reader["SeqNo"];
						orderDetail.Quantity = (int)reader["Qty"];
						orderDetail.OldQuantity = (int)reader["OldQty"];
						orderDetail.DiscountRate = (decimal)reader["DiscountRate"];
						orderDetail.DiscAmt = (decimal)reader["DiscAmt"];

						orderDetail.Product = new Product();
						orderDetail.Product.Id = (string)reader["pID"];
						orderDetail.Product.ThaiName = (string)reader["pTName"];
						orderDetail.Product.EngName = (string)reader["pEName"];
						orderDetail.Product.Name = new LocalizedString((string)reader["pTName"]);
						orderDetail.Product.Name["en"] = (string)reader["pEName"];
						orderDetail.Product.ThaiUnit = (string)reader["pTUnit"];
						orderDetail.Product.EngUnit = (string)reader["pEUnit"];
						orderDetail.Product.Unit = new LocalizedString((string)reader["pTUnit"]);
						orderDetail.Product.Unit["en"] = (string)reader["pEUnit"];
						orderDetail.Product.PriceIncVat = (decimal)reader["IncVatPrice"];
						orderDetail.Product.PriceExcVat = (decimal)reader["ExcVatPrice"];
						orderDetail.Product.TransCharge = (decimal)reader["DeliverFeeItem"];
						orderDetail.Product.IsVat = Mapper.ToClass<bool>((string)reader["IsVat"]);
						orderDetail.Product.PriceFlag = (string)reader["PriceFlag"];
						orderDetail.Product.ProdType = (string)reader["ProdType"];
						orderDetail.Product.IsBestDeal = Mapper.ToClass<bool>((string)reader["BestDealFlag"]);
						item.Add(orderDetail);
					}
				}
			}
			return item;
		}

		public IEnumerable<OrderDetail> GetOrderDetailBySearch(string orderId)
		{
			List<OrderDetail> item = null;

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	SeqNo,OrderId,pID,pTName,pEName,pTUnit,pEUnit,Qty,OldQty,
												IsFree,IncVatPrice,ExcVatPrice,DiscountRate,
												DiscAmt,DeliverFeeItem,IsVat,PriceFlag,ProdType,
												BestDealFlag
										from	TBOrderDetail
										where	orderId = @orderId";
				command.Parameters.AddWithValue("@orderId", orderId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					item = new List<OrderDetail>();
					while (reader.Read())
					{
						OrderDetail orderDetail = new OrderDetail();
						orderDetail.OrderId = (string)reader["OrderId"];
						orderDetail.Seq = (int)reader["SeqNo"];
						orderDetail.Quantity = (int)reader["Qty"];
						orderDetail.OldQuantity = (int)reader["OldQty"];
						orderDetail.DiscountRate = (decimal)reader["DiscountRate"];
						orderDetail.DiscAmt = (decimal)reader["DiscAmt"];

						orderDetail.Product = new Product();
						orderDetail.Product.Id = (string)reader["pID"];
						orderDetail.Product.ThaiName = (string)reader["pTName"];
						orderDetail.Product.EngName = (string)reader["pEName"];
						orderDetail.Product.Name = new LocalizedString((string)reader["pTName"]);
						orderDetail.Product.Name["en"] = (string)reader["pEName"];
						orderDetail.Product.ThaiUnit = (string)reader["pTUnit"];
						orderDetail.Product.EngUnit = (string)reader["pEUnit"];
						orderDetail.Product.Unit = new LocalizedString((string)reader["pTUnit"]);
						orderDetail.Product.Unit["en"] = (string)reader["pEUnit"];
						orderDetail.Product.PriceIncVat = (decimal)reader["IncVatPrice"];
						orderDetail.Product.PriceExcVat = (decimal)reader["ExcVatPrice"];
						orderDetail.Product.TransCharge = (decimal)reader["DeliverFeeItem"];
						orderDetail.Product.IsVat = Mapper.ToClass<bool>((string)reader["IsVat"]);
						orderDetail.Product.PriceFlag = (string)reader["PriceFlag"];
						orderDetail.Product.ProdType = (string)reader["ProdType"];
						orderDetail.Product.IsBestDeal = Mapper.ToClass<bool>((string)reader["BestDealFlag"]);
						item.Add(orderDetail);
					}
				}
			}
			return item;
		}

		public IEnumerable<Order> GetDataOrder(string companyId, string userId, Order.OrderStatus orderStatus)
		{
			List<Order> orders;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	T1.OrderId,T1.OrderGuid,T1.CreateOn,T1.OrderStatus,T1.CompanyId,T1.CostcenterId,T2.CostTName,T2.CostEName,CountProduct,GrandTotalAmt,
												T1.CurrentAppID,T1.NextAppID,isnull(T3.APPCreditlimit,0)as APPCreditlimit,T1.AdminAllowFlag
										FROM	TBOrder T1 INNER JOIN TBCostcenter T2 ON T1.CostcenterID = T2.CostcenterID AND T1.CompanyID=T2.CompanyID
												LEFT JOIN TBRequesterLine T3 ON t1.CurrentAppID=T3.APPUserID AND T1.UserID=T3.ReqUserID AND T1.CompanyID=T3.CompanyID AND T1.CostcenterID=T3.CostcenterID
										WHERE	T1.CompanyId=@companyId";


				if (orderStatus.ToString() == Order.OrderStatus.Waiting.ToString())
				{
					command.CommandText += " and UserId=@UserId and OrderStatus in (@OrderStatusWait,@OrderStatusPAP,@OrderStatusAdmin,@OrderStatusAllow)";
					command.Parameters.AddWithValue("@OrderStatusWait", Order.OrderStatus.Waiting.ToString());
					command.Parameters.AddWithValue("@OrderStatusPAP", Order.OrderStatus.Partial.ToString());
					command.Parameters.AddWithValue("@OrderStatusAdmin", Order.OrderStatus.WaitingAdmin.ToString());
					command.Parameters.AddWithValue("@OrderStatusAllow", Order.OrderStatus.AdminAllow.ToString());
				}
				if (orderStatus.ToString() == Order.OrderStatus.Approved.ToString())
				{
					command.CommandText += " and CurrentAppID=@UserId and OrderStatus in(@OrderStatusPAP,@OrderStatusAdmin,@OrderStatusWait,@OrderStatusAllow)";
					command.Parameters.AddWithValue("@OrderStatusWait", Order.OrderStatus.Waiting.ToString());
					command.Parameters.AddWithValue("@OrderStatusAdmin", Order.OrderStatus.WaitingAdmin.ToString());
					command.Parameters.AddWithValue("@OrderStatusPAP", Order.OrderStatus.Partial.ToString());
					command.Parameters.AddWithValue("@OrderStatusAllow", Order.OrderStatus.AdminAllow.ToString());
				}
				if (orderStatus.ToString() == Order.OrderStatus.Revise.ToString())
				{
					command.CommandText += " and UserId=@UserId and OrderStatus = @OrderStatusRevise";
					command.Parameters.AddWithValue("@OrderStatusRevise", Order.OrderStatus.Revise.ToString());
				}

				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@UserId", userId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					orders = new List<Order>();
					while (reader.Read())
					{
						Order order = new Order();
						order.OrderID = (string)reader["OrderId"];
						order.OrderGuid = (string)reader["OrderGuid"];
						order.OrderDate = (DateTime)reader["CreateOn"];

						order.CurrentApprover = new UserApprover();
						order.CurrentApprover.Approver = new User();
						order.CurrentApprover.Approver.UserId = (string)reader["CurrentAppID"];
						order.CurrentApprover.ApproveCreditLimit = (decimal)reader["APPCreditlimit"];

						if (!string.IsNullOrEmpty((string)reader["NextAppID"]))
						{
							order.NextApprover = new UserApprover();
							order.NextApprover.Approver = new User();
							order.NextApprover.Approver.UserId = (string)reader["NextAppID"];
						}

						order.Company = new Company();
						order.Company.CompanyId = (string)reader["CompanyId"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						order.CostCenter = new CostCenter();
						order.CostCenter.CostCenterID = (string)reader["CostcenterId"];
						order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
						order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];
						order.ItemCountOrder = (int)reader["CountProduct"];
						order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];
						order.AdminAllowFlag = Mapper.ToClass<bool>((string)reader["AdminAllowFlag"]);
						orders.Add(order);
					}
				}
			}
			return orders;
		}
		public IEnumerable<Order> GetDataOrder(string companyId, string userId)
		{
			List<Order> orders;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	TOP 10 OrderId,OrderGuid,CreateOn,OrderStatus,CompanyId,CostcenterId,CountProduct,GrandTotalAmt
										FROM	TBOrder
										WHERE	CompanyID=@companyId and UserID=@userId
												ORDER BY OrderDate DESC";
				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@userId", userId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					orders = new List<Order>();
					while (reader.Read())
					{
						Order order = new Order();
						order.OrderID = (string)reader["OrderId"];
						order.OrderGuid = (string)reader["OrderGuid"];
						order.OrderDate = (DateTime)reader["CreateOn"];
						order.Company = new Company();
						order.Company.CompanyId = (string)reader["CompanyId"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						order.CostCenter = new CostCenter();
						order.CostCenter.CostCenterID = (string)reader["CostcenterId"];
						order.ItemCountOrder = (int)reader["CountProduct"];
						order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];
						orders.Add(order);
					}
				}
			}
			return orders;
		}

		public List<Order> GetSearchOrders(string keyWord, string columName, string companyID, string userId)
		{
			List<Order> orders = new List<Order>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder(1024);
				sqlBuilder.AppendLine("SELECT * FROM");
				sqlBuilder.AppendLine("(SELECT	OrderID,OrderGUID,OrderDate,OrderStatus,t1.CompanyID,t1.CountProduct,t1.GrandTotalAmt,");
				sqlBuilder.AppendLine("t1.CostcenterID,t2.CostTName,t2.CostEName,t1.UserID,t1.UserTName,t1.UserEName,");
				sqlBuilder.AppendLine("t1.CurrentAppID as ApproverUserID,isnull(t3.UserTName,'') as ApproverUserThaiName,isnull(t3.UserEName,'') as ApproverEngName");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock)");
				sqlBuilder.AppendLine("LEFT JOIN   TBCostcenter t2 with (nolock) ON t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("LEFT JOIN   TBUserInfo t3 with (nolock) ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("WHERE t1.CompanyID = @CompanyID ");

				if (!string.IsNullOrEmpty(userId))
				{
					sqlBuilder.AppendLine("   and userId = @userId ");
					command.Parameters.AddWithValue("@userId", userId);
				}
				sqlBuilder.AppendLine(" )Table1");
				sqlBuilder.AppendLine("WHERE");
				if (columName == "OrderID")
				{
					sqlBuilder.AppendLine("Table1.OrderID LIKE @KeyWord");
				}
				else if (columName == "CustName")
				{
					sqlBuilder.AppendLine("Table1.UserTName LIKE  @KeyWord OR Table1.UserEName LIKE  @KeyWord");
				}
				else if (columName == "CurrentAppID")
				{
					sqlBuilder.AppendLine("ApproverUserThaiName LIKE @KeyWord OR  ApproverEngName LIKE @KeyWord");
				}
				else
				{
					sqlBuilder.AppendLine("Table1.CostTName LIKE @KeyWord OR Table1.CostEName LIKE @KeyWord");
				}
				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@KeyWord", "%" + keyWord + "%");
				command.Parameters.AddWithValue("@CompanyID", companyID);
				using (SqlDataReader reader = command.ExecuteReader())
				{

					while (reader.Read())
					{
						Order order = new Order();
						order.OrderID = (string)reader["OrderID"];
						order.OrderDate = (DateTime)reader["OrderDate"];
						order.CostCenter = new CostCenter();
						order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
						order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];
						order.Requester = new User();
						order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
						order.Requester.DisplayName["en"] = (string)reader["UserEName"];
						order.CurrentApprover = new UserApprover();
						order.CurrentApprover.Approver.DisplayName = new LocalizedString((string)reader["ApproverUserThaiName"]);
						order.CurrentApprover.Approver.DisplayName["en"] = (string)reader["ApproverEngName"];
						order.ItemCountOrder = (int)reader["CountProduct"];
						order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						orders.Add(order);
					}
					return orders;
				}
			}
		}

		private string GetOrderStatus(bool isSendToAdmin)
		{
			if (isSendToAdmin)
			{
				return Order.OrderStatus.WaitingAdmin.ToString();
			}
			return Order.OrderStatus.Waiting.ToString();
		}

		private string GetNextApproverID(string currentApproverId, string companyId, string costcenterId, bool isSendToAdmin, string userId)
		{
			UserRepository userRepo = new UserRepository(base.Connection);
			IEnumerable<UserApprover> userApprover = userRepo.GetListUserApprover(companyId, costcenterId, userId);
			int currentLevel = 0;
			if (userApprover.Where(o => o.Approver.UserId == currentApproverId).Any()) //CurrentApproverId มีอยู่จริง ไม่ใช่ admin
			{
				currentLevel = userApprover.Where(o => o.Approver.UserId == currentApproverId).Select(o => o.Level).Single();
			}
			if (isSendToAdmin) //ต้องผ่าน Admin ก่อน Approver Level 1 จึงกลายเป็น NextApproverId
			{
				return userApprover.Where(o => o.Level == 1).Select(o => o.Approver.UserId).Single();
			}
			if (userApprover.Count() == 0 || userApprover.Count() == 1 || currentLevel == userApprover.Count()) //มีผู้อนุมัติคนเดียว หรือ เป็นผู้อนุมัติคนสุดท้าย แสดงว่าไม่มีคนลำดับถัดไป
			{
				return string.Empty;
			}
			else
			{
				currentLevel += 1; //ขยับไปคนถัดไป
				return userApprover.Where(o => o.Level == currentLevel).Select(o => o.Approver.UserId).Single();
			}
		}

		private string GetApproverID(string approverId, string companyId, bool isSendToAdmin)
		{
			// 1. ถ้ามีสินค้าที่ไม่ได้อยู่ใน OFM Catalog  ให้ส่งไปหา Admin
			// 2. กรณีบริษัทมี่ควบคุมด้วย budget แล้วยอดในใบสั่งซื้อเกิน budget
			if (isSendToAdmin)
			{
				UserRepository user = new UserRepository(base.Connection);
				return user.GetAllUserAdmin(companyId).Where(o => o.UserRoleName == User.UserRole.RootAdmin).Single().UserId;
			}
			return approverId;
		}

		private IEnumerable<Holiday> GetHoliday()
		{
			List<Holiday> item = new List<Holiday>();

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select Holdate,Holdesc from TBHoliday";
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Holiday holiday = new Holiday();
						holiday.HolidayDate = Convert.ToDateTime(reader["Holdate"]);
						holiday.HolidayName = (string)reader["Holdesc"];
						item.Add(holiday);
					}
				}
			}
			return item;
		}

		private DateTime CalculateExprieDate(DateTime OrderDate, int parkday)
		{
			IEnumerable<Holiday> ListHoliday = GetHoliday();
			DateTime Expiredate = OrderDate.Date;

			ListHoliday = ListHoliday.Where(o => o.HolidayDate >= Expiredate).ToList(); //ดึงเฉพาะวันที่มากกว่าเท่ากับ  Expiredate จะได้ไม่ต้องคิดวันหยุดที่ผ่านมาแล้ว

			while (parkday != 0)
			{
				Expiredate = Expiredate.AddDays(1);
				if (Expiredate.DayOfWeek == DayOfWeek.Saturday || Expiredate.DayOfWeek == DayOfWeek.Sunday) { continue; } //ถ้าเป็นวันเสาร์หรือวันอาทิตย์จะข้ามไป
				else
				{
					foreach (var item in ListHoliday)
					{
						if (DateTime.Compare(Expiredate, item.HolidayDate) == 0) { continue; }
					}
				}
				parkday--;
			}
			DateTime thisDate = new DateTime(Expiredate.Year, Expiredate.Month, Expiredate.Day);

			return thisDate;
		}

		private DateTime CalculateWarningExpireDate(DateTime OrderDate, int parkday)
		{
			return CalculateExprieDate(OrderDate, parkday - 2);
		}

		public IEnumerable<Order> GetOrderAllStatus(string companyId, string userId)
		{
			List<Order> orders = new List<Order>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT * FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter T2 with (nolock) ON T1.CostcenterID = T2.CostcenterID AND T1.CompanyID=T2.CompanyID
										WHERE	t1.CompanyID=@CompanyID AND (UserID = @UserID
												OR (CurrentAppID = @UserID AND OrderStatus IN ('Waiting','WaitingAdmin','AdminAllow','Partial','Revise'))
												OR (PreviousAppID = @UserID AND OrderStatus IN ('Approved','Shipped','Deleted','Expired','Completed')))";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@UserID", userId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Order order = new Order();
						order.OrderID = (string)reader["OrderID"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						order.OrderGuid = (string)reader["OrderGuid"];
						orders.Add(order);
					}
				}
			}
			return orders;
		}

		public IEnumerable<Order> GetDataOrderStatus(string companyId, string userId, Order.OrderStatus orderStatus, bool isRequester, bool isApprover)
		{
			List<Order> orders = new List<Order>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	T1.OrderId,T1.OrderGuid,T1.CreateOn,T1.OrderStatus,T1.CompanyId,T1.CostcenterId,T2.CostTName,T2.CostEName,CountProduct,GrandTotalAmt,
												T1.CurrentAppID,T1.NextAppID,isnull(T3.APPCreditlimit,0) as APPCreditlimit
										FROM	TBOrder T1 with (nolock) INNER JOIN TBCostcenter T2 with (nolock) ON T1.CostcenterID = T2.CostcenterID AND T1.CompanyID=T2.CompanyID
												LEFT JOIN TBRequesterLine T3 with (nolock) ON t1.CurrentAppID=T3.APPUserID AND T1.UserID=T3.ReqUserID AND T1.CompanyID=T3.CompanyID AND T1.CostcenterID=T3.CostcenterID
										WHERE T1.CompanyId=@companyId AND (1 <> 1 ";

				if ((orderStatus == Order.OrderStatus.Waiting) || (orderStatus == Order.OrderStatus.Partial) || (orderStatus == Order.OrderStatus.Revise) || (orderStatus == Order.OrderStatus.AdminAllow))
				{
					command.CommandText += "OR (CurrentAppID = @userId AND OrderStatus = @OrderStatus)";
				}

				if ((orderStatus == Order.OrderStatus.Approved) || (orderStatus == Order.OrderStatus.Shipped) || (orderStatus == Order.OrderStatus.Deleted) || (orderStatus == Order.OrderStatus.Expired) || (orderStatus == Order.OrderStatus.Completed))
				{
					command.CommandText += "OR (PreviousAppID = @userId AND OrderStatus = @OrderStatus)";
				}
				if (isRequester)
				{
					command.CommandText += "OR (UserID = @userId AND OrderStatus = @OrderStatus)";
				}
				command.CommandText += ")";
				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@OrderStatus", orderStatus.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Order order = new Order();
						order.OrderID = (string)reader["OrderId"];
						order.OrderGuid = (string)reader["OrderGuid"];
						order.OrderDate = (DateTime)reader["CreateOn"];
						order.CurrentApprover = new UserApprover();
						order.CurrentApprover.Approver = new User();
						order.CurrentApprover.Approver.UserId = (string)reader["CurrentAppID"];
						order.CurrentApprover.ApproveCreditLimit = (decimal)reader["APPCreditlimit"];
						order.NextApprover = new UserApprover();
						order.NextApprover.Approver = new User();
						order.NextApprover.Approver.UserId = (string)reader["NextAppID"];
						order.Company = new Company();
						order.Company.CompanyId = (string)reader["CompanyId"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						order.CostCenter = new CostCenter();
						order.CostCenter.CostCenterID = (string)reader["CostcenterId"];
						order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
						order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];
						order.ItemCountOrder = (int)reader["CountProduct"];
						order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];
						orders.Add(order);
					}
				}
			}
			return orders;
		}

		public bool HaveOrderWaitingProcess(string companyId, string departmentId, string costcenterId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	count(OrderID) as countorder FROM TBOrder
										WHERE	CompanyID=@CompanyID and OrderStatus NOT IN (@Approved,@Shipped,@Deleted,@Expired,@Completed)
												AND (1=2 ";
				if (!string.IsNullOrEmpty(departmentId))
				{
					command.CommandText += "OR DepartmentID=@DepartmentID ";
					command.Parameters.AddWithValue("@DepartmentID", departmentId);
				}
				if (!string.IsNullOrEmpty(costcenterId))
				{
					command.CommandText += "OR CostcenterID=@CostcenterID ";
					command.Parameters.AddWithValue("@CostcenterID", costcenterId);
				}
				if (!string.IsNullOrEmpty(userId))
				{
					command.CommandText += "OR UserID=@UserID ";
					command.Parameters.AddWithValue("@UserID", userId);
				}
				command.CommandText += ")";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@Approved", Order.OrderStatus.Approved.ToString());
				command.Parameters.AddWithValue("@Shipped", Order.OrderStatus.Shipped.ToString());
				command.Parameters.AddWithValue("@Deleted", Order.OrderStatus.Deleted.ToString());
				command.Parameters.AddWithValue("@Expired", Order.OrderStatus.Expired.ToString());
				command.Parameters.AddWithValue("@Completed", Order.OrderStatus.Completed.ToString());
				return Convert.ToInt32(command.ExecuteScalar()) > 0;
			}
		}

		public IEnumerable<OrderTransLog> GetOrderTransLog(string orderId)
		{
			List<OrderTransLog> orders = new List<OrderTransLog>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT * FROM TBOrderTransLog with (nolock) where OrderID=@OrderID";
				command.Parameters.AddWithValue("@OrderID", orderId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						OrderTransLog order = new OrderTransLog();
						order.OrderID = (string)reader["OrderID"];
						order.ActionUserID = (string)reader["ActionUserID"];
						order.ActionName = new LocalizedString((string)reader["ActionTDesc"]);
						order.ActionName["en"] = (string)reader["ActionEDesc"];
						order.BeforeStatus = (string)reader["BeforeStatus"];
						order.AfterStatus = (string)reader["AfterStatus"];
						order.ActionRemark = (string)reader["ActionRemark"];
						order.ActionDate = (DateTime)reader["ActionDate"];
						orders.Add(order);
					}
				}
			}
			return orders;
		}

		public IEnumerable<OrderDetailTransLog> GetOrderDetailTransLog(string orderId)
		{
			List<OrderDetailTransLog> orderDetails = new List<OrderDetailTransLog>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT * FROM TBOrderDetailTransLog with (nolock) where OrderID=@OrderID";
				command.Parameters.AddWithValue("@OrderID", orderId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						OrderDetailTransLog orderDetail = new OrderDetailTransLog();
						orderDetail.OrderID = (string)reader["OrderID"];
						orderDetail.EditBy = (string)reader["EditBy"];
						orderDetail.ActionEditName = new LocalizedString((string)reader["EditTReason"]);
						orderDetail.ActionEditName["en"] = (string)reader["EditEReason"];
						orderDetail.PID = (string)reader["PID"];
						orderDetail.EditOn = (DateTime)reader["EditOn"];
						orderDetail.OldQty = (int)reader["OldQty"];
						orderDetail.EditQty = (int)reader["EditQty"];
						orderDetail.NewQty = (int)reader["NewQty"];
						orderDetail.EditRemark = (string)reader["EditRemark"];
						orderDetails.Add(orderDetail);
					}
				}
			}
			return orderDetails;
		}

		public void CreateGoodReceive(string orderId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"INSERT INTO TBGoodReceive (CompanyId ,OrderId,Status,NumOfAction,UserId,AppUserId,CreateOn,CreateBy,UpdateOn,UpdateBy)
										SELECT CompanyID,OrderID,@Status,0,UserId,@AppUserId,getdate(),@UserId,getdate(),@UserId
										FROM TBOrder WHERE OrderID=@OrderID

										INSERT INTO TBGoodReceiveDetail (OrderId,ActionDate,NumOfAction,pID,OriginalQty,ActionQty,CancelQty,RemainQty,CreateOn,CreateBy,UpdateOn,UpdateBy)
										SELECT OrderId,getdate(),0,pID,Qty,0,0,Qty,getdate(),@UserId,getdate(),@UserId
										FROM TBOrderDetail WHERE OrderID=@OrderID";

				command.Parameters.AddWithValue("@AppUserId", userId);
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@OrderID", orderId);
				command.Parameters.AddWithValue("@Status", GoodReceive.GoodReceiveStatus.Waiting.ToString());
				command.ExecuteNonQuery();
			}
		}

		public void CreateGoodReceiveAll(GoodReceive goodReceive, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				int numOfAction = ++goodReceive.NumOfAction;
				command.CommandText = @"UPDATE	TBGoodReceive SET Status=@Status,NumOfAction=@NumOfAction,UpdateOn = getdate(), UpdateBy=@userId
										WHERE	CompanyId=@CompanyId AND OrderId=@OrderId

										INSERT	TBGoodReceiveDetail(OrderId,ActionDate,NumOfAction,pID,OriginalQty,ActionQty,CancelQty,RemainQty,CreateOn,CreateBy,UpdateOn,UpdateBy)
										SELECT	OrderId,getdate(),NumOfAction+1,pID,OriginalQty,OriginalQty,CancelQty,OriginalQty-RemainQty,getdate(),@userId,getdate(),@userId
										FROM	TBGoodReceiveDetail
										WHERE	OrderID=@OrderID AND NumOfAction = 0";

				command.Parameters.AddWithValue("@CompanyId", goodReceive.CompanyId);
				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@Status", GoodReceive.GoodReceiveStatus.Completed.ToString());
				command.Parameters.AddWithValue("@OrderId", goodReceive.OrderId);
				command.Parameters.AddWithValue("@NumOfAction", numOfAction);
				command.ExecuteNonQuery();
			}
		}

		public void CreateGoodPartialReceive(List<GoodReceiveDetail> goodReceive, GoodReceive.GoodReceiveStatus status, string orderid, string userId, DateTime actionDate)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"UPDATE	TBGoodReceive
										SET		Status = @Status, NumOfAction = @NumOfAction, UpdateOn = getdate(), UpdateBy=@userId
										WHERE	OrderId=@OrderId";
				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@Status", status.ToString());

				int rowIndex = 0;
				foreach (var item in goodReceive)
				{
					command.CommandText += String.Format(@"  INSERT INTO TBGoodReceiveDetail (OrderId,ActionDate,NumOfAction,pID,OriginalQty,ActionQty,CancelQty,RemainQty,CreateOn,CreateBy,UpdateOn,UpdateBy)
										VALUES(@OrderId,@ActionDate,@NumOfAction,@pID_{0},@OriginalQty_{0},@ActionQty_{0},@CancelQty_{0},@RemainQty_{0},getdate(),@CreateBy_{0},getdate(),@UpdateBy_{0})", rowIndex);

					command.Parameters.AddWithValue("@pID_" + rowIndex, item.PId);
					command.Parameters.AddWithValue("@OriginalQty_" + rowIndex, item.OriginalQty);
					command.Parameters.AddWithValue("@ActionQty_" + rowIndex, item.ActionQty);
					command.Parameters.AddWithValue("@CancelQty_" + rowIndex, item.CancelQty);
					command.Parameters.AddWithValue("@RemainQty_" + rowIndex, item.RemainQty);
					command.Parameters.AddWithValue("@CreateBy_" + rowIndex, userId);
					command.Parameters.AddWithValue("@UpdateBy_" + rowIndex, userId);
					rowIndex++;
				}
				command.Parameters.AddWithValue("@ActionDate", Convert.ToDateTime(actionDate));
				command.Parameters.AddWithValue("@NumOfAction", goodReceive.First().NumOfAction);
				command.Parameters.AddWithValue("@OrderId", orderid);

				command.ExecuteNonQuery();
			}
		}

		public void UpdateOrderStatus(Order.OrderStatus status, string orderGuid, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"UPDATE	TBOrder SET OrderStatus=@Status,UpdateOn = getdate(), UpdateBy=@userId
										WHERE	OrderGuid=@OrderGuid";

				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@OrderGuid", orderGuid);
				command.Parameters.AddWithValue("@Status", status.ToString());
				command.ExecuteNonQuery();
			}
		}

		public GoodReceive GetMyGoodReceive(string orderId)
		{
			GoodReceive goodReceive = null;
			goodReceive = GetGoodReceive(orderId);
			if (goodReceive != null)
			{
				goodReceive.ItemReceiveDetail = GetGoodReceiveDetail(orderId);
			}
			return goodReceive;
		}

		private GoodReceive GetGoodReceive(string OrderId)
		{
			GoodReceive goodReceive = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT t1.*,
										isnull(t2.UserTName,'') as ReqTName, isnull(t2.UserEName,'') as ReqEName,
										isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName
										FROM TBGoodReceive t1 
										left join TBUserinfo t2 on t1.UserId=t2.UserID 
										left join TBUserinfo t3 on t1.AppUserId=t3.UserID 
										WHERE OrderID=@OrderId";
				command.Parameters.AddWithValue("@OrderId", OrderId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						goodReceive = new GoodReceive();
						goodReceive.CompanyId = (string)reader["CompanyId"];
						goodReceive.OrderId = (string)reader["OrderId"];
						goodReceive.Status = (GoodReceive.GoodReceiveStatus)Enum.Parse(typeof(GoodReceive.GoodReceiveStatus), (string)reader["Status"]);
						goodReceive.NumOfAction = (int)reader["NumOfAction"];
						goodReceive.UserId = (string)reader["UserId"];
						goodReceive.AppUserId = (string)reader["AppUserId"];
						goodReceive.CreateOn = (DateTime)reader["CreateOn"];
						goodReceive.CreateBy = (string)reader["CreateBy"];
						goodReceive.UpdateBy = (string)reader["UpdateBy"];

						goodReceive.Requester = new User(); //ดึงข้อมูลชื่อผู้สั่งซื้อ สำหรับแสดงหน้าส่งเมล์
						goodReceive.Requester.DisplayName = new LocalizedString((string)reader["ReqTName"]);
						goodReceive.Requester.DisplayName["en"] = (string)reader["ReqEName"];
						goodReceive.Requester.UserThaiName = (string)reader["ReqTName"];
						goodReceive.Requester.UserEngName = (string)reader["ReqEName"];


						goodReceive.UserApprover = new UserApprover(); //ดึงข้อมูลชื่อผู้อนุุมัติ สำหรับแสดงหน้าส่งเมล์
						goodReceive.UserApprover.Approver = new User();
						goodReceive.UserApprover.Approver.DisplayName = new LocalizedString((string)reader["AppTName"]);
						goodReceive.UserApprover.Approver.DisplayName["en"] = (string)reader["AppEName"];
						goodReceive.UserApprover.Approver.UserThaiName = (string)reader["AppTName"];
						goodReceive.UserApprover.Approver.UserEngName = (string)reader["AppEName"];
					}
				}
			}
			return goodReceive;
		}

		private IEnumerable<GoodReceiveDetail> GetGoodReceiveDetail(string OrderId)
		{
			List<GoodReceiveDetail> receiveDetail = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	T1.*,T2.pTName,T2.pEName,T2.pTUnit,T2.pEUnit,T2.Qty
										FROM	TBGoodReceiveDetail T1 INNER JOIN TBOrderDetail T2 ON T1.pID=T2.pID AND T1.OrderId=T2.OrderID
										WHERE	T1.OrderId=@OrderId";
				command.Parameters.AddWithValue("@OrderId", OrderId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					receiveDetail = new List<GoodReceiveDetail>();
					while (reader.Read())
					{
						GoodReceiveDetail receive = new GoodReceiveDetail();
						receive.Product = new Product();
						receive.Product.ThaiName = (string)reader["pTName"];
						receive.Product.EngName = (string)reader["pEName"];
						receive.Product.Name = new LocalizedString((string)reader["pTName"]);
						receive.Product.Name["en"] = new LocalizedString((string)reader["pEName"]);
						receive.Product.ThaiUnit = (string)reader["pTUnit"];
						receive.Product.EngUnit = (string)reader["pEUnit"];
						receive.Product.Unit = new LocalizedString((string)reader["pTUnit"]);
						receive.Product.Unit["en"] = new LocalizedString((string)reader["pEUnit"]);
						receive.OrderId = (string)reader["OrderId"];
						receive.ActionDate = (DateTime)reader["ActionDate"];
						receive.NumOfAction = (int)reader["NumOfAction"];
						receive.PId = (string)reader["pID"];
						receive.OriginalQty = (int)reader["OriginalQty"];
						receive.ActionQty = (int)reader["ActionQty"];
						receive.CancelQty = (int)reader["CancelQty"];
						receive.RemainQty = (int)reader["RemainQty"];
						receive.OrderQTY = (int)reader["Qty"];
						receiveDetail.Add(receive);
					}
				}
			}
			return receiveDetail;
		}

		public IEnumerable<GoodReceiveDetail> GetMaxGoodReceiveDetail(string OrderId)
		{
			List<GoodReceiveDetail> receiveDetail = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	tgrd.*
										FROM	TBGoodReceive tgr INNER JOIN TBGoodReceiveDetail tgrd ON tgr.OrderId=tgrd.OrderId AND tgr.NumOfAction=tgrd.NumOfAction
										WHERE	tgr.OrderId=@OrderId";
				command.Parameters.AddWithValue("@OrderId", OrderId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					receiveDetail = new List<GoodReceiveDetail>();
					while (reader.Read())
					{
						GoodReceiveDetail receive = new GoodReceiveDetail();
						receive.OrderId = (string)reader["OrderId"];
						receive.ActionDate = (DateTime)reader["ActionDate"];
						receive.NumOfAction = (int)reader["NumOfAction"];
						receive.PId = (string)reader["pID"];
						receive.OriginalQty = (int)reader["OriginalQty"];
						receive.ActionQty = (int)reader["ActionQty"];
						receive.CancelQty = (int)reader["CancelQty"];
						receive.RemainQty = (int)reader["RemainQty"];
						receiveDetail.Add(receive);
					}
				}
			}
			return receiveDetail;
		}

		public bool GetGoodReceivePeriod(int period, string companyId, string userId)
		{
			List<DateTime> warningDateGSItem = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	CompleteMailDate,datediff(day, dateadd(day,@Period,CompleteMailDate), getdate()) as DiffGoodReceivePeriod
										from	TBOrder tbo INNER JOIN TBGoodReceive tgr ON tbo.OrderID = tgr.OrderId
										WHERE	OrderStatus = @OrderStatus AND [Status] = @Status
												AND tgr.CompanyId=@CompanyId AND tgr.UserId=@UserID";
				command.Parameters.AddWithValue("@Period", period);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@OrderStatus", Order.OrderStatus.Shipped.ToString());
				command.Parameters.AddWithValue("@Status", GoodReceive.GoodReceiveStatus.Waiting.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.HasRows)
					{
						warningDateGSItem = new List<DateTime>();
					}
					while (reader.Read())
					{
						warningDateGSItem.Add((DateTime)reader["CompleteMailDate"]);
					}
				}
			}
			if (warningDateGSItem != null)
			{
				foreach (var item in warningDateGSItem)
				{
					DateTime expireDateGs = CalculateExprieDate(item, period);
					if (DateTime.Compare(expireDateGs, DateTime.Now.Date) < 0) { return true; }
					// DateTime.Compare(Date_1,Date_2)
					// If -[-1] is  Date_1 less than  Date_2
					// If -[0] is  Date_1 equal Date_2
					// If -[1] is  Date_1 more than Date_2
				}
			}
			return false;
		}

		public bool IsCompleteOrder(string guid)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select OrderStatus from tborder where OrderGUID = @guid";
				command.Parameters.AddWithValue("@guid", guid);
				if ((string)command.ExecuteScalar() == "Completed")
				{
					return true;
				}
			}
			return false;
		}

		public void CompleteOrder(string guid, string ipAddress)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"declare	@orderId varchar(max)
										declare	@userId varchar(max)
										select	@OrderID=OrderID,@userId=userid from tborder where OrderGUID=@guid

										update	tborder set OrderStatus='Completed',UpdateBy=@userId,UpdateOn=getdate() where OrderGUID = @guid
										insert	TBOrderProcessLog (OrderID,ProcessRemark,Remark,IPAddress,CreateOn,CreateBy) 
												values (@orderId,'Change OrderStatus','เปลี่ยนจาก Shipped เป็น Completed',@ipAddress,getdate(),@userId)
										insert	TBOrderActivity (OrderID,ActivityTName,ActivityEName,Remark,IPAddress,CreateOn,CreateBy)
												values(@orderId,'ยืนยันการรับสินค้า','Confirmation of receipt','เปลี่ยนจาก Shipped เป็น Completed',@ipAddress,getdate(),@userId)";
				command.Parameters.AddWithValue("@guid", guid);
				command.Parameters.AddWithValue("@ipAddress", ipAddress);
				command.ExecuteNonQuery();
			}
		}

		public void UpdateOrderEditByOFMAdmin(string orderId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"
										Update TBOrder set OrderBeforeStatus = OrderStatus, PreviousAppID = CurrentAppID where OrderID=@OrderID
										Update TBOrder set OrderStatus = @OrderStatus, CurrentAppID = '', NextAppID = '', Updateby = @Updateby, UpdateOn = getdate() where OrderID=@OrderID ";

				command.Parameters.AddWithValue("@OrderID", orderId);
				command.Parameters.AddWithValue("@Updateby", userId);
				command.Parameters.AddWithValue("@OrderStatus", Order.OrderStatus.Deleted.ToString());
				command.ExecuteNonQuery();
			}
		}

		//กรณีที่กดปุ่ม ApproveOrder ให้ไปเช็คว่าเคยทำการ Approve ไปแล้วหรือไม่
		public bool IsApproveOrder(string orderId, Order.OrderStatus status)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT OrderStatus FROM TBOrder where OrderID=@OrderID";

				command.Parameters.AddWithValue("@OrderID",orderId);
				return Convert.ToString(command.ExecuteScalar()) == status.ToString();
			}
		}

		//        public void SetUseSMSFeature(string userId, bool useSMSFeature,string orderId)
		//        {
		//            using (SqlCommand command = CreateCommandEnsureConnectionOpen())
		//            {
		//                command.CommandText = @"UPDATE TBOrder 
		//										SET UseSMSFeature=@UseSMSFeature 
		//										WHERE UserID=@UserID and orderId=@OrderId";
		//                command.Parameters.AddWithValue("@UserID", userId);
		//                command.Parameters.AddWithValue("@UseSMSFeature", Mapper.ToDatabase(useSMSFeature));
		//                command.Parameters.AddWithValue("@OrderId",orderId);
		//                command.ExecuteNonQuery();
		//            }
		//        }

	}
}




