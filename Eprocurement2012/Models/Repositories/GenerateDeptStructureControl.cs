﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.IO;
using System.Xml;
using System.Text;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class GenerateDeptStructureControl : RepositoryBase
	{
		public GenerateDeptStructureControl(SqlConnection connection) : base(connection) { }

		public string GentDeptStructure(string companyId)
		{
			//Delete data is equal companyId.
			clearDeptStructureControl(companyId);

			//Get product data is equal companyId.
			Dictionary<string, HashSet<string>> companyCatalog = GetCompanyDict(companyId);
			if (!companyCatalog.Any())
			{
				return "CompanyId : " + companyId + " ยังไม่มีข้อมูลในตาราง TBProductCatalog";
			}
			Dictionary<string, HashSet<string>> specialCatalog = GetOfficemateSpecialProductDict(companyId);

			Dictionary<string, HashSet<int>> deptProductDict = GetDeptProductDict();

			Dictionary<int, Department> deptDict = GetDepartmentDict();

			List<DeptStructureControl> userList = GetUsers(companyId);

			foreach (var user in userList)
			{
				if (companyCatalog.ContainsKey(user.CompanyId))
				{
					HashSet<string> productList = companyCatalog[user.CompanyId];
					if (specialCatalog.ContainsKey(user.UserId))
					{
						productList.UnionWith(specialCatalog[user.UserId]);
					}

					HashSet<int> deptIdList = new HashSet<int>();
					foreach (string productId in productList)
					{
						if (deptProductDict.ContainsKey(productId))
						{
							deptIdList.UnionWith(deptProductDict[productId]);
						}
					}
					List<Department> deptList = deptIdList.Select(id => deptDict.ContainsKey(id) ? deptDict[id] : new Department()).ToList();
					List<Department> deptListwithoutnull = deptList.Where(id => id.DeptId != 0).ToList();
					string stringWriter = GenerateDeptXml(deptListwithoutnull);
					AddDeptStructureControl(user.UserId, user.CompanyId, stringWriter);
				}
			}
			return "CompanyId : " + companyId + " ดำเนินการแล้วค่ะ";
		}

		public void GentDeptStructureForOFM()
		{
			string companyOFM = "Officemate";

			//Delete data is equal companyId.
			clearDeptStructureControl(companyOFM);

			Dictionary<string, HashSet<int>> deptProductDict = GetDeptProductDict();

			Dictionary<int, Department> deptDict = GetDepartmentDict();

			HashSet<int> deptIdList = new HashSet<int>();
			deptIdList.UnionWith(deptProductDict.SelectMany(d => d.Value).Distinct());

			List<Department> deptList = deptIdList.Select(id => deptDict.ContainsKey(id) ? deptDict[id] : new Department()).ToList();
			List<Department> deptListwithoutnull = deptList.Where(id => id.DeptId != 0).ToList();
			string stringWriter = GenerateDeptXml(deptListwithoutnull);
			AddDeptStructureControl("OFMCatalog", companyOFM, stringWriter);
		}

		public string GentDeptStructureForOCI(string companyId)
		{
			//Delete data is equal companyId.
			clearDeptStructureControl(companyId);

			//Get product data is equal companyId.
			Dictionary<string, HashSet<string>> companyCatalog = GetCompanyDict(companyId);
			if (!companyCatalog.Any())
			{
				return "CompanyId : " + companyId + " ยังไม่มีข้อมูลในตาราง TBProductCatalog";
			}
			Dictionary<string, HashSet<string>> specialCatalog = GetOfficemateSpecialProductDict(companyId);

			Dictionary<string, HashSet<int>> deptProductDict = GetDeptProductDict();

			Dictionary<int, Department> deptDict = GetDepartmentDict();

			List<DeptStructureControl> userList = GetOCIUsers(companyId);

			foreach (var user in userList)
			{
				if (companyCatalog.ContainsKey(user.CompanyId))
				{
					HashSet<string> productList = companyCatalog[user.CompanyId];
					if (specialCatalog.ContainsKey(user.UserId))
					{
						productList.UnionWith(specialCatalog[user.UserId]);
					}

					HashSet<int> deptIdList = new HashSet<int>();
					foreach (string productId in productList)
					{
						if (deptProductDict.ContainsKey(productId))
						{
							deptIdList.UnionWith(deptProductDict[productId]);
						}
					}
					List<Department> deptList = deptIdList.Select(id => deptDict.ContainsKey(id) ? deptDict[id] : new Department()).ToList();
					List<Department> deptListwithoutnull = deptList.Where(id => id.DeptId != 0).ToList();
					string stringWriter = GenerateDeptXml(deptListwithoutnull);
					AddDeptStructureControl(user.UserId, user.CompanyId, stringWriter);
				}
			}
			return "CompanyId : " + companyId + " ดำเนินการแล้วค่ะ";
		}

		private void clearDeptStructureControl(string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"DELETE FROM TBDeptStructureControl WHERE CompanyId = @companyId";
				command.Parameters.AddWithValue("@companyId", companyId);
				command.ExecuteNonQuery();
			}
		}

		private Dictionary<string, HashSet<string>> GetCompanyDict(string companyId)
		{
			Dictionary<string, HashSet<string>> catalog = new Dictionary<string, HashSet<string>>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	DISTINCT CatalogTypeID,CodeId
										FROM	TBProductCatalog
										WHERE	CatalogType = 'Company' AND CompanyID = @companyId
												ORDER BY CatalogTypeID";
				command.Parameters.AddWithValue("@companyId", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						string catalogTypeId = (string)reader["CatalogTypeId"];
						string codeId = (string)reader["CodeId"];
						if (!catalog.ContainsKey(catalogTypeId))
						{
							catalog.Add(catalogTypeId, new HashSet<string>());
						}
						catalog[catalogTypeId].Add(codeId);
					}
				}
				return catalog;
			}
		}

		private Dictionary<string, HashSet<string>> GetOfficemateSpecialProductDict(string companyId)
		{
			Dictionary<string, HashSet<string>> catalog = new Dictionary<string, HashSet<string>>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	DISTINCT CatalogTypeID,CodeId
										FROM	TBProductCatalog
										WHERE	CatalogType = 'User' AND CompanyID = @companyId
												ORDER BY CatalogTypeID";
				command.Parameters.AddWithValue("@companyId", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						string catalogTypeId = (string)reader["CatalogTypeId"];
						string codeId = (string)reader["CodeId"];
						if (!catalog.ContainsKey(catalogTypeId))
						{
							catalog.Add(catalogTypeId, new HashSet<string>());
						}
						catalog[catalogTypeId].Add(codeId);
					}
				}
				return catalog;
			}
		}

		private Dictionary<string, HashSet<int>> GetDeptProductDict()
		{
			Dictionary<string, HashSet<int>> productDict = new Dictionary<string, HashSet<int>>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	Codeid,ds.DeptId
										from	TBDeptProductFilter dpf Inner join TBDeptStructure ds on ds.refid = dpf.refid
										where	DeptStatus = 'Active'
												order by ISdefault DESC,CodeID";
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						string codeid = (string)reader["CodeID"];
						int deptId = (int)reader["DeptId"];
						if (!productDict.ContainsKey(codeid))
						{
							productDict.Add(codeid, new HashSet<int>());
						}
						productDict[codeid].Add(deptId);
					}
				}
				return productDict;
			}
		}

		private Dictionary<int, Department> GetDepartmentDict()
		{
			Dictionary<int, Department> deptDict = new Dictionary<int, Department>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	ds.RefID,ds.DeptId,ds.ParentDeptId,dm.DeptThaiName
										from	TBDeptStructure as ds inner join TBDeptMaster dm on dm.deptid = ds.deptid
										where	DeptStatus = 'Active'
												order by ISdefault DESC,DeptId";
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Department dept = new Department()
						{
							RefId = (string)reader["RefID"],
							DeptId = (int)reader["DeptId"],
							ParentDeptId = (int)reader["ParentDeptId"],
							DeptName = (string)reader["DeptThaiName"]
						};
						if (!deptDict.ContainsKey((int)reader["DeptId"]))
						{
							deptDict.Add((int)reader["DeptId"], dept);
						}
					}
				}
				return deptDict;
			}
		}

		private List<DeptStructureControl> GetUsers(string companyId)
		{
			List<DeptStructureControl> userList = new List<DeptStructureControl>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
//                command.CommandText = @"SELECT	u.UserID,uc.CompanyID
//										FROM	TBUserInfo u INNER JOIN TBUserCompany uc ON u.UserID = uc.UserID
//												INNER JOIN TBCompanyInfo ci ON uc.CompanyID = ci.CompanyID
//										WHERE	UserStatus = 'Active' and UseOfmCatalog = 'No' and uc.CompanyID = @companyId";

				command.CommandText = @"SELECT	u.UserID,uc.CompanyID
										FROM	TBUserInfo u INNER JOIN TBUserCompany uc ON u.UserID = uc.UserID
												INNER JOIN TBCompanyInfo ci ON uc.CompanyID = ci.CompanyID
										WHERE	uc.UserStatus = 'Active' and UseOfmCatalog = 'No' and uc.CompanyID = @companyId";

				command.Parameters.AddWithValue("@companyId", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						userList.Add(new DeptStructureControl() { UserId = (string)reader["UserID"], CompanyId = (string)reader["CompanyID"] });
					}
				}
				return userList;
			}
		}

		private List<DeptStructureControl> GetOCIUsers(string companyId)
		{
			List<DeptStructureControl> userList = new List<DeptStructureControl>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	UserID,CompanyID
										FROM	TBOCIUserInfo
										WHERE	UserStatus = 'Active' and CompanyID = @companyId";
				command.Parameters.AddWithValue("@companyId", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						userList.Add(new DeptStructureControl() { UserId = (string)reader["UserID"], CompanyId = (string)reader["CompanyID"] });
					}
				}
				return userList;
			}
		}

		private string GenerateDeptXml(List<Department> deptlist)
		{
			List<Department> parentSet = deptlist.Where(m => m.ParentDeptId == 0).ToList<Department>();
			List<Department> deptListNotParent = deptlist.Except(parentSet).ToList<Department>();

			List<Ancestor> ancesterList = new List<Ancestor>();
			foreach (var dept in parentSet)
			{
				Ancestor ancester = new Ancestor();

				ancester.AncestorDepartment = dept;
				if (dept.ParentDeptId == 0)
				{
					ancester.ParentDept = new Department() { DeptId = 0, ParentDeptId = 0, RefId = "0" };
				}
				List<Department> childs = deptListNotParent.Where(m => m.ParentDeptId == dept.DeptId).ToList<Department>();
				List<Child> listOfchild = new List<Child>();
				foreach (var child in childs)
				{
					Child newChild = new Child();
					newChild.ChildDepartment = child;
					listOfchild.Add(newChild);
				}
				ancester.ChildDept = listOfchild;

				foreach (var item in ancester.ChildDept)
				{
					List<Department> grandChilds = deptListNotParent.Where(m => m.ParentDeptId == item.ChildDepartment.DeptId).ToList<Department>();
					List<Department> listOfGrandChild = new List<Department>();
					foreach (var grandChild in grandChilds)
					{
						Department grandChildDept = new Department();
						grandChildDept = grandChild;
						listOfGrandChild.Add(grandChildDept);
					}
				}
				ancesterList.Add(ancester);
			}

			StringBuilder stringBuilder = new StringBuilder();
			StringWriterWithEncoding stringWriter = new StringWriterWithEncoding(stringBuilder, Encoding.UTF8);

			XmlWriterSettings settings = new XmlWriterSettings();
			settings.Indent = true;
			settings.CloseOutput = true;

			using (XmlTextWriter writer = new XmlTextWriter(stringWriter))
			{
				writer.WriteStartDocument();
				writer.WriteStartElement("Cats");
				foreach (var dept in parentSet)
				{
					WriteDeptElement(dept, writer, deptListNotParent);
				}
				writer.WriteEndElement();
				writer.WriteEndDocument();
			}
			return stringWriter.GetStringBuilder().ToString();
		}

		private void WriteDeptElement(Department dept, XmlTextWriter writer, List<Department> deptsList)
		{
			writer.WriteStartElement("Department");
			writer.WriteAttributeString("Id", dept.DeptId.ToString());
			writer.WriteAttributeString("Name", dept.DeptName);
			var childDept = deptsList.Where(m => m.ParentDeptId == dept.DeptId).ToList<Department>();
			foreach (var child in childDept)
			{
				WriteDeptElement(child, writer, deptsList);
			}
			writer.WriteEndElement();
		}

		private void AddDeptStructureControl(string userId, string companyId, string xmlstr)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"INSERT INTO TBDeptStructureControl (UserID,CompanyID,DeptId)
										VALUES (@UserID,@CompanyID,@DeptId)
										";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@DeptId", xmlstr);
				command.ExecuteNonQuery();
			}
		}

		#region all model
		public class StringWriterWithEncoding : StringWriter
		{
			public StringWriterWithEncoding(StringBuilder sringBuilder, Encoding encoding) : base(sringBuilder)
			{
				this.m_Encoding = encoding;
			}
			private readonly Encoding m_Encoding;
			public override Encoding Encoding
			{
				get
				{
					return this.m_Encoding;
				}
			}
		}

		private sealed class Child
		{
			public Department ChildDepartment { get; set; }
			public IEnumerable<Department> GrandChild { get; set; }
		}

		private sealed class Ancestor
		{
			public Department ParentDept { get; set; }
			public Department AncestorDepartment { get; set; }
			public IEnumerable<Child> ChildDept { get; set; }
		}

		private class Department
		{
			public string RefId { get; set; }
			public int DeptId { get; set; }
			public int ParentDeptId { get; set; }
			public string DeptName { get; set; }
		}

		private class Company
		{
			public string CompanyId { get; set; }
			public string CatalogType { get; set; }
		}

		private class DeptStructureControl
		{
			public string UserId { get; set; }
			public string CompanyId { get; set; }
			public string CatalogType { get; set; }
			public string Xml { get; set; }
		}
		#endregion

	}
}