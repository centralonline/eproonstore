﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class ProductCatalogRepository : RepositoryBase
	{
		public ProductCatalogRepository(SqlConnection connection) : base(connection) { }

		public int GetCountProductCatalogType(string companyId, ProductCatalog.ProductCatalogType productType)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select count(DISTINCT ProductID) as TotalProduct
										from TBProductCatalog t1 INNER JOIN TBProductCenter t2 ON t1.ProductID = t2.pID";
				if (productType == ProductCatalog.ProductCatalogType.User) { command.CommandText += " INNER JOIN TBUserInfo t3 ON t3.UserID = t1.CatalogTypeID "; }
				command.CommandText += " where CompanyID=@CompanyID AND CatalogType=@CatalogType";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@CatalogType", productType.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (int)reader["TotalProduct"];
					}
				}
				return 0;
			}
		}
		public bool HaveProductInYourRequestList(string productId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select count(ProductID) as CountProductID from TBProductCatalogRequest Where CompanyID = @CompanyID and ProductId = @ProductId ";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@ProductId", productId);
				return (int)command.ExecuteScalar() > 0;
			}
		}
		public bool HaveProductInYourCompanyList(string productId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select count(ProductID) as CountProductID from TBProductCatalog Where CompanyID = @CompanyID and ProductId = @ProductId and CatalogType = @CatalogType and CatalogTypeID = @CompanyID";
				command.Parameters.AddWithValue("@CatalogType", ProductCatalog.ProductCatalogType.Company.ToString());
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@ProductId", productId);
				return (int)command.ExecuteScalar() > 0;
			}
		}
		public bool HaveProductInYourUserList(string productId, string companyId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select count(ProductID) as CountProductID from TBProductCatalog Where CompanyID = @CompanyID and ProductId = @ProductId and CatalogType = @CatalogType and CatalogTypeID = @CatalogTypeID";
				command.Parameters.AddWithValue("@CatalogType", ProductCatalog.ProductCatalogType.User.ToString());
				command.Parameters.AddWithValue("@CatalogTypeID", userId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@ProductId", productId);
				return (int)command.ExecuteScalar() > 0;
			}
		}

		public IEnumerable<Product> GetItemProductTemplate(string templateId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select DISTINCT ProductID,pTName,pEName,PriceIncVat,pTUnit,pEUnit,FullPrice,PriceExcVat,CodeId,IsPromotion 
										from TBProductTemplate t1 with (nolock) inner JOIN TBProductCenter t2 with (nolock) ON t1.ProductID=t2.pID
										WHERE TemplateID = @TemplateID ";
				command.Parameters.AddWithValue("@TemplateID ", templateId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					List<Product> item = new List<Product>();
					Product product = null;
					while (reader.Read())
					{
						product = new Product();
						product.Id = (string)reader["ProductID"];
						product.EngName = (string)reader["pEName"];
						product.ThaiName = (string)reader["pTName"];
						product.Name = new LocalizedString((string)reader["pTName"]);
						product.Name["en"] = (string)reader["pEName"];
						product.FullPriceIncVat = (decimal)reader["FullPrice"];
						product.PriceIncVat = (decimal)reader["PriceIncVat"];
						product.PriceExcVat = (decimal)reader["PriceExcVat"];
						product.CodeId = (string)reader["CodeId"];
						product.ThaiUnit = (string)reader["pTUnit"];
						product.EngUnit = (string)reader["pEUnit"];
						product.Unit = new LocalizedString((string)reader["pTUnit"]);
						product.Unit["en"] = (string)reader["pEUnit"];
						product.IsPromotion = Mapper.ToClass<bool>((string)reader["IsPromotion"]);
						item.Add(product);
					}
					return item;
				}
			}
		}
		public IEnumerable<ProductTemplate> GetProductTemplate()
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select TemplateID,count(ProductID)as CountProduct from TBProductTemplate with (nolock) GROUP BY TemplateID ";
				using (SqlDataReader reader = command.ExecuteReader())
				{
					List<ProductTemplate> item = new List<ProductTemplate>();
					ProductTemplate productTemplate = null;
					while (reader.Read())
					{
						productTemplate = new ProductTemplate();
						productTemplate.TemplateId = (string)reader["TemplateID"];
						productTemplate.TotalProduct = (int)reader["CountProduct"];
						item.Add(productTemplate);
					}
					return item;
				}
			}
		}

		public void AddProductCatalogRequest(string productId, string companyId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBProductCatalogRequest(CompanyID,ProductID,CreateOn,CreateBy)values(@CompanyID,@ProductID,getdate(),@CreateBy)";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@ProductID", productId);
				command.Parameters.AddWithValue("@CreateBy", userId);
				command.ExecuteNonQuery();
			}

		}
		public void DeleteProductCatalogRequest(string productId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Delete  TBProductCatalogRequest Where CompanyID = @CompanyID and ProductID = @ProductID";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@ProductID", productId);
				command.ExecuteNonQuery();
			}
		}

		public void AddProductCatalog(string productId, string companyId, DateTime validFrom, DateTime validTo, string createBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"delete TBProductCatalog Where CompanyID = @CompanyID and ProductID = @ProductID

										insert into TBProductCatalog
										(
											CompanyID,CatalogType,CatalogTypeID,ProductID,PriceExcVat,PriceIncVat,IsVat,PriceType,IsBestDeal,
											CreateOn,CreateBy,UpdateOn,UpdateBy,ValidFrom,ValidTo,CodeId
										)
										select	@CompanyID,@CatalogType,@CompanyID,Pid,PriceExcVat,PriceIncVat,IsVat,'Float',IsBestDeal,
												getdate(),@CreateBy,getdate(),@UpdateBy,@ValidFrom,@ValidTo,CodeId
										from	TBProductCenter
										WHERE	pID=@ProductId

										delete TBProductCatalogRequest WHERE CompanyID = @CompanyID
										";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@ProductId", productId);
				command.Parameters.AddWithValue("@ValidFrom", validFrom.ToString("yyyy-MM-dd HH:mm"));
				command.Parameters.AddWithValue("@ValidTo", validTo.ToString("yyyy-MM-dd HH:mm"));
				command.Parameters.AddWithValue("@CatalogType", ProductCatalog.ProductCatalogType.Company.ToString());
				command.Parameters.AddWithValue("@CreateBy", createBy);
				command.Parameters.AddWithValue("@UpdateBy", createBy);

				command.ExecuteNonQuery();

				AddProductCatalogLog(companyId, "", productId, validFrom, validTo, "Insert", createBy,"");
			}

		}
		public void AddProductCatalogToRequester(string productId, string userId, string companyId, DateTime validFrom, DateTime validTo, string createBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBProductCatalog
											(
												CompanyID,CatalogType,CatalogTypeID,ProductID,PriceExcVat,PriceIncVat,IsVat,PriceType,IsBestDeal,
												CreateOn,CreateBy,UpdateOn,UpdateBy,ValidFrom,ValidTo,CodeId
											)
										select	@CompanyID,@CatalogType,@CatalogTypeID,Pid,PriceExcVat,PriceIncVat,IsVat,'Float',IsBestDeal,
												getdate(),@CreateBy,getdate(),@UpdateBy,@ValidFrom,@ValidTo,CodeId
										from	TBProductCenter
										WHERE	pID=@ProductId

										delete TBProductCatalogRequest WHERE CompanyID = @CompanyID
										";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@ProductId", productId);
				command.Parameters.AddWithValue("@CatalogTypeID", userId);
				command.Parameters.AddWithValue("@ValidFrom", validFrom.ToString("yyyy-MM-dd HH:mm"));
				command.Parameters.AddWithValue("@ValidTo", validTo.ToString("yyyy-MM-dd HH:mm"));
				command.Parameters.AddWithValue("@CatalogType", ProductCatalog.ProductCatalogType.User.ToString());
				command.Parameters.AddWithValue("@CreateBy", createBy);
				command.Parameters.AddWithValue("@UpdateBy", createBy);

				command.ExecuteNonQuery();

				AddProductCatalogLog(companyId, userId, productId, validFrom, validTo, "Insert", createBy, "");
			}

		}
		public void DeleteProductCatalog(string companyId, ProductCatalog.ProductCatalogType catalogType, string catalogTypeID, string productId, string createBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"DECLARE @pricetype varchar(max) 
										SET @pricetype = (select top 1 pricetype
										from TBProductCatalog
										where	CompanyID=@CompanyID AND CatalogType=@CatalogType AND CatalogTypeID=@CatalogTypeID AND ProductID=@ProductID)

										DELETE FROM TBProductCatalog
										where	CompanyID=@CompanyID AND CatalogType=@CatalogType AND CatalogTypeID=@CatalogTypeID AND ProductID=@ProductID

										SELECT @pricetype
										";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@CatalogType", catalogType.ToString());
				command.Parameters.AddWithValue("@CatalogTypeID", catalogTypeID);
				command.Parameters.AddWithValue("@ProductID", productId);

				string pricetype = (string)command.ExecuteScalar();

				AddProductCatalogLog(companyId, "", productId, DateTime.MinValue, DateTime.MinValue, "Delete", createBy, pricetype);
			}
		}

		private IEnumerable<CustCompany> GetCustCompany(string companyId)
		{
			List<CustCompany> custs = new List<CustCompany>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT comp.*
										FROM TBCustCompany comp
										INNER JOIN TBCustMaster mast ON comp.CustID = mast.CustID
										WHERE comp.CompanyID = @CompanyID";

				command.Parameters.AddWithValue("@CompanyID", companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						CustCompany cust = new CustCompany();
						cust.CustId = (string)reader["CustId"];
						cust.CompanyId = (string)reader["CompanyId"];
						cust.IsDefault = (string)reader["IsDefault"];
						custs.Add(cust);
					}
					return custs;
				}
			}
		}

		private void AddProductCatalogLog(string companyId, string userId, string productId, DateTime validFrom, DateTime validTo, string ActionType, string createBy, string pricetype)
		{
			if (validFrom == DateTime.MinValue) { validFrom = new DateTime(1900, 01, 01); }
			if (validTo == DateTime.MinValue) { validTo = new DateTime(1900, 01, 01); }
			if (string.IsNullOrEmpty(pricetype)) { pricetype = "Float"; }
			IEnumerable<CustCompany> listCust = GetCustCompany(companyId);
			StringBuilder sqlBuilder = new StringBuilder(2048);
			int rowIndex = 0;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				foreach (var item in listCust)
				{
					sqlBuilder.AppendFormat(@"
											INSERT	INTO TBProductCatalogLog
											(CompanyID,UserID,ProductID,PriceExcVat,PriceIncVat,IsVat,PriceType,IsBestDeal,ValidFrom,ValidTo,ActionType,Remark,CreateOn,CreateBy,CustID)
											SELECT	@CompanyID,@UserID,Pid,PriceExcVat,PriceIncVat,IsVat,@PriceType,IsBestDeal,@ValidFrom,@ValidTo,@ActionType,'',getdate(),@CreateBy,@CustId_{0}
											FROM	TBProductCenter
											WHERE	pID=@ProductId
										", rowIndex);
					sqlBuilder.AppendLine();
					command.Parameters.AddWithValue("@CustId_" + rowIndex, item.CustId);
					rowIndex++;
				}
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@ProductId", productId);
				command.Parameters.AddWithValue("@ValidFrom", validFrom.ToString("yyyy-MM-dd HH:mm"));
				command.Parameters.AddWithValue("@ValidTo", validTo.ToString("yyyy-MM-dd HH:mm"));
				command.Parameters.AddWithValue("@ActionType", ActionType);
				command.Parameters.AddWithValue("@CreateBy", createBy);
				command.Parameters.AddWithValue("@PriceType", pricetype);
				command.CommandText = sqlBuilder.ToString();
				command.ExecuteNonQuery();
			}

		}

		public IEnumerable<ProductCatalog> GetSearchProductCatalog(string keywords, string companyId)
		{
			List<ProductCatalog> productCatalogs = new List<ProductCatalog>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT t1.*,t2.pTName,t2.pEName,t2.pTUnit,t2.pEUnit,t2.FullPrice FROM TBProductCatalog t1 with (nolock)
										INNER JOIN TBProductCenter t2 with (nolock) on t1.ProductID=t2.pID
										WHERE CompanyID=@companyId AND( pTName LIKE @keywords OR pEName LIKE @keywords OR ProductID LIKE @keywords)
										ORDER BY ProductID";

				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@keywords", "%" + keywords + "%");

				using (SqlDataReader reader = command.ExecuteReader())
				{
					ProductCatalog item = null;

					while (reader.Read())
					{
						item = new ProductCatalog();
						item.ProductDetail = new Product();
						item.ProductDetail.Id = (string)reader["ProductID"];
						item.ProductDetail.ThaiName = (string)reader["pTName"];
						item.ProductDetail.EngName = (string)reader["pEName"];
						item.ProductDetail.Name = new LocalizedString((string)reader["pTName"]);
						item.ProductDetail.Name["en"] = (string)reader["pEName"];
						item.ProductDetail.FullPriceIncVat = (decimal)reader["FullPrice"];
						item.ProductDetail.PriceIncVat = (decimal)reader["PriceIncVat"];
						item.ProductDetail.PriceExcVat = (decimal)reader["PriceExcVat"];
						item.ProductDetail.Unit = new LocalizedString((string)reader["pTUnit"]);
						item.ProductDetail.Unit["en"] = (string)reader["pEUnit"];
						item.CatalogTypeId = (string)reader["CatalogTypeId"];
						item.PropType = (ProductCatalog.ProductCatalogType)Enum.Parse(typeof(ProductCatalog.ProductCatalogType), (string)reader["CatalogType"], true);
						item.ProductDetail.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						productCatalogs.Add(item);
					}
				}
			}
			return productCatalogs;
		}
		public IEnumerable<Product> GetProductCatalogRequestWaitForSetting(string companyId)
		{
			List<Product> item = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.ProductID,t2.pTName,t2.pEName,FullPrice,t2.PriceExcVat,t2.PriceIncVat,t2.CodeId,pTUnit,pEUnit,IsPromotion,t2.Status,t2.pCatId
										from	TBProductCatalogRequest t1 with (nolock) INNER JOIN TBProductCenter t2 with (nolock) ON t1.ProductID = t2.pID
										where	CompanyID = @CompanyID";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					Product product = null;
					item = new List<Product>();
					while (reader.Read())
					{
						product = new Product();
						product.Id = (string)reader["ProductID"];
						product.EngName = (string)reader["pEName"];
						product.ThaiName = (string)reader["pTName"];
						product.Name = new LocalizedString((string)reader["pTName"]);
						product.Name["en"] = (string)reader["pEName"];
						product.FullPriceIncVat = (decimal)reader["FullPrice"];
						product.PriceIncVat = (decimal)reader["PriceIncVat"];
						product.PriceExcVat = (decimal)reader["PriceExcVat"];
						product.CodeId = (string)reader["CodeId"];
						product.ThaiUnit = (string)reader["pTUnit"];
						product.EngUnit = (string)reader["pEUnit"];
						product.Unit = new LocalizedString((string)reader["pTUnit"]);
						product.Unit["en"] = (string)reader["pEUnit"];
						product.IsPromotion = Mapper.ToClass<bool>((string)reader["IsPromotion"]);
						item.Add(product);
					}
				}
			}
			return item;
		}
		public IEnumerable<Product> GetProductCatalogWaitForSetting(string companyId)
		{
			List<Product> item = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"
											select t1.ProductID,t2.pTName,t2.pEName,FullPrice,t2.PriceExcVat,t2.PriceIncVat,t1.CodeId,pTUnit,pEUnit,IsPromotion,t1.PriceType
											from TBProductCatalog  t1 with (nolock) INNER JOIN TBProductCenter t2 with (nolock) ON t1.ProductID = t2.pID
											where CompanyID = @CompanyID AND CatalogType= @CatalogType";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@CatalogType", ProductCatalog.ProductCatalogType.Hold.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					Product product = null;
					while (reader.Read())
					{
						product = new Product();
						product.Id = (string)reader["ProductID"];
						product.EngName = (string)reader["pEName"];
						product.ThaiName = (string)reader["pTName"];
						product.Name = new LocalizedString((string)reader["pTName"]);
						product.Name["en"] = (string)reader["pEName"];
						product.FullPriceIncVat = (decimal)reader["FullPrice"];
						product.PriceIncVat = (decimal)reader["PriceIncVat"];
						product.PriceExcVat = (decimal)reader["PriceExcVat"];
						product.CodeId = (string)reader["CodeId"];
						product.ThaiUnit = (string)reader["pTUnit"];
						product.EngUnit = (string)reader["pEUnit"];
						product.Unit = new LocalizedString((string)reader["pTUnit"]);
						product.Unit["en"] = (string)reader["pEUnit"];
						product.IsPromotion = Mapper.ToClass<bool>((string)reader["IsPromotion"]);
						product.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						item.Add(product);
					}
				}
			}
			return item;
		}
		public IEnumerable<Product> GetProductCatalog(string companyId)
		{
			List<Product> item = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	UserGuID,t3.UserID,UserTName,UserEName,t1.*,t2.pTName,t2.pEName,t2.PriceExcVat,t2.PriceIncVat,ValidFrom,ValidTo,FullPrice,pTUnit,pEUnit,IsPromotion,t1.PriceType
										from	TBProductCatalog  t1 with (nolock) INNER JOIN TBProductCenter t2 with (nolock) ON t1.ProductID = t2.pID
												INNER JOIN TBUserInfo t3 with (nolock) ON t3.UserID = t1.CatalogTypeID
												INNER JOIN TBUsers t4 with (nolock) ON t3.UserID=t4.UserID
										where	CompanyID=@CompanyID AND CatalogType=@CatalogType ORDER BY t1.ProductID";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@CatalogType", ProductCatalog.ProductCatalogType.User.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					Product product = null;
					item = new List<Product>();
					while (reader.Read())
					{
						product = new Product();
						product.Id = (string)reader["ProductID"];
						product.ThaiName = (string)reader["pTName"];
						product.EngName = (string)reader["pEName"];
						product.Name = new LocalizedString((string)reader["pTName"]);
						product.Name["en"] = (string)reader["pEName"];
						product.FullPriceIncVat = (decimal)reader["FullPrice"];
						product.PriceIncVat = (decimal)reader["PriceIncVat"];
						product.PriceExcVat = (decimal)reader["PriceExcVat"];
						product.CodeId = (string)reader["CodeId"];
						product.ThaiUnit = (string)reader["pTUnit"];
						product.EngUnit = (string)reader["pEUnit"];
						product.Unit = new LocalizedString((string)reader["pTUnit"]);
						product.Unit["en"] = (string)reader["pEUnit"];
						product.IsPromotion = Mapper.ToClass<bool>((string)reader["IsPromotion"]);
						product.User = new User();
						product.User.UserId = (string)reader["UserId"];
						product.User.UserGuid = (string)reader["UserGuid"];
						product.User.UserThaiName = (string)reader["UserTName"];
						product.User.UserEngName = (string)reader["UserEName"];
						product.User.DisplayName = new LocalizedString((string)reader["UserTName"]);
						product.User.DisplayName["en"] = (string)reader["UserEName"];
						product.ValidFrom = (DateTime)reader["ValidFrom"];
						product.ValidTo = (DateTime)reader["ValidTo"];
						product.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						item.Add(product);
					}
				}
			}
			return item;
		}
		public IEnumerable<ProductCatalog> GetAllProductBySearch(SearchProduct searchProduct)
		{
			List<ProductCatalog> products = new List<ProductCatalog>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder();
				sqlBuilder.AppendLine("SELECT t1.*,t2.pTName,t2.pEName,t2.pTUnit,t2.pEUnit,t2.FullPrice");
				sqlBuilder.AppendLine("FROM TBProductCatalog t1 with (nolock) INNER JOIN TBProductCenter t2 with (nolock) on t1.ProductID=t2.pID");
				sqlBuilder.AppendLine("WHERE 1=1");

				if (searchProduct.CheckCreateProduct && searchProduct.CreateDateFrom != DateTime.MinValue && searchProduct.CreateDateTo != DateTime.MinValue)
				{
					sqlBuilder.AppendLine(" AND t1.CreateOn BETWEEN @CreateDateFrom AND @CreateDateTo");
					command.Parameters.AddWithValue("@CreateDateFrom", searchProduct.CreateDateFrom);
					command.Parameters.AddWithValue("@CreateDateTo", searchProduct.CreateDateTo.AddDays(1));
				}
				if (searchProduct.CheckCompanyId && !string.IsNullOrEmpty(searchProduct.CompanyId))
				{
					sqlBuilder.AppendLine(" AND CompanyID LIKE @CompanyID");
					command.Parameters.AddWithValue("@CompanyID", "%" + searchProduct.CompanyId + "%");
				}
				if (searchProduct.CheckProductId && !string.IsNullOrEmpty(searchProduct.ProductIdFrom) && !string.IsNullOrEmpty(searchProduct.ProductIdTo))
				{
					sqlBuilder.AppendLine(" AND ProductID BETWEEN @ProductIdFrom AND @ProductIdTo");
					command.Parameters.AddWithValue("@ProductIdFrom", searchProduct.ProductIdFrom);
					command.Parameters.AddWithValue("@ProductIdTo", searchProduct.ProductIdTo);
				}
				if (searchProduct.CheckCreateBy && !string.IsNullOrEmpty(searchProduct.CreateBy))
				{
					sqlBuilder.AppendLine(" AND t1.CreateBy=@CreateBy");
					command.Parameters.AddWithValue("@CreateBy", searchProduct.CreateBy);
				}
				if (searchProduct.CheckCatalogTypeId && !string.IsNullOrEmpty(searchProduct.CatalogTypeId))
				{
					sqlBuilder.AppendLine("AND CatalogTypeID=@CatalogTypeID");
					command.Parameters.AddWithValue("@CatalogTypeID", searchProduct.CatalogTypeId);
				}
				//sqlBuilder.AppendLine("ORDER BY ProductID");

				sqlBuilder.AppendLine("ORDER BY t1.CreateOn DESC ,ProductID ASC");
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						ProductCatalog product = new ProductCatalog();
						product.ProductDetail = new Product();
						product.ProductDetail.Id = (string)reader["ProductID"];
						product.ProductDetail.ThaiName = (string)reader["pTName"];
						product.ProductDetail.EngName = (string)reader["pEName"];
						product.ProductDetail.Name = new LocalizedString((string)reader["pTName"]);
						product.ProductDetail.Name["en"] = (string)reader["pEName"];
						product.ProductDetail.FullPriceIncVat = (decimal)reader["FullPrice"];
						product.ProductDetail.PriceIncVat = (decimal)reader["PriceIncVat"];
						product.ProductDetail.PriceExcVat = (decimal)reader["PriceExcVat"];
						product.ProductDetail.ThaiUnit = (string)reader["pTUnit"];
						product.ProductDetail.EngUnit = (string)reader["pEUnit"];
						product.ProductDetail.Unit = new LocalizedString((string)reader["pTUnit"]);
						product.ProductDetail.Unit["en"] = (string)reader["pEUnit"];
						product.ProductDetail.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"]);
						product.CatalogTypeId = (string)reader["CatalogTypeId"];
						product.Company = new Company();
						product.Company.CompanyId = (string)reader["CompanyId"];
						product.ProductDetail.IsBestDeal = Mapper.ToClass<bool>((string)reader["IsBestDeal"]);
						product.ProductDetail.IsVat = Mapper.ToClass<bool>((string)reader["IsVat"]);
						product.ProductDetail.ValidFrom = (DateTime)reader["ValidFrom"];
						product.ProductDetail.ValidTo = (DateTime)reader["ValidTo"];
						product.CreateOn = (DateTime)reader["CreateOn"];
						product.CreateBy = (string)reader["CreateBy"];
						products.Add(product);
					}
				}
			}
			return products;
		}
		public IEnumerable<Product> GetProductCatalogForCompany(string companyId)
		{
			List<Product> products = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select t1.*,t2.pTName,t2.pEName,t2.PriceExcVat,t2.PriceIncVat,ValidFrom,ValidTo,FullPrice,pTUnit,pEUnit,IsPromotion  
										from TBProductCatalog t1 with (nolock) INNER JOIN TBProductCenter t2 with (nolock) ON t1.ProductID = t2.pID 
										where CompanyID=@CompanyID AND CatalogType=@CatalogType";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@CatalogType", ProductCatalog.ProductCatalogType.Company.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Product product = new Product();
						product.Id = (string)reader["ProductID"];
						product.ThaiName = (string)reader["pTName"];
						product.EngName = (string)reader["pEName"];
						product.Name = new LocalizedString((string)reader["pTName"]);
						product.Name["en"] = (string)reader["pEName"];
						product.FullPriceIncVat = (decimal)reader["FullPrice"];
						product.PriceIncVat = (decimal)reader["PriceIncVat"];
						product.PriceExcVat = (decimal)reader["PriceExcVat"];
						product.CodeId = (string)reader["CodeId"];
						//product.ThaiUnit = (string)reader["pTUnit"];
						//product.EngUnit = (string)reader["pEUnit"];
						product.Unit = new LocalizedString((string)reader["pTUnit"]);
						product.Unit["en"] = (string)reader["pEUnit"];
						product.IsPromotion = Mapper.ToClass<bool>((string)reader["IsPromotion"]);
						product.ValidFrom = (DateTime)reader["ValidFrom"];
						product.ValidTo = (DateTime)reader["ValidTo"];
						product.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						products.Add(product);
					}
				}
			}
			return products;
		}
		public void ImportDataProductCatalog(List<ExportProduct> exportProduct, string companyId, string userId)
		{

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string joined = string.Join(",", exportProduct.Select(o => o.Pid).ToArray());
				string replacedPid = "'" + joined.Replace(",", "','") + "'";
				string queryUpdate = "";


				command.CommandText = @"	delete TBProductCatalog WHERE CompanyID = @CompanyID and productid in ( " + replacedPid + ")";

				command.CommandText += @"Insert INTO TBProductCatalog 
											select  @CompanyID,'Company',@CompanyID,pID,PriceExcVat,PriceIncVat,IsVat,
													'Float',IsBestDeal,getdate(),DATEADD(year,50,getdate()),'','',
													getdate(),@userId,getdate(),@userId,isnull(t2.Minprice,0),isnull(t2.Maxprice,0),t1.FullPrice,
													pID
											FROM  TBProductCenter t1 LEFT JOIN TBPriceFilter t2 ON t1.pID = t2.RefID
											where pID   in ( " + replacedPid + ")";

				foreach (var item in exportProduct)
				{
					queryUpdate += "  Update TBProductCatalog set IsBestDeal = '" + item.StatusBaseDeal + "' Where CompanyID = @CompanyID and productid = '" + item.Pid + "' ";
				}

				command.CommandText += queryUpdate;
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@userId", userId);

				command.ExecuteNonQuery();
			}
		}

	}
}