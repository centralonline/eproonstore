﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class ProductRepository : UserFilteredRepository
	{
		public ProductRepository(SqlConnection connection, User user) : base(connection, user) { }

		public void AddNotifyMe(string userId, string pid, string remark, int qty, string mobileNo, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"declare	@GroupNo as int
										select	@GroupNo=ISNULL(Max(GroupNo)+1,1) from tbNotifyme where NotifyMailSuccess='Yes' and Pid=@pid
										insert into tbNotifyme
										(UserID,PID,remark,createon,createby,updateon,updateby,GroupNo,Qty,MobileNo,companyId)
										values
										(@userid,@pid,@remark,getdate(),@userid,getdate(),@userid,@GroupNo,@Qty,@MobileNo,@companyId)";
				command.Parameters.AddWithValue("@userid", userId);
				command.Parameters.AddWithValue("@pid", pid);
				command.Parameters.AddWithValue("@remark", remark);
				command.Parameters.AddWithValue("@Qty", qty);
				command.Parameters.AddWithValue("@MobileNo", mobileNo ?? string.Empty);
				command.Parameters.AddWithValue("@companyId", companyId);

				command.ExecuteNonQuery();
			}
		}

		public IEnumerable<NotifyMeData> GetProductNotifyMeMail()
		{
			List<NotifyMeData> notifyList = new List<NotifyMeData>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	nm.notifyid as 'id',nm.pid as productId,pc.ptname,nm.userid as 'notifymeemail',nm.remark as 'remark',
												isnull(nm.MobileNo,'') as 'MobileNo'
										from	tbnotifyme as nm with (nolock)
												inner join tbproductcenter as pc with (nolock) on nm.pid = pc.pid 
												and pc.status in ('Stock','Limited','NonStock') and nm.NotifyMailsuccess = 'No'";
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						NotifyMeData notify = new NotifyMeData();
						notify.NotifyID = (int)reader["id"];
						notify.ProductID = (string)reader["productId"];
						notify.ProductName = (string)reader["ptname"];
						notify.NotifyMeEmail = (string)reader["notifymeemail"];
						notify.NotifyRemark = (string)reader["remark"];
						notify.MobileNo = (string)reader["MobileNo"];
						notifyList.Add(notify);
					}
				}
			}
			return notifyList;
		}

		public void UpdateStatusNotifyMeMail(int notifyID)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"update tbnotifyme set NotifyMailsuccess = 'Yes' ,updateby = 'System', updateon = getdate(), RefillDate = getdate()  where notifyid = @notifyID";
				command.Parameters.AddWithValue("@notifyID", notifyID);
				command.ExecuteNonQuery();
			}
		}

		public IDictionary<string, IEnumerable<Product>> GetProductSKUs(IEnumerable<Product> products)
		{
			Dictionary<string, List<Product>> result = new Dictionary<string, List<Product>>();

			string SqlCommandText = @"	select	pID,tpcl.CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,pESDesc,pELDesc,
												tpcl.PriceType,tpcl.PriceIncVat,tpcl.PriceExcVat,FullPrice,DeliverFee,
												IsPromotion,IsRecommend,IsNew,tpcl.IsVat,tpcl.IsBestDeal,IsInstock,IsPremium,
												PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
												pcatId,psubcatId,supplierId,supplierName,PriceFlag,ValidFrom,ValidTo
										from	TBProductCenter tpc with (nolock) inner join TBProductCatalog tpcl with (nolock) on tpc.pid = tpcl.ProductID
												and ((tpcl.CompanyID=@companyId and tpcl.CatalogtypeID=@companyId) or (tpcl.CompanyID=@companyId and tpcl.CatalogtypeID=@userId ))
										where	Status not in ('Delete','Hold') and tpc.CodeId in ({0})
										";
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string[] inParameters = new string[products.Count()];
				int i = 0;
				foreach (var productItem in products)
				{
					inParameters[i] = "@" + i;
					command.Parameters.AddWithValue(inParameters[i], productItem.Id);
					result.Add(productItem.Id, new List<Product>());
					i++;
				}
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				if (inParameters.Length > 0) { command.CommandText = String.Format(SqlCommandText, String.Join(",", inParameters)); }
				else { command.CommandText = String.Format(SqlCommandText, "''"); }
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						var product = MapProduct(reader);
						result[product.CodeId].Add(product);
					}
					return result.ToDictionary(kp => kp.Key, kp => (IEnumerable<Product>)kp.Value);
				}
			}
		}

		public IDictionary<string, IEnumerable<Product>> GetAllProductSKUs(IEnumerable<Product> products)
		{
			Dictionary<string, List<Product>> result = new Dictionary<string, List<Product>>();

			string SqlCommandText = @"	select	pID,CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,pESDesc,pELDesc,
												PriceIncVat,PriceExcVat,FullPrice,DeliverFee,
												IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
												PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
												pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag
										from	TBProductCenter with (nolock)
										where	Status not in ('Delete','Hold') and IsSpcCatEpro='No' and CodeId in ({0})
												and not exists (select ProductId from TBProductCatalog Where TBProductCenter.pID=TBProductCatalog.ProductID
												and ((CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId)))
										";
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string[] inParameters = new string[products.Count()];
				int i = 0;
				foreach (var productItem in products)
				{
					inParameters[i] = "@" + i;
					command.Parameters.AddWithValue(inParameters[i], productItem.Id);
					result.Add(productItem.Id, new List<Product>());
					i++;
				}
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				if (inParameters.Length > 0) { command.CommandText = String.Format(SqlCommandText, String.Join(",", inParameters)); }
				else { command.CommandText = String.Format(SqlCommandText, "''"); }
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						var product = MapProduct(reader);
						result[product.CodeId].Add(product);
					}
					return result.ToDictionary(kp => kp.Key, kp => (IEnumerable<Product>)kp.Value);
				}
			}
		}

		public IEnumerable<Product> GetDeptProductFilter(string refId, int currentIndex, int pageSize, string orderBy)
		{
			string SqlCommandTxt = @"Select Top (@PageSize) * From
									(
										Select  ROW_NUMBER() OVER ({0}) AS 'row_number',
												refId,CodeId as pId,CodeId,DeptId,CodeTname as pTName,CodeEname as pEName,
												CodeTSDesc as pTSDesc,CodeESDesc as pESDesc,Status,MinPrice,MaxPrice,SortbyDeptId,
												IsPromotion,IsRecommend,BrandName,UploadOn,'' as pTUnit,'' as pEunit,DefaultSKU
										From	TBDeptProductFilter with (nolock)
										Where	refId=@RefId and Status not in ('Delete','Hold')
												AND CodeId in (select DISTINCT CodeId from TBProductCatalog Where (CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId))
									) P
									Where row_number >= @CurrentItem";

			List<Product> products = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = String.Format(SqlCommandTxt, orderBy);
				command.Parameters.AddWithValue("@RefId", refId);
				command.Parameters.AddWithValue("@CurrentItem", currentIndex);
				command.Parameters.AddWithValue("@PageSize", pageSize);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@userId", User.UserId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						products.Add(MapProduct(reader));
					}
				}
			}
			return products;
		}

		public IEnumerable<Product> GetAllDeptProductFilter(string refId, int currentIndex, int pageSize, string orderBy)
		{
			List<Product> products = new List<Product>();
			IEnumerable<string> permisstion = GetPermisstionProduct(refId);
			if (permisstion.Any())
			{
				string strCommand = @"Select Top (@PageSize) * From
									(
										Select  ROW_NUMBER() OVER ({0}) AS 'row_number',
												refId,CodeId as pId,CodeId,DeptId,CodeTname as pTName,CodeEname as pEName,
												CodeTSDesc as pTSDesc,CodeESDesc as pESDesc,Status,MinPrice,MaxPrice,SortbyDeptId,
												IsPromotion,IsRecommend,BrandName,UploadOn,'' as pTUnit,'' as pEunit,DefaultSKU
										From	TBDeptProductFilter with (nolock)
										Where	refId=@RefId and Status not in ('Delete','Hold')
									) P
									Where row_number >= @CurrentItem";

				using (SqlCommand command = CreateCommandEnsureConnectionOpen())
				{
					command.CommandText = String.Format(strCommand, orderBy);
					command.Parameters.AddWithValue("@RefId", refId);
					command.Parameters.AddWithValue("@CurrentItem", currentIndex);
					command.Parameters.AddWithValue("@PageSize", pageSize);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							if (permisstion.Any(id => id == (string)reader["pId"]))
							{
								products.Add(MapProduct(reader));
							}
						}
					}
				}
			}
			return products;
		}

		public IDictionary<string, IEnumerable<Product>> GetProductByDepartment(IEnumerable<Department> departments)
		{
			Dictionary<string, List<Product>> result = new Dictionary<string, List<Product>>();
			if (!departments.Any()) { return result.ToDictionary(kp => kp.Key, kp => (IEnumerable<Product>)kp.Value); }

			string SqlCommandText = "";

			if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
			{
				SqlCommandText = @"	Select	refId,tdpf.CodeId as pId,tdpf.CodeId,DeptId,CodeTname as pTName,CodeEname as pEName,
											CodeTSDesc as pTSDesc,Status,tdpf.MinPrice,tdpf.MaxPrice,'' as pTUnit,'' as pEunit,
											IsPromotion,IsRecommend,BrandID,BrandName,UploadOn,DefaultSku
									From	TBDeptProductFilter tdpf with (nolock)
											inner join TBProductCatalog tpc with (nolock) on (tdpf.CodeId = tpc.CodeId and ((tpc.CompanyID = @companyId and tpc.CatalogtypeID = @companyId) or (tpc.CompanyID = @companyId and tpc.CatalogtypeID=@userId )))
									Where	refId in ({0}) and Status not in ('Delete','Hold')";
			}
			else
			{
				SqlCommandText = @"	Select	refId,CodeId as pId,CodeId,DeptId,CodeTname as pTName,CodeEname as pEName,
											CodeTSDesc as pTSDesc,Status,MinPrice,MaxPrice,'' as pTUnit,'' as pEunit,
											IsPromotion,IsRecommend,BrandID,BrandName,UploadOn,DefaultSku
									From	TBDeptProductFilter with (nolock)
									Where	refId in ({0}) and Status not in ('Delete','Hold') ";
			}

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string[] inParameters = new string[departments.Count()];
				int i = 0;
				foreach (var deptItem in departments)
				{
					inParameters[i] = "@" + i;
					command.Parameters.AddWithValue(inParameters[i], deptItem.RefId);
					result.Add(deptItem.RefId, new List<Product>());
					i++;
				}
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.CommandText = string.Format(SqlCommandText, String.Join(",", inParameters));
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						var product = MapProduct(reader);
						result[product.RefId].Add(product);
					}
					return result.ToDictionary(kp => kp.Key, kp => (IEnumerable<Product>)kp.Value);
				}
			}
		}

		public IEnumerable<ProductJsonData> GetProductSearch(string searchkeyword)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				ProductJsonData product = null;
				List<ProductJsonData> item = new List<ProductJsonData>();
				if (!User.Company.UseOfmCatalog)
				{
					command.CommandText = @"Select	DISTINCT pID,pTname,PEname,TPCL.PriceType,pTunit,pEunit,'Catalog' AS CatType
											FROM	TBUserCatalogDetail Detail with (nolock)
													INNER JOIN TBProductCatalog TPCL with (nolock) on (Detail.ProductID=TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
													INNER JOIN TBProductCenter TPC with (nolock) ON TPCL.ProductID = TPC.pID
											Where	Status not in ('Delete','Hold') and (pTName LIKE @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)
											UNION ALL
											Select	pID,pTname,PEname,t2.PriceType,pTunit,pEunit,'' AS CatType
											From	TBProductCenter t1 with (nolock)
													inner join tbProductCatalog t2 with (nolock) on (t1.pID = t2.ProductID and ((t2.CompanyID = @companyId and t2.CatalogtypeID = @companyId) or (t2.CompanyID = @companyId and t2.CatalogtypeID = @userId )))
											Where	NOT EXISTS (SELECT cat.ProductID FROM TBUserCatalogDetail cat INNER JOIN TBProductCatalog TPCL
																	ON cat.ProductID = TPCL.ProductID AND ((TPCL.CompanyID = @companyId AND TPCL.CatalogtypeID = @companyId)
																	OR (TPCL.CompanyID = @companyId AND TPCL.CatalogtypeID = @userId)) AND cat.ProductID = t2.ProductID)
													and Status not in ('Delete','Hold') and (pTName LIKE @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)
													ORDER BY CatType DESC";
				}
				else
				{
					command.CommandText = @"Select	DISTINCT TPC.pID,pTname,PEname,pTunit,pEunit,'Catalog' AS CatType
											FROM	TBUserCatalogHead Head with (nolock)
													INNER JOIN TBUserCatalogDetail Detail with (nolock) ON Head.CatalogID=Detail.CatalogID
													INNER JOIN TBProductCenter TPC with (nolock) ON Detail.ProductID=TPC.pID
											Where	Head.CompanyID=@companyId AND Head.UserID=@userId AND TPC.Status not in ('Delete','Hold') and (pTName LIKE @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)
											UNION ALL
											Select	pID,PTname,PEname,pTunit,pEunit,'' AS CatType
											From	TBProductCenter TPC with (nolock)
											Where	NOT EXISTS (SELECT Detail.ProductID FROM TBUserCatalogHead Head
													INNER JOIN TBUserCatalogDetail Detail ON Head.CatalogID=Detail.CatalogID
													Where Head.CompanyID=@companyId AND Head.UserID=@userId AND Detail.ProductID=TPC.pID)
													AND Status not in ('Delete','Hold') and IsSpcCatEpro='No' and (pTName LIKE @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)
											UNION ALL
											Select	pID,pTname,PEname,pTunit,pEunit,'' AS CatType
											From	TBProductCenter t1 with (nolock)
													inner join tbProductCatalog t2 with (nolock) on (t1.pID = t2.ProductID and ((t2.CompanyID = @companyId and t2.CatalogtypeID = @companyId) or (t2.CompanyID = @companyId and t2.CatalogtypeID = @userId )))
											Where	NOT EXISTS (SELECT Detail.ProductID FROM TBUserCatalogHead Head
													INNER JOIN TBUserCatalogDetail Detail ON Head.CatalogID=Detail.CatalogID
													Where Head.CompanyID=@companyId AND Head.UserID=@userId AND Detail.ProductID=t1.pID)
													and Status not in ('Delete','Hold') and IsSpcCatEpro='Yes' and (pTName LIKE @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)
													ORDER BY CatType DESC";
				}
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@searchkeyword", "%" + searchkeyword + "%");
				try
				{
					using (SqlDataReader reader = command.ExecuteReader())
					{
						while (reader.Read())
						{
							product = new ProductJsonData();
							product.ProductEngName = (string)reader["PEname"];
							product.ProductThaiName = (string)reader["PTname"];
							product.ProductThaiUnit = (string)reader["pTunit"];
							product.ProductEngUnit = (string)reader["pEunit"];
							product.ProductId = (string)reader["pID"];
							item.Add(product);
						}
					}
					return item;
				}
				catch (Exception)
				{
					return item;
				}

			}
		}

		public IEnumerable<Product> GetProductSearchFilter(string searchkeyword)
		{
			List<Product> products = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	pID,pTname,pEname,tpc.CodeId,FullPrice,tpcl.PriceIncVat,tpcl.PriceExcVat,NumOfRating,
													pTUnit,pEunit,pTLDesc,pTSDesc,IsPromotion,PromotiontText,tpcl.IsBestDeal,
													DeliverFee,tpcl.IsVat,IsPremium,IsDelivery,pCatID,pSubCatID,Status,tpcl.PriceType,pESDesc,pELDesc
											From	TBProductCenter tpc with (nolock)
													inner join TBProductCatalog tpcl with (nolock) on (tpc.pID = tpcl.ProductID and ((tpcl.CompanyID = @companyId and tpcl.CatalogtypeID = @companyId) or (tpcl.CompanyID = @companyId and tpcl.CatalogtypeID = @userId )))
											Where	Status not in ('Delete','Hold') and (PTname like @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)";
				}
				else if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	pID,pTname,pEname,tpc.CodeId,FullPrice,tpcl.PriceIncVat,tpcl.PriceExcVat,NumOfRating,
													pTUnit,pEunit,pTLDesc,pTSDesc,IsPromotion,PromotiontText,tpcl.IsBestDeal,
													DeliverFee,tpcl.IsVat,IsPremium,IsDelivery,pCatID,pSubCatID,Status,tpcl.PriceType,pESDesc,pELDesc
											From	TBProductCenter tpc with (nolock)
													inner join TBProductCatalog tpcl with (nolock) on (tpc.pID = tpcl.ProductID and ((tpcl.CompanyID = @companyId and tpcl.CatalogtypeID = @companyId) or (tpcl.CompanyID = @companyId and tpcl.CatalogtypeID = @userId )))
											Where	Status not in ('Delete','Hold') and (PTname like @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)
											UNION ALL
											Select	pID,pTname,pEname,CodeId,FullPrice,PriceIncVat,PriceExcVat,NumOfRating,
													pTUnit,pEunit,pTLDesc,pTSDesc,IsPromotion,PromotiontText,IsBestDeal,
													DeliverFee,IsVat,IsPremium,IsDelivery,pCatID,pSubCatID,Status,'Float' AS PriceType,pESDesc,pELDesc
											From	TBProductCenter with (nolock)
											Where	Status Not In ('Delete', 'Hold') and IsSpcCatEpro='No' and (pTName LIKE @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)
													and not exists (select ProductId from TBProductCatalog Where TBProductCenter.pID=TBProductCatalog.ProductID
													and ((CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId)))";
				}
				else
				{
					command.CommandText = @"Select	pID,pTname,pEname,CodeId,FullPrice,PriceIncVat,PriceExcVat,NumOfRating,
													pTUnit,pEunit,pTLDesc,pTSDesc,IsPromotion,PromotiontText,IsBestDeal,
													DeliverFee,IsVat,IsPremium,IsDelivery,pCatID,pSubCatID,Status,'Float' AS PriceType,pESDesc,pELDesc
											From	TBProductCenter with (nolock)
											Where	Status not in ('Delete','Hold') and IsSpcCatEpro='No' and (pTName LIKE @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)
											UNION ALL
											Select	pID,pTname,pEname,tpc.CodeId,FullPrice,tpcl.PriceIncVat,tpcl.PriceExcVat,NumOfRating,
													pTUnit,pEunit,pTLDesc,pTSDesc,IsPromotion,PromotiontText,tpcl.IsBestDeal,
													DeliverFee,tpcl.IsVat,IsPremium,IsDelivery,pCatID,pSubCatID,Status,tpcl.PriceType,pESDesc,pELDesc
											From	TBProductCenter tpc with (nolock)
													inner join TBProductCatalog tpcl with (nolock) on (tpc.pID = tpcl.ProductID and ((tpcl.CompanyID = @companyId and tpcl.CatalogtypeID = @companyId) or (tpcl.CompanyID = @companyId and tpcl.CatalogtypeID = @userId )))
											Where	Status not in ('Delete','Hold') and IsSpcCatEpro='Yes' and (pTName LIKE @searchkeyword or PEname like @searchkeyword or pID like @searchkeyword)";
				}
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@searchkeyword", "%" + searchkeyword + "%");
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						products.Add(MapProduct(reader));
					}
				}
			}
			return products;
		}

		public IEnumerable<Product> GetProductInOrder(string orderId)
		{
			List<Product> products = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog)
				{
					command.CommandText = @"Select	pID,tpc.CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,isnull(tpcl.PriceType,'Float') as PriceType,
													(Case When tpcl.PriceType IS NULL Then 'Yes' Else 'No' End) AS IsOfmCatalog,
													isnull(tpcl.PriceIncVat,tpc.PriceIncVat) as PriceIncVat,isnull(tpcl.PriceExcVat,tpc.PriceExcVat) as PriceExcVat,FullPrice,
													IsPromotion,IsRecommend,IsNew,isnull(tpcl.IsVat,tpc.IsVat) as IsVat,isnull(tpcl.IsBestDeal,tpc.IsBestDeal) as IsBestDeal,
													DeliverFee,IsInstock,IsPremium,PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pcatId,psubcatId,supplierId,supplierName,PriceFlag,isnull(ValidFrom,'') as ValidFrom,isnull(ValidTo,'') as ValidTo,pESDesc,pELDesc
											From	TBProductCenter tpc with (nolock)
													left join tbProductCatalog tpcl with (nolock) on (tpc.pID = tpcl.ProductID and ((tpcl.CompanyID = @companyId and tpcl.CatalogtypeID = @companyId) or (tpcl.CompanyID = @companyId and tpcl.CatalogtypeID= @userId )))
											Where	Status not in ('Delete','Hold') and PID in(select PID from TBOrderDetail Where OrderId = @OrderId)";
				}
				else
				{
					command.CommandText = @"Select	pID,CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,
													'Float' as PriceType,PriceIncVat,PriceExcVat,FullPrice,DeliverFee,
													IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
													PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pcatId,psubcatId,supplierId,supplierName,PriceFlag,'' as ValidFrom,'' as ValidTo,pESDesc,pELDesc
											From	TBProductCenter with (nolock)
											Where	Status not in ('Delete','Hold') and IsSpcCatEpro='No' and PID in (select PID from TBOrderDetail Where OrderId = @OrderId)
											UNION ALL
											Select	pID,tpc.CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,
													tpcl.PriceType,tpcl.PriceIncVat,tpcl.PriceExcVat,FullPrice,DeliverFee,
													IsPromotion,IsRecommend,IsNew,tpcl.IsVat,tpcl.IsBestDeal,IsInstock,IsPremium,
													PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pcatId,psubcatId,supplierId,supplierName,PriceFlag,ValidFrom,ValidTo,pESDesc,pELDesc
											From	TBProductCenter tpc with (nolock)
													inner join tbProductCatalog tpcl with (nolock) on (tpc.pID = tpcl.ProductID and ((tpcl.CompanyID=@companyId and tpcl.CatalogtypeID=@companyId) or (tpcl.CompanyID=@companyId and tpcl.CatalogtypeID=@userId)))
											Where	Status not in ('Delete','Hold') and IsSpcCatEpro='Yes' and PID in(select PID from TBOrderDetail Where OrderId = @OrderId)";
				}
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@orderId", orderId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						products.Add(MapProduct(reader));
					}
				}
			}
			return products;
		}

		public IEnumerable<Product> GetProductSKUs(string codeId)
		{
			List<Product> products = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	pID,TPC.CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,pESDesc,pELDesc,
													TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,FullPrice,DeliverFee,
													IsPromotion,IsRecommend,IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
													PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pcatId,psubcatId,supplierId,supplierName,PriceFlag,ValidFrom,ValidTo
											From	TBProductCenter TPC with (nolock) inner join tbProductCatalog TPCL with (nolock)
													on (TPC.pID=TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
											Where	Status not in ('Delete','Hold') and TPC.CodeId=@CodeID and TPC.pID <> TPC.CodeID";
				}
				else if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	pID,TPC.CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,pESDesc,pELDesc,
													TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,FullPrice,DeliverFee,
													IsPromotion,IsRecommend,IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
													PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pcatId,psubcatId,supplierId,supplierName,PriceFlag,ValidFrom,ValidTo
											From	TBProductCenter TPC with (nolock) inner join tbProductCatalog TPCL with (nolock)
													on (TPC.pID=TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
											Where	Status not in ('Delete','Hold') and TPC.CodeId=@CodeID and TPC.pID <> TPC.CodeID
											UNION ALL
											Select	pID,CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,pESDesc,pELDesc,
													'Float' as PriceType,PriceIncVat,PriceExcVat,FullPrice,DeliverFee,
													IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
													PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pcatId,psubcatId,supplierId,supplierName,PriceFlag,'' as ValidFrom,'' as ValidTo
											From	TBProductCenter with (nolock)
											Where	Status Not In ('Delete', 'Hold') and IsSpcCatEpro='No' and CodeId=@CodeID and pID <> CodeID
													and not exists (select ProductId from TBProductCatalog Where TBProductCenter.pID=TBProductCatalog.ProductID
													and ((CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId)))";
				}
				else
				{
					command.CommandText = @"Select	pID,CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,pESDesc,pELDesc,
													'Float' as PriceType,PriceIncVat,PriceExcVat,FullPrice,DeliverFee,
													IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
													PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pcatId,psubcatId,supplierId,supplierName,PriceFlag,'' as ValidFrom,'' as ValidTo
											From	TBProductCenter with (nolock)
											Where	Status Not In ('Delete', 'Hold') and IsSpcCatEpro='No' and CodeId=@CodeID and pID <> CodeID
											UNION ALL
											Select	pID,TPC.CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pTLDesc,pESDesc,pELDesc,
													TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,FullPrice,DeliverFee,
													IsPromotion,IsRecommend,IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
													PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pcatId,psubcatId,supplierId,supplierName,PriceFlag,ValidFrom,ValidTo
											From	TBProductCenter TPC with (nolock) inner join tbProductCatalog TPCL with (nolock)
													on (TPC.pID=TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
											Where	Status not in ('Delete','Hold') and IsSpcCatEpro='Yes' and TPC.CodeId=@CodeID and TPC.pID <> TPC.CodeID";
				}
				command.Parameters.AddWithValue("@CodeId", codeId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						products.Add(MapProduct(reader));
					}
				}
			}
			return products;
		}

		public Product GetProductDetail(string pid)
		{
			Product product = new Product();
			if (pid.Length != 7)
			{
				return GetProductGroup(pid);
			}
			else
			{
				return GetProductSKU(pid);
			}
		}

		private Product GetProductSKU(string pId)
		{
			Product product = new Product();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	TPC.pID,TPC.CodeId,TPC.DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
													TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,TPC.FullPrice,0.0 as MinPrice,0.0 as MaxPrice,
													TPC.IsPromotion,TPC.IsRecommend,TPC.IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
													DeliverFee,TPC.PageNo,PromotionTtext,ProdType,Title,TPC.Status,TPC.UploadOn,TPC.BrandId,TPC.BrandName,
													pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,ValidFrom,ValidTo
											From	TBProductCenter TPC inner join TBProductCatalog TPCL 
													on (TPC.PID = TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
											Where	TPC.pID = @pId";
				}
				else if (!User.Company.UseOfmCatalog && User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	TPC.pID,TPC.CodeId,TPC.DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
													TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,TPC.FullPrice,0.0 as MinPrice,0.0 as MaxPrice,
													TPC.IsPromotion,TPC.IsRecommend,TPC.IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
													DeliverFee,TPC.PageNo,PromotionTtext,ProdType,Title,TPC.Status,TPC.UploadOn,TPC.BrandId,TPC.BrandName,
													pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,ValidFrom,ValidTo
											From	TBProductCenter TPC inner join TBProductCatalog TPCL 
													on (TPC.PID = TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
											Where	TPC.pID = @pId
											UNION ALL
											Select	pID,CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
													'Float' as PriceType,PriceIncVat,PriceExcVat,FullPrice,0.0 as MinPrice,0.0 as MaxPrice,
													IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
													DeliverFee,PageNo,PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,'' as ValidFrom,'' as ValidTo
											From	TBProductCenter
											Where	pID = @pId AND IsSpcCatEpro='No'
													and not exists (select ProductId from TBProductCatalog Where TBProductCenter.pID=TBProductCatalog.ProductID
													and ((CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId)))";
				}
				else
				{
					command.CommandText = @"Select	pID,CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
													'Float' as PriceType,PriceIncVat,PriceExcVat,FullPrice,0.0 as MinPrice,0.0 as MaxPrice,
													IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
													DeliverFee,PageNo,PromotionTtext,ProdType,Title,Status,UploadOn,BrandId,BrandName,
													pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,'' as ValidFrom,'' as ValidTo
											From	TBProductCenter
											Where	pID = @pId AND IsSpcCatEpro='No'
											UNION ALL
											Select	TPC.pID,TPC.CodeId,TPC.DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
													TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,TPC.FullPrice,0.0 as MinPrice,0.0 as MaxPrice,
													TPC.IsPromotion,TPC.IsRecommend,TPC.IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
													DeliverFee,TPC.PageNo,PromotionTtext,ProdType,Title,TPC.Status,TPC.UploadOn,TPC.BrandId,TPC.BrandName,
													pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,ValidFrom,ValidTo
											From	TBProductCenter TPC inner join TBProductCatalog TPCL 
													on (TPC.PID = TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
											Where	TPC.pID = @pId AND IsSpcCatEpro='Yes'";
				}
				command.Parameters.AddWithValue("@pId", pId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return MapProduct(reader);
					}
					return null;
				}
			}
		}

		private Product GetProductGroup(string codeId)
		{
			Product product = new Product();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	tdp.CodeId as pID,tdp.CodeId,tdp.DeptID,CodeTName as pTname,CodeEName as pEname,0.0 as NumOfRating,'' as pTUnit,'' as pEUnit,
													MinPrice,MaxPrice,tpc.PriceType,ValidFrom,ValidTo,0.0 as FullPrice,0.0 as PriceIncVat,0.0 as PriceExcVat,0.0 as DeliverFee,
													'Yes' as IsPG,'' as BrandId,PromotionTtext,'No' as IsBestDeal,CodeTLDesc as pTLDesc,CodeTSDesc as pTSDesc,'No' as IsPromotion,
													(select isnull(max(prodtype),'Trendy') as prodtype from TBProductCenter where codeid=@CodeID) as Prodtype,
													'No' as IsVat,'No' as IsInstock, 'No' as IsDelivery, Status,DefaultSKU
											From	TBDeptProduct tdp inner join TBProductCatalog tpc
													on (tdp.CodeId = tpc.CodeId and ((tpc.CompanyID = @companyId and tpc.CatalogtypeID = @companyId) or (tpc.CompanyID = @companyId and tpc.CatalogtypeID= @userId )))
											Where	tdp.CodeID = @CodeID and IsDefault='Yes'";
				}
				else
				{
					command.CommandText = @"Select	tdp.CodeId as pID,tdp.CodeId,tdp.DeptID,CodeTName as pTname,CodeEName as pEname,0.0 as NumOfRating,'' as pTUnit,'' as pEUnit,
													MinPrice,MaxPrice,isnull(tpc.PriceType,'Float') as PriceType,isnull(ValidFrom,'') as ValidFrom,isnull(ValidTo,'') as ValidTo,
													0.0 as FullPrice,0.0 as PriceIncVat,0.0 as PriceExcVat,0.0 as DeliverFee,'No' as IsVat,'No' as IsInstock, 'No' as IsDelivery,
													'Yes' as IsPG,'' as BrandId,PromotionTtext,'No' as IsBestDeal,CodeTLDesc as pTLDesc,CodeTSDesc as pTSDesc,'No' as IsPromotion,
													(select isnull(max(prodtype),'Trendy') as prodtype from TBProductCenter where codeid=@CodeID) as Prodtype,Status,DefaultSKU	
											From	TBDeptProduct tdp left join TBProductCatalog tpc
													on (tdp.CodeId = tpc.CodeId and ((tpc.CompanyID = @companyId and tpc.CatalogtypeID = @companyId) or (tpc.CompanyID = @companyId and tpc.CatalogtypeID= @userId )))
											Where	tdp.CodeID = @CodeID and IsDefault='Yes'";
				}
				command.Parameters.AddWithValue("@CodeID", codeId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return MapProduct(reader);
					}
					return null;
				}
			}
		}

		public int GetItemCountDeptProductsFilter(string refId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	count(CodeId) as 'ItemCount'
											From	TBDeptProductFilter
											Where	refId=@RefId and Status not in ('Delete','Hold')
													AND CodeId in (select DISTINCT CodeId from tbProductCatalog Where (CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId)) ";
				}
				else
				{
					command.CommandText = @"Select	count(CodeId) as 'ItemCount'
											From	TBDeptProductFilter
											Where	refId=@RefId and Status not in ('Delete','Hold') ";
				}
				command.Parameters.AddWithValue("@RefId", refId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				return (int)command.ExecuteScalar();
			}
		}

		public IEnumerable<Product> GetBrandFilter(string refBrandId, string refDeptId, int currentIndex, int pageSize, string orderBy, out int itemCount)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				itemCount = Int32.MinValue;
				string brandid = String.Empty;
				command.CommandText = @"select BrandId from TBBrandFilter with (nolock) where RefID = @RefBrandId";
				command.Parameters.AddWithValue("@RefBrandId", refBrandId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						brandid = (string)reader["BrandId"];
					}
				}

				string sqlcommand = "";

				if (!User.Company.UseOfmCatalog)
				{
					sqlcommand = @"	Select Top (@PageSize) *
									From(
										Select  ROW_NUMBER() OVER ({0}) AS 'row_number',
												PID,CodeId,pTname,pEName,PriceIncVat,PriceExcVat,pTSDesc,pESDesc,pTUnit,pEUnit,pTLDesc,pELDesc,
												UploadOn,deptid,Status,IsNew,IsPromotion,PageNo,SortByProduct,BrandId,
												IsDelivery,IsVat,IsBestDeal,PromotionTtext,ISRecommend,FullPrice,ValidFrom,ValidTo,PriceType
										From (
											Select	PC.PID,PC.CodeId,pTname,pEName,PCa.PriceIncVat,PCa.PriceExcVat,pTSDesc,pESDesc,pTUnit,pEUnit,pTLDesc,pELDesc,
													PC.UploadOn,PC.deptid,PC.Status,PC.IsNew,PC.IsPromotion,PC.PageNo,SortByProduct,PC.BrandId,
													PC.IsDelivery,PCa.IsVat,PCa.IsBestDeal,PC.PromotionTtext,PC.ISRecommend,PC.FullPrice,PCa.ValidFrom,PCa.ValidTo,PCa.PriceType
											from	TBProductCenter PC with (nolock)
													inner join TBProductCatalog PCa with (nolock) on (PC.PID = PCa.ProductID and ((PCa.CompanyID=@companyId and PCa.CatalogtypeID=@companyId) or (PCa.CompanyID=@companyId and PCa.CatalogtypeID=@userId )))
													inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId
											where	pc.status not in('delete','hold') and  DPF.refid = @RefDeptId and PC.BrandId= @BrandId
										) TB2
									)TB1
									Where row_number >= @CurrentItem

									Select	count(*) as 'ItemCount'
									from	TBProductCenter PC with (nolock)
											inner join TBProductCatalog PCa with (nolock) on (PC.PID = PCa.ProductID and ((PCa.CompanyID=@companyId and PCa.CatalogtypeID=@companyId) or (PCa.CompanyID=@companyId and PCa.CatalogtypeID=@userId )))
											inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId
									where	pc.status not in('delete','hold') and  DPF.refid = @RefDeptId and PC.BrandId=@BrandId";
				}
				else
				{
					sqlcommand = @"	Select Top (@PageSize) *
									From(
										Select  ROW_NUMBER() OVER ({0}) AS 'row_number',
												PID,CodeId,pTname,pEName,PriceIncVat,PriceExcVat,pTSDesc,pESDesc,pTUnit,pEUnit,pTLDesc,pELDesc,
												UploadOn,deptid,Status,IsNew,IsPromotion,PageNo,SortByProduct,BrandId,
												IsDelivery,IsVat,IsBestDeal,PromotionTtext,ISRecommend,FullPrice,ValidFrom,ValidTo,PriceType
										From (
											Select	PC.PID,PC.CodeId,pTname,pEName,PriceIncVat,PriceExcVat,pTSDesc,pESDesc,pTUnit,pEUnit,pTLDesc,pELDesc,
													PC.UploadOn,PC.deptid,PC.Status,PC.IsNew,PC.IsPromotion,PC.PageNo,SortByProduct,PC.BrandId,
													PC.IsDelivery,PC.IsVat,PC.IsBestDeal,PC.PromotionTtext,PC.ISRecommend,PC.FullPrice,'' as ValidFrom,'' as ValidTo,'Float' AS PriceType
											from	TBProductCenter PC with (nolock)
													Inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId
											where	pc.status not in('delete','hold') and PC.IsSpcCatEpro='No' and DPF.refid = @RefDeptId and PC.BrandId= @BrandId
											UNION ALL
											Select	PC.PID,PC.CodeId,pTname,pEName,PCa.PriceIncVat,PCa.PriceExcVat,pTSDesc,pESDesc,pTUnit,pEUnit,pTLDesc,pELDesc,
													PC.UploadOn,PC.deptid,PC.Status,PC.IsNew,PC.IsPromotion,PC.PageNo,SortByProduct,PC.BrandId,
													PC.IsDelivery,PCa.IsVat,PCa.IsBestDeal,PC.PromotionTtext,PC.ISRecommend,PC.FullPrice,PCa.ValidFrom,PCa.ValidTo,PCa.PriceType
											from	TBProductCenter PC with (nolock)
													inner join TBProductCatalog PCa with (nolock) on (PC.PID = PCa.ProductID and ((PCa.CompanyID=@companyId and PCa.CatalogtypeID=@companyId) or (PCa.CompanyID=@companyId and PCa.CatalogtypeID=@userId )))
													inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId
											where	pc.status not in('delete','hold') and PC.IsSpcCatEpro='Yes' and  DPF.refid = @RefDeptId and PC.BrandId= @BrandId
										) TB2
									)TB1
									Where row_number >= @CurrentItem

									Select sum(ItemCount) as ItemCount
									from (
										Select	count(*) as 'ItemCount'
										from	TBProductCenter PC with (nolock)
												Inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId
										where	pc.status not in('delete','hold') and PC.IsSpcCatEpro='No' and DPF.refid = @RefDeptId and PC.BrandId= @BrandId
										UNION All
										Select	count(*) as 'ItemCount'
										from	TBProductCenter PC with (nolock)
												inner join TBProductCatalog PCa with (nolock) on (PC.PID = PCa.ProductID and ((PCa.CompanyID=@companyId and PCa.CatalogtypeID=@companyId) or (PCa.CompanyID=@companyId and PCa.CatalogtypeID=@userId )))
												inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId
										where	pc.status not in('delete','hold') and PC.IsSpcCatEpro='Yes' and DPF.refid = @RefDeptId and PC.BrandId=@BrandId
									)tbCount";
				}
				command.CommandText = String.Format(sqlcommand, orderBy);
				List<Product> products = new List<Product>();
				command.Parameters.AddWithValue("@RefDeptId", refDeptId);
				command.Parameters.AddWithValue("@BrandId", brandid);
				command.Parameters.AddWithValue("@CurrentItem", currentIndex);
				command.Parameters.AddWithValue("@PageSize", pageSize);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						products.Add(MapProduct(reader));
					}
					reader.NextResult();
					if (reader.Read())
					{
						itemCount = (int)reader["ItemCount"];
					}
					return products;
				}
			}
		}

		public IEnumerable<Product> GetProductByProperty(string refId, int currentIndex, int pageSize, string orderBy, string refDeptId, out int itemCount)
		{
			itemCount = int.MinValue;

			string SqlCommandTxt = "";

			if (!User.Company.UseOfmCatalog)
			{
				SqlCommandTxt = @"	Select Top (@PageSize) * 
									From(
										Select  ROW_NUMBER() OVER ({0}) AS 'row_number',
												PID,CodeId,pTname,pEName,PriceExcVat,PriceIncVat,pTSDesc,pESDesc,pTUnit,pEUnit,pTLDesc,pELDesc,
												PriceType,FullPrice,DeliverFee,PageNo,BrandId,PromotionTtext,
												IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
												UploadOn,PropName,PropValue,deptid,Status,SortByProduct
										From (
											Select	TPC.pID,TPC.CodeId,TPC.DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
													TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,TPC.FullPrice,DeliverFee,TPC.PageNo,
													TPC.IsPromotion,TPC.IsRecommend,TPC.IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
													PromotionTtext,ProdType,Title,TPC.Status,TPC.UploadOn,TPC.BrandId,TPC.BrandName,
													pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,ValidFrom,ValidTo,
													TPP.propname,TPP.PropValue,DPF.SortByProduct
											From	TBProductCenter TPC with (nolock)
													inner join TBProductCatalog TPCL with (nolock) on (TPC.PID = TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
													Inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=TPC.CodeId and TPCL.CodeId=DPF.CodeID
													Inner Join TBPropProduct TPP with (nolock) On TPP.pId = TPC.pId
											Where	TPC.status not in('delete','hold') and DPF.refid = @RefDeptId and TPP.refid=@RefId
										) TB1
									) TB2
									Where row_number >= @CurrentItem

									Select	count(*) as 'ItemCount'
									From	TBProductCenter PC with (nolock)
											inner join TBProductCatalog PCa with (nolock) on (PC.PID = PCa.ProductID and ((PCa.CompanyID=@companyId and PCa.CatalogtypeID=@companyId) or (PCa.CompanyID=@companyId and PCa.CatalogtypeID=@userId )))
											Inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId and PCa.CodeId=DPF.CodeID
											Inner Join TBPropProduct  PP with (nolock) On PP.pId = PC.pId
									Where	DPF.refid = @RefDeptId and PP.refid=@RefId";
			}
			else
			{
				SqlCommandTxt = @"	Select Top (@PageSize) * 
									From(
										Select  ROW_NUMBER() OVER ({0}) AS 'row_number',
											PID,CodeId,pTname,pEName,PriceExcVat,PriceIncVat,pTSDesc,pESDesc,pTUnit,pEUnit,pTLDesc,pELDesc,
											PriceType,FullPrice,DeliverFee,PageNo,BrandId,PromotionTtext,
											IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
											UploadOn,PropName,PropValue,deptid,Status,SortByProduct
									From (
										Select	TPC.pID,TPC.CodeId,TPC.DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
												'Float' as PriceType,PriceIncVat,PriceExcVat,TPC.FullPrice,DeliverFee,TPC.PageNo,
												TPC.IsPromotion,TPC.IsRecommend,TPC.IsNew,TPC.IsVat,TPC.IsBestDeal,IsInstock,IsPremium,
												PromotionTtext,ProdType,Title,TPC.Status,TPC.UploadOn,TPC.BrandId,TPC.BrandName,
												pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,'' as ValidFrom,'' as ValidTo,
												TPP.propname,TPP.PropValue,DPF.SortByProduct
										From	TBProductCenter TPC with (nolock)
												Inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=TPC.CodeId
												Inner Join TBPropProduct TPP with (nolock) On TPP.pId = TPC.pId	
										Where	TPC.status not in('delete','hold') and TPC.IsSpcCatEpro='No' and DPF.refid = @RefDeptId and TPP.refid=@RefId
										UNION ALL
										Select	TPC.pID,TPC.CodeId,TPC.DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
												TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,TPC.FullPrice,DeliverFee,TPC.PageNo,
												TPC.IsPromotion,TPC.IsRecommend,TPC.IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
												PromotionTtext,ProdType,Title,TPC.Status,TPC.UploadOn,TPC.BrandId,TPC.BrandName,
												pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,ValidFrom,ValidTo,
												TPP.propname,TPP.PropValue,DPF.SortByProduct
										From	TBProductCenter TPC with (nolock)
												inner join TBProductCatalog TPCL with (nolock) on (TPC.PID = TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
												Inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=TPC.CodeId and TPCL.CodeId=DPF.CodeID
												Inner Join TBPropProduct TPP with (nolock) On TPP.pId = TPC.pId
										Where	TPC.status not in('delete','hold') and TPC.IsSpcCatEpro='Yes' and DPF.refid = @RefDeptId and TPP.refid=@RefId
									) TB1
								) TB2
								Where row_number >= @CurrentItem

								Select sum(ItemCount) as ItemCount
								from (
									Select	count(*) as 'ItemCount'
									from	TBProductCenter PC with (nolock)
											Inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId
											Inner Join TBPropProduct  PP with (nolock) On PP.pId = PC.pId
									where	PC.status not in('delete','hold') and PC.IsSpcCatEpro='No' and DPF.refid=@RefDeptId and PP.refid=@RefId
									UNION All
									Select	count(*) as 'ItemCount'
									From	TBProductCenter PC with (nolock)
											inner join TBProductCatalog PCa with (nolock) on (PC.PID = PCa.ProductID and ((PCa.CompanyID=@companyId and PCa.CatalogtypeID=@companyId) or (PCa.CompanyID=@companyId and PCa.CatalogtypeID=@userId )))
											Inner join TBDeptProductFilter DPF with (nolock) on DPF.CodeId=PC.CodeId and PCa.CodeId=DPF.CodeID
											Inner Join TBPropProduct  PP with (nolock) On PP.pId = PC.pId
									where	PC.status not in('delete','hold') and PC.IsSpcCatEpro='Yes' and DPF.refid=@RefDeptId and PP.refid=@RefId
								)tbCount";
			}

			List<Product> products = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = String.Format(SqlCommandTxt, orderBy);
				command.Parameters.AddWithValue("@RefId", refId);
				command.Parameters.AddWithValue("@CurrentItem", currentIndex);
				command.Parameters.AddWithValue("@PageSize", pageSize);
				command.Parameters.AddWithValue("@RefDeptId", refDeptId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						products.Add(MapProduct(reader));
					}
					reader.NextResult();
					if (reader.Read())
					{
						itemCount = (int)reader["ItemCount"];
					}
					return products;
				}
			}
		}

		private IEnumerable<string> GetPermisstionProduct(string refId) //ได้ codeid ทั้งหมดที่ไม่อยู่ใน TBProductCatalog
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				List<string> pids = new List<string>();
				command.CommandText = @"select	distinct tdp.CodeId
										from	TBDeptProductFilter tdp with (nolock) INNER JOIN TBProductCenter tc with (nolock) ON tdp.CodeID = tc.CodeId
										where	RefID = @refId
												and not exists (select ProductId from TBProductCatalog Where tc.pID=TBProductCatalog.ProductID 
												and ((CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId)))";
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@refId", refId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						pids.Add((string)reader["CodeId"]);
					}
				}
				return pids;
			}
		}

		public IEnumerable<Product> GetProducts(IEnumerable<string> productItems)
		{
			List<Product> products = new List<Product>();
			if (!productItems.Any()) { return products; }

			foreach (string product in productItems)
			{
				products.Add(GetProductDetail(product));
			}
			return products.Where(p => p != null);
		}

		public int GetItemCountProductPromotion(string RefId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"select	count(*)
											from	TBDeptProductFilter t1 
													inner join tbProductCatalog t2 on (t1.CodeId = t2.CodeId and ((t2.CompanyID = @companyId and t2.CatalogtypeID = @companyId) or (t2.CompanyID = @companyId and t2.CatalogtypeID= @userId )))
											where	RefID=@RefId and IsPromotion=@true and Status not in ('Delete','Hold')";
				}
				else
				{
					command.CommandText = @"select	count(*)
											from	TBDeptProductFilter t1
											where	RefID=@RefId and IsPromotion=@true and Status not in ('Delete','Hold')";
				}
				command.Parameters.AddWithValue("@RefID", RefId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@true", Mapper.ToDatabase(true));

				return (int)command.ExecuteScalar();
			}
		}

		public IEnumerable<Product> GetProductPromotion(string refId)
		{
			List<Product> products = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select top(20) *
											From (
													select	ROW_NUMBER() OVER (partition by DeptId order by DeptId,newid()) as RowNo,
															refId,DPF.CodeId as pId,DPF.CodeId,DeptId,CodeTname as pTName,CodeEname as pEName,
															Status,DPF.MinPrice,DPF.MaxPrice,PC.PriceType,
															SortbyDeptId,IsPromotion,IsRecommend,BrandName,HasBookPreview,UploadOn,DefaultSku,'' as pTUnit,'' as pEunit
													from	TBDeptProductFilter DPF with (nolock)
															inner join tbProductCatalog PC with (nolock) on (DPF.CodeId = PC.CodeId and (( PC.CompanyID = @companyId and PC.CatalogtypeID = @companyId) or (PC.CompanyID = @companyId and PC.CatalogtypeID= @userId )))
													where	RefID=@RefId and IsPromotion='Yes' and Status not in ('Delete','Hold')
												) a
											Where RowNo=1
											Order by NewID()";
				}
				else
				{
					command.CommandText = @"SELECT top(20) *
											From (
													Select	ROW_NUMBER() OVER (partition by DeptId order by DeptId,newid()) as RowNo,
															refId,CodeId as pId,CodeId,DeptId,CodeTname as pTName,CodeEname as pEName,
															Status,MinPrice,MaxPrice,'Float' as PriceType,
															SortbyDeptId,IsPromotion,IsRecommend,BrandName,HasBookPreview,UploadOn,DefaultSku,'' as pTUnit,'' as pEunit
													From	TBDeptProductFilter with (nolock)
													Where	RefID=@RefId and IsPromotion='Yes' and Status not in ('Delete','Hold')
												) a
											Where RowNo=1
											Order by NewID()";
				}
				command.Parameters.AddWithValue("@RefId", refId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						products.Add(MapProduct(reader));
					}
				}
			}
			return products;
		}

		public IEnumerable<Product> GetProductRelated(string pid)
		{
			List<Product> relateProductList = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"Select	p.pID,pTName,pEname,pTUnit,pEunit,NumOfRating,PC.PriceExcVat,PC.PriceIncVat,IsPromotion,PC.IsBestDeal,PC.IsVat,HasBookPreview,P.Status,PC.PriceType,ValidFrom,ValidTo
											From	TBProductCenter P with (nolock)
													inner join TBRelate R with (nolock) On P.PID=R.RelateId and R.TypeName='Product' and R.Status='Active' and P.Status not in ('Delete','Hold')
													inner join TBProductCatalog PC with (nolock) on (p.pid = PC.ProductID and ((PC.CompanyID = @companyId and PC.CatalogtypeID = @companyId) or (PC.CompanyID = @companyId and PC.CatalogtypeID= @userId)))
											where	SourceId=@pId";
				}
				else
				{
					command.CommandText = @"Select	p.pID,pTName,pEname,pTUnit,pEunit,NumOfRating,PC.PriceExcVat,PC.PriceIncVat,IsPromotion,PC.IsBestDeal,PC.IsVat,HasBookPreview,P.Status,PC.PriceType,ValidFrom,ValidTo
											From	TBProductCenter P with (nolock)
													inner join TBRelate R with (nolock) On P.PID=R.RelateId and R.TypeName='Product' and R.Status='Active' and P.Status not in ('Delete','Hold')
													inner join TBProductCatalog PC with (nolock) on (p.pid = PC.ProductID and ((PC.CompanyID = @companyId and PC.CatalogtypeID = @companyId) or (PC.CompanyID = @companyId and PC.CatalogtypeID= @userId)))
											where	SourceId=@pId
											UNION ALL
											Select	P.pID,pTName,pEname,pTUnit,pEunit,NumOfRating,P.PriceExcVat,P.PriceIncVat,IsPromotion,IsBestDeal,IsVat,HasBookPreview,P.Status,'Float' AS PriceType,'' AS ValidFrom,'' AS ValidTo
											From	TBProductCenter P with (nolock)
													inner join TBRelate R with (nolock) On P.PID=R.RelateId and R.TypeName='Product' and R.status='Active' and P.status not in ('Delete','Hold') and P.IsSpcCatEpro='No'
											where	SourceId=@pId
													and not exists (select ProductId from TBProductCatalog PC Where P.pID=PC.ProductID
													and ((CompanyID=@companyId and CatalogtypeID=@companyId) or (CompanyID=@companyId and CatalogtypeID=@userId)))";
				}
				command.Parameters.AddWithValue("@pId", pid);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						relateProductList.Add(MapProduct(reader));
					}
				}
			}
			return relateProductList;
		}

		private Product MapProduct(SqlDataReader reader)
		{
			Product product = new Product();

			var columns = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
			for (int i = 0; i < reader.FieldCount; i++)
			{
				columns.Add(reader.GetName(i));
			}
			product.Id = (string)reader["pid"];
			product.CodeId = columns.Contains("CodeId") ? (string)reader["CodeId"] : "";
			product.DeptId = columns.Contains("DeptId") ? (int)reader["DeptId"] : 0;//จริงๆ ตัวนี้ต้องมีค่าเสมอ
			product.ThaiName = (string)reader["pTname"];
			product.EngName = (string)reader["pEname"];
			product.Name = new LocalizedString((string)reader["pTname"]);
			product.Name["en"] = new LocalizedString((string)reader["pEname"]);
			product.NumOfRating = columns.Contains("NumOfRating") ? (decimal)reader["NumOfRating"] : 0;
			product.ThaiUnit = columns.Contains("pTUnit") ? (string)reader["pTUnit"] : "";
			product.EngUnit = columns.Contains("pEUnit") ? (string)reader["pEUnit"] : "";
			product.Unit = new LocalizedString((string)reader["pTUnit"]);
			product.Unit["en"] = (string)reader["pEUnit"];
			//product.Description = columns.Contains("pTLDesc") ? (string)reader["pTLDesc"] : "";
			//product.DescriptionShort = columns.Contains("pTSDesc") ? (string)reader["pTSDesc"] : "";

			//ให้แสดงเฉพาะ ภาษาไทย เพราะข้อมูลยังไม่พร้อม
			product.DescriptionThai = columns.Contains("pTLDesc") ? (string)reader["pTLDesc"] : "";
			product.DescriptionEng = columns.Contains("pTLDesc") ? (string)reader["pTLDesc"] : "";
			product.Description = new LocalizedString(columns.Contains("pTLDesc") ? (string)reader["pTLDesc"] : "");
			product.Description["en"] = columns.Contains("pTLDesc") ? (string)reader["pTLDesc"] : "";
			product.DescriptionShortThai = columns.Contains("pTSDesc") ? (string)reader["pTSDesc"] : "";
			product.DescriptionShortEng = columns.Contains("pTSDesc") ? (string)reader["pTSDesc"] : "";
			product.DescriptionShort = new LocalizedString(columns.Contains("pTSDesc") ? (string)reader["pTSDesc"] : "");
			product.DescriptionShort["en"] = columns.Contains("pTSDesc") ? (string)reader["pTSDesc"] : "";

			product.PriceType = columns.Contains("PriceType") ? (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true) : PriceType.Float;
			product.PriceIncVat = columns.Contains("PriceIncVat") ? (decimal)reader["PriceIncVat"] : 0;
			product.PriceExcVat = columns.Contains("PriceExcVat") ? (decimal)reader["PriceExcVat"] : 0;

			product.PriceBegin = columns.Contains("MinPrice") ? (decimal)reader["MinPrice"] : 0;
			product.PriceEnd = columns.Contains("MaxPrice") ? (decimal)reader["MaxPrice"] : 0;
			product.FullPriceIncVat = columns.Contains("FullPrice") ? (decimal)reader["FullPrice"] : 0;

			product.TransCharge = columns.Contains("DeliverFee") ? (decimal)reader["DeliverFee"] : 0;
			product.IsPromotion = columns.Contains("IsPromotion") ? Mapper.ToClass<bool>(reader["IsPromotion"]) : false;
			product.IsReccommend = columns.Contains("IsRecommend") ? Mapper.ToClass<bool>(reader["IsRecommend"]) : false;
			product.IsNew = columns.Contains("IsNew") ? Mapper.ToClass<bool>(reader["IsNew"]) : false;
			product.IsVat = columns.Contains("IsVat") ? Mapper.ToClass<bool>(reader["IsVat"]) : false;
			product.IsInstock = columns.Contains("IsInstock") ? Mapper.ToClass<bool>(reader["IsInstock"]) : false;
			product.IsBestDeal = columns.Contains("IsBestDeal") ? Mapper.ToClass<bool>(reader["IsBestDeal"]) : false;
			product.PromotionText = columns.Contains("PromotionTtext") ? (string)(reader["PromotionTtext"]) : "";
			product.IsDelivery = User.Company.IsDefaultDeliCharge; //ดูค่าจาก commpany เป็นหลัก
			product.ProdType = columns.Contains("ProdType") ? (string)reader["ProdType"] : "";
			product.Title = columns.Contains("Title") ? (string)reader["Title"] : "";
			product.Status = columns.Contains("Status") ? Mapper.ToClass<ProductStatusType>(reader["Status"].ToString().ToLower()) : ProductStatusType.Deleted;
			product.UploadOn = columns.Contains("UploadOn") ? (DateTime)reader["UploadOn"] : DateTime.MinValue;
			product.IsPremium = columns.Contains("IsPremium") ? Mapper.ToClass<bool>(reader["IsPremium"]) : false;
			product.BrandId = columns.Contains("BrandId") ? (string)(reader["BrandId"]) : "";
			product.BrandName = columns.Contains("BrandName") ? (string)(reader["BrandName"]) : "";
			product.RefId = columns.Contains("RefId") ? (string)(reader["RefId"]) : "";
			product.PropName = columns.Contains("PropName") ? (string)(reader["PropName"]) : "";
			product.PropValue = columns.Contains("PropValue") ? (string)(reader["PropValue"]) : "";
			product.pcatId = columns.Contains("pcatId") ? (string)(reader["pcatId"]) : "";
			product.psubcatId = columns.Contains("psubcatId") ? (string)(reader["psubcatId"]) : "";
			product.supplierId = columns.Contains("supplierId") ? (string)(reader["supplierId"]) : "";
			product.supplierName = columns.Contains("supplierName") ? (string)(reader["supplierName"]) : "";
			product.PriceFlag = columns.Contains("PriceFlag") ? (string)(reader["PriceFlag"]) : "";
			product.DefaultSku = columns.Contains("DefaultSku") ? (string)(reader["DefaultSku"]) : "";
			product.IsOfmCatalog = columns.Contains("IsOfmCatalog") ? Mapper.ToClass<bool>(reader["IsOfmCatalog"]) : false; //ใช้ในการ check ว่าเป็นสินค้า ofm catalog หรือไม่ (true = เป็นสินค้านอก catalog, false = เป็นสินค้าใน catalog)

			product.User = User;

			return product;
		}
		
		public bool IsPidInSystem(string pid, out string productId)
		{
			productId = string.Empty;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog && !User.Company.ShowOFMCat)
				{
					command.CommandText = @"SELECT t1.ProductID as pID FROM TBProductCatalog t1 INNER JOIN TBProductCenter t2 ON t1.ProductID=t2.pID
											WHERE	ProductID=@pid AND [Status] NOT IN ('Delete','Hold')
													AND ((CompanyID = @companyId and CatalogtypeID = @companyId) OR (CompanyID = @companyId AND CatalogtypeID= @userId))";
				}
				else
				{
					command.CommandText = @"SELECT	pID
											FROM	TBProductCenter
											WHERE	pID=@pid AND [Status] NOT IN ('Delete','Hold') and IsSpcCatEpro='No'
											UNION ALL
											SELECT	t2.pID
											FROM	TBProductCatalog t1 INNER JOIN TBProductCenter t2 ON t1.ProductID=t2.pID
											WHERE	ProductID=@pid AND [Status] NOT IN ('Delete','Hold') and IsSpcCatEpro='Yes'
													AND ((CompanyID = @companyId and CatalogtypeID = @companyId) OR (CompanyID = @companyId AND CatalogtypeID= @userId))";
				}
				command.Parameters.AddWithValue("@pid", pid);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						productId = (string)reader["pID"];
						return true;
					}
					return false;
				}
			}
		}

		public IEnumerable<string> IsListPidInProductCatalog() //เช็คว่าสินค้านี้ อยู่ใน TBProductCatalog หรือไม่ ใช้ในการ approve order ของ admin
		{
			List<string> listId = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT ProductID FROM TBProductCatalog WHERE CompanyID=@companyId";
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					listId = new List<string>();
					while (reader.Read())
					{
						listId.Add((string)reader["ProductID"]);
					}
					return listId;
				}
			}
		}

		public Product GetProductDetailByPidForOFMAdmin(string pId) //ดึงรายละเอียดสินค้าจาก TBProductCenter ใช้ในการ add product to catalog ของ ofm admin
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Select	pID,pTname,pEname,CodeId,0.0 as MinPrice,0.0 as MaxPrice,
												FullPrice,PriceIncVat,PriceExcVat,'No' as IsPG, BrandId,
												NumOfRating,pTUnit,pEunit,pTLDesc,pTSDesc,IsPromotion,PromotionTtext,IsBestDeal,deptId,ProdType,
												HasBookPreview, DeliverFee, IsVat,IsPremium, IsDelivery, IsPremium,
												MetaDescription,MetaKeyword,Title, Status,PriceFlag,pESDesc,pELDesc
										From	TBProductCenter
										Where	Status not in ('Delete','Hold') and pID = @pId";
				command.Parameters.AddWithValue("@pId", pId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return MapProduct(reader);
					}
					return null;
				}
			}
		}
	}
}