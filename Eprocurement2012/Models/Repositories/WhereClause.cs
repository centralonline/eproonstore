﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text;

namespace Eprocurement2012.Models.Repositories
{
	public abstract class WhereClause
	{
		public static WhereClause Create()
		{
			return new EmptyClause();
		}

		public static WhereClause Create(string sqlExpression)
		{
			return new UnaryClause(sqlExpression);
		}

		protected abstract void GenerateSqlExpression(StringBuilder sqlBuilder);

		protected virtual Type PreviewClauseType()
		{
			return GetType();
		}

		public override string ToString()
		{
			StringBuilder sqlBuilder = new StringBuilder(1024);
			GenerateSqlExpression(sqlBuilder);
			if (sqlBuilder.Length > 0)
			{
				sqlBuilder.Insert(0, " WHERE ");
			}
			return sqlBuilder.ToString();
		}

		public virtual WhereClause And(string sqlExpression)
		{
			return new AndClause(this, new UnaryClause(sqlExpression));
		}

		public virtual WhereClause And(WhereClause whereClause)
		{
			return new AndClause(this, whereClause);
		}

		public virtual WhereClause Or(string sqlExpression)
		{
			return new OrClause(this, new UnaryClause(sqlExpression));
		}

		public virtual WhereClause Or(WhereClause whereClause)
		{
			return new OrClause(this, whereClause);
		}

		class EmptyClause : WhereClause
		{
			protected override void GenerateSqlExpression(StringBuilder sqlBuilder)
			{
				return;
			}
		}

		class UnaryClause : WhereClause
		{
			protected string _sqlExpression;
			public UnaryClause(string sqlExpression)
			{
				_sqlExpression = sqlExpression;
			}

			protected override void GenerateSqlExpression(StringBuilder sqlBuilder)
			{
				sqlBuilder.Append(_sqlExpression);
			}
		}

		abstract class BinaryClause : WhereClause
		{
			protected WhereClause _leftClause;
			protected WhereClause _rightClause;

			public BinaryClause(WhereClause leftClause, WhereClause rightClause)
			{
				_leftClause = leftClause;
				_rightClause = rightClause;
			}

			protected void EncloseClauseInParen(StringBuilder sqlBuilder, WhereClause whereClause)
			{
				sqlBuilder.Append("( ");
				whereClause.GenerateSqlExpression(sqlBuilder);
				sqlBuilder.Append(" )");
			}

			protected override Type PreviewClauseType()
			{
				if (_leftClause.PreviewClauseType() == typeof(EmptyClause) && _rightClause.PreviewClauseType() == typeof(EmptyClause))
				{
					return typeof(EmptyClause);
				}

				if (_leftClause.PreviewClauseType() == typeof(EmptyClause))
				{
					return _rightClause.PreviewClauseType();
				}

				if (_rightClause.PreviewClauseType() == typeof(EmptyClause))
				{
					return _leftClause.PreviewClauseType();
				}

				return base.PreviewClauseType();
			}
		}

		class AndClause : BinaryClause
		{
			public AndClause(WhereClause leftClause, WhereClause rightClause) : base(leftClause, rightClause) { }

			protected override void GenerateSqlExpression(StringBuilder sqlBuilder)
			{
				if (_leftClause.PreviewClauseType() == typeof(EmptyClause) || _rightClause.PreviewClauseType() == typeof(EmptyClause))
				{
					_leftClause.GenerateSqlExpression(sqlBuilder);
					_rightClause.GenerateSqlExpression(sqlBuilder);
					return;
				}

				if (_leftClause.PreviewClauseType() == typeof(OrClause))
				{
					EncloseClauseInParen(sqlBuilder, _leftClause);
				}
				else
				{
					_leftClause.GenerateSqlExpression(sqlBuilder);
				}

				sqlBuilder.Append(" AND ");

				if (_rightClause.PreviewClauseType() == typeof(OrClause))
				{
					EncloseClauseInParen(sqlBuilder, _rightClause);
				}
				else
				{
					_rightClause.GenerateSqlExpression(sqlBuilder);
				}
			}
		}

		class OrClause : BinaryClause
		{
			public OrClause(WhereClause leftClause, WhereClause rightClause) : base(leftClause, rightClause) { }

			protected override void GenerateSqlExpression(StringBuilder sqlBuilder)
			{
				if (_leftClause.PreviewClauseType() == typeof(EmptyClause) || _rightClause.PreviewClauseType() == typeof(EmptyClause))
				{
					_leftClause.GenerateSqlExpression(sqlBuilder);
					_rightClause.GenerateSqlExpression(sqlBuilder);
					return;
				}

				if (_leftClause.PreviewClauseType() == typeof(AndClause))
				{
					EncloseClauseInParen(sqlBuilder, _leftClause);
				}
				else
				{
					_leftClause.GenerateSqlExpression(sqlBuilder);
				}

				sqlBuilder.Append(" OR ");

				if (_rightClause.PreviewClauseType() == typeof(AndClause))
				{
					EncloseClauseInParen(sqlBuilder, _rightClause);
				}
				else
				{
					_rightClause.GenerateSqlExpression(sqlBuilder);
				}
			}
		}
	}
}
