﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using System.Web.Mvc;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class CatalogRepository : UserFilteredRepository
	{
		public CatalogRepository(SqlConnection connection, User user) : base(connection, user) { }

		public string CreateMyCatalog(Catalog catalog)
		{
			StringBuilder sqlBuilder = new StringBuilder(2048);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string generateGuid = Guid.NewGuid().ToString();
				sqlBuilder.AppendLine(@"Declare @CatalogID Varchar(12)
										exec spc_GetMaxIDInc 'TBUserCatalogHead','CatalogID','Standard','CT','','',@CatalogID output");

				if (catalog.IsDefault == "Yes")
				{
					sqlBuilder.AppendLine(@"Update TBUserCatalogHead Set IsDefault ='No' Where UserID = @UserID and IsDefault ='Yes'");
				}

				sqlBuilder.AppendLine(@"INSERT INTO TBUserCatalogHead (CatalogID,UserID,CompanyID,CatalogName,CatalogDesc,CountProduct,GUID,Status,IsDefault,CreateOn,CreateBy)
										values (@CatalogID,@UserID,@CompanyID,@CatalogName,@CatalogDesc,@CountProduct,@GUID,@Status,@IsDefault,getdate(),@CreateBy)");

				sqlBuilder.AppendLine();

				command.Parameters.AddWithValue("@UserID", User.UserId);
				command.Parameters.AddWithValue("@CompanyID", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@CatalogName", catalog.CatalogName);
				command.Parameters.AddWithValue("@CatalogDesc", catalog.CatalogDesc);
				command.Parameters.AddWithValue("@CountProduct", catalog.CountProduct);
				command.Parameters.AddWithValue("@GUID", generateGuid);
				command.Parameters.AddWithValue("@Status", Catalog.CatalogStatus.Active.ToString());
				command.Parameters.AddWithValue("@IsDefault", catalog.IsDefault);
				command.Parameters.AddWithValue("@CreateBy", User.UserId);

				sqlBuilder.AppendLine(" Select @GUID");

				command.CommandText = sqlBuilder.ToString();
				string result = (string)command.ExecuteScalar();
				return result;
			}
		}

		public void AddProductByPID(string guid, string productId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Declare @CatalogID varchar(20)
										
										select @CatalogID=CatalogID
										from TBUserCatalogHead where GUID=@GUID

										DELETE FROM TBUserCatalogDetail
										WHERE @CatalogID=CatalogID AND ProductID IN (@productId)

										insert into TBUserCatalogDetail (CatalogID,ProductID,CreateOn,CreateBy)
										values (@CatalogID,@ProductID,getdate(),@UserID)

										update TBUserCatalogHead
										set CountProduct=(Select Count(ProductID) from TBUserCatalogDetail Where CatalogID=@CatalogID),
										UpdateOn =getdate(),UpdateBy =@UserID
										where CatalogID =@CatalogID";

				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@GUID", guid);
				command.Parameters.AddWithValue("@productId", productId);
				command.ExecuteNonQuery();
			}
		}

		public IEnumerable<Catalog> GetAllMyCatalog(string companyId)
		{
			List<Catalog> catalogs = new List<Catalog>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT * FROM TBUserCatalogHead where UserID=@UserID AND CompanyID=@CompanyID AND Status=@Status
												ORDER BY IsDefault DESC";

				command.Parameters.AddWithValue("@UserID", User.UserId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@Status", Catalog.CatalogStatus.Active.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Catalog catalog = new Catalog();
						catalog.CatalogID = (string)reader["CatalogID"];
						catalog.UserID = (string)reader["UserID"];
						catalog.CompanyID = (string)reader["CompanyID"];
						catalog.CatalogName = (string)reader["CatalogName"];
						catalog.CatalogDesc = (string)reader["CatalogDesc"];
						catalog.CountProduct = (int)reader["CountProduct"];
						catalog.GUID = (string)reader["GUID"];
						catalog.Status = (Catalog.CatalogStatus)Enum.Parse(typeof(Catalog.CatalogStatus), (string)reader["Status"]);
						catalog.IsDefault = (string)reader["IsDefault"];
						catalog.CreateOn = (DateTime)reader["CreateOn"];
						catalog.CreateBy = (string)reader["CreateBy"];
						catalogs.Add(catalog);
					}
				}
				return catalogs;
			}
		}

		public Catalog GetMyCatalog(string guid)
		{
			Catalog catalog = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT * FROM TBUserCatalogHead where GUID=@GUID";

				command.Parameters.AddWithValue("@GUID", guid);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						catalog = new Catalog();
						catalog.CatalogID = (string)reader["CatalogID"];
						catalog.UserID = (string)reader["UserID"];
						catalog.CompanyID = (string)reader["CompanyID"];
						catalog.CatalogName = (string)reader["CatalogName"];
						catalog.CatalogDesc = (string)reader["CatalogDesc"];
						catalog.CountProduct = (int)reader["CountProduct"];
						catalog.GUID = (string)reader["GUID"];
						catalog.Status = (Catalog.CatalogStatus)Enum.Parse(typeof(Catalog.CatalogStatus), (string)reader["Status"]);
						catalog.IsDefault = (string)reader["IsDefault"];
						catalog.CreateOn = (DateTime)reader["CreateOn"];
						catalog.CreateBy = (string)reader["CreateBy"];

						reader.Close();
						catalog.CatalogDetail = GetCatalogDetail(catalog.CatalogID, catalog.CompanyID,catalog.UserID);
						return catalog;
					}
					else
					{
						return null;
					}
				}
			}
		}


		//ดึงข้อมูล Detail มาแสดงใน MyCatalog
		private IEnumerable<Product> GetCatalogDetail(string catalogID, string companyId, string userId)
		{
			List<Product> products = new List<Product>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog)
				{

					command.CommandText = @"select	row_number() over(order by ProductID desc) as 'SeqNo',* 
										from	(
												SELECT	Detail.ProductID,TPC.CodeId,TPC.DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
														TPCL.PriceType,TPCL.PriceIncVat,TPCL.PriceExcVat,TPC.FullPrice,0.0 as MinPrice,0.0 as MaxPrice,
														TPC.IsPromotion,TPC.IsRecommend,TPC.IsNew,TPCL.IsVat,TPCL.IsBestDeal,IsInstock,IsPremium,
														DeliverFee,TPC.PageNo,PromotionTtext,ProdType,Title,TPC.Status,TPC.UploadOn,TPC.BrandId,TPC.BrandName,
														pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,ValidFrom,ValidTo

												FROM	TBUserCatalogDetail Detail with (nolock) INNER JOIN TBProductCatalog TPCL with (nolock)
														on (Detail.ProductID=TPCL.ProductID and ((TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@companyId) or (TPCL.CompanyID=@companyId and TPCL.CatalogtypeID=@userId )))
														INNER JOIN TBProductCenter TPC with (nolock) ON TPCL.ProductID = TPC.pID

												WHERE	Detail.CatalogID=@CatalogID AND TPC.Status NOT IN ('Delete','Hold')
										) catalog";
				}
				else
				{
					command.CommandText = @"Select	Detail.ProductID,CodeId,DeptId,pTname,pEname,NumOfRating,pTUnit,pEUnit,pTSDesc,pESDesc,pTLDesc,pELDesc,
													'Float' as PriceType,PriceIncVat,PriceExcVat,FullPrice,0.0 as MinPrice,0.0 as MaxPrice,
													IsPromotion,IsRecommend,IsNew,IsVat,IsBestDeal,IsInstock,IsPremium,
													DeliverFee,PageNo,PromotionTtext,ProdType,Title,TPC.Status,UploadOn,BrandId,BrandName,
													pCatID,pSubCatID,SupplierID,SupplierName,PriceFlag,'' as ValidFrom,'' as ValidTo

											From	TBProductCenter TPC with (nolock)
													INNER JOIN TBUserCatalogDetail Detail with (nolock) ON TPC.pID=Detail.ProductID
													INNER JOIN TBUserCatalogHead Head with (nolock) ON Detail.CatalogID=Head.CatalogID AND Head.CompanyID=@CompanyID

											Where	Detail.CatalogID=@CatalogID AND TPC.Status NOT IN ('Delete','Hold')";
				}

				command.Parameters.AddWithValue("@CatalogID", catalogID);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@UserId", userId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Product product = new Product();
						product.Id = (string)reader["ProductID"];
						product.CodeId = (string)reader["CodeId"];
						product.DeptId = (int)reader["DeptId"];//จริงๆ ตัวนี้ต้องมีค่าเสมอ
						product.ThaiName = (string)reader["pTname"];
						product.EngName = (string)reader["pEname"];
						product.Name = new LocalizedString((string)reader["pTname"]);
						product.Name["en"] = new LocalizedString((string)reader["pEname"]);
						product.NumOfRating = (decimal)reader["NumOfRating"];
						product.ThaiUnit = (string)reader["pTUnit"];
						product.EngUnit = (string)reader["pEUnit"];
						product.Unit = new LocalizedString((string)reader["pTUnit"]);
						product.Unit["en"] = (string)reader["pEUnit"];

						//ให้แสดงเฉพาะ ภาษาไทย เพราะข้อมูลยังไม่พร้อม
						product.DescriptionThai = (string)reader["pTLDesc"];
						product.DescriptionEng = (string)reader["pTLDesc"];
						product.Description = new LocalizedString((string)reader["pTLDesc"]);
						product.Description["en"] = new LocalizedString((string)reader["pTLDesc"]);
						product.DescriptionShortThai = (string)reader["pTSDesc"];
						product.DescriptionShortEng = (string)reader["pTSDesc"];
						product.DescriptionShort = new LocalizedString((string)reader["pTSDesc"]);
						product.DescriptionShort["en"] = new LocalizedString((string)reader["pTSDesc"]);

						product.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						product.PriceIncVat = (decimal)reader["PriceIncVat"];
						product.PriceExcVat = (decimal)reader["PriceExcVat"];
						product.PriceBegin = (decimal)reader["MinPrice"];
						product.PriceEnd = (decimal)reader["MaxPrice"];
						product.FullPriceIncVat = (decimal)reader["FullPrice"];
						product.pcatId = (string)(reader["pCatID"]);
						product.psubcatId = (string)(reader["pSubCatID"]);
						product.Status = Mapper.ToClass<ProductStatusType>(reader["Status"].ToString().ToLower());
						product.TransCharge = (decimal)reader["DeliverFee"];
						product.IsPromotion = Mapper.ToClass<bool>(reader["IsPromotion"]);
						product.IsReccommend = Mapper.ToClass<bool>(reader["IsRecommend"]);
						product.IsNew = Mapper.ToClass<bool>(reader["IsNew"]);
						product.IsVat = Mapper.ToClass<bool>(reader["IsVat"]);
						product.IsInstock = Mapper.ToClass<bool>(reader["IsInstock"]);
						product.IsBestDeal = Mapper.ToClass<bool>(reader["IsBestDeal"]);
						product.PromotionText = (string)reader["PromotionTtext"];
						product.ProdType = (string)reader["ProdType"];
						product.Title = (string)reader["Title"];
						product.UploadOn = (DateTime)reader["UploadOn"];
						product.IsPremium = Mapper.ToClass<bool>(reader["IsPremium"]);
						product.BrandId = (string)reader["BrandId"];
						product.BrandName = (string)reader["BrandName"];
						product.supplierId = (string)reader["supplierId"];
						product.supplierName = (string)reader["supplierName"];
						product.PriceFlag = (string)reader["PriceFlag"];

						product.User = User;
						products.Add(product);
					}
				}
				return products;
			}
		}

		public void UpdateMyCatalog(string guid, string catalogName, string catalogDesc, string isDefault, string userId)
		{
			StringBuilder sqlBuilder = new StringBuilder(2048);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (isDefault == "Yes")
				{
					sqlBuilder.AppendLine(@"Update TBUserCatalogHead Set IsDefault ='No'
											Where UserID = @UserID and IsDefault ='Yes'");
				}
				sqlBuilder.AppendLine(@"UPDATE TBUserCatalogHead SET CatalogName=@CatalogName,CatalogDesc=@CatalogDesc,IsDefault=@IsDefault,
												UpdateBy=@UserID,UpdateOn =getdate()
										where GUID=@GUID");

				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@GUID", guid);
				command.Parameters.AddWithValue("@CatalogName", catalogName);
				command.Parameters.AddWithValue("@CatalogDesc", catalogDesc);
				command.Parameters.AddWithValue("@IsDefault", Mapper.ToDatabase(isDefault));
				command.Parameters.AddWithValue("@UserID", userId);
				command.ExecuteNonQuery();
			}
		}

		public string DeleteMyCatalog(string guid, string userId, string isDefault)
		{
			StringBuilder sqlBuilder = new StringBuilder(2048);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine(@"UPDATE TBUserCatalogHead SET Status=@Status,IsDefault ='No',UpdateBy=@UserID,UpdateOn =getdate()
										where GUID=@GUID");

				if (isDefault == "Yes")
				{
					sqlBuilder.AppendLine(@"DECLARE @SelectGuid as varchar(36)
											SELECT TOP 1 @SelectGuid=GUID FROM TBUserCatalogHead where UserID=@UserID AND Status='Active' ORDER BY CatalogID ASC

											IF (@SelectGuid is not null)
											begin
												UPDATE TBUserCatalogHead SET IsDefault='Yes' WHERE guid=@SelectGuid
													select @SelectGuid
											end 
											Else
											begin
												select ''
											end
											");
				}


				command.CommandText = sqlBuilder.ToString();

				command.Parameters.AddWithValue("@GUID", guid);
				command.Parameters.AddWithValue("@Status", Catalog.CatalogStatus.Delete.ToString());
				command.Parameters.AddWithValue("@UserID", userId);
				return command.ExecuteScalar().ToString();
			}
		}

		//ฟังก์ชันสำหรับดึงข้อมูล ชื่อ Catalog ทั้งหมด
		public IEnumerable<SelectListItem> MyCatalogList(string companyId)
		{
			List<SelectListItem> catalogs = new List<SelectListItem>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT CatalogID,CatalogName,IsDefault,Status,GUID
										FROM TBUserCatalogHead
										where UserID=@UserID AND CompanyID=@CompanyID AND Status=@Status";

				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@Status", Eprocurement2012.Models.Catalog.CatalogStatus.Active.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						SelectListItem mycat = new SelectListItem();
						mycat.Text = (string)reader["CatalogName"];
						mycat.Value = (string)reader["GUID"];
						if ((string)reader["IsDefault"] == "Yes")
						{
							mycat.Selected = true;
						}
						catalogs.Add(mycat);
					}
				}
			}
			return catalogs;
		}

		//ฟังก์ชัน check ว่ามี Catalog หรือยัง
		public bool CheckHasCatalog()
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT CatalogID FROM TBUserCatalogHead 
										where UserID=@UserID AND CompanyID=@CompanyID AND Status=@Status";

				command.Parameters.AddWithValue("@UserID", User.UserId);
				command.Parameters.AddWithValue("@CompanyID", User.CurrentCompanyId);
				command.Parameters.AddWithValue("@Status", Eprocurement2012.Models.Catalog.CatalogStatus.Active.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return true; // มี catalog แล้ว
					}
				}
				return false;
			}
		}


		public bool CheckHasIsDefault(string guid)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT IsDefault FROM TBUserCatalogHead where GUID=@GUID";

				command.Parameters.AddWithValue("@GUID", guid);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						if ((string)reader["IsDefault"] == "Yes")
						{
							return true;
						}
					}
					return false;
				}
			}
		}

		//เพิ่มสินค้าลงใน Catalog
		public void AddToCatalog(string pID, string guid)
		{
			string[] itempID = pID.Split(',');
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine(@"
										Declare @CatalogID varchar(20)

										SELECT @CatalogID=CatalogID FROM TBUserCatalogHead
										WHERE GUID=@GUID

										DELETE FROM TBUserCatalogDetail
										WHERE @CatalogID=CatalogID AND ProductID IN (@pID)");
				sqlBuilder.AppendLine();
				foreach (string item in itempID)
				{

					sqlBuilder.AppendLine(@"
											INSERT INTO TBUserCatalogDetail (CatalogID,ProductID,CreateOn,CreateBy)
											values (@CatalogID,'" + item + "',getdate(),@UserID)");
				}

				sqlBuilder.AppendLine();
				sqlBuilder.AppendLine(@"
										UPDATE TBUserCatalogHead
										SET CountProduct = (SELECT count(ProductID) from TBUserCatalogDetail where CatalogID=@CatalogID),UpdateOn =getdate(),UpdateBy =@UserID
										where CatalogID =@CatalogID");

				command.Parameters.AddWithValue("@UserID", User.UserId);
				command.Parameters.AddWithValue("@GUID", guid);
				command.Parameters.AddWithValue("@pID", pID);

				command.CommandText = sqlBuilder.ToString();

				command.ExecuteNonQuery();

			}
		}

		//ลบรายการสินค้าจาก MyCatalog
		public void DeleteItemMyCatalog(string guid, string[] item)
		{
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine(@"
										Declare @CatalogID varchar(20)

										SELECT @CatalogID=CatalogID FROM TBUserCatalogHead
										WHERE GUID=@GUID

										DELETE FROM TBUserCatalogDetail
										WHERE @CatalogID=CatalogID AND ProductID IN ({0})

										UPDATE TBUserCatalogHead
										SET CountProduct = (SELECT count(ProductID) from TBUserCatalogDetail where CatalogID=@CatalogID),UpdateOn =getdate(),UpdateBy =@UserID
										where CatalogID =@CatalogID");

				command.Parameters.AddWithValue("@UserId", User.UserId);
				command.Parameters.AddWithValue("@GUID", guid);

				string[] parameters = new string[item.Count()];
				int i = 0;
				foreach (var pID in item)
				{
					parameters[i] = "@" + i;
					command.Parameters.AddWithValue(parameters[i], pID);
					i++;
				}

				command.CommandText = String.Format(sqlBuilder.ToString(), String.Join(",", parameters));
				command.ExecuteNonQuery();

			}
		}

	}
}