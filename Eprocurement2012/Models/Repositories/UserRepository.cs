﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Hosting;
using OfficeMate.Framework.Security.Cryptography;
using System.Drawing;
using System.IO;
using Eprocurement2012.Controllers.Helpers;
using System.Text;
using OfficeMate.Framework;
using OfficeMate.Framework.CustomerData;

namespace Eprocurement2012.Models.Repositories
{
	public class UserRepository : RepositoryBase
	{
		public UserRepository(SqlConnection connection) : base(connection) { }
		private string _cacheKey = "User/{0}";

		public User GetUser(string userGuid, User.UserStatus userStatus)
		{
			if (userGuid == null) { throw new ArgumentNullException("userGuid"); }
			User user = null;

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	T2.UserGuID,T1.UserId,T1.UserTName,T1.UserEName,T1.UserLang,T3.UserStatus,T1.LastActive,T1.ChangePasswordOn,
												T1.IsVerified,T4.RePasswordPeriod,T4.RePasswordPeriod - (DATEDIFF(day,ChangePasswordOn, getdate())) As RemainDayChanged,
												T4.DefaultCustID,T4.CompanyID,T4.CompTName,T4.CompEName,T4.CompModel,T4.DefaultDeliCharge,T4.DefaultParkDay,T4.IsSiteActive,
												T4.OrderControlType,T4.BudgetLevelType,T4.BudgetPeriodType,T4.PriceType,T4.AllowAddProduct,T4.OrderIDFormat,
												T4.ShowOutofStock,T4.ShowSpecialProd,T4.IsByPassAdmin,T4.IsByPassApprover,T4.IsAutoApprove,T4.ShowOfmCat,
												T4.ShowContactUs,T4.UseOfmCatalog,T4.UseCompanyNews,T4.UseOfmNews,T4.UseReferenceCode,T4.UseSMSFeature,T5.DiscountRate,
												T4.UseGoodReceive,T4.GoodReceivePeriod,T4.ShowMultiApprove
										from	TBUserInfo T1 inner join TBUsers T2 on T1.UserID = T2.UserID
												inner join TBUserCompany T3 on T1.UserID = T3.UserID
												inner join TBCompanyInfo T4 on T3.companyID = T4.CompanyID and T4.CompanyID <> 'Officemate'
												inner join TBCustMaster T5 on T5.CustID = T4.DefaultCustId
										where	T2.UserGuID=@UserGuID and T3.UserStatus=@UserStatus and T4.CompStatus = @CompStatus and IsDefaultCompany=@IsDefault";

				command.Parameters.AddWithValue("@UserGuID", userGuid);
				command.Parameters.AddWithValue("@UserStatus", userStatus.ToString());
				command.Parameters.AddWithValue("@CompStatus", Company.CompanyStatus.Active.ToString());
				command.Parameters.AddWithValue("@IsDefault", User.UserCompanyDefault.Yes.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						user = new User();
						user.Company = new Company();
						user.IsLogin = true;
						user.UserGuid = (string)reader["UserGuID"];
						user.UserId = (string)reader["UserId"];
						user.CustId = (string)reader["DefaultCustID"];
						user.CurrentCompanyId = (string)reader["CompanyID"];
						user.UserThaiName = (string)reader["UserTName"];
						user.UserEngName = (string)reader["UserEName"];
						user.DisplayName = new LocalizedString((string)reader["UserTName"]);
						user.DisplayName["en"] = (string)reader["UserEName"];
						user.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["UserLang"], true);
						user.UserStatusName = (User.UserStatus)Enum.Parse(typeof(User.UserStatus), (string)reader["UserStatus"], true);
						user.UserLastActive = (DateTime)reader["LastActive"];
						user.IsVerified = Mapper.ToClass<bool>((string)reader["IsVerified"]);
						user.RemainDayForceChangedPassword = (int)reader["RemainDayChanged"];
						user.Company.DefaultCustId = (string)reader["DefaultCustId"];
						user.Company.CompanyId = (string)reader["CompanyID"];
						user.Company.CompanyThaiName = (string)reader["CompTName"];
						user.Company.CompanyEngName = (string)reader["CompEName"];
						user.Company.CompanyName = new LocalizedString((string)reader["CompTName"]);
						user.Company.CompanyName["en"] = (string)reader["CompEName"];
						user.Company.IsCompModelThreeLevel = reader["CompModel"].ToString().Equals("3Level");
						user.Company.CompanyDisCountRate = (decimal)reader["DiscountRate"];
						user.Company.DefaultParkDay = (int)reader["DefaultParkDay"];
						user.Company.OrderControlType = (Company.OrderControl)Enum.Parse(typeof(Company.OrderControl), (string)reader["OrderControlType"], true);
						if (user.Company.OrderControlType == Company.OrderControl.ByBudgetAndOrder)
						{
							user.Company.BudgetLevelType = (Company.BudgetLevel)Enum.Parse(typeof(Company.BudgetLevel), (string)reader["BudgetLevelType"], true);
							user.Company.BudgetPeriodType = (Company.BudgetPeriod)Enum.Parse(typeof(Company.BudgetPeriod), (string)reader["BudgetPeriodType"], true);
						}
						user.Company.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						user.Company.IsCompanyFixPrice = reader["PriceType"].ToString().Equals("Fix");
						user.Company.OrderIDFormat = (Company.OrderIDFormatType)Enum.Parse(typeof(Company.OrderIDFormatType), (string)reader["OrderIDFormat"], true);
						user.Company.AllowAddProduct = Mapper.ToClass<bool>((string)reader["AllowAddProduct"]);
						user.Company.IsDefaultDeliCharge = Mapper.ToClass<bool>((string)reader["DefaultDeliCharge"]);
						user.Company.IsByPassAdmin = Mapper.ToClass<bool>((string)reader["IsByPassAdmin"]);
						user.Company.IsByPassApprover = Mapper.ToClass<bool>((string)reader["IsByPassApprover"]);
						user.Company.IsAutoApprove = Mapper.ToClass<bool>((string)reader["IsAutoApprove"]);
						user.Company.IsSiteActive = Mapper.ToClass<bool>((string)reader["IsSiteActive"]);
						user.Company.ShowOFMCat = Mapper.ToClass<bool>((string)reader["ShowOfmCat"]);
						user.Company.ShowOutofStock = Mapper.ToClass<bool>((string)reader["ShowOutofStock"]);
						user.Company.ShowContactUs = Mapper.ToClass<bool>((string)reader["ShowContactUs"]);
						user.Company.ShowSpecialProd = Mapper.ToClass<bool>((string)reader["ShowSpecialProd"]);
						user.Company.UseCompanyNews = Mapper.ToClass<bool>((string)reader["UseCompanyNews"]);
						user.Company.UseOfmNews = Mapper.ToClass<bool>((string)reader["UseOfmNews"]);
						user.Company.UseReferenceCode = Mapper.ToClass<bool>((string)reader["UseReferenceCode"]);
						user.Company.UseSMSFeature = Mapper.ToClass<bool>((string)reader["UseSMSFeature"]);
						user.Company.UseOfmCatalog = Mapper.ToClass<bool>((string)reader["UseOfmCatalog"]);
						user.Company.UseGoodReceive = Mapper.ToClass<bool>((string)reader["UseGoodReceive"]);
						user.Company.GoodReceivePeriod = (int)reader["GoodReceivePeriod"];
						user.Company.ShowMultiApprove = Mapper.ToClass<bool>((string)reader["ShowMultiApprove"]);
					}
				}
			}

			return user;
		}
		public bool TryGetUserGuid(string userId, string password, out string userGuid, out bool haveDefaultCompany)
		{
			if (userId == null) { throw new ArgumentNullException("UserId"); }
			if (password == null) { throw new ArgumentNullException("Password"); }
			userGuid = String.Empty;
			haveDefaultCompany = false;

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	DISTINCT t2.Password,t2.UserGuID,IsDefaultCompany
										FROM	TBUserInfo t1 inner join TBUsers t2 ON t1.UserID = t2.UserId
												inner join TBUserCompany t3 ON t1.UserID = t3.UserID
												inner join TBCompanyInfo t4 ON t3.CompanyID = t4.CompanyID
										WHERE	t1.UserID = @UserId AND t3.UserStatus = @UserStatus AND t4.CompStatus = @CompStatus";

				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@UserStatus", User.UserStatus.Active.ToString());
				command.Parameters.AddWithValue("@CompStatus", Company.CompanyStatus.Active.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					List<bool> DefaultCompany = new List<bool>();
					string hashedPassword = String.Empty;
					while (reader.Read())
					{
						userGuid = (string)reader["UserGuid"];
						hashedPassword = (string)reader["Password"];
						DefaultCompany.Add(Mapper.ToClass<bool>((string)reader["IsDefaultCompany"]));
					}
					if (!string.IsNullOrEmpty(userGuid) && new SaltedSha256PasswordHasher().CheckPassword(password, hashedPassword))
					{
						haveDefaultCompany = DefaultCompany.Any(d => d);
						return true;
					}
					return false;
				}
			}
		}
		public ImageData GetProfilePicture(string userGuid)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				ImageData profileImage = new ImageData();
				profileImage.Bytes = null;
				if (userGuid == null) { return profileImage; }
				command.CommandText = @"select ProfileImage from TBUserInfo t1 INNER JOIN TBUsers t2 ON t1.UserID=t2.UserId where UserGuID=@userGuid";
				command.Parameters.AddWithValue("@userGuid", userGuid);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read() && reader["ProfileImage"] != DBNull.Value)
					{
						profileImage.Bytes = (byte[])reader["ProfileImage"];
					}
				}
				return profileImage;
			}
		}
		public User GetUserOFMAdmin(string userGuid, User.UserStatus userStatus)
		{
			if (userGuid == null) { throw new ArgumentNullException("userGuid"); }
			User user = null;

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	distinct T2.UserGuid,T1.UserId,UserTName,UserEName,UserLang,T3.UserStatus,
												T3.CompanyID,LastActive,ChangePasswordOn,NextChangePassword,ProfileImage,T5.RoleName
										from	TBUserInfo T1 inner join TBUsers T2 on T1.UserID = T2.UserID
												inner join TBUserCompany T3 on T1.UserID = T3.UserID and T3.CompanyID='Officemate'
												inner join TBRoleUsers T4 on T1.UserID=T4.UserID and T3.CompanyID=T4.CompanyID
												inner join TBRoles T5 on T4.RoleID=T5.RoleID
										where	T2.UserGuID=@UserGuID and T3.UserStatus=@UserStatus and T5.RoleName=@RoleName";

				command.Parameters.AddWithValue("@UserGuID", userGuid);
				command.Parameters.AddWithValue("@UserStatus", userStatus.ToString());
				command.Parameters.AddWithValue("@RoleName", User.UserRole.OFMAdmin.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						user = new User();
						user.IsLogin = true;
						user.UserGuid = userGuid;
						user.UserId = (string)reader["UserId"];
						user.UserThaiName = (string)reader["UserTName"];
						user.UserEngName = (string)reader["UserEName"];
						user.DisplayName = new LocalizedString((string)reader["UserTName"]);
						user.DisplayName["en"] = (string)reader["UserEName"];
						user.CurrentCompanyId = (string)reader["CompanyId"];
						user.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["UserLang"], true);
						user.UserStatusName = (User.UserStatus)Enum.Parse(typeof(User.UserStatus), (string)reader["UserStatus"], true);
						user.UserLastActive = (DateTime)reader["LastActive"];
						user.UserRoleName = (User.UserRole)Enum.Parse(typeof(User.UserRole), (string)reader["RoleName"], true);
					}
				}
			}
			return user;
		}
		/*ดึงชื่อทุกคนที่มีสิทธิเป็น RootAdmin หรือ AssistantAdmin*/
		public IEnumerable<User> GetAllUserAdmin(string companyId)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }

			List<User> item = new List<User>();

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	t2.UserGuID, t1.UserID, t1.UserTName, t1.UserEName, t1.UserLang, t3.UserStatus, t1.ProfileImage,
												t3.CompanyId, LastActive, ChangePasswordOn, IsVerified, t5.RoleId, t5.RoleName,t5.RoleThName,t5.RoleEnName
										FROM	TBUserInfo t1 inner join TBUsers t2 ON t1.UserID = t2.UserID
												inner join TBUserCompany t3 ON t1.UserId = t3.UserID
												inner join TBRoleUsers t4 ON t1.UserId = t4.UserID AND t3.CompanyID = t4.CompanyID
												inner join TBRoles t5 ON t4.RoleId = t5.RoleId
										WHERE	t3.CompanyId=@companyId AND t3.UserStatus=@UserStatus AND t5.RoleName IN (@RootAdmin, @AssistantAdmin)
												ORDER BY t1.UserID
										";

				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@UserStatus", User.UserStatus.Active.ToString());
				command.Parameters.AddWithValue("@RootAdmin", User.UserRole.RootAdmin.ToString());
				command.Parameters.AddWithValue("@AssistantAdmin", User.UserRole.AssistantAdmin.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					User user = null;
					while (reader.Read())
					{
						user = new User();
						user.UserGuid = (string)reader["UserGuID"];
						user.UserId = (string)reader["UserId"];
						user.CurrentCompanyId = (string)reader["CompanyId"];
						user.UserThaiName = (string)reader["UserTName"];
						user.UserEngName = (string)reader["UserEName"];
						user.DisplayName = new LocalizedString((string)reader["UserTName"]);
						user.DisplayName["en"] = (string)reader["UserEName"];
						user.UserLastActive = (DateTime)reader["LastActive"];
						user.ChangePasswordOn = (DateTime)reader["ChangePasswordOn"];
						user.IsVerified = Mapper.ToClass<bool>((string)reader["IsVerified"]);
						user.UserRoleName = (User.UserRole)Enum.Parse(typeof(User.UserRole), (string)reader["RoleName"], true);
						user.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["UserLang"], true);
						user.UserStatusName = (User.UserStatus)Enum.Parse(typeof(User.UserStatus), (string)reader["UserStatus"], true);
						item.Add(user);
					}
				}
			}
			return item;
		}
		/*ดึงข้อมูล User ทั้งหมด ใน company โดย isAdmin = true จะดึงเฉพาะคนที่มีสิทธิ RootAdmin หรือ AssistantAdmin และ isAdmin = false จะดึงเฉพาะคนที่ไม่มีสิทธิ RootAdmin หรือ AssistantAdmin*/
		public IEnumerable<User> GetAllUser(string companyId, bool isAdmin)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			List<User> item = new List<User>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	t2.UserGuID, t1.UserID, t1.UserTName, t1.UserEName, t1.UserLang, t3.UserStatus, t1.ProfileImage,
												t3.CompanyId, LastActive, ChangePasswordOn, IsVerified, t5.RoleId, t5.RoleName,t5.RoleThName,t5.RoleEnName
										FROM	TBUserInfo t1 inner join TBUsers t2 ON t1.UserID = t2.UserID
												inner join TBUserCompany t3 ON t1.UserId = t3.UserID
												inner join TBRoleUsers t4 ON t1.UserId = t4.UserID AND t3.CompanyID = t4.CompanyID
												inner join TBRoles t5 ON t4.RoleId = t5.RoleId
										WHERE	t3.CompanyId=@companyId
										";
				if (isAdmin)
				{
					command.CommandText += "		and t1.UserID in (SELECT UserID FROM TBRoleUsers t1 inner join TBRoles t2 ON t1.RoleID = t2.RoleID and RoleName in (@RootAdmin,@AssistantAdmin) and CompanyID=@companyId) ORDER BY t1.UserID";
				}
				else
				{
					command.CommandText += "		and t1.UserID not in (SELECT UserID FROM TBRoleUsers t1 inner join TBRoles t2 ON t1.RoleID = t2.RoleID and RoleName in (@RootAdmin,@AssistantAdmin) and CompanyID=@companyId) ORDER BY t1.UserID";
				}
				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@RootAdmin", User.UserRole.RootAdmin.ToString());
				command.Parameters.AddWithValue("@AssistantAdmin", User.UserRole.AssistantAdmin.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					User user = null;
					while (reader.Read())
					{
						user = new User();
						user.UserGuid = (string)reader["UserGuID"];
						user.UserId = (string)reader["UserId"];
						user.CurrentCompanyId = (string)reader["CompanyId"];
						user.UserThaiName = (string)reader["UserTName"];
						user.UserEngName = (string)reader["UserEName"];
						user.DisplayName = new LocalizedString((string)reader["UserTName"]);
						user.DisplayName["en"] = (string)reader["UserEName"];
						user.UserLastActive = (DateTime)reader["LastActive"];
						user.IsVerified = Mapper.ToClass<bool>((string)reader["IsVerified"]);
						user.ChangePasswordOn = (DateTime)reader["ChangePasswordOn"];
						user.DisplayUserRoleName = new LocalizedString((string)reader["RoleThName"]);
						user.DisplayUserRoleName["en"] = (string)reader["RoleEnName"];
						user.UserRoleName = (User.UserRole)Enum.Parse(typeof(User.UserRole), (string)reader["RoleName"], true);
						user.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["UserLang"], true);
						user.UserStatusName = (User.UserStatus)Enum.Parse(typeof(User.UserStatus), (string)reader["UserStatus"], true);
						item.Add(user);
					}
				}
			}
			return item;
		}

		public IEnumerable<User> GetAllUserBySearch(SearchUser searchUser)
		{
			List<User> users = new List<User>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder();
				sqlBuilder.AppendLine(@"SELECT userinfo.* ,compInfo.CompanyID,
										roles.RoleID,roles.RoleName,roles.RoleThName,roles.RoleEnName,
										compInfo.DefaultCustID,contmail.CustID,contmail.ContactID,contmail.ContactEmail,
										custPhone.SequenceID,custPhone.PhoneNo,custPhone.Extension,custPhone.TypeNumber,custPhone.IsDefault,comp.UserStatus
										FROM TBUserInfo userinfo ");
				sqlBuilder.AppendLine("inner join TBUsers users ON userinfo.UserID = users.UserID");
				sqlBuilder.AppendLine("inner join TBUserCompany comp ON userinfo.UserId = comp.UserID");
				sqlBuilder.AppendLine("inner join TBCompanyInfo compInfo ON compInfo.CompanyID = comp.CompanyID");
				sqlBuilder.AppendLine("inner join TBRoleUsers roleuser ON userinfo.UserId = roleuser.UserID AND comp.CompanyID = roleuser.CompanyID");
				sqlBuilder.AppendLine("inner join TBRoles roles ON roleuser.RoleId = roles.RoleId");
				sqlBuilder.AppendLine("inner join TBCustContactEmail contmail on userinfo.UserID=contmail.ContactEmail AND compInfo.DefaultCustID=contmail.CustID");
				sqlBuilder.AppendLine("inner join TBCustomerPhone custPhone on custPhone.CustID=compInfo.DefaultCustID and contmail.ContactID=custPhone.SequenceID");
				sqlBuilder.AppendLine("AND custPhone.TypeDataSource='contact' AND custPhone.TypeNumber='PhoneOut'");
				sqlBuilder.AppendLine("WHERE 1=1");

				if (searchUser.CheckUserId && !string.IsNullOrEmpty(searchUser.UserId))
				{
					sqlBuilder.AppendLine(" and userinfo.UserID=@UserID");
					command.Parameters.AddWithValue("@UserID", searchUser.UserId);
				}
				if (searchUser.CheckCompanyId && !string.IsNullOrEmpty(searchUser.CompanyId))
				{
					sqlBuilder.AppendLine(" and compInfo.CompanyID LIKE @CompanyID");
					command.Parameters.AddWithValue("@CompanyID", "%" + searchUser.CompanyId + "%");
				}
				if (searchUser.CheckUserStatusName && !string.IsNullOrEmpty(searchUser.UserStatusName))
				{
					sqlBuilder.AppendLine(" and comp.UserStatus=@UserStatusName");
					command.Parameters.AddWithValue("@UserStatusName", searchUser.UserStatusName);
				}
				if (searchUser.CheckUserName && !string.IsNullOrEmpty(searchUser.UserName))
				{
					sqlBuilder.AppendLine(" and userinfo.UserTName LIKE @UserName OR userinfo.UserEName LIKE @UserName");
					command.Parameters.AddWithValue("@UserName", "%" + searchUser.UserName + "%");
				}
				if (searchUser.CheckUserRoleName && !string.IsNullOrEmpty(searchUser.UserRoleName))
				{
					sqlBuilder.AppendLine(" and roles.RoleName=@UserRoleName");
					command.Parameters.AddWithValue("@UserRoleName", searchUser.UserRoleName);
				}
				if (searchUser.CheckCreateBy && !string.IsNullOrEmpty(searchUser.CreateBy))
				{
					sqlBuilder.AppendLine(" and userinfo.CreateBy LIKE @CreateBy");
					command.Parameters.AddWithValue("@CreateBy", searchUser.CreateBy);
				}
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						User user = new User();
						user.UserId = (string)reader["UserId"];
						user.UserThaiName = (string)reader["UserTName"];
						user.UserEngName = (string)reader["UserEName"];
						user.DisplayName = new LocalizedString((string)reader["UserTName"]);
						user.DisplayName["en"] = (string)reader["UserEName"];
						user.UserLastActive = (DateTime)reader["LastActive"];
						user.ChangePasswordOn = (DateTime)reader["ChangePasswordOn"];
						user.DisplayUserRoleName = new LocalizedString((string)reader["RoleThName"]);
						user.DisplayUserRoleName["en"] = (string)reader["RoleEnName"];
						user.UserRoleName = (User.UserRole)Enum.Parse(typeof(User.UserRole), (string)reader["RoleName"], true);
						user.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["UserLang"], true);
						user.UserStatusName = (User.UserStatus)Enum.Parse(typeof(User.UserStatus), (string)reader["UserStatus"], true);
						user.CreateOn = (DateTime)reader["CreateOn"];
						user.CreateBy = (string)reader["CreateBy"];
						user.UpdateOn = (DateTime)reader["UpdateOn"];
						user.UpdateBy = (string)reader["UpdateBy"];
						user.Company = new Company();
						user.Company.CompanyId = (string)reader["CompanyID"];
						user.UserContact = new Contact();
						user.UserContact.Phone = new Phone();
						user.UserContact.Phone.PhoneNo = (string)reader["PhoneNo"];
						user.UserContact.Phone.Extension = (string)reader["Extension"];
						users.Add(user);
					}
				}
			}
			return users;
		}


		/*ดึงชื่อทุกคนที่มีสิทธิเป็น req หรือ app (ไม่เอาคนที่มีสิทธิ RootAdmin หรือ AssistantAdmin สิทธิเดียว)*/
		public IEnumerable<User> GetAllUserReqOrApp(string companyId)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			List<User> item = new List<User>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	t2.UserGuID, t1.UserID, t1.UserTName, t1.UserEName, t1.UserLang, t3.UserStatus, t1.ProfileImage,
												t3.CompanyId, LastActive, ChangePasswordOn, IsVerified, t5.RoleId, t5.RoleName,t5.RoleThName,t5.RoleEnName
										FROM	TBUserInfo t1 with (nolock) inner join TBUsers t2 with (nolock) ON t1.UserID = t2.UserID
												inner join TBUserCompany t3 with (nolock) ON t1.UserId = t3.UserID
												inner join TBRoleUsers t4 with (nolock) ON t1.UserId = t4.UserID AND t3.CompanyID = t4.CompanyID
												inner join TBRoles t5 with (nolock) ON t4.RoleId = t5.RoleId
										WHERE	t3.CompanyId=@companyId AND t3.UserStatus=@UserStatus AND t5.RoleName NOT IN (@RootAdmin,@AssistantAdmin)
												ORDER BY t1.UserID
										";

				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@UserStatus", User.UserStatus.Active.ToString());
				command.Parameters.AddWithValue("@RootAdmin", User.UserRole.RootAdmin.ToString());
				command.Parameters.AddWithValue("@AssistantAdmin", User.UserRole.AssistantAdmin.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					User user = null;
					while (reader.Read())
					{
						user = new User();
						user.UserGuid = (string)reader["UserGuID"];
						user.UserId = (string)reader["UserId"];
						user.CurrentCompanyId = (string)reader["CompanyId"];
						user.UserThaiName = (string)reader["UserTName"];
						user.UserEngName = (string)reader["UserEName"];
						user.DisplayName = new LocalizedString((string)reader["UserTName"]);
						user.DisplayName["en"] = (string)reader["UserEName"];
						user.UserLastActive = (DateTime)reader["LastActive"];
						user.IsVerified = Mapper.ToClass<bool>((string)reader["IsVerified"]);
						user.ChangePasswordOn = (DateTime)reader["ChangePasswordOn"];
						user.DisplayUserRoleName = new LocalizedString((string)reader["RoleThName"]);
						user.DisplayUserRoleName["en"] = (string)reader["RoleEnName"];
						user.UserRoleName = (User.UserRole)Enum.Parse(typeof(User.UserRole), (string)reader["RoleName"], true);
						user.UserLanguage = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["UserLang"], true);
						user.UserStatusName = (User.UserStatus)Enum.Parse(typeof(User.UserStatus), (string)reader["UserStatus"], true);
						item.Add(user);
					}
				}
			}
			return item;
		}

		public IEnumerable<string> GetAllRequester(string companyId, string term)
		{
			List<string> requester = new List<string>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	t1.UserID,t1.UserTName, t1.UserEName
										FROM	TBUserInfo t1 with (nolock) inner join TBUsers t2 with (nolock) ON t1.UserID = t2.UserID
												inner join TBUserCompany t3 with (nolock) ON t1.UserId = t3.UserID
												inner join TBRoleUsers t4 with (nolock) ON t1.UserId = t4.UserID AND t3.CompanyID = t4.CompanyID
												inner join TBRoles t5 with (nolock) ON t4.RoleId = t5.RoleId
										WHERE	t3.CompanyId=@CompanyId AND t3.UserStatus=@UserStatus AND t5.RoleName = @Requester 
												AND (t1.UserID like @term OR t1.UserTName LIKE @term OR t1.UserEName LIKE @term)
												ORDER BY t1.UserID";

				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@UserStatus", User.UserStatus.Active.ToString());
				command.Parameters.AddWithValue("@Requester", User.UserRole.Requester.ToString());
				command.Parameters.AddWithValue("@term", term + "%");

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						User user = new User();
						user.DisplayName = new LocalizedString((string)reader["UserTName"]);
						user.DisplayName["en"] = (string)reader["UserEName"];
						requester.Add(string.Format("{0} [{1}]", user.DisplayName, (string)reader["UserID"]));
					}
					return requester.AsReadOnly();
				}
			}
		}

		public bool GetDefaultRequester(string companyId, string costcenterId, string reqUserId)
		{
			List<string> requester = new List<string>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	count(ReqUserID)
										FROM	tbrequesterline
										WHERE	CompanyId=@CompanyId AND CostcenterID=@CostcenterID AND ReqUserID=@ReqUserID";

				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CostcenterID", costcenterId);
				command.Parameters.AddWithValue("@ReqUserID", reqUserId);
				return Convert.ToInt32(command.ExecuteScalar()) > 0;
			}
		}





		//        public IEnumerable<string> SelectProvinceCityDistrictPostCodeSubZoneStrings()
		//        {
		//            List<string> result = new List<string>();
		//            using (SqlCommand command = CreateCommandEnsureConnectionOpen())
		//            {
		//                command.CommandText = @"SELECT	DISTINCT District, City, ProvinceID, PostCode
		//										FROM	TBCMCity";
		//                using (SqlDataReader reader = command.ExecuteReader())
		//                {
		//                    while (reader.Read())
		//                    {
		//                        result.Add(string.Format("{0} {1} {2} {3}", (string)reader["District"], (string)reader["City"], (string)reader["ProvinceID"], (string)reader["PostCode"]));
		//                    }
		//                    return result.AsReadOnly();
		//                }
		//            }
		//        }

		public IEnumerable<UserPermission> GetUserPermission(string userId, string companyId)
		{
			if (userId == null) { throw new ArgumentNullException("userId"); }
			if (companyId == null) { throw new ArgumentNullException("companyId"); }

			List<UserPermission> item = new List<UserPermission>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	t1.UserId,RoleName,RoleThName,RoleEnName,t2.CreateOn
										FROM	TBUserCompany t1 INNER JOIN TBRoleUsers t2 ON t1.CompanyID=t2.CompanyID AND t1.UserID=t2.UserID
												INNER JOIN TBRoles t3 ON t2.RoleID=t3.RoleID
										WHERE	t1.CompanyID=@CompanyId AND t1.UserID=@UserId
										";
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					UserPermission user = null;
					while (reader.Read())
					{
						user = new UserPermission();
						user.UserId = (string)reader["UserId"];
						user.RoleName = (User.UserRole)Enum.Parse(typeof(User.UserRole), (string)reader["RoleName"], true);
						user.DisplayRoleName = new LocalizedString((string)reader["RoleThName"]);
						user.DisplayRoleName["en"] = (string)reader["RoleEnName"];
						user.CreateOn = (DateTime)reader["CreateOn"];
						item.Add(user);
					}
				}
			}
			return item;

		}
		public IEnumerable<UserPermission> GetUserReqOrAppPermission(string companyId)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			return GetPermission("", companyId, "");
		}
		public IEnumerable<UserPermission> GetUserReqOrAppPermission(string userId, string companyId)
		{
			if (userId == null) { throw new ArgumentNullException("userId"); }
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			return GetPermission(userId, companyId, "");
		}
		public IEnumerable<UserPermission> GetUserReqOrAppPermission(string userId, string companyId, string costcenterId)
		{
			if (userId == null) { throw new ArgumentNullException("userId"); }
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			return GetPermission(userId, companyId, costcenterId);
		}
		public void SetUserReqLineAndApp(SetRequesterLine setRequesterline)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"delete TBRequesterLine
										where companyId=@companyId and CostcenterID=@CostcenterID and ReqUserID=@ReqUserID and APPUserID=@APPUserID

										insert TBRequesterLine (companyId,CostcenterID,ReqUserID,APPUserID,APPLevel,APPCreditlimit,ParkDay,CreateOn,CreateBy,UpdateOn,UpdateBy)
										VALUES(@companyId,@CostcenterID,@ReqUserID,@APPUserID,@APPLevel,@APPCreditlimit,@ParkDay,getdate(),@CreateBy,getdate(),@UpdateBy)";
				command.Parameters.AddWithValue("@companyId", setRequesterline.CompanyId);
				command.Parameters.AddWithValue("@CostcenterID", setRequesterline.CostcenterId);
				command.Parameters.AddWithValue("@ReqUserID", setRequesterline.RequesterUserId);
				command.Parameters.AddWithValue("@APPUserID", setRequesterline.ApproverUserId);
				command.Parameters.AddWithValue("@APPLevel", setRequesterline.AppLevel);
				command.Parameters.AddWithValue("@APPCreditlimit", setRequesterline.AppCreditlimit);
				command.Parameters.AddWithValue("@ParkDay", setRequesterline.Parkday);
				command.Parameters.AddWithValue("@CreateBy", setRequesterline.CreateBy);
				command.Parameters.AddWithValue("@UpdateBy", setRequesterline.UpdateBy);
				command.ExecuteNonQuery();
			}
		}


		public void ClearReqLineAndAppByUser(string companyId, string costCenterId, string reqUserId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"delete TBRequesterLine
										where companyId=@companyId and CostcenterID=@CostcenterID and ReqUserID=@ReqUserID ";
				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@CostcenterID", costCenterId);
				command.Parameters.AddWithValue("@ReqUserID", reqUserId);

				command.ExecuteNonQuery();
			}
		}
		public IEnumerable<InformationRequesterLine> GetDataRequesterline(string companyId, string costcenterId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				List<InformationRequesterLine> item = new List<InformationRequesterLine>();
				command.CommandText = @"SELECT	t3.UserGuid,t2.UserID,t2.UserTName,t2.UserEName,CompanyID,CostcenterID,
												APPUserID,t4.UserTName as AppTName,t4.UserEName as AppEName,
												APPLevel,APPCreditlimit,ParkDay 
										FROM	TBRequesterLine t1 INNER JOIN TBUserInfo t2 ON t1.ReqUserID=t2.UserId
												INNER JOIN TBUsers t3 ON t2.UserId=t3.UserId
												INNER JOIN TBUserInfo t4 ON t1.APPUserID=t4.UserID
										WHERE	t1.CompanyID=@CompanyID AND CostcenterID=@CostcenterID";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@CostcenterID", costcenterId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					InformationRequesterLine requesterLine = null;
					while (reader.Read())
					{
						requesterLine = new InformationRequesterLine();
						requesterLine.ApproverId = (string)reader["AppUserId"];
						requesterLine.ApproverThaiName = (string)reader["AppTName"];
						requesterLine.ApprovweEngName = (string)reader["AppEName"];
						requesterLine.ApproverName = new LocalizedString((string)reader["AppTName"]);
						requesterLine.ApproverName["en"] = (string)reader["AppEName"];
						requesterLine.RequesterGuid = (string)reader["UserGuid"];
						requesterLine.RequesterId = (string)reader["UserID"];
						requesterLine.RequesterThaiName = (string)reader["UserTName"];
						requesterLine.RequesterEngName = (string)reader["UserEName"];
						requesterLine.RequesterName = new LocalizedString((string)reader["UserTName"]);
						requesterLine.RequesterName["en"] = (string)reader["UserEName"];
						requesterLine.ApproverLevel = (int)reader["AppLevel"];
						requesterLine.ApproverCreditBudget = (decimal)reader["AppCreditLimit"];
						requesterLine.Parkday = (int)reader["Parkday"];
						item.Add(requesterLine);
					}
				}
				return item;
			}
		}
		public IEnumerable<InformationRequesterLine> GetDataRequesterlineForUserSide(string companyId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				List<InformationRequesterLine> item = new List<InformationRequesterLine>();
				command.CommandText = @"SELECT	t2.UserGuid,t2.UserID,t1.CompanyID,t4.CostcenterID,t4.CostTName,t4.CostEName,
												APPUserID,t3.UserTName as AppTName,t3.UserEName as AppEName,
												APPLevel,APPCreditlimit,ParkDay 
										FROM	TBRequesterLine t1 INNER JOIN TBUsers t2 ON t1.ReqUserID=t2.UserId
												INNER JOIN TBUserInfo t3 ON t1.APPUserID=t3.UserID
												INNER JOIN TBCostcenter t4 ON t1.CostcenterID=t4.CostcenterID AND t1.CompanyID=t4.CompanyID
										WHERE	t4.CostStatus=@CostStatus AND t1.CompanyID=@CompanyID AND t1.ReqUserID=@UserId";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CostStatus", CostCenter.CostCenterStatus.Active.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					InformationRequesterLine requesterLine = null;
					while (reader.Read())
					{
						requesterLine = new InformationRequesterLine();
						requesterLine.ApproverId = (string)reader["AppUserId"];
						requesterLine.ApproverThaiName = (string)reader["AppTName"];
						requesterLine.ApprovweEngName = (string)reader["AppEName"];
						requesterLine.ApproverName = new LocalizedString((string)reader["AppTName"]);
						requesterLine.ApproverName["en"] = (string)reader["AppEName"];
						requesterLine.RequesterGuid = (string)reader["UserGuid"];
						requesterLine.RequesterId = (string)reader["UserID"];
						requesterLine.CostcenterID = (string)reader["CostcenterID"];
						requesterLine.CostcenterName = new LocalizedString((string)reader["CostTName"]);
						requesterLine.CostcenterName["en"] = (string)reader["CostEName"];
						requesterLine.ApproverLevel = (int)reader["AppLevel"];
						requesterLine.ApproverCreditBudget = (decimal)reader["AppCreditLimit"];
						requesterLine.Parkday = (int)reader["Parkday"];
						item.Add(requesterLine);
					}
				}
				return item;
			}
		}
		private IEnumerable<UserPermission> GetPermission(string userId, string companyId, string coscenterId)
		{
			List<UserPermission> item = new List<UserPermission>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	DISTINCT ReqUserID AS UserId,t4.CostcenterID,CostTName,CostEName,t4.CustID,t2.RoleName,
												t2.RoleThName,t2.RoleEnName,t1.CreateOn,0 as AppLevel,0 as Parkday,0 as AppCreditlimit,
												UserTName,UserEName
										FROM	TBRoleUsers t1 with (nolock) INNER JOIN TBRoles t2 with (nolock) ON t1.RoleID=t2.RoleID AND t2.RoleName=@Requester
												INNER JOIN TBRequesterLine t3 with (nolock) ON t3.ReqUserID=t1.UserID AND t1.CompanyID=t3.CompanyID
												INNER JOIN TBCostcenter t4 with (nolock) ON t3.CostcenterID=t4.CostcenterID AND t3.CompanyID=t4.CompanyID
												INNER JOIN TBUserInfo t5 with (nolock) ON t3.ReqUserID = t5.UserID	
										WHERE	t1.CompanyID=@CompanyId AND t4.CostStatus=@CostStatus ";
				if (!string.IsNullOrEmpty(userId))
				{
					command.CommandText += " AND  t1.UserID=@UserId ";
				}
				if (!string.IsNullOrEmpty(coscenterId))
				{
					command.CommandText += " AND  t3.CostcenterID=@coscenterId";
				}
				command.CommandText += @" UNION 
										SELECT	DISTINCT APPUserID AS UserId,t4.CostcenterID,CostTName,CostEName,t4.CustID,t2.RoleName,
												t2.RoleThName,t2.RoleEnName,t1.CreateOn,AppLevel,Parkday,AppCreditlimit,UserTName,UserEName
										FROM	TBRoleUsers t1 with (nolock) INNER JOIN TBRoles t2 with (nolock) ON t1.RoleID=t2.RoleID AND t2.RoleName=@Approver
												INNER JOIN TBRequesterLine t3 with (nolock) ON t3.APPUserID=t1.UserID AND t1.CompanyID=t3.CompanyID
												INNER JOIN TBCostcenter t4 with (nolock) ON t3.CostcenterID=t4.CostcenterID AND t3.CompanyID=t4.CompanyID
												INNER JOIN TBUserInfo t5 with (nolock) ON t3.AppUserID = t5.UserID	
										WHERE	t1.CompanyID=@CompanyId AND t4.CostStatus=@CostStatus";
				if (!string.IsNullOrEmpty(userId))
				{
					command.CommandText += " AND  t1.UserID=@UserId";
					command.Parameters.AddWithValue("@UserId", userId);
				}
				if (!string.IsNullOrEmpty(coscenterId))
				{
					command.CommandText += " AND  t3.CostcenterID=@coscenterId";
					command.Parameters.AddWithValue("@coscenterId", coscenterId);
				}

				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CostStatus", CostCenter.CostCenterStatus.Active.ToString());
				command.Parameters.AddWithValue("@Requester", User.UserRole.Requester.ToString());
				command.Parameters.AddWithValue("@Approver", User.UserRole.Approver.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					UserPermission user = null;
					while (reader.Read())
					{
						user = new UserPermission();
						user.CostCenter = new CostCenter();

						user.CustId = (string)reader["CustID"];
						user.CostCenter.CostCenterID = (string)reader["CostCenterID"];
						user.CostCenter.CostCenterEngName = (string)reader["CostEname"];
						user.CostCenter.CostCenterThaiName = (string)reader["CostTName"];
						user.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
						user.CostCenter.CostCenterName["en"] = (string)reader["CostEname"];
						user.UserId = (string)reader["UserId"];
						user.RoleName = (User.UserRole)Enum.Parse(typeof(User.UserRole), (string)reader["RoleName"], true);
						user.DisplayRoleName = new LocalizedString((string)reader["RoleThName"]);
						user.DisplayRoleName["en"] = (string)reader["RoleEnName"];
						user.DisplayUserName = new LocalizedString((string)reader["UserTName"]);
						user.DisplayUserName["en"] = (string)reader["UserEName"];
						user.CreateOn = (DateTime)reader["CreateOn"];
						user.AppLevel = (int)reader["AppLevel"];
						user.Parkday = (int)reader["Parkday"];
						user.AppCreditlimit = (decimal)reader["AppCreditlimit"];
						item.Add(user);
					}
				}
			}
			return item;
		}
		public Contact GetUserDataContact(string userId, string companyId)
		{
			if (string.IsNullOrEmpty(userId)) { throw new ArgumentNullException("userId"); }
			if (string.IsNullOrEmpty(companyId)) { throw new ArgumentNullException("companyId"); }
			Contact contact = GetUserContactEmail(userId, companyId);
			contact.Phone = GetUserContactPhoneAndFax(userId, companyId, Contact.TypeNumber.PhoneOut);
			contact.Mobile = GetUserContactPhoneAndFax(userId, companyId, Contact.TypeNumber.Mobile);
			contact.Fax = GetUserContactPhoneAndFax(userId, companyId, Contact.TypeNumber.FaxOut);
			return contact;
		}
		private Contact GetUserContactEmail(string userId, string companyId)
		{
			if (string.IsNullOrEmpty(companyId)) { throw new ArgumentNullException("userId"); }
			if (string.IsNullOrEmpty(companyId)) { throw new ArgumentNullException("companyId"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"DECLARE @DefaultCustID varchar(10)
										SET		@DefaultCustID = (Select t2.DefaultCustID FROM TBUserCompany t1 INNER JOIN TBCompanyInfo t2 ON t1.CompanyID=t2.CompanyID WHERE UserID=@ContactEmail AND t1.CompanyID=@CompanyID)

										Select	ContactID,ContactEmail
										FROM	TBCustContactEmail
										where	ContactEmail = @ContactEmail AND CustID=@DefaultCustID";
				command.Parameters.AddWithValue("@ContactEmail", userId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					Contact contact = new Contact();
					if (reader.Read())
					{
						contact.ContactID = (int)reader["ContactID"];
						contact.Email = (string)reader["ContactEmail"];
					}
					return contact;
				}
			}
		}
		public IEnumerable<UserApprover> GetListUserApprover(string companyId, string costcenterId, string userId)
		{
			List<UserApprover> item = new List<UserApprover>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	users.UserGuID,AppUserID,UserTName,UserEName,AppLevel,AppCreditLimit,ParkDay,IsByPassApprover
										FROM	TBRequesterLine Req inner join TBUserInfo Info on Req.AppUserID = Info.UserID
												inner join TBUsers users on Info.UserID= users.UserID
												inner join TBCompanyInfo comp on Req.CompanyID = comp.CompanyID
												INNER JOIN TBUserCompany userComp ON Info.UserID=userComp.UserID AND Req.CompanyID=userComp.CompanyID
										Where	ReqUserID=@UserID and Req.CompanyID = @CompanyID and Req.CostCenterId = @CostCenterId and userComp.Userstatus = @Userstatus
												ORDER BY APPLevel";

				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@CostCenterId", costcenterId);
				command.Parameters.AddWithValue("@Userstatus", User.UserStatus.Active.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{

					UserApprover user = null;
					while (reader.Read())
					{
						user = new UserApprover();
						user.Approver = new User();
						user.Approver.UserGuid = (string)reader["UserGuID"];
						user.Approver.UserId = (string)reader["AppUserID"];
						user.Approver.UserThaiName = (string)reader["UserTName"];
						user.Approver.UserEngName = (string)reader["UserEName"];
						user.Level = (int)reader["AppLevel"];
						user.ParkDay = (int)reader["ParkDay"];
						user.ApproveCreditLimit = (decimal)reader["AppCreditLimit"];
						user.IsByPassApprover = Mapper.ToClass<bool>((string)reader["IsByPassApprover"]);
						item.Add(user);
					}
				}
			}
			return item;

		}
		private Phone GetUserContactPhoneAndFax(string userId, string companyId, Contact.TypeNumber typeNumber)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"DECLARE @DefaultCustID varchar(10)
										SET		@DefaultCustID = (Select t2.DefaultCustID FROM TBUserCompany t1 INNER JOIN TBCompanyInfo t2 ON t1.CompanyID=t2.CompanyID WHERE UserID=@ContactEmail AND t1.CompanyID=@companyId)

										Select	Id, SequenceId, PhoneNo, Extension, TypeNumber, TypedataSource, t3.IsDefault
										FROM	TBCustContactEmail t1
												INNER JOIN TBCustContact t2 ON t2.CustID = @DefaultCustID AND t1.ContactID = t2.ContactID
												INNER Join TBCustomerPhone t3 ON t3.CustID = @DefaultCustID AND t2.ContactID = t3.SequenceID
										where	t1.ContactEmail = @ContactEmail AND TypedataSource = @Typedata AND TypeNumber = @TypeNumber";
				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@ContactEmail", userId);
				command.Parameters.AddWithValue("@Typedata", Contact.TypedataSource.Contact.ToString());
				command.Parameters.AddWithValue("@TypeNumber", typeNumber.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					Phone phone = null;
					if (reader.Read())
					{
						phone = new Phone();
						phone.Id = (int)reader["Id"];
						phone.SequenceId = (int)reader["SequenceId"];
						phone.PhoneNo = (string)reader["PhoneNo"];
						phone.Extension = (string)reader["Extension"];
						phone.TypeNumber = (Contact.TypeNumber)Enum.Parse(typeof(Contact.TypeNumber), (string)reader["TypeNumber"], true);
					}
					return phone;
				}
			}
		}
		public void UpdateUserName(EditProfileUser editUser, string updateBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBUserInfo set UserTName=@UserThaiName, UserEName=@UserEngName, UpdateOn=getdate(), UpdateBy=@UpdateBy  Where UserId=@UserId
										Update TBCustContact set ContactName=@UserThaiName, UpdateOn=getdate(), UpdateBy='EproV5'  Where ContactID=@ContactID";
				command.Parameters.AddWithValue("@UserId", editUser.Email);
				command.Parameters.AddWithValue("@ContactID", editUser.SequenceId);
				command.Parameters.AddWithValue("@UserThaiName", editUser.ThaiName);
				command.Parameters.AddWithValue("@UserEngName", editUser.EngName);
				command.Parameters.AddWithValue("@UpdateBy", updateBy);
				command.ExecuteNonQuery();
			}
		}
		public void ResetRequestline(string costcenterId, string companyId, string UserId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Delete TBRequesterLine  Where ReqUserId=@ReqUserId and CompanyId = @CompanyId and costcenterId = @costcenterId";
				command.Parameters.AddWithValue("@ReqUserId", UserId);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@costcenterId", costcenterId);
				command.ExecuteNonQuery();
			}
		}
		public void UpdateUserImage(byte[] image, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBUserInfo set ProfileImage=@ProfileImage Where UserId=@UserId";
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@ProfileImage", image);
				command.ExecuteNonQuery();
			}
		}
		public void RemoveUserImage(string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBUserInfo set ProfileImage=CONVERT(VARBINARY(MAX),'') Where UserId=@UserId";
				command.Parameters.AddWithValue("@UserId", userId);
				command.ExecuteNonQuery();
			}
		}
		public bool IsUserInSystem(string userId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT UserID FROM TBUserCompany Where UserID=@UserID AND CompanyID=@CompanyID";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					return reader.Read();
				}
			}
		}

		//public bool IsUserInOFMSystem(string userId)
		//{
		//    using (SqlCommand command = CreateCommandEnsureConnectionOpen())
		//    {
		//        command.CommandText = @"SELECT UserId FROM DBWebOFM..TBUserInfo where UserId=@UserId";
		//        command.Parameters.AddWithValue("@UserID", userId);

		//        using (SqlDataReader reader = command.ExecuteReader())
		//        {
		//            return reader.Read();
		//        }
		//    }
		//}

		//public void DeleteUserInOFM(string userId)
		//{
		//    using (SqlCommand command = CreateCommandEnsureConnectionOpen())
		//    {
		//        command.CommandText = "DELETE FROM DBWebOFM..TBUserInfo  WHERE UserID=@UserId";
		//        command.Parameters.AddWithValue("@UserId", userId);
		//        command.ExecuteNonQuery();
		//    }
		//}

		public bool IsOnlyAdmin(string userId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{

				command.CommandText = @"SELECT	t3.RoleID as RoleID,t3.RoleName
										FROM	TBUserCompany t1 INNER JOIN TBRoleUsers T2 ON t1.CompanyID=T2.CompanyID AND t1.UserID=T2.UserID 
												INNER JOIN TBRoles T3 on T2.RoleID = T3.RoleID
										Where	T1.UserID=@UserID AND T1.CompanyID=@CompanyID";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						if (reader["RoleID"].ToString().Count() == 1 && reader["RoleName"].ToString().Equals(User.UserRole.RootAdmin.ToString())) { return true; }
						if (reader["RoleID"].ToString().Count() == 1 && reader["RoleName"].ToString().Equals(User.UserRole.AssistantAdmin.ToString())) { return true; }
					}
				}
			}
			return false;
		}
		public bool IsOnlyObserve(string userId, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{

				command.CommandText = @"select t2.RoleID as RoleID,t2.RoleName  from TBRoleUsers T1 inner join TBRoles T2 on T1.RoleID = T2.RoleID Where UserID = @UserID and CompanyID = @CompanyID";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						if (reader["RoleID"].ToString().Count() == 1 && reader["RoleName"].ToString().Equals(User.UserRole.Observer.ToString())) { return true; }
					}
				}
			}
			return false;
		}
		public bool IsOFMAdmin(string userGuid)
		{
			if (string.IsNullOrEmpty(userGuid)) { return false; }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.UserID
										from	TBUsers t1 inner join TBUserCompany t2 on t1.UserID = t2.UserID
												inner join TBRoleUsers t3 on t1.UserID=t3.UserID AND t2.CompanyID=t3.CompanyID
												inner join TBRoles t4 on t3.RoleID=t4.RoleID
										where	t4.RoleName=@RoleName and t1.UserGuid=@UserGuid and t2.CompanyID='Officemate'";
				command.Parameters.AddWithValue("@UserGuid", userGuid);
				command.Parameters.AddWithValue("@RoleName", User.UserRole.OFMAdmin.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					return reader.Read();
				}
			}
		}
		public bool IsForceChangePassword(string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	DATEDIFF (day, NextChangePassword, getdate()) As NextChangePassword
										From	TBUserInfo
										Where	UserID = @UserID";
				command.Parameters.AddWithValue("@UserID", userId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						if ((int)reader["NextChangePassword"] >= 0) { return true; }
					}
				}
			}
			return false;
		}
		public bool IsUserMultiCompany(string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	count(t1.CompanyID) AS CountCompany
										FROM	TBUserCompany t1 INNER JOIN TBCompanyInfo t2 ON t1.CompanyID=t2.CompanyID
										WHERE	t2.CompStatus=@CompStatus AND t1.UserID=@UserID AND t1.UserStatus=@UserStatus";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CompStatus", Company.CompanyStatus.Active.ToString());
				command.Parameters.AddWithValue("@UserStatus", User.UserStatus.Active.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						if ((int)reader["CountCompany"] > 1) { return true; }
					}
				}
			}
			return false;
		}
		public bool IsPasswordCorrect(string userId, string password)
		{
			string userGuId = "";
			bool haveDefaultCompany;
			return TryGetUserGuid(userId, password, out userGuId, out haveDefaultCompany);
		}
		public void UpdateLastActive(string userId)
		{
			if (string.IsNullOrEmpty(userId)) { throw new ArgumentNullException("userId"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "update TBUserInfo set LastActive=getdate() where userid=@UserId";
				command.Parameters.AddWithValue("@UserId", userId);
				command.ExecuteNonQuery();
			}
		}
		public void UpdateChangPassword(string userId, string newPassword)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{

				command.CommandText = @"
										DECLARE @RePasswordPeriod int
										SET		@RePasswordPeriod = (select top 1 RePasswordPeriod 
										From	TBUserInfo T1 inner join TBUserCompany T2 on T1.UserID = T2.UserID and IsDefaultCompany= @IsDefault
												inner join TBCompanyInfo T3 on T2.companyID = T3.CompanyID
										Where	T1.UserID =  @UserID)
										Update TBUserInfo Set ChangePasswordOn = getdate(),NextChangePassword  = dateadd(day,@RePasswordPeriod,getdate())  Where userid=@UserId
										Update TBusers Set Password = @newPassword Where  userid=@UserId ";
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@IsDefault", User.UserCompanyDefault.Yes.ToString());
				command.Parameters.AddWithValue("@newPassword", new SaltedSha256PasswordHasher().HashPassword(newPassword));
				command.ExecuteNonQuery();
			}
		}
		private void ClearUserCache(string userGuid)
		{
			if (!String.IsNullOrEmpty(userGuid))
			{
				_cacheKey = string.Format(_cacheKey, userGuid);
				HostingEnvironment.Cache.Remove(_cacheKey);
			}
		}
		public void UpdateUserLanguage(string userId, User.Language language)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "Update TBUserInfo set UserLang=@Language where UserID=@UserId";
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@Language", language.ToString());
				command.ExecuteNonQuery();
			}
		}
		public Image byteArrayToImage(byte[] byteArrayIn)
		{
			try
			{
				MemoryStream ms = new MemoryStream(byteArrayIn);
				Image returnImage = Image.FromStream(ms);
				return returnImage;
			}
			catch (Exception)
			{
				return null;
			}
		}
		public bool VerifyUserForgotPassword(string userGuid, string verifyKey)
		{
			if (userGuid == null) { throw new ArgumentNullException("userGuid"); }
			if (verifyKey == null) { throw new ArgumentNullException("verifyKey"); }

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select VerifyKey from TBUserInfo t1 inner join TBUsers t2 on t1.UserID=T2.UserID
										where t2.UserGuid=@UserGuid and datediff(day,ForgotPasswordOn,getdate()) <= 1
										";
				command.Parameters.AddWithValue("@UserGuid", userGuid);
				string verifyKeyFromDB = (string)command.ExecuteScalar();
				if (String.Compare(verifyKeyFromDB, verifyKey, false) == 0)
				{
					return true;
				}
				return false;
			}
		}
		public bool VerifyUser(string userGuid, string verifyKey)
		{
			if (userGuid == null) { throw new ArgumentNullException("userGuid"); }
			if (verifyKey == null) { throw new ArgumentNullException("verifyKey"); }

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select VerifyKey from TBUserInfo t1 inner join TBUsers t2 on t1.UserID=T2.UserID
										where t2.UserGuid=@UserGuid
										";
				command.Parameters.AddWithValue("@UserGuid", userGuid);
				string verifyKeyFromDB = (string)command.ExecuteScalar();
				if (String.Compare(verifyKeyFromDB, verifyKey, false) == 0)
				{
					UpdateVerifyUser(userGuid);
					return true;
				}
				return false;
			}
		}
		private void UpdateVerifyUser(string userGuid)
		{
			if (userGuid == null) { throw new ArgumentNullException("userGuid"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"UPDATE	TBUserInfo SET IsVerified='Yes'
										WHERE	UserID=(SELECT UserID FROM TBUsers WHERE UserGuid=@UserGuid)";
				command.Parameters.AddWithValue("@UserGuid", userGuid);
				command.ExecuteNonQuery();
			}
		}

		public void UpdateIsDefaultCompanyUser(string userId, string orderguid) //ไว้อัพเดท DefaultCompany ของ User  เพื่อ ให้เข้าไปถูกตาม Flow  เนื่องจากเป็น User multicompany  มีสิทธิไม่เท่ากัน
		{
			if (userId == null) { throw new ArgumentNullException("userId"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"		Update TBUserCompany SET IsDefaultCompany = 'No' where UserID = @UserID
												Update TBUserCompany SET IsDefaultCompany = 'Yes' where UserID = @UserID AND  CompanyId IN
												(
													SELECT CompanyId FROM tborder WHERE orderguid = @orderguid
												)";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@orderguid", orderguid);
				command.ExecuteNonQuery();
			}
		}

		public bool SetPasswordAuthentication(string userGuid, string password, string verifyKey, bool isFirst)
		{
			if (String.IsNullOrEmpty(userGuid)) { throw new ArgumentNullException("userGuid"); }
			if (String.IsNullOrEmpty(password)) { throw new ArgumentNullException("verifyKey"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.UserId from TBUserInfo t1 inner join TBUsers t2 on t1.UserId=t2.UserId
										where	t2.UserGuid=@UserGuid and t1.VerifyKey=@VerifyKey ";
				if (!isFirst) { command.CommandText += " and datediff(day,ForgotPasswordOn,getdate()) <= 1"; }

				command.Parameters.AddWithValue("@UserGuid", userGuid);
				command.Parameters.AddWithValue("@Verifykey", verifyKey);
				string userId = (string)command.ExecuteScalar();
				if (!string.IsNullOrEmpty(userId))
				{
					UpdateChangPassword(userId, password);
					return true;
				}
				return false;
			}
		}
		public List<BusinessType> GetBusinessDetail()
		{
			List<BusinessType> ListBusinessType = new List<BusinessType>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBCustBusinessType order by BusinessTypeName";
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						BusinessType BusinessType = new BusinessType();
						BusinessType.id = (string)reader["BusinessTypeID"];
						BusinessType.Name = (string)reader["BusinessTypeName"];
						ListBusinessType.Add(BusinessType);
					}
					return ListBusinessType;
				}
			}
		}
		public bool TryForgotPasswordUser(string userId, out string verifyKey)
		{
			if (userId == null) { throw new ArgumentNullException("UserId"); }
			verifyKey = String.Empty;

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string hashedUserId = new SaltedSha256PasswordHasher().HashPassword(userId);
				verifyKey = new VerifyHasher().GenerateVerifyKey(hashedUserId);
				command.CommandText = @"update TBUserinfo set VerifyKey=@Verifykey, ForgotPasswordOn=getdate() Where UserId = @UserId";
				command.Parameters.AddWithValue("@Verifykey", verifyKey);
				command.Parameters.AddWithValue("@UserId", userId);
				return ((int)command.ExecuteNonQuery() > 0);
			}
		}
		public ForgotPasswordData GetForgotPasswordData(string userId)
		{
			if (userId == null) { throw new ArgumentNullException("UserId"); }
			ForgotPasswordData data = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t2.UserGuID, t1.UserID, t1.UserTName, t1.UserEName, t1.VerifyKey
										from	TBUserInfo t1 inner join TBUsers t2 on t1.UserId = t2.UserId
										where	t1.UserId=@UserId";
				command.Parameters.AddWithValue("@UserId", userId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						data = new ForgotPasswordData();
						data.UserGuid = (string)reader["UserGuID"];
						data.UserId = (string)reader["UserID"];
						data.UserName = new LocalizedString((string)reader["UserTName"]);
						data.UserName["en"] = (string)reader["UserEName"];
						data.VerifyKey = (string)reader["VerifyKey"];
					}
					return data;
				}
			}
		}
		public bool IsVerfyUser(string userId)
		{
			if (userId == null) { throw new ArgumentNullException("UserId"); }

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	IsVerified FROM TBUserInfo Info 
												inner JOIN TBUserCompany UserComp ON Info.UserID = UserComp.UserID
										WHERE	Info.UserID=@UserId AND UserComp.UserStatus = @UserStatus";
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@UserStatus", User.UserStatus.Active.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return Mapper.ToClass<bool>((string)reader["IsVerified"]);
					}
					return false;
				}
			}
		}
		public bool TryVerifyUser(string userId, out string verifyKey)
		{
			if (userId == null) { throw new ArgumentNullException("UserId"); }
			verifyKey = String.Empty;

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string hashedUserId = new SaltedSha256PasswordHasher().HashPassword(userId);
				verifyKey = new VerifyHasher().GenerateVerifyKey(hashedUserId);
				command.CommandText = @"Update TBUserinfo set VerifyKey=@Verifykey Where UserId = @UserId";
				command.Parameters.AddWithValue("@Verifykey", verifyKey);
				command.Parameters.AddWithValue("@UserId", userId);
				return ((int)command.ExecuteNonQuery() > 0);
			}
		}
		public NewSiteData GetCustDataFromMasterByCustID(string custId)
		{
			if (string.IsNullOrEmpty(custId)) { throw new ArgumentNullException("CustId"); }
			NewSiteData data = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.CustId, t1.CustTName, t1.CustEName, t1.DiscountRate, t1.InvAddr1, t1.InvAddr2, t1.InvAddr3, t1.InvAddr4,
												t2.ShipId, t2.ShipAddr1, t2.ShipAddr2, t2.ShipAddr3, t2.ShipAddr4, t2.ShipProvince, t2.ShipZipcode,
												t3.ContactId, t3.ContactName, t4.ContactEmail,
												isnull(t5.ID,0) AS PhoneId, isnull(t5.PhoneNo,'') AS PhoneNo, isnull(t5.Extension,'') AS Extention, 
												isnull(t6.ID,0) AS MobileId, isnull(t6.PhoneNo,'') AS Mobile, 
												isnull(t7.ID,0) AS FaxId, isnull(t7.PhoneNo,'') AS FaxNo
										from	TBCustMaster t1 INNER JOIN TBCustShipping t2 ON t1.CustId=t2.CustId and t2.IsDefault='Yes'
												INNER JOIN TBCustContact t3 ON t1.CustId=t3.CustId AND t3.IsDefault='Yes'
												INNER JOIN TBCustContactEmail t4 ON t1.CustId=t4.CustId AND t3.ContactId=t4.ContactId
												LEFT JOIN TBCustomerPhone t5 ON t1.CustId=t5.CustId AND t3.ContactId=t5.SequenceID AND t5.TypedataSource = @Typedata AND t5.TypeNumber = @PhoneOut
												LEFT JOIN TBCustomerPhone t6 ON t1.CustId=t6.CustId AND t3.ContactId=t6.SequenceID AND t6.TypedataSource = @Typedata AND t6.TypeNumber = @Mobile
												LEFT JOIN TBCustomerPhone t7 ON t1.CustId=t7.CustId AND t3.ContactId=t7.SequenceID AND t7.TypedataSource = @Typedata AND t7.TypeNumber = @FaxOut
										where	t1.CustId=@CustId
										";
				command.Parameters.AddWithValue("@CustId", custId);
				command.Parameters.AddWithValue("@Mobile", Contact.TypeNumber.Mobile.ToString());
				command.Parameters.AddWithValue("@PhoneOut", Contact.TypeNumber.PhoneOut.ToString());
				command.Parameters.AddWithValue("@FaxOut", Contact.TypeNumber.FaxOut.ToString());
				command.Parameters.AddWithValue("@Typedata", Contact.TypedataSource.Contact.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						data = new NewSiteData();
						data.IsMember = true;
						data.CustId = (string)reader["CustId"];
						data.CompanyTName = (string)reader["CustTName"];
						data.CompanyEName = (string)reader["CustEName"];
						data.DiscountRate = (decimal)reader["DiscountRate"];
						data.InvoiceAddress = new Invoice();
						data.InvoiceAddress.Address1 = (string)reader["InvAddr1"];
						data.InvoiceAddress.Address2 = (string)reader["InvAddr2"];
						data.InvoiceAddress.Address3 = (string)reader["InvAddr3"];
						data.InvoiceAddress.Address4 = (string)reader["InvAddr4"];
						data.ShippingAddress = new Shipping();
						data.ShippingAddress.ShipID = (int)reader["ShipId"];
						data.ShippingAddress.Address1 = (string)reader["ShipAddr1"];
						data.ShippingAddress.Address2 = (string)reader["ShipAddr2"];
						data.ShippingAddress.Address3 = (string)reader["ShipAddr3"];
						data.ShippingAddress.Address4 = (string)reader["ShipAddr4"];
						data.ShippingAddress.Province = (string)reader["ShipProvince"];
						data.ShippingAddress.ZipCode = (string)reader["ShipZipcode"];
						data.UserID = (string)reader["ContactEmail"];
						data.UserTName = (string)reader["ContactName"];
						data.SequenceId = (int)reader["ContactId"];
						data.PhoneId = (int)reader["PhoneId"];
						data.PhoneNumber = (string)reader["PhoneNo"];
						data.ExtentionPhoneNumber = (string)reader["Extention"];
						data.MobileId = (int)reader["MobileId"];
						data.MobileNumber = (string)reader["Mobile"];
						data.FaxId = (int)reader["FaxId"];
						data.FaxNumber = (string)reader["FaxNo"];
					}
				}
			}
			return data;
		}
		public void CreateUserForNewSite(NewSiteData newSiteData, string userId)
		{
			if (!UserExistEproUser(newSiteData.UserID))
			{
				InsertUserInfo(newSiteData.UserID, newSiteData.UserTName, newSiteData.UserEName, newSiteData.DefaultLang, User.UserStatus.Active, userId);
			}
			InsertUserCompany(newSiteData.UserID, newSiteData.CompanyId, userId);
			InsertRoleUsers(newSiteData.UserID, User.UserRole.RootAdmin, newSiteData.CompanyId, userId);
			if (newSiteData.IsRequester) { InsertRoleUsers(newSiteData.UserID, User.UserRole.Requester, newSiteData.CompanyId, userId); }
			if (newSiteData.IsApprover) { InsertRoleUsers(newSiteData.UserID, User.UserRole.Approver, newSiteData.CompanyId, userId); }

		}
		public void CreateNewUser(CreateNewUser createNewUser, string userId)
		{
			User.Language lang = (User.Language)Enum.Parse(typeof(User.Language), createNewUser.DefaultLang, true);
			if (!UserExistEproUser(createNewUser.Email))
			{
				InsertUserInfo(createNewUser.Email, createNewUser.ThaiName, createNewUser.EngName, lang, User.UserStatus.Active, userId);
			}
			InsertUserCompany(createNewUser.Email, createNewUser.DefaultCompanyId, userId);
			if (createNewUser.IsRequester) { InsertRoleUsers(createNewUser.Email, User.UserRole.Requester, createNewUser.DefaultCompanyId, userId); }
			if (createNewUser.IsApprover) { InsertRoleUsers(createNewUser.Email, User.UserRole.Approver, createNewUser.DefaultCompanyId, userId); }
			if (createNewUser.IsAssistantAdmin) { InsertRoleUsers(createNewUser.Email, User.UserRole.AssistantAdmin, createNewUser.DefaultCompanyId, userId); }
			if (createNewUser.IsObserve) { InsertRoleUsers(createNewUser.Email, User.UserRole.Observer, createNewUser.DefaultCompanyId, userId); }
		}
		private void InsertUserInfo(string userId, string userTName, string userEName, User.Language userLang, User.UserStatus userStatus, string createBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBUserInfo (UserId, UserTName, UserEName, UserLang, ProfileImage, CreateOn, CreateBy,IsVerified)
										values (@UserId, @UserTName, @UserEName, @UserLang, 0, getdate(), @CreateBy,'Yes')";

				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@UserTName", userTName);
				command.Parameters.AddWithValue("@UserEName", userEName);
				command.Parameters.AddWithValue("@UserLang", userLang.ToString());
				command.Parameters.AddWithValue("@CreateBy", createBy);
				command.ExecuteNonQuery();
			}
		}
		private void InsertRoleUsers(string userId, User.UserRole roleName, string companyId, string createBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"declare @RoleId as Varchar(6) set @RoleId=(select RoleId from TBRoles where RoleName=@RoleName)
										
										insert into TBRoleUsers (RoleId, UserId, CompanyId, CreateOn, CreateBy)
										values (@RoleId, @UserId, @CompanyId, getdate(), @CreateBy)";
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@RoleName", roleName.ToString());
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CreateBy", createBy);
				command.ExecuteNonQuery();
			}
		}
		private void InsertUserCompany(string userId, string companyId, string createBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"update TBUserCompany set IsDefaultCompany=@IsdefaultNo where UserId=@UserId

										insert into TBUserCompany (UserId, CompanyId, IsDefaultCompany, CreateOn, CreateBy)
										values (@UserId, @CompanyId, @IsdefaultYes, getdate(), @CreateBy)
										";
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CreateBy", createBy);
				command.Parameters.AddWithValue("@IsdefaultYes", User.UserCompanyDefault.Yes.ToString());
				command.Parameters.AddWithValue("@IsdefaultNo", User.UserCompanyDefault.No.ToString());
				command.ExecuteNonQuery();
			}
		}
		public void InsertUserMaster(string userId, string password)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string hashedPassword = new SaltedSha256PasswordHasher().HashPassword(password);
				command.CommandText = @"INSERT INTO TBUsers (UserId,Password,UserGuid) VALUES (@UserId,@Password,@UserGuid)";
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@Password", hashedPassword);
				command.Parameters.AddWithValue("@UserGuid", Guid.NewGuid().ToString());
				command.ExecuteNonQuery();
			}
		}
		public EditProfileUser GetUserDataForEdit(string userGuid, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				EditProfileUser data = null;
				command.CommandText = @"SELECT	t2.UserID,UserTName,UserEName,UserLang,CustID,ContactID,t3.UserStatus
										FROM	TBUsers t1 INNER JOIN TBUserInfo t2 ON t1.UserId=t2.UserID
												INNER JOIN TBUserCompany t3 ON t2.UserId = t3.UserID
												INNER JOIN TBCompanyInfo t4 ON t3.CompanyID = t4.CompanyID
												INNER JOIN TBCustContactEmail t5 ON t2.UserID=t5.ContactEmail AND t4.DefaultCustID=t5.CustID
										WHERE	t1.UserGuid=@UserGuid AND t3.CompanyID=@CompanyID
										";
				command.Parameters.AddWithValue("@UserGuid", userGuid);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						data = new EditProfileUser();
						data.UserGuid = userGuid;
						data.Email = (string)reader["UserID"];
						data.ThaiName = (string)reader["UserTName"];
						data.EngName = (string)reader["UserEName"];
						data.DefaultLang = Enum.GetName(typeof(User.Language), Enum.Parse(typeof(User.Language), (string)reader["UserLang"]));
						data.CustId = (string)reader["CustID"];
						data.SequenceId = (int)reader["ContactID"];
						data.Status = (User.UserStatus)Enum.Parse(typeof(User.UserStatus), (string)reader["UserStatus"], true);
						data.CurrentCompanyId = companyId;
						reader.Close();

						Phone phone = GetUserContactPhoneAndFax(data.Email, companyId, Contact.TypeNumber.PhoneOut);
						data.PhoneId = phone != null ? phone.Id : 0;
						data.Phone = phone != null ? phone.PhoneNo : "";
						data.PhoneExt = phone != null ? phone.Extension : "";
						Phone mobile = GetUserContactPhoneAndFax(data.Email, companyId, Contact.TypeNumber.Mobile);
						data.MobileId = mobile != null ? mobile.Id : 0;
						data.Mobile = mobile != null ? mobile.PhoneNo : "";
						Phone fax = GetUserContactPhoneAndFax(data.Email, companyId, Contact.TypeNumber.FaxOut);
						data.FaxId = fax != null ? fax.Id : 0;
						data.Fax = fax != null ? fax.PhoneNo : "";

						IEnumerable<UserPermission> permission = GetUserPermission(data.Email, companyId);
						data.IsApprover = permission.Any(p => p.RoleName == User.UserRole.Approver);
						data.IsRequester = permission.Any(p => p.RoleName == User.UserRole.Requester);
						data.IsAssistantAdmin = permission.Any(p => p.RoleName == User.UserRole.AssistantAdmin);
						data.IsObserve = permission.Any(p => p.RoleName == User.UserRole.Observer);
						data.IsRootAdmin = permission.Any(p => p.RoleName == User.UserRole.RootAdmin);
					}
				}
				return data;
			}
		}
		public void UpdateUserStatus(string userGuid, User.UserStatus status, string updateBy, string companyId)
		{

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"
										UPDATE TBUserCompany SET UserStatus=@Status, UpdateOn=getdate(), UpdateBy=@UpdateBy
										WHERE UserID=(SELECT UserId FROM TBUsers WHERE UserGuid=@UserGuid) AND CompanyID=@CompanyID
										";
				command.Parameters.AddWithValue("@UserGuid", userGuid);
				command.Parameters.AddWithValue("@Status", status.ToString());
				command.Parameters.AddWithValue("@UpdateBy", updateBy);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.ExecuteNonQuery();
			}
		}

		public void UpdateIsDefaultUserStatus(string userGuid, User.UserStatus status, string updateBy, string companyId)
		{

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"
										UPDATE TBUserCompany SET IsDefaultCompany='No', UpdateOn=getdate(), UpdateBy=@UpdateBy
										WHERE UserID=(SELECT UserId FROM TBUsers WHERE UserGuid=@UserGuid)

										UPDATE TBUserCompany
										SET
										 IsDefaultCompany = 'Yes'
										FROM
										 TBUserCompany
										 INNER JOIN (SELECT TOP 1 UserID
												 , usercomp.CompanyID
											FROM
											 TBUserCompany usercomp
											 INNER JOIN TBCompanyInfo comp
											  ON usercomp.CompanyID = comp.CompanyID
											WHERE
											 UserID = (SELECT UserId
												 FROM
												  TBUsers
												 WHERE
												  UserGuid = @UserGuid)
											 AND UserStatus = 'Active'
											 AND CompStatus = 'Active') DefaultCompany
										  ON TBUserCompany.UserID = DefaultCompany.UserID AND TBUserCompany.CompanyID = DefaultCompany.CompanyID

										";
				command.Parameters.AddWithValue("@UserGuid", userGuid);
				command.Parameters.AddWithValue("@Status", status.ToString());
				command.Parameters.AddWithValue("@UpdateBy", updateBy);
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.ExecuteNonQuery();
			}
		}

		public bool UserExistMasterUser(string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(*) as UserCount from TBUsers where UserId = @UserId";
				command.Parameters.AddWithValue("@UserId", userId);
				return ((int)command.ExecuteScalar() > 0);
			}
		}
		public bool UserExistEproUser(string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(*) as UserCount from TBUserinfo where UserId = @UserId";
				command.Parameters.AddWithValue("@UserId", userId);
				return ((int)command.ExecuteScalar() > 0);
			}
		}
		public void EditRoleUserFromAdmin(EditProfileUser data, string updateBy)
		{
			DeleteAllRoleUser(data.Email, data.CurrentCompanyId, updateBy);
			if (data.IsRequester) { InsertRoleUsers(data.Email, User.UserRole.Requester, data.CurrentCompanyId, updateBy); }
			if (data.IsApprover) { InsertRoleUsers(data.Email, User.UserRole.Approver, data.CurrentCompanyId, updateBy); }
			if (data.IsAssistantAdmin) { InsertRoleUsers(data.Email, User.UserRole.AssistantAdmin, data.CurrentCompanyId, updateBy); }
			if (data.IsObserve) { InsertRoleUsers(data.Email, User.UserRole.Observer, data.CurrentCompanyId, updateBy); }
		}
		private void DeleteAllRoleUser(string userId, string companyId, string updateBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "DELETE FROM TBRoleUsers WHERE RoleID <> (SELECT RoleID FROM TBRoles WHERE RoleName=@RoleName) AND UserID=@UserId AND CompanyID=@CompanyId";
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@RoleName", User.UserRole.RootAdmin.ToString());
				command.ExecuteNonQuery();
			}
		}
		public string GetUserIdFromUserGuid(string userGuid)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT t1.UserId FROM TBUsers t1
										INNER JOIN TBUserInfo t2 ON t1.UserId=t2.UserID
										WHERE t1.UserGuID=@userGuid";
				command.Parameters.AddWithValue("@userGuid", userGuid);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (string)reader["UserId"];
					}
					return string.Empty;
				}
			}
		}
		public bool CustIdExistMasterUser(string custId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(*) as UserCount from TBCustMaster where CustID = @custId";
				command.Parameters.AddWithValue("@custId", custId);
				return ((int)command.ExecuteScalar() > 0);
			}
		}

		public IEnumerable<SetRequesterLine> GetAllRequesterLineBySearch(SearchRequesterLine searchRequesterLine)
		{
			List<SetRequesterLine> requesterLines = new List<SetRequesterLine>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder();
				sqlBuilder.AppendLine("SELECT	t1.*,t3.UserGuid,t2.UserID,t2.UserTName,t2.UserEName,t4.UserTName as AppTName,");
				sqlBuilder.AppendLine("t4.UserEName as AppEName,t5.CostTName,t5.CostEName FROM TBRequesterLine t1 with (nolock)");
				sqlBuilder.AppendLine("INNER JOIN TBUserInfo t2 with (nolock) ON t1.ReqUserID=t2.UserId");
				sqlBuilder.AppendLine("INNER JOIN TBUsers t3 with (nolock) ON t2.UserId=t3.UserId");
				sqlBuilder.AppendLine("INNER JOIN TBUserInfo t4 with (nolock) ON t1.APPUserID=t4.UserID");
				sqlBuilder.AppendLine("INNER JOIN TBCostcenter t5 with (nolock) ON t1.CostcenterID=t5.CostcenterID AND t1.CompanyID=t5.CompanyID");
				sqlBuilder.AppendLine("WHERE 1=1");

				if (searchRequesterLine.CheckCompanyId && !string.IsNullOrEmpty(searchRequesterLine.CompanyId))
				{
					sqlBuilder.AppendLine(" AND t1.CompanyID LIKE @CompanyID");
					command.Parameters.AddWithValue("@CompanyID", "%" + searchRequesterLine.CompanyId + "%");
				}
				if (searchRequesterLine.CheckCostcenterId && !string.IsNullOrEmpty(searchRequesterLine.CostcenterId))
				{
					sqlBuilder.AppendLine(" AND t1.CostcenterID LIKE @CostcenterID");
					command.Parameters.AddWithValue("@CostcenterID", "%" + searchRequesterLine.CostcenterId + "%");
				}
				if (searchRequesterLine.CheckRequesterUserId && !string.IsNullOrEmpty(searchRequesterLine.RequesterUserId))
				{
					sqlBuilder.AppendLine(" AND ReqUserID=@ReqUserID");
					command.Parameters.AddWithValue("@ReqUserID", searchRequesterLine.RequesterUserId);
				}
				if (searchRequesterLine.CheckApproverUserId && !string.IsNullOrEmpty(searchRequesterLine.ApproverUserId))
				{
					sqlBuilder.AppendLine(" AND APPUserID=@APPUserID");
					command.Parameters.AddWithValue("@APPUserID", searchRequesterLine.ApproverUserId);
				}
				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						SetRequesterLine requesterLine = new SetRequesterLine();
						requesterLine.CompanyId = (string)reader["CompanyId"];
						requesterLine.CostcenterId = (string)reader["CostcenterId"];
						requesterLine.RequesterUserId = (string)reader["UserID"];
						requesterLine.ApproverUserId = (string)reader["APPUserID"];
						requesterLine.AppLevel = (int)reader["APPLevel"];
						requesterLine.Parkday = (int)reader["ParkDay"];
						requesterLine.AppCreditlimit = (decimal)reader["APPCreditlimit"];
						requesterLine.CreateOn = (DateTime)reader["CreateOn"];
						requesterLine.CreateBy = (string)reader["CreateBy"];
						requesterLine.UpdateOn = (DateTime)reader["UpdateOn"];
						requesterLine.UpdateBy = (string)reader["UpdateBy"];
						requesterLine.CostCenter = new CostCenter();
						requesterLine.CostCenter.CostCenterThaiName = (string)reader["CostTName"];
						requesterLine.CostCenter.CostCenterEngName = (string)reader["CostEName"];
						requesterLine.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTname"]);
						requesterLine.CostCenter.CostCenterName["en"] = (string)reader["CostEname"];
						requesterLine.UserReqDetail = new User();
						requesterLine.UserReqDetail.UserThaiName = (string)reader["UserTName"];
						requesterLine.UserReqDetail.UserEngName = (string)reader["UserEName"];
						requesterLine.UserReqDetail.DisplayName = new LocalizedString((string)reader["UserTName"]);
						requesterLine.UserReqDetail.DisplayName["en"] = (string)reader["UserEName"];
						requesterLine.ApproverThaiName = (string)reader["AppTName"];
						requesterLine.ApproverEngName = (string)reader["AppEName"];
						requesterLine.ApproverName = new LocalizedString((string)reader["AppTName"]);
						requesterLine.ApproverName["en"] = (string)reader["AppEName"];
						requesterLines.Add(requesterLine);
					}
				}
			}
			return requesterLines;
		}

		public IEnumerable<Payment> GetPaymentType()
		{
			List<Payment> payments = new List<Payment>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "SELECT PaymentCode, PaymentType, PaymentName from TBCustPayment WHERE PaymentType NOT IN ('LINE Pay')";

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{

						Payment payment = new Payment();
						payment.Code = (PaymentCode)Mapper.ToClass((string)reader["PaymentCode"], typeof(PaymentCode));
						payment.Type = (PaymentType)Mapper.ToClass((string)reader["PaymentType"], typeof(PaymentType));
						payment.Name = (string)reader["PaymentName"];
						payment.NameInDB = (string)reader["PaymentType"];
						payments.Add(payment);
					}
					return payments;
				}
			}
		}

		public NewCustCompany GetCustCompany(string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	cust.CustID,comp.*,mast.InvAddr1,mast.InvAddr2,mast.InvAddr3,mast.InvAddr4,
										userComp.UserId,RoleName,RoleThName,RoleEnName,UserTName,UserEName
										FROM	TBCustCompany cust
												INNER JOIN TBCompanyInfo comp ON cust.CustID=comp.DefaultCustID AND cust.CompanyId=comp.CompanyId
												INNER JOIN TBCustMaster mast ON comp.DefaultCustID = mast.CustID
												INNER JOIN TBUserCompany userComp ON comp.CompanyID = userComp.CompanyID
												INNER JOIN TBRoleUsers roleUser ON userComp.CompanyID = roleUser.CompanyID AND userComp.UserID=roleUser.UserID
												INNER JOIN TBRoles roles ON roleUser.RoleID=roles.RoleID
												INNER JOIN TBUserInfo Info ON userComp.UserID = Info.UserID
										Where	comp.CompanyId = @CompanyId AND RoleName=@RoleName AND cust.IsDefault='Yes'
												AND userComp.UserStatus=@UserStatus AND comp.CompStatus=@CompStatus AND userComp.IsDefaultCompany='Yes'";

				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@RoleName", User.UserRole.RootAdmin.ToString());
				command.Parameters.AddWithValue("@UserStatus", User.UserStatus.Active.ToString());
				command.Parameters.AddWithValue("@CompStatus", Company.CompanyStatus.Active.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					NewCustCompany data = new NewCustCompany();
					if (reader.Read())
					{
						data = new NewCustCompany();
						data.CompanyId = (string)reader["CompanyId"];
						data.CompanyThaiName = (string)reader["CompTName"];
						data.CompanyEngName = (string)reader["CompEName"];
						data.CompanyName = new LocalizedString((string)reader["CompTName"]);
						data.CompanyName["en"] = (string)reader["CompEName"];
						data.DefaultCustId = (string)reader["DefaultCustId"];
						data.UserId = (string)reader["UserId"];
						data.UserThaiName = (string)reader["UserTName"];
						data.UserEngName = (string)reader["UserEName"];
						data.DisplayName = new LocalizedString((string)reader["UserTName"]);
						data.DisplayName["en"] = (string)reader["UserEName"];
						data.InvAddr1 = (string)reader["InvAddr1"];
						data.InvAddr2 = (string)reader["InvAddr2"];
						data.InvAddr3 = (string)reader["InvAddr3"];
						data.InvAddr4 = (string)reader["InvAddr4"];
					}
					return data;
				}
			}
		}

		public bool IsDefaultEmailForCustId(string custId, string email)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(*) as UserCount from TBCustContactEmail where CustID = @custId and ContactEmail = @email and IsDefaultEmail = 'Yes'";
				command.Parameters.AddWithValue("@custId", custId);
				command.Parameters.AddWithValue("@email", email);
				return ((int)command.ExecuteScalar() > 0);
			}
		}

		public bool IsEmailInCustId(string custId, string email)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(*) as UserCount from TBCustContactEmail where CustID = @custId and ContactEmail = @email";
				command.Parameters.AddWithValue("@custId", custId);
				command.Parameters.AddWithValue("@email", email);
				return ((int)command.ExecuteScalar() > 0);
			}
		}

		public IEnumerable<CreateNewUser> GetUserForCustId(string custId, string companyId, string email)
		{
			List<CreateNewUser> users = new List<CreateNewUser>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	DISTINCT contact.ContactName,custMail.ContactEmail,custComp.CustId,
												isnull(PhoneOut.PhoneNo,'') AS PhoneNo, isnull(PhoneOut.Extension,'') AS Extention, 
												isnull(Mobile.PhoneNo,'') AS Mobile, 
												isnull(FaxOut.PhoneNo,'') AS FaxNo
										FROM	TBCustCompany custComp
												INNER JOIN TBUserCompany userComp ON custComp.CompanyId=userComp.CompanyID
												INNER JOIN TBCustContactEmail custMail ON userComp.UserID=custMail.ContactEmail AND custComp.CustID=custMail.CustID
												INNER JOIN TBCustContact contact ON contact.CustID=custMail.CustID AND contact.ContactID=custMail.ContactID
												LEFT JOIN TBCustomerPhone PhoneOut ON custComp.CustId=PhoneOut.CustId AND contact.ContactId=PhoneOut.SequenceID AND PhoneOut.TypedataSource = @Typedata AND PhoneOut.TypeNumber = @PhoneOut
												LEFT JOIN TBCustomerPhone Mobile ON custComp.CustId=Mobile.CustId AND contact.ContactId=Mobile.SequenceID AND Mobile.TypedataSource = @Typedata AND Mobile.TypeNumber = @Mobile
												LEFT JOIN TBCustomerPhone FaxOut ON custComp.CustId=FaxOut.CustId AND contact.ContactId=FaxOut.SequenceID AND FaxOut.TypedataSource = @Typedata AND FaxOut.TypeNumber = @FaxOut
										WHERE	custComp.CustId = @CustId AND custComp.CompanyId = @CompanyId AND custMail.ContactEmail <> @ContactEmail";

				command.Parameters.AddWithValue("@CustId", custId);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@ContactEmail", email);
				command.Parameters.AddWithValue("@Mobile", Contact.TypeNumber.Mobile.ToString());
				command.Parameters.AddWithValue("@PhoneOut", Contact.TypeNumber.PhoneOut.ToString());
				command.Parameters.AddWithValue("@FaxOut", Contact.TypeNumber.FaxOut.ToString());
				command.Parameters.AddWithValue("@Typedata", Contact.TypedataSource.Contact.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						CreateNewUser user = new CreateNewUser();
						user.ThaiName = (string)reader["ContactName"];
						user.Email = (string)reader["ContactEmail"];
						user.Phone = (string)reader["PhoneNo"];
						user.PhoneExt = (string)reader["Extention"];
						user.Mobile = (string)reader["Mobile"];
						user.Fax = (string)reader["FaxNo"];
						user.DefaultCustId = (string)reader["CustId"];
						users.Add(user);
					}
				}
			}
			return users;
		}

		public IEnumerable<CreateNewUser> GetUserInfoAndPhoneInCompany(string companyId, string email)
		{
			List<CreateNewUser> users = new List<CreateNewUser>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	DISTINCT contact.ContactID,contact.ContactName,custMail.ContactEmail,custComp.CustId,
												isnull(PhoneOut.ID,0) AS PhoneId,isnull(PhoneOut.PhoneNo,'') AS PhoneNo,isnull(PhoneOut.Extension,'') AS Extention,
												isnull(Mobile.ID,0) AS MobileId,isnull(Mobile.PhoneNo,'') AS Mobile,
												isnull(FaxOut.ID,0) AS FaxId,isnull(FaxOut.PhoneNo,'') AS FaxNo
										FROM	TBCustCompany custComp
												INNER JOIN TBUserCompany userComp ON custComp.CompanyId=userComp.CompanyID
												INNER JOIN TBCustContactEmail custMail ON userComp.UserID=custMail.ContactEmail AND custComp.CustID=custMail.CustID
												INNER JOIN TBCustContact contact ON contact.CustID=custMail.CustID AND contact.ContactID=custMail.ContactID
												LEFT JOIN TBCustomerPhone PhoneOut ON custComp.CustId=PhoneOut.CustId AND contact.ContactId=PhoneOut.SequenceID AND PhoneOut.TypedataSource = @Typedata AND PhoneOut.TypeNumber = @PhoneOut
												LEFT JOIN TBCustomerPhone Mobile ON custComp.CustId=Mobile.CustId AND contact.ContactId=Mobile.SequenceID AND Mobile.TypedataSource = @Typedata AND Mobile.TypeNumber = @Mobile
												LEFT JOIN TBCustomerPhone FaxOut ON custComp.CustId=FaxOut.CustId AND contact.ContactId=FaxOut.SequenceID AND FaxOut.TypedataSource = @Typedata AND FaxOut.TypeNumber = @FaxOut
										WHERE	custComp.CompanyId = @CompanyId AND custMail.ContactEmail=@ContactEmail";

				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@ContactEmail", email);
				command.Parameters.AddWithValue("@Mobile", Contact.TypeNumber.Mobile.ToString());
				command.Parameters.AddWithValue("@PhoneOut", Contact.TypeNumber.PhoneOut.ToString());
				command.Parameters.AddWithValue("@FaxOut", Contact.TypeNumber.FaxOut.ToString());
				command.Parameters.AddWithValue("@Typedata", Contact.TypedataSource.Contact.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						CreateNewUser user = new CreateNewUser();
						user.ContactId = (int)reader["ContactID"];
						user.ThaiName = (string)reader["ContactName"];
						user.Email = (string)reader["ContactEmail"];
						user.Phone = (string)reader["PhoneNo"];
						user.PhoneExt = (string)reader["Extention"];
						user.Mobile = (string)reader["Mobile"];
						user.Fax = (string)reader["FaxNo"];
						user.DefaultCustId = (string)reader["CustId"];
						user.PhoneId = (int)reader["PhoneId"];
						user.MobileId = (int)reader["MobileId"];
						user.FaxId = (int)reader["FaxId"];
						users.Add(user);
					}
				}
			}
			return users;
		}


		public void UpdateRootAdmin(ModelViewListUserData data, string companyId, string userId)
		{
			StringBuilder sqlBuilder = new StringBuilder(2048);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (data.UserData.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin))
				{
					sqlBuilder.AppendLine(@"UPDATE TBRoleUsers SET RoleID='4' 
											WHERE CompanyID=@CompanyID AND RoleID='3'");
				}

				sqlBuilder.AppendLine(@"UPDATE TBRoleUsers SET RoleID='3'
										where CompanyID=@CompanyID AND UserID=@UserID AND RoleID='4'");

				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@UserID", userId);
				command.ExecuteNonQuery();

			}
		}

		public void AddRootAdminProcessLog(ProcessLog process, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"INSERT INTO TBAdminProcessLog (CompanyId, ProcessRemark, Remark, ipAddress, CreateBy, CreateOn) 
										VALUES (@companyId, @processRemark, @remark, @ipAddress, @userId, getdate())
										";
				command.Parameters.AddWithValue("@companyId", process.CompanyId);
				command.Parameters.AddWithValue("@processRemark", process.ProcessRemark);
				command.Parameters.AddWithValue("@remark", process.Remark);
				command.Parameters.AddWithValue("@ipAddress", process.IPAddress);
				command.Parameters.AddWithValue("@userId", userId);
				command.ExecuteNonQuery();
			}
		}


		//ฟังก์ชันสำหรับดึงข้อมูล UserID ที่ผูกกับ CustID ของออฟฟิศเมท
//        public IEnumerable<UserOFM> GetUserIdInOFMSystem(string custId)
//        {
//            List<UserOFM> users = new List<UserOFM>();
//            using (SqlCommand command = CreateCommandEnsureConnectionOpen())
//            {
//                command.CommandText = @"SELECT email.CustID,info.UserId FROM TBCustContactEmail email 
//										INNER JOIN DBWebOFM..TBUserInfo info ON email.ContactEmail = info.UserId
//										WHERE email.CustID=@CustID";

//                command.Parameters.AddWithValue("@CustId", custId);

//                using (SqlDataReader reader = command.ExecuteReader())
//                {
//                    while (reader.Read())
//                    {
//                        UserOFM user = new UserOFM();
//                        user.CustID = (string)reader["CustID"];
//                        user.UserID = (string)reader["UserId"];
//                        users.Add(user);
//                    }
//                }
//                return users;
//            }
//        }


	}
}