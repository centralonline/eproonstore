﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public abstract class UserFilteredRepository : RepositoryBase
	{
		public UserFilteredRepository(SqlConnection connection, User user) : base(connection)
		{
			if (user == null) { throw new ArgumentNullException("user"); }
			User = user;
		}
		protected User User { get; set; }
	}
}