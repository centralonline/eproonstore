﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using OfficeMate.Framework;
using OfficeMate.Framework.CustomerData;
using OfficeMate.Framework.CustomerData.Repositories;


namespace Eprocurement2012.Models.Repositories
{
	public class CostCenterRepository : RepositoryBase
	{
		public CostCenterRepository(SqlConnection connection) : base(connection) { }

		public IEnumerable<CostCenter> GetMyCostCenter(string userId, string companyId, User.UserType userType)
		{
			if (userId == null) { throw new ArgumentNullException("userId"); }
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			List<CostCenter> item = new List<CostCenter>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				string sqlWhereCondition = "";
				switch (userType)
				{
					case User.UserType.Approver:
						sqlWhereCondition = "and AppUserID = @UserID";
						break;
					case User.UserType.Requester:
						sqlWhereCondition = "and ReqUserID = @UserID";
						break;
					case User.UserType.ReqAndApp:
						sqlWhereCondition = "and ReqUserID = @UserID Or AppUserID = @UserID ";
						break;
					default:
						sqlWhereCondition = "";
						break;
				}
				command.CommandText = @"select CostcenterId,CostTname,CostEname from TBCostcenter with (nolock)
										where CompanyId=@CompanyId and CostCenterID in(select CostCenterID from TBRequesterLine with (nolock) where CompanyId=@CompanyId " + sqlWhereCondition + " ) and CostStatus = @CostStatus";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CostStatus", CostCenter.CostCenterStatus.Active.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					CostCenter costcenter = null;
					while (reader.Read())
					{
						costcenter = new CostCenter();
						costcenter.CostCenterID = (string)reader["CostCenterID"];
						costcenter.CostCenterThaiName = (string)reader["CostTname"];
						costcenter.CostCenterEngName = (string)reader["CostEname"];
						costcenter.CostCenterName = new LocalizedString((string)reader["CostTname"]);
						costcenter.CostCenterName["en"] = (string)reader["CostEname"];
						item.Add(costcenter);
					}
				}
			}
			return item;
		}

		public IEnumerable<CostCenter> GetAllCostCenter(string companyId)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			List<CostCenter> item = new List<CostCenter>();

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.*,t2.InvAddr1,t2.InvAddr2,t2.InvAddr3,t2.InvAddr4,
												t2.Province,t2.ZipCode,t3.ShipAddr1,t3.ShipAddr2,t3.ShipAddr3,
												t3.ShipAddr4
										from	TBCostcenter t1 with (nolock)
												INNER JOIN TBCustMaster t2 with (nolock) ON t1.CustID = t2.CustID
												INNER JOIN TBCustShipping t3 with (nolock) ON t1.CustID = t3.CustID AND t1.ShipID = t3.ShipID
										where 	CompanyId=@CompanyId";
				command.Parameters.AddWithValue("@CompanyId", companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					CostCenter costcenter = null;
					while (reader.Read())
					{
						costcenter = new CostCenter();
						costcenter.CompanyId = (string)reader["CompanyId"];
						costcenter.CostCenterID = (string)reader["CostCenterID"];
						costcenter.CostCenterName = new LocalizedString((string)reader["CostTname"]);
						costcenter.CostCenterName["en"] = (string)reader["CostEname"];
						costcenter.CostCenterThaiName = (string)reader["CostTname"];
						costcenter.CostCenterEngName = (string)reader["CostEname"];
						costcenter.CostStatus = (CostCenter.CostCenterStatus)Enum.Parse(typeof(CostCenter.CostCenterStatus), (string)reader["CostStatus"], true);
						costcenter.UseByPassAdmin = Mapper.ToClass<bool>((string)reader["UseByPassAdmin"]);
						costcenter.UseAutoApprove = Mapper.ToClass<bool>((string)reader["UseAutoApprove"]);
						costcenter.UseByPassApprover = Mapper.ToClass<bool>((string)reader["UseByPassApprover"]);

						costcenter.IsDeliCharge = Mapper.ToClass<bool>((string)reader["UseDeliCharge"]);
						costcenter.CostCenterDepartment = new CompanyDepartment();
						costcenter.CostCenterDepartment.DepartmentID = (string)reader["DepartmentID"];
						costcenter.CostCenterCustID = (string)reader["CustID"];

						costcenter.CostCenterShipping = new Shipping();
						costcenter.CostCenterShipping.Address1 = (string)reader["ShipAddr1"];
						costcenter.CostCenterShipping.Address2 = (string)reader["ShipAddr2"];
						costcenter.CostCenterShipping.Address3 = (string)reader["ShipAddr3"];
						costcenter.CostCenterShipping.Address4 = (string)reader["ShipAddr4"];

						costcenter.CostCenterInvoice = new Invoice();
						costcenter.CostCenterInvoice.Address1 = (string)reader["InvAddr1"];
						costcenter.CostCenterInvoice.Address2 = (string)reader["InvAddr2"];
						costcenter.CostCenterInvoice.Address3 = (string)reader["InvAddr3"];
						costcenter.CostCenterInvoice.Address4 = (string)reader["InvAddr4"];

						item.Add(costcenter);
					}
				}
			}
			return item;
		}


		public void UpdateCostcenter(CostCenter costcenter, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update	TBCostcenter set CostTName=@CostTName, CostEName=@CostEName, CustId=@CustId, ShipId=@ShipId, CostStatus=@CostStatus,
												UseByPassAdmin=@UseByPassAdmin, UseAutoApprove=@UseAutoApprove,UseByPassApprover=@UseByPassApprover,UseDeliCharge=@UseDeliCharge,UpdateOn=getdate(), UpdateBy=@UpdateBy
										Where	CompanyId=@CompanyId and CostcenterId=@CostcenterId";
				command.Parameters.AddWithValue("@CostCenterID", costcenter.CostCenterID);
				command.Parameters.AddWithValue("@CompanyID", costcenter.CompanyId);
				command.Parameters.AddWithValue("@CostTName", costcenter.CostCenterThaiName);
				command.Parameters.AddWithValue("@CostEName", costcenter.CostCenterEngName);
				command.Parameters.AddWithValue("@CostStatus", costcenter.CostStatus.ToString());
				command.Parameters.AddWithValue("@UseByPassAdmin", Mapper.ToDatabase(costcenter.UseByPassAdmin));
				command.Parameters.AddWithValue("@UseAutoApprove", Mapper.ToDatabase(costcenter.UseAutoApprove));
				command.Parameters.AddWithValue("@UseByPassApprover", Mapper.ToDatabase(costcenter.UseByPassApprover));
				command.Parameters.AddWithValue("@UseDeliCharge", Mapper.ToDatabase(costcenter.IsDeliCharge));

				command.Parameters.AddWithValue("@CustID", costcenter.SelectInvoice);
				command.Parameters.AddWithValue("@ShipID", costcenter.SelectShipID);
				command.Parameters.AddWithValue("@UpdateBy", userId);
				command.ExecuteNonQuery();
			}
		}

		public void CreateCostcenter(CostCenter costcenter, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBCostcenter (CostCenterID, CompanyID, DepartmentID, CostTName, CostEName, CostStatus, UseByPassAdmin, UseAutoApprove,UseByPassApprover,UseDeliCharge, CustID, ShipID, CreateOn, CreateBy,OracleCode)
										values (@CostCenterID, @CompanyID, @DepartmentID, @CostTName, @CostEName, @CostStatus, @UseByPassAdmin, @UseAutoApprove,@UseByPassApprover,@UseDeliCharge, @CustID, @ShipID, getdate(), @CreateBy,@OracleCode)";
				command.Parameters.AddWithValue("@CompanyID", costcenter.CompanyId);
				command.Parameters.AddWithValue("@DepartmentID", costcenter.DepartmentID);
				command.Parameters.AddWithValue("@CostCenterID", costcenter.CostCenterID);
				command.Parameters.AddWithValue("@CostTName", costcenter.CostCenterThaiName);
				command.Parameters.AddWithValue("@CostEName", costcenter.CostCenterEngName);
				command.Parameters.AddWithValue("@CostStatus", costcenter.CostStatus.ToString());
				command.Parameters.AddWithValue("@UseByPassAdmin", Mapper.ToDatabase(costcenter.UseByPassAdmin));
				command.Parameters.AddWithValue("@UseAutoApprove", Mapper.ToDatabase(costcenter.UseAutoApprove));
				command.Parameters.AddWithValue("@UseByPassApprover", Mapper.ToDatabase(costcenter.UseByPassApprover));
				command.Parameters.AddWithValue("@UseDeliCharge", Mapper.ToDatabase(costcenter.IsDeliCharge));

				command.Parameters.AddWithValue("@CustID", costcenter.SelectInvoice);
				command.Parameters.AddWithValue("@ShipID", costcenter.SelectShipID);
				command.Parameters.AddWithValue("@CreateBy", userId);
				command.Parameters.AddWithValue("@OracleCode", costcenter.OracleCode);
				command.ExecuteNonQuery();
			}
		}
		public bool IsCostcenterIdInSystem(string companyId, string costcenterId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBCostcenter Where CompanyId = @CompanyId  and CostcenterId = @CostcenterId ";
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CostcenterId", costcenterId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}
		public CostCenter GetCostcenterDetail(string companyId, string costcenterId, string userId)
		{
			if (companyId == null) { throw new ArgumentException("companyId"); }
			if (costcenterId == null) { throw new ArgumentNullException("costcenterId"); }
			CostCenter costcenter = null;
			costcenter = GetCostcenter(companyId, costcenterId, false);
			costcenter.CostCenterShipping = GetCostcenterShipping(companyId, costcenterId);
			costcenter.CostCenterInvoice = GetCostCenterInvoice(costcenter.CostCenterCustID);
			costcenter.CostCenterContact = GetCostCenterContact(costcenter.CostCenterCustID, userId);
			return costcenter;
		}
		public CostCenter GetCostcenterDetailForAdmin(string companyId, string costcenterId)
		{
			if (companyId == null) { throw new ArgumentException("companyId"); }
			if (costcenterId == null) { throw new ArgumentNullException("costcenterId"); }
			CostCenter costcenter = null;
			costcenter = GetCostcenter(companyId, costcenterId, true);
			if (costcenter != null)
			{
				costcenter.CostCenterShipping = GetCostcenterShipping(companyId, costcenterId);
				costcenter.CostCenterInvoice = GetCostCenterInvoice(costcenter.CostCenterCustID);
			}
			return costcenter;
		}
		public Payment GetMyCostCenterPayment(string custId)
		{
			return GetCostCenterPayment(custId);
		}

		public IEnumerable<string> GetAllCostCenterForRequesterLine(string companyId, string term) 
		{
			List<string> costcenters = new List<string>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.CostcenterID,t1.CostTName,t1.CostEName
										from	TBCostcenter t1 with (nolock)
												INNER JOIN TBCustMaster t2 with (nolock) ON t1.CustID = t2.CustID
												INNER JOIN TBCustShipping t3 with (nolock) ON t1.CustID = t3.CustID AND t1.ShipID = t3.ShipID
										where 	CompanyId=@CompanyId AND (t1.CostcenterID LIKE @term OR t1.CostTName LIKE @term OR t1.CostEName LIKE @term)";

				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@term",term + "%");

				using(SqlDataReader reader = command.ExecuteReader())
				{
						while (reader.Read())
					{
							CostCenter costcenter = new CostCenter();
							costcenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
							costcenter.CostCenterName["en"] = (string)reader["CostEName"];
							costcenters.Add(string.Format("[{0}] {1}",(string)reader["CostcenterID"],costcenter.CostCenterName));
					}
				return costcenters.AsReadOnly();
				}
			}
		}

		private CostCenter GetCostcenter(string companyId, string costcenterId, bool allStatus)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			if (costcenterId == null) { throw new ArgumentNullException("costcenterId"); }
			CostCenter costcenter = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder(1024);
				sqlBuilder.AppendLine("select	t1.CustID,t2.CustTName,t2.CustEName,ShipID,CostCenterID,CostTname,CostEname,CostStatus,UseByPassAdmin,UseAutoApprove,UseByPassApprover,UseDeliCharge,");
				sqlBuilder.AppendLine("			t1.CompanyID,t1.DepartmentID,isnull(DeptTName,'') as DeptTName,isnull(DeptEname,'') as DeptEname,OracleCode");
				sqlBuilder.AppendLine("from		TBCostcenter t1 inner join TBCustMaster t2 on t1.CustID = t2.CustID");
				sqlBuilder.AppendLine("			left join TBDepartment t3 on t1.DepartmentId=t3.DepartmentId");
				sqlBuilder.AppendLine("where	t1.CompanyID=@CompanyId and CostCenterID=@CostcenterId");
				if (!allStatus)
				{
					sqlBuilder.AppendLine("and CostStatus=@CostStatus");
					command.Parameters.AddWithValue("@CostStatus", CostCenter.CostCenterStatus.Active.ToString());
				}
				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CostcenterId", costcenterId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						costcenter = new CostCenter();
						costcenter.CompanyId = (string)reader["CompanyID"];
						costcenter.DepartmentID = (string)reader["DepartmentID"];
						costcenter.CostCenterID = (string)reader["CostCenterID"];
						costcenter.CostCenterCustID = (string)reader["CustId"];
						costcenter.CostCenterThaiName = (string)reader["CostTname"];
						costcenter.CostCenterEngName = (string)reader["CostEname"];
						costcenter.CostCenterName = new LocalizedString((string)reader["CostTname"]);
						costcenter.CostCenterName["en"] = (string)reader["CostEname"];
						costcenter.CostCenterCustThaiName = (string)reader["CustTName"];
						costcenter.CostCenterCustEngName = (string)reader["CustEName"];
						costcenter.CostStatus = (CostCenter.CostCenterStatus)Enum.Parse(typeof(CostCenter.CostCenterStatus), (string)reader["CostStatus"], true);
						costcenter.UseByPassAdmin = Mapper.ToClass<bool>((string)reader["UseByPassAdmin"]);
						costcenter.UseAutoApprove = Mapper.ToClass<bool>((string)reader["UseAutoApprove"]);
						costcenter.UseByPassApprover = Mapper.ToClass<bool>((string)reader["UseByPassApprover"]);
						costcenter.OracleCode = (string)reader["OracleCode"];

						costcenter.IsDeliCharge = Mapper.ToClass<bool>((string)reader["UseDeliCharge"]);
						if (!string.IsNullOrEmpty((string)reader["DepartmentID"]))
						{
							costcenter.CostCenterDepartment = new CompanyDepartment();
							costcenter.CostCenterDepartment.DepartmentID = (string)reader["DepartmentID"];
							costcenter.CostCenterDepartment.DepartmentName = new LocalizedString((string)reader["DeptTName"]);
							costcenter.CostCenterDepartment.DepartmentName["en"] = (string)reader["DeptEname"];
							costcenter.CostCenterDepartment.DepartmentThaiName = (string)reader["DeptTName"];
							costcenter.CostCenterDepartment.DepartmentEngName = (string)reader["DeptEname"];
						}
					}
				}
			}
			return costcenter;
		}
		private Shipping GetCostcenterShipping(string companyId, string costcenterId)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			if (costcenterId == null) { throw new ArgumentNullException("costcenterId"); }
			Shipping shipping = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.CustId,CostCenterID,CostTname,CostEname,t2.ShipId,OfflineShipID,ShipContactor,ShipAddr1,ShipAddr2,ShipAddr3,ShipAddr4,
												ShipDistrict,ShipAmphur,ShipProvince,ShipZipCode,ShipRemark,isnull(t3.ID,'') as PhoneID,isnull(t3.PhoneNo,'') as PhoneNo,isnull(t3.Extension,'') as Extension
										from	TBCostcenter t1 inner join TBCustShipping t2 on t1.CustID = t2.CustID and t1.ShipID = t2.ShipID
												left join TBCustomerPhone t3 on t1.CustID = t3.CustID and t2.ShipID = t3.SequenceID and TypeNumber = @TypeNumber and TypeDataSource = @TypeData
										where	CostCenterID=@costcenterId and CompanyID=@companyId and ShipStatus=@ShipStatus";
				command.Parameters.AddWithValue("@costcenterId", costcenterId);
				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@ShipStatus", Shipping.ShippingAddressStatus.Active.ToString());
				command.Parameters.AddWithValue("@TypeNumber", Contact.TypeNumber.PhoneOut.ToString());
				command.Parameters.AddWithValue("@TypeData", Contact.TypedataSource.Shipping.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						shipping = new Shipping();
						shipping.ShipID = (int)reader["ShipID"];
						shipping.OfflineShipID = (int)reader["ShipID"];
						shipping.ShipContactor = (string)reader["ShipContactor"];
						shipping.PhoneID = (int)reader["PhoneID"];
						shipping.ShipPhoneNo = (string)reader["PhoneNo"];
						shipping.Extension = (string)reader["Extension"];
						shipping.ShipMobileNo = ""; // เตรียมไว้ก่อน มีเรื่อง SMS
						shipping.Address1 = (string)reader["ShipAddr1"];
						shipping.Address2 = (string)reader["ShipAddr2"];
						shipping.Address3 = (string)reader["ShipAddr3"];
						shipping.Address4 = (string)reader["ShipAddr4"];
						shipping.District = (string)reader["shipDistrict"];
						shipping.Amphur = (string)reader["ShipAmphur"];
						shipping.Province = (string)reader["ShipProvince"];
						shipping.ZipCode = (string)reader["ShipZipCode"];
						shipping.Remark = (string)reader["ShipRemark"];
					}
				}
			}
			return shipping;
		}
		private Invoice GetCostCenterInvoice(string custId)
		{
			if (custId == null) { throw new ArgumentNullException("custId"); }

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select InvAddr1,InvAddr2,InvAddr3,InvAddr4 from TBCustMaster Where CustID = @CustId";
				command.Parameters.AddWithValue("@CustId", custId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					Invoice invoice = null;
					if (reader.Read())
					{
						invoice = new Invoice();
						invoice.Address1 = (string)reader["InvAddr1"];
						invoice.Address2 = (string)reader["InvAddr2"];
						invoice.Address3 = (string)reader["InvAddr3"];
						invoice.Address4 = (string)reader["InvAddr4"];
					}
					return invoice;
				}
			}
		}
		private Contact GetCostCenterContact(string custId, string userId)
		{
			if (custId == null) { throw new ArgumentNullException("custId"); }

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	t1.ContactID, t1.OfflineContactID, 0 as ContactSeqNo, t1.ContactName, t2.ContactEmail,
												isnull(t3.PhoneNo,'') as ContactPhoneNo, isnull(t3.Extension,'') as ContactExtension,
												isnull(t4.PhoneNo,'') as ContactMobileNo, isnull(t5.PhoneNo,'') as ContactFaxNo
										FROM	TBCustContact t1 INNER JOIN TBCustContactEmail t2 ON t1.ContactID=t2.ContactID
												LEFT JOIN TBCustomerPhone t3 ON t1.ContactID=t3.SequenceID AND t3.TypeNumber=@PhoneOut AND t3.TypeDataSource=@Typedata
												LEFT JOIN TBCustomerPhone t4 ON t1.ContactID=t4.SequenceID AND t4.TypeNumber=@Mobile AND t4.TypeDataSource=@Typedata
												LEFT JOIN TBCustomerPhone t5 ON t1.ContactID=t5.SequenceID AND t5.TypeNumber=@FaxOut AND t5.TypeDataSource=@Typedata
										WHERE	t2.ContactEmail=@ContactEmail and t1.CustId=@CustId and t2.IsDefaultEmail=@IsDefaultEmail";
				command.Parameters.AddWithValue("@CustId", custId);
				command.Parameters.AddWithValue("@ContactEmail", userId);
				command.Parameters.AddWithValue("@IsDefaultEmail", Contact.Isdefault.Yes.ToString());
				command.Parameters.AddWithValue("@Mobile", Contact.TypeNumber.Mobile.ToString());
				command.Parameters.AddWithValue("@PhoneOut", Contact.TypeNumber.PhoneOut.ToString());
				command.Parameters.AddWithValue("@FaxOut", Contact.TypeNumber.FaxOut.ToString());
				command.Parameters.AddWithValue("@Typedata", Contact.TypedataSource.Contact.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					Contact contact = null;
					if (reader.Read())
					{
						contact = new Contact();
						contact.ContactID = (int)reader["ContactID"];
						contact.OfflineContactID = (int)reader["OfflineContactID"];
						contact.ContactSeqNo = (int)reader["ContactSeqNo"];
						contact.ContactorName = (string)reader["ContactName"];
						contact.ContactorPhone = (string)reader["ContactPhoneNo"];
						contact.ContactorExtension = (string)reader["ContactExtension"];
						contact.ContactMobileNo = (string)reader["ContactMobileNo"];
						contact.ContactorFax = (string)reader["ContactFaxNo"];
						contact.Email = (string)reader["ContactEmail"];
						contact.ContactMobileNo = (string)reader["ContactMobileNo"];
					}
					return contact;
				}
			}
		}
		private Payment GetCostCenterPayment(string custId)
		{
			PaymentType enumPaymentType = (PaymentType)Mapper.ToClass(GetPaymentType(custId), typeof(PaymentType));
			return GetPayment(enumPaymentType);
		}
		private string GetPaymentType(string custId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select PaymentType from TBCustMaster where CustID = @CustID";
				command.Parameters.AddWithValue("@CustID", custId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return (string)reader["PaymentType"];
					}
					return "";
				}
			}
		}
		private Payment GetPayment(PaymentType paymentType)
		{
			Payment payment = new Payment();
			payment.Type = paymentType;
			SqlConnection connection = base.Connection;
			payment.Code = new PaymentRepository(connection).GetPaymentCode(payment.Type);
			payment.Name = new PaymentRepository(connection).GetPaymentName(payment.Type);
			return payment;
		}
		public IEnumerable<Province> GetProvince()
		{
			List<Province> provinces = new List<Province>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "Select * from CmProvince order by ProvinceName";
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Province province = new Province();
						province.ProvinceId = (string)reader["ProvinceID"];
						province.ProvinceName = new LocalizedString((string)reader["ProvinceName"]);
						province.ProvinceName["en"] = (string)reader["ProvinceEName"];
						provinces.Add(province);
					}
					return provinces;
				}
			}
		}
		public IEnumerable<Invoice> GetAllInvoidAddress(string companyId)
		{
			if (string.IsNullOrEmpty(companyId)) { throw new ArgumentNullException("CompanyId"); }
			IEnumerable<Invoice> invAddr = GetAllInvoidAddr(companyId);
			if (invAddr.Count() > 0) { return invAddr; }
			else { return GetInvoidAddr(companyId); }
		}
		private IEnumerable<Invoice> GetAllInvoidAddr(string companyId) // ดึงข้อมูลจากตารางใหม่ 24/03/2014
		{
			List<Invoice> invoices = new List<Invoice>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	mast.*
										FROM	TBCompanyInfo comp
												INNER JOIN TBCustCompany cust ON comp.CompanyID = cust.CompanyID
												INNER JOIN TBCustMaster mast ON cust.CustID=mast.CustID
										WHERE	cust.CompanyID=@CompanyId";

				command.Parameters.AddWithValue("@CompanyId", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Invoice invoice = new Invoice();
						invoice.CustId = (string)reader["CustId"];
						invoice.Address1 = (string)reader["InvAddr1"];
						invoice.Address2 = (string)reader["InvAddr2"];
						invoice.Address3 = (string)reader["InvAddr3"];
						invoice.Address4 = (string)reader["InvAddr4"];
						invoices.Add(invoice);
					}
					return invoices;
				}
			}
		}
		private IEnumerable<Invoice> GetInvoidAddr(string companyId) // กรณีที่ยังไม่มีข้อมูลในตารางใหม่ กันไว้ก่อน
		{
			List<Invoice> invoices = new List<Invoice>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				//ดึงข้อมูล custId ที่ผูกอยู่กับ company
				command.CommandText = @"select * from TBCustMaster where CustId = (select DefaultCustID from TBCompanyInfo where CompanyId=@CompanyId)";

				command.Parameters.AddWithValue("@CompanyId", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Invoice invoice = new Invoice();
						invoice.CustId = (string)reader["CustId"];
						invoice.Address1 = (string)reader["InvAddr1"];
						invoice.Address2 = (string)reader["InvAddr2"];
						invoice.Address3 = (string)reader["InvAddr3"];
						invoice.Address4 = (string)reader["InvAddr4"];
						invoices.Add(invoice);
					}
					return invoices;
				}
			}
		}
		public IEnumerable<Shipping> GetAllShipAddress(string custId)
		{
			List<Shipping> items = new List<Shipping>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	T1.*,isnull(T2.ID,'') as PhoneID,isnull(T2.PhoneNo,'') as PhoneNo,isnull(T2.Extension,'') as Extension
										from	TBCustShipping T1 with (nolock) left JOIN TBCustomerPhone T2 with (nolock)
												ON T2.SequenceID=T1.ShipID and T1.CustID=T2.CustID  and TypeNumber = @TypeNumber and TypeDataSource = @TypeData
										Where	t1.CustID = @CustId and ShipStatus = @ShipStatus";
				command.Parameters.AddWithValue("@CustId", custId);
				command.Parameters.AddWithValue("@ShipStatus", Shipping.ShippingAddressStatus.Active.ToString());
				command.Parameters.AddWithValue("@TypeNumber", Contact.TypeNumber.PhoneOut.ToString());
				command.Parameters.AddWithValue("@TypeData", Contact.TypedataSource.Shipping.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Shipping shipping = new Shipping();
						shipping.ShipID = (int)reader["ShipID"];
						shipping.OfflineShipID = (int)reader["ShipID"];
						shipping.ShipContactor = (string)reader["ShipContactor"];
						shipping.PhoneID = (int)reader["PhoneID"];
						shipping.ShipPhoneNo = (string)reader["PhoneNo"];
						shipping.Extension = (string)reader["Extension"];
						shipping.ShipMobileNo = ""; // เตรียมไว้ก่อน มีเรื่อง SMS
						shipping.Address1 = (string)reader["ShipAddr1"];
						shipping.Address2 = (string)reader["ShipAddr2"];
						shipping.Address3 = (string)reader["ShipAddr3"];
						shipping.Address4 = (string)reader["ShipAddr4"];
						shipping.District = (string)reader["shipDistrict"];
						shipping.Amphur = (string)reader["ShipAmphur"];
						shipping.Province = (string)reader["ShipProvince"];
						shipping.ZipCode = (string)reader["ShipZipCode"];
						shipping.Remark = (string)reader["ShipRemark"];
						shipping.CustId = (string)reader["CustId"];
						items.Add(shipping);
					}
					return items;
				}
			}
		}
		public void RemoveCostCenter(string companyId, string costCenterId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update	TBCostcenter set CostStatus=@CostStatus, UpdateOn=getdate(), UpdateBy=@UpdateBy
										Where	CompanyId=@CompanyId and CostCenterId=@CostCenterId";
				command.Parameters.AddWithValue("@CostStatus", CostCenter.CostCenterStatus.Cancel.ToString());
				command.Parameters.AddWithValue("@UpdateBy", userId);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CostCenterId", costCenterId);
				command.ExecuteNonQuery();
			}
		}

		public int GetCountCostcenterWithoutRequesterLine(string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	count(*)
										FROM	(
												SELECT	DISTINCT tc.*,isnull(tr.CostcenterID,'') as ReqLine
												FROM	TBCostcenter tc LEFT JOIN TBRequesterLine tr ON tc.CompanyID=tr.CompanyID AND tc.CostcenterID=tr.CostcenterID
												WHERE	tc.CompanyID = @companyId AND CostStatus = @CostStatus
										) TR
										WHERE	ReqLine = ''
										";
				command.Parameters.AddWithValue("@CostStatus", CostCenter.CostCenterStatus.Active.ToString());
				command.Parameters.AddWithValue("@CompanyId", companyId);
				return (int)command.ExecuteScalar();
			}
		}

		public bool IsCostcenterDeliveryFee(string companyId, string costCenterId)
		{
			return GetCostcenter(companyId, costCenterId, false).IsDeliCharge;
		}

		public bool IsReqAndAppInOrder(string companyId, string costcenterId, string requesterId, string approverId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT * FROM TBOrder
										where	CompanyID=@CompanyID AND CostcenterID=@CostcenterID
												AND UserId = @requesterId
												AND (CurrentAppID=@approverId OR NextAppID=@approverId)
												AND OrderStatus IN (@Waiting,@WaitingAdmin,@Partial,@Revise,@AdminAllow)";

				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@CostcenterID", costcenterId);
				command.Parameters.AddWithValue("@requesterId", requesterId);
				command.Parameters.AddWithValue("@approverId", approverId);
				command.Parameters.AddWithValue("@Waiting", Order.OrderStatus.Waiting.ToString());
				command.Parameters.AddWithValue("@WaitingAdmin", Order.OrderStatus.WaitingAdmin.ToString());
				command.Parameters.AddWithValue("@AdminAllow", Order.OrderStatus.AdminAllow.ToString());
				command.Parameters.AddWithValue("@Partial", Order.OrderStatus.Partial.ToString());
				command.Parameters.AddWithValue("@Revise", Order.OrderStatus.Revise.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}

		//ฟังก์ชันสำหรับเช็ค ผู้ดูแลระบบ มี Order ที่ต้องทำการ Approve ค้างอยู่หรือไม่
		public bool IsAdminApproveInOrder(string companyId, string approverId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	CurrentAppID, NextAppID FROM TBOrder
										WHERE	CompanyID = @CompanyID
												AND ((CurrentAppID = @approverId AND OrderStatus = @WaitingAdmin)
												OR (NextAppID = @approverId AND OrderBeforeStatus = @WaitingAdmin AND OrderStatus = @Revise))";

				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@approverId", approverId);
				command.Parameters.AddWithValue("@WaitingAdmin", Order.OrderStatus.WaitingAdmin.ToString());
				command.Parameters.AddWithValue("@Revise", Order.OrderStatus.Revise.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return true;
					}
					else
					{
						return false;
					}
				}
			}
		}

	}

}