﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Mvc;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class ProvinceEproRepository : RepositoryBase
	{
		public ProvinceEproRepository(SqlConnection connection) : base(connection) { }

		public IEnumerable<SelectListItem> GetProvince()
		{
			List<SelectListItem> result = new List<SelectListItem>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select ProvinceID,ProvinceName from cmProvince";
				Province province = new Province();
				using (SqlDataReader reader = command.ExecuteReader())
				{

					while (reader.Read())
					{
						SelectListItem selectOption = new SelectListItem();
						selectOption.Value = (string)reader["ProvinceID"];
						selectOption.Text = (string)reader["ProvinceName"];

						if (selectOption.Value == "กรุงเทพฯ")
						{
							selectOption.Selected = true;
						}

						result.Add(selectOption);
					}
				}
			}
			return result;
		}

		public IEnumerable<SelectListItem> GetProvinceWithNoDefault()
		{
			List<SelectListItem> result = new List<SelectListItem>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select ProvinceID,ProvinceName from cmProvince";
				Province province = new Province();
				using (SqlDataReader reader = command.ExecuteReader())
				{

					while (reader.Read())
					{
						SelectListItem selectOption = new SelectListItem();
						selectOption.Value = (string)reader["ProvinceID"];
						selectOption.Text = (string)reader["ProvinceName"];

						result.Add(selectOption);
					}
				}
			}
			return result;
		}

		public bool GetDeliverFee(string province)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select provincetype from cmprovince where provinceId = @province";
				command.Parameters.AddWithValue("@province", province);
				var provincetype = (string)command.ExecuteScalar();
				if (provincetype == "1")
				{
					return false;
				}
				else
				{
					return true;
				}
			}
		}
	}

}