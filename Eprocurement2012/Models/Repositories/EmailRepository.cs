﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using Eprocurement2012.Controllers.Mails;
using System.Data;
using System.Text;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class EmailRepository : RepositoryBase
	{
		public EmailRepository(SqlConnection connection) : base(connection) { }

		public void AddNotifyProductMailLog(AbstractMailTemplate mail, MailType mailType, string sendMailSuccess, string username)
		{
			if (string.IsNullOrEmpty(username))
			{
				username = "system";
			}
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBNotifyProductMailTransLog (EmailFrom,EmailTo,EmailCC,EmailBCC,Subject,MailContent,MailCategory,SendMailSuccess,CreateOn,CreateBy,UpdateOn,UpdateBy)
										values (@EmailFrom,@EmailTo,@EmailCC,@EmailBCC,@Subject,@MailContent,@MailCategory,@SendMailSuccess,getdate(),@CreateBy,getdate(),@UpdateBy)";
				command.Parameters.AddWithValue("@EmailFrom", mail.From.Address.ToString());
				command.Parameters.AddWithValue("@EmailTo", mail.To.ToString());
				command.Parameters.AddWithValue("@EmailCC", mail.CC.ToString() ?? string.Empty);
				command.Parameters.AddWithValue("@EmailBCC", mail.Bcc.ToString() ?? string.Empty);
				command.Parameters.AddWithValue("@Subject", mail.Subject);
				command.Parameters.AddWithValue("@MailContent", mail.AlternateBody);
				command.Parameters.AddWithValue("@MailCategory", mailType.ToString());
				command.Parameters.AddWithValue("@SendMailSuccess", sendMailSuccess);
				command.Parameters.AddWithValue("@CreateBy", username);
				command.Parameters.AddWithValue("@UpdateBy", username);
				command.ExecuteNonQuery();
			}
		}

		public void AddMailTranLog(AbstractMailTemplate mail, string orderId, MailType mailType, string sendMailSuccess, string username, string errormessage)
		{
			if (string.IsNullOrEmpty(username))
			{
				username = "system";
			}
			if (string.IsNullOrEmpty(errormessage)) { errormessage = ""; }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBMailTransLog(EmailFrom,EmailTo,EmailCC,EmailBCC,Subject,MailContent,MailCategory,SendMailSuccess,OrderID,CreateOn,CreateBy,UpdateOn,UpdateBy,ErrorDesc)
										values (@EmailFrom,@EmailTo,@EmailCC,@EmailBCC,@Subject,@MailContent,@MailCategory,@SendMailSuccess,@OrderID,getdate(),@CreateBy,getdate(),@UpdateBy,@ErrorDesc)";
				command.Parameters.AddWithValue("@EmailFrom", mail.From.Address.ToString());
				command.Parameters.AddWithValue("@EmailTo", mail.To.ToString());
				command.Parameters.AddWithValue("@EmailCC", mail.CC.ToString() ?? string.Empty);
				command.Parameters.AddWithValue("@EmailBCC", mail.Bcc.ToString() ?? string.Empty);
				command.Parameters.AddWithValue("@Subject", mail.Subject);
				command.Parameters.AddWithValue("@MailContent", mail.AlternateBody);
				command.Parameters.AddWithValue("@MailCategory", mailType.ToString());
				command.Parameters.AddWithValue("@OrderID", orderId);
				command.Parameters.AddWithValue("@SendMailSuccess", sendMailSuccess);
				command.Parameters.AddWithValue("@CreateBy", username);
				command.Parameters.AddWithValue("@UpdateBy", username);
				command.Parameters.AddWithValue("@ErrorDesc", errormessage);
				command.ExecuteNonQuery();
			}
		}

		public void AddOrderProcessLog(string orderId, string processRemark, string orderRemark, string ipAddress, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"INSERT INTO TBOrderProcessLog (OrderId, ProcessRemark, Remark, ipAddress, CreateBy, CreateOn)
										VALUES (@orderId, @processRemark, @remark, @ipAddress, @userId, getdate())
										";
				command.Parameters.AddWithValue("@orderId", orderId);
				command.Parameters.AddWithValue("@processRemark", processRemark);
				command.Parameters.AddWithValue("@remark", orderRemark);
				command.Parameters.AddWithValue("@ipAddress", ipAddress);
				command.Parameters.AddWithValue("@userId", userId);
				command.ExecuteNonQuery();
			}
		}

		public IEnumerable<MailTransLog> GetAdvanceSearchMail(SearchMail searchMail)
		{
			List<MailTransLog> logs = new List<MailTransLog>();
			StringBuilder sqlBuilder = new StringBuilder(1024);

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT mailLog.*,isnull(ord.OrderGUID,'') as Guid FROM TBMailTransLog mailLog with (nolock)");
				sqlBuilder.AppendLine("LEFT JOIN TBOrder ord with (nolock) ON mailLog.OrderID=ord.OrderID");
				sqlBuilder.AppendLine("where 1=1");

				if (searchMail.CheckDate && searchMail.DateFrom != DateTime.MaxValue && searchMail.DateTo != DateTime.MinValue)
				{
					sqlBuilder.AppendLine("AND mailLog.CreateOn BETWEEN @DateFrom AND @DateTo");
					command.Parameters.AddWithValue("@DateFrom",searchMail.DateFrom);
					command.Parameters.AddWithValue("@DateTo",searchMail.DateTo.AddDays(1));
				}
				if (searchMail.CheckMailSuccess && !string.IsNullOrEmpty(searchMail.MailSuccess))
				{
					sqlBuilder.AppendLine("AND SendMailSuccess = @MailSuccess");
					command.Parameters.AddWithValue("@MailSuccess",searchMail.MailSuccess);
				}

				if (searchMail.CheckEmailTo && !string.IsNullOrEmpty(searchMail.EmailTo))
				{
					sqlBuilder.AppendLine("AND EmailTo LIKE @EmailTo");
					command.Parameters.AddWithValue("@EmailTo", "%" + searchMail.EmailTo + "%");
				}
				if (searchMail.CheckEmailCC && !string.IsNullOrEmpty(searchMail.EmailCC))
				{
					sqlBuilder.AppendLine("AND EmailCC LIKE @EmailCC");
					command.Parameters.AddWithValue("@EmailCC", "%" + searchMail.EmailCC + "%");
				}
				if (searchMail.CheckOrderId && !string.IsNullOrEmpty(searchMail.OrderId))
				{
					sqlBuilder.AppendLine("AND mailLog.OrderID=@OrderID");
					command.Parameters.AddWithValue("@OrderID",searchMail.OrderId);
				}
				if (searchMail.CheckMailCategory)
				{
					sqlBuilder.AppendLine(SetSearchMailCategory(searchMail));
				}
				sqlBuilder.AppendLine("ORDER BY CreateOn DESC");
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						MailTransLog log = new MailTransLog();
						log.LogID = (int)reader["LogID"];
						log.OrderId = (string)reader["OrderID"];
						log.EmailFrom = (string)reader["EmailFrom"];
						log.EmailTo = (string)reader["EmailTo"];
						log.EmailCC = (string)reader["EmailCC"];
						log.EmailBCC = (string)reader["EmailBCC"];
						log.Subject = (string)reader["Subject"];
						log.MailContent = (string)reader["MailContent"];
						log.MailCategory = (MailType)Enum.Parse(typeof(MailType), (string)reader["MailCategory"]);
						log.SendMailSuccess = (string)reader["SendMailSuccess"];
						log.ErrorDesc = (string)reader["ErrorDesc"];
						log.CreateBy = (string)reader["CreateBy"];
						log.CreateOn = (DateTime)reader["CreateOn"];
						log.Guid = (string)reader["Guid"];
						logs.Add(log);
					}
				}
				return logs;
			}
		}

		public IEnumerable<MailTransLog> GetMailTransLog(int logID)
		{
			List<MailTransLog> logs = new List<MailTransLog>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBMailTransLog with (nolock) WHERE LogID=@LogID";
				command.Parameters.AddWithValue("@LogID", logID);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					MailTransLog log = null;
					while (reader.Read())
					{
						log = new MailTransLog();
						log.LogID = (int)reader["LogID"];
						log.OrderId = (string)reader["OrderID"];
						log.EmailFrom = (string)reader["EmailFrom"];
						log.EmailTo = (string)reader["EmailTo"];
						log.EmailCC = (string)reader["EmailCC"];
						log.EmailBCC = (string)reader["EmailBCC"];
						log.Subject = (string)reader["Subject"];
						log.MailContent = (string)reader["MailContent"];
						log.MailCategory = (MailType)Enum.Parse(typeof(MailType), (string)reader["MailCategory"]);
						log.SendMailSuccess = (string)reader["SendMailSuccess"];
						log.ErrorDesc = (string)reader["ErrorDesc"];
						log.CreateBy = (string)reader["CreateBy"];
						log.CreateOn = (DateTime)reader["CreateOn"];
						logs.Add(log);
					}
				}
				return logs;
			}
		}

		private string SetSearchMailCategory(SearchMail searchMail)
		{
			string searchCategory = "";
			if (searchMail.TypeNotifyProduct || searchMail.TypeVerifyUser || searchMail.TypeForgotPassword || searchMail.TypeApplyNow
				|| searchMail.TypeCreateOrder || searchMail.TypeRequestAdminAllowBudget || searchMail.TypeRequestAdminAllowProduct
				|| searchMail.TypeRequesterSentReviseOrder || searchMail.TypeApproveOrder || searchMail.TypeApproveAndForwardOrder
				|| searchMail.TypeApproverDeleteOrder || searchMail.TypeApproverSentReviseOrder || searchMail.TypeAdminAllowBudgetToRequester
				|| searchMail.TypeAdminAllowBudgetToApprover || searchMail.TypeRequestSpecialProduct || searchMail.TypeContactUs
				|| searchMail.TypeCreateNewSite || searchMail.TypeGoodReceive)
			{
				searchCategory += "AND MailCategory IN (";
				if (searchMail.TypeNotifyProduct)
				{
					searchCategory += " '" + MailType.NotifyProduct.ToString() + "',";
				}
				if (searchMail.TypeVerifyUser)
				{
					searchCategory += " '" + MailType.VerifyUser.ToString() + "',";
				}
				if (searchMail.TypeForgotPassword)
				{
					searchCategory += " '" + MailType.ForgotPassword.ToString() + "',";
				}
				if (searchMail.TypeApplyNow)
				{
					searchCategory += " '" + MailType.ApplyNow.ToString() + "',";
				}
				if (searchMail.TypeCreateOrder)
				{
					searchCategory += " '" + MailType.CreateOrder.ToString() + "',";
				}
				if (searchMail.TypeRequestAdminAllowBudget)
				{
					searchCategory += " '" + MailType.RequestAdminAllowBudget.ToString() + "',";
				}
				if (searchMail.TypeRequestAdminAllowProduct)
				{
					searchCategory += " '" + MailType.RequestAdminAllowProduct.ToString() + "',";
				}
				if (searchMail.TypeRequesterSentReviseOrder)
				{
					searchCategory += " '" + MailType.RequesterSentReviseOrder.ToString() + "',";
				}
				if (searchMail.TypeApproveOrder)
				{
					searchCategory += " '" + MailType.ApproveOrder.ToString() + "',";
				}
				if (searchMail.TypeApproveAndForwardOrder)
				{
					searchCategory += " '" + MailType.ApproveAndForwardOrder.ToString() + "',";
				}
				if (searchMail.TypeApproverDeleteOrder)
				{
					searchCategory += " '" + MailType.ApproverDeleteOrder.ToString() + "',";
				}
				if (searchMail.TypeApproverSentReviseOrder)
				{
					searchCategory += " '" + MailType.ApproverSentReviseOrder.ToString() + "',";
				}
				if (searchMail.TypeAdminAllowBudgetToRequester)
				{
					searchCategory += " '" + MailType.AdminAllowBudgetToRequester.ToString() + "',";
				}
				if (searchMail.TypeAdminAllowBudgetToApprover)
				{
					searchCategory += " '" + MailType.AdminAllowBudgetToApprover.ToString() + "',";
				}
				if (searchMail.TypeRequestSpecialProduct)
				{
					searchCategory += " '" + MailType.RequestSpecialProduct.ToString() + "',";
				}
				if (searchMail.TypeContactUs)
				{
					searchCategory += " '" + MailType.ContactUs.ToString() + "',";
				}
				if (searchMail.TypeCreateNewSite)
				{
					searchCategory += " '" + MailType.CreateNewSite.ToString() + "',";
				}
				if (searchMail.TypeGoodReceive)
				{
					searchCategory += " '" + MailType.GoodReceive.ToString() + "',";
				}
				searchCategory = searchCategory.TrimEnd(',');
				searchCategory += ")";
			}
			return searchCategory;
		}

	}
}