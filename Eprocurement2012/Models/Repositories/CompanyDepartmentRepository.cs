﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class CompanyDepartmentRepository : RepositoryBase
	{
		public CompanyDepartmentRepository(SqlConnection connection) : base(connection) { }
		public bool IsDepartmentIdInSystem(string companyId, string departmentId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBDepartment Where CompanyId = @CompanyId  and DepartmentId = @DepartmentId ";
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@DepartmentId", departmentId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					return reader.Read();
				}
			}
		}
		public CompanyDepartment GetCompanyDepartment(string companyId, string departmentId)
		{
			CompanyDepartment department = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBDepartment Where CompanyId = @CompanyId  and DepartmentId = @DepartmentId ";
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@DepartmentId", departmentId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						department = new CompanyDepartment();
						department.CompanyId = (string)reader["CompanyID"];
						department.DepartmentID = (string)reader["DepartmentId"];
						department.DepartmentThaiName = (string)reader["DeptTName"];
						department.DepartmentEngName = (string)reader["DeptEName"];
						department.DepartmentName = new LocalizedString((string)reader["DeptTName"]);
						department.DepartmentName["en"] = (string)reader["DeptEName"];
						department.DepartmentStatus = (CompanyDepartment.DeptStatus)Enum.Parse(typeof(CompanyDepartment.DeptStatus), (string)reader["DeptStatus"], true);
					}
				}
			}
			return department;
		}
		public IEnumerable<CompanyDepartment> GetMyCompanyDepartment(string companyId)
		{
			List<CompanyDepartment> item = new List<CompanyDepartment>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBDepartment with (nolock) Where CompanyId = @CompanyId ";
				command.Parameters.AddWithValue("@CompanyId", companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					CompanyDepartment department = null;
					while (reader.Read())
					{
						department = new CompanyDepartment();
						department.CompanyId = (string)reader["CompanyID"];
						department.DepartmentID = (string)reader["DepartmentId"];
						department.DepartmentThaiName = (string)reader["DeptTName"];
						department.DepartmentEngName = (string)reader["DeptEName"];
						department.DepartmentName = new LocalizedString((string)reader["DeptTName"]);
						department.DepartmentName["en"] = (string)reader["DeptEName"];
						department.DepartmentStatus = (CompanyDepartment.DeptStatus)Enum.Parse(typeof(CompanyDepartment.DeptStatus), (string)reader["DeptStatus"], true);
						item.Add(department);
					}
				}
			}
			return item;
		}
		public void CreateCompanyDepartment(CompanyDepartment companyDepartment, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Insert TBDepartment (DeptTName, DeptEName, CompanyId, DepartmentId, DeptStatus, CreateOn, CreateBy, UpdateOn)
										Values(@DeptTName, @DeptEName, @CompanyId, @DepartmentId, @DeptStatus, getdate(), @CreateBy, getdate())";
				command.Parameters.AddWithValue("@DeptTName", companyDepartment.DepartmentThaiName);
				command.Parameters.AddWithValue("@DeptEName", companyDepartment.DepartmentEngName);
				command.Parameters.AddWithValue("@CompanyId", companyDepartment.CompanyId);
				command.Parameters.AddWithValue("@DepartmentId", companyDepartment.DepartmentID);
				command.Parameters.AddWithValue("@DeptStatus", companyDepartment.DepartmentStatus.ToString());
				command.Parameters.AddWithValue("@CreateBy", userId);
				command.ExecuteNonQuery();
			}
		}
		public void UpdateCompanyDepartment(CompanyDepartment companyDepartment, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBDepartment set DeptTName=@DeptTName, DeptEName=@DeptEName, DeptStatus=@DeptStatus, UpdateOn=getdate(), UpdateBy=@UpdateBy
										Where CompanyId=@CompanyId and DepartmentId=@DepartmentId";
				command.Parameters.AddWithValue("@DeptTName", companyDepartment.DepartmentThaiName);
				command.Parameters.AddWithValue("@DeptEName", companyDepartment.DepartmentEngName);
				command.Parameters.AddWithValue("@CompanyId", companyDepartment.CompanyId);
				command.Parameters.AddWithValue("@DepartmentId", companyDepartment.DepartmentID);
				command.Parameters.AddWithValue("@DeptStatus", companyDepartment.DepartmentStatus.ToString());
				command.Parameters.AddWithValue("@UpdateBy", userId);
				command.ExecuteNonQuery();
			}
		}
		public void RemoveCompanyDepartment(string companyId, string departmentId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBDepartment set DeptStatus=@DeptStatus, UpdateOn=getdate(), UpdateBy=@UpdateBy
										Where CompanyId=@CompanyId and DepartmentId=@DepartmentId";
				command.Parameters.AddWithValue("@DeptStatus", CompanyDepartment.DeptStatus.Cancel.ToString());
				command.Parameters.AddWithValue("@UpdateBy", userId);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@DepartmentId", departmentId);
				command.ExecuteNonQuery();
			}
		}
	}
}