﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class AuthorizeRepository : RepositoryBase
	{
		public AuthorizeRepository(SqlConnection connection) : base(connection) { }
		public bool AuthorizeMenu(string userId,string companyId, string actionName, string controllerName)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	UserId, TBPermissions.Action, TBPermissions.Controller
										from	TBRoleUsers inner join TBRolePermissions on TBRoleUsers.RoleId = TBRolePermissions.RoleId
												inner join TBPermissions on TBRolePermissions.PermissionID = TBPermissions.PermissionID
										where	UserId=@userId and CompanyId=@companyId and Action=@actionName and Controller=@controllerName";

				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@actionName", actionName);
				command.Parameters.AddWithValue("@controllerName", controllerName);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					return reader.Read();
				}
			}
		}

	}
}