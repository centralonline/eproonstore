﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class BudgetRepository : RepositoryBase
	{
		public BudgetRepository(SqlConnection connection) : base(connection) { }

		public Budget GetBudgetByCostcenterID(string costcenterId, string companyId, Company.BudgetLevel budgetLevel, Company.BudgetPeriod budgetPeriod)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				switch (budgetLevel)
				{
					case Company.BudgetLevel.Company:
						command.CommandText = @"SELECT	t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.CompanyID
												WHERE	t1.CompanyID=@companyID AND t2.CostcenterID=@costcenterID AND t1.PeriodNo=@periodNo AND t1.Year=@year";
						break;
					case Company.BudgetLevel.Department:
						command.CommandText = @"SELECT	t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.DepartmentID
												WHERE	t1.CompanyID=@companyID AND t2.CostcenterID=@costcenterID AND t1.PeriodNo=@periodNo AND t1.Year=@year";
						break;
					case Company.BudgetLevel.Costcenter:
						command.CommandText = @"SELECT	t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.CostcenterID
												WHERE	t1.CompanyID=@companyID AND t2.CostcenterID=@costcenterID AND t1.PeriodNo=@periodNo AND t1.Year=@year";
						break;
					default:
						return null;
				}
				command.Parameters.AddWithValue("@costcenterID", costcenterId);
				command.Parameters.AddWithValue("@companyID", companyId);
				command.Parameters.AddWithValue("@year", DateTime.Now.Year.ToString());
				command.Parameters.AddWithValue("@periodNo", setPeriodNo(budgetPeriod));
				using (SqlDataReader reader = command.ExecuteReader())
				{
					Budget budget = null;
					if (reader.Read())
					{
						budget = new Budget();
						budget.CompanyID = (string)reader["CompanyID"];
						budget.GroupID = (string)reader["GroupID"];
						budget.Year = (string)reader["Year"];
						budget.PeriodNo = (Budget.Period)Enum.Parse(typeof(Budget.Period), (string)reader["PeriodNo"], true);
						budget.OriginalAmt = (decimal)reader["OriginalAmt"];
						budget.BudgetAmt = (decimal)reader["BudgetAmt"];
						budget.UsedAmt = (decimal)reader["UsedAmt"];
						budget.RemainBudgetAmt = (decimal)reader["RemainBudgetAmt"];
						budget.CreateOn = (DateTime)reader["CreateOn"];
						budget.CreateBy = (string)reader["CreateBy"];
						budget.UpdateOn = (DateTime)reader["UpdateOn"];
						budget.UpdateBy = (string)reader["UpdateBy"];
						budget.BudgetLevel = budgetLevel;
						budget.BudgetPeriod = budgetPeriod;
					}
					return budget;
				}
			}
		}

		private string setPeriodNo(Company.BudgetPeriod periodType)
		{
			DateTime date = DateTime.Now;
			switch (periodType)
			{
				case Company.BudgetPeriod.Year:
					return Budget.Period.Y.ToString(); ;
				case Company.BudgetPeriod.Half:
					if (date.Month < 7) { return Budget.Period.H1.ToString(); }
					else if (date.Month > 6) { return Budget.Period.H2.ToString(); }
					else { return ""; }
				case Company.BudgetPeriod.Quarter:
					if (date.Month <= 3) { return Budget.Period.Q1.ToString(); }
					else if (date.Month > 3 && date.Month <= 6) { return Budget.Period.Q2.ToString(); }
					else if (date.Month > 6 && date.Month <= 9) { return Budget.Period.Q3.ToString(); }
					else if (date.Month > 9) { return Budget.Period.Q4.ToString(); }
					else { return ""; }
				case Company.BudgetPeriod.Month:
					return "M" + date.Month.ToString();
				default:
					return "";
			}
		}

		public void UpdateBudgetAfterApproveOrder(Budget currentBudget, string orderId, decimal grandTotalAmt, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"UPDATE	TBBudget SET UsedAmt=@sumUsedAmt, RemainBudgetAmt=@newAmt, UpdateOn = getdate(), UpdateBy=@userId
										WHERE	CompanyID=@companyID AND GroupID=@groupID AND PeriodNo=@periodNo AND Year=@year

										INSERT	INTO TBBudgetLog (CompanyID,GroupID,Year,PeriodNo,OldAmt,DecAmt,NewAmt,OrderID,CreateOn,CreateBy)
										VALUES	(@companyID,@groupID,@year,@periodNo,@oldAmt,@usedAmt,@newAmt,@orderID,getdate(),@userId)
										";

				command.Parameters.AddWithValue("@companyID", currentBudget.CompanyID);
				command.Parameters.AddWithValue("@groupID", currentBudget.GroupID);
				command.Parameters.AddWithValue("@periodNo", currentBudget.PeriodNo.ToString());
				command.Parameters.AddWithValue("@year", currentBudget.Year);
				command.Parameters.AddWithValue("@oldAmt", currentBudget.RemainBudgetAmt);
				command.Parameters.AddWithValue("@sumUsedAmt", currentBudget.UsedAmt + grandTotalAmt);
				command.Parameters.AddWithValue("@newAmt", currentBudget.RemainBudgetAmt - grandTotalAmt);
				command.Parameters.AddWithValue("@usedAmt", grandTotalAmt);
				command.Parameters.AddWithValue("@orderID", orderId);
				command.Parameters.AddWithValue("@userId", userId);
				command.ExecuteNonQuery();
			}
		}

		//สำหรับ update การแก้ไขงบประมาณ ทั้งเพิ่มจากงบประมาณที่เหลือ หรือ สร้างงบประมาณใหม่
		public void UpdateBudgetByAddRemainAmt(EditBudget currentBudget, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"UPDATE	TBBudget SET BudgetAmt=@BudgetAmt, RemainBudgetAmt=@RemainBudgetAmt, UpdateOn = getdate(), UpdateBy=@userId
										WHERE	CompanyID=@companyID AND GroupID=@groupID AND PeriodNo=@periodNo AND Year=@year

										INSERT	INTO TBBudgetLog (CompanyID,GroupID,Year,PeriodNo,OldAmt,IncAmt,NewAmt,CreateOn,CreateBy)
										VALUES	(@companyID,@groupID,@year,@periodNo,@oldAmt,@incAmt,@newAmt,getdate(),@userId) ";

				if (currentBudget.IsCreateBudgetAmt)
				{
					if ((currentBudget.Budget.BudgetAmt + currentBudget.NewBudgetAmt) > currentBudget.Budget.RemainBudgetAmt)
					{
						command.Parameters.AddWithValue("@BudgetAmt", (currentBudget.Budget.BudgetAmt + currentBudget.NewBudgetAmt) - currentBudget.Budget.RemainBudgetAmt);
					}
					else
					{
						command.Parameters.AddWithValue("@BudgetAmt", currentBudget.Budget.RemainBudgetAmt - (currentBudget.Budget.BudgetAmt + currentBudget.NewBudgetAmt));
					}

					command.Parameters.AddWithValue("@RemainBudgetAmt", currentBudget.NewBudgetAmt);
					command.Parameters.AddWithValue("@oldAmt", currentBudget.Budget.RemainBudgetAmt);
					command.Parameters.AddWithValue("@incAmt", currentBudget.NewBudgetAmt - currentBudget.Budget.RemainBudgetAmt);
					command.Parameters.AddWithValue("@newAmt", currentBudget.NewBudgetAmt);
				}
				else
				{
					command.Parameters.AddWithValue("@BudgetAmt", currentBudget.Budget.BudgetAmt + currentBudget.NewBudgetAmt);
					command.Parameters.AddWithValue("@RemainBudgetAmt", currentBudget.Budget.RemainBudgetAmt + currentBudget.NewBudgetAmt);
					command.Parameters.AddWithValue("@oldAmt", currentBudget.Budget.RemainBudgetAmt);
					command.Parameters.AddWithValue("@incAmt", currentBudget.NewBudgetAmt);
					command.Parameters.AddWithValue("@newAmt", currentBudget.Budget.RemainBudgetAmt + currentBudget.NewBudgetAmt);
				}

				command.Parameters.AddWithValue("@companyID", currentBudget.Budget.CompanyID);
				command.Parameters.AddWithValue("@groupID", currentBudget.Budget.GroupID);
				command.Parameters.AddWithValue("@userId", userId);
				command.Parameters.AddWithValue("@periodNo", currentBudget.Budget.PeriodNo.ToString());
				command.Parameters.AddWithValue("@year", currentBudget.Budget.Year);
				command.ExecuteNonQuery();
			}
		}

		public decimal GetWaitingOrderOnThisBudget(string groupID, string companyId, Company.BudgetLevel budgetLevel)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	isnull(sum(GrandTotalAmt),0) as  GrandTotalAmt
										FROM	TBOrder
										WHERE	CompanyID=@companyID AND OrderStatus In (@Waiting,@Partial,@WaitingAdmin,@AdminAllow) AND year(OrderDate) = year(getdate())";
				switch (budgetLevel)
				{
					case Company.BudgetLevel.Department:
						command.CommandText += " AND DepartmentID = @groupID";
						break;
					case Company.BudgetLevel.Costcenter:
						command.CommandText += " AND CostcenterID = @groupID";
						break;
					case Company.BudgetLevel.Company:
					default:
						break;
				}
				command.Parameters.AddWithValue("@groupID", groupID);
				command.Parameters.AddWithValue("@companyID", companyId);
				command.Parameters.AddWithValue("@Waiting", Order.OrderStatus.Waiting.ToString());
				command.Parameters.AddWithValue("@WaitingAdmin", Order.OrderStatus.WaitingAdmin.ToString());
				command.Parameters.AddWithValue("@AdminAllow", Order.OrderStatus.AdminAllow.ToString());
				command.Parameters.AddWithValue("@Partial", Order.OrderStatus.Partial.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					decimal orderAmt = 0;
					if (reader.Read())
					{
						orderAmt = (decimal)reader["GrandTotalAmt"];
					}
					return orderAmt;
				}
			}
		}

		public void GetBudgetDatailForApprover(Budget budget, string userID)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	isnull(WaitingOrderAmt,0) AS WaitingOrderAmt, isnull(WaitingCurrentUserAmt,0) AS WaitingCurrentUserAmt, isnull(CurrentUserApproveAmt,0) AS CurrentUserApproveAmt
										FROM
											(SELECT DISTINCT CompanyID, isnull(sum(GrandTotalAmt),0) AS WaitingOrderAmt
											FROM
												TBOrder
											WHERE
												CompanyID = @companyID
												AND OrderStatus IN (@Waiting, @Partial, @WaitingAdmin, @AdminAllow)
												AND year(OrderDate) = @year";
				switch (budget.BudgetLevel)
				{
					case Company.BudgetLevel.Department:
						command.CommandText += " AND DepartmentID = @groupID";
						break;
					case Company.BudgetLevel.Costcenter:
						command.CommandText += " AND CostcenterID = @groupID";
						break;
					case Company.BudgetLevel.Company:
					default:
						break;
				}
				command.CommandText += @"	GROUP BY
												CompanyID) AS WO
											LEFT JOIN
											(SELECT DISTINCT CompanyID, isnull(sum(GrandTotalAmt),0) AS WaitingCurrentUserAmt
											FROM
												TBOrder
											WHERE
												CompanyID = @companyID
												AND OrderStatus IN (@Waiting, @Partial, @WaitingAdmin, @AdminAllow)
												AND year(OrderDate) = @year
												AND CurrentAppID = @userID";
				switch (budget.BudgetLevel)
				{
					case Company.BudgetLevel.Department:
						command.CommandText += " AND DepartmentID = @groupID";
						break;
					case Company.BudgetLevel.Costcenter:
						command.CommandText += " AND CostcenterID = @groupID";
						break;
					case Company.BudgetLevel.Company:
					default:
						break;
				}
				command.CommandText += @"	GROUP BY
												CompanyID) AS WCU
												ON WO.CompanyID = WCU.CompanyID
											LEFT JOIN
											(SELECT DISTINCT CompanyID, isnull(sum(GrandTotalAmt),0) AS CurrentUserApproveAmt
											FROM
												TBOrder
											WHERE
												CompanyID = @companyID
												AND OrderStatus IN (@Partial, @AdminAllow, @Approved)
												AND year(OrderDate) = @year
												AND PreviousAppID = @userID";
				switch (budget.BudgetLevel)
				{
					case Company.BudgetLevel.Department:
						command.CommandText += " AND DepartmentID = @groupID";
						break;
					case Company.BudgetLevel.Costcenter:
						command.CommandText += " AND CostcenterID = @groupID";
						break;
					case Company.BudgetLevel.Company:
					default:
						break;
				}
				command.CommandText += @"	GROUP BY
												CompanyID) AS CUA
												ON WCU.CompanyID = CUA.CompanyID";

				command.Parameters.AddWithValue("@groupID", budget.GroupID);
				command.Parameters.AddWithValue("@companyID", budget.CompanyID);
				command.Parameters.AddWithValue("@year", budget.Year);
				command.Parameters.AddWithValue("@userID", userID);
				command.Parameters.AddWithValue("@Waiting", Order.OrderStatus.Waiting.ToString());
				command.Parameters.AddWithValue("@WaitingAdmin", Order.OrderStatus.WaitingAdmin.ToString());
				command.Parameters.AddWithValue("@AdminAllow", Order.OrderStatus.AdminAllow.ToString());
				command.Parameters.AddWithValue("@Partial", Order.OrderStatus.Partial.ToString());
				command.Parameters.AddWithValue("@Approved", Order.OrderStatus.Approved.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						budget.WaitingOrderAmt = (decimal)reader["WaitingOrderAmt"];
						budget.WaitingCurrentUserAmt = (decimal)reader["WaitingCurrentUserAmt"];
						budget.CurrentUserApproveAmt = (decimal)reader["CurrentUserApproveAmt"];
					}
				}
			}
		}

		//สำหรับค้นหาข้อมูล Budget โดยใช้รหัสองค์กร หรือ รหัสควบคุม
		public IEnumerable<Budget> GetBudgetByCompanyIdAndGroupId(Company.BudgetLevel budgetLevel, Company.BudgetPeriod budgetPeriod, string companyId, string groupId)
		{
			List<Budget> budgets = new List<Budget>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder();
				if (budgetLevel == Company.BudgetLevel.Company)
				{
					sqlBuilder.AppendLine("SELECT DISTINCT(GroupID),t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.CompanyID");
					sqlBuilder.AppendLine("WHERE	t1.CompanyID=@companyID AND t1.Year=@year");
				}
				if (budgetLevel == Company.BudgetLevel.Department)
				{
					sqlBuilder.AppendLine("SELECT	t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.DepartmentID");
					sqlBuilder.AppendLine("WHERE	t1.CompanyID=@companyID AND t1.Year=@year");
				}
				if (budgetLevel == Company.BudgetLevel.Costcenter)
				{
					sqlBuilder.AppendLine("SELECT	t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.CostcenterID");
					sqlBuilder.AppendLine("WHERE	t1.CompanyID=@companyID AND t1.Year=@year");
				}
				if (!String.IsNullOrEmpty(companyId) && !String.IsNullOrEmpty(groupId))
				{
					sqlBuilder.AppendLine("AND t1.GroupID=@GroupID");
					command.Parameters.AddWithValue("@groupId", groupId);
				}

				command.Parameters.AddWithValue("@companyID", companyId);
				command.Parameters.AddWithValue("@year", DateTime.Now.Year.ToString());
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Budget budget = new Budget();
						budget.CompanyID = (string)reader["CompanyID"];
						budget.GroupID = (string)reader["GroupID"];
						budget.Year = (string)reader["Year"];
						budget.PeriodNo = (Budget.Period)Enum.Parse(typeof(Budget.Period), (string)reader["PeriodNo"], true);
						budget.OriginalAmt = (decimal)reader["OriginalAmt"];
						budget.BudgetAmt = (decimal)reader["BudgetAmt"];
						budget.UsedAmt = (decimal)reader["UsedAmt"];
						budget.RemainBudgetAmt = (decimal)reader["RemainBudgetAmt"];
						budget.CreateOn = (DateTime)reader["CreateOn"];
						budget.CreateBy = (string)reader["CreateBy"];
						budget.UpdateOn = (DateTime)reader["UpdateOn"];
						budget.UpdateBy = (string)reader["UpdateBy"];
						budget.BudgetLevel = budgetLevel;
						budget.BudgetPeriod = budgetPeriod;
						budgets.Add(budget);
					}
					return budgets;
				}
			}
		}

		//สำหรับการดึงข้อมูล Budget โดยใช้ GroupId และ PeriodNo ของปัจจุบันเท่านั้น มาทำการแก้ไขงบประมาณ
		public Budget GetBudgetByGroupId(Company.BudgetLevel budgetLevel, Company.BudgetPeriod budgetPeriod, string companyId, string groupId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder();
				if (budgetLevel == Company.BudgetLevel.Company)
				{
					sqlBuilder.AppendLine("SELECT DISTINCT(GroupID),t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.CompanyID");
					sqlBuilder.AppendLine("WHERE	t1.CompanyID=@companyID AND t1.Year=@year AND t1.PeriodNo=@periodNo AND t1.GroupID=@GroupID");
				}
				if (budgetLevel == Company.BudgetLevel.Department)
				{
					sqlBuilder.AppendLine("SELECT	t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.DepartmentID");
					sqlBuilder.AppendLine("WHERE	t1.CompanyID=@companyID AND t1.Year=@year AND t1.PeriodNo=@periodNo AND t1.GroupID=@GroupID");
				}
				if (budgetLevel == Company.BudgetLevel.Costcenter)
				{
					sqlBuilder.AppendLine("SELECT	t1.* FROM TBBudget t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID = t2.CompanyID AND t1.GroupID = t2.CostcenterID");
					sqlBuilder.AppendLine("WHERE	t1.CompanyID=@companyID AND t1.Year=@year AND t1.PeriodNo=@periodNo AND t1.GroupID=@GroupID");
				}

				command.Parameters.AddWithValue("@companyID", companyId);
				command.Parameters.AddWithValue("@groupId", groupId);
				command.Parameters.AddWithValue("@year", DateTime.Now.Year.ToString());
				command.Parameters.AddWithValue("@periodNo", setPeriodNo(budgetPeriod));
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					Budget budget = null;
					if (reader.Read())
					{
						budget = new Budget();
						budget.CompanyID = (string)reader["CompanyID"];
						budget.GroupID = (string)reader["GroupID"];
						budget.Year = (string)reader["Year"];
						budget.PeriodNo = (Budget.Period)Enum.Parse(typeof(Budget.Period), (string)reader["PeriodNo"], true);
						budget.OriginalAmt = (decimal)reader["OriginalAmt"];
						budget.BudgetAmt = (decimal)reader["BudgetAmt"];
						budget.UsedAmt = (decimal)reader["UsedAmt"];
						budget.RemainBudgetAmt = (decimal)reader["RemainBudgetAmt"];
						budget.CreateOn = (DateTime)reader["CreateOn"];
						budget.CreateBy = (string)reader["CreateBy"];
						budget.UpdateOn = (DateTime)reader["UpdateOn"];
						budget.UpdateBy = (string)reader["UpdateBy"];
						budget.BudgetLevel = budgetLevel;
						budget.BudgetPeriod = budgetPeriod;
					}
					return budget;
				}
			}
		}


	}
}