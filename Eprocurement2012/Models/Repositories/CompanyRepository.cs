﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Text;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class CompanyRepository : RepositoryBase
	{
		public CompanyRepository(SqlConnection connection) : base(connection) { }
		public IEnumerable<Company> GetAllMyCompany(string userId, Company.CompanyStatus companyStatus)
		{
			if (userId == null) { throw new ArgumentNullException("userId"); }
			List<Company> item = new List<Company>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	T1.CompanyID,CompTName,CompEName,IsDefaultCompany
										from	TBCompanyInfo T1 Inner Join TBUserCompany T2 on T1.CompanyID = T2.CompanyID
										Where	T2.CompanyID <> 'Officemate' and UserID = @UserID and CompStatus = @CompStatus AND t2.UserStatus=@UserStatus";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@CompStatus", companyStatus.ToString());
				command.Parameters.AddWithValue("@UserStatus", User.UserStatus.Active.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					Company company = null;

					while (reader.Read())
					{
						company = new Company();
						company.CompanyId = (string)reader["CompanyID"];
						company.CompanyThaiName = (string)reader["CompTName"];
						company.CompanyEngName = (string)reader["CompEName"];
						company.CompanyName = new LocalizedString((string)reader["CompTName"]);
						company.CompanyName["en"] = (string)reader["CompEName"];
						company.IsSiteActive = Mapper.ToClass<bool>((string)reader["IsDefaultCompany"]); /*ยืมมาเก็บ IsDefaultCompany ใช้ในการ default ค่าใน select list*/
						item.Add(company);
					}
				}
			}
			return item;
		}
		public List<Company> GetAllCompany()//ใช้ในหน้า OFMAdmin Index
		{
			List<Company> companys = new List<Company>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	t1.*,t2.countUser,t3.DiscountRate
										FROM	TBCompanyInfo t1 INNER JOIN (SELECT count(UserID) as countUser, CompanyID FROM TBUserCompany GROUP BY CompanyID) t2 ON t1.CompanyID = t2.CompanyID
												INNER JOIN TBCustMaster t3 on t1.DefaultCustID=t3.CustID
										WHERE	t1.CompanyID <> 'Officemate'";

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Company company = new Company();
						company.CompanyId = (string)reader["CompanyID"];
						company.CompanyThaiName = (string)reader["CompTName"];
						company.CompanyEngName = (string)reader["CompEName"];
						company.CompanyName = new LocalizedString((string)reader["CompTName"]);
						company.CompanyName["en"] = (string)reader["CompEName"];
						company.CompStatus = (Company.CompanyStatus)Enum.Parse(typeof(Company.CompanyStatus), (string)reader["CompStatus"], true);
						company.OrderControlType = (Company.OrderControl)Enum.Parse(typeof(Company.OrderControl), (string)reader["OrderControlType"], true);
						company.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						company.DefaultParkDay = (int)reader["countUser"]; //ยืมมาเก็บจำนวนผู้ใช้งานระบบ
						company.DefaultCustId = (string)reader["DefaultCustId"];
						company.DiscountRate = (decimal)reader["DiscountRate"];
						companys.Add(company);
					}
				}
				return companys;
			}
		}
		public void SetCurrentUserCompany(string userId, string companyId)
		{
			if ((userId == null) || (companyId == null)) { throw new ArgumentNullException("userId"); }

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBUserCompany set IsdefaultCompany = @IsdefaultNo Where UserID = @UserID
										Update TBUserCompany set IsdefaultCompany = @IsdefaultYes Where UserID = @UserID and companyID = @companyID";
				command.Parameters.AddWithValue("@UserID", userId);
				command.Parameters.AddWithValue("@companyID", companyId);
				command.Parameters.AddWithValue("@IsdefaultYes", User.UserCompanyDefault.Yes.ToString());
				command.Parameters.AddWithValue("@IsdefaultNo", User.UserCompanyDefault.No.ToString());
				command.ExecuteNonQuery();
			}
		}
		public void UpdateCompanyInfo(Company company, string userId)
		{
			if (company == null) { throw new ArgumentNullException("company"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"update 	TBCompanyInfo set CompTName=@CompTName, CompEName=@CompEName, UpdateOn=getdate(), UpdateBy=@UpdateBy
										where	CompanyId=@CompanyId";
				command.Parameters.AddWithValue("@CompanyId", company.CompanyId);
				command.Parameters.AddWithValue("@CompTName", company.CompanyThaiName);
				command.Parameters.AddWithValue("@CompEName", company.CompanyEngName);
				command.Parameters.AddWithValue("@UpdateBy", userId);
				command.ExecuteNonQuery();
			}
		}
		public ImageData GetCompanyLogo(string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				ImageData companyLogo = new ImageData();
				if (companyId == null) { return companyLogo; }
				command.CommandText = @"select LogoImage from TBCompanyInfo where CompanyId = @CompanyId";
				command.Parameters.AddWithValue("@CompanyId", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						companyLogo.Bytes = (byte[])reader["LogoImage"];
					}
				}
				return companyLogo;
			}

		}
		public void UpdateCompanyLogo(byte[] logoImage, string companyId, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBCompanyInfo set LogoImage = @LogoImage, UpdateOn = getdate(), UpdateBy = @UserId Where CompanyId = @CompanyId";
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@LogoImage", logoImage);
				command.Parameters.AddWithValue("@UserId", userId);
				command.ExecuteNonQuery();
			}
		}

		public void CreateBudget(string budgetLevelType, string budgetPeriodType, string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @" 
										 DECLARE @CreateBy varchar(200)
										";
				command.Parameters.AddWithValue("@CompanyId", companyId);
				switch (budgetLevelType)
				{

					case "Company":
						if (budgetPeriodType == Company.BudgetPeriod.Year.ToString())
						{
							command.CommandText = @"Insert into TBBudget
													select @CompanyId,@CompanyId ,Year(getdate()),'Y',0,0,0,0,getdate(),'Auto System',getdate(),'' 
													";
						}
						else if (budgetPeriodType == Company.BudgetPeriod.Half.ToString())
						{
							command.CommandText = @"Insert into TBBudget
													select @CompanyId,@CompanyId ,Year(getdate()),'H1',0,0,0,0,getdate(),'Auto System',getdate(),'' 
													";
							command.CommandText += @"Insert into TBBudget
													select @CompanyId,@CompanyId ,Year(getdate()),'H2',0,0,0,0,getdate(),'Auto System',getdate(),'' 
													";
						}
						else if (budgetPeriodType == Company.BudgetPeriod.Quarter.ToString())
						{
							for (int i = 1; i < 5; i++)
							{
								string quarter = "Q" + i;
								command.CommandText += @" Insert into TBBudget
													  select @CompanyId,@CompanyId ,Year(getdate()),'" + quarter + "',0,0,0,0,getdate(),'Auto System',getdate(),'' ";
							}
						}
						else
						{
							for (int i = 1; i < 13; i++)
							{
								string month = "M" + i;
								command.CommandText += @" Insert into TBBudget
													  select @CompanyId,@CompanyId ,Year(getdate()),'" + month + "',0,0,0,0,getdate(),'Auto System',getdate(),'' ";
							}
						}
						break;
					case "Department":
						if (budgetPeriodType == Company.BudgetPeriod.Year.ToString())
						{
							command.CommandText = @" Insert into TBBudget
														select @CompanyId,DepartmentID ,Year(getdate()),'Y',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBDepartment where CompanyID = @CompanyId
														AND DepartmentID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId)
														";
						}
						else if (budgetPeriodType == Company.BudgetPeriod.Half.ToString())
						{
							command.CommandText = @" Insert into TBBudget
															select @CompanyId,DepartmentID ,Year(getdate()),'H1',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBDepartment where CompanyID = @CompanyId
															AND DepartmentID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId and PeriodNo = 'H1' )
														";
							command.CommandText += @" Insert into TBBudget
															select @CompanyId,DepartmentID ,Year(getdate()),'H2',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBDepartment where CompanyID = @CompanyId
															AND DepartmentID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId and PeriodNo = 'H2' )
														";
						}
						else if (budgetPeriodType == Company.BudgetPeriod.Quarter.ToString())
						{
							for (int i = 1; i < 5; i++)
							{
								string quarter = "Q" + i;
								command.CommandText += @" Insert into TBBudget
															select @CompanyId,DepartmentID ,Year(getdate()),'" + quarter + "',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBDepartment where CompanyID = @CompanyId AND DepartmentID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId and PeriodNo = '" + quarter + "' )";
							}
						}
						else
						{
							for (int i = 1; i < 13; i++)
							{
								string month = "M" + i;
								command.CommandText += @" Insert into TBBudget
															select @CompanyId,DepartmentID ,Year(getdate()),'" + month + "',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBDepartment where CompanyID = @CompanyId AND DepartmentID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId and PeriodNo = '" + month + "' )";
							}
						}
						break;
					case "Costcenter":
						if (budgetPeriodType == Company.BudgetPeriod.Year.ToString())
						{
							command.CommandText = @" Insert into TBBudget
																select @CompanyId,CostcenterID ,Year(getdate()),'Y',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBCostcenter where CompanyID = @CompanyId
																AND CostcenterID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId)
																";
						}
						else if (budgetPeriodType == Company.BudgetPeriod.Half.ToString())
						{
							command.CommandText = @" Insert into TBBudget
																	select @CompanyId,CostcenterID ,Year(getdate()),'H1',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBCostcenter where CompanyID = @CompanyId
																	AND CostcenterID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId and PeriodNo = 'H1' )
																";
							command.CommandText += @" Insert into TBBudget
																	select @CompanyId,CostcenterID ,Year(getdate()),'H2',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBCostcenter where CompanyID = @CompanyId
																	AND CostcenterID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId and PeriodNo = 'H2')
																";
						}
						else if (budgetPeriodType == Company.BudgetPeriod.Quarter.ToString())
						{
							for (int i = 1; i < 5; i++)
							{
								string quarter = "Q" + i;
								command.CommandText += @" Insert into TBBudget
																	select @CompanyId,CostcenterID ,Year(getdate()),'" + quarter + "',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBCostcenter where CompanyID = @CompanyId AND CostcenterID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId and PeriodNo = '" + quarter + "') ";
							}
						}
						else
						{
							for (int i = 1; i < 13; i++)
							{
								string month = "M" + i;
								command.CommandText += @" Insert into TBBudget
																	select @CompanyId,CostcenterID ,Year(getdate()),'" + month + "',0,0,0,0,getdate(),'Auto System',getdate(),'' FROM TBCostcenter where CompanyID = @CompanyId AND CostcenterID NOT IN(select GroupID from TBBudget Where CompanyID = @CompanyId and PeriodNo = '" + month + "') ";
							}
						}
						break;
					default:
						break;
				}
				command.ExecuteNonQuery();
			}
		}

		public void RemoveCompanyLogo(string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBCompanyInfo set LogoImage=CONVERT(VARBINARY(MAX),'') Where companyId=@companyId";
				command.Parameters.AddWithValue("@companyId", companyId);
				command.ExecuteNonQuery();
			}
		}

		public void CreateNewCompany(NewSiteData newSiteData, string createBy)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBCompanyInfo (CompanyId, CompTName, CompEName, CompStatus, CompModel, LogoImage, OrderControlType, BudgetLevelType, BudgetPeriodType,
													PriceType, UseOfmCatalog, RePasswordPeriod, AllowAddProduct, OrderIDFormat, DefaultLang, DefaultCustID, DefaultDeliCharge, DefaultParkDay,
													IsByPassAdmin, IsByPassApprover, IsAutoApprove, IsSiteActive, ShowOutofStock, ShowOfmCat, ShowContactUs, ShowSpecialProd, UseCompanyNews,
													UseOfmNews, UseReferenceCode, UseSMSFeature,AdminOfmRemark,SaleRequest, CreateOn, CreateBy, UpdateOn, UpdateBy)
										values (@CompanyId, @CompTName, @CompEName, @CompStatus, @CompModel, 0, @OrderControlType, @BudgetLevelType, @BudgetPeriodType,
												@PriceType, @UseOfmCatalog, @RePasswordPeriod, @AllowAddProduct, @OrderIDFormat, @DefaultLang, @DefaultCustID, @DefaultDeliCharge,
												@DefaultParkDay, @IsByPassAdmin, @IsByPassApprover, @IsAutoApprove, @IsSiteActive, @ShowOutofStock, @ShowOfmCat, @ShowContactUs,
												@ShowSpecialProd, @UseCompanyNews, @UseOfmNews, @UseReferenceCode, @UseSMSFeature,@AdminOfmRemark,@SaleRequest, getdate(), @CreateBy, getdate(), @CreateBy)";
				command.Parameters.AddWithValue("@CompanyId", newSiteData.CompanyId.ToUpper());//Request จากทาง TM ต้องการให้รหัสองค์กรเป็นตัวใหญ่ทั้งหมด
				command.Parameters.AddWithValue("@CompTName", newSiteData.CompanyTName);
				command.Parameters.AddWithValue("@CompEName", newSiteData.CompanyEName);
				command.Parameters.AddWithValue("@CompStatus", Company.CompanyStatus.Active.ToString());
				command.Parameters.AddWithValue("@CompModel", newSiteData.CompanyModel);
				command.Parameters.AddWithValue("@OrderControlType", newSiteData.OrderControlType);
				command.Parameters.AddWithValue("@BudgetLevelType", newSiteData.BudgetLevelType);
				command.Parameters.AddWithValue("@BudgetPeriodType", newSiteData.BudgetPeriodType);
				command.Parameters.AddWithValue("@PriceType", newSiteData.PriceType);
				command.Parameters.AddWithValue("@UseOfmCatalog", newSiteData.UseOfmCatalog);
				command.Parameters.AddWithValue("@OrderIDFormat", newSiteData.OrderIDFormat.ToString());
				command.Parameters.AddWithValue("@AllowAddProduct", Mapper.ToDatabase(newSiteData.AllowAddProduct));
				command.Parameters.AddWithValue("@RePasswordPeriod", newSiteData.RePasswordPeriod);
				command.Parameters.AddWithValue("@DefaultLang", newSiteData.DefaultLang.ToString());
				command.Parameters.AddWithValue("@DefaultCustID", newSiteData.CustId);
				command.Parameters.AddWithValue("@DefaultDeliCharge", Mapper.ToDatabase(newSiteData.DefaultDeliCharge));
				command.Parameters.AddWithValue("@DefaultParkDay", newSiteData.DefaultParkDay);
				command.Parameters.AddWithValue("@IsByPassAdmin", Mapper.ToDatabase(newSiteData.IsByPassAdmin));
				command.Parameters.AddWithValue("@IsByPassApprover", Mapper.ToDatabase(newSiteData.IsByPassApprover));
				command.Parameters.AddWithValue("@IsAutoApprove", Mapper.ToDatabase(newSiteData.IsAutoApprove));
				command.Parameters.AddWithValue("@IsSiteActive", Mapper.ToDatabase(newSiteData.IsSiteActivate));
				command.Parameters.AddWithValue("@ShowOutofStock", Mapper.ToDatabase(newSiteData.ShowOutofStock));
				command.Parameters.AddWithValue("@ShowOfmCat", Mapper.ToDatabase(newSiteData.ShowOfmCat));
				command.Parameters.AddWithValue("@ShowContactUs", Mapper.ToDatabase(newSiteData.ShowContactUs));
				command.Parameters.AddWithValue("@ShowSpecialProd", Mapper.ToDatabase(newSiteData.ShowSpecialProd));
				command.Parameters.AddWithValue("@UseCompanyNews", Mapper.ToDatabase(newSiteData.UseCompanyNews));
				command.Parameters.AddWithValue("@UseOfmNews", Mapper.ToDatabase(newSiteData.UseOfmNews));
				command.Parameters.AddWithValue("@UseReferenceCode", Mapper.ToDatabase(newSiteData.UseReferenceCode));
				command.Parameters.AddWithValue("@UseSMSFeature", Mapper.ToDatabase(newSiteData.UseSMSFeature));
				command.Parameters.AddWithValue("@AdminOfmRemark", !String.IsNullOrEmpty(newSiteData.AdminOfmRemark) ? newSiteData.AdminOfmRemark : "");
				command.Parameters.AddWithValue("@SaleRequest", newSiteData.SaleRequest);
				command.Parameters.AddWithValue("@CreateBy", createBy);
				command.ExecuteNonQuery();
			}
		}
		public NewSiteData GetNewSiteData(string companyId)
		{
			NewSiteData data = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	t1.CompanyId, CompTName, CompEName, CompModel, OrderControlType, BudgetLevelType, BudgetPeriodType,PriceType, UseOfmCatalog,
												RePasswordPeriod, AllowAddProduct, OrderIDFormat, DefaultLang, DefaultCustID, DefaultDeliCharge, DefaultParkDay,
												IsByPassAdmin, IsByPassApprover, IsAutoApprove, IsSiteActive, ShowOutofStock, ShowOfmCat, ShowContactUs, ShowSpecialProd,
												UseCompanyNews, UseOfmNews, UseReferenceCode, UseSMSFeature, t3.UserId, t3.UserTName, t3.UserEName, RoleName,t1.CreateBy
										from	TBCompanyInfo t1 inner join TBRoleUsers t2 on t1.CompanyId=t2.CompanyId
												inner join TBUserInfo t3 on t2.UserId=t3.UserId
												inner join TBRoles t4 on t2.RoleID=t4.RoleID
										where	t1.CompanyID=@CompanyID and t4.RoleName=@RoleName";
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@RoleName", User.UserRole.RootAdmin.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						data = new NewSiteData();
						data.CompanyId = (string)reader["CompanyId"];
						data.CompanyTName = (string)reader["CompTName"];
						data.CompanyEName = (string)reader["CompEName"];
						data.CompanyModel = (string)reader["CompModel"];
						data.OrderControlType = (string)reader["OrderControlType"];
						data.BudgetLevelType = (string)reader["BudgetLevelType"];
						data.BudgetPeriodType = (string)reader["BudgetPeriodType"];
						data.PriceType = (string)reader["PriceType"];
						data.UseOfmCatalog = (string)reader["UseOfmCatalog"];
						data.RePasswordText = Convert.ToString((int)reader["RePasswordPeriod"]);
						data.AllowAddProduct = Mapper.ToClass<bool>((string)reader["AllowAddProduct"]);
						data.OrderIDFormat = (Company.OrderIDFormatType)Enum.Parse(typeof(Company.OrderIDFormatType), (string)reader["OrderIDFormat"], true);
						data.DefaultLang = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["DefaultLang"], true);
						data.CustId = (string)reader["DefaultCustID"];
						data.DefaultDeliCharge = Mapper.ToClass<bool>((string)reader["DefaultDeliCharge"]);
						data.DefaultParkDay = (int)reader["DefaultParkDay"];
						data.IsByPassAdmin = Mapper.ToClass<bool>((string)reader["IsByPassAdmin"]);
						data.IsByPassApprover = Mapper.ToClass<bool>((string)reader["IsByPassApprover"]);
						data.IsAutoApprove = Mapper.ToClass<bool>((string)reader["IsAutoApprove"]);
						data.IsSiteActivate = (string)reader["IsSiteActive"];
						data.ShowOutofStock = Mapper.ToClass<bool>((string)reader["ShowOutofStock"]);
						data.ShowOfmCat = Mapper.ToClass<bool>((string)reader["ShowOfmCat"]);
						data.ShowContactUs = Mapper.ToClass<bool>((string)reader["ShowContactUs"]);
						data.ShowSpecialProd = Mapper.ToClass<bool>((string)reader["ShowSpecialProd"]);
						data.UseCompanyNews = Mapper.ToClass<bool>((string)reader["UseCompanyNews"]);
						data.UseOfmNews = Mapper.ToClass<bool>((string)reader["UseOfmNews"]);
						data.UseReferenceCode = Mapper.ToClass<bool>((string)reader["UseReferenceCode"]);
						data.UseSMSFeature = Mapper.ToClass<bool>((string)reader["UseSMSFeature"]);
						data.UserID = (string)reader["UserId"];
						data.UserTName = (string)reader["UserTName"];
						data.UserEName = (string)reader["UserEName"];
						data.CreateBy = (string)reader["CreateBy"];
						reader.Close();
						data.InvoiceAddress = GetCompanyInvoice(data.CustId);
						data.ShippingAddress = GetCompanyShipping(data.CompanyId).First(s => s.IsDefault == "Yes");
					}
				}
			}
			return data;
		}
		public bool IsCustIdInEproSystem(string custId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	CustID from TBCostcenter
										where	CustID=@CustID
										UNION
										select	DefaultCustID from TBCompanyInfo
										where	DefaultCustID=@CustID";
				command.Parameters.AddWithValue("@CustID", custId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					return reader.Read();
				}
			}
		}
		public bool IsCompanyIdInSystem(string companyId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select CompanyID from TBCompanyInfo where CompanyID=@CompanyID";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					return reader.Read();
				}
			}
		}

		//public bool IsOrderIdInOFMSystem(string custId)
		//{
		//    using (SqlCommand command = CreateCommandEnsureConnectionOpen())
		//    {
		//        command.CommandText = @"SELECT OrderID FROM DBWebOFM..TBOrder where CustID=@CustID AND OrderStatus IN ('Process','ProcessAndWaitPay','Waiting','ProcessAndPay')";
		//        command.Parameters.AddWithValue("@CustId", custId);
		//        using (SqlDataReader reader = command.ExecuteReader())
		//        {
		//            return reader.Read();
		//        }
		//    }
		//}

		public Company GetCompany(string companyId)
		{
			Company company = GetCompanyDetail(companyId);
			if (company != null)
			{
				company.ListCompanyAddress = GetAllCompanyAddress(companyId);
			}
			return company;
		}

		public Company GetCompanyDetail(string companyId)
		{
			Company company = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	T1.*,T2.DiscountRate
										from	TBCompanyInfo T1 inner join TBCustMaster T2 on T1.DefaultCustID = T2.CustID
										Where	CompanyId = @CompanyId";
				command.Parameters.AddWithValue("@CompanyId", companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						company = new Company();
						company.CompanyId = (string)reader["CompanyId"];
						company.CompanyThaiName = (string)reader["CompTName"];
						company.CompanyEngName = (string)reader["CompEName"];
						company.CompanyName = new LocalizedString((string)reader["CompTName"]);
						company.CompanyName["en"] = (string)reader["CompEName"];
						company.DefaultCustId = (string)reader["DefaultCustId"];
						company.IsCompModelThreeLevel = reader["CompModel"].ToString().Equals("3Level");
						company.CompanyDisCountRate = (decimal)reader["DiscountRate"];
						company.DefaultParkDay = (int)reader["DefaultParkDay"];
						company.OrderControlType = (Company.OrderControl)Enum.Parse(typeof(Company.OrderControl), (string)reader["OrderControlType"], true);
						company.BudgetLevelType = (Company.BudgetLevel)Enum.Parse(typeof(Company.BudgetLevel), (string)reader["BudgetLevelType"], true);
						company.BudgetPeriodType = (Company.BudgetPeriod)Enum.Parse(typeof(Company.BudgetPeriod), (string)reader["BudgetPeriodType"], true);
						company.DefaultLang = (User.Language)Enum.Parse(typeof(User.Language), (string)reader["DefaultLang"], true);
						company.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						company.OrderIDFormat = (Company.OrderIDFormatType)Enum.Parse(typeof(Company.OrderIDFormatType), (string)reader["OrderIDFormat"], true);
						company.UseOfmCatalog = Mapper.ToClass<bool>((string)reader["UseOfmCatalog"]);
						company.IsByPassAdmin = Mapper.ToClass<bool>((string)reader["IsByPassAdmin"]);
						company.IsByPassApprover = Mapper.ToClass<bool>((string)reader["IsByPassApprover"]);
						company.IsAutoApprove = Mapper.ToClass<bool>((string)reader["IsAutoApprove"]);
						company.IsDefaultDeliCharge = Mapper.ToClass<bool>((string)reader["DefaultDeliCharge"]);
						company.UseCompanyNews = Mapper.ToClass<bool>((string)reader["UseCompanyNews"]);
						company.UseOfmNews = Mapper.ToClass<bool>((string)reader["UseOfmNews"]);
						company.UseSMSFeature = Mapper.ToClass<bool>((string)reader["UseSMSFeature"]);
						company.ShowOFMCat = Mapper.ToClass<bool>((string)reader["ShowOFMCat"]);
						company.ShowContactUs = Mapper.ToClass<bool>((string)reader["ShowContactUs"]);
						company.ShowSpecialProd = Mapper.ToClass<bool>((string)reader["ShowSpecialProd"]);
						company.UseGoodReceive = Mapper.ToClass<bool>((string)reader["UseGoodReceive"]);
						company.GoodReceivePeriod = (int)reader["GoodReceivePeriod"];
						int period = (int)reader["RepasswordPeriod"];
						if (period == 30) { company.RePasswordText = "1Month"; }
						else if (period == 60) { company.RePasswordText = "2Month"; }
						else if (period == 90) { company.RePasswordText = "3Month"; }
						else { company.RePasswordText = "XMonth"; company.RePasswordPeriodFromUser = period.ToString(); }
					}
				}
			}
			return company;
		}

		public void UpdateCompanySetting(Company company, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBCompanyInfo set RePasswordPeriod = @RePasswordPeriod, DefaultLang = @DefaultLang, IsByPassAdmin = @IsByPassAdmin,
										IsByPassApprover = @IsByPassApprover, IsAutoApprove = @IsAutoApprove, UseCompanyNews = @UseCompanyNews, UseOfmNews = @UseOfmNews,
										UseSMSFeature = @UseSMSFeature, ShowOfmCat = @ShowOfmCat, ShowContactUs = @ShowContactUs, ShowSpecialProd = @ShowSpecialProd,
										DefaultParkDay = @DefaultParkDay,UpdateOn = getdate(), UpdateBy = @UserId Where CompanyId = @CompanyId ";
				command.Parameters.AddWithValue("@RePasswordPeriod", company.RePasswordPeriod);
				command.Parameters.AddWithValue("@DefaultLang", company.DefaultLang.ToString());
				command.Parameters.AddWithValue("@IsByPassAdmin", Mapper.ToDatabase(company.IsByPassAdmin));
				command.Parameters.AddWithValue("@IsByPassApprover", Mapper.ToDatabase(company.IsByPassApprover));
				command.Parameters.AddWithValue("@IsAutoApprove", Mapper.ToDatabase(company.IsAutoApprove));
				command.Parameters.AddWithValue("@UseCompanyNews", Mapper.ToDatabase(company.UseCompanyNews));
				command.Parameters.AddWithValue("@UseOfmNews", Mapper.ToDatabase(company.UseOfmNews));
				command.Parameters.AddWithValue("@UseSMSFeature", Mapper.ToDatabase(company.UseSMSFeature));
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@CompanyId", Mapper.ToDatabase(company.CompanyId));
				command.Parameters.AddWithValue("@ShowOfmCat", Mapper.ToDatabase(company.ShowOFMCat));
				command.Parameters.AddWithValue("@ShowContactUs", Mapper.ToDatabase(company.ShowContactUs));
				command.Parameters.AddWithValue("@ShowSpecialProd", Mapper.ToDatabase(company.ShowSpecialProd));
				command.Parameters.AddWithValue("@DefaultParkDay", company.DefaultParkDay);
				command.ExecuteNonQuery();
			}
		}

		public void UpdateCompanySettingFormAdmin(Company company, string userId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"update	TBCompanyInfo set CompModel = @CompModel, OrderControlType = @OrderControlType, BudgetLevelType = @BudgetLevelType, BudgetPeriodType = @BudgetPeriodType,
												PriceType = @PriceType, UseOfmCatalog = @UseOfmCatalog, OrderIDFormat = @OrderIDFormat, RePasswordPeriod = @RePasswordPeriod, DefaultLang = @DefaultLang,
												IsByPassAdmin = @IsByPassAdmin, IsByPassApprover = @IsByPassApprover, IsAutoApprove = @IsAutoApprove, UseCompanyNews = @UseCompanyNews, UseOfmNews = @UseOfmNews,
												UseSMSFeature = @UseSMSFeature, ShowOfmCat = @ShowOfmCat, ShowContactUs = @ShowContactUs, ShowSpecialProd = @ShowSpecialProd, DefaultParkDay = @DefaultParkDay,
												UpdateOn = getdate(), UpdateBy = @UserId
										where	CompanyId = @CompanyId ";
				command.Parameters.AddWithValue("@CompModel", company.IsCompModelThreeLevel ? "3Level" : "2Level");
				command.Parameters.AddWithValue("@OrderControlType", company.OrderControlType.ToString());
				command.Parameters.AddWithValue("@BudgetLevelType", company.BudgetLevelType.ToString());
				command.Parameters.AddWithValue("@BudgetPeriodType", company.BudgetPeriodType.ToString());
				command.Parameters.AddWithValue("@PriceType", company.PriceType.ToString());
				command.Parameters.AddWithValue("@UseOfmCatalog", Mapper.ToDatabase(company.UseOfmCatalog));
				command.Parameters.AddWithValue("@OrderIDFormat", company.OrderIDFormat.ToString());
				command.Parameters.AddWithValue("@RePasswordPeriod", company.RePasswordPeriod);
				command.Parameters.AddWithValue("@DefaultLang", company.DefaultLang.ToString());
				command.Parameters.AddWithValue("@IsByPassAdmin", Mapper.ToDatabase(company.IsByPassAdmin));
				command.Parameters.AddWithValue("@IsByPassApprover", Mapper.ToDatabase(company.IsByPassApprover));
				command.Parameters.AddWithValue("@IsAutoApprove", Mapper.ToDatabase(company.IsAutoApprove));
				command.Parameters.AddWithValue("@UseCompanyNews", Mapper.ToDatabase(company.UseCompanyNews));
				command.Parameters.AddWithValue("@UseOfmNews", Mapper.ToDatabase(company.UseOfmNews));
				command.Parameters.AddWithValue("@UseSMSFeature", Mapper.ToDatabase(company.UseSMSFeature));
				command.Parameters.AddWithValue("@ShowOfmCat", Mapper.ToDatabase(company.ShowOFMCat));
				command.Parameters.AddWithValue("@ShowContactUs", Mapper.ToDatabase(company.ShowContactUs));
				command.Parameters.AddWithValue("@ShowSpecialProd", Mapper.ToDatabase(company.ShowSpecialProd));
				command.Parameters.AddWithValue("@DefaultParkDay", company.DefaultParkDay);
				command.Parameters.AddWithValue("@CompanyId", company.CompanyId);
				command.Parameters.AddWithValue("@UserId", userId);
				command.ExecuteNonQuery();
			}
		}

		public IEnumerable<Shipping> GetCompanyShipping(string companyId)
		{
			List<Shipping> shippings = new List<Shipping>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	comp.CompanyID, ship.CustId, CompTName, CompEName, ship.ShipId, OfflineShipID, ShipContactor, ShipAddr1, ShipAddr2, ShipAddr3,
												ShipAddr4,ShipDistrict, ShipPhoneNo, ShipAmphur, ShipProvince, ShipZipCode, ShipRemark,ship.IsDefault,isnull(phone.ID,'') as PhoneID,
												isnull(phone.PhoneNo,'') as PhoneNo,isnull(phone.Extension,'') as Extension
										FROM	TBCustShipping ship
												INNER JOIN TBCustCompany cust ON ship.CustID=cust.CustID
												INNER JOIN TBCompanyInfo comp ON cust.CompanyID=comp.CompanyID
												LEFT JOIN TBCustomerPhone phone ON ship.ShipID=phone.SequenceID AND TypeDataSource = @TypeData and TypeNumber = @TypeNumber
										WHERE	cust.CompanyID = @CompanyId
												ORDER BY ship.CustID ASC";

				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@TypeNumber", Contact.TypeNumber.PhoneOut.ToString());
				command.Parameters.AddWithValue("@TypeData", Contact.TypedataSource.Shipping.ToString());

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Shipping shipping = new Shipping();
						shipping = new Shipping();
						shipping.ShipID = (int)reader["ShipID"];
						shipping.OfflineShipID = (int)reader["ShipID"];
						shipping.ShipContactor = (string)reader["ShipContactor"];
						shipping.PhoneID = (int)reader["PhoneID"];
						shipping.ShipPhoneNo = (string)reader["PhoneNo"];
						shipping.Extension = (string)reader["Extension"];
						shipping.ShipMobileNo = ""; // เตรียมไว้ก่อน มีเรื่อง SMS
						shipping.Address1 = (string)reader["ShipAddr1"];
						shipping.Address2 = (string)reader["ShipAddr2"];
						shipping.Address3 = (string)reader["ShipAddr3"];
						shipping.Address4 = (string)reader["ShipAddr4"];
						shipping.District = (string)reader["shipDistrict"];
						shipping.Amphur = (string)reader["ShipAmphur"];
						shipping.Province = (string)reader["ShipProvince"];
						shipping.ZipCode = (string)reader["ShipZipCode"];
						shipping.Remark = (string)reader["ShipRemark"];
						shipping.CustId = (string)reader["CustId"];
						shipping.IsDefault = (string)reader["IsDefault"];
						shippings.Add(shipping);
					}
				}
			}
			return shippings;
		}
		public Invoice GetCompanyInvoice(string custId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{

				command.CommandText = @" select InvAddr1,InvAddr2,InvAddr3,InvAddr4 from TBCustMaster Where CustID = @CustId";
				command.Parameters.AddWithValue("@CustId", custId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					Invoice invoice = null;

					if (reader.Read())
					{
						invoice = new Invoice();
						invoice.Address1 = (string)reader["InvAddr1"];
						invoice.Address2 = (string)reader["InvAddr2"];
						invoice.Address3 = (string)reader["InvAddr3"];
						invoice.Address4 = (string)reader["InvAddr4"];
					}
					return invoice;
				}
			}
		}

		public IEnumerable<CompanyAddress> GetAllCompanyAddress(string companyId)
		{
			List<CompanyAddress> item = new List<CompanyAddress>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	cust.CustID,mast.InvAddr1,count(ship.ShipID) as CountShip
										FROM	TBCustCompany cust
												INNER JOIN TBCustMaster mast ON cust.CustID=mast.CustID
												INNER JOIN TBCustShipping ship ON mast.CustID=ship.CustID
										WHERE	cust.CompanyID=@CompanyId
												GROUP BY cust.CustID,mast.InvAddr1";

				command.Parameters.AddWithValue("@CompanyId", companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						CompanyAddress company = new CompanyAddress();
						company.CustId = (string)reader["CustId"];
						company.InvoiceAddress1 = (string)reader["invaddr1"];
						company.CountShip = (int)reader["CountShip"];
						item.Add(company);
					}
				}
			}
			return item;
		}

		public IEnumerable<InformationRequesterLine> GetAllCompanyRequesterLine(string companyId)
		{
			List<InformationRequesterLine> item = new List<InformationRequesterLine>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT	t2.*,t1.ReqUserID,t3.UserTName AS ReqTName,t3.UserEName AS ReqEName,
												t1.APPUserID,t4.UserTName AS AppTName,t4.UserEName AS AppEName,
												t1.APPLevel,t1.APPCreditlimit,t1.ParkDay
										FROM	TBRequesterLine t1 with (nolock)
												INNER JOIN (
												SELECT	t1.CostcenterID,t1.CostTName,t1.CostEName,t1.CostStatus,t1.CompanyID,t1.CustID,
														isnull(t1.DepartmentID,'') AS DepartmentID,isnull(t2.DeptTName,'') AS DeptTName,isnull(t2.DeptEName,'') AS DeptEName
												FROM	TBCostcenter t1 with (nolock) LEFT JOIN TBDepartment t2 with (nolock) ON t1.DepartmentID=t2.DepartmentID
												WHERE	t1.CompanyID=@CompanyId
												) t2 ON t1.CompanyID=t2.CompanyID AND t1.CostcenterID=t2.CostcenterID
												INNER JOIN TBUserInfo t3 with (nolock) ON t1.ReqUserID=t3.UserID
												INNER JOIN TBUserInfo t4 with (nolock) ON t1.APPUserID=t4.UserID
												ORDER BY t1.CostcenterID";
				command.Parameters.AddWithValue("@CompanyId", companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						InformationRequesterLine reqLine = new InformationRequesterLine();
						reqLine.CostcenterID = (string)reader["CostcenterID"];
						reqLine.CostcenterName = new LocalizedString((string)reader["CostTName"]);
						reqLine.CostcenterName["en"] = (string)reader["CostEName"];
						reqLine.CostStatus = (string)reader["CostStatus"];
						reqLine.CustID = (string)reader["CustID"];
						reqLine.DepartmentID = (string)reader["DepartmentID"];
						reqLine.DepartmentName = new LocalizedString((string)reader["DeptTName"]);
						reqLine.DepartmentName["en"] = (string)reader["DeptEName"];
						reqLine.RequesterId = (string)reader["ReqUserID"];
						reqLine.RequesterName = new LocalizedString((string)reader["ReqTName"]);
						reqLine.RequesterName["en"] = (string)reader["ReqEName"];
						reqLine.ApproverId = (string)reader["APPUserID"];
						reqLine.ApproverName = new LocalizedString((string)reader["AppTName"]);
						reqLine.ApproverName["en"] = (string)reader["AppEName"];
						reqLine.ApproverLevel = (int)reader["APPLevel"];
						reqLine.ApproverCreditBudget = (decimal)reader["APPCreditlimit"];
						reqLine.Parkday = (int)reader["ParkDay"];
						item.Add(reqLine);
					}
				}
			}
			return item;
		}

		public IEnumerable<Company> GetAllCompanyBySearch(SearchCompany searchCompany)
		{
			List<Company> item = new List<Company>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder();
				sqlBuilder.AppendLine("SELECT t1.*,t2.DiscountRate FROM TBCompanyInfo t1");
				sqlBuilder.AppendLine("INNER JOIN TBCustMaster t2 on t1.DefaultCustID=t2.CustID");
				sqlBuilder.AppendLine("WHERE CompanyID <> 'Officemate'");
				if (searchCompany.CheckCompId && !string.IsNullOrEmpty(searchCompany.CompanyId))
				{
					sqlBuilder.AppendLine(" AND CompanyID LIKE @CompanyID");
					command.Parameters.AddWithValue("@CompanyID", "%" + searchCompany.CompanyId + "%");
				}
				if (searchCompany.CheckCompName && !string.IsNullOrEmpty(searchCompany.CompanyName))
				{
					sqlBuilder.AppendLine(" AND (CompTName LIKE @CompName OR CompEName LIKE @CompName)");
					command.Parameters.AddWithValue("@CompName", "%" + searchCompany.CompanyName + "%");
				}
				if (searchCompany.CheckCustId && !string.IsNullOrEmpty(searchCompany.CustId))
				{
					sqlBuilder.AppendLine(" AND DefaultCustID=@DefaultCustID");
					command.Parameters.AddWithValue("@DefaultCustID", searchCompany.CustId);
				}
				if (searchCompany.CheckCompStatus && !string.IsNullOrEmpty(searchCompany.CompanyStatus))
				{
					sqlBuilder.AppendLine(" AND CompStatus=@CompStatus");
					command.Parameters.AddWithValue("@CompStatus", searchCompany.CompanyStatus);
				}
				if (searchCompany.CheckPriceFormat && !string.IsNullOrEmpty(searchCompany.PriceFormat))
				{
					sqlBuilder.AppendLine(" AND PriceType=@PriceType");
					command.Parameters.AddWithValue("@PriceType", searchCompany.PriceFormat);
				}
				if (searchCompany.CheckOrderFormat && !string.IsNullOrEmpty(searchCompany.OrderFormat))
				{
					sqlBuilder.AppendLine(" AND OrderControlType=@OrderControlType");
					command.Parameters.AddWithValue("@OrderControlType", searchCompany.OrderFormat);
				}
				if (searchCompany.CheckCatalogFormat && !string.IsNullOrEmpty(searchCompany.CatalogFormat))
				{
					sqlBuilder.AppendLine(" AND UseOfmCatalog=@UseOfmCatalog");
					command.Parameters.AddWithValue("@UseOfmCatalog", searchCompany.CatalogFormat);
				}
				if (searchCompany.CheckSiteStatus && !string.IsNullOrEmpty(searchCompany.SiteStatus))
				{
					sqlBuilder.AppendLine(" AND IsSiteActive=@IsSiteActive");
					command.Parameters.AddWithValue("@IsSiteActive", searchCompany.SiteStatus);
				}
				if (searchCompany.CheckCreateDate && searchCompany.CreateDateFrom != DateTime.MinValue && searchCompany.CreateDateTo != DateTime.MinValue)
				{
					sqlBuilder.AppendLine(" AND t1.CreateOn BETWEEN @CreateDateFrom AND @CreateDateTo");
					command.Parameters.AddWithValue("@CreateDateFrom", searchCompany.CreateDateFrom);
					command.Parameters.AddWithValue("@CreateDateTo", searchCompany.CreateDateTo.AddDays(1));
				}
				if (searchCompany.CheckCreateBy && !string.IsNullOrEmpty(searchCompany.CreateBy))
				{
					sqlBuilder.AppendLine(" AND t1.CreateBy=@CreateBy ");
					command.Parameters.AddWithValue("@CreateBy", searchCompany.CreateBy);
				}
				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Company comp = new Company();
						comp.CompanyId = (string)reader["CompanyID"];
						comp.DefaultCustId = (string)reader["DefaultCustID"];
						comp.CompanyName = new LocalizedString((string)reader["CompTName"]);
						comp.CompanyName["en"] = (string)reader["CompEName"];
						comp.CompStatus = (Company.CompanyStatus)Enum.Parse(typeof(Company.CompanyStatus), (string)reader["CompStatus"], true);
						comp.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true);
						comp.OrderControlType = (Company.OrderControl)Enum.Parse(typeof(Company.OrderControl), (string)reader["OrderControlType"], true);
						comp.UseOfmCatalog = Mapper.ToClass<bool>((string)reader["UseOfmCatalog"]);
						comp.IsSiteActive = Mapper.ToClass<bool>((string)reader["IsSiteActive"]);
						comp.CreateOn = (DateTime)reader["CreateOn"];
						comp.CreateBy = (string)reader["CreateBy"];
						comp.CompanyModel = (string)reader["CompModel"];
						comp.SaleRequest = (string)reader["SaleRequest"];
						comp.DiscountRate = (decimal)reader["DiscountRate"];
						item.Add(comp);
					}
				}
			}
			return item;
		}


		public void InsertCustCompany(string custId, string companyId, string createBy,User.UserCompanyDefault isDefault)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"insert into TBCustCompany (CustID,CompanyID,IsDefault,CreateOn,CreateBy)
										values (@CustID, @CompanyID, @IsDefault, getdate(), @CreateBy)";

				command.Parameters.AddWithValue("@CustID", custId);
				command.Parameters.AddWithValue("@CompanyID", companyId.ToUpper());
				command.Parameters.AddWithValue("@IsDefault", isDefault.ToString());
				command.Parameters.AddWithValue("@CreateBy", createBy);
				command.ExecuteNonQuery();
			}
		}

		public bool IsCustExistCustCompany(string custId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select count(*) as CustId from TBCustCompany where CustID = @CustID";
				command.Parameters.AddWithValue("@CustID", custId);
				return ((int)command.ExecuteScalar() > 0);
			}
		}

	}
}