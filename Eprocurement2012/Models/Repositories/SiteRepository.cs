﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Web.Hosting;
using OfficeMate.Framework;
namespace Eprocurement2012.Models.Repositories
{
	public class SiteRepository : RepositoryBase
	{
		public SiteRepository(SqlConnection connection) : base(connection) { }
		public bool IsSiteActive(string companyId)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select IsSiteActive from TBCompanyInfo Where CompanyID = @CompanyID ";
				command.Parameters.AddWithValue("@companyID", companyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return String.Equals(reader["IsSiteActive"], Company.CompanyActive.Yes.ToString());
					}
				}
			}
			return false;
		}

		public Site GetDataActiveSite(string companyId, Site.OpenSite openSite)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select	Top 1 t1.CreateBy,t1.CreateOn,AdminRemark,t2.UserTname,t2.UserEname
										from	TBActivesitelog t1 inner join  TBUserInfo t2 on t1.CreateBy = t2.UserID
										Where	CompanyID = @CompanyID and IsSiteActive = @IsSiteActive
												Order By t1.CreateOn desc";
				command.Parameters.AddWithValue("@companyID", companyId);
				command.Parameters.AddWithValue("@IsSiteActive", openSite.ToString());
				using (SqlDataReader reader = command.ExecuteReader())
				{
					Site site = new Site();
					if (reader.Read())
					{
						site.AdminSiteUserId = (string)reader["CreateBy"];
						site.AdminSiteThaiName = (string)reader["UserTname"];
						site.AdminSiteEngName = (string)reader["UserEname"];
						site.AdminSiteName = new LocalizedString((string)reader["UserTname"]);
						site.AdminSiteName["en"] = (string)reader["UserEname"];
						site.AdminRemark = (string)reader["AdminRemark"];
						site.CloseSiteTime = (DateTime)reader["CreateOn"];
					}
					return site;
				}
			}

		}

		public void Closesite(string companyId)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBCompanyInfo set IssiteActive = @IssiteActive where CompanyID = @CompanyID";
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@IsSiteActive", Site.OpenSite.No.ToString());
				command.ExecuteNonQuery();
			}
		}

		public void InsertLogOpenSite(Site Site)
		{
			InsertLogOpenAndCloseSite(Site, Site.OpenSite.Yes);
		}

		public void InsertLogCloseSite(Site Site)
		{
			InsertLogOpenAndCloseSite(Site, Site.OpenSite.No);
		}

		private void InsertLogOpenAndCloseSite(Site Site, Site.OpenSite openSite)
		{
			if (Site == null) { throw new ArgumentNullException("closeSite"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Insert into TBActivesitelog(CompanyID,IsSiteActive,AdminRemark,CreateOn,Createby)values(@CompanyID,@IsSiteActive,@AdminRemark,getdate(),@Createby)";
				command.Parameters.AddWithValue("@CompanyID", Site.CompanyID);
				command.Parameters.AddWithValue("@AdminRemark", Site.AdminRemark + Site.AdminRemarkMail);
				command.Parameters.AddWithValue("@Createby", Site.AdminSiteUserId);
				command.Parameters.AddWithValue("@IsSiteActive", openSite.ToString());
				command.ExecuteNonQuery();
			}
		}

		public void SetSite(string companyId, Site.OpenSite openSite)
		{
			if (companyId == null) { throw new ArgumentNullException("companyId"); }
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBCompanyInfo Set IsSiteActive = @IsSiteActive Where CompanyID = @CompanyID ";
				command.Parameters.AddWithValue("@companyID", companyId);
				command.Parameters.AddWithValue("@IsSiteActive", openSite.ToString());
				command.ExecuteNonQuery();
			}
		}

	}
}