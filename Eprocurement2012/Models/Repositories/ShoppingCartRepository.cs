﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Transactions;
using System.Text;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class ShoppingCartRepository : RepositoryBase
	{
		public ShoppingCartRepository(SqlConnection connection) : base(connection) { }

		public int CreateCart(User user, string IPAddress)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Declare @NewSCID int
										Set @NewSCID = (Select IsNull(Max(SCID), 0) + 1 From TBCart)

										Insert Into TBCart (SCID,ScType,UserID,CompanyID,CountProduct,IPAddress,CreateOn,CreateBy,UpdateOn,UpdateBy)
										Values (@NewSCID,@ScType,@UserId,@CompanyID,0,@IPAddress,GetDate(),@UserId,GetDate(),@UserId)
										Select @NewSCID
										";
				command.Parameters.AddWithValue("@UserId", user.UserId);
				command.Parameters.AddWithValue("@CompanyID", user.CurrentCompanyId);
				command.Parameters.AddWithValue("@ScType", Cart.CartType.New.ToString());
				command.Parameters.AddWithValue("@IPAddress", IPAddress);

				int result = (int)command.ExecuteScalar();
				return result;
			}
		}

		public int CreateCartRevise(User user, string refOrderId, string IPAddress)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Declare @NewSCID int
										Set @NewSCID = (Select IsNull(Max(SCID), 0) + 1 From TBCart)

										Insert Into TBCart (SCID,ScType,RefOrderID,UserID,CompanyID,CountProduct,IPAddress,CreateOn,CreateBy,UpdateOn,UpdateBy)
										Values (@NewSCID,@ScType,@RefOrderID,@UserId,@CompanyID,0,@IPAddress,GetDate(),@UserId,GetDate(),@UserId)
										Select @NewSCID
										";
				command.Parameters.AddWithValue("@UserId", user.UserId);
				command.Parameters.AddWithValue("@CompanyID", user.CurrentCompanyId);
				command.Parameters.AddWithValue("@RefOrderID", refOrderId);
				command.Parameters.AddWithValue("@ScType", Cart.CartType.Revise.ToString());
				command.Parameters.AddWithValue("@IPAddress", IPAddress);
				int result = (int)command.ExecuteScalar();
				return result;
			}
		}

		public void SaveCart(IDictionary<string, int> cart, int scid, User user, string IPAddress)
		{
			using (TransactionScope scope = new TransactionScope())
			{
				using (SqlCommand command = CreateCommandEnsureConnectionOpen())
				{
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.AppendLine("Delete From TBCartDetail Where SCID = @SCID AND RefOrderID = ''");
					int rowIndex = 0;
					foreach (var keyPair in cart)
					{
						sqlBuilder.AppendFormat("Insert Into TBCartDetail (SCID,pID,pQty,ReferenceCode,CreateOn,CreateBy,UpdateOn,UpdateBy) Values (@SCID, @pID_{0}, @pQty_{0}, '', GetDate(), @UserId, GetDate(), @UserId)", rowIndex);
						sqlBuilder.AppendLine();
						command.Parameters.AddWithValue("@pID_" + rowIndex, keyPair.Key);
						command.Parameters.AddWithValue("@pQty_" + rowIndex, keyPair.Value);
						rowIndex++;
					}
					//sqlBuilder.AppendLine("Update TBCart Set CountProduct=@Count Where SCID = @SCID");
					sqlBuilder.AppendLine("Update TBCart Set CountProduct=@Count,CustFileName='',SystemFileName='' Where SCID = @SCID");
					command.CommandText = sqlBuilder.ToString();
					command.Parameters.AddWithValue("@SCID", scid);
					command.Parameters.AddWithValue("@UserId", user.IsLogin ? user.UserId : String.Empty);
					command.Parameters.AddWithValue("@CompanyId", user.CurrentCompanyId);
					command.Parameters.AddWithValue("@Count", cart.Sum(cartItem => cartItem.Value));
					command.ExecuteNonQuery();
				}
				scope.Complete();
			}
		}

		public void SaveCartRevise(IDictionary<string, int> cartRevise, int scid, User user, string refOrderId, string IPAddress)
		{
			using (TransactionScope scope = new TransactionScope())
			{
				using (SqlCommand command = CreateCommandEnsureConnectionOpen())
				{
					StringBuilder sqlBuilder = new StringBuilder();
					sqlBuilder.AppendLine("Delete From TBCartDetail Where SCID = @scid AND RefOrderID <> ''");
					int rowIndex = 0;
					foreach (var keyPair in cartRevise)
					{
						sqlBuilder.AppendFormat("Insert Into TBCartDetail (SCID,RefOrderID,pID,pQty,ReferenceCode,CreateOn,CreateBy,UpdateOn,UpdateBy) Values (@SCID,@refOrderID, @pID_{0}, @pQty_{0}, '', GetDate(), @UserId, GetDate(), @UserId)", rowIndex);
						sqlBuilder.AppendLine();
						command.Parameters.AddWithValue("@pID_" + rowIndex, keyPair.Key);
						command.Parameters.AddWithValue("@pQty_" + rowIndex, keyPair.Value);
						rowIndex++;
					}
					//sqlBuilder.AppendLine("Update TBCart Set CountProduct=@Count Where SCID = @SCID");
					sqlBuilder.AppendLine("Update TBCart Set CountProduct=@Count,CustFileName='',SystemFileName='' Where SCID = @SCID");
					command.CommandText = sqlBuilder.ToString();
					command.Parameters.AddWithValue("@SCID", scid);
					command.Parameters.AddWithValue("@refOrderID", refOrderId);
					command.Parameters.AddWithValue("@UserId", user.IsLogin ? user.UserId : String.Empty);
					command.Parameters.AddWithValue("@CompanyId", user.CurrentCompanyId);
					command.Parameters.AddWithValue("@Count", cartRevise.Sum(cartItem => cartItem.Value));
					command.ExecuteNonQuery();
				}
				scope.Complete();
			}
		}

		public void ClearCartRevise(int scid)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Delete From TBCartDetail Where SCID = @SCID and RefOrderId <> ''

										Update TBCart Set ScType=@ScType, RefOrderId='', CountProduct=isnull((SELECT Sum(pQty) FROM TBCartDetail Where SCID = @SCID),0) Where SCID = @SCID
										";
				command.Parameters.AddWithValue("@SCID", scid);
				command.Parameters.AddWithValue("@ScType", Cart.CartType.New.ToString());
				command.ExecuteNonQuery();
			}
		}

		public void ChangeCartReviseToNew(int scid)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Delete From TBCartDetail Where SCID = @SCID and RefOrderId = ''

										Update TBCartDetail Set RefOrderId = '' Where SCID = @SCID
										Update TBCart Set ScType=@ScType, RefOrderId='', CountProduct=(SELECT Sum(pQty) FROM TBCartDetail Where SCID = @SCID) Where SCID = @SCID
										";
				command.Parameters.AddWithValue("@SCID", scid);
				command.Parameters.AddWithValue("@ScType", Cart.CartType.New.ToString());
				command.ExecuteNonQuery();
			}
		}

		public void ChangeCartTypeToRevise(int scid, string orderId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Delete From TBCartDetail Where SCID = @SCID and RefOrderId <> ''

										Update TBCart Set ScType=@ScType, RefOrderId=@orderId Where SCID = @SCID
										";
				command.Parameters.AddWithValue("@SCID", scid);
				command.Parameters.AddWithValue("@orderId", orderId);
				command.Parameters.AddWithValue("@ScType", Cart.CartType.Revise.ToString());
				command.ExecuteNonQuery();
			}
		}

		public string UpdateAttachFile(int scid, string userId, string companyId, string custFileName,string typeFile)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update	TBCart Set UserId = @UserId, CompanyId = @CompanyId, CustFileName = @CustFileName,SystemFileName = @SystemFileName
										Where	SCID = @SCID

										SELECT SystemFileName FROM TBCart where SCID=@SCID
										";

				string systemFileName = string.Format("attach_{0}_{1}{2}", scid, DateTime.Now.ToString("yyyyMMddHHmmssff"), typeFile);
				command.Parameters.AddWithValue("@UserId", userId);
				command.Parameters.AddWithValue("@CompanyId", companyId);
				command.Parameters.AddWithValue("@CustFileName", custFileName);
				command.Parameters.AddWithValue("@SCID", scid);
				command.Parameters.AddWithValue("@SystemFileName", systemFileName);
				string result = (string)command.ExecuteScalar();
				return result;
			}
		}

		public string UpdateAttachFileByRevise(int scid, string custFileName, string guid, string typeFile)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"UPDATE	TBOrder SET CustFileName=@CustFileName, SystemFileName=@SystemFileName
										WHERE	OrderGUID=@Guid

										SELECT SystemFileName FROM TBOrder where OrderGUID=@Guid
										";

				string systemFileName = string.Format("attach_{0}_{1}{2}", scid, DateTime.Now.ToString("yyyyMMddHHmmssff"), typeFile);
				command.Parameters.AddWithValue("@CustFileName", custFileName);
				command.Parameters.AddWithValue("@SCID", scid);
				command.Parameters.AddWithValue("@Guid", guid);
				command.Parameters.AddWithValue("@SystemFileName", systemFileName);
				string result = (string)command.ExecuteScalar();
				return result;
			}
		}

		public void DeleteAttachFile(int scid)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update	TBCart Set CustFileName = '',SystemFileName = '' Where	SCID = @SCID";

				command.Parameters.AddWithValue("@SCID", scid);
				command.ExecuteNonQuery();
			}
		}

		public void DeleteAttachFileByRevise(string guid)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"UPDATE TBOrder SET CustFileName='' , SystemFileName='' WHERE OrderGUID=@Guid";

				command.Parameters.AddWithValue("@Guid",guid);
				command.ExecuteNonQuery();
			}
		}

	}
}
