﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Xml.Linq;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class DepartmentRepository : UserFilteredRepository
	{
		public DepartmentRepository(SqlConnection connection, User user) : base(connection, user) { }

		public IEnumerable<Department> GetProductBreadCrumbs(string productId, string paths)
		{

			//Find Default DeptId
			Product product = new ProductRepository(Connection, User).GetProductDetail(productId);
			int currentDeptId = product.DeptId;

			if (!String.IsNullOrEmpty(paths))
			{
				String[] deptPaths = (paths ?? string.Empty).Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
				Int32.TryParse(deptPaths[deptPaths.Length - 1], out currentDeptId);
				if (!CheckDeptExist(currentDeptId))
				{
					currentDeptId = product.DeptId;
				}
				//เอา Dept สุดท้ายใน paths ออก แล้วทำเป็นสตริงเหมือนเดิม
				deptPaths = deptPaths.Take(Math.Max(deptPaths.Length - 1, 0)).ToArray();
				string Paths = String.Join("-", deptPaths);
				return GetBreadCrumbs(currentDeptId, Paths);
			}
			else
			{
				return GetBreadCrumbs(currentDeptId, paths);
			}
		}

		public IEnumerable<Department> GetBreadCrumbs(int currentDeptId, string paths)
		{
			List<Department> result = new List<Department>();
			Department currentDept = new Department();
			currentDept = GetDepartment(currentDeptId);
			if (currentDept == null) { return result.Reverse<Department>(); }
			String[] deptPaths = (paths ?? string.Empty).Split(new char[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
			int pathIndex = deptPaths.Length - 1;
			int parentId;
			if (pathIndex >= 0 && Int32.TryParse(deptPaths[pathIndex], out parentId))
			{
				currentDept = GetDepartment(currentDept.Id, parentId);
				pathIndex--;
			}
			else
			{
				currentDept = GetDepartment(currentDept.Id);
				pathIndex = -1;
			}
			result.Add(currentDept);
			while (currentDept.ParentId != 0)
			{
				if (pathIndex >= 0 && Int32.TryParse(deptPaths[pathIndex], out parentId))
				{
					currentDept = GetDepartment(currentDept.ParentId, parentId);
					pathIndex--;
				}
				else
				{
					currentDept = GetDepartment(currentDept.ParentId);
					pathIndex = -1;
				}

				result.Add(currentDept);
			}
			return result.Reverse<Department>();
		}

		public Department GetDepartment(int id) { return GetDepartment(id, null); }

		public Department GetDepartment(int id, int? parentId)
		{
			if (parentId.HasValue)
			{
				using (SqlCommand command = CreateCommandEnsureConnectionOpen())
				{
					command.CommandText = @"Select	S.DeptId,S.ParentDeptId,S.RefId,S.HasSubDept,M.DeptThaiName,M.DeptEngName,
													S.ThemeDownFormat,S.ThemeUpFormat,S.ProductListLayout,S.DownFormat,
													S.UpFormat,M.DeptSeoName,S.MetaDescription,S.MetaKeyword,S.Title,
													S.DefaultSorting,S.IsDefault
											From	TBDeptStructure as S
													inner join TBDeptMaster as M On S.DeptId=M.DeptId
											Where	S.DeptStatus='Active' And S.DeptId=@DeptId And S.ParentDeptId=@ParentDeptId";
					command.Parameters.AddWithValue("@DeptId", id);
					command.Parameters.AddWithValue("@ParentDeptId", parentId.Value);
					using (SqlDataReader reader = command.ExecuteReader())
					{
						if (reader.Read())
						{
							return MapDepartment(reader);
						}
					}
				}
			}

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Select	S.DeptId,S.ParentDeptId,S.RefId,S.HasSubDept,M.DeptThaiName,M.DeptEngName,
												S.ThemeDownFormat,S.ThemeUpFormat,S.ProductListLayout,S.DownFormat,
												S.UpFormat,M.DeptSeoName,S.MetaDescription,S.MetaKeyword,S.Title,
												S.DefaultSorting,S.IsDefault
										From	TBDeptStructure as S
												inner join TBDeptMaster as M On S.DeptId=M.DeptId
										Where	S.DeptStatus='Active' And S.DeptId=@DeptId And S.IsDefault = 'Yes'";
				command.Parameters.AddWithValue("@DeptId", id);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return MapDepartment(reader);
					}
				}
			}

			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Select	S.DeptId,S.ParentDeptId,S.RefId,S.HasSubDept,M.DeptThaiName,M.DeptEngName,
												S.ThemeDownFormat,S.ThemeUpFormat,S.ProductListLayout,S.DownFormat,
												S.UpFormat,M.DeptSeoName,S.MetaDescription,M.MetaKeyword,S.Title,
												S.DefaultSorting,S.IsDefault
										From	TBDeptStructure as S
												inner join TBDeptMaster as M On S.DeptId=M.DeptId
										Where	S.DeptStatus='Active' And S.DeptId=@DeptId And S.IsDefault = 'No'";
				command.Parameters.AddWithValue("@DeptId", id);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						return MapDepartment(reader);
					}
				}
			}

			return null;
		}

		public IEnumerable<PropertyDept> GetPropertyByDeptId(int deptId)
		{
			List<PropertyDept> result = new List<PropertyDept>();
			List<PropertyDept> _tempdata = new List<PropertyDept>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select * from TBPropDept with (nolock) where DeptId=@DeptId and IsDeptFilter='Yes'";
				command.Parameters.AddWithValue("@DeptId", deptId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						PropertyDept propDept = new PropertyDept();
						propDept.DeptId = (int)reader["DeptId"];
						propDept.Name = (string)reader["PropName"];
						_tempdata.Add(propDept);
					}
				}
			}
			foreach (var item in _tempdata)
			{
				item.Choices = GetPropertyChoice(item.Name, deptId);
			}
			if (_tempdata.Any(c => c.Choices.Any()))
			{
				result = _tempdata;
			}
			return result;
		}

		public IEnumerable<PropertyChoice> GetPropertyChoice(string propName, int deptId)
		{
			List<PropertyChoice> result = new List<PropertyChoice>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog)
				{
					command.CommandText = @"SELECT	distinct tpc.*
											FROM	TBPropChoice tpc with (nolock) INNER JOIN TBPropProduct tpp with (nolock) ON tpc.RefID=tpp.RefID
													INNER JOIN TBProductCatalog tpcl with (nolock) ON tpp.PID=ProductID
											WHERE	tpc.Propname=@PropName AND tpc.DeptID=@DeptId AND tpp.Status NOT IN ('Delete','Hold')
													and ((tpcl.CompanyID=@companyId and tpcl.CatalogtypeID=@companyId) or (tpcl.CompanyID=@companyId and tpcl.CatalogtypeID=@userId))";
				}
				else
				{
					command.CommandText = @"SELECT	distinct tpc.*
											FROM	TBPropChoice tpc with (nolock) INNER JOIN TBPropProduct tpp with (nolock) ON tpc.RefID=tpp.RefID
											WHERE	tpc.Propname=@PropName AND tpc.DeptID=@DeptId AND tpp.Status NOT IN ('Delete','Hold')";
				}
				command.Parameters.AddWithValue("@DeptId", deptId);
				command.Parameters.AddWithValue("@PropName", propName);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						PropertyChoice propChoice = new PropertyChoice();
						propChoice.Name = (string)reader["PropName"];
						propChoice.Value = (string)reader["ChoiceName"];
						propChoice.RefId = (string)reader["RefId"];
						propChoice.ChoiceImageName = (string)reader["ChoiceImageName"];
						result.Add(propChoice);
					}
				}
				return result;
			}
		}

		public IEnumerable<Department> GetChildrenAndGrandChildrenDepartments(int parentDeptId)
		{
			List<Department> result = new List<Department>();
			if (parentDeptId == 0)
			{
				foreach (var deptIds in GetAccessibleDept())
				{
					result.Add(GetDepartment(deptIds));
				}
			}
			else
			{
				XDocument doc = GetAccessibleDoc();
				XElement selected = FindDept(doc.Element("Cats"), parentDeptId);
				foreach (var dept in selected.Descendants("Department"))
				{
					result.Add(GetDepartment(Convert.ToInt32(dept.Attribute("Id").Value)));
				}
			}
			return result;
		}

		public IEnumerable<Brand> GetBrandFilter(int deptId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (!User.Company.UseOfmCatalog)
				{
					command.CommandText = @"SELECT	DISTINCT t1.*
											FROM	TBBrandFilter AS t1 with (nolock)
													inner join TBProductCenter AS t2 with (nolock) ON t1.BrandID = t2.BrandID and t2.status not in ('Delete','Hold')
													inner join TBProductCatalog AS t3 with (nolock) ON (t2.pid = t3.ProductID and ((t3.CompanyID=@companyId and t3.CatalogtypeID=@companyId) or (t3.CompanyID=@companyId and t3.CatalogtypeID=@userId )))
											WHERE	t1.DeptId=@DeptId and t1.BrandName <> t1.DeptName
													Order By t1.BrandId ASC";
				}
				else
				{
					command.CommandText = @"SELECT	DISTINCT t1.*
											FROM	TBBrandFilter AS t1 with (nolock)
													inner join TBProductCenter AS t2 with (nolock) ON t1.BrandID = t2.BrandID and t2.status not in ('Delete','Hold') and t2.IsSpcCatEpro='No'
											WHERE	t1.DeptId=@DeptId and t1.BrandName <> t1.DeptName
													Order By t1.BrandId ASC";
				}
				command.Parameters.AddWithValue("@DeptId", deptId);
				command.Parameters.AddWithValue("@userId", User.UserId);
				command.Parameters.AddWithValue("@companyId", User.CurrentCompanyId);
				List<Brand> brandList = new List<Brand>();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Brand brand = new Brand();
						brand.DeptId = (int)reader["DeptId"];
						brand.RefId = (string)reader["RefId"];
						brand.DeptName = (string)reader["DeptName"];
						brand.BrandName = (string)reader["BrandName"];
						brand.CountProduct = (int)reader["CountPid"];
						brand.BrandId = (string)reader["brandId"];
						brand.BrandImageName = (string)reader["BrandImage"];
						brandList.Add(brand);
					}
				}
				return brandList;
			}
		}

		public IEnumerable<Department> GetRelateDepartment(Department currentDept)
		{
			List<Department> listOfRelateDepartment = new List<Department>();

			List<int> deptIds = new List<int>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select RelateID from tbrelate with (nolock) where SourceID = @currentDeptId And TypeName = 'Dept' and Status='Active'";
				command.Parameters.AddWithValue("@currentDeptId", currentDept.Id);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						deptIds.Add((int)reader["RelateID"]);
					}
				}
			}

			List<int> result = deptIds.Intersect(GetAccessibleDept()).ToList();

			foreach (var deptId in result)
			{
				listOfRelateDepartment.Add(GetDepartment(deptId));
			}
			return listOfRelateDepartment;
		}

		public IEnumerable<Department> GetSiblingDepartment(Department currentDept)
		{
			List<Department> listOfSiblingDepartment = new List<Department>();

			XDocument doc = GetAccessibleDoc();
			XElement selected = FindDept(doc.Element("Cats"), currentDept.Id);
			foreach (var dept in selected.Parent.Elements("Department"))
			{
				if (currentDept.Id != Convert.ToInt32(dept.Attribute("Id").Value))
				{
					listOfSiblingDepartment.Add(GetDepartment(Convert.ToInt32(dept.Attribute("Id").Value)));
				}
			}
			return listOfSiblingDepartment;
		}

		public IEnumerable<Department> GetSimilarDepartment(Department currentDept)
		{
			List<Department> listOfRelateDepartment = new List<Department>();
			List<int> deptIds = new List<int>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select RelateID from tbrelate with (nolock) where SourceID = @currentDeptId And TypeName = 'SimilarDept' and Status='Active'";
				command.Parameters.AddWithValue("@currentDeptId", currentDept.Id);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						deptIds.Add((int)reader["RelateID"]);
					}
				}
			}

			List<int> result = deptIds.Intersect(GetAccessibleDept()).ToList();
			foreach (var deptid in result)
			{
				listOfRelateDepartment.Add(GetDepartment(deptid));
			}
			return listOfRelateDepartment;
		}

		public Ancestor GetNavigatorList(Department currentDept)
		{
			XDocument doc = GetAccessibleDoc();

			XElement selected = FindDept(doc.Element("Cats"), currentDept.Id);

			Ancestor ancester = new Ancestor();
			ancester.AncestorDepartment = GetDepartment(Convert.ToInt32(selected.Attribute("Id").Value));
			ancester.ParentDept = GetDepartment(ancester.AncestorDepartment.ParentId);
			List<Child> listChild = new List<Child>();


			foreach (var deptElement in selected.Elements("Department"))
			{
				Child child = new Child();
				child.ChildDepartment = GetDepartment(Convert.ToInt32(deptElement.Attribute("Id").Value));
				listChild.Add(child);
				List<Department> listGrandChild = new List<Department>();
				foreach (var grandChild in deptElement.Elements("Department"))
				{
					listGrandChild.Add(GetDepartment(Convert.ToInt32(grandChild.Attribute("Id").Value)));
				}
				child.GrandChild = listGrandChild;
			}
			ancester.ChildDept = listChild;
			return ancester;
		}

		private bool CheckDeptExist(int deptId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select count(*) from TBDeptStructure where deptid=@DeptId";
				command.Parameters.AddWithValue("@DeptId", deptId);
				if ((int)command.ExecuteScalar() > 0)
				{
					return true;
				}
				else
				{
					return false;
				}
			}
		}

		private Department MapDepartment(SqlDataReader reader)
		{
			Department dept = new Department();
			var columns = new HashSet<string>(StringComparer.OrdinalIgnoreCase);
			for (int i = 0; i < reader.FieldCount; i++)
			{
				columns.Add(reader.GetName(i));
			}
			dept.Id = (int)reader["DeptId"];
			dept.ParentId = (int)reader["ParentDeptId"];
			dept.RefId = (string)reader["RefId"];
			dept.Name = new LocalizedString((string)reader["DeptThaiName"]);
			dept.Name["en"] = (string)reader["DeptEngName"];
			dept.HasSubDept = Mapper.ToClass<bool>(reader["HasSubDept"]);
			dept.DisplayTypeDown = Mapper.ToClass<DepartmentDisplayType>(reader["DownFormat"].ToString().ToLower());
			dept.DisplayTypeUp = Mapper.ToClass<DepartmentDisplayType>(reader["UpFormat"].ToString().ToLower());
			dept.ThemeTypeDown = Mapper.ToClass<DepartmentThemeType>(reader["ThemeDownFormat"].ToString().ToLower());
			dept.ThemeTypeUp = Mapper.ToClass<DepartmentThemeType>(reader["ThemeUpFormat"].ToString().ToLower());
			dept.ProductListViewType = Mapper.ToClass<DepartmentViewType>(reader["ProductListLayout"].ToString().ToLower());
			dept.MetaDescription = columns.Contains("MetaDescription") ? (string)reader["MetaDescription"] : "";
			dept.MetaKeyword = columns.Contains("MetaKeyword") ? (string)reader["MetaKeyword"] : "";
			dept.Title = columns.Contains("Title") ? (string)reader["Title"] : "";
			try
			{
				dept.DefaultSortBy = Mapper.ToClass<DepartmentSortByType>(reader["DefaultSorting"].ToString().ToLower());
			}
			catch (KeyNotFoundException)
			{
				dept.DefaultSortBy = DepartmentSortByType.New;
				_log.ErrorFormat("{0} is not an System.Enum.", reader["DefaultSorting"].ToString());
			}
			dept.IsDefault = columns.Contains("IsDefault") ? Mapper.ToClass<bool>((string)reader["IsDefault"]) : false;

			return dept;
		}

		public LocalizedString GetBrandName(string refId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select * from TBBrandFilter with (nolock) where RefId=@RefId";
				command.Parameters.AddWithValue("@RefId", refId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					LocalizedString result = new LocalizedString("");
					if (reader.Read())
					{
						result = new LocalizedString((string)reader["BrandName"]);
						result["en"] = (string)reader["BrandName"];
						if (result == "NonBrand")
						{
							result = new LocalizedString("Other Brand");
							result["en"] = "Other Brand";
						}
					}
					return result;
				}
			}
		}

		public LocalizedString GetPropertyName(string refId)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select PropName,ChoiceName from TBPropChoice where RefId=@RefId";
				command.Parameters.AddWithValue("@RefId", refId);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					LocalizedString result = new LocalizedString("");
					if (reader.Read())
					{
						if(string.Equals((string)reader["ChoiceName"], "Yes", StringComparison.OrdinalIgnoreCase))
						{
							result = new LocalizedString((string)reader["PropName"]);
							result["en"] = (string)reader["PropName"];
						}
						else
						{
							result = new LocalizedString((string)reader["ChoiceName"]);
							result["en"] = (string)reader["ChoiceName"];
						}
					}
					return result;
				}
			}
		}

		public IEnumerable<Department> GetSiblings(int currentDeptId)
		{
			List<Department> deptParent = new List<Department>();
			XDocument doc = GetAccessibleDoc();
			if (currentDeptId == 0)
			{
				var structure = doc.Element("Cats").Elements("Department").ToList();
				foreach (var dept in structure)
				{
					deptParent.Add(GetDepartment(Convert.ToInt32(dept.Attribute("Id").Value)));
				}
			}
			else
			{
				XElement selected = FindDept(doc.Element("Cats"), currentDeptId);
				foreach (var dept in selected.Parent.Elements("Department"))
				{
					deptParent.Add(GetDepartment(Convert.ToInt32(dept.Attribute("Id").Value)));
				}
			}
			return deptParent;
		}

		private XElement FindDept(XElement node, int currentDeptId)
		{
			XAttribute id = node.Attribute("Id");
			if (id != null && id.Value == currentDeptId.ToString())
			{
				return node;
			}
			else
			{
				foreach (var childNode in node.Elements("Department"))
				{
					var result = FindDept(childNode, currentDeptId);
					if (result != null)
					{
						return result;
					}
				}
			}
			return null;
		}

		private List<int> GetAccessibleDept()
		{
			List<int> listDept = new List<int>();
			XDocument doc = GetAccessibleDoc();

			foreach (var item in doc.Element("Cats").Descendants("Department"))
			{
				listDept.Add(Convert.ToInt32(item.Attribute("Id").Value));
			}
			return listDept;
		}

		private XDocument GetAccessibleDoc()
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (User.Company.UseOfmCatalog)
				{
					command.CommandText = "select Deptid from TBDeptStructureControl where userid = @userid and companyid = @companyid";
					command.Parameters.AddWithValue("@userid", "OFMCatalog");
					command.Parameters.AddWithValue("@companyid", "Officemate");
				}
				else
				{
					command.CommandText = "select Deptid from TBDeptStructureControl where userid = @userid and companyid = @companyid";
					command.Parameters.AddWithValue("@userid", User.UserId);
					command.Parameters.AddWithValue("@companyid", User.CurrentCompanyId);
				}
				return XDocument.Parse((string)command.ExecuteScalar());
			}
		}

	}
}