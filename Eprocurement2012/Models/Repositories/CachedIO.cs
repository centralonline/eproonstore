﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.IO;
using System.Web.Caching;

namespace Eprocurement2012.Models.Repositories
{
	public static class CachedIO
	{
		public static bool PathExists(string path)
		{
			bool result;
			string cacheKey = "PathExist/" + path;
			object cacheObject = HostingEnvironment.Cache.Get(cacheKey);
			if (cacheObject == null)
			{
				string localPath = HostingEnvironment.MapPath(path);
				result = File.Exists(localPath);
				HostingEnvironment.Cache.Insert(cacheKey, result, new CacheDependency(Path.GetDirectoryName(localPath)));
			}
			else
			{
				result = (bool)cacheObject;
			}
			return result;
		}
	}
}