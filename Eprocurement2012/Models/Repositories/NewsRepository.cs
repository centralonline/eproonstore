﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class NewsRepository : RepositoryBase
	{
		public NewsRepository(SqlConnection connection) : base(connection) { }
		public IEnumerable<News> GetAllNewsCompany(string companyId)
		{
			List<News> item = new List<News>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select t1.*,t2.UserGuID from TBNews t1 INNER JOIN TBUsers t2 on t1.CreateBy=t2.UserId where CompanyID = @companyId AND datediff(day,ValidFrom,getdate())>=0 AND datediff(day,getdate(),Validto)>=0 order by CreateOn desc";
				command.Parameters.AddWithValue("@companyId",companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						News data = new News();
						data.NewsGUID = (string)reader["NewsGUID"];
						data.NewsTitle = (string)reader["NewsTitle"];
						data.NewsDetail = (string)reader["NewsDetail"];
						data.NewsType = (string)reader["NewsType"];
						data.ValidFrom = (DateTime)reader["ValidFrom"];
						data.ValidTo = (DateTime)reader["ValidTo"];
						data.Priority = (string)reader["Priority"];
						data.NewsStatus = (string)reader["NewsStatus"];
						data.CreateBy = (string)reader["CreateBy"];
						data.CreateGuid = (string)reader["UserGuID"];
						item.Add(data);
					}
				}
			}
			return item;
		}

		public IEnumerable<News> GetAllOFMNews()
		{
			List<News> item = new List<News>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"select * from TBNews with (nolock) where NewsType = @NewsType order by CreateOn desc";
				command.Parameters.AddWithValue("@NewsType", "Public");

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						News data = new News();
						data.NewsGUID = (string)reader["NewsGUID"];
						data.NewsTitle = (string)reader["NewsTitle"];
						data.NewsDetail = (string)reader["NewsDetail"];
						data.NewsType = (string)reader["NewsType"];
						data.ValidFrom = (DateTime)reader["ValidFrom"];
						data.ValidTo = (DateTime)reader["ValidTo"];
						data.Priority = (string)reader["Priority"];
						data.NewsStatus = (string)reader["NewsStatus"];
						item.Add(data);
					}
				}
			}
			return item;
		}

		public IEnumerable<News> GetAllNews(string companyId)
		{
			List<News> item = new List<News>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = "select * from TBNews with (nolock) ";
				if (!string.IsNullOrEmpty(companyId))
				{
					command.CommandText += " where companyId=@companyId";
					command.Parameters.AddWithValue("@companyId", companyId);
				}
				command.CommandText += " order by CreateOn desc";

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						News data = new News();
						data.CompanyID = (string)reader["CompanyID"];
						data.NewsGUID = (string)reader["NewsGUID"];
						data.NewsTitle = (string)reader["NewsTitle"];
						data.NewsDetail = (string)reader["NewsDetail"];
						data.NewsType = (string)reader["NewsType"];
						data.ValidFrom = (DateTime)reader["ValidFrom"];
						data.ValidTo = (DateTime)reader["ValidTo"];
						data.Priority = (string)reader["Priority"];
						data.NewsStatus = (string)reader["NewsStatus"];
						item.Add(data);
					}
				}
			}
			return item;
		}

		public News GetNewsDetail(string newsGuid, string companyId)
		{
			News data = null;
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (string.IsNullOrEmpty(companyId))
				{
					command.CommandText = @"select * from TBNews Where NewsGuid = @NewsGuid";
				}
				else
				{
					command.CommandText = @"select * from TBNews Where CompanyId = @CompanyId and NewsGuid = @NewsGuid";
				}
				command.Parameters.AddWithValue("@NewsGuid", newsGuid);
				command.Parameters.AddWithValue("@CompanyId", companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					if (reader.Read())
					{
						data = new News();
						data.CompanyID = (string)reader["CompanyID"];
						data.NewsTitle = (string)reader["NewsTitle"];
						data.NewsDetail = (string)reader["NewsDetail"];
						data.ValidFrom = (DateTime)reader["ValidFrom"];
						data.ValidTo = (DateTime)reader["ValidTo"];
						data.ValidFromDisplay = data.ValidFrom.ToString("d/M/yyyy");
						data.ValidToDisplay = data.ValidTo.ToString("d/M/yyyy");
						data.Priority = (string)reader["Priority"];
						data.NewsStatus = (string)reader["NewsStatus"];
						data.CompanyID = (string)reader["CompanyID"];
						data.NewsType = (string)reader["NewsType"];
						data.NewsGUID = (string)reader["NewsGUID"];
					}
				}
			}
			return data;
		}

		public void UpdateNewsCompany(News news)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Update TBNews set NewsTitle = @NewsTitle, NewsDetail = @NewsDetail, ValidFrom = @ValidFrom, ValidTo = @ValidTo, 
										Priority = @Priority, NewsStatus = @NewsStatus, UpdateOn = getdate(),UpdateBy = @UserId
										Where NewsGuid = @NewsGuid";

				command.Parameters.AddWithValue("@NewsGuid", news.NewsGUID);
				command.Parameters.AddWithValue("@NewsTitle", news.NewsTitle);
				command.Parameters.AddWithValue("@NewsDetail", news.NewsDetail);
				command.Parameters.AddWithValue("@ValidFrom", news.ValidFrom);
				command.Parameters.AddWithValue("@ValidTo", news.ValidTo);
				command.Parameters.AddWithValue("@Priority", news.Priority);
				command.Parameters.AddWithValue("@NewsStatus", news.NewsStatus);
				command.Parameters.AddWithValue("@UserId", news.UpdateBy);
				command.ExecuteNonQuery();
			}
		}

		public void InsertNewsCompany(News news)
		{
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"Insert into TBNews (NewsGUID, CompanyId, NewsTitle, NewsDetail, NewsType, ValidFrom, ValidTo, Priority, NewsStatus,CreateOn, CreateBy)
										values (newid(), @CompanyId, @NewsTitle, @NewsDetail, @NewsType, @ValidFrom, @ValidTo, @Priority, 'Active',getdate(),@CreateBy)";
				command.Parameters.AddWithValue("@CompanyId", news.CompanyID);
				command.Parameters.AddWithValue("@NewsTitle", news.NewsTitle);
				command.Parameters.AddWithValue("@NewsDetail", news.NewsDetail);
				command.Parameters.AddWithValue("@NewsType", news.NewsType);
				command.Parameters.AddWithValue("@ValidFrom", news.ValidFrom);
				command.Parameters.AddWithValue("@ValidTo", news.ValidTo);
				command.Parameters.AddWithValue("@CreateBy", news.CreateBy);
				command.Parameters.AddWithValue("@Priority", news.Priority);
				command.ExecuteNonQuery();
			}
		}

	}
}