﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data.SqlClient;
using System.Text;
using OfficeMate.Framework;

namespace Eprocurement2012.Models.Repositories
{
	public class ReportRepository : RepositoryBase
	{
		public ReportRepository(SqlConnection connection) : base(connection) { }

		public int GetCountTrackOrders(JobStatus jobStatus, DateFilter? dateFilter) //Track Order ของ officemate admin
		{
			StringBuilder sqlBuilder = new StringBuilder(1024);
			sqlBuilder.AppendLine("Select Count(*) From TBOrder");
			if (jobStatus == JobStatus.Close)
			{
				sqlBuilder.AppendLine("Where OrderStatus In ('Shipped','Deleted','Expired','Completed')");
			}
			else
			{
				sqlBuilder.AppendLine("Where OrderStatus In ('Waiting','Approved','Partial','Revise','WaitingAdmin','AdminAllow')");
			}

			if (dateFilter.HasValue)
			{
				if (dateFilter == DateFilter.SevenDaysLater)
				{
					sqlBuilder.Append(" And ApproveOrderDate < @sevenDayLater");
				}
				else
				{
					sqlBuilder.Append(" And ApproveOrderDate Between @dateFrom AND @dateTo");
				}
			}
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = sqlBuilder.ToString();
				if (dateFilter.HasValue)
				{
					switch (dateFilter)
					{
						case DateFilter.Today:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today);
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case DateFilter.OneToThreeDays:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case DateFilter.FourToSevenDays:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(-3));
							break;
						case DateFilter.SevenDaysLater:
							command.Parameters.AddWithValue("@sevenDayLater", DateTime.Today.AddDays(-7));
							break;
					}
				}
				return (int)command.ExecuteScalar();
			}
		}

		public IEnumerable<Order> GetTrackOrders(int pageSize, int currentIndex, JobStatus jobStatus, DateFilter? dateFilter) //Track Order ของ officemate admin
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			sqlBuilder.AppendLine("Select Top (@pageSize) *");
			sqlBuilder.AppendLine("From (");
			sqlBuilder.AppendLine("Select	Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
			sqlBuilder.AppendLine("		OrderId, OrderGUID, OrderStatus, OrderDate, ApproveOrderDate,t1.CustID, t1.UserID, t1.UserTName, t1.UserEName, t1.ContactorPhone,");
			sqlBuilder.AppendLine("		t1.PreviousAppID, isnull(t2.UserTName,'') AS PreviousAppTName, isnull(t2.UserEName,'') AS PreviousAppEName,");
			sqlBuilder.AppendLine("		t1.CurrentAppID, isnull(t3.UserTName,'') AS CurrentAppTName, isnull(t3.UserEName,'') AS CurrentAppEName");
			sqlBuilder.AppendLine("From	TBOrder t1 with (nolock) LEFT JOIN TBUserInfo t2 with (nolock) ON t1.PreviousAppID=t2.UserID LEFT JOIN TBUserInfo t3 with (nolock) ON t1.CurrentAppID=t3.UserID");

			if (jobStatus == JobStatus.Close)
			{
				sqlBuilder.AppendLine("Where t1.OrderStatus In ('Shipped','Deleted','Expired','Completed')");
			}
			else
			{
				sqlBuilder.AppendLine("Where t1.OrderStatus In ('Waiting','Approved','Partial','Revise','WaitingAdmin','AdminAllow')");
			}

			if (dateFilter.HasValue)
			{
				if (dateFilter == DateFilter.SevenDaysLater)
				{
					sqlBuilder.Append(" And t1.ApproveOrderDate < @sevenDayLater");
				}
				else
				{
					sqlBuilder.Append(" And t1.ApproveOrderDate Between @dateFrom AND @dateTo");
				}
			}

			sqlBuilder.AppendLine(")Orders");
			sqlBuilder.AppendLine("Where row_number >= @currentItem");
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@pageSize", pageSize);
				command.Parameters.AddWithValue("@currentItem", currentIndex);

				if (dateFilter.HasValue)
				{
					switch (dateFilter)
					{
						case DateFilter.Today:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today);
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case DateFilter.OneToThreeDays:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case DateFilter.FourToSevenDays:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(-3));
							break;
						case DateFilter.SevenDaysLater:
							command.Parameters.AddWithValue("@sevenDayLater", DateTime.Today.AddDays(-7));
							break;
					}
				}

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Order order = new Order();
						order.OrderID = (string)reader["OrderId"];
						order.OrderGuid = (string)reader["OrderGUID"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						order.OrderDate = (DateTime)reader["OrderDate"];
						order.ApproveOrderDate = (DateTime)reader["ApproveOrderDate"];
						order.Requester = new User();
						order.Requester.CustId = (string)reader["CustID"];
						order.Requester.UserId = (string)reader["UserID"];
						order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
						order.Requester.DisplayName["en"] = (string)reader["UserEName"];
						order.Requester.TelephoneNumber = (string)reader["ContactorPhone"];
						order.PreviousApprover = new UserApprover();
						order.PreviousApprover.Approver = new User();
						order.PreviousApprover.Approver.UserId = (string)reader["PreviousAppID"];
						order.PreviousApprover.Approver.DisplayName = new LocalizedString((string)reader["PreviousAppTName"]);
						order.PreviousApprover.Approver.DisplayName["en"] = (string)reader["PreviousAppEName"];
						order.CurrentApprover = new UserApprover();
						order.CurrentApprover.Approver = new User();
						order.CurrentApprover.Approver.UserId = (string)reader["CurrentAppID"];
						order.CurrentApprover.Approver.DisplayName = new LocalizedString((string)reader["CurrentAppTName"]);
						order.CurrentApprover.Approver.DisplayName["en"] = (string)reader["CurrentAppEName"];
						orders.Add(order);
					}
					return orders;
				}
			}
		}

		public IEnumerable<Order> GetTrackOrdersWithoutPaging(JobStatus jobStatus, DateFilter? dateFilter) //Track Order No PageSize ของ officemate admin
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			sqlBuilder.AppendLine("Select *");
			sqlBuilder.AppendLine("From (");
			sqlBuilder.AppendLine("Select	Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
			sqlBuilder.AppendLine("		OrderId, OrderGUID, OrderStatus, OrderDate, ApproveOrderDate,t1.CustID, t1.UserID, t1.UserTName, t1.UserEName, t1.ContactorPhone,");
			sqlBuilder.AppendLine("		t1.PreviousAppID, isnull(t2.UserTName,'') AS PreviousAppTName, isnull(t2.UserEName,'') AS PreviousAppEName,");
			sqlBuilder.AppendLine("		t1.CurrentAppID, isnull(t3.UserTName,'') AS CurrentAppTName, isnull(t3.UserEName,'') AS CurrentAppEName");
			sqlBuilder.AppendLine("From	TBOrder t1 with (nolock) LEFT JOIN TBUserInfo t2 with (nolock) ON t1.PreviousAppID=t2.UserID LEFT JOIN TBUserInfo t3 with (nolock) ON t1.CurrentAppID=t3.UserID");

			if (jobStatus == JobStatus.Close)
			{
				sqlBuilder.AppendLine("Where t1.OrderStatus In ('Shipped','Deleted','Expired','Completed')");
			}
			else
			{
				sqlBuilder.AppendLine("Where t1.OrderStatus In ('Waiting','Approved','Partial','Revise','WaitingAdmin','AdminAllow')");
			}

			if (dateFilter.HasValue)
			{
				if (dateFilter == DateFilter.SevenDaysLater)
				{
					sqlBuilder.Append(" And t1.ApproveOrderDate < @sevenDayLater");
				}
				else
				{
					sqlBuilder.Append(" And t1.ApproveOrderDate Between @dateFrom AND @dateTo");
				}
			}

			sqlBuilder.AppendLine(")Orders");
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = sqlBuilder.ToString();

				if (dateFilter.HasValue)
				{
					switch (dateFilter)
					{
						case DateFilter.Today:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today);
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case DateFilter.OneToThreeDays:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case DateFilter.FourToSevenDays:
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(-3));
							break;
						case DateFilter.SevenDaysLater:
							command.Parameters.AddWithValue("@sevenDayLater", DateTime.Today.AddDays(-7));
							break;
					}
				}

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Order order = new Order();
						order.OrderID = (string)reader["OrderId"];
						order.OrderGuid = (string)reader["OrderGUID"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						order.OrderDate = (DateTime)reader["OrderDate"];
						order.ApproveOrderDate = (DateTime)reader["ApproveOrderDate"];
						order.Requester = new User();
						order.Requester.CustId = (string)reader["CustID"];
						order.Requester.UserId = (string)reader["UserID"];
						order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
						order.Requester.DisplayName["en"] = (string)reader["UserEName"];
						order.Requester.TelephoneNumber = (string)reader["ContactorPhone"];
						order.PreviousApprover = new UserApprover();
						order.PreviousApprover.Approver = new User();
						order.PreviousApprover.Approver.UserId = (string)reader["PreviousAppID"];
						order.PreviousApprover.Approver.DisplayName = new LocalizedString((string)reader["PreviousAppTName"]);
						order.PreviousApprover.Approver.DisplayName["en"] = (string)reader["PreviousAppEName"];
						order.CurrentApprover = new UserApprover();
						order.CurrentApprover.Approver = new User();
						order.CurrentApprover.Approver.UserId = (string)reader["CurrentAppID"];
						order.CurrentApprover.Approver.DisplayName = new LocalizedString((string)reader["CurrentAppTName"]);
						order.CurrentApprover.Approver.DisplayName["en"] = (string)reader["CurrentAppEName"];
						orders.Add(order);
					}
					return orders;
				}
			}
		}

		public IEnumerable<Order> GetAdvanceSearchOrders(JobStatus jobStatus, AdvanceSearchField advanceSearchField) //Advance Search ของ officemate admin
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("Select *");
				sqlBuilder.AppendLine("From (");
				sqlBuilder.AppendLine("Select	Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		OrderId, OrderGUID, OrderStatus, OrderDate, ApproveOrderDate,t1.CustID, t1.UserID, t1.UserTName, t1.UserEName, t1.ContactorPhone,");
				sqlBuilder.AppendLine("		t1.PreviousAppID, isnull(t2.UserTName,'') AS PreviousAppTName, isnull(t2.UserEName,'') AS PreviousAppEName,");
				sqlBuilder.AppendLine("		t1.CurrentAppID, isnull(t3.UserTName,'') AS CurrentAppTName, isnull(t3.UserEName,'') AS CurrentAppEName");
				sqlBuilder.AppendLine("From	TBOrder t1 with (nolock) LEFT JOIN TBUserInfo t2 with (nolock) ON t1.PreviousAppID=t2.UserID LEFT JOIN TBUserInfo t3 with (nolock) ON t1.CurrentAppID=t3.UserID");
				if (jobStatus == JobStatus.Close)
				{
					sqlBuilder.AppendLine("Where t1.OrderStatus In ('Shipped','Deleted','Expired','Completed')");
				}
				else
				{
					sqlBuilder.AppendLine("Where t1.OrderStatus In ('Waiting','Approved','Partial','Revise','WaitingAdmin','AdminAllow')");
				}
				if (advanceSearchField.CheckCompanyId && !string.IsNullOrEmpty(advanceSearchField.CompanyId))
				{
					sqlBuilder.AppendLine(" AND CompanyId LIKE @CompanyId");
					command.Parameters.AddWithValue("@CompanyId", "%" + advanceSearchField.CompanyId + "%");
				}
				if (advanceSearchField.CheckCustId && !string.IsNullOrEmpty(advanceSearchField.CustId))
				{
					sqlBuilder.AppendLine(" AND CustId LIKE @CustId");
					command.Parameters.AddWithValue("@CustId", "%" + advanceSearchField.CustId + "%");
				}
				if (advanceSearchField.CheckUserId && !string.IsNullOrEmpty(advanceSearchField.UserId))
				{
					sqlBuilder.AppendLine(" AND t1.UserId LIKE @UserId");
					command.Parameters.AddWithValue("@UserId", "%" + advanceSearchField.UserId + "%");
				}
				if (advanceSearchField.CheckOrderId && !string.IsNullOrEmpty(advanceSearchField.OrderId))
				{
					sqlBuilder.AppendLine(" AND OrderId = @OrderId");
					command.Parameters.AddWithValue("@OrderId", advanceSearchField.OrderId);
				}
				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND OrderDate BETWEEN @DateFrom AND @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}
				if (advanceSearchField.CheckAppDate && !string.IsNullOrEmpty(advanceSearchField.AppDateFrom) && !string.IsNullOrEmpty(advanceSearchField.AppDateTo))
				{
					DateTime appFrom = ConvertStringToDateTime(advanceSearchField.AppDateFrom);
					DateTime appTo = ConvertStringToDateTime(advanceSearchField.AppDateTo);
					if (appFrom != DateTime.MinValue && appTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND ApproveOrderDate BETWEEN @AppDateFrom AND @AppDateTo");
						command.Parameters.AddWithValue("@AppDateFrom", appFrom.Date);
						command.Parameters.AddWithValue("@AppDateTo", appTo.Date.AddDays(1));
					}
				}
				sqlBuilder.AppendLine(SetSearchOrderStatus(advanceSearchField));
				sqlBuilder.AppendLine(" )Orders");
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						Order order = new Order();
						order.OrderID = (string)reader["OrderId"];
						order.OrderGuid = (string)reader["OrderGUID"];
						order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						order.OrderDate = (DateTime)reader["OrderDate"];
						order.ApproveOrderDate = (DateTime)reader["ApproveOrderDate"];
						order.Requester = new User();
						order.Requester.CustId = (string)reader["CustID"];
						order.Requester.UserId = (string)reader["UserID"];
						order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
						order.Requester.DisplayName["en"] = (string)reader["UserEName"];
						order.Requester.TelephoneNumber = (string)reader["ContactorPhone"];
						order.PreviousApprover = new UserApprover();
						order.PreviousApprover.Approver = new User();
						order.PreviousApprover.Approver.UserId = (string)reader["PreviousAppID"];
						order.PreviousApprover.Approver.DisplayName = new LocalizedString((string)reader["PreviousAppTName"]);
						order.PreviousApprover.Approver.DisplayName["en"] = (string)reader["PreviousAppEName"];
						order.CurrentApprover = new UserApprover();
						order.CurrentApprover.Approver = new User();
						order.CurrentApprover.Approver.UserId = (string)reader["CurrentAppID"];
						order.CurrentApprover.Approver.DisplayName = new LocalizedString((string)reader["CurrentAppTName"]);
						order.CurrentApprover.Approver.DisplayName["en"] = (string)reader["CurrentAppEName"];
						orders.Add(order);
					}
					return orders;
				}
			}
		}

		public IEnumerable<ProductSummary> GetProductSummary(AdvanceSearchField advanceSearchField, string companyId, string userId, bool isAdmin)
		{
			List<ProductSummary> productSummarys = new List<ProductSummary>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("select	row_number()over(order by parentSortSeq,deptSortSeq) as 'row_number',*");
				sqlBuilder.AppendLine("from");
				sqlBuilder.AppendLine("(");
				sqlBuilder.AppendLine("	select	parent.DeptId as parentId,parentmas.DeptThaiName as parentTName,parentmas.DeptEngName as parentEName,parent.SortSeq as parentSortSeq,");
				sqlBuilder.AppendLine("			dept.DeptId as deptId,deptmas.DeptThaiName as deptTName,deptmas.DeptEngName as deptEName,dept.RefID,dept.SortSeq as deptSortSeq");
				sqlBuilder.AppendLine("	from	TBDeptStructure parent with (nolock)");
				sqlBuilder.AppendLine("			inner join TBDeptMaster parentmas with (nolock) on parent.DeptID = parentmas.DeptID");
				sqlBuilder.AppendLine("			inner join TBDeptStructure dept with (nolock) on dept.ParentDeptId=parent.DeptId");
				sqlBuilder.AppendLine("			inner join TBDeptMaster deptmas with (nolock) on dept.DeptID = deptmas.DeptID");
				sqlBuilder.AppendLine("	where	parent.ParentDeptId=0 and parent.Isdefault='Yes'");

				if (advanceSearchField.CheckDeptProduct && advanceSearchField.DeptProduct.Any())
				{
					sqlBuilder.Append("and ( 1=2 ");
					int index = 0;
					foreach (var deptProt in advanceSearchField.DeptProduct)
					{
						sqlBuilder.AppendFormat("or parent.DeptId = @deptProt_{0} ", index);
						command.Parameters.AddWithValue("@deptProt_" + index, deptProt);
						++index;
					}
					sqlBuilder.AppendLine(")");
				}

				sqlBuilder.AppendLine(")product");
				sqlBuilder.AppendLine("inner join");
				sqlBuilder.AppendLine("(");
				sqlBuilder.AppendLine("	select	deptfil.RefID,head.OrderID,head.OrderDate,head.CompanyID,head.DepartmentID,head.CostcenterID,");
				sqlBuilder.AppendLine("			head.UserID,head.UserTName,head.UserEName,head.OrderStatus,head.OrderGUID,");
				sqlBuilder.AppendLine("			detail.Qty,detail.IncVatPrice,detail.ExcVatPrice,detail.DiscountRate,detail.OldQty,");
				sqlBuilder.AppendLine("			prodcent.pID,prodcent.pTName,prodcent.pEName,prodcent.pTUnit,prodcent.pEUnit,");
				sqlBuilder.AppendLine("			prodcent.IsPromotion,prodcent.IsBestDeal,prodcent.IsVat,isnull(prodcat.ProductID,'') as CatPID,prodcat.PriceType,");
				sqlBuilder.AppendLine("			isnull(cost.CostTName,'') as CostTName, isnull(cost.CostEName,'') as CostEName,");
				sqlBuilder.AppendLine("			isnull(dept.DeptTName,'') as DeptTName, isnull(dept.DeptEName,'') as DeptEName");
				sqlBuilder.AppendLine("	from	TBOrder head with (nolock)");
				sqlBuilder.AppendLine("			inner join TBOrderDetail detail with (nolock) on head.OrderID=detail.OrderID");
				sqlBuilder.AppendLine("			inner join TBCostcenter cost with (nolock) on head.CompanyID=cost.CompanyID AND head.CostcenterID=cost.CostcenterID");
				sqlBuilder.AppendLine("			left join TBDepartment dept with (nolock) on head.CompanyID=dept.CompanyID AND head.DepartmentID=dept.DepartmentID");
				sqlBuilder.AppendLine("			left join TBProductCatalog prodcat with (nolock) on detail.pID=prodcat.ProductID and head.CompanyID=prodcat.CompanyID");
				sqlBuilder.AppendLine("			inner join TBProductCenter prodcent with (nolock) on detail.pID=prodcent.pID");
				sqlBuilder.AppendLine("			inner join TBDeptProductFilter deptfil with (nolock) on prodcent.CodeId=deptfil.CodeId");
				sqlBuilder.AppendLine("	where	head.CompanyID=@CompanyID");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("and (head.UserID = @UserID or head.PreviousAppID = @UserID or head.CurrentAppID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (advanceSearchField.CheckOrderStatus)
				{
					sqlBuilder.AppendLine(SetSearchOrderStatus(advanceSearchField));
				}

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine("and OrderDate between @DateFrom and @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckOrderId && !string.IsNullOrEmpty(advanceSearchField.OrderIdFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderIdTo))
				{
					sqlBuilder.AppendLine("and head.OrderID between @OrderIdFrom and @OrderIdTo");
					command.Parameters.AddWithValue("@OrderIdFrom", advanceSearchField.OrderIdFrom);
					command.Parameters.AddWithValue("@OrderIdTo", advanceSearchField.OrderIdTo);
				}

				if (advanceSearchField.CheckProductId && !string.IsNullOrEmpty(advanceSearchField.ProductIdFrom) && !string.IsNullOrEmpty(advanceSearchField.ProductIdTo))
				{
					sqlBuilder.AppendLine("and prodcent.pID between @ProductIdFrom and @ProductIdTo");
					command.Parameters.AddWithValue("@ProductIdFrom", advanceSearchField.ProductIdFrom);
					command.Parameters.AddWithValue("@ProductIdTo", advanceSearchField.ProductIdTo);
				}

				if (advanceSearchField.CheckDepartment && advanceSearchField.Department.Any())
				{
					sqlBuilder.Append("and ( 1=2 ");
					int index = 0;
					foreach (var dept in advanceSearchField.Department)
					{
						sqlBuilder.AppendFormat("or head.DepartmentID = @Dept_{0}", index);
						command.Parameters.AddWithValue("@Dept_" + index, dept);
						++index;
					}
					sqlBuilder.AppendLine(")");
				}

				if (advanceSearchField.CheckCostcenter && advanceSearchField.Costcenter.Any())
				{
					sqlBuilder.Append("and ( 1=2 ");
					int index = 0;
					foreach (var cost in advanceSearchField.Costcenter)
					{
						sqlBuilder.AppendFormat("or head.CostcenterID = @Cost_{0} ", index);
						command.Parameters.AddWithValue("@Cost_" + index, cost);
						++index;
					}
					sqlBuilder.AppendLine(")");
				}

				sqlBuilder.AppendLine(")orders");
				sqlBuilder.AppendLine("on orders.RefID = product.RefID");

				command.CommandText = command.CommandText + sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					ProductSummary productSummary = null;
					while (reader.Read())
					{
						productSummary = new ProductSummary();
						productSummary.Order = new Order();
						productSummary.Order.OrderGuid = (string)reader["OrderGUID"];
						productSummary.Order.OrderID = (string)reader["OrderID"];
						productSummary.Order.OrderDate = (DateTime)reader["OrderDate"];
						productSummary.Order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
						productSummary.Order.Requester = new User();
						productSummary.Order.Requester.UserId = (string)reader["UserId"];
						productSummary.Order.Requester.UserThaiName = (string)reader["UserTName"];
						productSummary.Order.Requester.UserEngName = (string)reader["UserEName"];
						productSummary.Order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
						productSummary.Order.Requester.DisplayName["en"] = (string)reader["UserEName"];

						productSummary.OrderDetail = new ProductOrderDetail();
						productSummary.OrderDetail.Quantity = (int)reader["Qty"];
						productSummary.OrderDetail.OldQuantity = (int)reader["OldQty"];
						productSummary.OrderDetail.DiscountRate = (decimal)reader["DiscountRate"];
						productSummary.OrderDetail.ExcVatPrice = (decimal)reader["ExcVatPrice"];
						productSummary.OrderDetail.IncVatPrice = (decimal)reader["IncVatPrice"];
						productSummary.OrderDetail.IsOfmCatalog = reader["CatPID"].ToString().Equals("");

						productSummary.DepartmentProduct = new DepartmentProduct();
						productSummary.DepartmentProduct.ParentId = (int)reader["parentId"];
						productSummary.DepartmentProduct.ParentThaiName = (string)reader["parentTName"];
						productSummary.DepartmentProduct.ParentEngName = (string)reader["parentEName"];
						productSummary.DepartmentProduct.ParentName = new LocalizedString((string)reader["parentTName"]);
						productSummary.DepartmentProduct.ParentName["en"] = (string)reader["parentEName"];
						productSummary.DepartmentProduct.DeptId = (int)reader["deptId"];
						productSummary.DepartmentProduct.DeptThaiName = (string)reader["deptTName"];
						productSummary.DepartmentProduct.DeptEngName = (string)reader["deptEName"];
						productSummary.DepartmentProduct.DeptName = new LocalizedString((string)reader["deptTName"]);
						productSummary.DepartmentProduct.DeptName["en"] = (string)reader["deptEName"];

						productSummary.OrderDetail.Product = new Product();
						productSummary.OrderDetail.Product.Id = (string)reader["pID"];
						productSummary.OrderDetail.Product.ThaiName = (string)reader["pTName"];
						productSummary.OrderDetail.Product.EngName = (string)reader["pEName"];
						productSummary.OrderDetail.Product.Name = new LocalizedString((string)reader["pTName"]);
						productSummary.OrderDetail.Product.Name["en"] = (string)reader["pEName"];
						productSummary.OrderDetail.Product.ThaiUnit = (string)reader["pTUnit"];
						productSummary.OrderDetail.Product.EngUnit = (string)reader["pEUnit"];
						productSummary.OrderDetail.Product.Unit = new LocalizedString((string)reader["pTUnit"]);
						productSummary.OrderDetail.Product.Unit["en"] = (string)reader["pEUnit"];
						productSummary.OrderDetail.Product.IsVat = Mapper.ToClass<bool>((string)reader["IsVat"]);
						productSummary.OrderDetail.Product.IsBestDeal = Mapper.ToClass<bool>((string)reader["IsBestDeal"]);
						productSummary.OrderDetail.Product.IsPromotion = Mapper.ToClass<bool>((string)reader["IsPromotion"]);

						if (String.IsNullOrEmpty(reader["PriceType"].ToString())) { productSummary.OrderDetail.Product.PriceType = PriceType.Float; }
						else { productSummary.OrderDetail.Product.PriceType = (PriceType)Enum.Parse(typeof(PriceType), (string)reader["PriceType"], true); }

						productSummary.Order.Company = new Company();
						productSummary.Order.Company.CompanyId = (string)reader["CompanyId"];

						productSummary.Order.Department = new CompanyDepartment();
						productSummary.Order.Department.DepartmentID = (string)reader["DepartmentID"];
						productSummary.Order.Department.DepartmentThaiName = (string)reader["DeptTName"];
						productSummary.Order.Department.DepartmentEngName = (string)reader["DeptEName"];
						productSummary.Order.Department.DepartmentName = new LocalizedString((string)reader["DeptTName"]);
						productSummary.Order.Department.DepartmentName["en"] = (string)reader["DeptEName"];

						productSummary.Order.CostCenter = new CostCenter();
						productSummary.Order.CostCenter.CostCenterCustID = (string)reader["CostcenterID"];
						productSummary.Order.CostCenter.CostCenterThaiName = (string)reader["CostTName"];
						productSummary.Order.CostCenter.CostCenterEngName = (string)reader["CostEName"];
						productSummary.Order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
						productSummary.Order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];

						productSummarys.Add(productSummary);
					}
				}
			}
			return productSummarys;
		}

		public int GetCountConditionalOrder(AdvanceSearchField advanceSearchField, string companyId, string userId, bool isAdmin)
		{
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT	count(*) FROM TBOrder t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		INNER JOIN TBUsers tr ON t1.UserID=tr.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t1.PreviousAppID = @UserID OR t1.CurrentAppID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (advanceSearchField.CheckOrderStatus)
				{
					sqlBuilder.AppendLine(SetSearchOrderStatus(advanceSearchField));
				}

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND OrderDate BETWEEN @DateFrom AND @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckAppDate && !string.IsNullOrEmpty(advanceSearchField.AppDateFrom) && !string.IsNullOrEmpty(advanceSearchField.AppDateTo))
				{
					DateTime appFrom = ConvertStringToDateTime(advanceSearchField.AppDateFrom);
					DateTime appTo = ConvertStringToDateTime(advanceSearchField.AppDateTo);
					if (appFrom != DateTime.MinValue && appTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND ApproveOrderDate BETWEEN @AppDateFrom AND @AppDateTo");
						command.Parameters.AddWithValue("@AppDateFrom", appFrom.Date);
						command.Parameters.AddWithValue("@AppDateTo", appTo.Date.AddDays(1));
					}
				}

				if (isAdmin)
				{
					if (advanceSearchField.CheckRequester && advanceSearchField.Requester.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var req in advanceSearchField.Requester)
						{
							sqlBuilder.AppendFormat("OR tr.UserGuid = @Req_{0} ", index);
							command.Parameters.AddWithValue("@Req_" + index, req);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckApprover && advanceSearchField.Approver.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var app in advanceSearchField.Approver)
						{
							sqlBuilder.AppendFormat("OR t4.UserGuid = @App_{0} ", index);
							command.Parameters.AddWithValue("@App_" + index, app);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckCostcenter && advanceSearchField.Costcenter.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var cost in advanceSearchField.Costcenter)
						{
							sqlBuilder.AppendFormat("OR t1.CostcenterID = @Cost_{0} ", index);
							command.Parameters.AddWithValue("@Cost_" + index, cost);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckOrderPrice && !string.IsNullOrEmpty(advanceSearchField.MinOrderPrice) && !string.IsNullOrEmpty(advanceSearchField.MaxOrderPrice))
					{
						sqlBuilder.AppendLine("AND t1.GrandTotalAmt BETWEEN @MinOrderPrice AND @MaxOrderPrice");
						command.Parameters.AddWithValue("@MinOrderPrice", advanceSearchField.MinOrderPrice);
						command.Parameters.AddWithValue("@MaxOrderPrice", advanceSearchField.MaxOrderPrice);
					}
				}

				command.CommandText = sqlBuilder.ToString();
				return (int)command.ExecuteScalar();
			}
		}

		public IEnumerable<Order> GetConditionalOrder(int pageSize, int currentIndex, AdvanceSearchField advanceSearchField, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT Top (@pageSize) * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("		isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM	TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		INNER JOIN TBUsers tr with (nolock) ON t1.UserID=tr.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID");

				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t1.PreviousAppID = @UserID OR t1.CurrentAppID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (advanceSearchField.CheckOrderStatus)
				{
					sqlBuilder.AppendLine(SetSearchOrderStatus(advanceSearchField));
				}

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND OrderDate BETWEEN @DateFrom AND @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckAppDate && !string.IsNullOrEmpty(advanceSearchField.AppDateFrom) && !string.IsNullOrEmpty(advanceSearchField.AppDateTo))
				{
					DateTime appFrom = ConvertStringToDateTime(advanceSearchField.AppDateFrom);
					DateTime appTo = ConvertStringToDateTime(advanceSearchField.AppDateTo);
					if (appFrom != DateTime.MinValue && appTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND ApproveOrderDate BETWEEN @AppDateFrom AND @AppDateTo");
						command.Parameters.AddWithValue("@AppDateFrom", appFrom.Date);
						command.Parameters.AddWithValue("@AppDateTo", appTo.Date.AddDays(1));
					}
				}

				if (isAdmin)
				{
					if (advanceSearchField.CheckRequester && advanceSearchField.Requester.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var req in advanceSearchField.Requester)
						{
							sqlBuilder.AppendFormat("OR tr.UserGuid = @Req_{0} ", index);
							command.Parameters.AddWithValue("@Req_" + index, req);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckApprover && advanceSearchField.Approver.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var app in advanceSearchField.Approver)
						{
							sqlBuilder.AppendFormat("OR t4.UserGuid = @App_{0} ", index);
							command.Parameters.AddWithValue("@App_" + index, app);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckCostcenter && advanceSearchField.Costcenter.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var cost in advanceSearchField.Costcenter)
						{
							sqlBuilder.AppendFormat("OR t1.CostcenterID = @Cost_{0} ", index);
							command.Parameters.AddWithValue("@Cost_" + index, cost);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckOrderPrice && !string.IsNullOrEmpty(advanceSearchField.MinOrderPrice) && !string.IsNullOrEmpty(advanceSearchField.MaxOrderPrice))
					{
						sqlBuilder.AppendLine("AND t1.GrandTotalAmt BETWEEN @MinOrderPrice AND @MaxOrderPrice");
						command.Parameters.AddWithValue("@MinOrderPrice", advanceSearchField.MinOrderPrice);
						command.Parameters.AddWithValue("@MaxOrderPrice", advanceSearchField.MaxOrderPrice);
					}
				}

				sqlBuilder.AppendLine(") Orders");
				sqlBuilder.AppendLine("WHERE row_number >= @currentItem");
				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@pageSize", pageSize);
				command.Parameters.AddWithValue("@currentItem", currentIndex);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		public IEnumerable<Order> GetConditionalOrderWithoutPaging(AdvanceSearchField advanceSearchField, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("		isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM	TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		INNER JOIN TBUsers tr with (nolock) ON t1.UserID=tr.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t1.PreviousAppID = @UserID OR t1.CurrentAppID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (advanceSearchField.CheckOrderStatus)
				{
					sqlBuilder.AppendLine(SetSearchOrderStatus(advanceSearchField));
				}

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND OrderDate BETWEEN @DateFrom AND @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckAppDate && !string.IsNullOrEmpty(advanceSearchField.AppDateFrom) && !string.IsNullOrEmpty(advanceSearchField.AppDateTo))
				{
					DateTime appFrom = ConvertStringToDateTime(advanceSearchField.AppDateFrom);
					DateTime appTo = ConvertStringToDateTime(advanceSearchField.AppDateTo);
					if (appFrom != DateTime.MinValue && appTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND ApproveOrderDate BETWEEN @AppDateFrom AND @AppDateTo");
						command.Parameters.AddWithValue("@AppDateFrom", appFrom.Date);
						command.Parameters.AddWithValue("@AppDateTo", appTo.Date.AddDays(1));
					}
				}

				if (isAdmin)
				{
					if (advanceSearchField.CheckRequester && advanceSearchField.Requester.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var req in advanceSearchField.Requester)
						{
							sqlBuilder.AppendFormat("OR tr.UserGuid = @Req_{0} ", index);
							command.Parameters.AddWithValue("@Req_" + index, req);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckApprover && advanceSearchField.Approver.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var app in advanceSearchField.Approver)
						{
							sqlBuilder.AppendFormat("OR t4.UserGuid = @App_{0} ", index);
							command.Parameters.AddWithValue("@App_" + index, app);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckCostcenter && advanceSearchField.Costcenter.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var cost in advanceSearchField.Costcenter)
						{
							sqlBuilder.AppendFormat("OR t1.CostcenterID = @Cost_{0} ", index);
							command.Parameters.AddWithValue("@Cost_" + index, cost);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckOrderPrice && !string.IsNullOrEmpty(advanceSearchField.MinOrderPrice) && !string.IsNullOrEmpty(advanceSearchField.MaxOrderPrice))
					{
						sqlBuilder.AppendLine("AND t1.GrandTotalAmt BETWEEN @MinOrderPrice AND @MaxOrderPrice");
						command.Parameters.AddWithValue("@MinOrderPrice", advanceSearchField.MinOrderPrice);
						command.Parameters.AddWithValue("@MaxOrderPrice", advanceSearchField.MaxOrderPrice);
					}
				}

				sqlBuilder.AppendLine(") Orders");
				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		public int GetCountTrackOrderByStatus(DateFilter? dateFilter, Order.OrderStatus orderStatus, string companyId, string userId, bool isAdmin)
		{
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT	count(*) FROM TBOrder t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID AND t1.OrderStatus=@orderStatus ");
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@orderStatus", orderStatus.ToString());

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t4.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (dateFilter.HasValue)
				{
					switch (dateFilter)
					{
						case DateFilter.Today:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today);
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case DateFilter.OneToThreeDays:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case DateFilter.FourToSevenDays:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(-3));
							break;
						case DateFilter.SevenDaysLater:
							sqlBuilder.Append(" AND OrderDate < @sevenDayLater");
							command.Parameters.AddWithValue("@sevenDayLater", DateTime.Today.AddDays(-7));
							break;
					}
				}

				command.CommandText = sqlBuilder.ToString();
				return (int)command.ExecuteScalar();
			}
		}

		public IEnumerable<Order> GetTrackOrderByStatus(int pageSize, int currentIndex, DateFilter? dateFilter, Order.OrderStatus orderStatus, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT Top (@pageSize) * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("		isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID AND t1.OrderStatus=@orderStatus ");
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@orderStatus", orderStatus.ToString());

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t4.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (dateFilter.HasValue)
				{
					switch (dateFilter)
					{
						case DateFilter.Today:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today);
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case DateFilter.OneToThreeDays:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case DateFilter.FourToSevenDays:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(-3));
							break;
						case DateFilter.SevenDaysLater:
							sqlBuilder.Append(" AND OrderDate < @sevenDayLater");
							command.Parameters.AddWithValue("@sevenDayLater", DateTime.Today.AddDays(-7));
							break;
					}
				}

				sqlBuilder.AppendLine(") Orders");
				sqlBuilder.AppendLine("WHERE row_number >= @currentItem");
				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@pageSize", pageSize);
				command.Parameters.AddWithValue("@currentItem", currentIndex);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		public IEnumerable<Order> GetTrackOrderByStatusWithoutPaging(DateFilter? dateFilter, Order.OrderStatus orderStatus, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("		isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID AND t1.OrderStatus=@orderStatus ");
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@orderStatus", orderStatus.ToString());

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t4.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (dateFilter.HasValue)
				{
					switch (dateFilter)
					{
						case DateFilter.Today:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today);
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case DateFilter.OneToThreeDays:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case DateFilter.FourToSevenDays:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(-3));
							break;
						case DateFilter.SevenDaysLater:
							sqlBuilder.Append(" AND OrderDate < @sevenDayLater");
							command.Parameters.AddWithValue("@sevenDayLater", DateTime.Today.AddDays(-7));
							break;
					}
				}

				sqlBuilder.AppendLine(") Orders");
				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		public IEnumerable<Order> GetSearchOrderByStatus(AdvanceSearchField advanceSearchField, Order.OrderStatus orderStatus, string companyId)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("		isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID AND t1.OrderStatus=@orderStatus ");
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@orderStatus", orderStatus.ToString());

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					if (dateFrom != DateTime.MinValue)
					{
						sqlBuilder.Append(" AND OrderDate >= @DateFrom AND OrderDate < @dateTo");
						command.Parameters.AddWithValue("@dateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@dateTo", dateFrom.Date.AddDays(1));
					}
				}
				if (advanceSearchField.CheckOrderId && !string.IsNullOrEmpty(advanceSearchField.OrderId))
				{
					sqlBuilder.Append(" AND OrderId LIKE @orderId");
					command.Parameters.AddWithValue("@orderId", "%" + advanceSearchField.OrderId + "%");
				}
				if (advanceSearchField.CheckUserId && !string.IsNullOrEmpty(advanceSearchField.UserId))
				{
					sqlBuilder.Append(" AND t1.UserId LIKE @userId");
					command.Parameters.AddWithValue("@userId", "%" + advanceSearchField.UserId + "%");
				}
				sqlBuilder.AppendLine(") Orders");
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		public int GetCountOrderHistory(MonthFilter? monthFilter, string companyId, string userId, bool isAdmin)
		{
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT	count(*) FROM TBOrder t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID ");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t4.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (monthFilter.HasValue)
				{
					switch (monthFilter)
					{
						case MonthFilter.SevenDaysLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case MonthFilter.OneMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-1));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case MonthFilter.ThreeMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case MonthFilter.SixMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						default:
							break;
					}
				}
				command.CommandText = sqlBuilder.ToString();
				return (int)command.ExecuteScalar();
			}
		}

		public IEnumerable<Order> GetOrderHistory(int pageSize, int currentIndex, MonthFilter? monthFilter, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT Top (@pageSize) * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("		isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID ");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t4.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (monthFilter.HasValue)
				{
					switch (monthFilter)
					{
						case MonthFilter.SevenDaysLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case MonthFilter.OneMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-1));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case MonthFilter.ThreeMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case MonthFilter.SixMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						default:
							break;
					}
				}
				sqlBuilder.AppendLine(") Orders");
				sqlBuilder.AppendLine("WHERE row_number >= @currentItem");
				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@pageSize", pageSize);
				command.Parameters.AddWithValue("@currentItem", currentIndex);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		public IEnumerable<Order> GetOrderHistoryWithoutPaging(MonthFilter? monthFilter, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("		isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID ");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t4.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (monthFilter.HasValue)
				{
					switch (monthFilter)
					{
						case MonthFilter.SevenDaysLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
							break;
						case MonthFilter.OneMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-1));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case MonthFilter.ThreeMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-3));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						case MonthFilter.SixMonthLater:
							sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
							command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddMonths(-7));
							command.Parameters.AddWithValue("@dateTo", DateTime.Today);
							break;
						default:
							break;
					}
				}
				sqlBuilder.AppendLine(") Orders");
				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		public IEnumerable<Order> GetSearchOrderHistory(AdvanceSearchField advanceSearchField, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("		isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID ");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t4.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					if (dateFrom != DateTime.MinValue)
					{
						sqlBuilder.Append(" AND OrderDate >= @DateFrom AND OrderDate < @dateTo");
						command.Parameters.AddWithValue("@dateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@dateTo", dateFrom.Date.AddDays(1));
					}
				}
				if (advanceSearchField.CheckOrderId && !string.IsNullOrEmpty(advanceSearchField.OrderId))
				{
					sqlBuilder.Append(" AND OrderId LIKE @orderId");
					command.Parameters.AddWithValue("@orderId", "%" + advanceSearchField.OrderId + "%");
				}
				if (advanceSearchField.CheckUserId && !string.IsNullOrEmpty(advanceSearchField.UserId))
				{
					sqlBuilder.Append(" AND t1.UserId LIKE @userId");
					command.Parameters.AddWithValue("@userId", "%" + advanceSearchField.UserId + "%");
				}
				sqlBuilder.AppendLine(") Orders");
				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		public IEnumerable<CostcenterSummaryOrder> GetSummaryOrderCostCenter(TrackCostCenter trackCostCenter, string companyId)
		{
			List<CostcenterSummaryOrder> sumData = new List<CostcenterSummaryOrder>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder();
				switch (trackCostCenter.RankFilter)
				{
					case RankFilter.TopTen:
						sqlBuilder.Append("SELECT TOP 10 * ");
						break;
					case RankFilter.TopTwenty:
						sqlBuilder.Append("SELECT TOP 20 * ");
						break;
					case RankFilter.TopFifty:
						sqlBuilder.Append("SELECT TOP 50 * ");
						break;
					case RankFilter.All:
						sqlBuilder.Append("SELECT * ");
						break;
				}
				sqlBuilder.AppendLine("FROM (");
				sqlBuilder.AppendLine("SELECT max(t2.CostcenterID) AS CostcenterID, max(t2.CostTName) AS CostTName, max(t2.CostEName) AS CostEName,");
				sqlBuilder.AppendLine(" sum(t1.GrandTotalAmt) AS SumGrandTotalAmt, count(t1.OrderID) AS CountOrder");
				sqlBuilder.AppendLine("FROM	TBOrder t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID=t2.CompanyID AND t1.CostcenterID=t2.CostcenterID");

				var where = WhereClause.Create("t1.CompanyID=@companyId");
				command.Parameters.AddWithValue("@companyId", companyId);

				where = where.And(WhereClause.Create("OrderStatus IN ('Approved','Shipped','Completed')"));

				if (trackCostCenter.Months != null)
				{
					var monthWhere = WhereClause.Create();
					foreach (var month in trackCostCenter.Months)
					{
						monthWhere = monthWhere.Or(string.Format(" OrderDate between @from_{0} and @to_{0}", month));
						command.Parameters.AddWithValue("@from_" + month, new DateTime(DateTime.Today.Year, month, 1));
						command.Parameters.AddWithValue("@to_" + month, new DateTime(DateTime.Today.Year, month, 1).AddMonths(1).AddDays(-1));
					}
					where = where.And(monthWhere);
				}

				var quarterWhere = WhereClause.Create();
				if (trackCostCenter.Quarters1)
				{
					quarterWhere = quarterWhere.Or(" OrderDate between @fromQ1 and @toQ1");
					command.Parameters.AddWithValue("@fromQ1", new DateTime(DateTime.Today.Year, 1, 1));
					command.Parameters.AddWithValue("@toQ1", new DateTime(DateTime.Today.Year, 3, 1).AddMonths(1).AddDays(-1));
				}
				if (trackCostCenter.Quarters2)
				{
					quarterWhere = quarterWhere.Or(" OrderDate between @fromQ2 and @toQ2");
					command.Parameters.AddWithValue("@fromQ2", new DateTime(DateTime.Today.Year, 4, 1));
					command.Parameters.AddWithValue("@toQ2", new DateTime(DateTime.Today.Year, 6, 1).AddMonths(1).AddDays(-1));
				}
				if (trackCostCenter.Quarters3)
				{
					quarterWhere = quarterWhere.Or(" OrderDate between @fromQ3 and @toQ3");
					command.Parameters.AddWithValue("@fromQ3", new DateTime(DateTime.Today.Year, 7, 1));
					command.Parameters.AddWithValue("@toQ3", new DateTime(DateTime.Today.Year, 9, 1).AddMonths(1).AddDays(-1));
				}
				if (trackCostCenter.Quarters4)
				{
					quarterWhere = quarterWhere.Or(" OrderDate between @fromQ4 and @toQ4");
					command.Parameters.AddWithValue("@fromQ4", new DateTime(DateTime.Today.Year, 10, 1));
					command.Parameters.AddWithValue("@toQ4", new DateTime(DateTime.Today.Year, 12, 1).AddMonths(1).AddDays(-1));
				}
				where = where.And(quarterWhere);

				sqlBuilder.Append(where);
				sqlBuilder.AppendLine(" GROUP BY t1.CostcenterID");
				sqlBuilder.AppendLine(") SumOrder ORDER BY SumGrandTotalAmt DESC");

				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						CostcenterSummaryOrder data = new CostcenterSummaryOrder();
						data.CostcenterId = (string)reader["CostcenterID"];
						data.CostcenterName = new LocalizedString((string)reader["CostTName"]);
						data.CostcenterName["en"] = (string)reader["CostEName"];
						data.SumGrandTotalAmt = (decimal)reader["SumGrandTotalAmt"];
						data.CountOrder = (int)reader["CountOrder"];
						sumData.Add(data);
					}
				}
				return sumData;
			}
		}

		public IEnumerable<Order> GetListOrderFromSummaryOrderCostCenter(TrackCostCenter trackCostCenter, string companyId)
		{
			List<Order> orders = new List<Order>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				StringBuilder sqlBuilder = new StringBuilder();
				sqlBuilder.AppendLine("SELECT	t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("isnull(t3.UserGuid,'') as AppGuid, isnull(t3.UserTName,'') as AppTName, isnull(t3.UserEName,'') as AppEName, isnull(t3.UserLang,'') as AppLang,");
				sqlBuilder.AppendLine("isnull(t4.UserGuid,'') as PreAppGuid, isnull(t4.UserTName,'') as PreAppTName, isnull(t4.UserEName,'') as PreAppEName, isnull(t4.UserLang,'') as PreAppLang");
				sqlBuilder.AppendLine("FROM	TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("INNER JOIN TBUsers tr with (nolock) ON t1.UserID=tr.UserID");
				sqlBuilder.AppendLine("LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.CurrentAppID=t3.UserID");
				sqlBuilder.AppendLine("LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t4 ON t1.PreviousAppID = t4.UserID");

				var where = WhereClause.Create("t1.CompanyID = @companyId");
				command.Parameters.AddWithValue("@companyId", companyId);

				where = where.And(WhereClause.Create("OrderStatus IN ('Approved','Shipped','Completed')"));

				where = where.And(WhereClause.Create("t1.CostcenterId = @costcenterId"));
				command.Parameters.AddWithValue("@costcenterId", trackCostCenter.CostcenterId);

				var monthWhere = WhereClause.Create();
				foreach (var month in trackCostCenter.Months)
				{
					monthWhere = monthWhere.Or(string.Format(" OrderDate between @from_{0} and @to_{0}", month));
					command.Parameters.AddWithValue("@from_" + month, new DateTime(DateTime.Today.Year, month, 1));
					command.Parameters.AddWithValue("@to_" + month, new DateTime(DateTime.Today.Year, month, 1).AddMonths(1).AddDays(-1));
				}

				where = where.And(monthWhere);

				var quarterWhere = WhereClause.Create();
				if (trackCostCenter.Quarters1)
				{
					quarterWhere = quarterWhere.Or(" OrderDate between @fromQ1 and @toQ1");
					command.Parameters.AddWithValue("@fromQ1", new DateTime(DateTime.Today.Year, 1, 1));
					command.Parameters.AddWithValue("@toQ1", new DateTime(DateTime.Today.Year, 3, 1).AddMonths(1).AddDays(-1));
				}
				if (trackCostCenter.Quarters2)
				{
					quarterWhere = quarterWhere.Or(" OrderDate between @fromQ2 and @toQ2");
					command.Parameters.AddWithValue("@fromQ2", new DateTime(DateTime.Today.Year, 4, 1));
					command.Parameters.AddWithValue("@toQ2", new DateTime(DateTime.Today.Year, 6, 1).AddMonths(1).AddDays(-1));
				}
				if (trackCostCenter.Quarters3)
				{
					quarterWhere = quarterWhere.Or(" OrderDate between @fromQ3 and @toQ3");
					command.Parameters.AddWithValue("@fromQ3", new DateTime(DateTime.Today.Year, 7, 1));
					command.Parameters.AddWithValue("@toQ3", new DateTime(DateTime.Today.Year, 9, 1).AddMonths(1).AddDays(-1));
				}
				if (trackCostCenter.Quarters4)
				{
					quarterWhere = quarterWhere.Or(" OrderDate between @fromQ4 and @toQ4");
					command.Parameters.AddWithValue("@fromQ4", new DateTime(DateTime.Today.Year, 10, 1));
					command.Parameters.AddWithValue("@toQ4", new DateTime(DateTime.Today.Year, 12, 1).AddMonths(1).AddDays(-1));
				}

				where = where.And(quarterWhere);
				sqlBuilder.Append(where);

				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrder(reader));
					}
				}
				return orders;
			}
		}

		private string SetSearchOrderStatus(AdvanceSearchField advanceSearchField)
		{
			string searchStatus = "";
			if (advanceSearchField.StatusWaiting || advanceSearchField.StatusApproved || advanceSearchField.StatusPartial || advanceSearchField.StatusRevise
				|| advanceSearchField.StatusAdminAllow || advanceSearchField.StatusWaitingAdmin || advanceSearchField.StatusShipped
				|| advanceSearchField.StatusDeleted || advanceSearchField.StatusExpired || advanceSearchField.StatusCompleted)
			{
				searchStatus += "AND OrderStatus IN (";
				if (advanceSearchField.StatusWaiting)
				{
					searchStatus += " '" + Order.OrderStatus.Waiting.ToString() + "',";
				}
				if (advanceSearchField.StatusApproved)
				{
					searchStatus += " '" + Order.OrderStatus.Approved.ToString() + "',";
				}
				if (advanceSearchField.StatusPartial)
				{
					searchStatus += " '" + Order.OrderStatus.Partial.ToString() + "',";
				}
				if (advanceSearchField.StatusRevise)
				{
					searchStatus += " '" + Order.OrderStatus.Revise.ToString() + "',";
				}
				if (advanceSearchField.StatusAdminAllow)
				{
					searchStatus += " '" + Order.OrderStatus.AdminAllow.ToString() + "',";
				}
				if (advanceSearchField.StatusWaitingAdmin)
				{
					searchStatus += " '" + Order.OrderStatus.WaitingAdmin.ToString() + "',";
				}
				if (advanceSearchField.StatusShipped)
				{
					searchStatus += " '" + Order.OrderStatus.Shipped.ToString() + "',";
				}
				if (advanceSearchField.StatusDeleted)
				{
					searchStatus += " '" + Order.OrderStatus.Deleted.ToString() + "',";
				}
				if (advanceSearchField.StatusExpired)
				{
					searchStatus += " '" + Order.OrderStatus.Expired.ToString() + "',";
				}
				if (advanceSearchField.StatusCompleted)
				{
					searchStatus += " '" + Order.OrderStatus.Completed.ToString() + "',";
				}
				searchStatus = searchStatus.TrimEnd(',');
				searchStatus += ")";
			}
			return searchStatus;
		}

		private DateTime ConvertStringToDateTime(string stringDate)
		{
			DateTime dateTime = DateTime.MinValue;
			if (!String.IsNullOrEmpty(stringDate) && DateTime.TryParseExact(stringDate, "d/M/yyyy", System.Globalization.CultureInfo.InvariantCulture, System.Globalization.DateTimeStyles.None, out dateTime))
			{
				return dateTime;
			}
			return dateTime;
		}

		private Order MapOrder(SqlDataReader reader)
		{
			Order order = new Order();
			order.CustId = (string)reader["CustId"];
			order.OrderID = (string)reader["OrderID"];
			order.OrderGuid = (string)reader["OrderGuid"];
			order.OrderDate = (DateTime)reader["OrderDate"];
			order.ApproveOrderDate = (DateTime)reader["ApproveOrderDate"];
			order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
			order.ItemCountOrder = (int)reader["CountProduct"];
			order.TotalNetAmt = (decimal)reader["NetAmt"];
			order.TotalAllDiscount = (decimal)reader["DiscountAmt"];
			order.TotalDeliveryCharge = (decimal)reader["OrdDeliverFee"];
			order.TotalDeliveryFee = (decimal)reader["DeliveryFee"];
			order.TotalPriceProductExcVatAmount = (decimal)reader["VatProdNetAmt"];
			order.TotalPriceProductNoneVatAmount = (decimal)reader["NonVatProdNetAmt"];
			order.TotalVatAmt = (decimal)reader["VatAmt"];
			order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];
			order.ParkDay = (int)reader["ParkDay"];
			order.WarningExpireDate = (DateTime)reader["WarningExpireDate"];
			order.ExpireDate = (DateTime)reader["ExpireDate"];
			order.ApproverRemark = (string)reader["ApproverRemark"];
			order.OFMRemark = (string)reader["OFMRemark"];
			order.ReferenceRemark = (string)reader["ReferenceRemark"];
			order.AttachFile = (string)reader["AttachFile"];
			order.CallBackRequest = (string)reader["CallBackRequest"];
			order.NumOfAdjust = (int)reader["NumOfAdjust"];

			order.CostCenter = new CostCenter();
			order.CostCenter.CostCenterID = (string)reader["CostcenterID"];
			order.CostCenter.CostCenterThaiName = (string)reader["CostTName"];
			order.CostCenter.CostCenterEngName = (string)reader["CostEName"];
			order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
			order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];

			order.Invoice = new Invoice();
			order.Invoice.Address1 = (string)reader["InvAddr1"];
			order.Invoice.Address2 = (string)reader["InvAddr2"];
			order.Invoice.Address3 = (string)reader["InvAddr3"];
			order.Invoice.Address4 = (string)reader["InvAddr4"];

			order.Shipping = new Shipping();
			order.Shipping.ShipContactor = (string)reader["ShipContactor"];
			order.Shipping.Address1 = (string)reader["ShipAddr1"];
			order.Shipping.Address2 = (string)reader["ShipAddr2"];
			order.Shipping.Address3 = (string)reader["ShipAddr3"];
			order.Shipping.Address4 = (string)reader["ShipAddr4"];
			order.Shipping.Province = (string)reader["ShipProvince"];
			order.Shipping.ShipPhoneNo = (string)reader["ShipPhoneNo"];
			order.Shipping.ShipMobileNo = (string)reader["ShipMobileNo"];
			order.Shipping.Remark = (string)reader["ShipRemark"];

			order.Contact = new Contact();
			order.Contact.ContactorName = (string)reader["ContactorName"];
			order.Contact.ContactorPhone = (string)reader["ContactorPhone"];
			order.Contact.ContactorExtension = (string)reader["ContactorExtension"];
			order.Contact.ContactMobileNo = (string)reader["ContactMobileNo"];
			order.Contact.ContactorFax = (string)reader["ContactorFax"];
			order.Contact.Email = (string)reader["ContactorEmail"];

			order.Requester = new User();
			order.Requester.UserId = (string)reader["UserID"];
			order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
			order.Requester.DisplayName["en"] = (string)reader["UserEName"];
			order.Requester.UserThaiName = (string)reader["UserTName"];
			order.Requester.UserEngName = (string)reader["UserEName"];


			if (!string.IsNullOrEmpty(reader["CurrentAppID"].ToString()))
			{
				order.CurrentApprover = new UserApprover();
				order.CurrentApprover.Approver = new User();
				order.CurrentApprover.Approver.UserGuid = (string)reader["AppGuid"];
				order.CurrentApprover.Approver.UserId = (string)reader["CurrentAppID"];
				order.CurrentApprover.Approver.DisplayName = new LocalizedString((string)reader["AppTName"]);
				order.CurrentApprover.Approver.DisplayName["en"] = (string)reader["AppEName"];
				order.CurrentApprover.Approver.UserThaiName = (string)reader["AppTName"];
				order.CurrentApprover.Approver.UserEngName = (string)reader["AppEName"];
				order.CurrentApprover.Approver.UserLanguage = !string.IsNullOrEmpty(reader["AppLang"].ToString()) ? (User.Language)Enum.Parse(typeof(User.Language), (string)reader["AppLang"], true) : User.Language.TH;
			}

			if (!string.IsNullOrEmpty(reader["PreviousAppID"].ToString()))
			{
				order.PreviousApprover = new UserApprover();
				order.PreviousApprover.Approver = new User();
				order.PreviousApprover.Approver.UserGuid = (string)reader["PreAppGuid"];
				order.PreviousApprover.Approver.UserId = (string)reader["PreviousAppID"];
				order.PreviousApprover.Approver.DisplayName = new LocalizedString((string)reader["PreAppTName"]);
				order.PreviousApprover.Approver.DisplayName["en"] = (string)reader["PreAppEName"];
				order.PreviousApprover.Approver.UserThaiName = (string)reader["PreAppTName"];
				order.PreviousApprover.Approver.UserEngName = (string)reader["PreAppEName"];
				order.PreviousApprover.Approver.UserLanguage = !string.IsNullOrEmpty(reader["PreAppLang"].ToString()) ? (User.Language)Enum.Parse(typeof(User.Language), (string)reader["PreAppLang"], true) : User.Language.TH;
			}
			return order;
		}

		public IEnumerable<ReportBudgetData> GetReportBudget(string companyId, Company.BudgetLevel budgetLevel)
		{
			List<ReportBudgetData> budgets = new List<ReportBudgetData>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				if (budgetLevel == Company.BudgetLevel.Company)
				{
					command.CommandText = @"SELECT * FROM TBBudget T1 WHERE T1.CompanyID=@companyId ORDER BY Year desc, CAST(SUBSTRING (PeriodNo, 2, 2) AS INT)";
				}
				else if (budgetLevel == Company.BudgetLevel.Costcenter)
				{
					command.CommandText = @"SELECT T1.*,T2.CostcenterID,T2.CostTName,T2.CostEName
											FROM TBBudget T1
											INNER JOIN TBCostcenter T2 ON T1.CompanyID=T2.CompanyID AND T1.GroupID=T2.CostcenterID
											WHERE T1.CompanyID=@companyId ORDER BY Year desc, CAST(SUBSTRING (PeriodNo, 2, 2) AS INT)";
				}
				else
				{
					command.CommandText = @"SELECT T1.*,T2.DepartmentID,T2.DeptTName,T2.DeptEName
											FROM TBBudget T1
											INNER JOIN TBDepartment T2 ON T1.CompanyID=T2.CompanyID AND T1.GroupID=T2.DepartmentID
											WHERE T1.CompanyID=@companyId ORDER BY Year desc, CAST(SUBSTRING (PeriodNo, 2, 2) AS INT)";
				}
				command.Parameters.AddWithValue("@companyId", companyId);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					ReportBudgetData budget = null;
					while (reader.Read())
					{
						budget = new ReportBudgetData();
						budget.Budget = new Budget();
						budget.Budget.OriginalAmt = (decimal)reader["OriginalAmt"];
						budget.Budget.BudgetAmt = (decimal)reader["BudgetAmt"];
						budget.Budget.UsedAmt = (decimal)reader["UsedAmt"];
						budget.Budget.RemainBudgetAmt = (decimal)reader["RemainBudgetAmt"];
						budget.Budget.GroupID = (string)reader["GroupID"];
						budget.Budget.PeriodNo = (Budget.Period)Enum.Parse(typeof(Budget.Period), (string)reader["PeriodNo"], true);
						budget.Budget.Year = (string)reader["Year"];

						if (budgetLevel == Company.BudgetLevel.Costcenter)
						{
							budget.CostCenter = new CostCenter();
							budget.CostCenter.CostCenterID = (string)reader["CostCenterID"];
							budget.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
							budget.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];
							budget.CostCenter.CostCenterThaiName = (string)reader["CostTName"];
							budget.CostCenter.CostCenterEngName = (string)reader["CostEName"];
						}

						if (budgetLevel == Company.BudgetLevel.Department)
						{
							budget.Department = new CompanyDepartment();
							budget.Department.DepartmentID = (string)reader["DepartmentID"];
							budget.Department.DepartmentName = new LocalizedString((string)reader["DeptTName"]);
							budget.Department.DepartmentName["en"] = (string)reader["DeptEName"];
							budget.Department.DepartmentThaiName = (string)reader["DeptTName"];
							budget.Department.DepartmentEngName = (string)reader["DeptEName"];
						}
						budgets.Add(budget);
					}
				}
			}
			return budgets;
		}

		public IEnumerable<ReportBudgetData> GetReportBudgetDetail(string companyId, string groupId, string periodNo, string year)
		{
			List<ReportBudgetData> budgets = new List<ReportBudgetData>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT * FROM TBBudgetLog with (nolock) WHERE CompanyID=@companyId AND GroupID=@groupId AND PeriodNo=@periodNo AND Year=@year";
				command.Parameters.AddWithValue("@companyId", companyId);
				command.Parameters.AddWithValue("@groupId", groupId);
				command.Parameters.AddWithValue("@periodNo", periodNo);
				command.Parameters.AddWithValue("@year", year);

				using (SqlDataReader reader = command.ExecuteReader())
				{
					ReportBudgetData budget = null;
					while (reader.Read())
					{
						budget = new ReportBudgetData();
						budget.Budget = new Budget();
						budget.Budget.PeriodNo = (Budget.Period)Enum.Parse(typeof(Budget.Period), (string)reader["PeriodNo"], true);
						budget.Budget.OriginalAmt = (decimal)reader["OldAmt"];
						budget.Budget.BudgetAmt = (decimal)reader["IncAmt"];
						budget.Budget.UsedAmt = (decimal)reader["DecAmt"];
						budget.Budget.RemainBudgetAmt = (decimal)reader["NewAmt"];
						budget.Budget.GroupID = (string)reader["GroupID"];
						budget.Budget.CreateOn = (DateTime)reader["CreateOn"];
						budget.Budget.Year = (string)reader["Year"];

						budget.Order = new Order();
						budget.Order.OrderID = (string)reader["OrderID"];

						budgets.Add(budget);
					}
				}
			}
			return budgets;
		}


		public IEnumerable<Order> GetTrackOrderByGoodReceive(int pageSize, int currentIndex, DateFilter? dateFilter, Order.OrderStatus orderStatus, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT Top (@pageSize) * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, t1.OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as PreAppGuid, isnull(t3.UserTName,'') as PreAppTName, isnull(t3.UserEName,'') as PreAppEName, isnull(t3.UserLang,'') as PreAppLang,");
				sqlBuilder.AppendLine("		t4.ActionQty,t4.CancelQty,t4.Status,t4.UpdateOn as ReceiveDate");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.PreviousAppID = t3.UserID");
				sqlBuilder.AppendLine("		INNER JOIN (");
				sqlBuilder.AppendLine("		SELECT	DISTINCT GRHead.CompanyId,GRHead.OrderId,GRHead.Status,GRHead.UpdateOn,sum(GRDetail.ActionQty) AS ActionQty,sum(GRDetail.CancelQty) AS CancelQty");
				sqlBuilder.AppendLine("		FROM	TBGoodReceive GRHead with (nolock)");
				sqlBuilder.AppendLine("				INNER JOIN TBGoodReceiveDetail GRDetail with (nolock) ON GRHead.OrderId=GRDetail.OrderId");
				sqlBuilder.AppendLine("		WHERE	GRHead.CompanyID=@CompanyID AND GRHead.Status <> 'Completed'");
				sqlBuilder.AppendLine("				GROUP BY GRHead.CompanyId,GRHead.OrderId,GRHead.Status,GRHead.UpdateOn");
				sqlBuilder.AppendLine("		)t4 ON t1.OrderID=t4.OrderId AND t1.CompanyID=t4.CompanyId");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID AND t1.OrderStatus=@orderStatus ");
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@orderStatus", orderStatus.ToString());

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t3.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				sqlBuilder.AppendLine(") Orders");
				sqlBuilder.AppendLine("WHERE row_number >= @currentItem");
				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@pageSize", pageSize);
				command.Parameters.AddWithValue("@currentItem", currentIndex);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrderGoodReceive(reader));
					}
				}
				return orders;
			}
		}

		public IEnumerable<Order> GetTrackOrderByGoodReceiveWithoutPaging(DateFilter? dateFilter, Order.OrderStatus orderStatus, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By ApproveOrderDate desc, t1.OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as PreAppGuid, isnull(t3.UserTName,'') as PreAppTName, isnull(t3.UserEName,'') as PreAppEName, isnull(t3.UserLang,'') as PreAppLang,");
				sqlBuilder.AppendLine("		t4.ActionQty,t4.CancelQty,t4.Status,t4.UpdateOn as ReceiveDate");
				sqlBuilder.AppendLine("FROM TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.PreviousAppID = t3.UserID");
				sqlBuilder.AppendLine("		INNER JOIN (");
				sqlBuilder.AppendLine("		SELECT	DISTINCT GRHead.CompanyId,GRHead.OrderId,GRHead.Status,GRHead.UpdateOn,sum(GRDetail.ActionQty) AS ActionQty,sum(GRDetail.CancelQty) AS CancelQty");
				sqlBuilder.AppendLine("		FROM	TBGoodReceive GRHead with (nolock)");
				sqlBuilder.AppendLine("				INNER JOIN TBGoodReceiveDetail GRDetail with (nolock) ON GRHead.OrderId=GRDetail.OrderId");
				sqlBuilder.AppendLine("		WHERE	GRHead.CompanyID=@CompanyID AND GRHead.Status <> 'Completed'");
				sqlBuilder.AppendLine("				GROUP BY GRHead.CompanyId,GRHead.OrderId,GRHead.Status,GRHead.UpdateOn");
				sqlBuilder.AppendLine("		)t4 ON t1.OrderID=t4.OrderId AND t1.CompanyID=t4.CompanyId");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID AND t1.OrderStatus=@orderStatus ");
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@orderStatus", orderStatus.ToString());

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t3.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}
				sqlBuilder.AppendLine(") Orders");
				command.CommandText = sqlBuilder.ToString();
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrderGoodReceive(reader));
					}
				}
				return orders;
			}
		}

		public int GetCountTrackOrderByGoodReceive(DateFilter? dateFilter, Order.OrderStatus orderStatus, string companyId, string userId, bool isAdmin)
		{
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT	count(*) FROM TBOrder t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t3 ON t1.PreviousAppID = t3.UserID");
				sqlBuilder.AppendLine("		INNER JOIN TBGoodReceive t4 ON t1.OrderID=t4.OrderId AND t1.CompanyID=t4.CompanyId AND t4.Status <> 'Completed'");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID AND t1.OrderStatus=@orderStatus ");
				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.Parameters.AddWithValue("@orderStatus", orderStatus.ToString());

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t3.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				//if (dateFilter.HasValue)
				//{
				//    switch (dateFilter)
				//    {
				//        case DateFilter.Today:
				//            sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
				//            command.Parameters.AddWithValue("@dateFrom", DateTime.Today);
				//            command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(1));
				//            break;
				//        case DateFilter.OneToThreeDays:
				//            sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
				//            command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-3));
				//            command.Parameters.AddWithValue("@dateTo", DateTime.Today);
				//            break;
				//        case DateFilter.FourToSevenDays:
				//            sqlBuilder.Append(" AND OrderDate Between @dateFrom And @dateTo");
				//            command.Parameters.AddWithValue("@dateFrom", DateTime.Today.AddDays(-7));
				//            command.Parameters.AddWithValue("@dateTo", DateTime.Today.AddDays(-3));
				//            break;
				//        case DateFilter.SevenDaysLater:
				//            sqlBuilder.Append(" AND OrderDate < @sevenDayLater");
				//            command.Parameters.AddWithValue("@sevenDayLater", DateTime.Today.AddDays(-7));
				//            break;
				//    }
				//}

				command.CommandText = sqlBuilder.ToString();
				return (int)command.ExecuteScalar();
			}
		}

		public int GetCountGoodReceiveOrder(AdvanceSearchField advanceSearchField, string companyId, string userId, bool isAdmin)
		{
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT	count(*) FROM TBOrder t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		INNER JOIN TBUsers tr ON t1.UserID=tr.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t3 ON t1.PreviousAppID = t3.UserID");
				sqlBuilder.AppendLine("		INNER JOIN TBGoodReceive t4 ON t1.OrderID=t4.OrderId AND t1.CompanyID=t4.CompanyId");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t3.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (advanceSearchField.CheckOrderStatus)
				{
					sqlBuilder.AppendLine(SetSearchGoodReceiveStatus(advanceSearchField));
				}

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND OrderDate BETWEEN @DateFrom AND @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckAppDate && !string.IsNullOrEmpty(advanceSearchField.AppDateFrom) && !string.IsNullOrEmpty(advanceSearchField.AppDateTo))
				{
					DateTime appFrom = ConvertStringToDateTime(advanceSearchField.AppDateFrom);
					DateTime appTo = ConvertStringToDateTime(advanceSearchField.AppDateTo);
					if (appFrom != DateTime.MinValue && appTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND t4.UpdateOn BETWEEN @AppDateFrom AND @AppDateTo");
						command.Parameters.AddWithValue("@AppDateFrom", appFrom.Date);
						command.Parameters.AddWithValue("@AppDateTo", appTo.Date.AddDays(1));
					}
				}

				if (isAdmin)
				{
					if (advanceSearchField.CheckRequester && advanceSearchField.Requester.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var req in advanceSearchField.Requester)
						{
							sqlBuilder.AppendFormat("OR tr.UserGuid = @Req_{0} ", index);
							command.Parameters.AddWithValue("@Req_" + index, req);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckApprover && advanceSearchField.Approver.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var app in advanceSearchField.Approver)
						{
							sqlBuilder.AppendFormat("OR t3.UserGuid = @App_{0} ", index);
							command.Parameters.AddWithValue("@App_" + index, app);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckCostcenter && advanceSearchField.Costcenter.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var cost in advanceSearchField.Costcenter)
						{
							sqlBuilder.AppendFormat("OR t1.CostcenterID = @Cost_{0} ", index);
							command.Parameters.AddWithValue("@Cost_" + index, cost);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}
				}

				command.CommandText = sqlBuilder.ToString();
				return (int)command.ExecuteScalar();
			}
		}

		public IEnumerable<Order> GetGoodReceiveOrder(int pageSize, int currentIndex, AdvanceSearchField advanceSearchField, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT Top (@pageSize) * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By t4.UpdateOn desc, t1.OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as PreAppGuid, isnull(t3.UserTName,'') as PreAppTName, isnull(t3.UserEName,'') as PreAppEName, isnull(t3.UserLang,'') as PreAppLang,");
				sqlBuilder.AppendLine("		t4.ActionQty,t4.CancelQty,t4.Status,t4.UpdateOn as ReceiveDate");
				sqlBuilder.AppendLine("FROM	TBOrder t1 INNER JOIN TBCostcenter t2 ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		INNER JOIN TBUsers tr ON t1.UserID=tr.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta INNER JOIN TBUsers tb ON ta.UserID=tb.UserId) t3 ON t1.PreviousAppID = t3.UserID");
				sqlBuilder.AppendLine("		INNER JOIN (");
				sqlBuilder.AppendLine("			SELECT	DISTINCT GRHead.CompanyId,GRHead.OrderId,GRHead.Status,GRHead.UpdateOn,sum(GRDetail.ActionQty) AS ActionQty,sum(GRDetail.CancelQty) AS CancelQty");
				sqlBuilder.AppendLine("			FROM	TBGoodReceive GRHead");
				sqlBuilder.AppendLine("					INNER JOIN TBGoodReceiveDetail GRDetail ON GRHead.OrderId=GRDetail.OrderId");
				sqlBuilder.AppendLine("			WHERE	GRHead.CompanyID=@CompanyID");
				sqlBuilder.AppendLine("					GROUP BY GRHead.CompanyId,GRHead.OrderId,GRHead.Status,GRHead.UpdateOn");
				sqlBuilder.AppendLine("		) t4 ON t1.OrderID=t4.OrderId AND t1.CompanyID=t4.CompanyId");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t3.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (advanceSearchField.CheckOrderStatus)
				{
					sqlBuilder.AppendLine(SetSearchGoodReceiveStatus(advanceSearchField));
				}

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND OrderDate BETWEEN @DateFrom AND @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckAppDate && !string.IsNullOrEmpty(advanceSearchField.AppDateFrom) && !string.IsNullOrEmpty(advanceSearchField.AppDateTo))
				{
					DateTime appFrom = ConvertStringToDateTime(advanceSearchField.AppDateFrom);
					DateTime appTo = ConvertStringToDateTime(advanceSearchField.AppDateTo);
					if (appFrom != DateTime.MinValue && appTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND t4.UpdateOn BETWEEN @AppDateFrom AND @AppDateTo");
						command.Parameters.AddWithValue("@AppDateFrom", appFrom.Date);
						command.Parameters.AddWithValue("@AppDateTo", appTo.Date.AddDays(1));
					}
				}

				if (isAdmin)
				{
					if (advanceSearchField.CheckRequester && advanceSearchField.Requester.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var req in advanceSearchField.Requester)
						{
							sqlBuilder.AppendFormat("OR tr.UserGuid = @Req_{0} ", index);
							command.Parameters.AddWithValue("@Req_" + index, req);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckApprover && advanceSearchField.Approver.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var app in advanceSearchField.Approver)
						{
							sqlBuilder.AppendFormat("OR t3.UserGuid = @App_{0} ", index);
							command.Parameters.AddWithValue("@App_" + index, app);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckCostcenter && advanceSearchField.Costcenter.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var cost in advanceSearchField.Costcenter)
						{
							sqlBuilder.AppendFormat("OR t1.CostcenterID = @Cost_{0} ", index);
							command.Parameters.AddWithValue("@Cost_" + index, cost);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}
				}

				sqlBuilder.AppendLine(") Orders");
				sqlBuilder.AppendLine("WHERE row_number >= @currentItem");
				command.CommandText = sqlBuilder.ToString();
				command.Parameters.AddWithValue("@pageSize", pageSize);
				command.Parameters.AddWithValue("@currentItem", currentIndex);
				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrderGoodReceive(reader));
					}
				}
				return orders;
			}
		}

		public IEnumerable<Order> GetGoodReceiveOrderWithoutPaging(AdvanceSearchField advanceSearchField, string companyId, string userId, bool isAdmin)
		{
			List<Order> orders = new List<Order>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT * FROM (");
				sqlBuilder.AppendLine("SELECT Row_Number() Over (Order By t4.UpdateOn desc, t1.OrderId desc) As 'row_number',");
				sqlBuilder.AppendLine("		t1.*, t2.CostTName, t2.CostEName,");
				sqlBuilder.AppendLine("		isnull(t3.UserGuid,'') as PreAppGuid, isnull(t3.UserTName,'') as PreAppTName, isnull(t3.UserEName,'') as PreAppEName, isnull(t3.UserLang,'') as PreAppLang,");
				sqlBuilder.AppendLine("		t4.ActionQty,t4.CancelQty,t4.Status,t4.UpdateOn as ReceiveDate");
				sqlBuilder.AppendLine("FROM	TBOrder t1 with (nolock) INNER JOIN TBCostcenter t2 with (nolock) ON t1.CompanyID=t2.CompanyID AND t1.DepartmentID=t2.DepartmentID AND t1.CostcenterID=t2.CostcenterID");
				sqlBuilder.AppendLine("		INNER JOIN TBUsers tr with (nolock) ON t1.UserID=tr.UserID");
				sqlBuilder.AppendLine("		LEFT JOIN (SELECT ta.*,tb.UserGuid FROM TBUserInfo ta with (nolock) INNER JOIN TBUsers tb with (nolock) ON ta.UserID=tb.UserId) t3 ON t1.PreviousAppID = t3.UserID");
				sqlBuilder.AppendLine("		INNER JOIN (");
				sqlBuilder.AppendLine("			SELECT	DISTINCT GRHead.CompanyId,GRHead.OrderId,GRHead.Status,GRHead.UpdateOn,sum(GRDetail.ActionQty) AS ActionQty,sum(GRDetail.CancelQty) AS CancelQty");
				sqlBuilder.AppendLine("			FROM	TBGoodReceive GRHead with (nolock)");
				sqlBuilder.AppendLine("					INNER JOIN TBGoodReceiveDetail GRDetail with (nolock) ON GRHead.OrderId=GRDetail.OrderId");
				sqlBuilder.AppendLine("			WHERE	GRHead.CompanyID=@CompanyID");
				sqlBuilder.AppendLine("					GROUP BY GRHead.CompanyId,GRHead.OrderId,GRHead.Status,GRHead.UpdateOn");
				sqlBuilder.AppendLine("		) t4 ON t1.OrderID=t4.OrderId AND t1.CompanyID=t4.CompanyId");
				sqlBuilder.AppendLine("WHERE	t1.CompanyID=@CompanyID");
				command.Parameters.AddWithValue("@CompanyID", companyId);

				if (!isAdmin)
				{
					sqlBuilder.AppendLine("AND (t1.UserID = @UserID OR t3.UserID = @UserID)");
					command.Parameters.AddWithValue("@UserID", userId);
				}

				if (advanceSearchField.CheckOrderStatus)
				{
					sqlBuilder.AppendLine(SetSearchGoodReceiveStatus(advanceSearchField));
				}

				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND OrderDate BETWEEN @DateFrom AND @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckAppDate && !string.IsNullOrEmpty(advanceSearchField.AppDateFrom) && !string.IsNullOrEmpty(advanceSearchField.AppDateTo))
				{
					DateTime appFrom = ConvertStringToDateTime(advanceSearchField.AppDateFrom);
					DateTime appTo = ConvertStringToDateTime(advanceSearchField.AppDateTo);
					if (appFrom != DateTime.MinValue && appTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND t4.UpdateOn BETWEEN @AppDateFrom AND @AppDateTo");
						command.Parameters.AddWithValue("@AppDateFrom", appFrom.Date);
						command.Parameters.AddWithValue("@AppDateTo", appTo.Date.AddDays(1));
					}
				}

				if (isAdmin)
				{
					if (advanceSearchField.CheckRequester && advanceSearchField.Requester.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var req in advanceSearchField.Requester)
						{
							sqlBuilder.AppendFormat("OR tr.UserGuid = @Req_{0} ", index);
							command.Parameters.AddWithValue("@Req_" + index, req);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckApprover && advanceSearchField.Approver.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var app in advanceSearchField.Approver)
						{
							sqlBuilder.AppendFormat("OR t3.UserGuid = @App_{0} ", index);
							command.Parameters.AddWithValue("@App_" + index, app);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}

					if (advanceSearchField.CheckCostcenter && advanceSearchField.Costcenter.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var cost in advanceSearchField.Costcenter)
						{
							sqlBuilder.AppendFormat("OR t1.CostcenterID = @Cost_{0} ", index);
							command.Parameters.AddWithValue("@Cost_" + index, cost);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}
				}

				sqlBuilder.AppendLine(") Orders");
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						orders.Add(MapOrderGoodReceive(reader));
					}
				}
				return orders;
			}
		}

		private Order MapOrderGoodReceive(SqlDataReader reader)
		{
			Order order = new Order();
			order.CustId = (string)reader["CustId"];
			order.OrderID = (string)reader["OrderID"];
			order.OrderGuid = (string)reader["OrderGuid"];
			order.OrderDate = (DateTime)reader["OrderDate"];
			order.ApproveOrderDate = (DateTime)reader["ApproveOrderDate"];
			order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);
			order.ItemCountOrder = (int)reader["CountProduct"];
			order.TotalNetAmt = (decimal)reader["NetAmt"];
			order.TotalAllDiscount = (decimal)reader["DiscountAmt"];
			order.TotalDeliveryCharge = (decimal)reader["OrdDeliverFee"];
			order.TotalDeliveryFee = (decimal)reader["DeliveryFee"];
			order.TotalPriceProductExcVatAmount = (decimal)reader["VatProdNetAmt"];
			order.TotalPriceProductNoneVatAmount = (decimal)reader["NonVatProdNetAmt"];
			order.TotalVatAmt = (decimal)reader["VatAmt"];
			order.GrandTotalAmt = (decimal)reader["GrandTotalAmt"];
			order.ParkDay = (int)reader["ParkDay"];
			order.WarningExpireDate = (DateTime)reader["WarningExpireDate"];
			order.ExpireDate = (DateTime)reader["ExpireDate"];
			order.ApproverRemark = (string)reader["ApproverRemark"];
			order.OFMRemark = (string)reader["OFMRemark"];
			order.ReferenceRemark = (string)reader["ReferenceRemark"];
			order.AttachFile = (string)reader["AttachFile"];
			order.CallBackRequest = (string)reader["CallBackRequest"];
			order.NumOfAdjust = (int)reader["NumOfAdjust"];

			order.GoodReceiveDetail = new GoodReceiveDetail();
			order.GoodReceiveDetail.ActionQty = (int)reader["ActionQty"];
			order.GoodReceiveDetail.CancelQty = (int)reader["CancelQty"];

			order.CostCenter = new CostCenter();
			order.CostCenter.CostCenterID = (string)reader["CostcenterID"];
			order.CostCenter.CostCenterThaiName = (string)reader["CostTName"];
			order.CostCenter.CostCenterEngName = (string)reader["CostEName"];
			order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
			order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];

			order.Invoice = new Invoice();
			order.Invoice.Address1 = (string)reader["InvAddr1"];
			order.Invoice.Address2 = (string)reader["InvAddr2"];
			order.Invoice.Address3 = (string)reader["InvAddr3"];
			order.Invoice.Address4 = (string)reader["InvAddr4"];

			order.Shipping = new Shipping();
			order.Shipping.ShipContactor = (string)reader["ShipContactor"];
			order.Shipping.Address1 = (string)reader["ShipAddr1"];
			order.Shipping.Address2 = (string)reader["ShipAddr2"];
			order.Shipping.Address3 = (string)reader["ShipAddr3"];
			order.Shipping.Address4 = (string)reader["ShipAddr4"];
			order.Shipping.Province = (string)reader["ShipProvince"];
			order.Shipping.ShipPhoneNo = (string)reader["ShipPhoneNo"];
			order.Shipping.ShipMobileNo = (string)reader["ShipMobileNo"];
			order.Shipping.Remark = (string)reader["ShipRemark"];

			order.Contact = new Contact();
			order.Contact.ContactorName = (string)reader["ContactorName"];
			order.Contact.ContactorPhone = (string)reader["ContactorPhone"];
			order.Contact.ContactorExtension = (string)reader["ContactorExtension"];
			order.Contact.ContactMobileNo = (string)reader["ContactMobileNo"];
			order.Contact.ContactorFax = (string)reader["ContactorFax"];
			order.Contact.Email = (string)reader["ContactorEmail"];

			order.Requester = new User();
			order.Requester.UserId = (string)reader["UserID"];
			order.Requester.DisplayName = new LocalizedString((string)reader["UserTName"]);
			order.Requester.DisplayName["en"] = (string)reader["UserEName"];
			order.Requester.UserThaiName = (string)reader["UserTName"];
			order.Requester.UserEngName = (string)reader["UserEName"];


			if (!string.IsNullOrEmpty(reader["PreviousAppID"].ToString()))
			{
				order.PreviousApprover = new UserApprover();
				order.PreviousApprover.Approver = new User();
				order.PreviousApprover.Approver.UserGuid = (string)reader["PreAppGuid"];
				order.PreviousApprover.Approver.UserId = (string)reader["PreviousAppID"];
				order.PreviousApprover.Approver.DisplayName = new LocalizedString((string)reader["PreAppTName"]);
				order.PreviousApprover.Approver.DisplayName["en"] = (string)reader["PreAppEName"];
				order.PreviousApprover.Approver.UserThaiName = (string)reader["PreAppTName"];
				order.PreviousApprover.Approver.UserEngName = (string)reader["PreAppEName"];
				order.PreviousApprover.Approver.UserLanguage = !string.IsNullOrEmpty(reader["PreAppLang"].ToString()) ? (User.Language)Enum.Parse(typeof(User.Language), (string)reader["PreAppLang"], true) : User.Language.TH;
			}

			order.GoodReceive = new GoodReceive();
			order.GoodReceive.Status = (GoodReceive.GoodReceiveStatus)Enum.Parse(typeof(GoodReceive.GoodReceiveStatus), (string)reader["Status"]);
			order.GoodReceive.UpdateOn = (DateTime)reader["ReceiveDate"];
			return order;
		}

		private string SetSearchGoodReceiveStatus(AdvanceSearchField advanceSearchField)
		{
			string searchStatus = "";
			if (advanceSearchField.Waiting || advanceSearchField.Partial || advanceSearchField.Completed)
			{
				searchStatus += "AND Status IN (";
				if (advanceSearchField.Waiting)
				{
					searchStatus += " '" + GoodReceive.GoodReceiveStatus.Waiting.ToString() + "',";
				}
				if (advanceSearchField.Partial)
				{
					searchStatus += " '" + GoodReceive.GoodReceiveStatus.Partial.ToString() + "',";
				}
				if (advanceSearchField.Completed)
				{
					searchStatus += " '" + GoodReceive.GoodReceiveStatus.Completed.ToString() + "',";
				}
				searchStatus = searchStatus.TrimEnd(',');
				searchStatus += ")";
			}
			return searchStatus;
		}

		public IEnumerable<ProductSupplier> GetProductSupplier(AdvanceSearchField advanceSearchField,string companyId)
		{
			List<ProductSupplier> products = new List<ProductSupplier>();
			StringBuilder sqlBuilder = new StringBuilder(1024);
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				sqlBuilder.AppendLine("SELECT ord.OrderID,ord.OrderDate,ord.CompanyID,ord.CostcenterID,ord.OrderStatus,ord.OrderGUID,");
				sqlBuilder.AppendLine("detail.Qty,detail.IncVatPrice,detail.ExcVatPrice,prodcent.pID,prodcent.pTName,prodcent.pEName,");
				sqlBuilder.AppendLine("prodcent.pTUnit,prodcent.pEUnit,prodcent.IsPromotion,prodcent.IsBestDeal,prodcent.IsVat,");
				sqlBuilder.AppendLine("prodcent.SupplierID,prodcent.SupplierName,");
				sqlBuilder.AppendLine("isnull(cost.CostTName,'') as CostTName, isnull(cost.CostEName,'') as CostEName,cost.OracleCode,ApproveOrderDate");
			
				sqlBuilder.AppendLine("FROM TBOrder ord with (nolock)");
				sqlBuilder.AppendLine("inner join TBOrderDetail detail with (nolock) on ord.OrderID = detail.OrderID");
				sqlBuilder.AppendLine("inner join TBCostcenter cost with (nolock) on ord.CompanyID=cost.CompanyID AND ord.CostcenterID=cost.CostcenterID");
				sqlBuilder.AppendLine("inner join TBProductCenter prodcent with (nolock) on detail.pID=prodcent.pID");
				sqlBuilder.AppendLine("WHERE 1=1 AND ord.CompanyID = @CompanyID ");
				//sqlBuilder.AppendLine("WHERE 1=1 AND ord.CompanyID = 'OFMST' ");


				if (advanceSearchField.CheckOrderDate && !string.IsNullOrEmpty(advanceSearchField.OrderDateFrom) && !string.IsNullOrEmpty(advanceSearchField.OrderDateTo))
				{
					DateTime dateFrom = ConvertStringToDateTime(advanceSearchField.OrderDateFrom);
					DateTime dateTo = ConvertStringToDateTime(advanceSearchField.OrderDateTo);
					if (dateFrom != DateTime.MinValue && dateTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND ord.OrderDate BETWEEN @DateFrom AND @DateTo");
						command.Parameters.AddWithValue("@DateFrom", dateFrom.Date);
						command.Parameters.AddWithValue("@DateTo", dateTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckAppDate && !string.IsNullOrEmpty(advanceSearchField.AppDateFrom) && !string.IsNullOrEmpty(advanceSearchField.AppDateTo))
				{
					DateTime appFrom = ConvertStringToDateTime(advanceSearchField.AppDateFrom);
					DateTime appTo = ConvertStringToDateTime(advanceSearchField.AppDateTo);
					if (appFrom != DateTime.MinValue && appTo != DateTime.MinValue)
					{
						sqlBuilder.AppendLine(" AND ApproveOrderDate BETWEEN @AppDateFrom AND @AppDateTo");
						command.Parameters.AddWithValue("@AppDateFrom", appFrom.Date);
						command.Parameters.AddWithValue("@AppDateTo", appTo.Date.AddDays(1));
					}
				}

				if (advanceSearchField.CheckSupplier && advanceSearchField.Supplier.Any())
					{
						sqlBuilder.Append("AND ( 1=2 ");
						int index = 0;
						foreach (var supplier in advanceSearchField.Supplier)
						{
							sqlBuilder.AppendFormat("OR prodcent.SupplierID = @Supplier_{0} ", index);
							command.Parameters.AddWithValue("@Supplier_" + index, supplier);
							++index;
						}
						sqlBuilder.AppendLine(")");
					}



				sqlBuilder.AppendLine("ORDER BY prodcent.pID");

				command.Parameters.AddWithValue("@CompanyID", companyId);
				command.CommandText = sqlBuilder.ToString();

				using (SqlDataReader reader = command.ExecuteReader())
				{
					ProductSupplier product = null;
					while (reader.Read())
					{
						product = new ProductSupplier();
						product.SupplierID = (string)reader["SupplierID"];
						product.SupplierName = (string)reader["SupplierName"];

						product.Order = new Order();
						product.Order.OrderGuid = (string)reader["OrderGUID"];
						product.Order.OrderID = (string)reader["OrderID"];
						product.Order.OrderDate = (DateTime)reader["OrderDate"];
						product.Order.Status = (Order.OrderStatus)Enum.Parse(typeof(Order.OrderStatus), (string)reader["OrderStatus"]);

						product.OrderDetail = new ProductDetail();
						product.OrderDetail.Quantity = (int)reader["Qty"];
						product.OrderDetail.ExcVatPrice = (decimal)reader["ExcVatPrice"];
						product.OrderDetail.IncVatPrice = (decimal)reader["IncVatPrice"];
						product.OrderDetail.Product = new Product();
						product.OrderDetail.Product.Id = (string)reader["pID"];
						product.OrderDetail.Product.ThaiName = (string)reader["pTName"];
						product.OrderDetail.Product.EngName = (string)reader["pEName"];
						product.OrderDetail.Product.Name = new LocalizedString((string)reader["pTName"]);
						product.OrderDetail.Product.Name["en"] = (string)reader["pEName"];
						product.OrderDetail.Product.ThaiUnit = (string)reader["pTUnit"];
						product.OrderDetail.Product.EngUnit = (string)reader["pEUnit"];
						product.OrderDetail.Product.Unit = new LocalizedString((string)reader["pTUnit"]);
						product.OrderDetail.Product.Unit["en"] = (string)reader["pEUnit"];
						product.OrderDetail.Product.IsVat = Mapper.ToClass<bool>((string)reader["IsVat"]);
						product.OrderDetail.Product.IsBestDeal = Mapper.ToClass<bool>((string)reader["IsBestDeal"]);
						product.OrderDetail.Product.IsPromotion = Mapper.ToClass<bool>((string)reader["IsPromotion"]);


						product.Order.CostCenter = new CostCenter();
						product.Order.CostCenter.CostCenterID = (string)reader["CostcenterID"];
						product.Order.CostCenter.CostCenterThaiName = (string)reader["CostTName"];
						product.Order.CostCenter.CostCenterEngName = (string)reader["CostEName"];
						product.Order.CostCenter.CostCenterName = new LocalizedString((string)reader["CostTName"]);
						product.Order.CostCenter.CostCenterName["en"] = (string)reader["CostEName"];
						product.Order.CostCenter.OracleCode = (string)reader["OracleCode"];

						products.Add(product);
					}
				}
			}
			return products;
		}

		public IEnumerable<ProductSupplier> GetSuppliers()
		{
			List<ProductSupplier> suppliers = new List<ProductSupplier>();
			using (SqlCommand command = CreateCommandEnsureConnectionOpen())
			{
				command.CommandText = @"SELECT DISTINCT SupplierID ,SupplierName FROM TBProductCenter";

				using (SqlDataReader reader = command.ExecuteReader())
				{
					while (reader.Read())
					{
						ProductSupplier supplier = new ProductSupplier();
						supplier.SupplierID = (string)reader["SupplierID"];
						supplier.SupplierName = (string)reader["SupplierName"];
						suppliers.Add(supplier);
					}
				}
				return suppliers;
			}
		}

	}
}
