﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class EditProfileUser : MasterPageData
	{
		public string UserGuid { get; set; }
		public string Email { get; set; }

		[Required(ErrorMessage = "CreateUser.ThaiNameEmptry")]
		public string ThaiName { get; set; }
		[Required(ErrorMessage = "CreateUser.EngNameEmptry")]
		public string EngName { get; set; }
		public int PhoneId { get; set; }
		public int MobileId { get; set; }
		public int FaxId { get; set; }
		public string CustId { get; set; }
		public string CurrentCompanyId { get; set; }
		public int SequenceId { get; set; }

		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		[Required(ErrorMessage = "CreateUser.PhoneEmptry")]
		public string Phone { get; set; }

		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		//[Required(ErrorMessage = "CreateUser.MobileEmptry")]
		public string Mobile { get; set; }

		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string PhoneExt { get; set; }

		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string Fax { get; set; }

		public bool IsApprover { get; set; }
		public bool IsRequester { get; set; }
		public bool IsAssistantAdmin { get; set; }
		public bool IsObserve { get; set; }
		public bool IsRootAdmin { get; set; }
		public string DefaultLang { get; set; }
		public User.UserStatus Status { get; set; }
		public IEnumerable<StringForSelectList> ListUserStatus = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Active", Name = new ResourceString("Admin.ListUserStatus.Active")},
			new StringForSelectList { Id="Cancel", Name = new ResourceString("Admin.ListUserStatus.Cancel")}
		};
	}
}