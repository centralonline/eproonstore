﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class TrackOrders : MasterPageData
	{
		public int PageNumber { get; set; }
		public int PageSize
		{
			get
			{
				return 50;
			}
		}
		public int TotalOrdersCount { get; set; }
		public int OrderIndex
		{
			get
			{
				return ((PageNumber - 1) * PageSize) + 1;
			}
		}
		public ListOrder ListOrder { get; set; }
		public JobStatus JobStatus { get; set; }
		public DateFilter? DateFilter { get; set; }
		public MonthFilter? MonthFilter { get; set; }
		public AdvanceSearchField AdvanceSearchField { get; set; }
		public IEnumerable<User> UserRequester { get; set; }
		public IEnumerable<User> UserApprover { get; set; }
		public IEnumerable<CostCenter> Costcenter { get; set; }
		public IEnumerable<CompanyDepartment> CompanyDepartment { get; set; }
		public IEnumerable<ProductSummary> ProductSummary { get; set; }
		public IEnumerable<Department> DeptProduct { get; set; }
		public GoodReceive GoodReceive { get; set; }
	}

	public enum JobStatus
	{
		Open,
		Close
	}

	public enum DateFilter
	{
		Today,
		OneToThreeDays,
		FourToSevenDays,
		SevenDaysLater
	}

	public enum MonthFilter
	{
		SevenDaysLater,
		OneMonthLater,
		ThreeMonthLater,
		SixMonthLater,
		All
	}

	public class AdvanceSearchField
	{
		public AdvanceSearchField()
		{
			Requester = new string[0] { };
			Approver = new string[0] { };
			Costcenter = new string[0] { };
			Department = new string[0] { };
			DeptProduct = new string[0] { };
			Supplier = new string[0] { };
		}

		public string CompanyId { get; set; }
		public string CustId { get; set; }
		public string UserId { get; set; }
		public string OrderId { get; set; }
		public string AppDateFrom { get; set; }
		public string AppDateTo { get; set; }
		public string OrderDateFrom { get; set; }
		public string OrderDateTo { get; set; }
		public string[] Requester { get; set; }
		public string[] Approver { get; set; }
		public string[] Costcenter { get; set; }
		public string MinOrderPrice { get; set; }
		public string MaxOrderPrice { get; set; }
		public string[] Supplier { get; set; }

		//สำหรับ ProductSummary
		public string OrderIdFrom { get; set; }
		public string OrderIdTo { get; set; }
		public string ProductIdFrom { get; set; }
		public string ProductIdTo { get; set; }
		public string[] Department { get; set; }
		public string[] DeptProduct { get; set; }

		//Order Status
		public bool StatusWaiting { get; set; }
		public bool StatusApproved { get; set; }
		public bool StatusPartial { get; set; }
		public bool StatusRevise { get; set; }
		public bool StatusWaitingAdmin { get; set; }
		public bool StatusAdminAllow { get; set; }
		public bool StatusShipped { get; set; }
		public bool StatusDeleted { get; set; }
		public bool StatusExpired { get; set; }
		public bool StatusCompleted { get; set; }

		//Order Status GoodReceive
		public bool Waiting { get; set; }
		public bool Partial { get; set; }
		public bool Completed { get; set; }

		//Check Value
		public bool CheckCompanyId { get; set; }
		public bool CheckCustId { get; set; }
		public bool CheckOrderId { get; set; }
		public bool CheckUserId { get; set; }
		public bool CheckAppDate { get; set; }
		public bool CheckOrderDate { get; set; }
		public bool CheckOrderStatus { get; set; }
		public bool CheckRequester { get; set; }
		public bool CheckApprover { get; set; }
		public bool CheckCostcenter { get; set; }
		public bool CheckOrderPrice { get; set; }
		public bool CheckProductId { get; set; }
		public bool CheckDepartment { get; set; }
		public bool CheckDeptProduct { get; set; }
		public bool CheckSupplier { get; set; }

		//สำหรับดึงรายงานตาม Supplier
		public IEnumerable<ProductSupplier> ListSuppliers { get; set; }
		public IEnumerable<ProductSupplier> ListProductSuppliers { get; set; }
		public string SelectSupplier { get; set; }
		//public string SupplierID { get; set; }
		//public string SupplierName { get; set; }
		public string SupplierAll { get; set; }
		public ProductSupplier ProductSupplier { get; set; }
		//public Dictionary<string ,int> ProductIdSeq { get; set; }

	}

	public class ListOrder
	{
		public IEnumerable<Order> Orders { get; set; }
		public IEnumerable<OrderDetail> ItemOrders { get; set; }
		public int Index { get; set; }
	}

	public class TrackCostCenter : MasterPageData
	{
		public IEnumerable<CostcenterSummaryOrder> ListSummaryOrder { get; set; }
		public ListOrder ListOrder { get; set; }
		public RankFilter RankFilter { get; set; }
		public string CostcenterId { get; set; }
		public int[] Months { get; set; }
		
		//Quarters
		public bool Quarters1 { get; set; }
		public bool Quarters2 { get; set; }
		public bool Quarters3 { get; set; }
		public bool Quarters4 { get; set; }

		public TrackCostCenter()
		{
			Months = new int[0]{};
		}
	}

	public class CostcenterSummaryOrder
	{
		public string CostcenterId { get; set; }
		public LocalizedString CostcenterName { get; set; }
		public decimal SumGrandTotalAmt { get; set; }
		public int CountOrder { get; set; }
	}

	public enum RankFilter
	{
		TopTen,
		TopTwenty,
		TopFifty,
		All
	}

	
}