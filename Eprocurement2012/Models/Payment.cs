﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeMate.Framework.CustomerData;
namespace Eprocurement2012.Models
{
    public class Payment
    {
        public PaymentCode Code { get; set; }
        public PaymentType Type { get; set; }
        public string Name { get; set; }
		public string NameInDB { get; set; }
    }
}