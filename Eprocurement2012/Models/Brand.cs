﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class Brand : MasterPageData
	{
		private const string _imageBrandPath = "/images/logo/{0}.jpg";
		public List<Brand> ItemBrand { get; set; }
		public string RefId { get; set; }
		public int DeptId { get; set; }
		public string DeptName { get; set; }
		public string BrandId { get; set; }
		public string BrandName { get; set; }
		public int CountProduct { get; set; }
		public string BrandImageName { get; set; }
		public string ImageBrandDisplay
		{
			get
			{
				string strPath = String.Format(_imageBrandPath, BrandImageName);
				if (CachedIO.PathExists(strPath))
				{
					return String.Format("<img width='60' height='60' src='{0}' alt='{1}' />", strPath, BrandImageName);
				}
				return BrandName;
			}
		}
		public string ImageBrandDisplayFull
		{
			get
			{
				string strPath = String.Format(_imageBrandPath, BrandImageName);
				if (CachedIO.PathExists(strPath))
				{
					return String.Format("<img src='{0}' alt='{1}' />", strPath, BrandImageName);
				}
				return BrandName;
			}
		}
	}
}