﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class OrderData : MasterPageData
	{
		public Order Order { get; set; }
		public IEnumerable<UserApprover> UserApprover { get; set; }
		public OrderActivity CurrentOrderActivity { get; set; }
		public IEnumerable<OrderActivity> OrderActivity { get; set; }
		public IEnumerable<ProcessLog> ProcessLog { get; set; }
		public IEnumerable<MailTransLog> MailTransLog { get; set; }
		public Budget CurrentBudget { get; set; }
		public LogIn LogIn { get; set; }
		public enum OrderStatusLog
		{
			CreateNewOrder,
			Waiting,
			Approved,
			Partial,
			Revise,
			WaitingAdmin,
			AdminAllow,
			Shipped,
			Deleted,
			Expired,
			Completed
		}

		public string OrderStatusLogThaiName(OrderStatusLog orderStatusLog)
		{
			switch (orderStatusLog)
			{
				case OrderStatusLog.CreateNewOrder:
					return "สร้างใบสั่งซื้อ";
				case OrderStatusLog.Revise:
					return "แก้ไขใบสั่งซื้อ";
				default:
					return "";
			}
		}
		public string OrderStatusLogEngName(OrderStatusLog orderStatusLog)
		{
			switch (orderStatusLog)
			{
				case OrderStatusLog.CreateNewOrder:
					return "CreateOrder";
				case OrderStatusLog.Revise:
					return "EditOrder";
				default:
					return "";
			}
		}
	}
}