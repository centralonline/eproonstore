﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class AdminPageData : EprocurementMasterPageData
	{
		public IEnumerable<News> CompanyNews { get; set; }
		public IEnumerable<News> OFMNews { get; set; }
		public SearchOrder SearchOrder { get; set; }
		public CountOrderProcess ItemOrderProcess { get; set; }
		public TrackOrders TrackOrders { get; set; }
		public bool IsNoLogo { get; set; }
		public int UserNotVerify { get; set; }
		public int CostNotReqLine { get; set; }
	}
}