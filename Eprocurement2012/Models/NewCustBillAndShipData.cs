﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;

namespace Eprocurement2012.Models
{
	public class NewCustMasterData : MasterPageData
	{
		public string CustomerId { get; set; }
		//contact
		[Required(ErrorMessage = "Error.Name")]
		public string ContactName { get; set; }
		[Required(ErrorMessage = "Error.PhoneNumber")]
		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string ContactPhoneNumber { get; set; }
		public string ContactPhoneNumberExtension { get; set; }
		[RegularExpression(@"^\d{2,3}-?\d{3}-?\d{4}$", ErrorMessage = "Error.Mobile")]
		public string ContactMobileNumber { get; set; }
		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string ContactFaxNumber { get; set; }
		public string ContactEmail { get; set; }

		//billing
		public string InvName { get; set; }
		[Required(ErrorMessage = "Error.InvName")]
		public string InvAddr1 { get; set; }
		public string InvAddr2 { get; set; }
		public string InvAddr3 { get; set; }
		public string InvAddr4 { get; set; }

		//Payment
		public string PaymentSelected { get; set; }
		public IEnumerable<Payment> PaymentTypeList { get; set; }
		public decimal DiscountRate { get; set; }

		//shipping
		public int ShipID { get; set; }
		public string ShipContactor { get; set; }
		[Required(ErrorMessage = "Account.ApplyNow.ErrorPhoneNoEmptry")]
		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string ShipPhoneNumber { get; set; }
		public string ShipPhoneNumberExtension { get; set; }
		[Required(ErrorMessage = "Error.ContactName")]
		public string ShipAddress1 { get; set; }
		[Required(ErrorMessage = "Error.ShippingAddress")]
		public string ShipAddress2 { get; set; }
		public string ShipAddress3 { get; set; }
		[Required(ErrorMessage = "Error.Province")]
		[RegularExpression(@"^\d{5}$", ErrorMessage = "Error.NotFormatProvince")]
		public string ShipAddress4 { get; set; }
		public string ProvinceSelected { get; set; }
		public IEnumerable<SelectListItem> ProvinceList { get; set; }
	}
}