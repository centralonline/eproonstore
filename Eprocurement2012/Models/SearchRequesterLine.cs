﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class SearchRequesterLine : MasterPageData
	{
		public string CompanyId { get; set; }
		public string CostcenterId { get; set; }
		public string RequesterUserId { get; set; }
		public string ApproverUserId { get; set; }
		public IEnumerable<SetRequesterLine> SearchRequesterLineData { get; set; }

		//CheckBox
		public bool CheckCompanyId { get; set; }
		public bool CheckCostcenterId { get; set; }
		public bool CheckRequesterUserId { get; set; }
		public bool CheckApproverUserId { get; set; }
	}
}