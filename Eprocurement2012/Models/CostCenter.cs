﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Controllers;
using System.ComponentModel.DataAnnotations;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class CostCenter : MasterPageData
	{
		public string CompanyId { get; set; }
		public bool IsByPassAdmin { get; set; } //ดึงค่า setting ของ company มาใช้ใน admin OFM อย่างเดียว
		public bool IsAutoApprove { get; set; } //ดึงค่า setting ของ company มาใช้ใน admin OFM อย่างเดียว
		public bool IsByPassApprover { get; set; } //ดึงค่า setting ของ company มาใช้ใน admin OFM อย่างเดียว
		public string DepartmentID { get; set; }
		[RegularExpression(@"^[A-Za-z0-9_\-]*$", ErrorMessage = "FormatNotValid")]
		[Required(ErrorMessage = "Admin.EditCostCenter.ErrorCostCenterIDEmptry")]
		public string CostCenterID { get; set; }
		public string CostCenterCustID { get; set; }
		public string CostCenterCustName { get { return CostCenterCustThaiName; } }
		public string CostCenterCustThaiName { get; set; }
		public string CostCenterCustEngName { get; set; }
		[Required(ErrorMessage = "Admin.EditCostCenter.ErrorCostCenterThaiNameEmptry")]
		public string CostCenterThaiName { get; set; }
		public LocalizedString CostCenterName { get; set; }
		[Required(ErrorMessage = "Admin.EditCostCenter.ErrorCostCenterEngNameEmptry")]
		public string CostCenterEngName { get; set; }
		public CostCenterStatus CostStatus { get; set; }
		public CompanyDepartment CostCenterDepartment { get; set; }
		public Shipping CostCenterShipping { get; set; }
		public Invoice CostCenterInvoice { get; set; }
		public Contact CostCenterContact { get; set; }
		public Payment CostCenterPayment { get; set; }
		public IEnumerable<Invoice> ListInvoiceAddress { get; set; }
		public string SelectInvoice { get; set; }
		public IEnumerable<Shipping> ListShipAddress { get; set; }
		public string SelectShipID { get; set; }
		public bool IsDeliCharge { get; set; }
		public bool UseByPassAdmin { get; set; }
		public bool UseAutoApprove { get; set; }
		public bool UseByPassApprover { get; set; }
		public bool IsDefaultDeliCharge { get; set; } //สำหรับ Display หน้า CostCenter

		[RegularExpression(@"^[A-Za-z0-9_\-]*$", ErrorMessage = "FormatNotValid")]
		public string OracleCode { get; set; }

		public string DispalyDeliCharge
		{
			get
			{
				if (IsDefaultDeliCharge)
				{
					return new ResourceString("Company.DefaultDeliCharge");
				}
				return new ResourceString("Company.NoDefaultDeliCharge");
			}
		}

		//เพิ่มมาสำหรับดึงข้อมูล OrderIDFormat
		public Company Company { get; set; }
		public string OrderIDFormat { get; set; }


		public bool IsCompModelThreeLevel { get; set; }
		public IEnumerable<CompanyDepartment> ListCompanyDepartment { get; set; }
		public IEnumerable<StringForSelectList> ListCostCenterStatus = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Active", Name = new ResourceString("Admin.ListCostCenterStatus.Active")},
			new StringForSelectList { Id="Cancel", Name = new ResourceString("Admin.ListCostCenterStatus.Cancel")}
		};
		public enum CostCenterStatus
		{
			Active,
			Cancel
		}
		public string DisplayCostCenterStatus
		{
			get
			{
				if (CostStatus == CostCenterStatus.Active) { return new ResourceString("Admin.Status.Active"); }
				return new ResourceString("Admin.Status.Cancel");
			}
		}
		public string CostCenterDisplayname
		{
			get
			{
				return "[" + CostCenterID + "] " + CostCenterName;
			}

		}
	}

	/*สร้างมาใช้กับ ViewAllCostCenter เท่านั้น*/
	public class ViewAllCostCenterData : MasterPageData
	{
		public string CompanyId { get; set; }
		public bool IsCompModelThreeLevel { get; set; }
		public IEnumerable<CostCenter> CostCenters { get; set; }
	}

}