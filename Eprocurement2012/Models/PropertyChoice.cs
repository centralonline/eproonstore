﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Models.Repositories;

namespace Eprocurement2012.Models
{
	public sealed class PropertyChoice
	{
		private const string _imageProductDefaultSmallPath = "/images/image-product-deafult-small.jpg";
		private const string _imageChoicePath = "/images/property/{0}";
		public string Name { get; set; }
		public string Value { get; set; }
		public string RefId { get; set; }
		public string ChoiceImageName { get; set; }
		public string ChoiceImageUrl
		{
			get
			{
				string strPath = String.Format(_imageChoicePath, ChoiceImageName);
				if (CachedIO.PathExists(strPath))
				{
					return strPath;
				}
				return _imageProductDefaultSmallPath;
			}
		}
	}
}