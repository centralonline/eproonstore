﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class CatalogData:MasterPageData
	{
		public Catalog Catalog { get; set; }
		public IEnumerable<Catalog> ListCatalog { get; set; }
		public Product Product { get; set; }
		public string[] pIDs { get; set; }
		public IEnumerable<Product> ProductSku { get; set; }
	}
}