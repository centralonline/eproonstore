﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class GoodReceive : MasterPageData
	{
		public string CompanyId { get; set; }
		public string orderGuid { get; set; }
		public string OrderId { get; set; }
		public GoodReceiveStatus Status { get; set; }
		public int NumOfAction { get; set; }
		public string UserId { get; set; }
		public string AppUserId { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }
		public string UpdateBy { get; set; }
		public DateTime UpdateOn { get; set; }
		public IEnumerable<GoodReceiveDetail> ItemReceiveDetail { get; set; }
		public IEnumerable<GoodReceiveDetail> ItemReceiveNow { get; set; }
		public OrderActivity CurrentOrderActivity { get; set; }
		public Order Order { get; set; }
		public User Requester { get; set; }
		public UserApprover UserApprover { get; set; }
		public Company Company { get; set; }
		public bool IsReceive { get; set; }
		public string ActionDate { get; set; }

		//check period GoodReceive
		public int DiffGoodReceivePeriod { get; set; }

		public enum GoodReceiveStatus
		{
			Waiting,
			Completed,
			Partial,
			Deleted
		}
	}

	public class GoodReceiveDetail
	{
		public Product Product { get; set; }
		public string OrderId { get; set; }
		public DateTime ActionDate { get; set; }
		public int NumOfAction { get; set; }
		public string PId { get; set; }
		public int OriginalQty { get; set; }
		public int ActionQty { get; set; }
		public int ActionQty_New { get; set; }
		public int CancelQty { get; set; }
		public int RemainQty { get; set; }
		public int CancelQty_New { get; set; }
		public string CreateBy { get; set; }
		public string UpdateBy { get; set; }
		public int OrderQTY { get; set; }
	}
}