﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class InformationRequesterLine
	{
		public string RequesterGuid { get; set; }
		public string RequesterId { get; set; }
		public LocalizedString RequesterName { get; set; }
		public string RequesterThaiName { get; set; }
		public string RequesterEngName { get; set; }
		public string ApproverId { get; set; }
		public LocalizedString ApproverName { get; set; }
		public string ApproverThaiName { get; set; }
		public string ApprovweEngName { get; set; }
		public int ApproverLevel { get; set; }
		public decimal ApproverCreditBudget { get; set; }
		public int Parkday { get; set; }

		//pear เพิ่ม ใช้ในหน้า FinalReview
		public string CostcenterID { get; set; }
		public LocalizedString CostcenterName { get; set; }
		public string CostStatus { get; set; }
		public string CustID { get; set; }
		public string DepartmentID { get; set; }
		public LocalizedString DepartmentName { get; set; }
	}
}