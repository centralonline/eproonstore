﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class Site : MasterPageData
	{
		public IEnumerable<User> ListUserAdmins { get; set; }
		public string AdminSiteUserId { get; set; }
		public LocalizedString AdminSiteName { get; set; }
		public string AdminSiteThaiName { get; set; }
		public string AdminSiteEngName { get; set; }
		public DateTime CloseSiteTime { get; set; }
		public string CompanyID { get; set; }
		public string AdminRemark { get; set; }
		public string AdminRemarkMail { get; set; }
		public enum OpenSite
		{
			Yes,
			No
		}

	}
}