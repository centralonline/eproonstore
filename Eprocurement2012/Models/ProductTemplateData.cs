﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class ProductTemplateData : MasterPageData
	{
		public IEnumerable<ProductTemplate> ProductTemplate { get; set; }
		public IEnumerable<Product> ItemProduct { get; set; }
		public string CurrentTemplateId { get; set; }
	}
}