﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Eprocurement2012.Models
{
	public class LogIn : MasterPageData
	{
		[Required(ErrorMessage = "Shared.LogInCenter.ErrorUserIdEmptry")]
		public string UserId { get; set; }
		[Required(ErrorMessage = "Shared.LogInCenter.ErrorPasswordEmptry")]
		public string Password { get; set; }
		public bool Remember { get; set; }
	}
}