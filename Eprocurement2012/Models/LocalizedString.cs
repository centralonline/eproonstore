﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class LocalizedString
	{
		private const string _defaultLocale = "th";
		private Dictionary<string, string> _data = new Dictionary<string, string>();
		public LocalizedString(string defaultString)
		{
			_data[_defaultLocale] = defaultString;
		}

		public string this[string key]
		{
			get
			{
				return _data.ContainsKey(key) ? _data[key] : null;
			}
			set
			{
				if (!_data.ContainsKey(key))
				{
					_data.Add(key, value);
				}
				else
				{
					_data[key] = value;
				}
			}
		}

		public static implicit operator string(LocalizedString localizedString)
		{
			return localizedString.ToString();
		}

		public override string ToString()
		{
			string value = this[System.Globalization.CultureInfo.CurrentUICulture.TwoLetterISOLanguageName];
			return !String.IsNullOrEmpty(value) ? value : this[_defaultLocale];
		}
	}
}