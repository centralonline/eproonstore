﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class Phone
	{
		public int Id { get; set; }
		public int SequenceId { get; set; }
		public string PhoneNo { get; set; }
		public string Extension { get; set; }
		public Contact.TypeNumber TypeNumber { get; set; }
	}
}