﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class User : MasterPageData
	{
		public string UserGuid { get; set; }
		public string UserId { get; set; }
		public DateTime UserRoleCreateOn { get; set; }
		public DateTime UserLastActive { get; set; }
		public LocalizedString DisplayName { get; set; }
		public string UserThaiName { get; set; }
		public string UserEngName { get; set; }
		public Language UserLanguage { get; set; }
		public UserStatus UserStatusName { get; set; }
		public UserType UserTypeName { get; set; }
		public UserRole UserRoleName { get; set; }
		public LocalizedString DisplayUserRoleName { get; set; }
		public Image ProfileImage { get; set; }
		public DateTime ChangePasswordOn { get; set; }
		public bool IsVerified { get; set; }
		public int RemainDayForceChangedPassword { get; set; }
		public string DisplayFullname { get; set; }
		public string CustId { get; set; }
		public string CurrentCompanyId { get; set; }
		public string CurrentCostCenterId { get; set; }
		public int TelephoneId { get; set; }
		public string TelephoneNumber { get; set; }
		public string ExtTelephoneNumber { get; set; }
		public int MobileId { get; set; }
		public string MobileNumber { get; set; }
		public int FaxId { get; set; }
		public string FaxNumber { get; set; }
		public bool IsLogin { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }
		public DateTime UpdateOn { get; set; }
		public string UpdateBy { get; set; }
		public IEnumerable<UserPermission> UserPermission { get; set; }
		public bool IsMultiCompany { get; set; }
		public bool IsAdmin
		{
			get
			{
				if (UserPermission != null)
				{
					if (UserPermission.Any(m => m.RoleName == UserRole.RootAdmin))
					{
						return true;
					}
				}
				return false;
			}
		}
		public bool IsAssistantAdmin
		{
			get
			{
				if (UserPermission != null)
				{
					if (UserPermission.Any(m => m.RoleName == UserRole.AssistantAdmin))
					{
						return true;
					}
				}
				return false;
			}
		}
		public bool IsApprover
		{
			get
			{
				if (UserPermission != null)
				{
					if (UserPermission.Any(m => m.RoleName == UserRole.Approver))
					{
						return true;
					}
				}
				return false;
			}
		}
		public bool IsRequester
		{
			get
			{
				if (UserPermission != null)
				{
					if (UserPermission.Any(m => m.RoleName == UserRole.Requester))
					{
						return true;
					}
				}
				return false;
			}
		}
		public bool IsObserve
		{
			get
			{
				if (UserPermission != null)
				{
					if (UserPermission.Any(m => m.RoleName == UserRole.Observer))
					{
						return true;
					}
				}
				return false;
			}
		}
		public string IPAddress { get; set; }
		public Contact UserContact { get; set; }
		public Company Company { get; set; }

		public enum UserStatus
		{
			Active,
			Cancel
		}
		public enum Language
		{
			TH,
			EN
		}
		public enum UserType
		{
			Requester,
			Approver,
			ReqAndApp,
			MainAdmin,
			Both,
			AdmAndObse,
			Observer,
		}
		public enum UserRole
		{
			Approver,
			Requester,
			RootAdmin,
			AssistantAdmin,
			Observer,
			OFMAdmin
		}

		public enum UserCompanyDefault
		{
			Yes,
			No
		}

		public string DisplayUserStatus
		{
			get
			{
				if (UserStatusName == UserStatus.Active)
				{
					return new ResourceString("Admin.Status.Active");
				}
				else
				{
					return new ResourceString("Admin.Status.Cancel");
				}
			}
		}

		public string DisplayNameAndEmail
		{
			get
			{
				return DisplayName +" "+ "[" + UserId + "]";
			}
		}

	}
}