﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class Shipping
	{
		public int ShipID { get; set; }
		public string ShipContactor { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string Address3 { get; set; }
		public string Address4 { get; set; }
		public string District { get; set; }
		public string Amphur { get; set; }
		public string Province { get; set; }
		public string ZipCode { get; set; }
		public string Remark { get; set; }
		public int OfflineShipID { get; set; }
		public int ShippingSeqNo { get; set; }
		public int PhoneID { get; set; }
		public string ShipPhoneNo { get; set; }
		public string ShipMobileNo { get; set; }
		public string ShipType { get; set; } // ค่า OFM (จัดส่งโดย OFM) หรือ POS (จัดส่งโดยไปรษณีย์) insert ค่าว่าง ให้ DTS จัดการ
		public string CustId { get; set; }
		public string IsDefault { get; set; }
		public string Extension { get; set; }
		public enum ShippingAddressStatus
		{
			Active,
			Delete
		}

		public string DisplayShipPhoneNo
		{
			get
			{
				if (!string.IsNullOrEmpty(Extension))
				{
					return ShipPhoneNo + " ext : " + Extension;
				}
				return ShipPhoneNo;
			}
		}

		public string DisplayShipAddress
		{
			get{ return "[" + Address1 + "]" + " " + Address2; }
		}
	}

	public class Province
	{
		public string ProvinceId { get; set; }
		public LocalizedString ProvinceName { get; set; }
	}
}