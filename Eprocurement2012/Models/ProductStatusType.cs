﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeMate.Framework;

namespace Eprocurement2012.Models
{
	public enum ProductStatusType
	{
		[EnumValue("normal")]
		Normal,
		[EnumValue("stock")]
		InStock,
		[EnumValue("nonstock")]
		NonStock,
		[EnumValue("outofstock")]
		OutOfStock,
		[EnumValue("new")]
		New,
		[EnumValue("preorder")]
		PreOrder,
		[EnumValue("prepaid")]
		Prepaid,
		[EnumValue("delete")]
		Deleted,
		[EnumValue("limited")]
		Limited,
		[EnumValue("hold")]
		Hold,
		[EnumValue("byorder")]
		ByOrder,
		[EnumValue("no")] //ห้ามมี (data ผิด)
		No,
		[EnumValue("")]//ห้ามมี (data ผิด)
		Wrong
	}
}