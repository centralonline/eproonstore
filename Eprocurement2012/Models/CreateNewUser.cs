﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Drawing;
using System.ComponentModel.DataAnnotations;
namespace Eprocurement2012.Models
{
	public class CreateNewUser : MasterPageData
	{
		[RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "CreateUser.ErrorEmailIncorrectFormat")]
		[Required(ErrorMessage = "CreateUser.EmailEmptry")]
		public string Email { get; set; }
		public bool IsInitialPassword { get; set; }
		public string Password { get; set; }
		public string DefaultCustId { get; set; }
		public string DefaultCompanyId { get; set; }
		public string RePassword { get; set; }
		[Required(ErrorMessage = "CreateUser.ThaiNameEmptry")]
		public string ThaiName { get; set; }
		[Required(ErrorMessage = "CreateUser.EngNameEmptry")]
		public string EngName { get; set; }
		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		[Required(ErrorMessage = "CreateUser.PhoneEmptry")]
		public string Phone { get; set; }
		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string Mobile { get; set; }
		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string PhoneExt { get; set; }
		[RegularExpression(@"^\d*$", ErrorMessage = "CreateUser.ErrorPhoneNotCorrect")]
		public string Fax { get; set; }
		public bool IsApprover { get; set; }
		public bool IsRequester { get; set; }
		public bool IsAssistantAdmin { get; set; }
		public bool IsObserve { get; set; }
		public bool IsThaiLanguage { get; set; }
		public string DefaultLang
		{
			get
			{
				if (IsThaiLanguage) { return "TH"; }
				return "EN";
			}
		}
		public Image ProfileImage { get; set; }
		public bool SetVerifyNow { get; set; }
		public int ContactId { get; set; }
		public int PhoneId { get; set; }
		public int MobileId { get; set; }
		public int FaxId { get; set; }
		public bool IsUserOFM { get; set; }
	}
}