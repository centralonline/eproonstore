﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class ProductTemplate : MasterPageData
	{
		public string TemplateId { get; set; }
		public int TotalProduct { get; set; }

		public string DisplayTemplate 
		{
			get 
			{
				if (TemplateId=="Template1")
				{
					return new ResourceString("ProductTemplate.Template1");
				}
				else if (TemplateId == "Template2")
				{
					return new ResourceString("ProductTemplate.Template2");
				}
				else
				{
					return new ResourceString("ProductTemplate.Template3");
				}
			}
		}
	}
}