﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Drawing;
using Eprocurement2012.Controllers.Helpers;
using System.ComponentModel.DataAnnotations;

namespace Eprocurement2012.Models
{
	public class NewSiteData : MasterPageData
	{
		//ข้อมูลบริษัท
		[Required(ErrorMessage = "NewSiteData.ErrorCustIdEmptry")]
		public string CustId { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorCompanyIdEmptry")]
		public string CompanyId { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorCompanyTNameEmptry")]
		[StringLength(100, ErrorMessage = "NewSiteData.ErrorExcessOnehundredCharacters")]
		public string CompanyTName { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorCompanyENameEmptry")]
		[StringLength(100, ErrorMessage = "NewSiteData.ErrorExcessOnehundredCharacters")]
		public string CompanyEName { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorDiscountRateEmptry")]
		public decimal DiscountRate { get; set; }
		public Invoice InvoiceAddress { get; set; }
		public Shipping ShippingAddress { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorIsSiteActivateEmptry")]
		public string IsSiteActivate { get; set; }
		public ImageData CompanyLogo { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorCompanyModelEmptry")]
		public string CompanyModel { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorOrderControlTypeEmptry")]
		public string OrderControlType { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorBudgetLevelTypeEmptry")]
		public string BudgetLevelType { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorBudgetPeriodTypeEmptry")]
		public string BudgetPeriodType { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorPriceTypeEmptry")]
		public string PriceType { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorUseOfmCatalogEmptry")]
		public string UseOfmCatalog { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorOrderIDFormatEmptry")]
		public Company.OrderIDFormatType OrderIDFormat { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorDefaultLangEmptry")]
		public User.Language DefaultLang { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorDefaultParkDayEmptry")]
		public String SaleRequest { get; set; }
		public String AdminOfmRemark { get; set; }
		public int DefaultParkDay { get; set; }
		public bool IsByPassAdmin { get; set; }
		public bool IsByPassApprover { get; set; }
		public bool IsAutoApprove { get; set; }
		public bool UseCompanyNews { get; set; }
		public bool UseOfmNews { get; set; }
		public bool UseSMSFeature { get; set; }
		public bool DefaultDeliCharge { get; set; }
		public bool AllowAddProduct { get; set; }
		public bool ShowOutofStock { get; set; }
		public bool ShowOfmCat { get; set; }
		public bool ShowContactUs { get; set; }
		public bool ShowSpecialProd { get; set; }
		public bool UseReferenceCode { get; set; }
		public string CreateBy { get; set; }

		//ข้อมูลผู้ดูแลระบบ
		public int SequenceId { get; set; }
		//[Required(ErrorMessage = "NewSiteData.ErrorUserIDEmptry")]
		//[RegularExpression("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.([a-zA-Z]{2,4})$", ErrorMessage = "NewSiteData.ErrorUserIDFalse")]
		public string UserID { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorUserTNameEmptry")]
		[StringLength(50, ErrorMessage = "NewSiteData.ErrorExcessFiftyCharacters")]
		public string UserTName { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorUserENameEmptry")]
		[StringLength(50, ErrorMessage = "NewSiteData.ErrorExcessFiftyCharacters")]
		public string UserEName { get; set; }
		public int PhoneId { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorPhoneNumberEmptry")]
		public string PhoneNumber { get; set; }
		public string ExtentionPhoneNumber { get; set; }
		public int MobileId { get; set; }
		public string MobileNumber { get; set; }
		public int FaxId { get; set; }
		public string FaxNumber { get; set; }
		public bool IsRequester { get; set; }
		public bool IsApprover { get; set; }
		[Required(ErrorMessage = "NewSiteData.ErrorRePasswordPeriodEmptry")]
		public int RePasswordPeriod
		{
			get
			{
				switch (RePasswordText)
				{
					case "XMonth":
						return Convert.ToInt32(RePasswordPeriodFromUser);
					case "1Month":
						return 30;
					case "2Month":
						return 60;
					default:
						return 90;
				}
			}
		}

		//ใช้แสดงข้อมูลหน้า view
		public bool IsMember { get; set; }
		public HttpPostedFileBase FileCompanyLogo { get; set; }
		public string RePasswordText { get; set; }
		public string RePasswordPeriodFromUser { get; set; }
		public IEnumerable<Province> ListProvince { get; set; }
		public IEnumerable<StringForSelectList> SiteActivate = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Yes", Name = new ResourceString("NewSiteData.SiteActivate.Yes")},
			new StringForSelectList { Id="No", Name = new ResourceString("NewSiteData.SiteActivate.No")}
		};
		public IEnumerable<StringForSelectList> ListOrderControlType = new List<StringForSelectList>
		{
			new StringForSelectList { Id="ByOrder", Name = new ResourceString("NewSiteData.OrderControlType.ByOrder")},
			new StringForSelectList { Id="ByBudgetAndOrder", Name = new ResourceString("NewSiteData.OrderControlType.ByBudgetAndOrder")}
		};
		public IEnumerable<StringForSelectList> ListBudgetLevelType = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Company", Name = new ResourceString("NewSiteData.ListBudgetLevelType.Company")},
			new StringForSelectList { Id="Department", Name = new ResourceString("NewSiteData.ListBudgetLevelType.Department")},
			new StringForSelectList { Id="Costcenter", Name = new ResourceString("NewSiteData.ListBudgetLevelType.Costcenter")}
		};
		public IEnumerable<StringForSelectList> ListBudgetPeriodType = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Year", Name = new ResourceString("NewSiteData.ListBudgetPeriodType.Year")},
			new StringForSelectList { Id="Half", Name = new ResourceString("NewSiteData.ListBudgetPeriodType.Half")},
			new StringForSelectList { Id="Quarter", Name = new ResourceString("NewSiteData.ListBudgetPeriodType.Quarter")},
			new StringForSelectList { Id="Month", Name = new ResourceString("NewSiteData.ListBudgetPeriodType.Month")}
		};
		public IEnumerable<StringForSelectList> ListPriceType = new List<StringForSelectList>
		{
			new StringForSelectList { Id="Float", Name = new ResourceString("NewSiteData.ListPriceType.Float")},
			new StringForSelectList { Id="Fix", Name = new ResourceString("NewSiteData.ListPriceType.Fix")}
		};
		public IEnumerable<StringForSelectList> ListParkDay = new List<StringForSelectList>
		{
			new StringForSelectList { Id="3", Name = new ResourceString("NewSiteData.ListParkDay.3")},
			new StringForSelectList { Id="5", Name = new ResourceString("NewSiteData.ListParkDay.5")},
			new StringForSelectList { Id="7", Name = new ResourceString("NewSiteData.ListParkDay.7")}
		};

		//แสดงหน้า view ส่งเมล์สำหรับสร้างไซต์ใหม่
		public string DisplayBudgetPeriodType
		{
			get
			{
				if (BudgetPeriodType == "Year")
				{
					return new ResourceString("Mail.BudgetPeriodType.Year");
				}
				else if (BudgetPeriodType == "Haft")
				{
					return new ResourceString("Mail.BudgetPeriodType.Haft");
				}
				else if (BudgetPeriodType == "Quarter")
				{
					return new ResourceString("Mail.BudgetPeriodType.Quarter");
				}
				else
				{
					return new ResourceString("Mail.BudgetPeriodType.Month");
				}
			}
		}

		public string DisplayBudgetLevelType
		{
			get
			{
				if (BudgetLevelType == "Company")
				{
					return new ResourceString("Mail.BudgetLevelType.Company");
				}
				else if (BudgetLevelType == "Department")
				{
					return new ResourceString("Mail.BudgetLevelType.Department");
				}
				else
				{
					return new ResourceString("Mail.BudgetLevelType.CostCenter");
				}
			}
		}

		public string DisplayUseOfmCatalog
		{ 
			get 
			{
				if (UseOfmCatalog == "Yes")
				{
					return new ResourceString("Mail.UseOfmCatalog.Yes");
				}
				else
				{
					return new ResourceString("Mail.UseOfmCatalog.No");
				}
			} 
		}

		public string DisplayOrderControlType
		{
			get 
			{
				if (OrderControlType == "ByBudgetAndOrder")
				{
					return new ResourceString("Mail.OrderControlType.ByBudgetAndOrder");
				}
				else
				{
					return new ResourceString("Mail.OrderControlType.ByOrder");
				}
			} 
		}

		public string DisplayCompanyModel
		{ 
			get 
			{
				if (CompanyModel == "3Level")
				{
					return new ResourceString("Mail.CompanyModel.3Level");
				}
				else
				{
					return new ResourceString("Mail.CompanyModel.2Level");
				}
			} 
		}

		public string DisplayPriceType
		{ 
			get 
			{
				if (PriceType == "Float")
				{
					return new ResourceString("Mail.PriceType.Float");
				}
				else
				{
					return new ResourceString("Mail.PriceType.Fix");
				}
			} 
		}

	}
}
