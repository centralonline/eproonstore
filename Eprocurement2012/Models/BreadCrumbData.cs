﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class BreadCrumbData
	{
		public bool IsBreadCrumbUp { get; set; }
		public IEnumerable<Department> BreadCrumbs { get; set; }
	}
}