﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Eprocurement2012.Models
{
	public class Invoice
	{
		public string CustId { get; set; }
		public string Address1 { get; set; }
		public string Address2 { get; set; }
		public string Address3 { get; set; }
		public string Address4 { get; set; }

		public string DisplayInvoiceAddress
		{
			get
			{
				return CustId + " " + Address1;
			}
		}

	}
}