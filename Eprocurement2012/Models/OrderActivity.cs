﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace Eprocurement2012.Models
{
	public class OrderActivity
	{
		public int LogID { get; set; }
		public string OrderId { get; set; }
		public LocalizedString ActivityName { get; set; }
		public string ActivityTName { get; set; }
		public string ActivityEName { get; set; }
		public string IPAddress { get; set; }
		public string Remark { get; set; }
		public string CreateBy { get; set; }
		public DateTime CreateOn { get; set; }
		public string UpdateBy { get; set; }
		public DateTime UpdateOn { get; set; }
	}

	public class ProcessLog
	{
		public int LogID { get; set; }
		public string OrderId { get; set; }
		public string ProcessRemark { get; set; }
		public string Remark { get; set; }
		public string IPAddress { get; set; }
		public string CreateBy { get; set; }
		public DateTime CreateOn { get; set; }
		public string CompanyId { get; set; }
	}

	public class MailTransLog
	{
		public int LogID { get; set; }
		public string Guid { get; set; }
		public string OrderId { get; set; }
		public string EmailFrom { get; set; }
		public string EmailTo { get; set; }
		public string EmailCC { get; set; }
		public string EmailBCC { get; set; }
		public string Subject { get; set; }
		public string MailContent { get; set; }
		public MailType MailCategory { get; set; }
		public string SendMailSuccess { get; set; }
		public string ErrorDesc { get; set; }
		public string CreateBy { get; set; }
		public DateTime CreateOn { get; set; }
	}

	public enum MailType
	{
		NotifyProduct,
		VerifyUser,
		ForgotPassword,
		ApplyNow,
		CreateOrder,
		RequestAdminAllowBudget,
		RequestAdminAllowProduct,
		RequesterSentReviseOrder,
		ApproveOrder,
		ApproveAndForwardOrder,
		ApproverDeleteOrder,
		ApproverSentReviseOrder,
		AdminAllowBudgetToRequester,
		AdminAllowBudgetToApprover,
		RequestSpecialProduct,
		ContactUs,
		CreateNewSite,
		GoodReceive
	}


	public class ConfirmHandelOrder
	{
		public string InputActionName { get; set; }
		public string OrderGuid { get; set; }
		[Required(ErrorMessage = "Shared.LogInCenter.ErrorPasswordEmptry")]
		public string Password { get; set; }
		public string Remark { get; set; }
	}
}