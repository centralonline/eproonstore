﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;

namespace Eprocurement2012.Models
{
	public class DepartmentListData : DepartmentViewData
	{
		public IEnumerable<Department> Departments { get; set; }
		public IEnumerable<Product> ProductRecommendRelateDept { get; set; }
		public IEnumerable<Product> ProductBestSellerRelateDept { get; set; }
		public DataTable BrandLoGo { get; set; }
	}
}