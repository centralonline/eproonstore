﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class RequestSpecialProduct : MasterPageData
	{
		public string CompanyId { get; set; }
		public string UserId { get; set; }
		public string UserName { get; set; }
		public string ProductName { get; set; }
		public string Remark { get; set; }
		public string Period { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }
	}
}