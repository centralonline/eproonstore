﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class SearchResultData : MasterPageData
	{
		public IEnumerable<Product> SearchResult { get; set; }
		public IDictionary<int, Department> DefaultDepartmentMap { get; set; }
		public string TimeStamp { get; set; }
		public string SearchType { get; set; }
	}
}