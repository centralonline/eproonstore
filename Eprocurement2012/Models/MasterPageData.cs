﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Eprocurement2012.Models
{
	public class MasterPageData : IMasterPageData
	{
		public User User { get; set; }
		public string RedirectUrl { get; set; }
		public string CurrentUrl { get; set; }
		public string BackToShoppingUrl { get; set; }
		public bool IsSecureConnection { get; set; }
		public bool IsProduction { get; set; }
		public int ItemCount { get; set; }
		public int ItemCountQty { get; set; }
		protected string GetSetting(string settingKey)
		{
			return WebConfigurationManager.AppSettings[settingKey];
		}
		//protected virtual bool IsCalculateIncVat
		//{
		//    get { return Boolean.Parse(GetSetting("IsCalculateIncVat")); }
		//}
	}

	public class MasterPageData<T> : MasterPageData
	{
		public MasterPageData(T value)
		{
			Value = value;
		}
		public T Value { get; set; }

		public override string ToString()
		{
			return Value.ToString();
		}
	}

}