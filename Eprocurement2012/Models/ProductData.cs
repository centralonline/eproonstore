﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Eprocurement2012.Models
{
	public class ProductData : MasterPageData
	{
		public Product ProductDetail { get; set; }
		public IEnumerable<Product> ProductSku { get; set; }
		public List<Product> ItemProduct { get; set; }
		//public IEnumerable<SelectListItem> MyCatalogs { get; set; }
		public BreadCrumbData BreadCrumb { get; set; }
		public IEnumerable<Product> ProductRelated { get; set; }
		public IEnumerable<Product> ProductHistory { get; set; }
		public IEnumerable<SelectListItem> MyCatalogs { get; set; }
	}
}