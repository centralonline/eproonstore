﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012.Models
{
	public class ApplyNow : MasterPageData
	{
		[Required(ErrorMessage = "Account.ApplyNow.ErrorCompanyNameEmptry")]
		public string CompanyName { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorBusinessTypeEmptry")]
		public string BusinessType { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorCustEmpNumEmptry")]
		public string CustEmpNum { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorNumOfCompanyEmptry")]
		public string NumOfCompany { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorNameEmptry")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorPositionEmptry")]
		public string Position { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorPhoneNoEmptry")]
		public string PhoneNo { get; set; }
		
		[Required(ErrorMessage = "Account.ApplyNow.ErrorMobileNoEmptry")]
		public string MobileNo { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorEmailEmptry")]
		public string Email { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorIsCustomerEmptry")]
		public string IsCustomer { get; set; }
		public string CustID { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorInterestProgramEmptry")]
		public string InterestProgram { get; set; }

		[Required(ErrorMessage = "Account.ApplyNow.ErrorUseOnlineEmptry")]
		public string UseOnline { get; set; }
		public DateTime CreateOn { get; set; }
		public string CreateBy { get; set; }

		[Required]
		public string Captcha { get; set; }
		
		public IEnumerable<BusinessType> ListBusinessType { get; set; }
		public string IsCustomerDisplay
		{
			get
			{
				if (IsCustomer == "Yes")
				{
					if (!string.IsNullOrEmpty(CustID))
					{
						return String.Format("ApplyNow.CustomerCustID",CustID);
					}
					return new ResourceString("ApplyNow.Customer");
				}
				else { return new ResourceString("ApplyNow.NeverService"); }
			}
		}
	}



	public class BusinessType
	{
		public string id { get; set; }
		public string Name { get; set; }
	}
}