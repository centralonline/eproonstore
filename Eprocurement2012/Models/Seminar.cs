﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class Seminar
	{
		public string CustType { get; set; }
		public string CompName { get; set; }
		public string CustID { get; set; }
		public string InvAddr1 { get; set; }
		public string InvAddr2 { get; set; }
		public string InvAddr3 { get; set; }
		public string InvAddr4 { get; set; }
		public string PhoneNo { get; set; }
		public string FaxNo { get; set; }
		public string Website { get; set; }
		public string ContactName1 { get; set; }
		public string ContactPosition1 { get; set; }
		public string ContactPhone1 { get; set; }
		public string ContactMobile1 { get; set; }
		public string ContactMail1 { get; set; }
		public string ContactName2 { get; set; }
		public string ContactPosition2 { get; set; }
		public string ContactPhone2 { get; set; }
		public string ContactMobile2 { get; set; }
		public string ContactMail2 { get; set; }
		public string NewsChannel { get; set; }
		public string NewsChannelOther { get; set; }
		public string ERPSystem { get; set; }
		public bool IsCustOFM { get; set; }
		public bool HaveSystem { get; set; }

		public string NewsChannelToDB {
			get
			{
					if (!string.IsNullOrEmpty(NewsChannel))
					{
						switch (NewsChannel)
						{
							case "1":
								return "จดหมายอิเล็คทรอนิคส์เชิญจากออฟฟิศเมท";	
							case "2":
								return "เว็บไซต์ www.OfficeMate.co.th";	
							case "3":
								return "เจ้าหน้าที่ฝ่ายขาย บริษัท ออฟฟิศเมท";	
							case "4":
								return NewsChannelOther;
							case "5":
								return "www.officemate.co.th";
							case "6":
								return "หนังสือพิมพ์";
							default:
								return "";		
						}
					}
					return "";	
			}
		}

		public string CheckIsCustomerOFM 
		{
			get 
			{
				if (IsCustOFM) { return "checked=checked"; }
					else{ return "";}
			}
		}
		public string CheckNoneCustomerOFM
		{
			get
			{
				if (!IsCustOFM) { return "checked=checked"; }
				else { return ""; }
			}
		}
		public string CheckIsEmailChanel
		{
			get 
			{
				if (!string.IsNullOrEmpty(NewsChannel) && NewsChannel.Equals("1")) { return "checked=checked"; }
					else{ return "";}
			}
		}
		public string CheckIsRadioChanel
		{
			get 
			{
				if (!string.IsNullOrEmpty(NewsChannel) &&  NewsChannel.Equals("2")) { return "checked=checked"; }
					else{ return "";}
			}
		}
		public string CheckIsOFMChanel
		{
			get 
			{
				if (!string.IsNullOrEmpty(NewsChannel) &&  NewsChannel.Equals("3")) { return "checked=checked"; }
					else{ return "";}
			}
		}
		public string CheckIsWebOFMChanel
		{
			get
			{
				if (!string.IsNullOrEmpty(NewsChannel) && NewsChannel.Equals("5")) { return "checked=checked"; }
				else { return ""; }
			}
		}
		public string CheckIsNewsPaperChanel
		{
			get
			{
				if (!string.IsNullOrEmpty(NewsChannel) && NewsChannel.Equals("6")) { return "checked=checked"; }
				else { return ""; }
			}
		}
		public string CheckIsOtherChanel
		{
			get 
			{
				if (!string.IsNullOrEmpty(NewsChannel) &&  NewsChannel.Equals("4")) { return "checked=checked"; }
					else{ return "";}
			}
		}
		public string CheckIsSystem
		{
			get
			{
				if (HaveSystem) { return "checked=checked"; }
				else { return ""; }
			}
		}
		public string CheckIsNoneSystem
		{
			get
			{
				if (!HaveSystem) { return "checked=checked"; }
				else { return ""; }
			}
		}

		public string CheckIsPersonal
		{
			get
			{
				if (!string.IsNullOrEmpty(CustType) &&  CustType.Equals("Personal")) { return "checked=checked"; }
				else { return ""; }
			}
		}
		public string CheckIsCorporate
		{
			get
			{
				if (!string.IsNullOrEmpty(CustType) && CustType.Equals("Corporate")) { return "checked=checked"; }
				else { return ""; }
			}
		}

		public DateTime CreateOn { get; set; }

		public string BusinessType { get; set; }
		public IEnumerable<BusinessType> ListBusinessType { get; set; }
		//public int CustEmpNum { get; set; }
		//public decimal Capital { get; set; }

		public string CustEmpNum { get; set; }
		public string Capital { get; set; }

	}
}