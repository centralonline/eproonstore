﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class SearchMail: MasterPageData
	{
		public string EmailTo { get; set; }
		public string EmailCC { get; set; }
		public string OrderId { get; set; }
		public DateTime DateFrom { get; set; }
		public DateTime DateTo { get; set; }
		public string MailSuccess { get; set; }
		public IEnumerable<MailTransLog> MailTransLog { get; set; }
		public MailTransLog MailLog { get; set; }

		//Check Box
		public bool CheckEmailTo { get; set; }
		public bool CheckEmailCC { get; set; }
		public bool CheckOrderId { get; set; }
		public bool CheckMailCategory { get; set; }
		public bool CheckDate { get; set; }
		public bool CheckMailSuccess { get; set; }

		//Mail Type
		public bool TypeNotifyProduct { get; set; }
		public bool TypeVerifyUser { get; set; }
		public bool TypeForgotPassword { get; set; }
		public bool TypeApplyNow { get; set; }
		public bool TypeCreateOrder { get; set; }
		public bool TypeRequestAdminAllowBudget { get; set; }
		public bool TypeRequestAdminAllowProduct { get; set; }
		public bool TypeRequesterSentReviseOrder { get; set; }
		public bool TypeApproveOrder { get; set; }
		public bool TypeApproveAndForwardOrder { get; set; }
		public bool TypeApproverDeleteOrder { get; set; }
		public bool TypeApproverSentReviseOrder { get; set; }
		public bool TypeAdminAllowBudgetToRequester { get; set; }
		public bool TypeAdminAllowBudgetToApprover { get; set; }
		public bool TypeRequestSpecialProduct { get; set; }
		public bool TypeContactUs { get; set; }
		public bool TypeCreateNewSite { get; set; }
		public bool TypeGoodReceive { get; set; }

	}
}