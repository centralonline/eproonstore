﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using OfficeMate.Framework;

namespace Eprocurement2012.Models
{
	public enum DepartmentDisplayType
	{
		[EnumValue("dept")]
		Department,
		[EnumValue("product")]
		Product,
		[EnumValue("static")]
		Static
	}

	public enum DepartmentViewType
	{
		[EnumValue("")]
		None,
		[EnumValue("list")]
		List,
		[EnumValue("thumbnail")]
		Thumbnail
	}

	public enum DepartmentSortByType
	{
		[EnumValue("new")]
		New,
		[EnumValue("pricelowtohigh")]
		PriceLowToHigh,
		[EnumValue("pricehightolow")]
		PriceHighToLow,
		[EnumValue("promotion")]
		Promotion,
		[EnumValue("brand")]
		Brand,
		[EnumValue("webrecommend")]
		Officemate
	}

	public enum DepartmentThemeType
	{
		[EnumValue("themea")]
		ThemeA,
		[EnumValue("themeanonbrand")]
		ThemeANonBrand,
		[EnumValue("themeb")]
		ThemeB,
		[EnumValue("themec")]
		ThemeC,
		[EnumValue("themed")]
		ThemeD,
		[EnumValue("officesupply")]
		OfficeSupply,
		[EnumValue("inktoner")]
		InkToner,
		[EnumValue("printing")]
		Printing,
		[EnumValue("bookpreview")]
		BookPreview,
		[EnumValue("football")]
		Football,
		[EnumValue("groupproperty")]
		GroupProperty,
		[EnumValue("grouppropertyofficesupply")]
		GroupPropertyOfficeSupply,
		[EnumValue("listview")]
		ListView
	}
}