﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class DepartmentViewData : MasterPageData
	{
		public string pRefId { get; set; }
		public string bRefId { get; set; }
		public string cRefId { get; set; }

		public string Title { get; set; }
		public Department CurrentDepartment { get; set; }
		public BreadCrumbData BreadCrumb { get; set; }
		public Ancestor NavigatorDepartment { get; set; }
		public IEnumerable<Brand> BrandFilter { get; set; }
		public DepartmentViewType ProductListViewType { get; set; }
		public IEnumerable<PropertyDept> PropertyDepartment { get; set; }
		public IEnumerable<Department> SiblingDepartment { get; set; }
		public IEnumerable<Department> RelateDepartment { get; set; }
		public IEnumerable<Product> ProductPromotion { get; set; }
		public IEnumerable<Product> ProductHistory { get; set; }
		public string Paths { get; set; }
		public int PromotionCount { get; set; }
	}
}