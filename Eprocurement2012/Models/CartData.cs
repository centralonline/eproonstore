﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class CartData : MasterPageData
	{
		public Cart Cart { get; set; }
		public string ApproverRemark { get; set; }
		public string OFMRemark { get; set; }
		public string ReferenceRemark { get; set; }
		//public string ReferenceDocument { get; set; }

		public string CallBackRequestStatus { get; set; }
		public bool IsCallBackRequestStatus
		{
			get
			{
				if (CallBackRequestStatus == null) { return false; }
				if (CallBackRequestStatus.Equals("Yes")) { return true; }
				return false;
			}
		}
		public string AutoApprove { get; set; }
		public bool IsAutoApprove
		{
			get
			{
				if (AutoApprove == null) { return false; }
				if (AutoApprove.Equals("Yes")) { return true; }
				return false;
			}
		}

		public bool IsNotSetBudget { get; set; } //กรณีที่ยังไม่ได้กำหนด Budget ตั้งต้น
		public string AdminAllowFlag { get; set; }
		public string costcenterId { get; set; }
		public string approverId { get; set; }
		public bool IsOfmContact { get; set; }
		public IEnumerable<CostCenter> ItemCostcenter { get; set; }
		public IEnumerable<UserApprover> ListUserApprover { get; set; }
		public CostCenter CurrentUsedCostCenter { get; set; }
		public UserApprover CurrentSelectApprover { get; set; }
		public Budget CurrentBudget { get; set; }
		public bool IsGoodReceivePeriod { get; set; }
		public bool? UseSMSFeature { get; set; }// ส่ง SMS การสั่งซื้อสินค้า
	}
}