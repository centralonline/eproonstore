﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class NotifyMeMailData
	{
		public Product ProductDetail { get; set; }
		public string NotifyRemark { get; set; }
		public string NotifyEmail { get; set; }
	}
}