﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Eprocurement2012.Models
{
	public class HomePageData : EprocurementMasterPageData
	{
		public IEnumerable<Product> ProductHistory { get; set; }
		public User RootAdmin { get; set; }
		public IEnumerable<User> UserAdmin { get; set; }
		public IEnumerable<News> CompanyNews { get; set; }
		public IEnumerable<News> OFMNews { get; set; }
		public SearchOrder SearchOrder { get; set; }
		public CountOrderProcess ItemOrderProcess { get; set; }
		public IEnumerable<Order> ItemOrderAllStatus { get; set; }
		public bool IsGoodReceivePeriod { get; set; }
	}
}