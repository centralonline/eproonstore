﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Configuration;

namespace Eprocurement2012.Models
{
	public class OrderDetail : MasterPageData
	{
		public Product Product { get; set; }
		public int Seq { get; set; }
		public decimal DiscountRate { get; set; }
		public decimal DiscAmt { get; set; }
		public int Quantity { get; set; }
		public int OldQuantity { get; set; }
		public bool IsOfmCatalog { get; set; }
		public string OrderId { get; set; }
		//public decimal ItemPriceDiscount { get; set; }
		public decimal ItemPriceNet {
			get 
			{
				//if (IsCalculateIncVat) 
				//{
				//    return (Product.PriceIncVat * Quantity) - DiscAmt;
				//}
				return (Product.PriceExcVat * Quantity) - DiscAmt;
			} 
		}

		//แสดงราคาสินค้าที่ลบส่วนลดแล้ว แต่ยังไม่ได้คูณ Quantity
		public decimal PriceDiscount
		{
			get
			{
				if (Product.IsBestDeal || Product.IsPromotion)
				{
					return 0;
				}
				else
				{
					return (Math.Round((Product.PriceExcVat * DiscountRate) / 100, 2, MidpointRounding.AwayFromZero));
				}
			}
		}

		public decimal ItemIncVatPrice
		{
			get
			{
				if (Product.IsVat)
				{
					return (Math.Round((ItemExcVatPrice * Convert.ToDecimal(1.07)), 2, MidpointRounding.AwayFromZero));
				}
				else
				{
					return Product.PriceExcVat - PriceDiscount;
				}
			}
		}

		public decimal ItemExcVatPrice
		{
			get
			{
				return Product.PriceExcVat - PriceDiscount;
			}
		}
	}
}