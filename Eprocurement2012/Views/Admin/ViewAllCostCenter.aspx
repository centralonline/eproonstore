﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.CostCenter>>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "CostCenterGuide", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "UserGuide", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.CostCenter.ContentInfo")%></h2>
	</div>
	<div class="grid_16">
		<%int count = 0; %>
		<p class="btn" style="float: right">
			<%=Html.ActionLink(Html._("Admin.CostCenter.Add"), "CreateCostCenter", "Admin")%></p>
		<span style="color: #FF3300">
			<%=TempData["deleteError"]%></span>
		<%if (Model.Value.Any())
		{%>
		<div class="block" id="tables">
			<table>
				<tr>
					<th>
						<%=Html._("Admin.CompanyInfo.No")%>
					</th>
					<%if (Model.User.Company.IsCompModelThreeLevel)
					{%>
					<th>
						<%=Html._("Admin.CostCenter.DepartmentID")%>
					</th>
					<%} %>
					<th>
						<%=Html._("Admin.CostCenter.CostCenterID")%>
					</th>
					<th>
						<%=Html._("Admin.CostCenter.ThaiName")%>
					</th>
					<th>
						<%=Html._("Admin.CostCenter.EngName")%>
					</th>
					<th>
						<%=Html._("Admin.CostCenter.Status")%>
					</th>
					<th>
						<%=Html._("Admin.CostCenter.Modify")%>
					</th>
					<th>
						<%=Html._("Admin.Department.Cancel")%>
					</th>
					<th>
						<%=Html._("Admin.CompanySetting.HeadUser")%>
					</th>
				</tr>
				<%foreach (var item in Model.Value)%>
				<%{%>
				<tr id="color">
					<td>
						<%=++count%>
					</td>
					<%if (Model.User.Company.IsCompModelThreeLevel)
					{%>
					<td>
						<%=item.CostCenterDepartment.DepartmentID%>
					</td>
					<%} %>
					<td>
						<%=item.CostCenterID%>
					</td>
					<td>
						<%=item.CostCenterThaiName%>
					</td>
					<td>
						<%=item.CostCenterEngName%>
					</td>
					<td>
						<%=item.DisplayCostCenterStatus%>
					</td>
					<td>
						<%=Html.ActionLink(Html._("Admin.CostCenter.Edit"), "EditCostCenter", "Admin", new { costCenterID = item.CostCenterID }, null)%>
					</td>
					<td>
						<%if (item.CostStatus == Eprocurement2012.Models.CostCenter.CostCenterStatus.Active)
						{%>
						<%=Html.ActionLink("Cancel", "RemoveCostCenter", "Admin", new { costCenterId = item.CostCenterID, companyId = Model.Value.First().CompanyId }, null)%>
						<%} %>
					</td>
					<td>
						<%=Html.ActionLink(Html._("Admin.CompanySetting.User"), "SetRequesterLine", "Admin", new { companyId = item.CompanyId, costcenterId = item.CostCenterID }, null)%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<%} %>
	</div>
	<div style="min-height: 550px;">
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
