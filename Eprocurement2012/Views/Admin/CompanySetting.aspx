﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<br />
		<div style="text-align: center;">
			<img src="/images/step03.png" alt="" /></div>
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "CompanyLogo", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "CompanyInfoReview", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.CompanySetting.User")%></h2>
	</div>
	<div class="grid_2">
		<p style="font-size: 11px; color: #98accb">
			<%=Html._("Admin.CompanySetting.ContentImage")%></p>
	</div>
	<div class="grid_12">
		<%Html.BeginForm("CompanySetting", "Admin", FormMethod.Post);%>
		<%=Html.HiddenFor(m=>m.CompanyId)%>
		<p class="block">
			<%=Html._("Admin.CompanySetting.RePassword")%></p>
		<div>
			<ul class="select-radio-inline">
				<li>
					<%=Html.RadioButtonFor(m => m.RePasswordText, "3Month", new { id = "3Month" })%><label
						for="3Month"><%=Html._("Admin.CompanySetting.3Month")%></label></li>
				<li>
					<%=Html.RadioButtonFor(m => m.RePasswordText, "2Month", new { id = "2Month" })%><label
						for="2Month"><%=Html._("Admin.CompanySetting.2Month")%></label></li>
				<li>
					<%=Html.RadioButtonFor(m => m.RePasswordText, "1Month", new { id = "1Month" })%><label
						for="1Month"><%=Html._("Admin.CompanySetting.1Month")%></label></li>
				<li>
					<%=Html.RadioButtonFor(m => m.RePasswordText, "XMonth", new { id = "XMonth" })%><label
						for="XMonth"><%=Html._("Admin.CompanySetting.XMonth")%></label>&nbsp;<%=Html.TextBoxFor(m => m.RePasswordPeriodFromUser, new { size=1})%>&nbsp;<%=Html._("Admin.CompanySetting.Day")%></li>
			</ul>
		</div>
		<p class="block">
			<%=Html._("Admin.CompanySetting.DefaultLang")%></p>
		<ul class="select-radio-inline">
			<li>
				<%=Html.RadioButtonFor(m => m.DefaultLang, "TH", new { id = "THLang"})%><label for="THLang"><%=Html._("Admin.CompanySetting.THLang")%></label></li>
			<li>
				<%=Html.RadioButtonFor(m => m.DefaultLang, "EN", new { id = "ENlang"})%><label for="ENlang"><%=Html._("Admin.CompanySetting.ENLang")%></label></li>
		</ul>
		<div style="margin-top: 25px">
			<h5>
				<%=Html._("Admin.CompanySetting.Order")%></h5>
		</div>
		<div>
			<fieldset>
				<legend>
					<%=Html._("Admin.CompanySetting.Agreement")%></legend><span>
						<%=Html._("Admin.CompanySetting.CompanyModel")%>
						:
						<%=Model.IsCompModelThreeLevel ? Html._("Admin.CompanySetting.3Level") : Html._("Admin.CompanySetting.2Level")%></span><br />
				<span>
					<%=Html._("Admin.CompanySetting.Control")%>
					:
					<%=Model.DisplayControlTypeName%></span><br />
				<span>
					<%=Html._("Admin.CompanySetting.BudgetLevel")%>
					:
					<%=Model.DisplayBudgetLevelType%></span><br />
				<span>
					<%=Html._("Admin.CompanySetting.BudgetPeriod")%>
					:
					<%=Model.DisplayBudgetPeriodType%></span><br />
				<span>
					<%=Html._("Admin.CompanySetting.OrderIDFormat")%>
					:
					<%=Model.DisplayOrderIDFormat%>&nbsp;&nbsp;<%=Html._("Admin.CompanySetting.Example")%></span><br />
				<span>
					<%=Html._("Admin.CompanySetting.CompanyCatalog")%>
					:
					<%=Model.DisplayUseCompanyCatalog%></span><br />
				<span>
					<%=Html._("Admin.CompanySetting.PriceType")%>
					:
					<%=Model.DisplayPriceType%></span>
			</fieldset>
		</div>
		<div style="margin-top: 25px">
			<p>
				<%=Html._("Admin.CompanySetting.ParkDay")%>
				<%=Html.DropDownListFor(m => m.DefaultParkDay, new SelectList(Model.ListParkDay,"Id","Name"))%><%=Html._("Admin.CompanySetting.Day")%>&nbsp;&nbsp;<span><%=Html._("Admin.CompanySetting.ContentParkDay")%></span></p>
		</div>
		<div>
			<%=Html.CheckBoxFor(m => m.IsByPassApprover, new { id = "IsByPassApprover"})%>
			<label for="IsByPassApprover">
				<%=Html._("Admin.CompanySetting.IsByPassApprover")%></label>&nbsp;&nbsp;
			<img alt="" src="../../images/icon/icon_tooltip_help.gif" />
			&nbsp;<a href="/Help/AdminInstruction" target="_blank">Learn more</a><br />
			<%--<%=Html.CheckBoxFor(m => m %> <label for="">อนุญาติให้ผู้ใช้งาน ร้องขอรายการสินค้านอกเหนือจากแคตตาล็อกองค์กรของคุณ</label><br />--%>
			<%=Html.CheckBoxFor(m => m.IsByPassAdmin, new { id = "IsByPassAdmin"})%>
			<label for="IsByPassAdmin" style="padding-right: 200">
				<%=Html._("Admin.CompanySetting.IsByPassAdmin")%></label>&nbsp;&nbsp;
			<img alt="" src="../../images/icon/icon_tooltip_help.gif" />&nbsp;<a href="/Help/AdminInstruction"
				target="_blank">Learn more</a><br />
			<%=Html.CheckBoxFor(m => m.IsAutoApprove, new { id = "IsAutoApprove"})%>
			<label for="IsAutoApprove" style="padding-right: 200">
				<%=Html._("Admin.CompanySetting.IsAutoApprove")%></label>
			<img alt="" src="../../images/icon/icon_tooltip_help.gif" />&nbsp;<a href="/Help/AdminInstruction"
				target="_blank">Learn more</a><br />
			<%=Html.CheckBoxFor(m => m.UseCompanyNews, new { id = "UseCompanyNews"})%>
			<label for="UseCompanyNews">
				<%=Html._("Admin.CompanySetting.UseCompanyNews")%></label><br />
			<%=Html.CheckBoxFor(m => m.UseOfmNews, new { id = "UseOfmNews"})%><label for="UseOfmNews">
			<%=Html._("Admin.CompanySetting.UseOfmNews")%></label><br />
			<%=Html.CheckBoxFor(m => m.UseSMSFeature, new { id = "UseSMSFeature"})%><label for="UseSMSFeature">
			<%=Html._("Admin.CompanySetting.UseSMSFeature")%></label><br />
			<%if (!Model.User.Company.UseOfmCatalog)
	 { %>
			<%=Html.CheckBoxFor(m => m.ShowOFMCat, new { id = "ShowOFMCat" })%><label for="ShowOFMCat">
			<%=Html._("Admin.CompanySetting.ShowOFMCat")%></label><br />
			<%} %>
			<%=Html.CheckBoxFor(m => m.ShowContactUs, new { id = "ShowContactUs" })%><label for="ShowContactUs">
			<%=Html._("Admin.CompanySetting.ShowContactUs")%></label><br />
			<%=Html.CheckBoxFor(m => m.ShowSpecialProd, new { id = "ShowSpecialProd" })%>
			<label for="ShowSpecialProd">
			<%=Html._("Admin.CompanySetting.ShowSpecialProd")%></label>&nbsp;&nbsp;
			<img alt="" src="../../images/icon/icon_tooltip_help.gif" />&nbsp;<a href="/Help/AdminInstruction"
				target="_blank">Learn more</a><br />
		</div>
		<br />
		<input type="submit" value="<%=Html._("Button.Save") %>" class="graybutton" />
		<span style="color: Red;">
			<%=TempData["CompanySettingComplete"]%></span>
		<%Html.EndForm();%>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
