﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Admin.CompanyHome.Title")%></h2>
	</div>
	<div class="grid_8">
		<div class="block">
			<p>
			<%=Html._("Admin.CompanyHome.Content")%>
			</p>
		</div>
		<img src="/images/Company_info/Company_sec01.jpg" alt="you are here" /><br />
	</div>
	<div class="grid_4">
		<div class="block">
			<p>
				<%=Html._("Admin.CompanyHome.SettingHere")%>
			</p>
			<p class="btn">
				<%=Html.ActionLink("Continue to Company Info", "CompanyGuide", "Admin")%></p>
		</div>
	</div>
	<br />
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
