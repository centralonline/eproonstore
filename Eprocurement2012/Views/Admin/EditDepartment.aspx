﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CompanyDepartment>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "ViewAllDepartment", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "CostCenterGuide", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.Department.ContentModify")%></h2>
	</div>
	<div class="grid_16">
		<div class="grid_8">
			<p class="btn" style="float: right">
				<%=Html.ActionLink(Html._("Admin.Department.Add"), "CreateDepartment", "Admin")%>
				<%=Html.ActionLink(Html._("Admin.Department.ViewData"), "ViewAllDepartment", "Admin")%></p>
			<h5>
				<%=Html._("Admin.Department.InfoModify")%></h5>
			<div class="box" style="margin-top: 15px">
				<%Html.BeginForm("EditDepartment", "Admin", FormMethod.Post);%>
				<div class="table-format">
					<%=Html.HiddenFor(m=>m.CompanyId)%>
					<label>
						<%=Html._("Admin.Department.DeptID")%></label>
					<%=Model.DepartmentID%>
					<%=Html.HiddenFor(m=>m.DepartmentID)%>
					<span style="color: Red">
						<%=Html.LocalizedValidationMessageFor(m => m.DepartmentID)%></span>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.Department.ThaiName")%></label>
					<%=Html.TextBoxFor(m=>m.DepartmentThaiName)%>
					<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m=>m.DepartmentThaiName)%></span>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.Department.EngName")%></label>
					<%=Html.TextBoxFor(m=>m.DepartmentEngName)%>
					<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m=>m.DepartmentEngName)%></span>
				</div>
				<%if (Model.DepartmentStatus == Eprocurement2012.Models.CompanyDepartment.DeptStatus.Cancel)
				{ %>
				<div class="table-format">
					<label>
						<%=Html._("Admin.Department.StatusModify")%></label>
					<%=Html.DropDownListFor(m => m.DepartmentStatus, new SelectList(Model.ListDepartmentStatus, "Id", "Name"))%>
				</div>
				<%}
				else
				{%>
				<%=Html.HiddenFor(m=>m.DepartmentStatus) %>
				<%} %>
				<div style="padding-left: 176px; margin-bottom: 5px">
					<input id="Save" type="submit" value="<%=Html._("Button.Save")%>" />
				</div>
				<br />
				<br />
				<%Html.EndForm();%>
			</div>
		</div>
	</div>
	<div style="min-height: 500px;">
	</div>
</asp:Content>
