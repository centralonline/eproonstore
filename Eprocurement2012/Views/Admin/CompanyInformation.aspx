﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<br />
		<div style="text-align: center;">
			<img src="/images/step01.png" alt="" /></div>
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "CompanyGuide", "Admin",null, new { @style = "width:75px;"})%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "CompanyLogo", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.CompanyInformation")%></h2>
	</div>
	<div class="grid_16">
		<%Html.BeginForm("CompanyInformation", "Admin", FormMethod.Post);%>
		<div class="grid_16">
			<br />
			<p>
				<%=Html._("Admin.CompanyInfo.Content")%>
			</p>
		</div>
		<br class="clear" />
		<div class="grid_8">
			<div class="table-format">
				<label>
					<%=Html._("Admin.CompanyInfo.CompanyId")%>
				</label>
				<%=Model.CompanyId%>
				<%=Html.Hidden("CompanyId", Model.CompanyId)%>
			</div>
			<div class="table-format">
				<label>
					<%=Html._("Admin.CompanyInfo.CompanyThaiName")%>
				</label>
				<%=Html.TextBoxFor(m => m.CompanyThaiName, new { size=40})%>
				<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CompanyThaiName)%></span>
				<%=Html._("Admin.CompanyInfo.ShowWeb")%>
			</div>
			<div class="table-format">
				<label>
					<%=Html._("Admin.CompanyInfo.CompanyEngName")%>
				</label>
				<%=Html.TextBoxFor(m => m.CompanyEngName, new { size = 40 })%>
				<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CompanyEngName)%></span>
			</div>
			<div class="table-format">
				<label>
					<%=Html._("Admin.CompanyInfo.DefaultCustId")%>
				</label>
				<%=Model.DefaultCustId%>
				<span>
					<%=Html._("Admin.CompanyInfo.SetOFM")%></span>
			</div>
			<br />
			<input type="submit" id="UpdateInfo" value="<%=Html._("Button.Save") %>" />
			<span style="color: Red;">
				<%=TempData["CompanyInfoComplete"]%></span>
		</div>
		<br class="clear" />
		<div class="grid_16" style="padding-top: 15px">
			<h5>
				<%=Html._("Admin.CompanyInfo.Detail")%></h5>
			<table>
				<tr>
					<th>
						<%=Html._("Admin.CompanyInfo.CustId")%>
					</th>
					<th>
						<%=Html._("Admin.CompanyInfo.InvoiceAddress")%>
					</th>
					<th>
						<%=Html._("Admin.CompanyInfo.ShippingAddress")%>
					</th>
					<th>
						<%=Html._("Admin.CompanyInfo.Data")%>
					</th>
				</tr>
				<%foreach (var item in Model.ListCompanyAddress)%>
				<%{%>
				<tr>
					<td>
						<%=item.CustId%>
					</td>
					<td>
						<%=item.InvoiceAddress1%>
					</td>
					<td>
						<%=item.CountShip%>
						<span>
							<%=Html._("Admin.CompanyInfo.ShippingAddress")%></span>
					</td>
					<td>
						<%=Html.ActionLink(Html._("Admin.CompanyInfo.Verify"), "ViewShippingInformation", "Admin", new { custId = item.CustId }, null)%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<div class="grid_16" style="padding-top: 15px">
			<%if (Model.CompanyInvoice != null && Model.ListShipAddress != null)
	 { %>
			<%Html.RenderPartial("ListShipAddress", Model); %>
			<%} %>
			<%Html.EndForm();%>
		</div>
	</div>
	<div style="min-height: 500px;">
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
