﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ProductCatalog>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("SetProductCatalogRequest")%></h2>
		<%Html.BeginForm("SetProductCatalogRequest", "Admin", FormMethod.Post); %>
		<%Html.RenderPartial("ProductCatalogPartial", Model.ItemProductCatalog); %><br />
		<%Html.EndForm(); %>
		<br />
		<br />
		<%Html.BeginForm("AddProductCatalog", "Admin", FormMethod.Post); %>
		<div class="content">
			<div class="left">
				<fieldset>
					<legend>
						<%=Html._("SetProductCatalogRequest.SelectType")%></legend>
					<%=Html.RadioButtonFor(o => o.IsByCompany, true, new { id = "IsByCompany", name="IsByCompany", @checked = "checked" })%>
					<label>
						<%=Html._("SetProductCatalogRequest.SelectAllCompany")%></label>
					<%=Html.RadioButtonFor(o => o.IsByCompany, false, new { id = "IsByRequester", name = "IsByCompany" })%>
					<label>
						<%=Html._("SetProductCatalogRequest.SelectByRequester")%></label>
				</fieldset>
				<input id="chkDuringDate" type="checkbox" /><%=Html._("SetProductCatalogRequest.SelectExpireDate")%>
				<div id="divExpireDate">
					<p>
						<label>
							<%=Html._("SetProductCatalogRequest.StartDate")%></label>
						<%=Html.TextBoxFor(o => o.StartDate, new { id = "StartDate", name = "StartDate", maxlength = 10 })%>
					</p>
					<p>
						<label>
							<%=Html._("SetProductCatalogRequest.EndDate")%></label>
						<%=Html.TextBoxFor(o => o.EndDate, new { id = "EndDate", name = "EndDate", maxlength = 10 })%>
					</p>
				</div>
				<br />
				<input id="setReqProduct" name="setReqProduct" type="submit" value="<%=Html._("Button.Save")%>" />
				<%=TempData["ErrorSelectUser"]%>
			</div>
			<div class="right" id="divListUser">
				<%if (Model.UserRequester != null)
				{ %>
				<fieldset>
					<legend>
						<%=Html._("SetProductCatalogRequest.ListRequester")%></legend>
					<table>
						<thead>
							<tr>
								<th>
									<%=Html._("SetProductCatalogRequest.HeadSelectUserId")%>
								</th>
								<th>
									<%=Html._("SetProductCatalogRequest.HeadUserId")%>
								</th>
								<th>
									<%=Html._("SetProductCatalogRequest.HeadUserName")%>
								</th>
							</tr>
						</thead>
						<tbody>
							<%foreach (var item in Model.UserRequester)
							{ %>
							<tr>
								<td>
									<input id="select_<%=item.UserId %>" name="listuserId" class="classlistuserId" type="checkbox"
										value="<%=item.UserId %>" />
								</td>
								<td>
									<%=item.UserId %>
								</td>
								<td>
									<%=item.DisplayName%>
								</td>
							</tr>
							<%} %>
						</tbody>
					</table>
				</fieldset>
				<%} %>
			</div>
		</div>
		<%Html.EndForm(); %>
	</div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript" language="javascript">
		$(function () {
			$("#divExpireDate").hide();
			$("#divListUser").hide();
			$(".classlistuserId").attr("disabled", "disabled ");

			if ($('#chkDuringDate:checked').is(':checked')) { $("#divExpireDate").show(); }

			if ($('#IsByCompany').is(':checked')) {
				$(".classlistuserId").attr("disabled", "disabled ");
				$('.classlistuserId').prop('checked', false);
				$("#divListUser").hide();
			}

			if ($('#IsByRequester').is(':checked')) {
				$(".classlistuserId").removeAttr("disabled");
				$("#divListUser").show();
			}

			$("#chkDuringDate").click(function () {
				var isChecked = $('#chkDuringDate:checked').is(':checked');
				if (isChecked) { $("#divExpireDate").show(); }
				else { $("#divExpireDate").hide(); }
			});
			$("#IsByRequester").click(function () {
				var isChecked = $('#IsByRequester').is(':checked');
				if (isChecked) {
					$(".classlistuserId").removeAttr("disabled");
					$("#divListUser").show();
				}
			});
			$("#IsByCompany").click(function () {
				var isChecked = $('#IsByCompany').is(':checked');
				if (isChecked) {
					$(".classlistuserId").attr("disabled", "disabled ");
					$('.classlistuserId').prop('checked', false);
					$("#divListUser").hide();
				}
			});

			$("#StartDate").datepicker({ showOn: "button", buttonImage: "/images/icon/icon_my_catalog.gif", buttonImageOnly: true });
			$("#EndDate").datepicker({ showOn: "button", buttonImage: "/images/icon/icon_my_catalog.gif", buttonImageOnly: true });
			$("#StartDate").datepicker("option", "dateFormat", "dd-mm-yy");
			$("#EndDate").datepicker("option", "dateFormat", "dd-mm-yy");
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.content
		{
			overflow: auto;
			width: 100%;
		}
		.left
		{
			float: left;
			width: 30%;
		}
		.right
		{
			float: right;
			width: 70%;
		}
	</style>
</asp:Content>
