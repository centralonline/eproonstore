﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.News>>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Admin.NewsCompany")%></h2>
		<p class="btn">
			<%=Html.ActionLink(Html._("Admin.NewsCompany.Add"), "CreateNewsCompany", "Admin")%></p>
		<div class="spaceTop">
		</div>
		<%if (Model.Value.Any())
	{%>
		<div>
			<%int index = 0; %>
			<table>
				<tr>
					<th>
						<%=Html._("Admin.NewsCompany.Sequence")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.NewsTitle")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.Period")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.NewsStatus")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.Edit")%>
					</th>
				</tr>
				<%foreach (var item in Model.Value)%>
				<%{%>
				<tr>
					<td>
						<%=++index%>
					</td>
					<td>
						<%=Html.ActionLink(item.NewsTitle, "NewsCompanyDetail", "Admin", new { newsGuid = item.NewsGUID }, null)%>
					</td>
					<td>
						<%=item.ValidFrom.ToString(new DateFormat()) + " " + Html._("Admin.NewsCompany.To") + " " + item.ValidTo.ToString(new DateFormat())%>
					</td>
					<td>
						<%=item.DisplayNewsStatus %>
					</td>
					<td>
						<%=Html.ActionLink("Edit", "ManageNews", "Admin", new { newsGuid = item.NewsGUID }, null)%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<%} %>
	</div>
	<div style="min-height: 500px;">
	</div>
</asp:Content>
