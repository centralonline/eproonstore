﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SetRequesterLine>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<%Html.BeginForm("SetRequesterLine", "Admin", FormMethod.Post); %>
		<h2 id="page-heading">
			<%=Html._("Admin.SetRequesterLine")%></h2>
		<fieldset>
			<legend>
				<%=Html._("Admin.CostCenterInfo")%></legend>
			<p>
				<%=Html._("SetRequesterLine.CoscenterId")%>
				:
				<%=Model.CostCenter.CostCenterID %>
				<input id="costcenterId" type="hidden" name="costcenterId" value="<%=Model.CostCenter.CostCenterID %>" />
			</p>
			<p>
				<%=Html._("SetRequesterLine.CoscenterName")%>
				:
				<%=Model.CostCenter.CostCenterName %></p>
		</fieldset>
		<div class="content">
			<div style="float: left; width: 25%">
				<label for="tags">
					Requester:
				</label>
				<input type="text" id="tags" style="width: 270px;" />
				<input type="button" id="AddRequester" name="AddRequester" value="Add" /><br /><br />
				<span style="color:#C4484B;">กรุณาระบุขั้นต่ำ 3 ตัวอักษร เพื่อให้ระบบทำงาน</span>

				<div id="DisplayRequester">
					<%if (TempData["userRequester"] != null)
	   {%>
					<%foreach (var userId in (IEnumerable<string[,]>)TempData["userRequester"])
	   {%>
					<input id="UserRequester_<%=userId[0,1]%>" name="userRequester" type="checkbox" value="<%=userId[0,1]%>"
						checked="checked" />
					<label for="UserRequester_<%=userId[0,1] %>">
						<%=userId[0,0]%>
						[<%=userId[0,1]%>]</label><br />
					<input id="userName_<%=userId[0,1]%>" type="hidden" name="userName" value="<%=userId[0,0]%>" />
					<%} %>
					<%} %>
				</div>
			</div>
			<%--			<%if (Model.UserRequester != null)
	 { %>
			<div style="float: left; width: 25%">
				<h3>
					<%=Html._("SetRequesterLine.Requester")%></h3>
				<hr />
				<%foreach (var item in Model.UserRequester)
	  {
		  string reqCheck = "";
		  string reqEnable = "";
		  string reqClass = "style='color: #0000FF'";
		  if (Model.UserCurrentPermission.Any(o => o.UserId == item.UserId && Model.CostCenter.CostCenterID == o.CostCenter.CostCenterID && o.RoleName == Eprocurement2012.Models.User.UserRole.Requester))
		  {
			  reqCheck = "checked='checked'";
			  reqEnable = "disabled='disabled'";
			  reqClass = "";
		  }
		  if ((Model.SelectRequester ?? new string[] { }).Any(o => item.UserId == o))
		  {
			  reqCheck = "checked='checked'";
		  }%>
				<input id="UserRequester_<%=item.UserGuid %>" name="userRequester" type="checkbox"
					<%=reqEnable %> value="<%=item.UserId%>" <%=reqCheck%> />
				<label for="UserRequester_<%=item.UserGuid %>" <%=reqClass%>>
					<%=item.DisplayNameAndEmail%></label>
				<br />
				<%} %>
			</div>
			<%}%>--%>
			<% if (Model.UserApprover != null)
	  { %>
			<div style="float: right; width: 75%">
				<h3>
					<%=Html._("SetRequesterLine.Approver")%></h3>
				<hr />
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel1")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[0] != null && Model.SelectApprover[0] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[0] == null ? 0 : Model.SelectBudget[0]%>"
						id="UserApprover_First_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[0] != null && Model.SelectParkday[0] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<%=Html._("Admin.SetRequsterLine.Day")%>
				</div>
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel2")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[1] != null && Model.SelectApprover[1] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[1] == null ? 0 : Model.SelectBudget[1]%>"
						id="UserApprover_Second_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[1] != null && Model.SelectParkday[1] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<span>
						<%=Html._("Admin.SetRequsterLine.Day")%></span>
				</div>
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel3")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[2] != null && Model.SelectApprover[2] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[2] == null ? 0 : Model.SelectBudget[2]%>"
						id="UserApprover_Third_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[2] != null && Model.SelectParkday[2] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<%=Html._("Admin.SetRequsterLine.Day")%>
				</div>
				<%} %>
				<input id="SetRequesterLine" type="submit" value="<%=Html._("Button.Save")%>" />
				<span style="color: Red">
					<%=TempData["Error"]%></span>
			</div>
		</div>
		<% Html.EndForm(); %>
		<br />
		<%if (Model.InformationRequesterLine.Any())
	{%>
		<h3>
			<%=Html._("SetRequesterLine.RoleRequesterAndApprovals")%></h3>
		<hr />
		<%foreach (var item in Model.InformationRequesterLine.GroupBy(o => o.RequesterId))
	{ %>
		<div style="width: 70%; border-width: thin; background-color: #EEEEEE; border-style: ridge">
			<table>
				<thead>
					<tr>
						<th style="width: 20%">
							<%=Html._("SetRequesterLine.Requester")%>
						</th>
						<th style="width: 50%">
							<%=Html._("SetRequesterLine.Approver")%>
						</th>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>
							<div>
								<% Html.BeginForm("ResetRequesterLine", "Admin", FormMethod.Post); %>
								<%=item.First().RequesterName%><br />
								<%=item.Key %><br />
								<input type="hidden" name="costcenterId" value="<%=Model.CostCenter.CostCenterID %>" />
								<input type="hidden" name="userGuid" value="<%=item.First().RequesterGuid%>" />
								<input type="submit" name="resetApprover.x" value="<%=Html._("SetRequesterLine.ResetApproval")%>" /><br />
								<%--<span><%=Html._("Content.ResetApprover")%></span>--%>
								<% Html.EndForm(); %>
							</div>
						</td>
						<td>
							<div>
								<table>
									<%foreach (var itemapp in item.OrderBy(o => o.ApproverLevel))
		   { %>
									<tr>
										<td>
											<strong>
												<%=Html._("SetRequesterLine.Approver")%><%=Html._("SetRequesterLine.ApproverLevel" + itemapp.ApproverLevel)%>:</strong>
											<%=itemapp.ApproverName%><br />
											<%=itemapp.ApproverId %>
										</td>
										<td>
											<strong>
												<%=Html._("SetRequesterLine.ApprovalBudget")%>:</strong>
											<%=itemapp.ApproverCreditBudget.ToString(new MoneyFormat())%>
											<%=Html._("Order.ConfirmOrder.Baht")%>
										</td>
										<td>
											<strong>
												<%=Html._("SetRequesterLine.ApprovalPeriod")%>:</strong>
											<%=itemapp.Parkday%>
											<%=Html._("Admin.CompanySetting.Day")%>
										</td>
									</tr>
									<%} %>
								</table>
							</div>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<br />
		<%} %>
		<%} %>
	</div>
	<%--	<div class="grid_16">
		<label for="tags">
			Requester:
		</label>
		<input type="text" id="tags" style="width: 270px;" />
		<input type="button" id="AddRequester" name="AddRequester" value="Add" />
		<div id="DisplayReqester">
		</div>
	</div>--%>
	<div class="grid_8">
		<p class="btn">
			<%=Html.ActionLink("Back", "ViewAllCostCenter", "Admin")%></p>
		<br />
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			var checknum = function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
								 event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
								(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			};

			$("#UserApprover_First_Budget").keydown(checknum);
			$("#UserApprover_Second_Budget").keydown(checknum);
			$("#UserApprover_Third_Budget").keydown(checknum);


			var companyId = '<%=Model.CostCenter.CompanyId%>';
			var costcenterId = '<%=Model.CostCenter.CostCenterID %>';

			$("#tags").autocomplete({
				source: function (request, response) {
					$.ajax({
						url: '<%=Url.Action("AutoCompleteSetRequesterLine","Admin")%>',
						cache: false, //ดึงข้อมุลใหม่ตลอด ไม่เก็บใน cache
						type: "GET",
						data: ({ companyId: companyId, rows: 20, term: request.term }),
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 3
			}).autocomplete("widget").addClass("highlight"); ;


			$("#AddRequester").click(function () {
				var str = $("#tags").val();
				var n = str.indexOf("[");
				var m = str.indexOf("]");
				var userId = str.substring(n + 1, m);
				var username = str.substring(0, n);

				if (userId == "") {
					userId = str;
				}

				var txt1 = $("input[class='chk'][value='" + userId + "']").val();

				if (userId != txt1) {
					var userIdInsystem = HasUserIdInSystem(companyId, userId);
					var result = HasRequesterLine(companyId, costcenterId, userId);

					if (userIdInsystem) {//มีข้อมุล UserId ในระบบ
						if (result) {
							alert("มีข้อมูลในระบบแล้วค่ะ");
							$("#tags").val("");
						}
						else {
							var html = $('<input/>').attr({ type: 'checkbox', value: userId, checked: 'checked', class: "chk", name: "userRequester", id: "UserRequester_" + userId });
							var userNamehtml = $('<input/>').attr({ type: 'hidden', value: username, name: 'userName', id: 'userName_' + userId });
							$('#DisplayRequester').append(html, str, '<br />', userNamehtml);
							$("#tags").val("");
						}
					}
					else {
						alert("ไม่มีข้อมูลในระบบค่ะ");
						$("#tags").val("");
					}
				}
			});

			$(".chk").live("click", function () {
				var result = $(this).attr("id");
				var n = result.indexOf("_");
				var email = result.substring(n + 1);
				var username = "userName_" + email;

				if ($(this).attr("checked")) {
					$("input[id='" + username + "']").attr("disabled", false);
				}
				else {
					$("input[id='" + username + "']").attr("disabled", true);
				}
			});

		});


		function HasRequesterLine(companyId, costcenterId, userId) {
			var isHasRequesterLine = false;
			$.ajax({
				url: '<%=Url.Action("CheckHasRequesterLine","Admin")%>',
				cache: false, //ดึงข้อมุลใหม่ตลอด ไม่เก็บใน cache
				type: "GET",
				async: false, //ทำงานตามลำดับขั้นตอน
				data: ({ companyId: companyId, costcenterId: costcenterId, reqUserId: userId }),
				success: function (data) {
					isHasRequesterLine = data;
				}
			});
			return isHasRequesterLine;
		};

		function HasUserIdInSystem(companyId, userId) {
			var isHasUserIdInSystem = false;
			$.ajax({
				url: '<%=Url.Action("CheckHasUserIdInSystem","Admin")%>',
				cache: false, //ดึงข้อมุลใหม่ตลอด ไม่เก็บใน cache
				type: "GET",
				async: false, //ทำงานตามลำดับขั้นตอน
				data: ({ companyId: companyId, userId: userId }),
				success: function (data) {
					isHasUserIdInSystem = data;
				}
			});
			return isHasUserIdInSystem;
		};

	</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.highlight
		{
			background: #e9e9e9;
		}
	</style>
</asp:Content>
