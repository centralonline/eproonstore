﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CreateNewUser>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".number").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
						event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
						(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});
		});
	</script>
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Admin.CreateNewUser")%></h2>
		<%Html.BeginForm("CreateNewUser", "Admin", FormMethod.Post); %>
		<div class="left">
			<label class="addWidth">
				<%=Html._("CreateUser.UserId")%>:</label>
		</div>
		<div class="right">
			<%=Html.TextBoxFor(o => o.Email, new { id = "Email", maxlength = 100 })%><span style="color: red;">*</span>
			<br />
			<span style="color: red;">
				<%=Html.LocalizedValidationMessageFor(model => model.Email)%></span>
			<label>
				<%=Html._("CreateUser.DetailUserId")%></label>
		</div>
		<div class="left">
			<p>
			</p>
		</div>
		<div class="right">
			<%=Html.CheckBoxFor(o=>o.IsInitialPassword,new {id ="InitialPassword"}) %>
			<label for="InitialPassword">
				<%=Html._("CreateUser.InitializePassword")%></label>
		</div>
		<div class="right">
			<%=Html.PasswordFor(o => o.Password, new { id = "Password", maxlength = 60 })%>
			<label>
				<%=Html._("CreateUser.DetailPassword")%></label>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.Password")%>:</label>
		</div>
		<div class="right">
			<%=Html.PasswordFor(o => o.RePassword, new { id = "RePassword", maxlength = 100 })%><br />
			<span style="color: red;">
				<%=Html.LocalizedValidationMessageFor(model => model.Password)%></span>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.RePassword")%>:</label>
		</div>
		<div class="right">
			<%=Html.TextBoxFor(o => o.ThaiName, new { id = "ThaiName", maxlength = 50 })%><span
				style="color: red;">*</span><br />
			<span style="color: red;">
				<%=Html.LocalizedValidationMessageFor(model => model.ThaiName)%></span>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.ThaiName")%>:</label>
		</div>
		<div class="right">
			<%=Html.TextBoxFor(o => o.EngName, new { id = "EngName", maxlength = 50 })%><span
				style="color: red;">*</span><br />
			<span style="color: red;">
				<%=Html.LocalizedValidationMessageFor(model => model.EngName)%></span>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.EngName")%>:</label>
		</div>
		<div class="right">
			<%=Html.TextBoxFor(o => o.Phone, new { id = "Phone", maxlength = 10, @class = "number" })%><span
				style="color: red;">*</span>
			<label>
				<%=Html._("CreateUser.PhoneExt")%></label>
			<%=Html.TextBoxFor(o => o.PhoneExt, new { id = "PhoneExt", maxlength = 5, @class = "number" })%><br />
			<span style="color: red;">
				<%=Html.LocalizedValidationMessageFor(model => model.Phone)%>
				<%=Html.LocalizedValidationMessageFor(model => model.PhoneExt)%>
			</span>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.Phone")%>:</label>
		</div>
		<div class="right">
			<%=Html.TextBoxFor(o => o.Mobile, new { id = "Mobile", maxlength = 10, @class = "number" })%><%--<span style="color: red;">*</span><br />--%>
			<%--<span style="color: red;">
				<%=Html.LocalizedValidationMessageFor(model => model.Mobile)%>
			</span>--%>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.Mobile")%>:</label>
		</div>
		<div class="right">
			<%=Html.TextBoxFor(o => o.Fax, new { id = "Fax", maxlength = 10, @class = "number" })%><br />
			<span style="color: red;">
				<%=Html.LocalizedValidationMessageFor(model => model.Fax)%>
			</span>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.Fax")%>:</label>
		</div>
		<div class="right">
			<%=Html.CheckBoxFor(o => o.IsRequester, new { id = "IsRequester" })%>
			<label for="IsRequester">
				<%=Html._("CreateUser.RoleRequester")%></label>
			<%=Html.CheckBoxFor(o => o.IsApprover, new { id = "IsApprover" })%>
			<label for="IsApprover">
				<%=Html._("CreateUser.RoleApprover")%></label>
			<%=Html.CheckBoxFor(o => o.IsAssistantAdmin, new { id = "IsAssistantAdmin" })%>
			<label for="IsAssistantAdmin">
				<%=Html._("CreateUser.RoleAssistantAdmin")%></label>
			<%=Html.CheckBoxFor(o => o.IsObserve, new { id = "IsObserve" })%>
			<label for="IsObserve">
				<%=Html._("CreateUser.RoleObserve")%></label><br />
			<span style="color: red;">
				<%=Html.LocalizedValidationMessageFor(model => model.IsApprover)%></span>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.Role")%>:</label>
		</div>
		<div class="right">
			<%=Html.RadioButtonFor(o => o.IsThaiLanguage, true, new { id = "IsThaiLanguage", @checked = "checked" })%>
			<label for="IsThaiLanguage">
				<%=Html._("CreateUser.ThaiLanguage")%></label>
			<%=Html.RadioButtonFor(o => o.IsThaiLanguage, false, new { id = "IsEngLanguage" })%>
			<label for="IsEngLanguage">
				<%=Html._("CreateUser.EngLanguage")%></label>
		</div>
		<div class="left">
			<label>
				<%=Html._("CreateUser.Language")%>:</label>
		</div>
		<%--<div class="right">
			<%=Html.CheckBoxFor(o => o.SetVerifyNow, new { id = "SetVerifyNow" })%>
			<label for="SetVerifyNow">
				<%=Html._("CreateUser.VerifyNow")%>
			</label>
		</div>--%>
		<%if (TempData["ErrorOFMSystem"] != null)
	{%>
		<div class="right" style="margin-top: 20px;">
			<%=Html.CheckBoxFor(o => o.IsUserOFM, new { id = "IsUserOFM" })%>
			<span style="color: Red">
				<%=TempData["ErrorOFMSystem"]%></span>
		</div>
		<%} %>
		<div class="right" style="margin-top: 30px;">
			<input type="submit" id="SaveNewUser" name="SaveNewUser.x" value="<%=Html._("Button.Create")%>" />
		</div>
		<div class="right" style="margin-top: 1%">
			<div style="width: 40%; height: auto;">
				<img alt="" src="/images/icon/help_content.png" />
				<%=Html._("AdminCreateNewUser.HelpContent")%></div>
		</div>
		<%Html.EndForm(); %>
	</div>
	<div style="min-height: 500px;">
	</div>
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			if ($('#InitialPassword').attr('checked')) {
				$("#Password").attr("disabled", "disabled");
				$("#RePassword").attr("disabled", "disabled");
			}
			$("#InitialPassword").click(function () {
				if ($('#InitialPassword').attr('checked')) {
					$("#Password").attr("disabled", "disabled");
					$("#RePassword").attr("disabled", "disabled");
				}
				else {
					$("#Password").removeAttr("disabled");
					$("#RePassword").removeAttr("disabled");
				}
			});
			$('#IsAssistantAdmin').click(function () {
				if ($('#IsAssistantAdmin').attr('checked')) {
					$("#IsObserve").attr("disabled", "disabled");
				}
				else {
					$("#IsObserve").removeAttr("disabled");
				}
			});
			$('#IsObserve').click(function () {
				if ($('#IsObserve').attr('checked')) {
					$("#IsAssistantAdmin").attr("disabled", "disabled");
				}
				else {
					$("#IsAssistantAdmin").removeAttr("disabled");
				}
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.content
		{
			overflow: auto;
			width: 100%;
		}
		.left
		{
			float: left;
		}
		label .addWidth
		{
			width: 350px;
		}
		.right
		{
			float: right;
			width: 82%;
		}
	</style>
</asp:Content>
