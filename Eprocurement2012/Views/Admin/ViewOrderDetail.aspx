﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<%@ Import Namespace="Eprocurement2012.Models" %>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<div class="centerwrap">
			<%--<div class="step noPrint">
				<img src="/images/theme/Step-3.jpg" /></div>--%>
			<div class="step noPrint">
				<%if (Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Shipped)
	  {%>
				<img src="/images/theme/Step-4.jpg" />
				<%} %>
				<%else if (Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Completed)
	  {%>
				<img src="/images/theme/Step-5.jpg" />
				<%} %>
				<%else
	  {%>
				<img src="/images/theme/Step-3.jpg" />
				<%} %>
			</div>
			<div class="GroupData">
				<h3 class="noPrint">
					<img src="/images/theme/h3-list-icon.png" /><%=Html._("Order.ViewOrder.OrderCompleted")%></h3>
				<div class="Column-Full noPrint">
					<h4>
						<%=Html._("Order.ViewOrder.StepProcess")%></h4>
					<div class="border2">
						<div class="position">
							<label>
								<%=Html._("Order.ViewDataOrder.OrderId")%>
								:</label>
						</div>
						<div style="float: left;">
							<span class="textSpanGray">
								<%=Model.Order.OrderID %></span>
						</div>
						<div class="eclear">
						</div>
						<div class="position">
							<label>
								<%=Html._("Order.ViewDataOrder.OrderDate")%>
								:</label>
						</div>
						<div style="float: left;">
							<span class="textSpanGray">
								<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></span>
						</div>
						<div class="eclear">
						</div>
						<div class="position">
							<label>
								<%=Html._("Order.ViewDataOrder.OrderStatus")%>
								:</label>
						</div>
						<div style="float: left;">
							<span class="textSpanGray">
								<%=Html._(Model.Order.Status.ToResourceKey())%>
								<a href="/Help/OrderInstruction" target="_blank">
									<%=Html._("Order.ViewOrder.OrderDetail")%></a> </span>
						</div>
						<div class="eclear">
						</div>
						<div class="position">
							<label>
								<%=Html._("Order.ViewOrder.WaitingApproval")%>
								:</label>
						</div>
						<div style="float: left;">
							<span class="textSpanGray">
								<%=Model.Order.CurrentApprover != null? Model.Order.CurrentApprover.Approver.DisplayName : Model.Order.PreviousApprover.Approver.DisplayName%></span>
						</div>
						<div class="eclear">
						</div>
						<div class="ApproveStyle">
							<%Html.RenderPartial("OrderDataFlow", Model.Order); %>
						</div>
					</div>
				</div>
			</div>
			<!--end GroupData-->
			<!---->
			<div class="eclear">
			</div>
			<!--Print-->
			<div class="eclear">
			</div>
			<div class="boxPrint" style="position: absolute; margin-left: 740px; margin-top: 50px;">
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintTHPO"})%>">
						Print PO (TH)</a>
				</div>
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid, ActionName = "PrintEnPO"})%>">
						Print PO (EN)</a>
				</div>
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintThQU"})%>">
						Print QU (TH)</a>
				</div>
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintEnQU"})%>">
						Print QU (EN)</a>
				</div>
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_printOrder.jpg" />
					<a href="#" onclick="window.print(); return false">Print web form</a>
				</div>
			</div>
			<div id="print" class="right noPrint btnPrint">
				<img src="/images/theme/icon_printOrder.jpg" />
				<span>Print Document</span>
			</div>
			<div style="margin: 30px 0;">
				<img src="/images/theme/logo-ePro.jpg" class="left" />
				<!--Print Page-->
				<div class="eclear">
				</div>
				<!--Start printOrder detail-->
				<div class="GroupData">
					<div style="text-align: center;">
						<h1 style="color: #4479A3;">
							<%=Html._("Order.ViewDataOrder.OrderSummary")%></h1>
					</div>
					<div style="width: 100%; padding: 20px 8px 8px 0px; margin-right: 20px; margin-top: 35px;">
						<div class="printOrderlabel">
							<label>
								<%=Html._("Order.ViewDataOrder.OrderId")%>
								:
							</label>
						</div>
						<div style="float: left;">
							<p class="printOrderSpanGray">
								<%=Model.Order.OrderID %></p>
						</div>
						<div class="eclear">
						</div>
					</div>
				</div>
				<!--end GroupData-->
				<div class="left" style="width: 450px; min-height: 320px; padding: 5px; margin-right: 9px;
					margin-top: 0px; border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;">
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.CustID")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.CostCenter.CostCenterCustID %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("CartDetail.ShipContactor")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.Contact.ContactorName %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.UserId")%>
							:
						</label>
					</div>
					<div style="float: left;">
						<p class="printOrderSpanGray">
							<%=Model.Order.Contact.Email %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.Phone")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.Contact.ContactorPhone %>
							<%=string.IsNullOrEmpty(Model.Order.Contact.ContactorExtension) ? "" : " #" + Model.Order.Contact.ContactorExtension%></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.Fax")%>
							:
						</label>
					</div>
					<div style="float: left;">
						<p class="printOrderSpanGray">
							<%=Model.Order.Contact.ContactorFax %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.InvoiceAddress")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.Invoice.Address1 %>
							<%=Model.Order.Invoice.Address2 %>
							<%=Model.Order.Invoice.Address3 %>
							<%=Model.Order.Invoice.Address4 %>
						</p>
					</div>
					<div class="eclear">
					</div>
				</div>
				<div class="right" style="width: 450px; min-height: 320px; margin-right: 9px; margin-top: 0px;
					border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
					padding: 5px; float: right">
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewDataOrder.CompanyId")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.Company.CompanyId %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.CompanyName")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.Company.CompanyName %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.DepartmentName")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=string.IsNullOrEmpty(Model.Order.Department.DepartmentID)? "-": "[" + Model.Order.Department.DepartmentID + "] " + Model.Order.Department.DepartmentName %>
						</p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.CostCenterName")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderDate")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewOrder.ShippingAddress")%>
							:
						</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.Shipping.Address1 %>
							<%=Model.Order.Shipping.Address2 %>
							<%=Model.Order.Shipping.Address3 %>
							<%=Model.Order.Shipping.Address4 %>
							<%=Model.Order.Shipping.Province %></p>
					</div>
					<div class="eclear">
					</div>
				</div>
				<!--end printOrder detail-->
				<div class="eclear">
				</div>
				<div class="printOrderlabel" style="padding-top: 20px; padding-bottom: 10px;">
					<label>
						<%=Html._("Order.ViewOrder.OrderItems")%>
					</label>
				</div>
				<div class="eclear">
				</div>
				<div style="float: left">
					<%Html.RenderPartial("OrderDetailPartial", Model.Order); %>
				</div>
				<div class="eclear">
				</div>
				<div class="GroupData">
					<div class="Column-Full">
						<h4>
							<%=Html._("Order.ViewOrder.Remark")%></h4>
						<div class="border2">
							<div class="Remarklabel">
								<label>
									<%=Html._("Order.ViewOrder.ApproverRemark")%>
									:</label>
							</div>
							<div class="left">
								<p class="printOrderSpanGray">
									<%=Model.Order.ApproverRemark %></p>
							</div>
							<div class="eclear">
							</div>
							<div class="Remarklabel">
								<label>
									<%=Html._("Order.ViewOrder.OFMRemark")%>
									:</label>
							</div>
							<div class="left">
								<p class="printOrderSpanGray">
									<%=Model.Order.OFMRemark %></p>
							</div>
							<div class="eclear">
							</div>
							<div class="Remarklabel">
								<label>
									<%=Html._("Order.ViewOrder.ReferenceRemark")%>
									:</label>
							</div>
							<div class="left">
								<p class="printOrderSpanGray">
									<%=Model.Order.ReferenceRemark %></p>
							</div>
							<div class="eclear">
							</div>
							<!--Show AttachFile -->
							<div class="Remarklabel">
								<label>
									<%=Html._("Order.AttachFile")%>
									:</label>
							</div>
							<div class="left">
								<span class="printOrderSpanGray">
									<%if (!String.IsNullOrEmpty(Model.Order.CustFileName))
		   {%>
									<a target="_blank" href="<%=Url.Action("DownloadAttachFile", "Order", new { systemFileName = Model.Order.SystemFileName})%>">
										<%=Model.Order.CustFileName %></a>
									<%} %>
								</span>
							</div>
							<div class="eclear">
							</div>
							<!--End AttachFile -->
						</div>
					</div>
					<!--Column-Full-->
					<div class="eclear">
					</div>
					<div class="Column-Full">
						<h4>
							<%=Html._("Order.ViewOrder.OrderActivity")%></h4>
						<div class="eclear">
						</div>
						<%Html.RenderPartial("OrderActivityPartial", Model.OrderActivity); %>
					</div>
				</div>
				<!--end GroupData-->
			</div>
			<div class="eclear">
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$(".boxPrint").hide();
			$("#print").click(function () {
				$(".boxPrint").slideToggle("slow");
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.GroupData
		{
			margin-top: 10px;
			margin-right: 0px;
			margin-left: 0px;
			margin-bottom: 0px;
		}
		
		.GroupData h3
		{
			font-size: 13px;
			font-weight: bold;
			margin: 8px 0;
			padding: 5px;
			text-shadow: 1px 1px 1px white;
			border: solid 1px #C5C5C5;
			border-radius: 2px;
			-moz-border-radius: 2px;
			background: url(../images/theme/bg-pattern-h3.jpg) repeat-x;
			height: 20px;
			color: #7C7C7C;
		}
		
		.Column-Full
		{
			width: 100%;
			float: left;
		}
		
		.clear
		{
			clear: both;
		}
		.eclear
		{
			clear: both;
			height: 0px !important;
			line-height: 10px !important;
			float: none !important;
			margin-bottom: 10px !important;
		}
		
		#content-shopping-cart-detail
		{
			clear: both;
		}
		
		.printOrderlabel
		{
			color: #7C7C7C;
			display: block;
			float: left; /*font-weight:normal;*/
			margin-right: 10px;
			padding-right: 10px;
			position: relative;
			text-align: left;
			width: 150px;
			line-height: 25px;
			font-weight: bold;
			font-size: 12px;
			top: 0px;
			left: 0px;
		}
		.left
		{
			float: left;
		}
		#content-shopping-cart-detail td.thead, .format-table td.thead
		{
			background-color: #e6e6e6;
			font: bold 11px tahoma;
			padding: 5px;
			text-align: center;
		}
		#content-shopping-cart-detail td
		{
			border: solid 1px #c5c5c5;
			padding: 5px;
			color: #666666;
		}
		.centerwrap
		{
			width: 950px;
			margin: 0 auto;
			margin-top: 30px;
		}
		.GroupData h4
		{
			font-size: 13px;
			color: #4570b7;
			font-weight: bold;
			margin: 0;
			padding-left: 0;
			background: none;
		}
		a
		{
			color: #4570B7;
			text-decoration: none;
		}
		.right
		{
			float: right;
		}
		label
		{
			color: Black;
		}
		
		.boxPrint
		{
			width: 200px;
			padding: 10px;
			margin: 10px;
			height: 250px;
		}
		
		.btnPrint
		{
			width: 120px;
			border: 1px solid #E5E5E5;
			border-radius: 5px 5px 5px 5px;
			cursor: pointer;
			-moz-border-radius: 5px 5px 5px 5px;
			padding: 5px;
			margin-top: 5px;
			font-size: 11px;
		}
	</style>
</asp:Content>
