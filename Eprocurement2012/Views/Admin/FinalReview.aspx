﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.FinalReview>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="noPrint">
		<%Html.RenderPartial("SubMenuAdmin"); %>
	</div>
	<div class="grid_16">
		<div class="grid_2 noPrint" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "RequesterLineGuide", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2 noPrint" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "AllSetting", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.FinalReview.Head")%></h2>
	</div>
	<div class="grid_2 noPrint" style="width: 120px; border: 1px solid #E5E5E5; padding: 9px;
		border-radius: 5px 5px 5px 5px;">
		<a style="color: #4570B7; ext-decoration: none; font-weight: normal" href="<%=Url.Action("ExportFinalReview","ExportExcel")%>">
			<img src="/images/Excel-icon.png" alt="" title="" />
			Export Excel</a>
	</div>
	<!--Print-->
	<div class="eclear">
	</div>
	<div class="grid_2 noPrint" style="width: 120px; margin-left: 180px; margin-top: -58px;
		border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
		padding: 5px; float: left">
		<img src="/images/theme/icon_printOrder.jpg" />
		<a style="color: #4570B7; text-decoration: none; font-weight: normal" href="#" onclick="window.print(); return false">
			<%=Html._("Order.ViewOrder.Print")%></a>
	</div>
	<br />
	<div class="eclear">
	</div>
	<!--Start printOrder detail-->
	<div class="grid_16">
		<%int count = 0;%>
		<div>
			<table>
				<tr>
					<td>
						<%=Html._("Admin.CompanyInfo.CompanyId")%>
						:
						<%=Model.FinalCompany.CompanyId%><br />
						<%=Html._("Admin.CompanyInfo.CompanyThaiName")%>
						:
						<%=Model.FinalCompany.CompanyThaiName%><br />
						<%=Html._("Admin.CompanyInfo.CompanyEngName")%>
						:
						<%=Model.FinalCompany.CompanyEngName%><br />
						<%=Html._("Admin.CompanyInfo.DefaultCustId")%>
						:
						<%=Model.FinalCompany.DefaultCustId%><br />
					</td>
					<td style="padding-right: 500px;">
						<%=Html._("Admin.CompanyLogo")%><br />
						<img src="<%=Url.Action("CompanyLogoImage","Admin", new { companyId = Model.FinalCompany.CompanyId }) %>"
							alt="Company Logo" width="200px" /><br />
						<br />
					</td>
				</tr>
			</table>
		</div>
		<div>
			<table>
				<tr>
					<th>
						<%=Html._("Admin.NewsCompany.Sequence")%>
					</th>
					<th>
						<%=Html._("Admin.CompanyInfo.CustId")%>
					</th>
					<th>
						<%=Html._("Admin.CompanyInfo.InvoiceAddress")%>
					</th>
					<th>
						<%=Html._("Admin.CompanyInfo.ShippingAddress")%>
					</th>
				</tr>
				<%foreach (var item in Model.Invoice)
	  {%>
				<tr>
					<td>
						<%=++count%>
					</td>
					<td>
						<%=item.CustId%>
					</td>
					<td>
						<%=item.Address1%>
						<%=item.Address2%>
						<%=item.Address3%>
						<%=item.Address4%>
					</td>
					<td>
						<%foreach (var shipping in Model.FinalCompany.ListShipAddress.Where(s => s.CustId == item.CustId))
		{ %>
						<div style="width: 220px; padding: 10px; border: 3px solid gray; margin: 0px;">
							<%=shipping.Address1%>
							<%=shipping.Address2%>
							<%=shipping.Address3%>
							<%=shipping.Address4%>
						</div>
						<br />
						<%} %>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
	</div>
	<!--ข้อมูลฝ่าย-->
	<div class="grid_16">
		<%count = 0;%>
		<h5>
			<a href="<%=Url.Action("FinalReview", "Admin", new { name = "Department" })%>" target="_blank">
				<span style="font-weight: bold; cursor: pointer;" id="Department">
					<%=Html._("Admin.Department.DeptInfo")%><img src="/images/icon/icon_2.png" alt="open" /></span>
			</a>
		</h5>
	</div>
	<div class="grid_16" id="DisplayDepartment" style="display: none;">
	</div>
	<!--ข้อมูลหน่วยงาน-แผนก-->
	<div class="grid_16">
		<h5>
			<a href="<%=Url.Action("FinalReview", "Admin", new { name = "CostCenter" })%>" target="_blank">
				<span style="font-weight: bold; cursor: pointer;" id="CostCenter">
					<%=Html._("Admin.CostCenter.CostCenterInfo")%><img src="/images/icon/icon_2.png"
						alt="open" /></span> </a>
		</h5>
	</div>
	<div class="grid_16" id="DisplayCostCenter" style="display: none;">
	</div>
	<!--ข้อมูลผู้ใช้งาน-->
	<div class="grid_16">
		<h5>
			<a href="<%=Url.Action("FinalReview", "Admin", new { name = "User" })%>" target="_blank">
				<span style="font-weight: bold; cursor: pointer;" id="User">
					<%=Html._("Admin.FinalReview.UserData")%><img src="/images/icon/icon_2.png" alt="open" /></span>
			</a>
		</h5>
	</div>
	<div class="grid_16" id="DisplayUser" style="display: none;">
	</div>
	<!--ข้อมูลสิทธิ์การสั่งซื้อ-->
	<div class="grid_16">
		<h5>
			<a href="<%=Url.Action("FinalReview", "Admin", new { name = "RequesterLine" })%>"
				target="_blank"><span style="font-weight: bold; cursor: pointer;" id="RequesterPermission">
					<%=Html._("Admin.FinalReview.RequesterPermission")%><img src="/images/icon/icon_2.png"
						alt="open" /></span> </a>
		</h5>
	</div>
	<div class="grid_16" id="DisplayRequesterPermission" style="display: none;">
	</div>
	<br />
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<%--<script type="text/javascript">
		$(function () {
			$(".complain-toggle").click(function () {
				$(this).parent().next().toggle();
			});


			$("#CostCenter").click(function () {
				GetAllCostCenterFinalReview();
			});

			$("#User").click(function () {
				GetAllUserFinalReview();
			});

			$("#RequesterPermission").click(function () {
				GetRequesterLineFinalReview();
			});

			$("#Department").click(function () {
				GetAllDepartmentFinalReview();
			});

		});

		function GetAllCostCenterFinalReview() {
			var isVisible = $("#DisplayCostCenter").is(":visible"); //เช็คว่า dev เป็น true หรือ false
			if (!isVisible) {
				$.ajax({
					type: "POST",
					url: '<%=Url.Action("FinalReview","Admin")%>',
					data: { name: "CostCenter" },
					success: function (data) {
						$("#DisplayCostCenter").html(data);
						$("#DisplayCostCenter").show();
					}
				});
			}
			else {
				$("#DisplayCostCenter").hide();
			}
		};

		function GetAllUserFinalReview() {
			var isVisible = $("#DisplayUser").is(":visible");
			if (!isVisible) {
				$.ajax({
					type: "POST",
					url: '<%=Url.Action("FinalReview","Admin")%>',
					data: { name: "User" },
					success: function (data) {
						$("#DisplayUser").html(data);
						$("#DisplayUser").show();
					}
				});
			}
			else {
				$("#DisplayUser").hide();
			}
		};

		function GetRequesterLineFinalReview() {
			var isVisible = $("#DisplayRequesterPermission").is(":visible");
			if (!isVisible) {
				$.ajax({
					type: "POST",
					url: '<%=Url.Action("FinalReview","Admin")%>',
					data: { name: "RequesterLine" },
					success: function (data) {
						$("#DisplayRequesterPermission").html(data);
						$("#DisplayRequesterPermission").show();
					}
				});
			}
			else {
				$("#DisplayRequesterPermission").hide();
			}
		};


		function GetAllDepartmentFinalReview() {
			var isVisible = $("#DisplayCostCenter").is(":visible");
			if (!isVisible) {
				$.ajax({
					type: "POST",
					url: '<%=Url.Action("FinalReview","Admin")%>',
					data: { name: "Department" },
					success: function (data) {
						$("#DisplayDepartment").html(data);
						$("#DisplayDepartment").show();
					}
				});
			}
			else {
				$("#DisplayDepartment").hide();
			}
		};



	</script>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
