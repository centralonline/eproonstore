﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/AdminProductCatalog.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ProductCatalog>" %>

<asp:Content ID="Content2" runat="server" ContentPlaceHolderID="CssContent">
</asp:Content>
<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainContent">
	<div style="height: 20px;">
	</div>
	<!--Lin's Space-->
	<fieldset>
		<legend>
			<%=Html._("ProductCatalog.InformationYourProduct")%></legend>
		<div class="grid_2" style="position: absolute; margin-left: 280px;">
			<a href="<%=Url.Action("ExportProduct","ExportExcel")%>">
				<img src="/images/Excel-icon.png" /></a>
		</div>
		<%=string.Format(Html._("ProductCatalog.TotalProductOFM"),Model.CountCompanyCatalog)%><br />
		<br />
		<%=string.Format(Html._("ProductCatalog.TotalControlProduct"), Model.CountUserCatalog)%>
		<%Html.BeginForm("ViewProductCatalog", "Admin", FormMethod.Post);%><br />
		<input name="showProduct" type="submit" value="<%=Html._("ProductCatalog.ButtonListProduct")%>" />
		<%Html.EndForm();%>
	</fieldset>
	<%if (Model.ItemProductCatalog != null)
   {%>
	<%if (Model.ItemProductCatalog.Any())
   {%>
	<div>
		<table>
			<thead>
				<tr>
					<th>
						<%=Html._("ProductCatalog.HeadNo")%>
					</th>
					<th>
						<%=Html._("ProductCatalog.HeadProductId")%>
					</th>
					<th>
						<%=Html._("ProductCatalog.HeadProductName")%>
					</th>
					<th>
						<%=Html._("ProductCatalog.HeadProductPrice")%>
					</th>
					<th>
						<%=Html._("ProductCatalog.HeadProductUnit")%>
					</th>
					<th>
					</th>
				</tr>
			</thead>
			<tbody>
				<%int index = 0; %>
				<%foreach (var product in Model.ItemProductCatalog.GroupBy(m => m.Id))
				{%>
				<tr>
					<td>
						<%=++index%>
					</td>
					<td>
						<%=product.Key%>
					</td>
					<td>
						<%=product.First().Name%>
					</td>
					<td>
						<%=product.First().PriceIncVat.ToString(new MoneyFormat())%>
					</td>
					<td>
						<%=product.First().Unit%>
					</td>
					<td>
					</td>
				</tr>
				<tr>
					<th colspan="2" style="background-color: #FFFFFF; border: 0">
					</th>
					<th style="border-bottom: 2px solid #888; background: #D8D8D8; padding: .4em 1em .2em;">
						<%=Html._("ProductCatalog.HeadDisplayName")%>
					</th>
					<th style="border-bottom: 2px solid #888; background: #D8D8D8; padding: .4em 1em .2em;">
						<%=Html._("ProductCatalog.HeadValidFrom")%>
					</th>
					<th style="border-bottom: 2px solid #888; background: #D8D8D8; padding: .4em 1em .2em;">
						<%=Html._("ProductCatalog.HeadValidTo")%>
					</th>
					<th style="border-bottom: 2px solid #888; background: #D8D8D8; padding: .4em 1em .2em;
						width: 50px;">
					</th>
				</tr>
				<%foreach (var user in product)
				{%>
				<tr>
					<td colspan="2" style="background-color: #FFFFFF; border: 0;">
					</td>
					<td style="background-color: #F8F8F8;">
						<%=user.User.DisplayName%>
					</td>
					<td style="background-color: #F8F8F8;">
						<%=user.ValidFrom.ToString(new DateFormat())%>
					</td>
					<td style="background-color: #F8F8F8;">
						<%=user.ValidTo.ToString(new DateFormat())%>
					</td>
					<td style="background-color: #F8F8F8;">
						<%Html.BeginForm("RemoveProduct", "Admin", new { productId = product.Key, guid = user.User.UserGuid }, FormMethod.Post);%>
						<input type="submit" name="DeleteProduct" value="<%=Html._("Button.Delete")%>" />
						<%Html.EndForm();%>
					</td>
				</tr>
				<%} %>
				<%} %>
			</tbody>
		</table>
	</div>
	<%} %>
	<%else
	{%>
	<span>
		<%=Html._("ProductCatalog.NoProduct")%></span>
	<%} %>
	<%} %>
	<div style="min-height: 400px;">
	</div>
</asp:Content>
