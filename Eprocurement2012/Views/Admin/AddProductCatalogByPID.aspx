﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/AdminProductCatalog.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ProductCatalog>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<h2 id="page-heading">
		<%=Html._("ProductCatalog.AddProductCatalogByPID")%></h2>
	<div class="center" style="width: 100%; margin-top: 10px;">
		<div style="height: 190px; padding: 10px; background: #fff url(/images/theme/inner-bg-gray.jpg) repeat-x top;
			border: solid 1px #C5C5C5; margin-bottom: 10px;">
			<img src="/images/theme/catalog2012.png" style="margin-right: 5px; margin-bottom: 10px;
				position: absolute; border: 0; margin-left: 40px; margin-top: 10px;" alt="" />
			<div class="field" style="margin-left: 380px; margin-top: 20px;">
				<%Html.BeginForm("AddProductCatalogByPID", "Admin", FormMethod.Post); %>
				<h3 class="texttitle" style="border-bottom: 1px dotted #CCCCCC; margin-bottom: 20px;">
					<%=Html._("ProductCatalog.AddProductCatalogByPID")%></h3>
				<div class="by-ProductID">
					<label for="productId">
						<%=Html._("ProductCatalog.ProductID")%>:</label>
					<input id="productId" name="productId" maxlength="7" type="text" />
				</div>
				<br clear="all" />
				<div class="by-ProductID">
					<input id="AddToCartByPId" name="AddToCartByPId.x" type="submit" value="<%=Html._("Button.AddToCartByPId")%>"
						class="default" />
					<label class="errormessage">
						<%=TempData["Error"]%></label>
				</div>
				<div style="width: 90%; padding: 10px; margin-left: 0px;">
					<%=Html._("Admin.ViewProductCatalogAddByPID.Detail")%>
				</div>
				<%Html.EndForm(); %>
			</div>
		</div>
	</div>
	<%Html.BeginForm("AddProductCatalogByPID", "Admin", FormMethod.Post); %>
	<%Html.RenderPartial("ProductCatalogPartial", Model.ItemProductCatalog); %><br />
	<%Html.EndForm(); %>
	<%if (Model.ItemProductCatalog != null && Model.ItemProductCatalog.Any())
	{ %>
	<div style="right">
		<p class="btn">
			<%=Html.ActionLink(Html._("ProductCatalog.ButtonGoToSetProductCatalog"), "SetProductCatalogRequest", "Admin")%></p>
	</div>
	<%} %>
	<div style="min-height: 250px;">
	</div>
</asp:Content>
