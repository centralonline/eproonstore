﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.ProductCatalog>>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Admin.ProductCatalogByPID.Title")%></h2>
		<div>
			<div>
				<%Html.BeginForm("SearchProductCatalog", "Admin", FormMethod.Post); %>
				<span style="font-weight: bold; font-size: 20px;">
					<%=Html._("Admin.ProductCatalogByPID.Search")%></span>
				<%=Html.TextBox("keyWords", "", new { ID = "keywords", style = "width: 250px; height: 20px;" })%>
				<input class="graybutton" type="submit" value="<%=Html._("Button.Search")%>" />
				<span style="color: Blue">
					<%=TempData["Error"]%></span> <span>
						<img alt="" src="/images/icon/help_content.png" />
						<%=Html._("Admin.ProductCatalogByPID.HelpContent")%></span>
				<%Html.EndForm(); %>
			</div>
			<div style="min-height: 50px;">
			</div>
			<%if (Model.Value != null && Model.Value.Count() > 0)
	 {%>
			<div>
				<div>
					<%if (Model.Value.Where(m => m.PropType == Eprocurement2012.Models.ProductCatalog.ProductCatalogType.Company).Any())
	   {%>
					<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
						border: solid 1px #c5c5c5">
						<thead>
							<tr>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("ProductCatalog.HeadProductId")%>
								</th>
								<th width="80" class="thead" style="text-align: center">
									<%=Html._("ProductCatalog.HeadProductName")%>
								</th>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("ProductCatalog.HeadProductPrice")%>
								</th>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("ProductCatalog.HeadProductUnit")%>
								</th>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("Admin.ViewProductCatalogByPID.CatalogType")%>
								</th>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("Admin.ViewProductCatalogByPID.CatalogTypeID")%>
								</th>
								<th width="20" class="thead" style="text-align: center">
									<%=Html._("Admin.ViewProductCatalogByPID.Delete")%>
								</th>
							</tr>
						</thead>
						<%foreach (var item in Model.Value.Where(m => m.PropType == Eprocurement2012.Models.ProductCatalog.ProductCatalogType.Company))
		{ %>
						<tr>
							<td style="text-align: center">
								<%=item.ProductDetail.Id%>
							</td>
							<td>
								<div id="product-list" class="office-list-product-infomation-name">
									<div class="quickinfobt" style="width: 100%;">
										<%=Html.Hidden("pid", item.ProductDetail.Id, new { @class = "productId" })%>
										<span style="margin-left: 100px; font: normal 13px tahoma;">
											<%=Html.Encode(item.ProductDetail.Name)%></span>
									</div>
								</div>
								<%--<%=item.ProductDetail.Name %>
								<input name="productId" type="hidden" value="<%=item.ProductDetail.Id%>" />--%>
							</td>
							<td style="text-align: right">
								<%=item.ProductDetail.PriceIncVat.ToString(new MoneyFormat())%>
							</td>
							<td style="text-align: center">
								<%=item.ProductDetail.Unit %>
							</td>
							<td style="text-align: center">
								<%=item.PropType%>
							</td>
							<td style="text-align: center">
								<%=item.CatalogTypeId %>
							</td>
							<%Html.BeginForm("DeleteProductCatalog", "Admin", FormMethod.Post); %>
							<td style="text-align: center">
								<input type="submit" value="<%=Html._("Button.RemoveProduct")%>" name="Remove" />
								<input name="CatalogTypeId" type="hidden" value="<%=item.CatalogTypeId%>" />
								<input name="PropType" type="hidden" value="<%=item.PropType%>" />
								<input name="ProductId" type="hidden" value="<%=item.ProductDetail.Id%>" />
							</td>
							<%Html.EndForm(); %>
						</tr>
						<%} %>
					</table>
					<%} %>
				</div>
				<div style="min-height: 50px;">
				</div>
				<div>
					<%if (Model.Value.Where(m => m.PropType == Eprocurement2012.Models.ProductCatalog.ProductCatalogType.User).Any())
	   {%>
					<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
						border: solid 1px #c5c5c5">
						<thead>
							<tr>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("ProductCatalog.HeadProductId")%>
								</th>
								<th width="80" class="thead" style="text-align: center">
									<%=Html._("ProductCatalog.HeadProductName")%>
								</th>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("ProductCatalog.HeadProductPrice")%>
								</th>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("ProductCatalog.HeadProductUnit")%>
								</th>
								<th width="50" class="thead" style="text-align: center">
									<%=Html._("Admin.ViewProductCatalogByPID.CatalogType")%>
								</th>
								<th width="40" class="thead" style="text-align: center">
									<%=Html._("Admin.ViewProductCatalogByPID.CatalogTypeID")%>
								</th>
								<th width="20" class="thead" style="text-align: center">
									<%=Html._("Admin.ViewProductCatalogByPID.Delete")%>
								</th>
							</tr>
						</thead>
						<%foreach (var item in Model.Value.Where(m => m.PropType == Eprocurement2012.Models.ProductCatalog.ProductCatalogType.User))
		{ %>
						<tr>
							<td style="text-align: center">
								<%=item.ProductDetail.Id%>
							</td>
							<td>
								<%=item.ProductDetail.Name %>
								<input name="productId" type="hidden" value="<%=item.ProductDetail.Id%>" />
							</td>
							<td style="text-align: right">
								<%=item.ProductDetail.PriceIncVat.ToString(new MoneyFormat())%>
							</td>
							<td style="text-align: center">
								<%=item.ProductDetail.Unit %>
							</td>
							<td style="text-align: center">
								<%=item.PropType%>
							</td>
							<td style="text-align: center">
								<%=item.CatalogTypeId %>
							</td>
							<%Html.BeginForm("DeleteProductCatalog", "Admin", FormMethod.Post); %>
							<td style="text-align: center">
								<input type="submit" value="<%=Html._("Button.RemoveProduct")%>" name="Remove" />
							</td>
							<input name="CatalogTypeId" type="hidden" value="<%=item.CatalogTypeId%>" />
							<input name="PropType" type="hidden" value="<%=item.PropType%>" />
							<input name="ProductId" type="hidden" value="<%=item.ProductDetail.Id%>" />
							<%Html.EndForm(); %>
						</tr>
						<%} %>
					</table>
					<%} %>
				</div>
				<div style="min-height: 50px;">
				</div>
			</div>
			<%}%>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<link rel="stylesheet" type="text/css" href="../../css/QuickInfo.css" />
	<script src="/js/easySlider1.5.js" type="text/javascript"></script>
	<script src="/js/QuickInfo.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function () {
			$("#slider").easySlider({
				vertical: true,
				nextText: "",
				prevText: ""
			});
			if ($("#slider li").size() < 2) {
				$("#nextBtn").hide();
			}
			initQuickInfo($('.quickinfobt'), '<%=Url.Action("QuickInfoForAdmin","Admin")%>');
		});
	</script>
	<style type="text/css">
		.icon-quicklook
		{
			cursor: pointer;
			background: url('/images/btn/quickinfo2.png') no-repeat left center;
			margin-top: 10px;
			margin-bottom: 10px; /*margin-left:35px;*/
			height: 20px;
			width: 80px;
		}
	</style>
</asp:Content>
