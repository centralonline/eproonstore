﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.ProductData>" %>
<%Html.BeginForm("HandleBuy", "Product", FormMethod.Post, new { id = "quickinfo-handleBuy" }); %>
<div id="quickinfo-content">
	<div id="quickinfo-product-image">
		<%--<a href="<%=Url.Action("Details", "Product", new { Model.ProductDetail.Id, title=Model.ProductDetail.Name })%>">--%>
			<img id="productImageQuickInfo" src="<%=Model.ProductDetail.ImageLargeUrl %>" width="300"
				height="300" alt="" /><%--</a>--%>
		<%
			if (Model.ProductDetail.GalleryImages.Any())
			{
		%>
		<div id="quickinfo-product-gallery">
			<ul id="quickinfo-product-gallery-detail">
				<%
				int count = 0;
				foreach (var item in Model.ProductDetail.GalleryImages)
				{
					count++;
				%>
				<li><a href="<%=item.ImageLargeUrl %>" target="_blank" class="imgGalleryQuickInfo">
					<img src="<%=item.ImageSmallUrl %>" alt="" /></a></li>
				<% 
				}
				%>
			</ul>
		</div>
		<%
			}
		%>
	</div>
	<div id="quickinfo-content-detail">
		<div style="width: 370px">
			<div id="product-name">
				<h1 style="color: #4479A3;font: 500 24px/30px tahoma;">
					<%= Html.Encode(Model.ProductDetail.Name)%></h1>
				<p>
					<strong>
						<%=Html._("Product.Detail.ProductId")%>
						:
						<%=Html.Encode(Model.ProductDetail.Id)%></strong>
				</p>
			</div>
			<%
				if ((Model.ProductDetail.FullPriceIncVat > 0) && (Model.ProductDetail.FullPriceIncVat > Model.ProductDetail.PriceIncVat))
				{
			%>
			<span>
				<%=Html._("Product.Detail.NormalPrice")%>
				: </span><i><strike>
					<%=Model.ProductDetail.FullPriceIncVat.ToString(new MoneyFormat())%></strike></i><%=Html._("Product.Detail.Baht")%>
			/
			<%=Model.ProductDetail.Unit%><br />
			<span>
				<%=Html._("Product.Detail.Price")%>
				: </span><strong class="sale-price">
					<%=Model.ProductDetail.DisplayPrice%>
					<%=Html._("Product.Detail.Baht")%>
					/
					<%=Model.ProductDetail.Unit%></strong><br />
			<span>
				<%=Html._("Product.Detail.Discount")%>
				: </span>
			<%=Model.ProductDetail.Discount %>
			<%=Html._("Product.Detail.Baht")%>
			(<%=Model.ProductDetail.DiscountPercent.ToString("#") %>%)
			<%
				}
				else
				{
			%>
			<p class="sale-price">
				<%=Html._("Product.Detail.Price")%>
				:
				<%=Model.ProductDetail.DisplayPrice%>
				<%=Html._("Product.Detail.Baht")%>
				/
				<%=Model.ProductDetail.Unit%>
			</p>
			<%
				}
			%>
			<%			
				if (!Model.ProductDetail.IsPG)
				{ 
			%>
			<%
					if (Model.ProductDetail.TransCharge > 0)
					{
			%>
			<p>
				<img src='/images/icon/icon-car-s.jpg' alt='ค่าจัดส่ง' />
				<%=Html._("CartDetailPartial.TotalDeliveryCharge")%>
				<strong>
					<%=Html._("Product.Detail.Price")%>
					<%=Model.ProductDetail.TransCharge %>
					<%=Html._("Product.Detail.Baht")%>
					/
					<%=Model.ProductDetail.Unit %></strong>
				<br />
				<%=Html._("Shared.QuickInfo.TransCharge")%>
				<a href='<%=Url.Action("AdminInstruction","Help") %>' target='_blank'>More</a>
			</p>
			<%
					}
			%>
			<p>
				<%--<img src="http://www.trendyday.com/images/icon/tick_circle.png" alt="" style="vertical-align: middle;" />--%>
				<%=Html._("Product.Detail.PriceIncVat")%></p>
			<% 
				}
			%>
			<%
				if (Model.ProductDetail.IsContactForDelivery)
				{
			%>
			<p style="color:Red">
				* สินค้ามีค่าจัดส่ง กรุณาติดต่อเจ้าหน้าที่ ที่เบอร์. 027395555
			</p>
			<%
				}
			%>
			<%= Html.Hidden("Id", Model.ProductDetail.Id)%>
			<%= Html.Hidden("CodeId", Model.ProductDetail.CodeId)%>
			<%= Html.Hidden("IsPG", Model.ProductDetail.IsPG)%>
			<div id="validation-message" class="field-validation-error" style="font-weight: bolder;">
			</div>
			<%
				if (Model.ProductSku.Count() > 1)
				{
			%>
			<div>
				<select id="ChildVariationPIDQuickInfo" name="ChildVariationPID" style="width: 300px;">
					<%
					if (Model.ProductDetail.IsPG)
					{
					%>
					<option value="">
						<%=Html._("Product.Detail.SelectProduct")%>
					</option>
					<%
					}
					%>
					<%
					foreach (var item in Model.ProductSku)
					{
						string selectPid = "";
						if (item.Id == Model.ProductDetail.Id)
						{
							selectPid = "selected='selected'";
						}
					%>
					<option value="<%=item.Id %>" <%=selectPid %>>
						<%=item.Name%>
						<%=item.DisplayPrice %>฿</option>
					<%	
					}
					%>
				</select>
			</div>
			<% 
				} 
			%>
			<!--Promotion Text -->
			<%
				if (!String.IsNullOrEmpty(Model.ProductDetail.PromotionText))
				{
			%>
			<p class="description">
				<strong style="background-color: #339966; color: White; padding: 2px 5px">
					<%=Html._("Product.Detail.Special")%></strong>
				<%=Model.ProductDetail.PromotionText %></p>
			<%
				}
			%>
			<!--BestDeal-->
			<p class="status">
				<%=Model.ProductDetail.DisplayBestDeal %></p>
			<!-- Display Stock Text-->
			<% 
				if (!Model.ProductDetail.IsPG)
				{ 
			%>
			<p class="status">
				<%=Model.ProductDetail.DisplayStatusText%></p>
			<% } %>
		</div>
		<%--<%
			if (Model.User.IsRequester && Model.ProductDetail.IsDisplayBuyButton)
			{
		%>
		<div style="margin-top: 10px">
			<%=Html._("Shared.QuickInfo.Quantity")%>
			<input id="Qty" name="Qty" class="qty input-qty" type="text" maxlength="4" value="1" />
			<input type="image" id="AddToCart" name="AddToCart.x" value="AddToCart" src="/images/btn/btn_add_to_cart.png" />
		</div>
		<%
			}
		%>--%>
	</div>
	<div class="clear1">
		<% 
			if (!String.IsNullOrEmpty(Model.ProductDetail.Description))
			{
		%>
		<strong class="left" style="margin: 10px 0">
			<%=Html._("Product.Detail.Description")%></strong>
		<%--<strong class="left" style="margin: 10px 0">
			<%=Html._("Product.Detail.Description")%>&nbsp;|&nbsp;</strong>--%>
		<% 
			}
		%>
		<%--<strong class="left" style="margin: 10px 0"><a href="<%=Url.Action("Details", "Product", new { Model.ProductDetail.Id, title=Model.ProductDetail.Name })%>"
			class="clear1">
			<%=Html._("Shared.QuickInfo.Description")%></a></strong>--%>
		<% 
			if (!String.IsNullOrEmpty(Model.ProductDetail.Description))
			{
		%>
		<div id="description" class="left clear1" style="overflow: auto; height: 100px; width: 690px;">
			<%Html.FormatTextile(Model.ProductDetail.Description); %>
		</div>
		<% 
			}
		%>
	</div>
</div>
<%Html.EndForm(); %>
<script type="text/javascript">
	$(function () {
		$("a.newwindow").attr("target", "_blank");
		$("#AddToWishList").click(addToWishList);
		$("#TellYourFriend").click(tellYourFriend);
		$("#AddToCart").click(addToCart);
		$("#ChildVariationPIDQuickInfo").change(selectSubProduct);
		$('.imgGalleryQuickInfo').click(function (event) {
			event.preventDefault();
			$('#productImageQuickInfo').attr('src', $(this).attr('href'));
		});
	});

	function selectSubProduct(event) {

		var dialog_quickinfo = $("#dialog-quickinfo-content");
		var id = $("#ChildVariationPIDQuickInfo").val();
		var oldWidth = dialog_quickinfo.width();
		var oldHeight = dialog_quickinfo.height();
		dialog_quickinfo.html($("<div>", { style: "background: url('/images/icon_loader.gif') no-repeat center" }).width(oldWidth).height(oldHeight));
		$.ajax({ url: '<%=Url.Action("QuickInfo", "Product") %>',
			cache: false,
			type: "Get",
			data: ({ id: id }),
			success: function (data) {
				dialog_quickinfo.html(data);
			}
		});
	}

	function handleQuickInfo(event, btnName) {
		var dialog_quickinfo = $("#dialog-quickinfo-content");
		var url = '<%=Url.FullUrlAction("HandleBuy", "Product") %>';
		var data = $("#quickinfo-handleBuy").serialize() + btnName;

		$.ajax({ url: url,
			type: "POST",
			data: $("#quickinfo-handleBuy").serialize() + btnName,
			success: function (data) {
				if (data.Result) {
					$("#header-shoppingcart-countproduct").text(data.ItemCount);
					var Message = data.Message;
					var urlQuick = '<%=Url.FullUrlAction("QuickInfoProcessStatus", "Product") %>';
					$.ajax({ url: urlQuick,
						type: "POST",
						data: ({ status: Message }),
						success: function (data) {
							dialog_quickinfo.html(data);
						}
					});
				}
				else {
					alert(data.Message)
				}
			}
		});
	}

	function addToCart(event) {
		event.preventDefault();
		handleQuickInfo(this, "&AddToCart.x=")
	}

	function tellYourFriend(event) {
		event.preventDefault();
	}

	function addToWishList(event) {
		if ($("#ChildVariationPIDQuickInfo option:selected").val() == "") {
			event.preventDefault();
			alert("กรุณาเลือกสินค้าที่ต้องการ ก่อนที่จะ Add to Catalog");
		}
	}
</script>

<style type="text/css">
	.clear1
{
	background: none;
	border: 0;
	clear: both;
	display: block;
	float: none;
	/*font-size: 0;*/
	list-style: none;
	margin: 0;
	padding: 0;
	overflow: hidden;
	/*visibility: hidden;
	width: 0;
	height: 0;*/
}
</style>
