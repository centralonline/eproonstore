﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<br />
		<div style="text-align: center;">
			<img src="/images/step04.png" alt="" /></div>
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "CompanySetting", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%if (Model.User.Company.IsCompModelThreeLevel)
				{ %>
				<%=Html.ActionLink("Next >>", "DepartmentGuide", "Admin",null, new { @style = "width:75px;" })%><%} %>
				<%else
				{ %>
				<%=Html.ActionLink("Next >>", "CostCenterGuide", "Admin",null, new { @style = "width:75px;" })%>
				<%} %>
				</p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.CompanyInfoReview")%></h2>
	</div>
	<div class="grid_16">
		<div>
			<%Html.BeginForm("CompanyInformation", "Admin", FormMethod.Get);%>
			<div class="grid_2" style="padding-top:8px;">
				<label>
					<%=Html._("Admin.CompanyInfoReview.Introduction")%></label>
			</div>
			<div class="grid_2" style="padding-top:8px;">
				<span style="color: Green">Complete</span>
			</div>
			<div class="grid_2">
				<input type="submit" name="submit" value="<%=Html._("Button.Edit") %>" />
			</div>
			<%Html.EndForm();%>
		</div>
		<div>
			<%Html.BeginForm("CompanyLogo", "Admin", FormMethod.Get);%>
			<div class="grid_2" style="padding-top:8px;">
				<label>
					<%=Html._("Admin.CompanyInfoReview.CompanyLogo")%></label>
			</div>
			<div class="grid_2" style="padding-top:8px;">
				<%if (Model.CompanyLogoReview.Bytes.All(b => b == 0))
				{%>
				<span style="color: Red">Not Complete</span>
				<%} %>
				<%else
				{%>
				<span style="color: Green">Complete</span>
				<%} %>
			</div>
			<div class="grid_2">
				<input type="submit" name="submit" value="<%=Html._("Button.Edit") %>" />
			</div>
			<%Html.EndForm();%>
		</div>
		<div>
			<%Html.BeginForm("CompanySetting", "Admin", FormMethod.Get);%>
			<div class="grid_2" style="padding-top:8px;">
				<label>
					<%=Html._("Admin.CompanyInfoReview.CompanySetting")%></label>
			</div>
			<div class="grid_2" style="padding-top:8px;">
				<span style="color: Green">Complete</span>
			</div>
			<div class="grid_2">
				<input type="submit" name="submit" value="<%=Html._("Button.Edit") %>" />
			</div>
			<%Html.EndForm();%>
		</div>
	</div>
	<div style="min-height: 550px;">
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
