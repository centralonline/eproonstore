﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.Order>>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<%if (Model.Value.Count() > 0)
   { %>
	<div class="centerwrap" style="margin-bottom: 30px;">
		<div class="GroupData">
			<h3>
				<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("Order.ViewDataOrder.OrderInformation")%></h3>
		</div>
		<div id="content-shopping-cart-detail">
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
				border: solid 1px #c5c5c5; text-align: center;">
				<tr>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.OrderId")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.OrderDate")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.OrderStatus")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.CompanyId")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.CostCenterId")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ConfirmOrder.Quantity")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.GrandTotalAmount")%>
					</td>
				</tr>
				<%if (Model.Value.Any())%>
				<%{%>
				<%foreach (var item in Model.Value)%>
				<%{ %>
				<tr>
					<td style="text-align: left">
						<%if (item.CurrentApprover.Approver.UserId == Model.User.UserId
						&& (item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.Waiting)
						|| item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin)
						|| item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.Partial)
						|| item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.AdminAllow)))
						{ %>
						<%=Html.ActionLink(item.OrderID, "ViewOrderForApprover", "Admin", new { Id = item.OrderGuid }, null)%>
						<%}
						else { %>
						<%=Html.ActionLink(item.OrderID, "ViewOrderDetail", "Order", new { Id = item.OrderGuid }, null)%>
						<%} %>
					</td>
					<td style="text-align: left">
						<%=item.OrderDate.ToString(new DateTimeFormat())%>
					</td>
					<td style="text-align: center">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus." + item.Status.ToString())%>
					</td>
					<td style="text-align: left">
						<%=item.Company.CompanyId%>
					</td>
					<td style="text-align: left">
						<%="["+item.CostCenter.CostCenterID+"]"+" "+item.CostCenter.CostCenterName%>
					</td>
					<td style="text-align: center">
						<%=item.ItemCountOrder%>
					</td>
					<td style="text-align: right">
						<%=item.GrandTotalAmt.ToString(new MoneyFormat())%>
					</td>
				</tr>
				<%} %>
				<%} %>
			</table>
		</div>
	</div>
	<%} %>
	<%else
   { %>
	<div class="centerwrap" style="text-align: center;">
		<h2>
			<%=Html._("Order.ViewOrder.EmptryOrder")%></h2>
	</div>
	<%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.GroupData
		{
			margin-top: 10px;
			margin-right: 0px;
			margin-left: 0px;
			margin-bottom: 0px;
		}
		
		.GroupData h3
		{
			font-size: 13px;
			font-weight: bold;
			margin: 8px 0;
			padding: 5px;
			text-shadow: 1px 1px 1px white;
			border: solid 1px #C5C5C5;
			border-radius: 2px;
			-moz-border-radius: 2px;
			background: url(../images/theme/bg-pattern-h3.jpg) repeat-x;
			height: 20px;
			color: #7C7C7C;
		}
		
		.Column-Full
		{
			width: 100%;
			float: left;
		}
		
		.clear
		{
			clear: both;
		}
		.eclear
		{
			clear: both;
			height: 0px !important;
			line-height: 10px !important;
			float: none !important;
			margin-bottom: 10px !important;
		}
		
		#content-shopping-cart-detail
		{
			clear: both;
		}
		
		.printOrderlabel
		{
			color: #7C7C7C;
			display: block;
			float: left; /*font-weight:normal;*/
			margin-right: 10px;
			padding-right: 10px;
			position: relative;
			text-align: left;
			width: 150px;
			line-height: 25px;
			font-weight: bold;
			font-size: 12px;
			top: 0px;
			left: 0px;
		}
		.left
		{
			float: left;
		}
		#content-shopping-cart-detail td.thead, .format-table td.thead
		{
			background-color: #e6e6e6;
			font: bold 11px tahoma;
			padding: 5px;
			text-align: center;
		}
		#content-shopping-cart-detail td
		{
			border: solid 1px #c5c5c5;
			padding: 5px;
			color: #666666;
		}
		.centerwrap
		{
			width: 950px;
			margin: 0 auto;
			margin-top: 30px;
		}
		.GroupData h4
		{
			font-size: 13px;
			color: #4570b7;
			font-weight: bold;
			margin: 0;
			padding-left: 0;
			background: none;
		}
		a
		{
			color: #4570B7;
			text-decoration: none;
		}
		.right
		{
			float: right;
		}
		label
		{
			color: Black;
		}
	</style>
</asp:Content>
