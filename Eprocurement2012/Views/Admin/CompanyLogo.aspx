﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16"><br />
		<div style="text-align: center;">
			<img src="/images/step02.png" alt="" /></div>
		<div class="grid_2" 
			style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "CompanyInformation", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "CompanySetting", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.CompanyLogo")%></h2>
	</div>
	<div class="grid_16">
		<div class="grid_2">
			<label>
				<%=Html._("Admin.CompanyLogo.Example")%></label>
			<img src="/images/tip.jpg" alt="tip" /><br />
		</div>
		<div class="grid_14">
			<p>
				<%=Html._("Admin.CompanyLogo.Content")%></p>
			<br />
			<img src="<%=Url.Action("CompanyLogoImage","Admin", new { Model.CompanyId }) %>" alt="Company Logo" width="200px" /><br />
			<%Html.BeginForm("RemoveCompanyLogo", "Admin", FormMethod.Post); %>
			<input class="graybutton" type="submit" value="Remove" />
			<%Html.EndForm(); %>
			<%Html.BeginForm("UploadLogo", "Admin", FormMethod.Post, new { enctype = "multipart/form-data" }); %>
			<br />
			<div>
				<label>
					<%=Html._("Admin.CompanyLogo.Upload")%></label>
			</div>
			<div>
				<input id="file" name="file" type="file" style="border: solid 1px #7895c1" />
				<input id="Upload" type="submit" value="<%=Html._("Button.Upload")%>" class="btn" />
				<span style="color: Red;">
					<%=TempData["errormessage"]%></span>
			</div>
			<div>
				<p class="f11 colorgray">
					<i>
						<%=Html._("Admin.CompanyLogo.FileExtension")%></i></p>
			</div>
			<%Html.EndForm(); %>
		</div>
	</div>
	<div style="min-height: 500px;">
	</div>
</asp:Content>
