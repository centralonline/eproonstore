﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.News>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$('#NewsDetail').wysiwyg({
				controls: {
					insertImage: { visible: false }
				}
			});
			$("#NewsHead").focus();
		});
	</script>
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Admin.ManageNews.AddNews")%></h2>
		<%--		<div>
			<label>
				<%=Html._("Admin.NewsCompany.News")%></label>
		</div>--%>
		<div>
			<%Html.BeginForm("HandleAction", "Admin", FormMethod.Post);%>
			<%=Html.HiddenFor(m => m.NewsGUID, new { id = "NewsGUID" })%>
			<%=Html.HiddenFor(m => m.CompanyID, new { id = "CompanyID" })%>
			<label style="font-weight: bold;">
				<%=Html._("Admin.NewsCompany.NewsTitle")%></label>
			<%=Html.TextBoxFor(m => m.NewsTitle, new { size = "90", id = "NewsHead", tabindex = "1" })%>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(m=>m.NewsTitle)%></span>
		</div>
		<div style="padding-top: 15px;">
			<label style="font-weight: bold;">
				<%=Html._("Admin.NewsCompany.NewsDetail")%></label>
			<%=Html.TextAreaFor(m => m.NewsDetail, new { cols = "100", rows = "10", @id = "NewsDetail", tabindex = "2" })%>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(m=>m.NewsDetail)%></span>
		</div>
		<div>
			<label>
				<%=Html._("Admin.NewsCompany.PeriodDefine")%></label>
			<%=Html.RadioButton("Period", "Now", new { id = "Soon" })%><label for="Soon"><%=Html._("Admin.NewsCompany.SoonNews")%></label>
			<%=Html.RadioButton("Period", "Period", new { id = "Period" })%><label for="Period"><%=Html._("Admin.NewsCompany.PeriodDefine")%></label>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(m=>m.Period)%></span>
		</div>
		<div id="divPeriod">
			<span>
				<%=Html._("Admin.NewsCompany.Since")%>&nbsp;<%=Html.TextBoxFor(m => m.ValidFromDisplay, new { id = "fromDate" })%></span>
			<span>
				<%=Html._("Admin.NewsCompany.To")%>&nbsp;<%=Html.TextBoxFor(m => m.ValidToDisplay, new { id = "toDate" })%></span>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(m=>m.ValidFrom)%></span>
		</div>
		<div>
			<label>
				<%=Html._("Admin.NewsCompany.Priority")%></label>
			<%=Html.RadioButtonFor(m => m.Priority, "Low", new { id = "Low", Checked = "checked" })%><label
				for="Low"><%=Html._("Admin.NewsCompany.Normal")%></label>
			<%=Html.RadioButtonFor(m => m.Priority, "High", new { id = "High" })%><label for="High"><%=Html._("Admin.NewsCompany.High")%></label>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(m=>m.Priority)%></span>
		</div>
		<div>
			<label>
				<%=Html._("Admin.NewsCompany.NewsStatus")%></label>
			<%=Html.RadioButtonFor(m => m.NewsStatus, "Active", new { id = "Active", Checked = "checked" })%><label
				for="Active"><%=Html._("Admin.NewsCompany.Active")%></label>
			<%=Html.RadioButtonFor(m => m.NewsStatus, "Delete", new { id = "Delete" })%><label
				for="Delete"><%=Html._("Admin.NewsCompany.Cancel")%></label>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(m=>m.NewsStatus)%></span>
		</div>
		<br />
		<div>
			<%if (string.IsNullOrEmpty(Model.NewsGUID))
	 {%>
			<input id="AddNews" type="submit" name="AddNews.x" value="<%=Html._("Button.AddNews")%>" />
			<%} %>
			<%else
	 {%>
			<input id="Save" type="submit" name="Save.x" value="<%=Html._("Button.Save")%>" />
			<input id="NewsBack" type="submit" name="NewsBack.x" value="<%=Html._("Button.NewsBack")%>" />
			<%} %>
			<span style="color: Red;">
				<%=TempData["NewsComplete"]%></span>
		</div>
		<%Html.EndForm();%>
	</div>
	<script type="text/javascript">
		$(function () {
			$("#Soon").attr("checked", true);
			if ($("#Soon").attr("checked")) { $("#divPeriod").hide(); } else { $("#divPeriod").show(); }

			$("#Soon").live("click", function () {
				if (this.checked) {
					$("#divPeriod").hide();
				}
			});

			$("#Period").live("click", function () {
				if (this.checked) {
					$("#divPeriod").show();
				}
			});

			$("#fromDate").datepicker({
				changeMonth: true,
				dateFormat: 'd/m/yy'
			});

			$("#toDate").datepicker({
				changeMonth: true,
				dateFormat: 'd/m/yy'
			});

			var newGuid = $("#NewsGUID").attr("value");
			if (newGuid == "") {
				$("#fromDate").datepicker("setDate", new Date());
				$("#toDate").datepicker("setDate", new Date());
			}
			else {
				$("#Period").attr("checked", true);
				$("#divPeriod").show();
			//	$("#fromDate").datepicker("setDate", datefrom);
			//	$("#toDate").datepicker("setDate", dateto);
			}
		});
	</script>
	<div style="min-height: 550px;">
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
