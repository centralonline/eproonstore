﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.AdminPageData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="container_16">
		<div class="grid_16">
			<h2 id="page-heading">
				<%=Html._("Admin.Index.Administrator")%></h2>
		</div>
		<div class="grid_6">
			<div class="box">
				<h2 style="background-color: #FF7F00">
					<%=Html._("Admin.Index.AlertNotComplete")%>
				</h2>
				<div class="block">
					<p>
						<label>
							<%if (Model.User.IsAdmin || Model.User.IsAssistantAdmin)
		 {%>
							<%=string.Format(Html._("Admin.Index.NowUserNotVerify"), Html.ActionLink(Model.UserNotVerify.ToString(), "ViewAllUser", "Admin"))%>
							<%} %>
							<%else
		 {%>
							<%=string.Format(Html._("Admin.Index.NowUserNotVerify"),Model.UserNotVerify)%>
							<%} %>
						</label>
						<br />
						<label>
							<%if (Model.User.IsAdmin || Model.User.IsAssistantAdmin)
		 {%>
							<%=string.Format(Html._("Admin.Index.NowCostNotReqLine"), Html.ActionLink(Model.CostNotReqLine.ToString(), "ViewAllCostCenter", "Admin"))%>
							<%} %>
							<%else
		 {%>
							<%=string.Format(Html._("Admin.Index.NowUserNotVerify"), Model.CostNotReqLine)%>
							<span>
								<%=Model.CostNotReqLine %></span>
							<%} %>
						</label>
						<br />
						<%if (Model.IsNoLogo)
		{ %>
						<label>
							<%=Html._("Admin.Index.IsNoLogo")%></label>
						<%} %>
					</p>
				</div>
			</div>
			<%if (Model.User.IsAdmin || Model.User.IsAssistantAdmin)
	 {%>
			<div class="box">
				<h2 style="background-color: #FF7F00">
					<%=Html._("Admin.Index.IsPendingFromYou")%>
				</h2>
				<div class="block">
					<p>
						<%=Html._("Home.Index.DoyouhaveOrder")%>
						<%if (Model.ItemOrderProcess.CountOrderAllowAdmin > 0)
		{ %>
						<span class="p-red">
							<%=Html.ActionLink(Model.ItemOrderProcess.CountOrderAllowAdmin.ToString(), "GetDataOrderApproveProcess", "Admin")%></span>
						<%}
		else
		{%>
						<span>0</span>
						<%} %>
						<%=Html._("Admin.Index.Order.WaitingAdmin")%>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<%} %>
			<div class="box">
				<h2>
					<%=Html._("SearchDataOrder.SearchMyOrder")%></h2>
				<br />
				<%Html.BeginForm("SearchResult", "Admin", FormMethod.Get);%>
				<%Html.RenderPartial("SearchOrder", Model.SearchOrder);%>
				<input id="redirectUrl" name="redirectUrl" type="hidden" value="<%=Model.RedirectUrl %>" />
				<%Html.EndForm();%>
			</div>
			<div class="box">
				<h2>
					<%=Html._("Admin.Index.PurchaseOrder")%></h2>
				<div class="block" id="tables">
					<span><a href="<%=Url.Action("Index", "Admin", new {thisMonth = true })%>">
						<%=Html._("Admin.Index.Month")%></a></span>&nbsp;|&nbsp; <span><a href="<%=Url.Action("Index", "Admin", new {thisMonth = false })%>">
							<%=Html._("Admin.Index.LastMonth")%></a></span>
					<table>
						<tbody>
							<tr>
								<th>
									<%=Html._("Admin.Index.Order+OrderStatus.Waiting")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusWaiting > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusWaiting.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.Waiting, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
							<tr>
								<th>
									<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Partial")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusPartial > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusPartial.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.Partial, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
							<tr>
								<th>
									<%=Html._("Admin.Index.Order+OrderStatus.WaitingAdmin")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusWaitingAdmin > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusWaitingAdmin.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
							<tr>
								<th>
									<%=Html._("Admin.Index.Order+OrderStatus.Revise")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusRevise > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusRevise.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.Revise, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
							<tr>
								<th>
									<%=Html._("Admin.Index.Order+OrderStatus.Approved")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusApproved > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusApproved.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.Approved, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
							<tr>
								<th>
									<%=Html._("Admin.Index.Order+OrderStatus.Shipped")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusShipped > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusShipped.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.Shipped, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
							<tr>
								<th>
									<%=Html._("Admin.Index.Order+OrderStatus.Completed")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusCompleted > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusCompleted.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.Completed, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
							<tr>
								<th>
									<%=Html._("Admin.Index.Order+OrderStatus.Expired")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusExpired > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusExpired.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.Expired, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
							<tr>
								<th>
									<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Deleted")%>:
								</th>
								<td>
									<%if (Model.ItemOrderProcess.CountStatusDeleted > 0)
		   {%>
									<span>
										<%=Html.ActionLink(Model.ItemOrderProcess.CountStatusDeleted.ToString(), "ViewOrderByStatus", "Report", new { status = Eprocurement2012.Models.Order.OrderStatus.Deleted, thisMonth = Model.ItemOrderProcess.thisMonth }, null)%></span>
									<%} %>
									<%else
		   {%>
									<span>0</span>
									<%} %>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
		<div class="grid_6">
			<div class="box">
				<h2>
					Short Cut</h2>
				<div class="block">
					<p>
						<%if (Model.User.IsAdmin || Model.User.IsAssistantAdmin)
		{%>
						<a href="<%=Url.Action("Index", "Report")%>">
							<img src="/images/icon/reports.png" alt="" /></a> <a href="<%=Url.Action("ViewAllUser", "Admin")%>">
								<img src="/images/icon/users.png" alt="" /></a> <a href="<%=Url.Action("NewsCompany", "Admin")%>">
									<img src="/images/icon/news.png" alt="" /></a> <a href="<%=Url.Action("AllSetting", "Admin")%>">
										<img src="/images/icon/allsetting.png" alt="" /></a>
						<%} %>
						<%else
		{%>
						<a href="<%=Url.Action("Index", "Report")%>">
							<img src="/images/icon/reports.png" alt="" /></a>
						<%} %>
					</p>
				</div>
			</div>
		</div>
		<div class="grid_4">
			<div id="indexAdmin" class="box">
				<h2>
					Administrator Site</h2>
				<div style="height: 120px;">
					<label>
						<%=Html._("Admin.Index.SiteActivateStatus")%>
					</label>
					<hr />
					<div class="block" style="float: left;">
						<p>
							<%if (Model.User.Company.IsSiteActive)
		 {%>
							<label>
								<%=Html._("NewSiteData.SiteActivate.Yes")%></label>
							<%} %>
							<%else
		 {%>
							<label>
								<%=Html._("NewSiteData.SiteActivate.No")%></label>
							<%if (Model.User.IsAdmin || Model.User.IsAssistantAdmin)
		 {%>
							<%Html.BeginForm("GoToOpenSitePage", "Account", FormMethod.Post);%>
							<input type="submit" id="OpenSite" value="<%=Html._("Button.OpenSitePage")%>" />
							<%Html.EndForm();%>
							<%} %>
							<%} %>
							<br />
						</p>
					</div>
					<%--ปุ่มเปลี่ยนไซต์ สำหรับคนที่ดูแลหลายไซต์และเป็น Admin --%>
					<%if ((Model.User.IsAdmin || Model.User.IsAssistantAdmin) && Model.User.IsMultiCompany)
	   { %>
					<div class="block" style="float: right;">
						<label>
							<%=Html._("Admin.Index.ChangeSiteStatus")%>
						</label>
						<p>
							<%Html.BeginForm("OpenSite", "Site", FormMethod.Post);%>
							<input type="submit" id="ChangeSite" name="changeSite.x" value="<%=Html._("Button.ChangeSite")%>" />
							<%Html.EndForm();%>
						</p>
					</div>
					<%} %>
				</div>
			</div>
			<div class="box articles">
				<h2>
					exclusive Provide To</h2>
				<div class="block" id="articles">
					<div class="first article" style="padding-bottom: 10px">
						<a href="#" class="image">
							<img src="<%=Url.Action("CompanyLogoImage","Home", new { companyId = Model.User.CurrentCompanyId }) %>"
								alt="Company Logo" width="200px" class="image" /></a>
						<p>
							<%=Model.User.Company.CompanyName%></p>
						<div class="clear">
						</div>
						<%if (Model.User.IsAdmin || Model.User.IsAssistantAdmin)
		{%>
						<%Html.BeginForm("FinalReview", "Admin", FormMethod.Post);%>
						<input type="submit" id="Detail" value="<%=Html._("Button.Detail")%>" />
						<%Html.EndForm();%>
						<%Html.BeginForm("CompanyLogo", "Admin", FormMethod.Get);%>
						<input type="submit" id="Edit" value="<%=Html._("Button.Edit")%>" style="margin: 0 20px" />
						<%Html.EndForm();%>
						<%} %>
					</div>
				</div>
			</div>
			<div class="box">
				<%if (Model.User.Company.UseCompanyNews)
	  {%>
				<h2>
					<%=Html._("Home.Index.NewsAdmin")%></h2>
				<%} %>
				<div class="adminbox">
					<%if (Model.CompanyNews.Any())
	   {%>
					<%Html.BeginForm("AdminNewsCompany", "Admin", FormMethod.Post);%>
					<%=Html.Hidden("newsType", Model.CompanyNews.First().NewsType)%>
					<%Html.RenderPartial("ListNewsCompany", Model.CompanyNews);%>
					<div style="position: absolute;">
						<input type="submit" id="Submit2" value="<%=Html._("Button.All")%>" />
					</div>
					<%Html.EndForm();%>
					<%} %>
					<%else
	   {%>
					<label>
						<%=Html._("Admin.Index.ErrorNotCompanyNews")%></label>
					<%} %>
					<%if (Model.User.IsAdmin || Model.User.IsAssistantAdmin)
	   {%>
					<%Html.BeginForm("CreateNewsCompany", "Admin", FormMethod.Get);%>
					<div style="margin: 0 100px;">
						<input type="submit" value="<%=Html._("Button.News")%>" />
					</div>
					<%Html.EndForm();%>
					<%} %>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		input[type="submit"]
		{
			width: 80px;
			height: 33px;
			background-image: url(/images/theme/GrayButton.png);
			background-color: transparent;
			background-repeat: no-repeat;
			border: none;
			cursor: pointer;
			color: #7C7C7C;
			font-size: 11px;
			padding-left: 5px;
		}
		input[type="submit"]:hover
		{
			background-image: url(/images/theme/GrayButton-hover.png);
			width: 80px;
			height: 33px;
			background-color: transparent;
			background-repeat: no-repeat;
			border: none;
			cursor: pointer;
			color: #7C7C7C;
			font-size: 11px;
			padding-left: 5px;
		}
		.article form
		{
			display: inline-block;
			margin-bottom: 1em;
		}
	</style>
</asp:Content>
