﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.News>" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div style="min-height: 550px;">
		<div class="grid_16">
			<%Html.BeginForm("AdminNewsCompany", "Admin", FormMethod.Post);%>
			<%=Html.Hidden("newsType", Model.NewsType)%>
			<%Html.RenderPartial("NewsDetail", Model);%><br />
			<div>
				<input class="graybutton" type="submit" id="ViewAll" value="<%=Html._("Button.NewsBack")%>" style="margin-left: 270px;" />
			</div>
			<%Html.EndForm();%>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
