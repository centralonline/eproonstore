﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.IMasterPageData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
			<%if (Model.User.Company.IsCompModelThreeLevel)
			{%>
			<%=Html.ActionLink("<< Previous", "DepartmentGuide", "Admin",null, new { @style = "width:75px;" })%><%} %>
			<%else
			{ %>
			<%=Html.ActionLink("<< Previous", "CompanyGuide", "Admin")%><%} %>
			</p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "ViewAllCostCenter", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.CostCenterGuide")%></h2>
	</div>
	<div class="grid_16">
		<img src="/images/Company_info/Company_sec02_3.jpg" alt="you are here" /><br />
	</div>
	<div style="min-height: 500px;">
	</div>
	<!--Lin's min-height-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
