﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.News>>>" %>


<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<div style="min-height:550px;">
	<div class="grid_16">
		<%Html.RenderPartial("AllNews", Model);%>
		<%Html.BeginForm("Index", "Admin", FormMethod.Post);%>
		<div>
			<input type="submit" value="<%=Html._("Button.IndexBack")%>" />
		</div>
		<%Html.EndForm();%>
	</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
