﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "UserGuide", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "FinalReview", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.RequesterLineGuide")%></h2>
	</div>
	<div class="grid_16">
		<img src="/images/Company_sec04.jpg" alt="Company" />
	</div>
	<div style="min-height: 300px;">
	</div>
	<div>
		<div class="grid_3">
				<p class="btn" style="margin-left: 3%">
				<%=Html.ActionLink(Html._("Admin.RequesterLineGuide.RoleCostCenter"), "ViewAllCostCenter", "Admin")%></p>
		</div>
		<div class="grid_3">
			<p class="btn" style="margin-left: 3%">
				<%=Html.ActionLink(Html._("Admin.RequesterLineGuide.RoleUser"), "ViewAllUser", "Admin")%></p>
		</div>
	</div>
	<div style="min-height: 500px;">
	</div>
	<!--Lin's min-height-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
