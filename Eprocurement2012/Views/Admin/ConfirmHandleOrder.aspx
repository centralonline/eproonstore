﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SelectSite.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ConfirmHandelOrder>" %>


<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div id="confirm-handel-order" class="minHeight650">
		<p class="pTitleCenter" style="padding-left: 10px;">
			<%=Html._("Admin.ConfirmHandelOrder.ConfirmSecurity")%></p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<%Html.BeginForm("HandleOrder", "Admin", FormMethod.Post, new { id = "confirmhandelorder" }); %>
		<div style="padding-left: 300px; margin-top: 50px;">
			<div class="Sitelabel3">
				<label>
					<%=Html._("Shared.LogInCenter.Password")%>:
				</label>
				<span style="margin-left: 10px;">
					<%=Html.PasswordFor(m => m.Password)%></span> <span style="color: Red; font-size: 11px;
						padding-left: 10px;">
						<%=TempData["Error"]%></span>
			</div>
			<div class="eclear">
			</div>
			<div class="Sitelabel3">
				<label>
					<%=Html._("CartDetail.Remark")%>:
				</label>
				<span style="margin-left: 10px;">
					<%=Html.TextBoxFor(m => m.Remark)%></span>
			</div>
			<div class="eclear">
			</div>
			<div class="Sitelabel3" style="margin-left: 70px;">
				<label>
					<%=Html._("ApproverOrder.ConfirmHandelOrder.Content")%></label>
			</div>
			<div class="eclear">
			</div>
			<div class="PositionButton">
				<%=Html.HiddenFor(m => m.InputActionName)%>
				<%=Html.HiddenFor(m => m.OrderGuid)%>
				<input class="graybutton" type="submit" id="Confirm" name="Confirm" value="<%=Html._("Button.OK")%>"
					style="margin-top: 20px;" />
			</div>
		</div>
		<%Html.EndForm(); %>
	</div>
</asp:Content>
