﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.CompanyDepartment>>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "DepartmentGuide", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "CostCenterGuide", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.Department.ContentInfo")%></h2>
	</div>
	<div class="grid_16">
		<div class="grid_16">
			<%int count = 0; %>
			<p class="btn" style="float: right">
				<%=Html.ActionLink(Html._("Admin.Department.Add"), "CreateDepartment", "Admin")%></p>
			<h5>
				<%=Html._("Admin.Department.DeptInfo")%></h5>
			<span style="color: #FF3300">
				<%=TempData["deleteError"]%></span>
			<%if (Model.Value.Any())
			{%>
			<table>
				<tr>
					<th>
						<%=Html._("Admin.CompanyInfo.No")%>
					</th>
					<th>
						<%=Html._("Admin.Department.DeptID")%>
					</th>
					<th>
						<%=Html._("Admin.Department.ThaiName")%>
					</th>
					<th>
						<%=Html._("Admin.Department.EngName")%>
					</th>
					<th>
						<%=Html._("Admin.Department.Status")%>
					</th>
					<th>
						<%=Html._("Admin.Department.Modify")%>
					</th>
					<th>
						<%=Html._("Admin.Department.Cancel")%>
					</th>
				</tr>
				<%foreach (var item in Model.Value)%>
				<%{%>
				<tr>
					<td>
						<%=++count%>
					</td>
					<td>
						<%=item.DepartmentID%>
					</td>
					<td>
						<%=item.DepartmentThaiName%>
					</td>
					<td>
						<%=item.DepartmentEngName%>
					</td>
					<td>
						<%=item.DisplayDepartmentStatus%>
					</td>
					<td>
						<%=Html.ActionLink("Edit", "EditDepartment", "Admin", new { departmentId = item.DepartmentID }, null)%>
					</td>
					<td>
						<%if (item.DepartmentStatus == Eprocurement2012.Models.CompanyDepartment.DeptStatus.Active)
						{%>
						<%=Html.ActionLink("Cancel", "RemoveDepartment", "Admin", new { departmentId = item.DepartmentID, companyId = Model.Value.First().CompanyId }, null)%>
						<%} %>
					</td>
				</tr>
				<%} %>
			</table>
			<%} %>
		</div>
	</div>
	<div style="min-height: 550px;">
	</div>
</asp:Content>
