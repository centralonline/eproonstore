﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ModelViewListUserData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<br />
		<div style="text-align: center; margin-bottom: 10px;">
			<img src="/images/step3step.png" alt="" /></div>
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "UserGuide", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "RequesterLineGuide", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>

		<h2 id="page-heading">
			<%=Html._("OFMAdmin.UserInfo.AllAdmin")%></h2>
	</div>
	<div class="grid_16">
		<div id="UnderlineLink"  align="right">
			<table>
				<tr>
					<td>
						<%=Html._("Admin.ViewAllUser.SelectType")%>
						:
						<%=Html.ActionLink(Html._("Admin.ViewAdminCompany.AdminUser"), "ViewAllAdmin", "Admin")%>
						|
						<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.UserInfo"), "ViewAllUser", "Admin")%>
					</td>
					<td width="100">
						<%Html.BeginForm("CreateNewUser", "Admin", FormMethod.Get); %>
						<p style="text-align: right; margin-bottom: 0px">
							<input id="CreateUser" name="CreateUser" type="submit" value="<%=Html._("Button.Create")%>" /></p>
						<%Html.EndForm(); %>
					</td>
				</tr>
			</table>
		</div>
		<div class="box">
			<%Html.BeginForm("ManageUser", "Admin", FormMethod.Post); %>
			<span style="color:Red;float:right;"><%=TempData["ErrorSetRootAdmin"]%></span>
			<%Html.RenderPartial("DataListUser", Model.UserData); %>
			<%Html.EndForm(); %>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
