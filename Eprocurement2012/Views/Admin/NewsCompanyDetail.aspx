﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.News>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		.bg-news
		{
			background-image: url(/images/theme/news-bg.jpg);
			background-repeat: no-repeat;
			background-position: top left;
			min-height: 300px;
		}
		.newsPostion
		{
			margin-left: 265px;
			background: #FFFFFF;
		}
		.newsLeft
		{
			width: 150px;
			text-align: left;
			font-weight: bold;
			float: left;
			display: inline-block;
			line-height: 25px;
		}
		.newsRight
		{
			text-align: left;
			line-height: 25px;
		}
	</style>
	<div class="grid_16">
		<div class="bg-news">
			<div class="newsPostion">
				<h2 id="page-heading">
					<%=Html._("OFMAdmin.ViewAllNews.News")%></h2>
				<div style="border: 1px solid #C8C8C8; padding: 10px; min-height: 280px;">
					<div>
						<div class="newsLeft">
							<%=Html._("Admin.NewsCompany.NewsTitle")%>
							:
						</div>
						<div class="newsRight">
							<%=Model.NewsTitle%>
						</div>
						<div class="newsLeft">
							<%=Html._("Admin.NewsCompany.NewsPeriod")%>
							:
						</div>
						<div class="newsRight">
							<%=Model.ValidFrom.ToString(new DateFormat())%>
							&nbsp; <b>
								<%=Html._("Admin.NewsCompany.To")%></b> &nbsp;
							<%=Model.ValidTo.ToString(new DateFormat())%>
						</div>
						<div class="newsLeft">
							<%=Html._("Admin.NewsCompany.NewsStatus")%>
							:
						</div>
						<div class="newsRight">
							<%=Model.DisplayNewsStatus%>
						</div>
						<div class="newsLeft">
							<%=Html._("Admin.NewsCompany.Priority")%>
							:
						</div>
						<div class="newsRight">
							<%=Model.DisplayPriority%>
						</div>
						<div class="newsLeft">
							<%=Html._("Admin.NewsCompany.NewsDetail")%>
							:
						</div>
						<div class="newsRight">
							<p>
								<%=Model.NewsDetail%></p>
						</div>
					</div>
				</div>
				<br />
				<p class="btn">
					<%=Html.ActionLink(Html._("Button.NewsBack"), "NewsCompany", "Admin")%></p>
				<br />
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
