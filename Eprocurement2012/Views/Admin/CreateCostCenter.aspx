﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CostCenter>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">

	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "ViewAllCostCenter", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "UserGuide", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.CostCenter.ContentCreate")%></h2>
		<div class="grid_12">
			<%Html.BeginForm("CreateCostCenter", "Admin", FormMethod.Post);%>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<%if (Model.User.Company.IsCompModelThreeLevel)
	 { %>
			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.SelcetDept")%>
				</label>
			</div>
			<div>
				<%=Html.DropDownListFor(m => m.DepartmentID, new SelectList(Model.ListCompanyDepartment, "DepartmentID", "DepartmentDisplayname"), "กรุณาเลือก", new { @style = "width: 200px;" })%>
				<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.DepartmentID)%></span>
			</div>
			<%} %>
			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.CostCenterID")%>
				</label>
			</div>
			<!-- Start TBRunningnumbers Standard-->
			<div>
				<%if (Model.User.Company.OrderIDFormat == Eprocurement2012.Models.Company.OrderIDFormatType.Standard)
	  {%>
				<span>
					<%=Html.TextBoxFor(m => m.CostCenterID, new { maxlength = 15, @id="costId" })%></span>
				<%} %>
				<%else
	  {%>
				<span>
					<%=Html.TextBoxFor(m => m.CostCenterID, new { maxlength = 8, @id="costId" })%></span>
				<%} %>
				<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CostCenterID)%></span>
			</div>
			<!-- End TBRunningnumbers Standard-->

			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.OracleCode")%>
				</label>
			</div>
			<div>
				<span>
					<%=Html.TextBoxFor(m => m.OracleCode)%>
				</span>
			</div>

			<div class="ContentCostCenterID">
				<label for="IsByPassAdmin">
					<%=Html._("Admin.CostCenter.ContentCostCenterID")%></label>
			</div>
			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.ThaiName")%>
				</label>
			</div>
			<div>
				<span>
					<%=Html.TextBoxFor(m => m.CostCenterThaiName)%>
				</span><span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CostCenterThaiName)%></span>
			</div>
			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.EngName")%>
				</label>
			</div>
			<div>
				<span>
					<%=Html.TextBoxFor(m => m.CostCenterEngName)%>
				</span><span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CostCenterEngName)%></span>
			</div>
			<%if (Model.User.Company.IsByPassAdmin)
	 { %>
			<div class="ContentCostCenterID">
				<%=Html.CheckBoxFor(m => m.UseByPassAdmin, new { id = "UseByPassAdmin" })%>
				<label for="UseByPassAdmin">
					<%=Html._("Admin.CostCenter.IsByPassAdmin")%></label>
			</div>
			<%} %>
			<%if (Model.User.Company.IsAutoApprove)
	 { %>
			<div class="ContentCostCenterID">
				<%=Html.CheckBoxFor(m => m.UseAutoApprove, new { id = "UseAutoApprove" })%>
				<label for="UseAutoApprove">
					<%=Html._("Admin.CostCenter.IsAutoApprove")%></label>
			</div>
			<%} %>
			<%if (Model.User.Company.IsByPassApprover)
	 { %>
			<div class="ContentCostCenterID">
				<%=Html.CheckBoxFor(m => m.UseByPassApprover, new { id = "UseByPassApprover" })%>
				<label for="UseByPassApprover">
					<%=Html._("Admin.CostCenter.IsByPassApprover")%></label>
			</div>
			<%} %>
			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.SelectInvoice")%>
				</label>
			</div>
			<div>
				<%=Html.DropDownListFor(m => m.SelectInvoice, new SelectList(Model.ListInvoiceAddress, "CustId", "DisplayInvoiceAddress"), new { @style = "width: 300px;" })%>
				<input type="submit" id="ShowAddressInv" name="ShowAddressForCreate.x" value="<%=Html._("Admin.CostCenter.ShowInvoiceAddress")%>" />
				<input type="submit" id="AddNewShipAddr" name="AddNewShipAddress.x" value="<%=Html._("Admin.CostCenter.AddNewShipAddress")%>" />
			</div>
			<div class="changInv"><!-- div สำหรับเช็คให้กดปุ่ม แสดงที่อยู่ใบกำกับภาษี ก่อนแก้ไขรหัสลูกค้า-->
			<%if (Model.CostCenterInvoice != null)
	 {%>
			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.CustID")%>
				</label>
				<%=Model.CostCenterCustID%>
				<%=Html.HiddenFor(m => m.CostCenterCustID)%>
			</div>
			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.InvoiceAddress")%>
				</label>
			</div>
			<div class="boxAddress format">
				<%=Model.CostCenterInvoice.Address1%><br />
				<%=Model.CostCenterInvoice.Address2%><br />
				<%=Model.CostCenterInvoice.Address3%><br />
				<%=Model.CostCenterInvoice.Address4%><br />
			</div>
			<%} %>
			<div style="float: right;">
				<label>
					&nbsp;</label>
				<input id="Submit1" type="submit" name="CreateCostCenter.x" value="<%=Html._("Button.Save")%>" />
				<input id="Reset1" type="reset" name="Cancel.x" value="<%=Html._("Button.Cancel")%>" />
			</div>
			<!-- Start แสดงผลแบบตาราง-->
			<div class="format">
				<label>
					<%=Html._("Admin.CostCenter.SelectShipping")%>
				</label>
			</div>
			<div class="format">
				<label  style="text-align:left;color:Red;">
					<%=TempData["ErrorSelectShipID"]%>
				</label>
			</div>
			<div>
				<table id="sortBy">
					<thead>
						<tr>
							<th>
							</th>
							<th>
								ลำดับ
							</th>
							<th>
								รหัสสถานที่จัดส่ง
							</th>
							<th>
								ที่อยู่สถานที่จัดส่ง
							</th>
							<th>
								เบอร์ติดต่อ
							</th>
							<th>
								จังหวัด
							</th>
							<th>
								รหัสไปรษณีย์
							</th>
						</tr>
					</thead>
					<%int count = 0;%>
					<%foreach (var shipping in Model.ListShipAddress)
	   {%>
					<tr>
						<td>
							<%=Html.RadioButtonFor(m => m.SelectShipID, shipping.ShipID, new { id = shipping.ShipID })%>
						</td>
						<td>
							<%=++count%>
						</td>
						<td>
							<%=shipping.ShipID %>
						</td>
						<td>
							<%=shipping.Address1%><br />
							<%=shipping.Address2%><br />
							<%=shipping.Address3%><br />
						</td>
						<td>
							<%=shipping.ShipPhoneNo%>
						</td>
						<td>
							<%=shipping.Province %>
						</td>
						<td>
							<%=shipping.ZipCode %>
						</td>
					</tr>
					<%} %>
				</table>
			</div>
			<br />
			<br />
			<!-- End แสดงผลแบบตาราง-->
			</div>
			<%Html.EndForm();%>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script src="/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<link href="/css/dataTables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(function () {
			$("#costId").keypress(function (e) {
				var key = String.fromCharCode(e.which);
				if (isLetter(key) == null) {
					return false;
				}
			});
			$('#sortBy').dataTable();

			$("#SelectInvoice").change(function () {
				$(".changInv").hide();
			});
		});
		function isLetter(s) {
			//return s.match("^[a-zA-Z0-9\(\)]+$");
			return s.match("^[A-Za-z0-9_\-]*$");
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.format label
		{
			float: left;
			margin-right: 3%;
			text-align: right;
			width: 30%;
		}
		.forlabel
		{
			width: 250px;
			float: left;
		}
		.boxAddress
		{
			float: left;
			margin-right: 3%;
			text-align: left;
			width: 60%;
		}
		.ContentCostCenterID
		{
			/*margin-left: 28%;*/
			margin-left: 33%;
		}
		.IsByPassAdmin
		{
			margin-left: 28%;
		}
		.IsAutoApprove
		{
			margin-left: 28%;
		}
		.IsByPassApprover
		{
			margin-left: 33%;
		}
		.SubmitCostCenter
		{
			margin-left: 250px;
		}
		
		#ShowAddressInv
		{
			clear: both;
			width: 140px;
			background-size: 140px 33px;
		}
	</style>
</asp:Content>
