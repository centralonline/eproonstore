﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.FinalReview>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16"><br />
		<div style="text-align: center; margin-bottom: 10px;">
			<img src="/images/stepFinalstep.png" alt="" /></div>
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "LearnAboutApproval", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "RequesterLineGuide", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.UserFinalReview.head")%></h2>
		<div class="grid_16">
			<%Html.BeginForm("ViewAllUser", "Admin", FormMethod.Get);%>
			<div>
				<div class="grid_2" style="padding-top: 8px;">
					<label>
						<%=Html._("Admin.UserFinalReview.DataUser")%></label>
				</div>
				<div class="grid_2" style="padding-top: 8px;">
					<%if (Model.UserPurchase.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Approver || o.UserRoleName == Eprocurement2012.Models.User.UserRole.Requester).Any())
	   {  %>
					<span style="color: Green">
						<%=Model.UserPurchase.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Approver || o.UserRoleName == Eprocurement2012.Models.User.UserRole.Requester).Count()%></span>
					<span style="color: Green; float: right; margin-right: 90px;">Created</span>
					<%} %>
					<%else
	   { %>
					<span style="color: red; float: right; margin-right: 90px; width: 82px;">Not Complete</span>
					<%} %>
				</div>
				<div class="grid_2">
					<%--<a href="<%=Url.GetFullUrl("/Admin/ViewAllUser")%>">Edit</a>--%>
					<input type="submit" name="submit" value="<%=Html._("Button.Edit") %>" />
				</div>
				<br />
				<br />
			</div>
			<div>
				<div class="grid_2" style="padding-top: 8px;">
					<label>
						<%=Html._("Admin.UserFinalReview.Requester")%></label>
				</div>
				<div class="grid_2" style="padding-top: 8px;">
					<%if (Model.UserPurchase.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Requester).Any())
	   {  %>
					<span style="color: Green">
						<%=Model.UserPurchase.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Requester).Count()%></span>
					<span style="color: Green; float: right; margin-right: 90px;">Created</span>
					<%} %>
					<%else
	   { %>
					<span style="color: red; float: right; margin-right: 90px; width: 84px;">Not Complete</span>
					<%} %>
				</div>
				<div class="grid_2">
					<%--<a href="<%=Url.GetFullUrl("/Admin/ViewAllUser")%>">Edit</a>--%>
					<input type="submit" name="submit" value="<%=Html._("Button.Edit") %>" />
				</div>
			</div>
			<br />
			<br />
			<div>
				<div class="grid_2" style="padding-top: 8px;">
					<label>
						<%=Html._("Admin.UserFinalReview.Approver")%></label>
				</div>
				<div class="grid_2" style="padding-top: 8px;">
					<%if (Model.UserPurchase.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Approver).Any())
	   {  %>
					<span style="color: Green">
						<%=Model.UserPurchase.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Approver).Count()%></span>
					<span style="color: Green; float: right; margin-right: 90px;">Created</span>
					<%} %>
					<%else
	   { %>
					<span style="color: red; float: right; margin-right: 90px; width: 82px;">Not Complete</span>
					<%} %>
				</div>
				<div class="grid_2">
					<%--<a href="<%=Url.GetFullUrl("/Admin/ViewAllUser")%>">Edit</a>--%>
					<input type="submit" name="submit" value="<%=Html._("Button.Edit") %>" />
				</div>
			</div>
			<br />
			<br />
			<div>
				<div class="grid_2" style="padding-top: 8px;">
					<label>
						<%=Html._("Admin.UserFinalReview.SetApprover")%></label>
				</div>
				<div class="grid_2" style="padding-top: 8px;">
					<%
						if (Model.RequesterLineDetail.Where(o => o.ApproverId.Contains(Model.UserPurchase.Where(m => m.UserRoleName == Eprocurement2012.Models.User.UserRole.Approver).Select(m => m.UserId).ToString())).Count() > 0)
						{ %>
					<span style="color: Green; float: right; margin-right: 90px;">Complete</span>
					<%} %>
					<%else
						{ %>
					<span style="color: red; float: right; margin-right: 90px; width: 84px;">Not Complete</span>
					<%} %>
				</div>
				<div class="grid_2">
					<%--<a href="<%=Url.GetFullUrl("/Admin/ViewAllUser")%>">Edit</a>--%>
					<input type="submit" name="submit" value="<%=Html._("Button.Edit") %>" />
				</div>
			</div>
			<%Html.EndForm();%>
		</div>
	</div>
	<div style="min-height: 500px;">
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
