﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ShippingData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<%Html.BeginForm("ShippingInfo", "Admin", FormMethod.Post);%>
		<%=Html.HiddenFor(m => m.ShipID)%>
		<%=Html.HiddenFor(m => m.PhoneID)%>
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "CreateCostCenter", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "ViewAllCostCenter", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Order.ViewOrder.ShippingAddress")%>
		</h2>
		<div>
			<fieldset>
				<legend><%=Html._("Order.ViewOrder.ShippingAddress")%></legend>
				<div class="table-format">
					<%if (Model.ShipID != 0)
	   {%>
					<label>
						<%=Html._("Admin.CompanyInfo.CustId")%>:</label>
					<%=Model.CustIdSelected %>
					<%=Html.HiddenFor(m => m.CustIdSelected)%>
					<%}
	   else
	   {%>
					<label>
						<%=Html._("Admin.ManageShipping.SelectCustId")%></label>
					<%=Html.DropDownListFor(m => m.CustIdSelected, new SelectList(Model.ListCustId), new { @style = "width:100px;" })%>
					<%} %>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.ManageShipping.Name")%></label>
					<%=Html.TextBoxFor(m => m.ShipAddress1, new { maxlength = 55, @style = "width:200px;" })%>
					<span style="color: Red">*
					<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress1)%></span>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.ManageShipping.PhoneNumber")%></label>
					<%=Html.TextBoxFor(m => m.ShipPhoneNumber, new { maxlength=10, @style = "width:200px;"})%>
					<span style="color: Red">*
					<%=Html.LocalizedValidationMessageFor(m => m.ShipPhoneNumber)%></span>
					<span><%=Html._("Shared.MyProfile.ExtNumber")%></span>
					<%=Html.TextBoxFor(m => m.ShipPhoneNumberExtension, new { maxlength = 5, @style = "width:100px;" })%>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.ManageShipping.ShippingAddress")%></label>
					<%=Html.TextBoxFor(m => m.ShipAddress2, new { maxlength = 55, @style = "width:500px;" })%>
					<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress2)%></span>
				</div>
				<div class="table-format">
					<label>
						&nbsp;</label>
					<%=Html.TextBoxFor(m => m.ShipAddress3, new { maxlength = 55, @style = "width:500px;" })%>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.CostCenter.Province")%></label>
					<%=Html.DropDownListFor(m => m.ProvinceSelected, Model.ProvinceList, new { @style = "width:200px;" })%>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.CostCenter.ZipCode")%></label>
					<%=Html.TextBoxFor(m => m.ShipAddress4, new { maxlength = 5 , @style = "width:200px;" })%>
					<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress4)%></span>
				</div>
				<%if (Model.ShipID == 0)
	  {%>
				<div class="table-format">
					<label>
						&nbsp;</label>
					<input type="submit" name="CreateShipping.x" value="<%=Html._("Button.Save")%>" />
				</div>
				<%} %>
				<%else
	  {%>
				<div class="table-format">
					<label>
						&nbsp;</label>
					<input type="submit" name="SaveShipping.x" value="<%=Html._("Button.Save")%>" />
					<input type="submit" name="CancelShipping.x" value="<%=Html._("Button.Cancel")%>" />
				</div>
				<%} %>
			</fieldset>
		</div>
		<%Html.EndForm();%>
		<%if (Model.OtherShippings.Any())
	{%>
		<div>
			<fieldset>
				<legend><%=Html._("Admin.ManageShipping.OtherShipping")%></legend>
				<table>
					<tr>
						<th>
							<%=Html._("Admin.CompanyInfo.No")%>
						</th>
						<th>
							<%=Html._("Admin.CompanyInfo.CustId")%>
						</th>
						<th>
							<%=Html._("Admin.ManageShipping.ShipContactor")%>
						</th>
						<th>
							<%=Html._("Admin.ManageShipping.ShipPhoneNo")%>
						</th>
						<th>
							<%=Html._("Admin.ManageShipping.ShippingAddress")%>
						</th>
						<th>
						</th>
					</tr>
					<%int count = 0;
	   foreach (var shipping in Model.OtherShippings)
	   {%>
					<tr>
						<td>
							<%=++count%>
						</td>
						<td>
							<%=shipping.CustId%>
						</td>
						<td>
							<%=shipping.ShipContactor%>
						</td>
						<td>
							<%=shipping.DisplayShipPhoneNo%>
						</td>
						<td>
							<%=shipping.Address1 %><br />
							<%=shipping.Address2%><br />
							<%=shipping.Address3%><br />
							<%=shipping.Address4%>
						</td>
						<td>
							<%=Html.ActionLink(Html._("Button.Edit"), "GetShipping", "Admin", new { custId = shipping.CustId, shipId = shipping.ShipID }, null)%>
						</td>
					</tr>
					<%} %>
				</table>
			</fieldset>
		</div>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
