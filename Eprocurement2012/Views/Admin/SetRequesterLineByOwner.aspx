﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SetRequesterLine>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<%Html.BeginForm("SetRequesterLineByOwner", "Admin", FormMethod.Post); %>
		<h2 id="page-heading">
			<%=Html._("Admin.SetRequesterLine")%></h2>
		<fieldset>
			<legend>
				<%=Html._("Admin.CostCenterInfo")%></legend>
			<p>
				<%=Html._("SetRequesterLine.CoscenterId")%>
				:
				<%=Model.CostCenter.CostCenterID %>
				<input id="costcenterId" type="hidden" name="costcenterId" value="<%=Model.CostCenter.CostCenterID %>" />
				<input id="companyId" type="hidden" name="companyId" value="<%=Model.CompanyId %>" />
			</p>
			<p>
				<%=Html._("SetRequesterLine.CoscenterName")%>
				:
				<%=Model.CostCenter.CostCenterName %></p>
			<p>
				<%=Html._("SetRequesterLine.Requester")%>
				:
				<%=Model.UserReqDetail.DisplayNameAndEmail %>
				<input id="userRequester" type="hidden" name="userRequester" value="<%=Model.UserReqDetail.UserId %>" /></p>
		</fieldset>
		<div class="content">
			<% if (Model.UserApprover != null)
	  { %>
			<div style="float: left; width: 75%">
				<h3>
					<%=Html._("SetRequesterLine.Approver")%></h3>
				<hr />
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel1")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[0] != null && Model.SelectApprover[0] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[0] == null ? 0 : Model.SelectBudget[0]%>"
						id="UserApprover_First_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[0] != null && Model.SelectParkday[0] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<%=Html._("Admin.SetRequsterLine.Day")%>
				</div>
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel2")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[1] != null && Model.SelectApprover[1] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[1] == null ? 0 : Model.SelectBudget[1]%>"
						id="UserApprover_Second_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[1] != null && Model.SelectParkday[1] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<span>
						<%=Html._("Admin.SetRequsterLine.Day")%></span>
				</div>
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel3")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[2] != null && Model.SelectApprover[2] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[2] == null ? 0 : Model.SelectBudget[2]%>"
						id="UserApprover_Third_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[2] != null && Model.SelectParkday[2] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<%=Html._("Admin.SetRequsterLine.Day")%>
				</div>
			</div>
			<%} %>
		</div>
		<div class="grid_8">
			<p class="btn">
				<input id="SetRequesterLine" type="submit" value="<%=Html._("Button.Save")%>" />
				<%if (String.IsNullOrEmpty(TempData["ErrorResetReqLine"].ToString()) && String.IsNullOrEmpty(TempData["Error"].ToString()))
	  {%>
				<input id="DeleteRequesterLine" name="DeleteRequesterLine" type="submit" value="<%=Html._("Button.DeleteRequesterLine")%>" />
				<%} %>
				<input id="SetRequesterLineBack" name="SetRequesterLineBack" type="submit" value="<%=Html._("Button.Back")%>" />
				<span style="color: Red">
					<%=TempData["Error"]%>
				</span><span style="color: Blue">
					<%=TempData["Success"]%>
				</span><span style="color: Blue">
					<%=TempData["DeleteReqLine"]%>
				</span>
			</p>
			<div>
				<span style="color: Red">
					<%=TempData["ErrorResetReqLine"]%>
				</span>
			</div>
			<%if (String.IsNullOrEmpty(TempData["ErrorResetReqLine"].ToString()) && String.IsNullOrEmpty(TempData["Error"].ToString()))
	 {%>
			<div>
				<span>
					<%=Html._("Content.ResetApprover")%></span>
			</div>
			<%} %>
			<br />
		</div>
		<% Html.EndForm(); %>
		<br />
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			var checknum = function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
								 event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
								(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			};

			$("#UserApprover_First_Budget").keydown(checknum);
			$("#UserApprover_Second_Budget").keydown(checknum);
			$("#UserApprover_Third_Budget").keydown(checknum);
		});
	</script>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
