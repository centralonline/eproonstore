﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SearchOrder>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<div class="grid_9" style="float: right; margin: 20px 500px;">
			<h3 style="font-size: 13px; font-weight: bold; margin: 8px 0; padding: 5px; text-shadow: 1px 1px 1px white;
				border: solid 1px #C5C5C5; border-radius: 2px; -moz-border-radius: 2px; background: url(../images/theme/bg-pattern-h3.jpg) repeat-x;
				height: 20px; color: #7C7C7C;">
				<img src="../../images/theme/h3-list-icon.png" alt="SearchData" style="vertical-align: middle;" /><%=Html._("Order.ViewSearchDataOrder.Head")%></h3>
			<%Html.RenderPartial("SearchOrderData", Model.Orders); %>
		</div>
	</div>
</asp:Content>
