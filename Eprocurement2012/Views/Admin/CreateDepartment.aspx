﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CompanyDepartment>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
<script type="text/javascript">
	$(function () {
		$("#DepartmentID").keypress(function (e) {
			var key = String.fromCharCode(e.which);
			if (isLetter(key) == null) {
				return false;
			}
		});
	});
	function isLetter(s) {
		//return s.match("^[a-zA-Z0-9\(\)]+$");
		return s.match("^[A-Za-z0-9_\-]*$");
	}
</script>

	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16"><br />
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "ViewAllDepartment", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Next >>", "CostCenterGuide", "Admin", null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.Department.ContentCreate")%></h2>
	</div>
	<div class="grid_16">
		<div class="grid_8">
			<p class="btn" style="float: right">
				<%=Html.ActionLink(Html._("Admin.Department.ViewData"), "ViewAllDepartment", "Admin")%></p>
			<h5>
				<%=Html._("Admin.Department.Create")%></h5>
			<div class="box" style="margin-top: 15px">
				<%Html.BeginForm("CreateDepartment", "Admin", FormMethod.Post);%>
				<div class="table-format">
					<%=Html.HiddenFor(m=>m.CompanyId)%>
					<label>
						<%=Html._("Admin.Department.DeptID")%></label>
					<%=Html.TextBoxFor(m=>m.DepartmentID, new { maxlength = 8 ,@id="DepartmentID"})%>
					<span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.DepartmentID)%></span>
				</div>
				<div style="margin-left: 23%; margin-bottom: 5px;">
						<label>
							<%=Html._("Admin.CostCenter.ContentCostCenterID")%></label>
					</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.Department.ThaiName")%></label>
					<%=Html.TextBoxFor(m=>m.DepartmentThaiName)%>
					<span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.DepartmentThaiName)%></span>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.Department.EngName")%></label>
					<%=Html.TextBoxFor(m=>m.DepartmentEngName)%>
					<span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.DepartmentEngName)%></span>
				</div>
				<div style="padding-left: 176px; margin-bottom: 5px">
					<input type="submit" id="SaveNewDepartment" name="SaveNewDepartment.x" value="<%=Html._("Button.Save")%>" /></div>
				<%Html.EndForm(); %>
			</div>
		</div>
	</div>
	<div style="min-height: 550px;">
	</div>
	<%--<div class="grid_8">
		<p class="btn">
			<%=Html.ActionLink("Back", "ViewAllDepartment", "Admin")%></p>
	</div>
	<div class="grid_8">
		<p class="btn" style="float: right">
			<%=Html.ActionLink("Continue", "CostCenterGuide", "Admin")%></p>
	</div>--%>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
