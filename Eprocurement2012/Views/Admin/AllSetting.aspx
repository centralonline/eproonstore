﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.IMasterPageData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("SubMenuAdmin"); %>
	<div class="grid_16">
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<p class="btn" style="float: right">
				<%=Html.ActionLink("<< Previous", "FinalReview", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<p class="btn" style="float: left;">
				<%=Html.ActionLink("Home", "CompanyHome", "Admin",null, new { @style = "width:75px;" })%></p>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.AllSetting.Setting")%></h2>
	</div>
	<div class="grid_16">
		<%--<h6>
			<%=Html._("Admin.AllSetting.CompanySetup")%></h6>--%>
		<br />
		<div class="grid_8">
			<img alt="" src="/images/icon/bank.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.FinalReview"), "FinalReview", "Admin")%><br />
			<br />
			<img alt="" src="/images/icon/bank.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.CompanyInfo"), "CompanyInformation", "Admin")%><br />
			<br />
			<img alt="" src="/images/icon/photography.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.CompanyLogo"), "CompanyLogo", "Admin")%><br />
			<br />
			<%if (Model.User.Company.IsCompModelThreeLevel)
	 {%>
			<img alt="" src="/images/icon/category.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.Departments"), "ViewAllDepartment", "Admin")%><br />
			<br />
			<%} %>
			<img alt="" src="/images/icon/category.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.CostCenters"),"ViewAllcostCenter","Admin")%><br />
			<br />
			<img alt="" src="/images/icon/user.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.Users"),"ViewAllUser","Admin")%><br />
			<br />
			<img alt="" src="/images/icon/role.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.RoleUsers"), "ViewAllUser", "Admin")%><br />
			<br />
			<img alt="" src="/images/icon/role.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.RequesterLinesAndApprovals"), "ViewAllCostCenter", "Admin")%><br />
			<br />
			<img alt="" src="/images/icon/config.png" />&nbsp;&nbsp;<%=Html.ActionLink(Html._("Admin.AllSetting.CompanySetting"), "CompanySetting", "Admin")%><br />
			<br />
		</div>
	</div>
	<br />
	<div style="min-height: 500px;">
	</div>
	<!--Lin's min-height-->
</asp:Content>
