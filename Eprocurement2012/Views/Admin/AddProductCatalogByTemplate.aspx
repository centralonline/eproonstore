﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Admin/Admin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ProductTemplateData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Admin.ViewProductCatalogAddByTemplate.Title")%></h2>
		<div>
			<%=Html._("Admin.ViewProductCatalogAddByTemplate.Content")%>
		</div>

		<%if (Model.ProductTemplate.Count() > 0)
		{ %>
		<%Html.BeginForm("GetDatailProductTemplate", "Admin", FormMethod.Post); %>
		<div id="content-shopping-cart-detail">
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
				border: solid 1px #c5c5c5">
				<thead>
					<tr>
						<th>
						</th>
						<th>
							<%=Html._("ProductCatalog.HeadTemplateId")%>
						</th>
						<th>
							<%=Html._("ProductCatalog.HeadTemplatetotalProduct")%>
						</th>
					</tr>
				</thead>
				<%foreach (var itemTemplate in Model.ProductTemplate)
				{ %>
				<tr>
					<td>
						<input id="<%=itemTemplate.TemplateId %>" name="templateId" value="<%=itemTemplate.TemplateId %>"
							type="radio" />
					</td>
					<td>
						<%--<%=itemTemplate.TemplateId %>--%>
						<%=itemTemplate.DisplayTemplate %>
					</td>
					<td>
						<%=itemTemplate.TotalProduct %>
					</td>
				</tr>
				<%} %>
			</table>
			<input id="SubmitDetail" type="submit" value="<%=Html._("ProductCatalog.ButtonListProduct")%>" />
			<label class="errormessage" style="color: Red;">
				<%=TempData["Error"]%></label>
		</div>
		<%Html.EndForm(); %>
		<%} %>
		<%else
		{ %>
		<%=Html._("Admin.ViewProductCatalogAddByTemplate.NoData")%>
		<%} %>
		<%if (Model.ItemProduct != null && Model.ItemProduct.Count() > 0)
		{ %>
		<%Html.BeginForm("AddProductCatalogByTemplate", "Admin", FormMethod.Post); %>
		<input id="templateId" type="hidden" value="<%=Model.CurrentTemplateId %>" name="templateId" />
		<div>
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
				border: solid 1px #c5c5c5">
				<tr>
					<th width="40" class="thead">
						<input id="checkall" type="checkbox" />
						<label for="checkall">
							<%=Html._("ProductCatalog.HeadCheckAll")%></label>
					</th>
					<th width="40" class="thead" style="text-align: center">
						<%=Html._("ProductCatalog.HeadProductId")%>
					</th>
					<th width="80" class="thead" style="text-align: center">
						<%=Html._("ProductCatalog.HeadProductName")%>
					</th>
					<th width="40" class="thead" style="text-align: center">
						<%=Html._("ProductCatalog.HeadProductPrice")%>
					</th>
					<th width="40" class="thead" style="text-align: center">
						<%=Html._("ProductCatalog.HeadProductUnit")%>
					</th>
				</tr>
				<%foreach (var item in Model.ItemProduct)
				{ %>
				<tr>
					<td>
						<input id="chk<%=item.Id%>" name="listproductId" type="checkbox" value="<%=item.Id%>" />
					</td>
					<td style="text-align: center">
						<%=item.Id%>
					</td>
					<td>
						<%=item.Name %>
						<input name="productId" type="hidden" value="<%=item.Id%>" />
					</td>
					<td style="text-align:right">
						<%=item.PriceIncVat.ToString(new MoneyFormat())%>
					</td>
					<td style="text-align: center">
						<%=item.Unit %>
					</td>
					
				</tr>
				<%} %>
			</table>
		</div>
		<div class="by-ProductID">
			<input type="submit" name="AddToCartByPId" value="<%=Html._("ProductCatalog.ButtonGoToSetProductCatalog")%>" />
			<%=TempData["Error"]%>
			<a href="#" style="color: Gray; margin-left: 3px; line-height: 30px; float: right;">
				back to top</a>
		</div>
		<%Html.EndForm(); %>
		<%} %>
		<%else
		{ %>
		<%=Html._("ProductCatalog.Emptry")%>
		<%} %>
	</div>
	<div style="min-height: 500px;">
	</div>
	<script type="text/javascript">
		$('#checkall').click(function () {
			var check = this.checked;
			$(':checkbox').prop('checked', check);
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
