﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.SubMenuAdmin" %>
<div class="grid_16">
	<ul class="nav subnav">
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "CompanyHome")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.Home"), "CompanyHome", "Admin")%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "CompanyGuide", "CompanyInformation", "ViewShippingInformation", "CompanyLogo", "CompanySetting", "CompanyInfoReview")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.CompanyInfo"), "CompanyGuide", "Admin")%></li>
		<%if (Model.User.Company.IsCompModelThreeLevel)
		{%>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "DepartmentGuide", "ViewAllDepartment", "EditDepartment", "CreateDepartment")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.Department"), "DepartmentGuide", "Admin")%></li>
		<%} %>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "CostCenterGuide", "ViewAllCostCenter", "EditCostCenter", "CreateCostCenter")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.CostCenter"), "CostCenterGuide", "Admin")%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "UserGuide", "ViewAllUser", "ViewAllAdmin", "ManageUser", "ViewUserDetail", "EditUser", "CreateNewUser", "SetRequesterLineForUser", "LearnAboutApproval", "UserFinalReview")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.UserInfo"), "UserGuide", "Admin")%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "RequesterLineGuide", "SetRequesterLine")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.RequesterLine"), "RequesterLineGuide", "Admin")%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "FinalReview")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.FinalReview"), "FinalReview", "Admin")%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "AllSetting")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.AllSetting"), "AllSetting", "Admin")%></li>
	</ul>
</div>
<style type="text/css">
	ul.subnav li.active a
	{
		background-color: #e17c00;
	}
	ul.nav li a.show-sub-menu
	{
		color: #fff;
		background: #fd8c00;
		cursor: default;
		font-weight: bold;
		border-top: solid 1px #e88000;
	}
</style>
