﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.NotifyMeData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%=Html.Partial("NotifyMe", Model)%>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content5" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
