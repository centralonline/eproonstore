﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ProductData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
		});
	</script>
	<div>
		<%Html.RenderPartial("BreadCrumb", Model.BreadCrumb);%>
	</div>
	<%Html.BeginForm("HandleBuy", "Product", new { id = "handleBuy", defaultbutton = "AddToCart" }, FormMethod.Post, new { @id = "handleBuy" });%>
	<div id="content-product">
		<table class="table-layout">
			<tr>
				<td id="left-cols" class="style1">
					<div id="content-product-summary-picture">
						<div>
							<img id="productImage" src="<%=Model.ProductDetail.ImageLargeUrl %>" width="400"
								height="400" alt="รูปสินค้า <%= Html.Encode(Model.ProductDetail.Name)%>" title="<%= Html.Encode(Model.ProductDetail.Name)%>" />
						</div>
					</div>
				</td>
				<td id="center-cols" class="style2">
					<div id="content-product-summary-main" style="margin-bottom: 8px;">
						<h1 style="color: #4479A3;">
							<%=Html.Encode(Model.ProductDetail.Name) %></h1>
						<span id="product-id">
							<%=Html._("Product.Detail.ProductId")%>
							:</span><%=Html.Encode(Model.ProductDetail.Id)%>
						<% 
							if ((Model.ProductDetail.FullPriceIncVat > 0) && (Model.ProductDetail.FullPriceIncVat > Model.ProductDetail.PriceIncVat))
							{
						%>
						<span id="normal-price">
							<%=Html._("Product.Detail.NormalPrice")%>
							:</span><i><strike><%=Model.ProductDetail.FullPriceIncVat.ToString(new MoneyFormat())%></strike></i>
						<%=Html._("Product.Detail.Baht")%>
						/
						<%=Model.ProductDetail.Unit%><br />
						<span class="buy-trendyday">
							<%=Html._("Product.Detail.Price")%>
							:</span><h2>
								<%=Model.ProductDetail.DisplayPrice%>
								<%=Html._("Product.Detail.Baht")%>
							</h2>
						/
						<%=Model.ProductDetail.Unit%><br />
						<span id="save">
							<%=Html._("Product.Detail.Discount")%>
							:</span><%=Model.ProductDetail.Discount.ToString(new MoneyFormat()) %>
						<%=Html._("Product.Detail.Baht")%>
						(<%=Model.ProductDetail.DiscountPercent.ToString("#.##") %>%)
						<%
							}
							else
							{
						%>
						<span class="buy-trendyday">
							<%=Html._("Product.Detail.Price")%>
							:</span><h2 style="display: inline; font-weight: bold; font-size: 12px; padding: 0px margin:0px;">
								<%=Model.ProductDetail.DisplayPrice%>
								<%=Html._("Product.Detail.Baht")%></h2>
						/
						<%=Model.ProductDetail.Unit%>
						<% 
							}
						%>
						<%
							if (!Model.ProductDetail.IsPG)
							{
						%>
						<%	if (Model.ProductDetail.TransCharge > 0 && Model.User.Company.IsDefaultDeliCharge)
			{%>
						<p>
							(<%=Html._("Product.Detail.TransCharge")%>
							<%=Model.ProductDetail.TransCharge.ToString(new MoneyFormat()) %>
							<%=Html._("Product.Detail.Baht")%>
							/<%=Model.ProductDetail.Unit %>) <a href="/help/AdminInstruction" target='_blank'>
								<%=Html._("Product.Detail.LearnMore")%></a>
						</p>
						<%
			}
						%>
						<div style="font-weight: bold; font-size: 10px; color: #999999; padding-bottom: 10px;">
							*<%=Html._("Product.Detail.PriceIncVat")%></div>
						<%
							}
						%>
						<%= Html.Hidden("TransCharge",Model.ProductDetail.TransCharge) %>
						<%= Html.Hidden("Id", Model.ProductDetail.Id)%>
						<%= Html.Hidden("CodeId", Model.ProductDetail.CodeId)%>
						<%= Html.Hidden("IsPG", Model.ProductDetail.IsPG)%>
						<%=Html.Hidden("Paths",Model.ProductDetail.Paths) %>
						<%=Html.Hidden("BrandId",Model.ProductDetail.BrandId) %>
						<%=Html.Hidden("DeptId",Model.ProductDetail.DeptId) %>
						<%
							if (Model.ProductSku.Count() > 1)
							{
						%>
						<div class="extendVariationSelectionBox boxcontent-border">
							<div id="select-product-error" class="field-validation-error" style="font-weight: bolder;">
								<%=TempData["SelectItemError"]%>
							</div>
							<div id="select-product-notify-info" class="field-validation-error" style="font-weight: bolder;
								display: none;">
								<%=Html._("Product.Detail.Select")%>...</div>
							<select id="ChildVariationPID" name="ChildVariationPID" style="width: 300px;">
								<%
								if (Model.ProductDetail.IsPG)
								{
								%>
								<option value="">
									<%=Html._("Product.Detail.SelectProduct")%>
								</option>
								<%
								}
								%>
								<%
								foreach (var item in Model.ProductSku)
								{
									string selectPid = "";
									if (item.Id == Model.ProductDetail.Id)
									{
										selectPid = "selected='selected'";
									}
								%>
								<option value="<%=item.Id %>" <%=selectPid %>>
									<%=item.Name%>
									<%=item.DisplayPrice %>฿</option>
								<%	
								}
								%>
							</select>
							<input class="btn-update-page right" type="submit" name="UpdatePage.x" value="Update" />
						</div>
						<% 
							}
						%>
						<!--Promotion Text -->
						<%
							if (!String.IsNullOrEmpty(Model.ProductDetail.PromotionText))
							{
						%>
						<p class="description" style="margin-bottom: 15px;">
							<strong style="background-color: #339966; color: White; padding: 2px 5px;">
								<%=Html._("Product.Detail.Special")%></strong><br />
							<%=Model.ProductDetail.PromotionText %></p>
						<%
							}
						%>
						<!--Icon Promotion -->
						<%
							if (Model.ProductDetail.IsPromotion)
							{
						%>
						<%
							}			
						%>
						<!--BestDeal-->
						<p class="status color-deal">
							<%=Model.ProductDetail.DisplayBestDeal %></p>
						<!-- Display Stock Text-->
						<p class="status color-stock">
							<%=Model.ProductDetail.DisplayStatusText%></p>
						<p class="status color-stock">
							<%=Model.ProductDetail.IsContactForDelivery ? "<br /><span style='color:Red'>* สินค้ามีค่าจัดส่ง กรุณาติดต่อเจ้าหน้าที่ ที่เบอร์. 027395555</span>" : String.Empty%>
						</p>
						<%
							if (Model.ProductDetail.Status == Eprocurement2012.Models.ProductStatusType.OutOfStock)
							{
						%>
						<%if (Model.User.IsRequester)
		{
						%>
						<input type="submit" value="" name="NotifyMe.x" id="notifyme" class="forbtn btn-product-notifyme" />
						<%
		}%>
						<br />
						<%
							} 
						%>
						<br class="clear" />
						<!-- Display Product Detail -->
						<p>
							<%=Html._("Product.Detail.ProductId")%>
							: <span style="font-weight: bold;">
								<%=Model.ProductDetail.Id %></span></p>
						<p>
							<%=Model.ProductDetail.Name %></p>
						<div id="content-shopping-cart-detail">
							<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
								border: solid 1px #c5c5c5; text-align: center; margin-top: 10px;">
								<tr>
									<td class="thead">
										<%=Html._("Product.Detail.ProductId")%>
									</td>
									<td class="thead">
										<%=Html._("Product.Detail.PriceExcVat")%>
									</td>
									<%if (Model.ProductDetail.TransCharge > 0 && Model.User.Company.IsDefaultDeliCharge)
		   { %>
									<td class="thead">
										<%=Html._("Product.Detail.TotalDeliveryFee")%>
									</td>
									<%} %>
									<td class="thead">
										<%=Html._("Product.Detail.DiscountPercent")%>
									</td>
									<td class="thead">
										<%=Html._("Product.Detail.IsVat")%>
									</td>
									<td class="thead">
										<%=Html._("Product.Detail.TotalPriceProductExcVatWithDisCountAmount")%>
									</td>
									<td class="thead">
										<%=Html._("Product.Detail.Unit")%>
									</td>
								</tr>
								<tr>
									<td>
										<%=Model.ProductDetail.Id %>
									</td>
									<td>
										<%=Model.ProductDetail.PriceExcVat.ToString(new MoneyFormat()) %>
									</td>
									<%if (Model.ProductDetail.TransCharge > 0 && Model.User.Company.IsDefaultDeliCharge)
		   { %>
									<td>
										<%=Model.ProductDetail.TransCharge.ToString(new MoneyFormat()) %>
									</td>
									<%} %>
									<td>
										<%if (Model.ProductDetail.IsBestDeal || Model.ProductDetail.IsPromotion)
			{ %>
										0
										<%}
			else
			{%>
										<%=Model.User.Company.CompanyDisCountRate %>
										<%} %>
									</td>
									<td>
										<%=(Model.ProductDetail.PriceIncVat - Model.ProductDetail.PriceExcVat).ToString(new MoneyFormat()) %>
									</td>
									<td>
										<%=Model.ProductDetail.DisplayPrice%>
									</td>
									<td>
										<%=Model.ProductDetail.Unit %>
									</td>
								</tr>
							</table>
						</div>
					</div>
				</td>
				<td>
					<div class="RightColumn" style="margin-right: 10px;">
						<%
							if (Model.ProductDetail.IsDisplayBuyButton && Model.User.IsRequester)
							{
						%>
						<h3 class="heading" style="text-align: center; color: White; padding-left: 20px;
							margin: 0; padding-top: 10px;">
							<%=Html._("Product.Detail.NowOrder")%></h3>
						<div class="contentbox">
							<div style="margin-left: 30px;">
								<label>
									<%=Html._("Repurchase.MyProductQuantity")%></label>
								<input id="Qty" name="Qty" class="qty input-qty" type="text" maxlength="4" value="1"
									style="margin-top: 6px; width: 40px;" />
							</div>
							<div style="text-align: center;">
								<input type="image" name="AddToCart" value="AddToCart" class="default" src="/images/theme/button-addTocart.png" />
							</div>
							<!---Start Add to Catalog -->
							<div style="text-align: center;">
								<label>
									หรือ</label>
							</div>
							<div style="text-align: center;">
								<%
								if (Model.MyCatalogs.Any())
								{
								%>
									<%=Html.DropDownList("catalogGUID", Model.MyCatalogs, new { style = "width:170px; margin-bottom:5px;", @class = "catalog" })%>
								<%
								}
								%>
								<input type="image" name="AddToWishList" value="AddToWishList" src="/images/btn/btn_add_to_wish_list.png" />
							</div>
							<!---End---->
						</div>
						<%} %>
					</div>
				</td>
			</tr>
		</table>
		<br class="clear" />
		<% 
			if (!String.IsNullOrEmpty(Model.ProductDetail.Description))
			{
		%>
		<div id="content-product-description" style="margin: 10px; font: normal 13px Tahoma, Helvetica, sans-serif;">
			<h3>
				<%=Html._("Product.Detail.Description")%></h3>
			<p style="width: 95%; margin: 10px auto; font: 13px/18px tahoma; color: #676767">
				<%Html.FormatTextile(Model.ProductDetail.Description.ToString()); %></p>
		</div>
		<% 
			}
		%>
		<!--Relate Product-->
		<%
			if (Model.ProductRelated.Any())
			{
		%>
		<div id="relate-product" style="margin: 10px;">
			<h3>
				<%=Html._("Product.Detail.ProductRelated")%>
			</h3>
			<ul>
				<%
				foreach (var product in Model.ProductRelated)
				{
					Html.RenderPartial("Product", product);
				}
				%>
			</ul>
		</div>
		<% 
			}
			if (Model.ProductHistory.Any())
			{
		%>
		<div id="product-history">
			<h3 style="background-color: #FFFFFF;">
				<%=Html._("Product.Detail.ProductHistory")%></h3>
			<ul>
				<%
				foreach (var product in Model.ProductHistory)
				{
					Html.RenderPartial("Product", product);
				}
				%></ul>
		</div>
		<%
			}
		%>
	</div>
	<%Html.EndForm(); %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<link href="/css/cloud-zoom.css" rel="stylesheet" type="text/css" />
	<link href="/css/botton.css" rel="stylesheet" type="text/css" />
	<style type="text/css">
		#content-product-Question textarea
		{
			margin-left: 0px;
		}
		#dialog-background
		{
			position: absolute;
			top: 0;
			left: 0;
			min-height: 100%;
			width: 100%;
			background: Black;
			z-index: 2;
			filter: alpha(opacity=60);
			opacity: 0.6;
		}
		#dialog-content
		{
			z-index: 3;
			position: absolute;
			min-width: 500px;
			max-height: 500px;
			overflow: auto;
			top: 0;
			left: 0;
			border-bottom-color: #e6e6e6;
			border-bottom-left-radius: 8px;
			border-bottom-right-radius: 8px;
			border-bottom-style: solid;
			border-bottom-width: 6px;
			border-left-color: #e6e6e6;
			border-left-style: solid;
			border-left-width: 6px;
			border-right-color: #e6e6e6;
			border-right-style: solid;
			border-right-width: 6px;
			border-top-color: #e6e6e6;
			border-top-left-radius: 8px;
			border-top-right-radius: 8px;
			border-top-style: solid;
			border-top-width: 6px;
			padding: 10px;
			-moz-border-radius: 6px;
			font: normal 13px/16px tahoma;
			color: #676767;
			background-color: #eaeaea;
		}
		#dialog-loading
		{
			position: absolute;
			top: 0;
			left: 0;
			width: 220px;
			height: 19px;
			background: url('/images/icon_loader.gif') no-repeat center;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script src="/js/cloud-zoom.1.0.2.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function () {
			//////////NotifyMe

			$("#notifyme").click(function (event) {
				event.preventDefault();
				createNotifyMeDialog();
				showNotifyMeDialog(this, "&NotifyMe.x=");
			});
		});

		//////NotifyMe Dialog Box
		function createNotifyMeDialog() {
			var dialog = $("<div>", { id: "dialog" }).hide();
			var dialog_background = $("<div>", { id: "dialog-background" });
			var dialog_loading = $("<div>", { id: "dialog-loading" });
			var dialog_content = $("<div>", { id: "dialog-content" });
			dialog.append(dialog_background);
			dialog.append(dialog_loading);
			dialog.append(dialog_content);
			$("body").append(dialog);
		}

		var dialogPadding = 50;

		function showNotifyMeDialog(sender, btnName) {
			var documentHeight = $(document).height();
			$("#dialog-background").css("height", documentHeight);

			var dialog_loading = $("#dialog-loading");
			dialog_loading.show();
			$("#dialog").show();

			var dialog_content = $("#dialog-content");
			dialog_content.hide();

			var windowHeight = $(window).height();
			var windowWidth = $(window).width();

			var loadingTop = ((windowHeight - dialog_loading.height()) / 2) + $(window).scrollTop();
			var loadingLeft = (windowWidth - dialog_loading.width()) / 2;

			if (loadingTop > documentHeight - dialog_loading.height() - dialogPadding) {
				loadingTop = documentHeight - dialog_loading.height() - dialogPadding;
			}
			if (loadingTop < dialogPadding) {
				loadingTop = dialogPadding;
			}

			if (loadingLeft < dialogPadding) {
				loadingLeft = dialogPadding;
			}

			dialog_loading
			.css("top", loadingTop)
			.css("left", loadingLeft);

			$.ajax({ url: '<%=Url.Action("HandleBuy", "Product") %>',
				type: "POST",
				data: $("#handleBuy").serialize() + btnName,
				success: function (data) {
					dialog_content.html(data);
					dialog_content.show();

					var contentTop = ((windowHeight - dialog_content.height()) / 2) + $(window).scrollTop();
					var contentLeft = (windowWidth - dialog_content.width()) / 2;

					dialog_content
					.css("top", contentTop)
					.css("left", contentLeft);
				}
			});
		}

		function hideNotifyMeDialog() {
			$("#dialog").hide();
		}

		function cancel(event) {
			event.preventDefault();
			hideNotifyMeDialog();
		}
	</script>
</asp:Content>
