﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.NotifyMeData>" %>
<div>
	<%Html.BeginForm("NotifyMe", "Product", FormMethod.Post, new { id = "notifydata" }); %>
	<fieldset>
		<legend>
			<%=Html._("OFMAdmin.ViewOrderDetail.HeadRemark")%></legend>
		<br />
		<table>
			<tr>
				<th style="height: 30px; text-align: right;">
					<span>
						<%=Html._("Account.ForgotPassword.Mail")%>
					</span><span class="errormessage">*</span>
				</th>
				<td>
					<%=Html.TextBoxFor(m => m.NotifyMeEmail)%><span class="errormessage" id="mailerror"><%=Html.ValidationMessageFor(m => m.NotifyMeEmail)%></span><span>
						<%=Html._("Product.NotifyMe.Quantity")%>: </span>
					<input type="text" name="Qty" value="1" maxlength="4" size="4" style="width: 20px;" />
				</td>
			</tr>
			<tr>
				<th style="height: 30px; text-align: right; padding-right: 15px;">
					<span>
						<%=Html._("Product.NotifyMe.Remark")%>
						:</span>
				</th>
				<td>
					<%=Html.TextBox("NotifyRemark", Model.NotifyRemark)%>
				</td>
			</tr>
		</table>
		<br />
		<%=Html.Hidden("ProductID", Model.ProductID, new {@id = "pId" })%>
		<span id="notify-btn">
			<input type="submit" value="" id="submit" name="submit.x" class="forbtn btn-submit-gray" />
			<input type="submit" value="" id="cancel" name="cancel.x" class="forbtn btn-cancel-gray" />
		</span>
		<br />
	</fieldset>
	<%Html.EndForm(); %>
</div>
<script type="text/javascript">
	$(function () {
		$('#cancel').click(cancel);
		$('#submit').click(notifysubmit);
	});

	function notifysubmit(event) {
		event.preventDefault();
		$('#notify-btn').hide();
		var mailformat = new RegExp("^[a-zA-Z0-9_\\+-]+(\\.[a-zA-Z0-9_\\+-]+)*@[a-zA-Z0-9-]+(\\.[a-zA-Z0-9-]+)*\\.([a-zA-Z]{2,4})$");
		if (mailformat.test($('input[name="NotifyMeEmail"]').val())) {
			$.ajax({ url: '<%=Url.Action("NotifyMe", "Product") %>',
				type: "POST",
				data: $("#notifydata").serialize(),
				success: function (data) {
					$('#dialog-content').html('<div style="font-size:large;color:Red;height:100px;text-align:center"><br/><br/><img src="/images/product_email_alert.gif" alt="" width="460" height="45" /><br/><br/></div>')
					var t = setTimeout("hideNotifyMeDialog()", 3000);
				}
			})
		}
		else {
			alert("Error");
			$("#mailerror").text("Email ไม่อยู่ในรูปแบบที่ถูกต้องค่ะ");
			$('#notify-btn').show();

		}
	}; 
</script>
