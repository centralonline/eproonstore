﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.FinalReview>" %>

<div>
		<table>
			<tr>
				<th width="30">
					<%=Html._("Admin.Department.DeptID")%>
				</th>
				<th width="50">
					<%=Html._("Admin.CostCenter.DepartmentName")%>
				</th>
				<th>
					<%=Html._("Admin.CostCenter.CostCenterID")%>
				</th>
				<th width="70">
					<%=Html._("DataUserPermission.CostcenterName")%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.RequesterLogin")%>
				</th>
				<th width="120">
					<%=Html._("Admin.FinalReview.RequesterName")%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverLogin", Html._("Admin.FinalReview.Level1"))%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverName", Html._("Admin.FinalReview.Level1"))%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverCreditBudget", Html._("Admin.FinalReview.Level1"))%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverLogin", Html._("Admin.FinalReview.Level2"))%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverName", Html._("Admin.FinalReview.Level2"))%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverCreditBudget", Html._("Admin.FinalReview.Level2"))%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverLogin", Html._("Admin.FinalReview.Level3"))%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverName", Html._("Admin.FinalReview.Level3"))%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.ApproverCreditBudget", Html._("Admin.FinalReview.Level3"))%>
				</th>
			</tr>
			<%foreach (var costcenter in Model.RequesterLineDetail.GroupBy(c => c.CostcenterID))%>
			<%{ %>
			<% foreach (var user in costcenter.GroupBy(u => u.RequesterId)) %><%
	  { %>
			<tr>
				<td>
					<%=user.First().DepartmentID%>
				</td>
				<td>
					<%=user.First().DepartmentName%>
				</td>
				<td>
					<%=user.First().CostcenterID%>
				</td>
				<td>
					<%=user.First().CostcenterName%>
				</td>
				<td>
					<%=user.First().RequesterId%>
				</td>
				<td>
					<%=user.First().RequesterName%>
				</td>
				<td>
					<%=user.First(a => a.ApproverLevel == 1).ApproverId%>
				</td>
				<td>
					<%=user.First(a => a.ApproverLevel == 1).ApproverName%>
				</td>
				<td style="text-align: right">
					<%=user.First(a => a.ApproverLevel == 1).ApproverCreditBudget.ToString(new MoneyFormat())%>
				</td>
				<td>
					<%if (user.Any(a => a.ApproverLevel == 2))
	   { %>
					<%=user.First(a => a.ApproverLevel == 2).ApproverId%>
					<%} %>
				</td>
				<td>
					<%if (user.Any(a => a.ApproverLevel == 2))
	   { %>
					<%=user.First(a => a.ApproverLevel == 2).ApproverName%>
					<%} %>
				</td>
				<td style="text-align: right">
					<%if (user.Any(a => a.ApproverLevel == 2))
	   { %>
					<%=user.First(a => a.ApproverLevel == 2).ApproverCreditBudget.ToString(new MoneyFormat())%>
					<%} %>
				</td>
				<td>
					<%if (user.Any(a => a.ApproverLevel == 3))
	   { %>
					<%=user.First(a => a.ApproverLevel == 3).ApproverId%>
					<%} %>
				</td>
				<td>
					<%if (user.Any(a => a.ApproverLevel == 3))
	   { %>
					<%=user.First(a => a.ApproverLevel == 3).ApproverName%>
					<%} %>
				</td>
				<td style="text-align: right">
					<%if (user.Any(a => a.ApproverLevel == 3))
	   { %>
					<%=user.First(a => a.ApproverLevel == 3).ApproverCreditBudget.ToString(new MoneyFormat())%>
					<%} %>
				</td>
			</tr>
			<%} %>
			<%} %>
		</table>
	</div>
