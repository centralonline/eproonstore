﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Eprocurement2012.Models.Order>>" %>

<div>
<%if (Model.Count() > 0)
{ %>
	<%int count = 0; %>
	<table id="table-2">
		<tr>
			<th>
				<%=Html._("Admin.CompanyInfo.No")%>
			</th>
			<th>
				<%=Html._("Order.ViewDataOrder.OrderId")%>
			</th>
			<th>
				<%=Html._("Order.ViewDataOrder.OrderDate")%>
			</th>
			<th>
				<%=Html._("Order.ViewOrder.Requester")%>
			</th>
			<th>
				<%=Html._("Order.ViewOrder.Approver")%>
			</th>
			<th>
				<%=Html._("DataUserPermission.CostcenterName")%>
			</th>
			<th>
				<%=Html._("Order.ConfirmOrder.Quantity")%>
			</th>
			<th>
				<%=Html._("Order.ViewDataOrder.GrandTotalAmount")%>
			</th>
			<th>
				<%=Html._("Order.ViewDataOrder.OrderStatus")%>
			</th>
		</tr>
		<%foreach (var order in Model)
		{%>
		<tr>
			<td style="text-align: center">
				<%=++count%>
			</td>
			<td style="text-align: left">
				<%if (ViewContext.RouteData.Values["controller"].ToString() == "Admin")
				{%>
				<%=Html.ActionLink(order.OrderID, "ViewOrderDetail", "Admin", new { Id = order.OrderGuid }, null)%>
				<%} %>
				<%else
				{%>
				<%=Html.ActionLink(order.OrderID, "ViewOrderDetail", "Order", new { Id = order.OrderGuid }, null)%>
				<%} %>
			</td>
			<td style="text-align: left">
				<%=order.OrderDate.ToString(new DateTimeFormat())%>
			</td>
			<td style="text-align: left">
				<%=order.Requester.DisplayName%>
			</td>
			<td style="text-align: left">
				<%if (!string.IsNullOrEmpty(order.CurrentApprover.Approver.UserId))
				{ %>
				<%=order.CurrentApprover.Approver.DisplayName%>
				<%} %>
			</td>
			<td style="text-align: left">
				<%=order.CostCenter.CostCenterName%>
			</td>
			<td style="text-align: center">
				<%=order.ItemCountOrder%>
			</td>
			<td style="text-align: right">
				<%=order.GrandTotalAmt.ToString(new MoneyFormat())%>
			</td>
			<td style="text-align: center">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus." + order.Status.ToString())%>
			</td>
		</tr>
		<%} %>
	</table>
<%} %>
	<%else
	{%>
	<h3>
		<%=Html._("Order.ViewOrder.EmptryOrder")%></h3>
	<%} %>
	<style type="text/css">
		#table-2
		{
			border: 1px solid #e3e3e3;
			background-color: #f2f2f2;
			width: 100%;
			border-radius: 6px;
			-webkit-border-radius: 6px;
			-moz-border-radius: 6px;
		}
		#table-2 th
		{
			padding: 5px;
			color: #333;
			border: solid 1px #C5C5C5;
		}
		
		#table-2 td
		{
			line-height: 20px;
			font: normal 14px Tahoma; /*border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;*/
			border: solid 1px #C5C5C5;
		}
		#table-2 thead
		{
			padding: .2em 0 .2em .5em;
			text-align: center;
			color: #4B4B4B;
			background-color: #C8C8C8;
			background-image: -webkit-gradient(linear, left top, left bottom, from(#f2f2f2), to(#e3e3e3), color-stop(.6,#B3B3B3));
			background-image: -moz-linear-gradient(top, #D6D6D6, #B0B0B0, #B3B3B3 90%);
			border-bottom: solid 1px #999;
		}
		#table-2 th
		{
			background-color: #E6E6E6;
			font: bold 13px tahoma;
			padding: 5px;
			padding-left: 10px;
			text-align: center;
			width: 200px;
			border: solid 1px #C5C5C5;
		}
		#table-2 td
		{
			line-height: 20px;
			font: normal 13px Tahoma; /*border-bottom: 1px solid #fff;
	border-top: 1px solid #fff;*/
			border: solid 1px #C5C5C5;
			background-color: #FFFFFF;
			height: 30px;
			vertical-align: middle;
		}
		#table-2 td:hover
		{
			background-color: #fff;
			border: solid 1px #C5C5C5;
		}
	</style>
</div>
