﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Cart>" %>
<style type="text/css">
	.style1
	{
		width: 85px;
	}
</style>
<!--สั่งซื้อด้วยรหัส-->
<!--new code-->
<div class="centerwrap">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".quantity").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
							 event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
							(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});

			$("#showname").html("");
			$('#productId').keyup(function (e) {
				if ($(this).val().length == 7) {
					$.getJSON('<%=Url.FullUrlAction("SearchProductSKU","Product")%>', { pId: $(this).val() }, function (json) {
						$("#showname").html(json.ProductName);
					});
				}
			});
		}); 
	</script>
	<div class="step">
		<img src="/images/theme/Step-1.jpg" alt="step1" /></div>
	<div class="orderByCode">
		<img src="/images/theme/man.png" class="man" alt="man" />
		<div class="productCode GroupData">
			<h5>
				<%=Html._("CartDetailPartial.AddCartByProductId")%></h5>
			<div class="field">
				<%Html.BeginForm("AddToCartByProductId", "Revise", FormMethod.Post, new { id = "formAddToCartByProductId" }); %>
				<div class="by-ProductID">
					<label for="pID">
						<%=Html._("CartDetailPartial.ProductId")%>
						:</label>
					<input id="productId" name="productId" maxlength="7" type="text" /><span id="showname"></span>
				</div>
				<br clear="all" />
				<div class="by-ProductID">
					<label for="pQty">
						<%=Html._("CartDetailPartial.Quantity")%>
						:</label>
					<input id="quantity" name="quantity" class="quantity" maxlength="4" type="text" />
				</div>
				<br /><input id="Hidden1" name="refOrderId" type="hidden" value="<%=Model.RefOrderId %>" />
				<span style="margin: 10px 0px 0px 125px;">
					<input type="image" name="AddToCartByPId" value="AddToCartByPId" class="default"
						src="/images/theme/<%=Html._("CartDetailPartial.ButtonAddcart")%> " />
				</span>
				<span style="color: Blue">
					<%=TempData["ErrorAddProductByPid"]%></span>
				<%Html.EndForm();%>
			</div>
		</div>
	</div>
	<div class="eclear">
	</div>
</div>
<!--new code-->
<div class="center" style="width: 100%; margin-top: 10px;">
	<div class="eclear">
	</div>
	<!--GroupData-->
	<div class="GroupData">
		<h3>
			<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("CartDetailPartial.ItemInCart")%></h3>
	</div>
	<%if (Model.ItemCart != null && Model.ItemCart.Any())
	{%>
	<!--GroupData-->
	<div id="table-cart">
		<%Html.BeginForm("HandleAction", "Revise", FormMethod.Post);%>
		<input id="refOrderId" name="refOrderId" type="hidden" value="<%=Model.RefOrderId %>" />
		<div id="content-shopping-cart-detail">
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="border: solid 1px #c5c5c5;
				margin-top: 0px;">
				<tr>
					<td width="50" class="thead">
					</td>
					<td class="thead">
						<%=Html._("CartDetailPartial.ProductName")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("CartDetailPartial.Price")%>
						<br />
						<%=Html._("CartDetailPartial.IncVat")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("CartDetailPartial.Price")%>
						<br />
						<%=Html._("CartDetailPartial.ExcVat")%>
					</td>
					<td width="40" class="thead">
						<%=Html._("CartDetailPartial.Quantity")%>
					</td>
					<td width="40" class="thead">
						<%--<%=Html._("CartDetailPartial.Discount")%>--%>
						<%=Html._("CartDetailPartial.Unit")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("CartDetailPartial.NetAmt")%>
						<%--<br />
						<%=Html._("CartDetailPartial.IncVat")%>--%>
					</td>
				</tr>
				<%foreach (Eprocurement2012.Models.CartDetail Items in Model.ItemCart)
				{%>
				<tr>
					<td valign="top" align="center">
						<input name="DeleteCartItem" type="checkbox" value="<%=Html.Encode(Items.Product.Id)%>" />
					</td>
					<td valign="top">
						<img src="<%=Html.Encode(Items.Product.ImageSmallUrl) %>" width="50" height="50"
							alt="" style="background-color: #CCCCCC; margin: 10px; margin-top: 0;" class="left" />
						<strong style="color: #000000;">
							<%=Html._("CartDetailPartial.ProductId")%>
							:
							<%=Html.Encode(Items.Product.Id)%></strong><br />
						<div class="content-shopping-cart-table">
							<p class="productnameCart">
								<%=Html.ActionLink(Items.Product.Name, "Details", "Product", new { Items.Product.Id, title = Items.Product.Name }, null)%>
							</p>
							<%if (Items.IsOfmCatalog)
							{ %>
							<p>
								<%=Html._("CartDetailPartial.ProductNotInYourCatalog")%>
							</p>
							<%} %>
						</div>
					</td>
					<td valign="top">
						<div align="center">
							<%--<%=Html.Encode(Items.Product.PriceIncVat.ToString(new MoneyFormat()))%>--%>
							<%=Html.Encode(Items.ItemIncVatPrice.ToString(new MoneyFormat()))%>
						</div>
					</td>
					<td valign="top">
						<div align="center">
							<%--<%=Html.Encode(Items.Product.PriceExcVat.ToString(new MoneyFormat()))%>--%>
							<%=Html.Encode(Items.ItemExcVatPrice.ToString(new MoneyFormat()))%>
						</div>
					</td>
					<td valign="top">
						<div align="center">
							<%=Html.TextBox("Quantity", Items.Quantity, new { maxlength = 4, size = "4" })%><br />
							<%--<%=Html.Encode(Items.Product.Unit)%>--%>
							<%=TempData["QuantityErrorPid" + Items.Product.Id]%>
						</div>
					</td>
					<td valign="top">
						<div align="center">
						<%--<% = Items.ItemPriceDiscount.ToString(new MoneyFormat())%>--%>
							<%=Html.Encode(Items.Product.Unit)%>
						</div>
					</td>
					<td colspan="2" valign="top">
						<div align="center">
							<%=Html.Encode(Items.ItemPriceNet.ToString(new MoneyFormat()))%><%=Html.Hidden("ProductID", Items.Product.Id)%></div>
					</td>
				</tr>
				<%}%>
				<tr style="line-height: 25px">
					<td colspan="2">
						<p class="left" style="color: #a64a00">
							<%=Html._("CartDetailPartial.PleaseCalculate")%></p>
						<div align="right">
							<%=Html._("CartDetailPartial.NetAmt")%>&nbsp;</div>
					</td>
					<td valign="top">
						<div align="center">
						</div>
					</td>
					<td valign="top">
						<div align="center">
						</div>
					</td>
					<td valign="top">
						<div align="center">
							<b>
								<% = Html.Encode(Model.ItemCountCart.ToString())%></b><%=Html._("CartDetailPartial.Item")%></div>
					</td>
					<td valign="top">
						<div align="center">
							<%--<b><% = Html.Encode(Model.TotalAllDiscount.ToString(new MoneyFormat()))%></b>--%>
							<b><%=Html._("CartDetailPartial.Item")%></b>
						</div>
					</td>
					<td valign="top">
						<div align="center">
							<b>
								<% = Html.Encode(Model.TotalPriceProductWithDisCountAmount.ToString(new MoneyFormat()))%></b></div>
					</td>
				</tr>
			</table>
		</div>
		<div style="margin-top: 10px;">
			<%if (Model.ItemCart.Count() > 1)
			{ %><input name="DeleteCart" type="image" src="/images/theme/<%=Html._("CartDetailPartial.ButtonDelate")%>"
				alt="ลบสินค้าค่ะ" title="ลบสินค้าค่ะ" class="left" style="float: left;" />
			<%} %>
			<p>
				<%=TempData["DeleteError"]%>
			</p>
			<div style="width: 88px; margin-right: 0; float: right;">
				<input name="UpdateCart" type="image" src="/images/theme/<%=Html._("CartDetailPartial.ButtonCalculate")%>"
					alt="คำนวณราคาใหม่ค่ะ" title="คำนวณราคาใหม่ค่ะ" style="vertical-align: text-bottom" /></div>
		</div>
		<%Html.EndForm();%>
		<div style="width: 100%" class="clear">
			<div id="total" class=" margin-top-10 gray-m">
				<%if (Model.TotalDeliveryFee > 0)
				{ %>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalDeliveryFee")%></label>
					<span>
						<%=Html.Encode(Model.TotalDeliveryFee.ToString(new MoneyFormat()))%></span>&nbsp;&nbsp;&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<%} %>
				<%if (Model.TotalDeliveryCharge > 0)
				{%>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalDeliveryCharge")%></label>
					<span>
						<%=Html.Encode(Model.TotalDeliveryCharge.ToString(new MoneyFormat()))%></span>&nbsp;&nbsp;&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<%} %>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalPriceProductNoneVatWithDisCountAmount")%></label>
					<span>
						<%=Html.Encode(Model.TotalPriceProductNoneVatWithDisCountAmount.ToString(new MoneyFormat()))%></span>&nbsp;&nbsp;&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalPriceProductExcVatWithDisCountAmount")%></label>
					<span>
						<%=Html.Encode(Model.TotalPriceProductExcVatWithDisCountAmount.ToString(new MoneyFormat()))%></span>&nbsp;&nbsp;&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalVatAmt")%></label>
					<span>
						<%=Html.Encode(Model.TotalVatAmt)%></span>&nbsp;&nbsp;&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<div style="border-bottom: 1px solid #999999; width: 370px; float: right; margin-top: 5px;">
				</div>
				<p style="text-align: right; font-weight: bold;">
					<label>
						<%=Html._("CartDetailPartial.GrandTotalAmt")%></label>
					<span class="price" style="color: #CC0000; font-size: 13px; font-weight: bolder;">
						<%=Html.Encode(Model.GrandTotalAmt.ToString(new MoneyFormat()))%></span>&nbsp;&nbsp;&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
			</div>
			<br class="clear" />
		</div>
		<br class="clear" />
		<div class="margin-top-10">
		</div>
		<br class="clear" />
	</div>
	<br class="clear" />
	<%}%>
	<%else
   { %>
	<h1 style="text-align: center">
		<%=Html._("CartDetail.ErrorSelectedProduct")%></h1>
	<%} %>
</div>
