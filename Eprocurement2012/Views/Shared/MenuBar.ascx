﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div class="epro-menu-bar">
	<ul>
		<li class="menu-small">
			<%=Html.ActionLink("Home", "LogIn", "Account")%></li>
		<li class="menu-small">
			<%=Html.ActionLink("About us", "Introduction", "StaticPage")%></li>
		<li>
			<%=Html.ActionLink("Customers Reference", "CustomersReference", "StaticPage")%></li>
		<li>
			<%=Html.ActionLink("Privilege", "Privilege", "StaticPage")%></li>
		<li class="menu-small">
			<%=Html.ActionLink("Apply Now", "ApplyNow", "Account")%></li>
		<li>
			<%=Html.ActionLink("FAQ", "FAQ", "StaticPage")%></li>
	</ul>
</div>
