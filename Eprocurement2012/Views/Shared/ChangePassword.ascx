﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Password>" %>
<!--New design-->
<div class="positionApplylabel">
	<div class="applylabel">
		<label>
			<%=Html._("Shared.ChangePassword.OldPassword")%> <span style="color: Red">*</span></label>
	</div>
	<div class="leftFloat">
		<p class="applySpanGray">
			<input type="password" name="OldPassword" id="Password1" maxlength="60" /></p>
		<p class="applyAlert">
			<span style="color:Red"><%=Html.LocalizedValidationMessageFor(m => m.OldPassword)%></span></p>
	</div>
	<div class="eclear">
	</div>
	<div class="applylabel">
		<label>
			<%=Html._("Shared.ChangePassword.NewPassword")%> <span style="color:Red;">*</span></label>
	</div>
	<div class="leftFloat">
		<p class="applySpanGray">
			<input type="password" name="NewPassword" id="Password2" maxlength="60" /></p>
		<p class="applyAlert">
			<span style="color:Red"><%=Html.LocalizedValidationMessageFor(m => m.NewPassword)%><%=Html.LocalizedValidationMessageFor(m => m)%> </span></p>
	</div>
	<div class="eclear">
	</div>
	<div class="applylabel">
		<label>
			<%=Html._("Shared.ChangePassword.ConfirmNewPassword")%> <span style="color: Red">*</span></label>
	</div>
	<div class="leftFloat">
		<p class="applySpanGray">
			<input type="password" name="ConfirmNewPassword" id="Password3" maxlength="60" /></p>
		<p class="applyAlert">
			<span style="color:Red"><%=Html.LocalizedValidationMessageFor(m => m.ConfirmNewPassword)%></span></p>
	</div>
	<div class="eclear">
	</div>
	<div class="eclear">
	</div>
	<div class="positionBtnContact">
		<input class="graybutton" id="Save" type="submit" value="<%=Html._("Button.Save")%>" />
	</div>
	<div class="eclear">
	</div>
</div>
<!--New design-->
