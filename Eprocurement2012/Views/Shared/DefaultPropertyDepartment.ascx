﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.DepartmentViewData>" %>
<% if (Model.BrandFilter.Count() > 1)
   { %>
<div class="list-box">
	<h3>
		<%=Html._("Shared.DefaultPropertyDepartment.BrandFilter")%>
	</h3>
	<ul>
		<!-- Brand Filter -->
		<li class="icon-arrow"><span class="propname">
			<%=Html._("Department.DepartmentList.ThemeA.BrandFilter")%></span>
			<input id="BrandCount" type="hidden" value="<%=Model.BrandFilter.Count()%>" />
			<input id="BrandCurrent" type="hidden" value="10" />
		</li>
		<li class="for-slider">
			<div id="slider">
				<ul style="width: 170px; padding-left: 10px">
					<% 
	   int count = 0;
	   foreach (var item in Model.BrandFilter)
	   {
		   count++;
		   string rowState;
		   rowState = Model.bRefId == item.RefId ? "active" : rowState = "";
					%>
					<%
				if (count % 10 == 1)
				{
					%>
					<li>
						<table>
							<%
				}
							%>
							<tr class="<%=rowState %>">
								<td>
									<%
				if (rowState == "active")
				{
									%>
									<%=item.BrandName%>
									<a href='<%=Url.Action("DepartmentList", "Department", new { IsBreadCrumbUp="True", Paths=Model.Paths }) %>'
										class="btn-close-filter"></a>
									<%
				}
				else
				{
									%>
									<a href='<%=Url.Action("DepartmentList", "Department", new { bRefId = item.RefId, IsBreadCrumbUp="True", Paths=Model.Paths }) %>'>
										<%=item.BrandName%></a>
									<%
				}
									%>
								</td>
							</tr>
							<%
				if (count % 10 == 0 || count == Model.BrandFilter.Count())
				{
							%>
						</table>
					</li>
					<%
				}
			}
					%>
				</ul>
			</div>
		</li>
	</ul>
</div>
<% } %>
