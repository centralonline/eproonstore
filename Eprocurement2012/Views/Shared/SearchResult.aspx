﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SearchResultData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div id="searchresult">
		<%if (Model.SearchResult.Any())
	{%>
		<h1>
			<%=String.Format(Html._("Shared.SearchResult.Content"), Model.SearchResult.Count())%>
		</h1>
		<div id="contentCol" style="padding-left: 10px;">
			<%Html.BeginForm("HandleBuyOfficeSupply", "Product", FormMethod.Post, new { @class = "handleBuy-OfficeSupply" }); %>
			<div id="content" class="content">
				<table class="office-list-product" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<td class="text-center fix50">
								<%=Html._("Repurchase.MyProductId")%>
							</td>
							<td class="text-center" style="width: 80%">
								<%=Html._("Repurchase.MyProductName")%>
							</td>
							<td class="text-center bgcolor-hilight">
								<%=Html._("Repurchase.MyProductPrice")%>
							</td>
							<%if (Model.User.IsRequester && Model.SearchResult.Any(p => p.IsDisplayBuyButton))
		 { %>
							<td class="text-center bgcolor-hilight2 fix50">
								<%=Html._("Repurchase.MyProductQuantity")%>
							</td>
							<%} %>
							<td class="text-center fix50">
								<%=Html._("Repurchase.MyProductUnit")%>
							</td>
						</tr>
					</thead>
					<tbody>
						<%foreach (var item in Model.SearchResult)
		{ %>
						<tr>
							<td style="text-align: center">
								<span class="gray">
									<%=item.Id%></span>
							</td>
							<td>
								<div id="product-list" class="office-list-product-infomation-name">
									<div class="quickinfobt" style="display: inline-block;">
										<%=Html.Hidden("pid", item.Id)%>
									</div>
									<div style="display: inline-block; margin-left: 10px;">
										<span style="margin-right: 20px; font: normal 13px tahoma;"><a href="<%=Url.Action("Details", "Product", new { item.Id })%>">
											<%=Html.Encode(item.Name)%></a></span>
										<%=item.IsPromotion ? "<span style='color:Red'>สินค้าโปรโมชั่น</span>" : String.Empty%>
										<%=item.IsContactForDelivery ? "<br /><span style='color:Red'>* สินค้ามีค่าจัดส่ง กรุณาติดต่อเจ้าหน้าที่ ที่เบอร์. 027395555</span>" : String.Empty%>
									</div>
								</div>
							</td>
							<td class="text-right">
								<%=item.DisplayPrice%>
							</td>
							<%if (Model.User.IsRequester && Model.SearchResult.Any(p => p.IsDisplayBuyButton))
		 { %>
							<td class="text-center">
								<%=Html.Hidden("productId", item.Id)%>
								<%--<%=item.IsDisplayBuyButton ? "<input type='text' style='width:30px;' maxlength='4' name='quantity' class='quantity' />" : item.IsContactForDelivery ? "<span style='color:Red;'>สินค้ามีค่าจัดส่ง</span><input type='hidden' name='quantity' value='' />" : "<span style='color:Red;'>สินค้าขาด</span><input type='hidden' name='quantity' value='' />"%>--%>
								<%=item.IsDisplayBuyButton ? "<input type='text' style='width:30px;' maxlength='4' name='quantity' />" : item.IsContactForDelivery ? "<input type='hidden' name='quantity' value='' />" : "<span style='color:Red;'>สินค้าขาด</span><input type='hidden' name='quantity' value='' />"%>
							</td>
							<%} %>
							<td class="text-center">
								<%=item.Unit%>
							</td>
						</tr>
						<%} %>
					</tbody>
				</table>
				<%if (Model.User.IsRequester && Model.SearchResult.Any(p => p.IsDisplayBuyButton))
	  { %>
				<table class="office-list-product" cellpadding="0" cellspacing="0">
					<tr>
						<td style="background-color: #d1d1d1;">
							<div style="float: right; background-color: #d1d1d1; width: auto">
								<input type="image" name="AddToCartOfficeSupply" value="AddToCartOfficeSupply" class="AddToCartOfficeSupply"
									id="AddToCartOfficeSupply" src="/images/theme/button-addTocart.png" />
							</div>
						</td>
					</tr>
				</table>
				<%} %>
			</div>
			<%Html.EndForm(); %>
		</div>
		<div id="message-Popup" style="display: none; text-align: center;">
		</div>
		<div id="BackToShopOrGotoCheckOut" style="display: none;">
			<br />
			<br />
			<br />
			<input type="image" src="/images/btn/btn_shop_continue.gif" value="กลับไป shop" id="backtoshop"
				onclick="closeMessagePopup();" />
			หรือ <a href="<%=Url.Action("CartDetail", "Cart") %>">
				<img src="/images/btn/btn_go2_checkout.gif" alt="ไปหน้า Check Out" id="gotocheckout" />
			</a>
		</div>
		<%}%>
		<%else
	{ %>
		<%=Html._("Shared.SearchResult.Notfound")%>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<link href="/css/QuickInfo.css" rel="Stylesheet" type="text/css" />
	<style type="text/css">
		div#searchresult
		{
			margin-top: 13px;
			min-height: 1500px;
			min-width: 462px;
			margin-left: 220px;
			padding-right: 0px;
			position: relative;
		}
		div#searchresult-left-panel
		{
			left: 7px;
			position: absolute;
			top: -12px;
		}
		div#searchresult-right-panel
		{
			right: 7px;
			min-height: 600px;
		}
		
		div#searchresult-right-panel > ol
		{
			border: 0 none inherit;
			font-family: inherit;
			font-size: 100%;
			font-style: inherit;
			font-weight: inherit;
			list-style-type: none;
			margin: 0;
			outline-color: -moz-use-text-color;
			outline-style: none;
			outline-width: 0;
			padding: 0;
			vertical-align: baseline;
		}
		div#searchresult-right-panel > ol
		{
			display: inline-block;
			margin: 15px 5px;
			min-height: 200px;
			min-width: 140px;
			vertical-align: top;
			width: 150px;
		}
		div#searchresult-right-panel > div.product-infomation-description
		{
			display: none;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script src="/js/QuickInfo.js" type="text/javascript"></script>
	<script src="/js/easySlider1.5.js" type="text/javascript"></script>
	<script type="text/javascript">
		var popupTimer;
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});

			$("#slider").easySlider({
				vertical: true,
				nextText: "",
				prevText: ""
			});
			if ($("#slider li").size() < 2) {
				$("#nextBtn").hide();
			}
			initQuickInfo($('.quickinfobt'), '<%=Url.Action("QuickInfo","Product")%>');

			$('.AddToCartOfficeSupply').click(addToCartOfficeSupply);
			$("#message-Popup").bind("dialogclose", function (event, ui) {
				clearTimeout(popupTimer);
			});
		});

		function addToCartOfficeSupply(event) {
			event.preventDefault();
			if ($('input:text[name="quantity"][value!=""]').length > 0) {
				var thisForm = $(this).closest('form');
				var sendData = thisForm.serialize() + "&AddToCartOfficeSupply.AddToCartOfficeSupply.x=";
				formProcess(sendData);
			}
		}

		function formProcess(formData) {
			$("#message-Popup").dialog({
				autoOpen: false,
				modal: true,
				width: 420,
				height: 200
			});
			$.ajax({
				url: '<%=Url.Action("HandleBuyOfficeSupply", "Product") %>',
				type: "POST",
				data: formData,
				success: function (data) {
					if (data.Result) {
						$("#header-shoppingcart-countproduct").text(data.ItemCount);
						$("[name='quantity']").val('');
						$("#message-Popup").html("<h3>" + data.Message + "</h3>");
						$("#message-Popup").append($("#BackToShopOrGotoCheckOut").html());
						$("#message-Popup").dialog('open');
						popupTimer = setTimeout(closeMessagePopup, 30000);

					} else {
						$("#message-Popup").html("<h3>" + data.Message + "</h3>");
						$("#message-Popup").append($("#BackToShopOrGotoCheckOut").html());
						$("#message-Popup").dialog('open');
					}
				}
			});
		}

		function closeMessagePopup() {
			clearTimeout(popupTimer);
			$("#message-Popup").dialog('close');
		}
	</script>
</asp:Content>
