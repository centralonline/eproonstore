﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Product>" %>
<li style="margin: 10px; display: inline-block; width: 150px;">
	<div style="position: relative">
		<%
			//promotion
			if (Model.IsPromotion)
			{
		%>
		<div id="icon-promotion" align="center" style="position: absolute; z-index: 0; bottom: 0px;
			right: 0px;">
			<img height="33" width="74" style="padding-left: 5px; padding-top: 5px;" src="<%=Url.GetFullUrl("/images/icon/icon_product_promotion.png")%>"
				alt="" class="promotion" />
		</div>
		<%
			}
		%>
		<div>
			<a href="<%= Url.FullUrlAction("Details", "Product", new { Model.Id, Paths=Model.Paths })%>">
				<img width="130" height="130" src="<%=Url.GetFullUrl(Model.ImageSmallUrl) %>" alt="<%=Html.Encode(Model.Name) %>"
					title="<%=Html.Encode(Model.Name) %>" />
			</a>
		</div>
	</div>
	<div class="quickinfobt">
		<%=Html.Hidden("pid",Model.Id) %></div>
	<div class="group">
		<div class="product-infomation">
			<div class="product-infomation-name">
				<a href="<%= Url.FullUrlAction("Details", "Product", new { Model.Id, Paths=Model.Paths })%>">
					<%=Html.Encode(Model.Name) %>
				</a>
			</div>
			<div class="product-infomation-price">
				<%=Model.DisplayPrice.ToString() %>
				<%=Html._("Product.Detail.Baht")%>
			</div>
		</div>
	</div>
	<input type="hidden" name="uploadon" value="<%=Model.UploadOn.ToString(new DateTimeFormat()) %>" />
</li>
