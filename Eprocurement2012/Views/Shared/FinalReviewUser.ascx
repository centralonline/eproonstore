﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.FinalReview>" %>

<div>
		<%int count = 0;%>
		<table>
			<tr>
				<th width="30">
					<%=Html._("Admin.CompanyInfo.No")%>
				</th>
				<th width="150" style="text-align: left;">
					<%=Html._("Shared.MyProfile.ProfileImage")%>
				</th>
				<th>
					<%=Html._("Admin.FinalReview.Data")%>
				</th>
			</tr>
			<%
				foreach (var item in Model.UserAdmin.GroupBy(u => u.UserId))%>
			<%{%>
			<tr>
				<td>
					<%=++count%>
				</td>
				<td>
					<div id="profile-pic-preview" style="text-align: center; margin-right: 30px; float: left;
						margin-top: 10px;">
						<img src="<%=Url.Action("ProfileImage","UserProfile", new {item.First().UserGuid }) %>"
							alt="<%=item.First().DisplayName%>" height="100" />
					</div>
				</td>
				<td valign="top">
					<div style="text-align: left; float: left; margin-top: 0px; top: -30px; position: relative;">
						<%=item.First().UserId%><br />
						<%=item.First().DisplayName%><br />
						<%foreach (var role in item)%>
						<%{%>
						<%=role.DisplayUserRoleName%>
						<%}%>
						<br />
						<label><%=Html._("Admin.ExportFinalReview.Status")%> : </label><%=item.First().DisplayUserStatus%>
					</div>
				</td>
			</tr>
			<%} %>
			<%
				foreach (var item in Model.UserPurchase.GroupBy(u => u.UserId))%>
			<%{%>
			<tr>
				<td valign="top">
					<%=++count%>
				</td>
				<td>
					<div id="profile-pic-preview" style="text-align: center; margin-right: 30px; float: left;
						margin-top: 10px;">
						<img src="<%=Url.Action("ProfileImage","UserProfile", new {item.First().UserGuid }) %>"
							alt="<%=item.First().DisplayName%>" width="100" height="100" />
					</div>
				</td>
				<td>
					<div style="text-align: left; float: left; margin-top: 0px; top: -30px; position: relative;">
						<%=item.First().UserId%><br />
						<%=item.First().DisplayName%><br />
						<%foreach (var role in item)%>
						<%{%>
						<%=role.DisplayUserRoleName%>
						<%}%>
						<br />
						<label><%=Html._("Admin.ExportFinalReview.Status")%> : </label><%=item.First().DisplayUserStatus%>
					</div>
				</td>
			</tr>
			<%} %>
		</table>
	</div>
