﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>QuickInfoProcessStatus</title>
</head>
<body>
	<div id="quickinfo-processstatus" style="text-align:center">
		<h3 id="quickinfo-processstatus-statustext" style="margin-top:20px; margin-bottom:20px">
			<%=TempData["Status"]%>
		</h3>
		<input type="image" src="/images/btn/btn_shop_continue.gif" value="กลับไป shop" id="backtoshop" /> <%=Html._("Shared.QuickInfoProcessStatus.Or")%>
		<a href="<%=Url.Action("CartDetail", "Cart") %>" ><img src="/images/btn/btn_go2_checkout.gif" alt="ไปหน้า Check Out" id="gotocheckout" /></a>
	</div>
</body>
</html>


<script type="text/javascript">

	$(function() {
		var t = setTimeout("hideDialog()", 5000);
		$("#backtoshop").click(function() {
			clearTimeout(t);
			hideDialog();
		});
	});
	
</script>