﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.EditProfileUser>" %>
<!--new-->
<%Html.BeginForm("EditMyProfile", "UserProfile", FormMethod.Post); %>
<h5 class="Head5">
	<%=Html._("Shared.MyProfile")%></h5>
<div class="position">
	<label>
		<%=Html._("Shared.MyProfile.Email")%></label>
</div>
<div class="left">
	<p class="textSpanGray">
		<%=Model.Email%><%=Html.HiddenFor(m => m.Email)%></p>
	<%=Html.HiddenFor(m => m.SequenceId)%>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.MyProfile.ThaiName")%>
		<%--<span style="color: Red">*</span>--%></label>
</div>
<div class="left">
	<p class="textSpanGray">
	<%--<%=Html.TextBoxFor(m => m.ThaiName, new { @class = "medium" })%>--%><%=Model.ThaiName%><%=Html.HiddenFor(m => m.ThaiName)%></p>
	<%--<span style="color: Red">
		<%=Html.LocalizedValidationMessageFor(m => m.ThaiName)%></span>--%>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.MyProfile.EngName")%>
		<%--<span style="color: Red">*</span>--%></label>
</div>
<div class="left">
	<p class="textSpanGray">
	<%--<%=Html.TextBoxFor(m => m.EngName, new { @class = "medium" })%>--%><%=Model.EngName%><%=Html.HiddenFor(m => m.EngName)%></p>
	<%--<span style="color: Red">
		<%=Html.LocalizedValidationMessageFor(m => m.EngName)%></span>--%>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.MyProfile.PhoneNumber")%>
		<span style="color: Red">*</span></label>
</div>
<div class="left">
	<p class="textSpanGray">
		<%=Html.HiddenFor(m => m.PhoneId)%><%=Html.TextBoxFor(m => m.Phone, new { maxlength = "10", @class = "medium"})%><br />
	</p>
	<span style="color: Red">
		<%=Html.LocalizedValidationMessageFor(m => m.Phone)%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.MyProfile.ExtNumber")%></label>
</div>
<div class="left">
	<p class="textSpanGray">
		<%=Html.TextBoxFor(m => m.PhoneExt, new { maxlength = "5", @class = "medium" })%>
	</p>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.MyProfile.MobileNumber")%></label>
</div>
<div class="left">
	<p class="textSpanGray">
		<%=Html.HiddenFor(m => m.MobileId)%><%=Html.TextBoxFor(m => m.Mobile, new { maxlength = "10", @class = "medium" })%></p>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.MyProfile.FaxNumber")%></label>
</div>
<div class="left">
	<p class="textSpanGray">
		<%=Html.HiddenFor(m => m.FaxId)%><%=Html.TextBoxFor(m => m.Fax, new { maxlength = "10", @class = "medium" })%></p>
</div>
<div class="eclear">
</div>
<div class="positionBtnContact">
	<input class="graybutton" type="submit" value="<%=Html._("Button.Save")%>" name="SendContactUs" />
</div>
<div class="eclear">
</div>
<%Html.EndForm(); %>
