﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Eprocurement2012.Models.OrderActivity>>" %>
<div id="content-shopping-cart-detail">
	<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
		border: solid 1px #c5c5c5; text-align: center; margin-bottom: 10px;">
		<thead>
			<tr style="height: 30px;">
				<th class="thead">
					<%=Html._("Shared.OrderActivity.DateTime")%>
				</th>
				<th class="thead">
					<%=Html._("Shared.OrderActivity.Process")%>
				</th>
				<th class="thead">
					<%=Html._("Order.ViewOrder.OrderActivity")%>
				</th>
			</tr>
		</thead>
		<tbody>
			<%foreach (var item in Model)
			{ %>
			<tr style="height: 30px;">
				<td>
					<%=item.CreateOn.ToString(new DateTimeFormat()) %>
				</td>
				<td align="left">
					<%=item.ActivityName %>
				</td>
				<td align="left">
					<%=item.Remark %>
				</td>
			</tr>
			<%} %>
		</tbody>
	</table>
	<p>
		<%=Html._("Shared.OrderActivity.Content")%></p>
</div>
