﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Order>" %>
<%@ Import Namespace="Eprocurement2012.Models" %>
<table cellspacing="0" cellpadding="0" width="670">
	<tr>
		<% if (Model.Status == Order.OrderStatus.Waiting || Model.Status == Order.OrderStatus.WaitingAdmin)
		{%>
		<% if (Model.CurrentApprover.Approver.UserId != null)
		{%>
		<td style="float: left; background: none; padding: .0em 0em;">
			<% if (Model.NextApprover == null)
			{%>
			<img src="/images/MyApproveStatus/status_short_waiting.jpg" alt="" /><br />
			<%}%>
			<%else%>
			<%{%>
			<img src="/images/MyApproveStatus/status_Long_waiting.jpg" alt="" /><br />
			<%} %>
			<%if (Model.Status == Order.OrderStatus.WaitingAdmin)
			{%>
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.AppThaiName")%></span><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.CurrentApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.CurrentApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
			<%}%>
			<%else%>
			<%{%>
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.UserThaiName")%></span><%=Model.CurrentApprover.Level%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.CurrentApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> : <%=Model.CurrentApprover.ApproveCreditLimit.ToString(new MoneyFormat())%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.CurrentApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
			<%}%>
		</td>
		<%}%>
		<% if (Model.NextApprover != null)
		{%>
		<td style="float: left; background: none; padding: .0em 0em;">
			<img src="/images/MyApproveStatus/status_Short_next.jpg" alt="" /><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.UserThaiName")%></span><%=Model.NextApprover.Level%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.NextApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> : <%=Model.NextApprover.ApproveCreditLimit.ToString(new MoneyFormat())%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.NextApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
		</td>
		<%}%>
		<%}%>
		<% else if (Model.Status == Order.OrderStatus.Revise)
		{%>
		<% if (Model.PreviousApprover != null)
		{%>
		<td style="float: left; background: none; padding: .0em 0em;">
			<img src="/images/MyApproveStatus/status_Long_pass.jpg" alt="" /><br />
			<%if (Model.OldStatus == Order.OrderStatus.WaitingAdmin)
			{%>
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.AppThaiName")%></span><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.PreviousApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.PreviousApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
			<%}%>
			<%else%>
			<%{%>
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.UserThaiName")%></span> <%=Model.PreviousApprover.Level%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.PreviousApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> : <%=Model.PreviousApprover.ApproveCreditLimit.ToString(new MoneyFormat())%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.PreviousApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
			<%} %>
		</td>
		<%}%>
		<td style="float: left;">
			<img src="/images/MyApproveStatus/status_Long_waiting.jpg" alt="" /><br />
			<%=Model.Requester.DisplayName%>
		</td>
		<% if (Model.NextApprover != null)
		{%>
		<td style="float: left; background: none; padding: .0em 0em;">
			<img src="/images/MyApproveStatus/status_Short_current.jpg" alt="" /><br />
			<%if (Model.OldStatus == Order.OrderStatus.WaitingAdmin)
			{%>
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.AppThaiName")%></span><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.NextApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.NextApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
			<%}%>
			<%else%>
			<%{%>
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.UserThaiName")%></span> <%=Model.NextApprover.Level%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.NextApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> : <%=Model.NextApprover.ApproveCreditLimit.ToString(new MoneyFormat())%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.NextApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
			<%} %>
		</td>
		<%}%>
		<%}%>
		<%else if (Model.Status == Order.OrderStatus.Partial || Model.Status == Order.OrderStatus.AdminAllow)
		{%>
		<% if (Model.PreviousApprover != null)
		{%>
		<td style="float: left; background: none; padding: .0em 0em;">
			<%if (Model.Status == Order.OrderStatus.AdminAllow)
			{%>
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.AppThaiName")%></span><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.PreviousApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.PreviousApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
			<%}%>
			<%else%>
			<%{%>
			<img src="/images/MyApproveStatus/status_Long_pass.jpg" alt="" /><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.UserThaiName")%></span> <%=Model.PreviousApprover.Level%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.PreviousApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> : <%=Model.PreviousApprover.ApproveCreditLimit.ToString(new MoneyFormat())%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.PreviousApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
			<%}%>
		</td>
		<%}%>
		<% if (Model.CurrentApprover.Approver.UserId != null)
		{%>
		<td style="float: left; background: none; padding: .0em 0em;">
			<% if (Model.NextApprover == null)
			{%>
			<img src="/images/MyApproveStatus/status_short_waiting.jpg" alt="" /><br />
			<%}%>
			<%else%>
			<%{%>
			<img src="/images/MyApproveStatus/status_Long_waiting.jpg" alt="" /><br />
			<%} %>
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.UserThaiName")%></span><%=Model.CurrentApprover.Level%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.CurrentApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> : <%=Model.CurrentApprover.ApproveCreditLimit.ToString(new MoneyFormat())%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.CurrentApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
		</td>
		<%}%>
		<% if (Model.NextApprover != null)
		{%>
		<td style="float: left; background: none; padding: .0em 0em;">
			<img src="/images/MyApproveStatus/status_Short_next.jpg" alt="" /><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.UserThaiName")%></span><%=Model.NextApprover.Level%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.NextApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> : <%=Model.NextApprover.ApproveCreditLimit.ToString(new MoneyFormat())%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.NextApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
		</td>
		<%}%>
		<%}%>
		<% else if (Model.Status == Order.OrderStatus.Approved || Model.Status == Order.OrderStatus.Deleted)
		{%>
		<% if (Model.PreviousApprover != null)
		{%>
		<td style="float: left; background: none; padding: .0em 0em;">
			<img src="/images/MyApproveStatus/status_Short_pass.jpg" alt="" /><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.UserThaiName")%></span><%=Model.PreviousApprover.Level%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.Name")%></span> : <%=Model.PreviousApprover.Approver.DisplayName%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> : <%=Model.PreviousApprover.ApproveCreditLimit.ToString(new MoneyFormat())%><br />
			<span style="font-weight: bolder;"><%=Html._("Shared.MyApprover.ParkDay")%></span> : <%=Model.PreviousApprover.ParkDay%> <%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
		</td>
		<%}%>
		<%}%>
	</tr>
</table>
