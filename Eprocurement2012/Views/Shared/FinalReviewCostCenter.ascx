﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.FinalReview>" %>
<div>
		<%int count = 0;%>
		<%if (Model.CostCenter.Any())
	{%>
		<table>
			<tr>
				<th width="30">
					<%=Html._("Admin.CompanyInfo.No")%>
				</th>
				<th width="70">
					<%=Html._("Admin.Department.DeptID")%>
				</th>
				<th width="120">
					<%=Html._("Admin.CostCenter.CostCenterID")%>
				</th>
				<th width="120">
					<%=Html._("Admin.CostCenter.ThaiName")%>
				</th>
				<th width="120">
					<%=Html._("Admin.CostCenter.EngName")%>
				</th>
				<th width="60">
					<%=Html._("Admin.CostCenter.CustID")%>
				</th>
				<th width="200">
					<%=Html._("Admin.CompanyInfo.InvoiceAddress")%>
				</th>
				<th width="200">
					<%=Html._("Admin.CompanyInfo.ShippingAddress")%>
				</th>
				<th width="70">
					<%=Html._("Admin.CostCenter.Status")%>
				</th>
			</tr>
			<%foreach (var item in Model.CostCenter)%>
			<%{%>
			<tr>
				<td>
					<%=++count%>
				</td>
				<td>
					<%=item.CostCenterDepartment.DepartmentID%>
				</td>
				<td>
					<%=item.CostCenterID%>
				</td>
				<td>
					<%=item.CostCenterThaiName%>
				</td>
				<td>
					<%=item.CostCenterEngName%>
				</td>
				<td>
					<%=item.CostCenterCustID%>
				</td>
				<td>
					<%=item.CostCenterInvoice.Address1%><br />
					<%=item.CostCenterInvoice.Address2%><br />
					<%=item.CostCenterInvoice.Address3%><br />
					<%=item.CostCenterInvoice.Address4%><br />
				</td>
				<td>
					<%=item.CostCenterShipping.Address1%><br />
					<%=item.CostCenterShipping.Address2%><br />
					<%=item.CostCenterShipping.Address3%><br />
					<%=item.CostCenterShipping.Address4%><br />
				</td>
				<td>
					<%=item.DisplayCostCenterStatus %>
				</td>
			</tr>
			<%} %>
		</table>
		<%} %>
	</div>