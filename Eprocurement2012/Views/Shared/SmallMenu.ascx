﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Eprocurement2012.Models.Department>>" %>
<%@ Import Namespace="Eprocurement2012.Models" %>
<div id="small-header-menu">
	<div>
		All Departments</div>
	<ul style="display: none;">
		<%foreach (var deptChild in Model.Where(d => d.ParentId == 0))
		{ %>
		<li>
			<div>
				<%=deptChild.Name%><a href="<%=Url.Action("DepartmentList","Department",new { deptChild.Id }) %>">
					<img alt="" src="/images/btn/play_music.png" /></a></div>
			<ul style="display: none;">
				<%foreach (var deptGrandChild in Model.Where(d => d.ParentId == deptChild.Id))
				{%>
				<li>
					<div>
						<a href="<%=Url.Action("DepartmentList", "Department", new { deptGrandChild.Id, Paths = deptChild.Id }) %>">
							<%=deptGrandChild.Name%><img alt="" src="/images/btn/play_music.png" /></a>
					</div>
				</li>
				<%}%>
			</ul>
		</li>
		<%}%>
	</ul>
</div>
<script type="text/javascript">
	$(function () {
		$("#small-header-menu div").click(function () {
			$(this).siblings("ul").toggle();
		});
	});
</script>
