﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Eprocurement2012.Models.User>>" %>
<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
	border: solid 1px #c5c5c5; text-align: center;">
	<thead>
		<tr>
			<th class="thead" rowspan="2" width="50" style="text-align: center">
				<%=Html._("OFMAdmin.ViewOrderDetail.HeadCount")%>
			</th>
			<th class="thead" rowspan="2" style="text-align: center">
				<%=Html._("Shared.DataListUser.HeadUserId")%>
			</th>
			<th class="thead" rowspan="2" style="text-align: center">
				<%=Html._("Shared.MyProfile.ThaiName")%>
			</th>
			<th class="thead" rowspan="2" style="text-align: center">
				<%=Html._("Shared.MyProfile.EngName")%>
			</th>
			<%if (Model.Any(m => m.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin) || Model.Any(m => m.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
	 { %>
			<th class="thead" rowspan="2" style="text-align: center">
				<%=Html._("Shared.DataListUser.UserRoleRootAdmin")%>
			</th>
			<%} %>
			<th class="thead" colspan="3" style="text-align: center">
				<%=Html._("Shared.DataListUser.UserRole")%>
			</th>
			<th class="thead" rowspan="2" width="70" style="text-align: center">
				<%=Html._("Shared.DataListUser.UserLastActive")%>
			</th>
			<th class="thead" rowspan="2" width="70" style="text-align: center">
				<%=Html._("Shared.DataListUser.ChangePasswordOn")%>
			</th>
			<th class="thead" rowspan="2" width="70" style="text-align: center">
				<%=Html._("Shared.DataListUser.IsVerified")%>
			</th>
			<th class="thead" rowspan="2" style="text-align: center">
				<%=Html._("Shared.DataListUser.UserStatusName")%>
			</th>
			<th class="thead" rowspan="2" width="50" style="text-align: center">
				<%=Html._("Shared.DataListUser.EditUser")%>
			</th>
			<%if (Model.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Requester && o.UserStatusName == Eprocurement2012.Models.User.UserStatus.Active))
	 { %>
			<th class="thead" rowspan="2" width="50" style="text-align: center">
				<%=Html._("Admin.CompanySetting.HeadUser")%>
			</th>
			<%} %>
			<%--<th class="thead" rowspan="2"  width="50">
				<%=Html._("Shared.DataListUser.RemoveUser")%>
			</th>--%><%if (Model.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
			  {%>
			<th class="thead" rowspan="2" width="50">
				<%=Html._("เปลี่ยนผู้ดูแลระบบ")%>
			</th>
			<%} %>
		</tr>
		<tr>
			<th class="thead" width="50" style="text-align: center">
				<%=Html._("Shared.DataListUser.Requester")%>
			</th>
			<th class="thead" width="50" style="text-align: center">
				<%=Html._("Shared.DataListUser.Approver")%>
			</th>
			<th class="thead" width="90" style="text-align: center">
				<%=Html._("Shared.DataListUser.Observer")%>
			</th>
		</tr>
	</thead>
	<tbody>
		<%int rowIndex = 0;
	string controller = ViewContext.RouteData.Values["controller"].ToString();
	foreach (var item in Model.GroupBy(u => u.UserId))
	{ %>
		<tr>
			<td style="text-align: center">
				<%=++rowIndex%>
			</td>
			<td>
				<%=item.First().UserId%>
				<%if (ViewContext.RouteData.Values["controller"].ToString() == "Admin")
	  {%>
				<%=Html.ActionLink(Html._("Button.Detail"), "ViewUserDetail", "Admin", new { userGuid = item.First().UserGuid }, null)%>
				<%} %>
				<%else if (ViewContext.RouteData.Values["controller"].ToString() == "OFMAdmin")
	  {%>
				<%=Html.ActionLink(Html._("Button.Detail"), "ViewUserDetail", "OFMAdmin", new { userGuid = item.First().UserGuid, companyId = item.First().CurrentCompanyId }, null)%>
				<%} %>
			</td>
			<td>
				<%=item.First().UserThaiName%>
			</td>
			<td>
				<%=item.First().UserEngName%>
			</td>
			<%if (Model.Any(u => u.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin) || Model.Any(u => u.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
	 { %>
			<td>
				<%if (item.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin))
	  { %>
				<%=Html._("Account.AdminUser.RootAdmin")%>
				<%}
	  else if (item.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
	  { %>
				<%=Html._("Account.AdminUser.AssistantAdmin")%>
				<%} %>
			</td>
			<%} %>
			<td style="text-align: center">
				<%if (item.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Requester))
	  { %>
				<img src="/images/icon/icon_correct.png" />
				<%}
	  else
	  {%>
				<img src="/images/icon/action_delete.gif" />
				<%} %>
			</td>
			<td style="text-align: center">
				<%if (item.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Approver))
	  { %>
				<img src="/images/icon/icon_correct.png" />
				<%}
	  else
	  { %>
				<img src="/images/icon/action_delete.gif" />
				<%} %>
			</td>
			<td style="text-align: center">
				<%if (item.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Observer))
	  { %>
				<img src="/images/icon/icon_correct.png" />
				<% }
	  else
	  { %>
				<img src="/images/icon/action_delete.gif" />
				<%} %>
			</td>
			<td>
				<%=item.First().UserLastActive == new DateTime(1900, 1, 1) ? "-" : item.First().UserLastActive.ToString(new DateTimeFormat())%>
			</td>
			<td>
				<%=item.First().ChangePasswordOn == new DateTime(1900, 1, 1) ? "-" : item.First().ChangePasswordOn.ToString(new DateTimeFormat())%>
			</td>
			<td style="text-align: center">
				<%if (item.First().IsVerified)
	  { %>
				<img src="/images/icon/icon_correct.png" />
				<% }
	  else
	  { %>
				<img src="/images/icon/action_delete.gif" /><br />
				<%if (controller == "Admin")
	  {%>
				<%=Html.ActionLink("send mail verify", "SetVerifyUser", "Admin", new { userGuid = item.First().UserGuid }, null)%>
				<%} %>
				<%else if (controller == "OFMAdmin")
	  {%>
				<%=Html.ActionLink("send mail verify", "SetVerifyUser", "OFMAdmin", new { userGuid = item.First().UserGuid, companyId = item.First().CurrentCompanyId}, null)%>
				<%} %>
				<%} %>
			</td>
			<td>
				<%=item.First().DisplayUserStatus %>
			</td>
			<td style="text-align: center">
				<input type="submit" id="edit_<%=item.First().UserGuid%>" value="<%=Html._("Button.Edit")%>"
					name="EditUser.<%=item.First().UserGuid%>.x" />
			</td>
			<%if (Model.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Requester && o.UserStatusName == Eprocurement2012.Models.User.UserStatus.Active))
	 { %>
			<td style="text-align: center">
				<%if (item.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.Requester && o.UserStatusName == Eprocurement2012.Models.User.UserStatus.Active))
	  { %>
				<input type="submit" id="SetUserReq_<%=item.First().UserGuid%>" value="<%=Html._("Admin.CompanySetting.User")%>"
					name="SetUserReq.<%=item.First().UserGuid%>.x" />
				<%} %>
			</td>
			<%} %>
			<%--<td>
				<%if (item.First().UserStatusName == Eprocurement2012.Models.User.UserStatus.Active){ %>
				<input type="submit" id="remove_<%=item.First().UserId%>" value="<%=Html._("Button.Cancel")%>" name="RemoveUser.<%=item.First().UserGuid%>.x" />
				<%} %>
			</td>--%>
			<%if (Model.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
			{%>
			<td>
				<%if (item.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
				{ %>
				<input type="submit" id="SetRootAdmin<%=item.First().UserId%>" value="<%=Html._("SetRootAdmin")%>"
					name="SetRootAdmin.<%=item.First().UserGuid%>.x" />
				<%} %>
			</td>
			<%} %>
		</tr>
		<%} %>
	</tbody>
</table>
