﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Budget>" %>
<%if (Model != null)
{%>
<div class="position">
	<label>
		<%=Html._("OFMAdmin.CreateNewSite.BudgetPeriodType")%>:</label></div>
<div style="float: left;">
	<span class="textSpanGray">
		<%=Html._("NewSiteData.ListBudgetPeriodType." + Model.BudgetPeriod)%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("OFMAdmin.CreateNewSite.BudgetLevelType")%>:</label></div>
<div style="float: left;">
	<span class="textSpanGray">
		<%=Html._("NewSiteData.ListBudgetLevelType." + Model.BudgetLevel)%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("ReportBudget.OriginalAmt")%>
		:</label></div>
<div style="float: left;">
	<span class="textSpanGray">
		<%=Model.BudgetAmt.ToString(new MoneyFormat())%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("ReportBudget.UsedAmt")%>
		:</label></div>
<div style="float: left;">
	<span class="textSpanGray">
		<%=Model.UsedAmt.ToString(new MoneyFormat())%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("ReportBudget.WaitingOrderAmt")%>
		:</label></div>
<div style="float: left;">
	<span class="textSpanGray">
		<%=Model.WaitingOrderAmt.ToString(new MoneyFormat())%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("ReportBudget.RemainBudgetAmt")%>
		:</label></div>
<div style="float: left;">
	<span class="textSpanGray">
		<%=Model.RemainBudgetAmt.ToString(new MoneyFormat())%></span>
</div>
<div class="eclear">
</div>
<%} %>
