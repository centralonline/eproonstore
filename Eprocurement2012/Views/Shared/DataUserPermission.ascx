﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Eprocurement2012.Models.UserPermission>>" %>
<div id="content-shopping-cart-detail">
	<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
		border: solid 1px #c5c5c5; text-align: center;">
		<tr>
			<td class="thead">
				<%=Html._("DataUserPermission.CostcenterId")%>
			</td>
			<td class="thead">
				<%=Html._("DataUserPermission.CostcenterName")%>
			</td>
			<td class="thead">
				<%=Html._("DataUserPermission.Permission")%>
			</td>
			<td class="thead">
				<%=Html._("DataUserPermission.CustId")%>
			</td>
			<td class="thead">
				<%=Html._("DataUserPermission.StartDate")%>
			</td>
		</tr>
		<%foreach (var item in Model) %>
		<%{ %>
		<tr>
			<td align="left">
				<%=item.CostCenter.CostCenterID %>
			</td>
			<td align="left">
				<%=item.CostCenter.CostCenterName %>
			</td>
			<td>
				<%=item.DisplayRoleName%>
			</td>
			<td>
				<%=item.CustId %>
			</td>
			<td>
				<%=item.CreateOn.ToString(new DateTimeFormat()) %>
			</td>
		</tr>
		<%} %>
	</table>
</div>
<div class="eclear">
</div>
