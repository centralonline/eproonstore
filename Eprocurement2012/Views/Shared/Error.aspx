﻿<%@ Page Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div align="center">
		<%
			if (String.Equals(ViewData["Error"].ToString(), "Http500", StringComparison.OrdinalIgnoreCase))
			{ 
		%>
		<div>
			<%=DateTime.Now.ToString("yyyy'-'MM'-'dd HH':'mm':'ss'Z'")%>
		</div>
		<% 
			} 
		%>
		<div style="width: 1070px; margin: auto">
			<div style="float: left; width: 284px; height: auto;">
				<%
					string imgUrl = "";
					string imgUrl2 = "";
					string altText = "";
					if (String.Equals(ViewData["Error"].ToString(), "Http500", StringComparison.OrdinalIgnoreCase))
					{
						imgUrl = "/images/banner/error-01-500.jpg";
						imgUrl2 = "/images/banner/error-500.jpg";
						altText = "Error 500 INTERNAL SERVER ERROR ขอโทษค่ะ ระบบไม่สามารถแสดงหน้านี้ได้ในขณะนี้ อาจเป็นไปได้ว่า เกิดการผิดพลาดบางอย่างเกี่ยวกับหน้าที่กำลังเปิดอยู่ในขณะนี้ค่ะ";
					}
					else if (String.Equals(ViewData["Error"].ToString(), "Http404", StringComparison.OrdinalIgnoreCase))
					{
						imgUrl = "/images/banner/error-01-404.jpg";
						imgUrl2 = "/images/banner/error-404.jpg";
						altText = "Error 404 PAGE NOT FOUND ขอโทษค่ะ หน้าที่คุณต้องการไม่มีในระบบ อาจเป็นไปได้ว่า หน้านั้นอาจถูกลบไปแล้ว หรือถูกแก้ไข ย้ายที่ หรือเกิดการผิดพลาดบางอย่าง";
					}
					else if (String.Equals(ViewData["Error"].ToString(), "Http403", StringComparison.OrdinalIgnoreCase))
					{
						imgUrl = "/images/banner/error-01-403.jpg";
						imgUrl2 = "/images/banner/error-403.jpg";
						altText = "Error 403 FORBIDDEN EXPLAINED ขอโทษค่ะระบบไม่อนุญาติให้แสดงหน้านี้ได้ เนื่องจากคุณไม่ได้รับสิทธิ์ให้เข้าถึงหน้านี้ได้ค่ะ";
					}
				%>
				<!-- ตรงนี้แก้ค่าแบนเนอร์ตาม error นะ ถ้า error 403 ก้อใช้ error-01-403.jpg -->
				<img alt="" src="<%=imgUrl %>" />
				<!-- จบ -->
			</div>
			<%--<img alt="" src="/images/logo.gif" />--%>
			<div style="float: left">
				<!-- ตรงนี้แก้ค่าแบนเนอร์ตาม error นะ ถ้า error 404 ก้อใช้ 404.jpg -->
				<img alt="<%=altText %>" src="<%=imgUrl2 %>" /><br />
				<!-- จบ -->
				<img alt="" src="/images/banner/Error.jpg" width="616" height="459" border="0" usemap="#Map" />
				<map name="Map" id="Map">
					<area shape="rect" coords="21, 83, 165, 173" href="javascript:location.reload(true)"
						alt="" />
					<area shape="rect" coords="165, 83, 305, 173" href="/" target="_self" alt="หน้าแรก trendyday" />
					<area id="clicktochat" shape="rect" coords="177, 254, 415, 290" href="javascript:clickChat()"
						target="_self" alt="" />
				</map>
				<br />
			</div>
		</div>
		<script type="text/javascript">			var gaJsHost = (("https:" == document.location.protocol) ? "https://ssl." : "http://www.");
			document.write(unescape("%3Cscript src='" + gaJsHost + "google-analytics.com/ga.js' type='text/javascript'%3E%3C/script%3E"));
		</script>
		<script type="text/javascript">
			try {
				var pageTracker = _gat._getTracker("UA-11520015-1");
				pageTracker._trackPageview();
			} catch (err) { }
		</script>
		<script type="text/javascript">
			function clickChat() {
				window.open("https://livechat.trendyday.com/webchat/Eprocurement/ChatAndCoBrowse.htm", "livechat", "fullscreen=no,toolbar=no,location=no,directories=no,status=no,menubar=no,scrollbars=no,resizable=no,width=500,height=415,left=100,top=100,screenX=100,screenY=100");
			}
		</script>
	</div>
</asp:Content>
