﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.User>" %>
<div class="left">
	<p class="textSpanGray">
		<%=Html._("Shared.ChangeLanguage.Menu")%></p>
	<p class="textSpanGray">
		<%= Html.RadioButtonFor(m => m.UserLanguage, "TH", new { id = "TH" })%><%=Html._("Shared.ChangeLanguage.UserLanguageTH")%>
		<%= Html.RadioButtonFor(m => m.UserLanguage, "EN", new { id = "EN" })%><%=Html._("Shared.ChangeLanguage.UserLanguageEN")%>
	</p>
</div>
<div class="eclear">
</div>
<div class="positionBtnContact">
	<input class="graybutton" id="editUser" type="submit" value="<%=Html._("Button.Save")%>" />
</div>
