﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.CostCenter>" %>
<%if (Model.CostCenterContact != null)
  { %>
<div class="position">
	<label>
		<%=Html._("CreateUser.RoleRequester")%>
		:</label></div>
<div style="float: left;">
	<span class="textSpanGray">
		<%=Model.CostCenterContact.ContactorName%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("CreateUser.Mobile")%>
		:</label></div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.CostCenterContact.ContactMobileNo%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.PhoneNo")%>
		:</label></div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.CostCenterContact.ContactorPhone%> <%=string.IsNullOrEmpty(Model.CostCenterContact.ContactorExtension) ? "" : " #" + Model.CostCenterContact.ContactorExtension%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("CreateUser.Fax")%>
		:</label></div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.CostCenterContact.ContactorFax%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("CreateUser.Email")%>
		:</label></div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.CostCenterContact.Email%></span>
</div>
<div class="eclear">
</div>
<div class="spaceTop">
</div>
<h5>
	<%=Html._("OFMAdmin.CreateNewSite.InvAddr")%></h5>
<span class="textSpanGray">
	<%=Model.CostCenterInvoice.Address1%></span> <span class="textSpanGray">
		<%=Model.CostCenterInvoice.Address2%>
		<%=Model.CostCenterInvoice.Address3%>
		<%=Model.CostCenterInvoice.Address4%>
	</span>
<div class="spaceTop">
</div>
<h5>
	<%=Html._("Order.ViewOrder.ShippingAddress")%></h5>
<div class="position">
	<label>
		<%=Html._("CartDetail.ShipContactor")%>
		:</label></div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.CostCenterShipping.ShipContactor%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("CreateUser.Mobile")%>
		:</label></div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.CostCenterShipping.ShipMobileNo%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("CreateUser.Phone")%>
		:</label></div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.CostCenterShipping.ShipPhoneNo%> <%=string.IsNullOrEmpty(Model.CostCenterShipping.Extension) ? "" : " #" + Model.CostCenterShipping.Extension%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("OFMAdmin.CreateNewSite.ShipAddr")%>
		:</label></div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.CostCenterShipping.Address1%>
		<%=Model.CostCenterShipping.Address2%>
		<%=Model.CostCenterShipping.Address3%>
		<%=Model.CostCenterShipping.Address4%>
	</span>
</div>
<div class="eclear">
</div>
<div class="left">
	<div class="position">
		<label>
			<%=Html._("CartDetail.Remark")%>
			:</label></div>
	<span class="textSpanGray">
		<%=Model.CostCenterShipping.Remark%></span>
</div>
<div class="eclear">
</div>
<%}
  else
  {
%>
<%} %>
