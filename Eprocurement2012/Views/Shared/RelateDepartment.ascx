﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.DepartmentViewData>" %>
<% if (Model.RelateDepartment.Any())
   { %>
<div class="list-box">
	<h3>
		<%=Html._("Shared.RelateDepartment")%>
	</h3>
	<ul>
		<%
			foreach (var item in Model.RelateDepartment)
			{
		%><li>
			<%=Html.ActionLink(item.Name, "DepartmentList", "Department", new { item.Id, title = item.Name, Model.Paths }, null)%></li>
		<%
			}
		%>
	</ul>
</div>
<% 
	} 
%>
