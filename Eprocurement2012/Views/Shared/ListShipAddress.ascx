﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Company>" %>
<div style="float: left">
	<div style="border-top: 1px solid #bbb; border-bottom: 1px solid #bbb; border: 1px solid #bbb;
		padding: .2em 1em; text-align: left;">
		<label>
			<%=Html._("Admin.CompanyInfo.SelcetData")%></label>
		<div>
			<label>
				<%=Html._("Admin.CompanyInfo.CustId")%>
			</label>
			<%=Model.ListShipAddress.First().CustId%>
		</div>
		<div>
			<label>
				<%=Html._("Admin.CompanyInfo.InvoiceAddress")%>
			</label>
			<%=Model.CompanyInvoice.Address1%>
			<%=Model.CompanyInvoice.Address2%>
			<%=Model.CompanyInvoice.Address3%>
			<%=Model.CompanyInvoice.Address4%>
		</div>
	</div>
	<div>
		<label>
			<%=Html._("Admin.CompanyInfo.Shipping")%></label>
		<table>
			<tr>
				<th>
					<%=Html._("Admin.CompanyInfo.No")%>
				</th>
				<th>
					<%=Html._("Admin.CompanyInfo.ShippingAddress")%>
				</th>
			</tr>
			<%int i = 1;
	 foreach (var item in Model.ListShipAddress)%>
			<%{%>
			<tr>
				<td>
					<%=i %>
				</td>
				<td>
					<%=item.Address1+item.Address2+item.Address3%>
				</td>
			</tr>
			<%
		 i++;
	 } %>
		</table>
	</div>
</div>
