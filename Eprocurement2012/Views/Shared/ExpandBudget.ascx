﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Budget>" %>
<style type="text/css">
	.budgetdiv
	{
		width: 250px;
		float: left;
		text-align: left;
		margin: 10px 0px 0px 30px;
		font-size: 12px;
	}
	.budgethead
	{
		font-size: 14px;
		color: #666666;
		font-weight: bold;
	}
</style>
<%if (Model != null)
  {%>
<div class="budgetdiv">
	<label class="budgethead">
		<%=Html._("OFMAdmin.CreateNewSite.BudgetPeriodType")%>:</label></div>
<div class="budgetdiv">
	<label>
		<%=Html._("NewSiteData.ListBudgetPeriodType." + Model.BudgetPeriod)%></label>
</div>
<div class="eclear">
</div>
<div class="budgetdiv">
	<label class="budgethead">
		<%=Html._("OFMAdmin.CreateNewSite.BudgetLevelType")%>:</label></div>
<div class="budgetdiv">
	<label>
		<%=Html._("NewSiteData.ListBudgetLevelType." + Model.BudgetLevel)%></label>
</div>
<div class="eclear">
</div>
<div class="budgetdiv">
	<label class="budgethead">
		<%=Html._("ReportBudget.OriginalAmt")%>:</label></div>
<div class="budgetdiv">
	<label>
		<%=Model.BudgetAmt.ToString(new MoneyFormat())%></label>
</div>
<div class="eclear">
</div>
<div class="budgetdiv">
	<label class="budgethead">
		<%=Html._("ReportBudget.UsedAmt")%>:</label></div>
<div class="budgetdiv">
	<label>
		<%=Model.UsedAmt.ToString(new MoneyFormat())%></label>
</div>
<div class="eclear">
</div>
<div class="budgetdiv">
	<label class="budgethead">
		<%=Html._("ReportBudget.WaitingOrderAmt")%>:</label></div>
<div class="budgetdiv">
	<label>
		<%=Model.WaitingOrderAmt.ToString(new MoneyFormat())%></label>
</div>
<div class="eclear">
</div>
<div class="budgetdiv">
	<label class="budgethead">
		<%=Html._("ReportBudget.RemainBudgetAmt")%>:</label></div>
<div class="budgetdiv">
	<label>
		<%=Model.RemainBudgetAmt.ToString(new MoneyFormat())%></label>
</div>
<div class="eclear">
</div>
<div class="budgetdiv">
	<label class="budgethead">
		<%=Html._("ReportBudget.WaitingCurrentUserAmt")%>:</label></div>
<div class="budgetdiv">
	<label>
		<%=Model.WaitingCurrentUserAmt.ToString(new MoneyFormat())%></label>
</div>
<div class="eclear">
</div>
<div class="budgetdiv">
	<label class="budgethead">
		<%=Html._("ReportBudget.CurrentUserApproveAmt")%>:</label></div>
<div class="budgetdiv">
	<label>
		<%=Model.CurrentUserApproveAmt.ToString(new MoneyFormat())%></label>
</div>
<div class="eclear">
</div>
<%} %>
