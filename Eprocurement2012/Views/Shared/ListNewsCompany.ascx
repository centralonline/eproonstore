﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Eprocurement2012.Models.News>>" %>
<%if (Model.Any())
  { %>
<%foreach (var news in Model)%>
<%{%>
<%if (news.NewsType == "Private")
  {%>
<img src="<%=Url.Action("ProfileImage","UserProfile", new { userGuid = news.CreateGuid }) %>"
	alt="" width="50" height="50" class="imgepro-left" />
<%} %>
<%else
  {%>
<img src="/images/theme/icon-newupdate.jpg" class="imgepro-left" alt="" />
<%} %>
<%if (ViewContext.RouteData.Values["controller"].ToString() == "Admin")
  {%>
<p class="fontBold">
	<a href="<%= Url.Action("AdminNewsCompanyDetail", "Admin", new { newsGuid = news.NewsGUID, newsType = news.NewsType}, null)%>">
		<%=news.NewsTitle%></a></p>
<span style="color: #999;">
	<%if (news.NewsDetail.Length > 15)
   { %>
	<%=news.NewsDetail.Substring(0,15)%><a href="<%= Url.Action("AdminNewsCompanyDetail", "Admin", new { newsGuid = news.NewsGUID, newsType = news.NewsType}, null)%>">...
		read more</a></span>
<%} %>
<%else
   { %>
<%=news.NewsDetail%>
<%} %>
<%}
  else
  {%>
<p class="fontBold">
	<a href="<%= Url.Action("NewsCompanyDetail", "Home", new { newsGuid = news.NewsGUID, newsType = news.NewsType},null)%>">
		<%=news.NewsTitle%></a></p>
<span style="color: #999;">
	<%if (news.NewsDetail.Length > 15)
   { %>
	<%=news.NewsDetail.Substring(0,15)%><a href="<%= Url.Action("NewsCompanyDetail", "Home", new { newsGuid = news.NewsGUID, newsType = news.NewsType}, null)%>">...
		read more</a></span>
<%} %>
<%else
   { %>
<%=news.NewsDetail%>
<%} %>
<%}%>
<hr />
<%}%>
<%} %>