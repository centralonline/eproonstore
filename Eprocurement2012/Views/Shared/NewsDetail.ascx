﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.News>" %>
<div class="GroupData 100percent bg-news">
	<div class=" newsPostion">
		<h4>
			<%=Html._("Admin.NewsCompany.News")%></h4>
		<div class="border">
			<div>
				<div class="position">
					<label>
						<%=Html._("Admin.NewsCompany.NewsTitle")%></label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.NewsTitle%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Admin.NewsCompany.NewsPeriod")%></label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.ValidFrom.ToString(new DateFormat())%> &nbsp; <b><%=Html._("Admin.NewsCompany.To")%></b> &nbsp; <%=Model.ValidTo.ToString(new DateFormat())%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Admin.NewsCompany.NewsStatus")%></label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.DisplayNewsStatus%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Admin.NewsCompany.Priority")%></label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.DisplayPriority%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Admin.NewsCompany.NewsDetail")%></label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.NewsDetail%></p>
				</div>
				<div class="eclear">
				</div>
			</div>
		</div>
	</div>
</div>
