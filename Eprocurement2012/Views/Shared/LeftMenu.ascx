﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Eprocurement2012.Models.Department>>" %>
<%@ Import Namespace="Eprocurement2012.Models" %>
<div class="menu-epro dropdown">
	<ul id="menu">
		<%
			int count = Model.Where(d => d.ParentId == 0).Count();
			const string imageMenuPath = "/images/menu-epro/{0}.png";
			const string imageMenuBackPath = "/images/menu-epro/{0}-back.png";
			string imageMenuUrl;
			string imageMenuBackUrl;

			foreach (var deptChild in Model.Where(d => d.ParentId == 0))
			{
				if (Model.Any(c => c.ParentId == deptChild.Id))
				{
					imageMenuUrl = String.Format(imageMenuPath, deptChild.Id);
		%>
		<li class="icon"><a title="<%=deptChild.Name%>" href="<%=Url.Action("DepartmentList","Department",new { deptChild.Id }) %>">
			<img alt="<%=deptChild.Name%>" src="<%=imageMenuUrl%>" style="float: left" /><span><%=deptChild.Name%></span></a>
			<div class="menu-radius stylesub">
				<img alt="" src="/images/menu-epro/inside.png" style="width: 45px; height: 32px;
					position: absolute; top: 17px; left: -40px; z-index: 10" />
				<%
					int count2 = 0;
					int perItem = 16;
					int DeptItemCount = Model.Where(d => d.ParentId == deptChild.Id).Count();
					foreach (var deptGrandChild in Model.Where(d => d.ParentId == deptChild.Id))
					{
						count2++;
						if (count2 % perItem == 1)
						{
				%>
				<ul style="display: table-cell; width: 160px" class="cell">
					<%
					}
					imageMenuBackUrl = String.Format(imageMenuBackPath, deptGrandChild.Id);
					string imageMenuBackDiaplay = "";
					if (CachedIO.PathExists(imageMenuBackUrl))
					{
						imageMenuBackDiaplay = "<img alt='' src='" + imageMenuBackUrl + "' style='width:19px; height:13px' />";
					}
					%>
					<li>
						<%=Html.ActionLink(deptGrandChild.Name, "DepartmentList", "Department", new { deptGrandChild.Id, Paths = deptChild.Id }, new { target = deptGrandChild.ThemeTypeDown == Eprocurement2012.Models.DepartmentThemeType.Printing ? "_blank" : null })%>
					</li>
					<%
					if (count2 % perItem == 0 || count2 == DeptItemCount)
					{
					%>
				</ul>
				<%
					}
				}
				%>
			</div>
		</li>
		<%
				}
			}
		%>
	</ul>
	<br clear="all" />
</div>
