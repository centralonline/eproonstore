﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.CartData>" %>
<!--Status Bar-->
<table cellspacing="0" cellpadding="0" width="800">
	<tr>
		<%	int colcount = 0;
		int countApprover = Model.ListUserApprover.Count();
		foreach (var item in Model.ListUserApprover)
		{%>
		<%	string checkradio = "";
			colcount += 1;
			if (Model.CurrentSelectApprover != null && Model.CurrentSelectApprover.Approver.UserId == item.Approver.UserId) { checkradio = "checked='checked'"; }
			else if (Model.CurrentSelectApprover == null && item.Level == 1) { checkradio = "checked='checked'"; }
			else { checkradio = ""; }%>
		<td style="float: left;">
			<%if (countApprover == 1)
			{ %>
			<img src="/images/MyApproveStatus/status_Short_current.jpg" alt="" /><br />
			<%} %>
			<%else if (colcount < countApprover)
			{ %>
			<img src="/images/MyApproveStatus/status_Long_current.jpg" style="width: 240px;" alt="" /><br />
			<%} %>
			<%else
			{ %>
			<img src="/images/MyApproveStatus/status_Short_current.jpg" alt="" /><br />
			<%} %>
			<%if (item.IsByPassApprover)
			{%>
			<%if (Model.CurrentSelectApprover == null || (Model.CurrentSelectApprover != null && Model.CurrentSelectApprover.Approver.UserId == item.Approver.UserId))
			{%>
			<input id="approverId_<%=item.Approver.UserGuid %>" name="approverId" value="<%=item.Approver.UserGuid %>" <%=checkradio%> type="radio" />
			<%} %>
			<%} %>
			<%else
			{ %>
			<input id="approverId_<%=item.Approver.UserGuid %>" name="approverId" value="<%=item.Approver.UserGuid %>" type="hidden" />
			<%} %>
			<span style="font-weight: bolder;">
				<%=Html._("Shared.MyApprover.UserThaiName")%>
				<%=item.Level%></span><br />
			<span style="font-weight: bolder;">
				<%=Html._("Shared.MyApprover.Name")%></span> :
			<%=item.Approver.UserThaiName%><br />
			<span style="font-weight: bolder;">
				<%=Html._("Shared.MyApprover.ApproveCreditLimit")%></span> :
			<%=item.ApproveCreditLimit.ToString(new MoneyFormat())%>
			<%=Html._("Shared.MyApprover.ApproveCreditLimit.Bath")%><br />
			<span style="font-weight: bolder;">
				<%=Html._("Shared.MyApprover.ParkDay")%></span> :
			<%=item.ParkDay%>
			<%=Html._("Shared.MyApprover.ParkDay.Day")%><br />
		</td>
		<%}%>
	</tr>
</table>
