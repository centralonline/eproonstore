﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SelectSite.Master" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Eprocurement2012.Models.Company>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div style="min-height: 550px;">
		<div style="width: 100%;">
			<div id="selectcompay">
				<!--choose company-->
				<div class="selectSite">
					<%Html.BeginForm("UserSelectCompany", "Account", FormMethod.Post); %>
					<div class="minHeight650">
						<div class="boxblue3">
							<div class="box_input_select">
								<p class="positionSelect">
									<span>
										<%=Html._("Shared.UserRoleMoreOneCompany")%></span>
									<br />
									<span>
										<%=Html._("Shared.SelectCompany")%></span><br />
									<br />
									<%=Html.DropDownList("companyId", new SelectList(Model, "CompanyId", "CompanyName", Model.FirstOrDefault(c => c.IsSiteActive).CompanyId))%>
								</p>
								<div class="PositionButton">
									<input class="button-e2" id="selectCompany" name="selectCompany" type="submit" value="<%=Html._("Button.OK")%>"
										style="border: none;" />
								</div>
							</div>
						</div>
					</div>
					<%Html.EndForm(); %>
				</div>
				<!--choose company-->
			</div>
		</div>
		<img alt="" src="/images/index/slideShadow.jpg" />
	</div>
</asp:Content>
