﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.User>" %>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.CompanyId")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.Company.CompanyId %></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.CompanyName")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.Company.CompanyName%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.DisplayName")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.DisplayName %></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.UserId")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.UserId %></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.PhoneNo")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.UserContact.Phone != null ? Model.UserContact.Phone.PhoneNo : "-"%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.Extension")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.UserContact.Phone != null && !string.IsNullOrEmpty(Model.UserContact.Phone.Extension) ? Model.UserContact.Phone.Extension : "-"%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.fax")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.UserContact.Fax != null ? Model.UserContact.Fax.PhoneNo : "-"%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.mobile")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.UserContact.Mobile != null ? Model.UserContact.Mobile.PhoneNo : "-"%></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.UserLastActive")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=Model.UserLastActive.ToString("dd/MM/yyyy HH:mm") %></span>
</div>
<div class="eclear">
</div>
<div class="position">
	<label>
		<%=Html._("Shared.DataUserProfile.RemainDayForceChangedPassword")%></label>
</div>
<div class="left">
	<span class="textSpanGray">
		<%=string.Format(Html._("MyProfile.RemainDayForceChangedPassword"), Model.RemainDayForceChangedPassword)%></span>
</div>
<div class="eclear">
</div>
