﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.FinalReview>" %>
<div>
		<%int count = 0;%>

		<%if (Model.CompanyDepartment.Any())
	{%>
		<table class="info-bg" style="display: none;">
			<tr>
				<th width="30">
					<%=Html._("Admin.CompanyInfo.No")%>
				</th>
				<th width="70">
					<%=Html._("Admin.Department.DeptID")%>
				</th>
				<th width="300">
					<%=Html._("Admin.Department.ThaiName")%>
				</th>
				<th width="300">
					<%=Html._("Admin.Department.EngName")%>
				</th>
				<th>
					<%=Html._("Admin.Department.Status")%>
				</th>
			</tr>
			<%foreach (var item in Model.CompanyDepartment)%>
			<%{%>
			<tr>
				<td>
					<%=++count%>
				</td>
				<td>
					<%=item.DepartmentID%>
				</td>
				<td>
					<%=item.DepartmentThaiName%>
				</td>
				<td>
					<%=item.DepartmentEngName%>
				</td>
				<td>
					<%=item.DisplayDepartmentStatus %>
				</td>
			</tr>
			<%} %>
		</table>
		<%} %>
	</div>
