﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.Order>" %>
<div class="centerwrap">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".quantity").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
							 event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
							(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});
		});
	</script>
	<%Html.BeginForm("HandleEditOrder", "ApproveOrder", FormMethod.Post); %>
	<div id="content-shopping-cart-detail">
		<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
			border: solid 1px #c5c5c5">
			<tr>
				<td class="thead">
					<%=Html._("CartDetailPartial.ProductName")%>
				</td>
				<td width="80" class="thead">
					<%=Html._("CartDetailPartial.Price")%>
					<br />
					<%=Html._("CartDetailPartial.IncVat")%>
				</td>
				<td width="80" class="thead">
					<%=Html._("CartDetailPartial.Price")%>
					<br />
					<%=Html._("CartDetailPartial.ExcVat")%>
				</td>
				<td width="40" class="thead">
					<%=Html._("Shared.OrderDetailPartial.Quantity")%>
				</td>
				<%if (Model.IsEdit)
				{ %>
				<td width="40" class="thead">
					<%=Html._("Shared.OrderDetailPartial.EditQuantity")%>
				</td>
				<%} %>
				<td width="40" class="thead">
					<%--<%=Html._("CartDetailPartial.Discount")%>--%>
					<%=Html._("CartDetailPartial.Unit")%>
				</td>
				<td width="80" class="thead">
					<%=Html._("CartDetailPartial.NetAmt")%>
				</td>
			</tr>
			<% 
				foreach (Eprocurement2012.Models.OrderDetail Items in Model.ItemOrders)
				{ 
			%>
			<tr>
				<td style="margin-top: 10px;">
					<img src="<%=Html.Encode(Items.Product.ImageSmallUrl) %>" width="50" height="50"
						alt="" style="background-color: #CCCCCC; margin: 10px; margin-top: 0px;" class="left" />
					<strong style="color: #000066;">
						<%=Html._("CartDetailPartial.ProductId")%>
						:
						<%=Html.Encode(Items.Product.Id)%></strong><br />
					<input type="hidden" name="productID" value="<%=Items.Product.Id%>" />
					<div class="content-shopping-cart-table">
						<p class="productnameCart">
							<%=Html.ActionLink(Items.Product.Name, "Details", "Product", new { Items.Product.Id, title = Items.Product.Name }, null)%>
						</p>
						<%if (Items.IsOfmCatalog)
						{ %>
						<p>
							<%=Html._("CartDetailPartial.ProductNotInYourCatalog")%>
						</p>
						<%} %>
					</div>
				</td>
				<td valign="top">
					<%--<div align="center">
						<%=Html.Encode(Items.Product.PriceIncVat.ToString(new MoneyFormat()))%></div>--%>
						<div align="center">
						<%=Html.Encode(Items.ItemIncVatPrice.ToString(new MoneyFormat()))%></div>
				</td>
				<td valign="top">
					<%--<div align="center">
						<%=Html.Encode(Items.Product.PriceExcVat.ToString(new MoneyFormat()))%></div>--%>
						<div align="center">
						<%=Html.Encode(Items.ItemExcVatPrice.ToString(new MoneyFormat()))%></div>
				</td>
				<td valign="top">
					<div align="center">
						<%=Html.Encode(Items.Quantity)%>
						<br />
						<%--<%=Html.Encode(Items.Product.Unit)%>--%>
					</div>
				</td>
				<%if (Model.IsEdit)
				{ %>
				<td valign="top">
					<div align="center">
						<input type="text" name="quantity" maxlength="4" size="4" value="<%=Items.Quantity%>" />
						<br />
						<%=TempData["QuantityErrorPid" + Items.Product.Id]%>
					</div>
				</td>
				<%} %>
				<td valign="top">
					<div align="center">
						<%--<% = Items.DiscAmt.ToString(new MoneyFormat())%>--%>
						<%=Html.Encode(Items.Product.Unit)%>
					</div>
				</td>
				<td colspan="2" valign="top">
					<div align="center">
						<%=Html.Encode(Items.ItemPriceNet.ToString(new MoneyFormat()))%></div>
				</td>
			</tr>
			<%}%>
			<tr style="line-height: 25px">
				<td colspan="1">
					<p class="left" style="color: #a64a00">
					</p>
					<div align="right">
						<%=Html._("CartDetailPartial.NetAmt")%>&nbsp;</div>
				</td>
				<td valign="top">
					<div align="center">
					</div>
				</td>
				<td valign="top">
					<div align="center">
					</div>
				</td>
				<td valign="top">
					<div align="center">
						<b>
							<% = Html.Encode(Model.ItemCountOrder.ToString())%></b>
						<%--<%=Html._("CartDetailPartial.Item")%>--%></div>
				</td>
				<%if (Model.IsEdit)
				{ %>
				<td valign="top">
				</td>
				<%} %>
				<td valign="top">
					<%--<div align="center">
						<b>
							<% = Html.Encode(Model.TotalAllDiscount.ToString(new MoneyFormat()))%></b></div>--%>
					<div align="center">
						<b>
							<%=Html._("CartDetailPartial.Item")%></b></div>
				</td>
				<td valign="top">
					<div align="center">
						<b>
							<% = Html.Encode(Model.TotalNetAmt.ToString(new MoneyFormat()))%></b></div>
				</td>
			</tr>
		</table>
		<%if (Model.User.IsLogin && Model.CurrentApprover != null && string.Equals(Model.CurrentApprover.Approver.UserId, Model.User.UserId) && ViewContext.RouteData.Values["controller"].ToString() == "ApproveOrder")
		{ %>
		<div>
			<br />
			<%if (Model.IsEdit)
			{ %>
			<input id="SaveOrder" name="SaveOrder.<%=Model.OrderGuid%>.x" type="submit" value="<%=Html._("Button.SaveOrder")%>"
				class="graybutton" style="margin: 0 10px;" />
			<%}
			else
			{ %>
			<input id="EditOrder" name="EditOrder.<%=Model.OrderGuid%>.x" type="submit" value="<%=Html._("Button.EditOrder")%>"
				class="graybutton" style="margin: 0 10px;" />
			<%} %>
		</div>
		<%}%>
		<div style="width: 100%" class="">
			<div id="total" class=" margin-top-10 gray-m">
				<%
		
					if (Model.TotalDeliveryFee > 0)
					{ %>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalDeliveryFee")%></label>
					<span>
						<%=Html.Encode(Model.TotalDeliveryFee.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<%} %>
				<%
					if (Model.TotalDeliveryCharge > 0)
					{%>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalDeliveryCharge")%></label>
					<span>
						<%=Html.Encode(Model.TotalDeliveryCharge.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<%	} %>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalPriceProductNoneVatWithDisCountAmount")%></label>
					<span>
						<%=Html.Encode(Model.TotalPriceProductNoneVatAmount.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalPriceProductExcVatWithDisCountAmount")%></label>
					<span>
						<%=Html.Encode(Model.TotalPriceProductExcVatAmount.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.TotalVatAmt")%></label>
					<span>
						<%=Html.Encode(Model.TotalVatAmt.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
				<div style="border-bottom: 1px solid #999999; width: 370px; float: right; margin-top: 5px;">
				</div>
				<br class="clear" />
				<p style="text-align: right">
					<label>
						<%=Html._("CartDetailPartial.GrandTotalAmt")%></label>
					<span class="price" style="color: #CC0000; font-size: 13px; font-weight: bolder;">
						<%=Html.Encode(Model.GrandTotalAmt.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("CartDetailPartial.THB")%>
				</p>
			</div>
		</div>
		<br class="clear" />
	</div>
	<% Html.EndForm(); %>
	<br />
	<br />
</div>
<br class="clear" />
