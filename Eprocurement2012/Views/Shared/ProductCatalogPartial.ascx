﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<IEnumerable<Eprocurement2012.Models.Product>>" %>
<%if (Model != null && Model.Any())
{ %>
<div id="content-shopping-cart-detail">
	<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto; border: solid 1px #c5c5c5">
		<tr>
			<td width="40" class="thead">
				<%=Html._("ProductCatalog.HeadProductId")%>
			</td>
			<td width="80" class="thead">
				<%=Html._("ProductCatalog.HeadProductName")%>
			</td>
			<td width="40" class="thead">
				<%=Html._("ProductCatalog.HeadProductPrice")%>
			</td>
			<td width="40" class="thead">
				<%=Html._("ProductCatalog.HeadProductUnit")%>
			</td>
		</tr>
		<%foreach (var item in Model)
		{ %>
		<tr>
			<td>
				<%=item.Id%>
			</td>
			<td>
				<%=item.Name %>
				<input id="deleteProduct" name="deleteProduct.<%=item.Id%>" type="image" src="/images/icon/action_delete.gif" />
			</td>
			<td>
				<%=item.PriceIncVat.ToString(new MoneyFormat())%>
			</td>
			<td>
				<%=item.Unit %>
				<input name="productId" type="hidden" value="<%=item.CodeId %>" />
			</td>
		</tr>
		<%} %>
	</table>
</div>
<%}
else
{%>
<%=Html._("ProductCatalog.Emptry")%>
<%} %>