﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.BreadCrumbData>" %>
<%
	if (Model.BreadCrumbs != null)
	{
%>
<div id="breadcrumbs">
	<a href="<%= Url.Action("Index","Home") %>">
		<img alt="หน้าแรก" style="vertical-align: middle;" src="/images/theme/icon_to_home.gif" /></a>
	<%
		int count = 1;
		string paths = string.Empty;
		foreach (var item in Model.BreadCrumbs)
		{
			if (count != Model.BreadCrumbs.Count())
			{
	%>
	<%=Html.ActionLink(item.Name, "DepartmentList", "Department", new { item.Id, title = item.Name, Model.IsBreadCrumbUp, Paths = paths }, null)%>
	<%
				paths += (count > 1 ? "-" : String.Empty) + item.Id;
				count++;
	%>
	<span class="gray">&nbsp;&raquo;&nbsp;</span>
	<%
			}
			else
			{
	%>
	<strong style="color: #660000">
		<%=item.Name %></strong>
	<%
			}
		} 
	%>
</div>
<%} %>
