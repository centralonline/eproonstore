﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.DepartmentViewData>" %>
<ul>
	<%
		if (Model.NavigatorDepartment.ParentDept != null)
		{
	%>
	<li>
		<%=Html.ActionLink(Model.NavigatorDepartment.ParentDept.Name, "DepartmentList", "Department", new { Model.NavigatorDepartment.ParentDept.Id, Paths = Html.BackDepartmentPaths(Model.Paths) }, null)%></li>
	<%
		}
	%>
	<li>
		<ul style="margin: 5px 0 5px 10px; padding: 0" class="bullet">
			<li><strong style="color: #660000">
				<%=Model.NavigatorDepartment.AncestorDepartment.Name%></strong> </li>
			<li>
				<ul style="margin: 5px 0; padding: 0">
					<%
						foreach (var item in Model.NavigatorDepartment.ChildDept)
						{
					%>
					<li class="departmentNavigator-childlist">
						<%=Html.ActionLink(item.ChildDepartment.Name, "DepartmentList", "Department", new { item.ChildDepartment.Id , Paths=Html.GenerateDepartmentPaths(Model.Paths, Model.CurrentDepartment.Id) }, null)%>
						<%
							if (item.GrandChild.Count() > 0)
							{
						%>
						<ul style="margin: -20px 0 5px 60px; padding: 5px; background-color: white">
							<%
							foreach (var grandChild in item.GrandChild)
							{
							%>
							<li class="departmentNavigator-grandchildlist">
								<%=Html.ActionLink(grandChild.Name, "DepartmentList", "Department", new { grandChild.Id, Paths = Html.GenerateDepartmentPaths(Model.Paths, Model.CurrentDepartment.Id, item.ChildDepartment.Id) }, null)%></li>
							<%
							} 
							%>
						</ul>
						<%
						} 
						%>
					</li>
					<%
						}
					%>
				</ul>
			</li>
			<%
				if (Model.SiblingDepartment.Any())
				{
					foreach (var item in Model.SiblingDepartment)
					{
			%>
			<li>
				<%=Html.ActionLink(item.Name, "DepartmentList", "Department", new { item.Id, Model.Paths }, null)%></li>
			<%
					}
				} 
			%>
		</ul>
	</li>
</ul>
