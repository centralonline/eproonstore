﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.DepartmentViewData>" %>
<%
	if (Model.PropertyDepartment.Any())
	{
%>
<div class="list-box">
	<h3>
		<%=Html._("Shared.PropertyDepartment")%>
	</h3>
	<ul>
		<%
		foreach (var propItem in Model.PropertyDepartment.Where(p => !p.Choices.All(c => String.Equals(c.Value, "Yes", StringComparison.OrdinalIgnoreCase) || String.Equals(c.Value, "No", StringComparison.OrdinalIgnoreCase))))
		{
		%>
		<li class="icon-arrow">
			<%--<input type="checkbox" name="PropName" class="propname" value="<%=propItem.Name %>"
				style="display: none;" />--%>
			<span class="propname">
				<%=propItem.Name %></span></li>
		<li>
			<ul style="margin-left: 10px">
				<%
			foreach (var choiceItem in propItem.Choices)
			{
				string rowState;
				rowState = Model.cRefId == choiceItem.RefId ? "active" : rowState = "";
				%>
				<li class="<%=rowState %>">
					<%
				if (rowState == "active")
				{
					%>
					<%=choiceItem.Value %>
					<a href='<%=Url.Action("DepartmentList", "Department", new { IsBreadCrumbUp="True", Paths=Model.Paths }) %>'
						class="btn-close-filter" style="margin: 5px; width: 10px; height: 10px; background-position: 0px;">
					</a>
					<%
				}
				else
				{
					%>
					<a href="<%=Url.Action("DepartmentList", "Department", new { cRefId = choiceItem.RefId, IsBreadCrumbUp="True", Paths=Model.Paths })%>">
						<%=choiceItem.Value %></a>
					<%
				}
					%>
				</li>
				<%
			}
				%>
			</ul>
		</li>
		<%
		}
		%>
		<%
		foreach (var propItem in Model.PropertyDepartment.Where(p => p.Choices.All(c => String.Equals(c.Value, "Yes", StringComparison.OrdinalIgnoreCase) || String.Equals(c.Value, "No", StringComparison.OrdinalIgnoreCase))))
		{
			foreach (var choiceItem in propItem.Choices.Where(c => String.Equals(c.Value, "Yes", StringComparison.OrdinalIgnoreCase)))
			{
				string rowState;
				rowState = Model.cRefId == choiceItem.RefId ? "active" : rowState = "";
		%>
		<li class="<%=rowState %>">
			<%
				if (rowState == "active")
				{
			%>
			<%=choiceItem.Name %>
			<a href='<%=Url.Action("DepartmentList", "Department", new { IsBreadCrumbUp="True", Paths=Model.Paths }) %>'
				class="btn-close-filter" style="margin: 5px; width: 10px; height: 10px; background-position: 0px;">
			</a>
			<%
				}
				else
				{
			%>
			<a href="<%=Url.Action("DepartmentList", "Department", new { cRefId = choiceItem.RefId, IsBreadCrumbUp="True", Paths=Model.Paths })%>">
				<%=choiceItem.Name %></a>
			<%
				}
			%>
		</li>
		<%
			}
		}
		%>
	</ul>
</div>
<%
	}
%>
