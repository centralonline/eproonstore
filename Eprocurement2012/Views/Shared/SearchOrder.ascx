﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.SearchOrder>" %>
<div>
	<%=Html.TextBox("keyWord", "", new { ID = "keyword", style = "width: 250px; height: 20px;" })%>
	<%=Html.DropDownList("selectedSearch", new SelectList(Model.ListOrderSearch, "Id", "Name"), new { style = "width: 200px; height: 26px;" })%>
	<input class="graybutton" type="submit" value="<%=Html._("Button.Search")%>" />
	<span style="color: Blue">
		<%=TempData["Error"]%></span>
</div>
