﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.News>>>" %>
<script type="text/javascript">
	$(function () {
		$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
			$("#menuAnchor").show();
			$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
			$(".navShopAll").hover(incCounter, decCounter);
			$("#jMenu").hover(incCounter, decCounter);
		});
	});
</script>
<h2 id="page-heading">
	<%--<img alt="" src="../../images/theme/h3-list-icon.png" />--%>
	<%if (Model.Value.Any(n => n.NewsType == "Public"))
   { %>
	<h3>
		<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("Admin.NewsCompany.News")%></h3>
	<%}
   else
   { %>
	<h3>
		<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("Admin.NewsCompany")%></h3>
	<%} %>
</h2>
<div id="table-cart">
	<div id="content-shopping-cart-detail">
		<%int index = 0; %>
		<table>
			<tr>
				<td width="30" class="thead" align="center">
					<%=Html._("Admin.NewsCompany.Sequence")%>
				</td>
				<td width="350" class="thead">
					<%=Html._("Admin.NewsCompany.NewsTitle")%>
				</td>
				<td width="400" class="thead">
					<%=Html._("Admin.NewsCompany.Period")%>
				</td>
				<td width="150" class="thead">
					<%=Html._("Admin.NewsCompany.NewsStatus")%>
				</td>
			</tr>
			<%foreach (var item in Model.Value)%>
			<%{%>
			<tr>
				<td align="center">
					<%=++index%>
				</td>
				<td>
					<%if (ViewContext.RouteData.Values["controller"].ToString() == "Admin")
	   { %>
					<%=Html.ActionLink(item.NewsTitle, "AdminNewsCompanyDetail", "Admin", new { newsGuid = item.NewsGUID, newsType = item.NewsType }, null)%>
					<%}
	   else
	   { %>
					<%=Html.ActionLink(item.NewsTitle, "NewsCompanyDetail", "Home", new { newsGuid = item.NewsGUID ,newsType = item.NewsType}, null)%>
					<%} %>
				</td>
				<td align="center">
					<%=item.ValidFrom.ToString(new DateFormat()) + " " + Html._("Admin.NewsCompany.To") + " " + item.ValidTo.ToString(new DateFormat())%>
				</td>
				<td align="center">
					<%=item.DisplayNewsStatus %>
				</td>
			</tr>
			<%} %>
		</table>
	</div>
	<br />
</div>
