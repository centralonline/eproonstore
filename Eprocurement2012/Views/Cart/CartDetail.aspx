﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CartData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
			$("#All").hide();
			$(".help1").click(function () {
				$("#All").slideToggle("slow");
			});
		});
	</script>
	<div class="centerwrap">
		<input type="hidden" name="userLang" id="userLang" value="<%=Model.User.UserLanguage.ToString()%>" />
		<%Html.RenderPartial("CartDetailPartial", Model.Cart); %>
		<%if (Model.Cart.ItemCountCart == 0)
	{%>
		<p style="text-align: center">
			<a href="<%=Url.Action("Index","Home")%>">
				<img src="/images/btn/btn_back.jpg" alt="<%=Html._("Button.BackToShopping")%>" /></a></p>
		<%} %>
		<div class="eclear">
		</div>
		<%if (Model.User.IsRequester && Model.User.Company.UseGoodReceive && Model.IsGoodReceivePeriod)
	{%>
		<h1 style="text-align: center">
			<a href="<%=Url.Action("PurchaseGoodReceiveOrder","Report")%>">
				<%=Html._("CartDetail.ErrorGoodReceive")%></a></h1>
		<%} %>
		<%else
	{%>
		<!--GroupData-->
		<div class="GroupData 100percent">
			<%if (Model.Cart.ItemCountCart > 0)
	 {%>
			<%if (Model.ItemCostcenter.Any())
	 {%>
			<h3>
				<img alt="" src="../../images/theme/h3-list-icon.png" /><%=Html._("CartDetail.InformationPurchase")%></h3>
			<%if (Model.ItemCostcenter.Count() > 1)
	 {%>
			<div class="head-text-home" style="height: 30px; padding-top: 15px; padding-left: 20px;">
				<%=Html._("CartDetail.SelectedCostcenter")%>
				<span style="position: absolute; width: 330px; margin-left: 30px; margin-top: -5px;">
					<%Html.BeginForm("ShowApproverAndCostCenter", "Cart", FormMethod.Post);  %>
					<%=Html.DropDownList("costcenterId", new SelectList(Model.ItemCostcenter, "CostCenterID", "CostCenterDisplayname"), Html._("Admin.CostCenter.SelectList"), new { @style = "min-width: 230px;" })%>
					<input class="graybutton" name="ShowCostCenter.x" id="ShowCostCenter" type="submit"
						value="<%=Html._("Button.ShowCostCenter")%>" style="padding-top: 0px; margin-top: 0px;
						float: right; margin-top: -5px;" /><br />
					<span style="color: Blue">
						<%=TempData["ErrorCostCenter"]%></span>
					<%Html.EndForm(); %></span>
			</div>
			<%}%>
			<%}
	 else
	 {%>
			<h3 style="color: #354668; border: none">
				<%=Html._("CartDetail.EmptryCostCenter")%></h3>
			<%}%>
			<%}%>
			<% if (Model.ListUserApprover != null)
	  {%>
			<%if (Model.ListUserApprover.Where(o => o.Level == 1).Any())
	 {%>
			<%if (!Model.IsNotSetBudget)
	 {%>
			<!-- Column-Full-->
			<div class="Column-Full">
				<% if (Model.Cart.ItemCountCart > 0 && Model.CurrentUsedCostCenter != null)
	   {%>
				<h4>
					<%=Html._("CartDetail.MyCostCenter")%></h4>
				<div class="border2">
					<div class="position">
						<label>
							<%=Html._("CartDetail.CostcenterPurchase")%>
							:
						</label>
					</div>
					<div class="left">
						<span class="textSpanGray">[<%= Model.CurrentUsedCostCenter.CostCenterID%>]
							<%= Model.CurrentUsedCostCenter.CostCenterName%></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("CartDetail.CustId")%>
							:
						</label>
					</div>
					<div class="left">
						<span class="textSpanGray">
							<%= Model.CurrentUsedCostCenter.CostCenterCustID%></span>
					</div>
					<div class="eclear">
					</div>
				</div>
				<% Html.BeginForm("HandleActionCart", "Cart", FormMethod.Post, new { enctype = "multipart/form-data" });%>
				<!--ListUserApprover-->
				<%if (Model.ListUserApprover != null)
	  {%>
				<h4>
					<%=Html._("CartDetail.ApproverCostcenter")%></h4>
				<div class="border2">
					<div class="ApproveStyle">
						<%Html.RenderPartial("MyApprover", Model); %>
					</div>
					<div class="eclear">
					</div>
				</div>
				<%} %>
				<!--Costcenter Budget-->
				<%if (Model.CurrentBudget != null)
	  {%>
				<h4>
					<%=Html._("CartDetail.CurrentBudget")%>
					[<%= Model.CurrentUsedCostCenter.CostCenterID%>]
					<%= Model.CurrentUsedCostCenter.CostCenterName%></h4>
				<div class="border2">
					<div class="ApproveStyle">
						<%Html.RenderPartial("CurrentBudget", Model.CurrentBudget); %>
					</div>
					<div class="eclear">
					</div>
				</div>
				<%} %>
				<!--CostCenterAddress-->
				<%if (Model.CurrentUsedCostCenter != null)
	  {%>
				<h4>
					<%=Html._("CartDetail.InformationOrder")%></h4>
				<div class="border2">
					<div class="ApproveStyle">
						<%Html.RenderPartial("CostCenterAddress", Model.CurrentUsedCostCenter); %>
					</div>
					<div class="eclear">
					</div>
				</div>
				<%} %>
				<h4>
					<%=Html._("Order.ViewOrder.Remark")%></h4>
				<div class="border2">
					<label>
						<%=Html._("Order.ViewOrder.ApproverRemark")%>
						:</label>
					<input id="ApproverRemark" name="ApproverRemark" type="text" class="large" value="<%=Model.ApproverRemark == null ? "" : Model.ApproverRemark%>" maxlength="200" />
					<br />
					<label>
						<%=Html._("Order.ViewOrder.OFMRemark")%>
						:</label>
					<input id="OFMRemark" name="OFMRemark" type="text" class="large" value="<%=Model.OFMRemark == null ? "" : Model.OFMRemark%> " maxlength="200" />
					<br />
					<label>
						<%=Html._("Order.ViewOrder.ReferenceRemark")%>
						:</label>
					<input id="ReferenceRemark" name="ReferenceRemark" class="large" type="text" value="<%=Model.ReferenceRemark == null ? "" : Model.ReferenceRemark%> " maxlength="200" />
					<br />

					<!--Start AttachFile--->
					<label for="file">
						<%=Html._("Order.AttachFile")%>
						:</label>
					<input type="file" name="file" id="file" style="border: 1px solid #B8B4B4;" />
					<input type="submit" name="AttachFile.x" value="<%=Html._("Shared.MyImage.UploadFile")%>" />
					<a class="help1"><img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
					<%if (!String.IsNullOrEmpty(Model.Cart.CustFileName))
	   {%>
					<%=Model.Cart.CustFileName %>
					<input name="DeleteAttachFile.x" type="image" src="/images/icon/icon_wrong.png" alt="ลบเอกสารแนบ"
						title="ลบเอกสารแนบค่ะ" />
					<%} %>
					<%else
	   {%>
					<span></span>
					<%} %>
					<span style="color: Red;">
						<%=TempData["errormessage"]%></span>
					<%=Html.HiddenFor(m => m.Cart.ShoppingCartId)%>
					<%=Html.HiddenFor(m => m.Cart.CustFileName)%>
					<%=Html.HiddenFor(m => m.Cart.SystemFileName)%>
					<div id="All" style="padding: 10px; width: 200px; height: 30px; background-color: #CCCCCC;
						margin-left: 520px;">
						<p>
							<%=Html._("Content.AttachFile")%></p>
					</div>
					<br />
					<br />
					<!--End AttachFile--->

					<input id="CallBackRequestStatus" name="CallBackRequestStatus" type="checkbox" value="Yes" checked="checked" />
					<label for="CallBackRequestStatus" style="width: auto; display: inline; float: none">
						<%=Html._("CartDetail.CallBackRequestStatus")%></label>
				</div>
				<%if (Model.CurrentUsedCostCenter.UseAutoApprove && Model.ListUserApprover.Where(a => a.Level == 1).FirstOrDefault().Approver.UserId == Model.User.UserId)
	  { %>
				<div class="border2">
					<label style="width: auto; display: inline; float: none">
						<b>
							<%=Html._("Cart.CartDetail.ContentAutoApprove")%></b>
					</label>
					<br />
					<%=Html.RadioButton("AutoApprove", "Yes", true, new { id = "AutoApprove" })%>
					<label for="AutoApprove" style="width: auto; display: inline; float: none">
						<%=Html._("Cart.CartDetail.AutoApprove")%></label>
					<%=Html.RadioButton("AutoApprove", "No", false, new { id = "NotAutoApprove" })%>
					<label for="NotAutoApprove" style="width: auto; display: inline; float: none">
						<%=Html._("Cart.CartDetail.NotAutoApprove")%></label>
					<span style="color: Blue">
						<%=TempData["Error"]%></span>
				</div>
				<%} %>
				<%if (Model.Cart.IsOverCreditlimit || Model.Cart.SendToAdminAppBudg)
				{%>
				<div class="border2">
					<%if (Model.Cart.IsOverCreditlimit)
					{%>
					<label style="color: #FF0000; width: 900px;">
						<%=Html._("ConfirmOrder.ErrorOverCreditLimit")%><br />
					</label>
					<%}%>
					<%if (Model.Cart.SendToAdminAppBudg)
					{%>
					<label style="color: #FF0000; width: 900px;">
						<%=Html._("CartDetail.CurrentBudget.Caution")%><br />
					</label>
					<%}%>
				</div>
				<%}%>
				<div>
					<p>
						<%=Html.Hidden("costcenterId", Model.CurrentUsedCostCenter.CostCenterID)%>
						<a href="<%=Url.Action("Index","Home")%>">
							<img src="/images/btn/btn_back.jpg" alt="<%=Html._("Button.BackToShopping")%>" class="left" /></a>
						<input type="image" src="/images/btn/btn_next_step.jpg" name="CheckLogIn.x" alt="<%=Html._("Button.NextStep")%>"
							class="right" />
					</p>
				</div>
				<%Html.EndForm(); %>
				<%}%>
			</div>
			<!--end Column-Full-->
			<%} %>
			<%else
	 {%>
			<h1 style="text-align: center">
				<%=Html._("CartDetail.EmptryBudget")%></h1>
			<%} %>
			<%}%>
			<%	else
	 { %>
			<h3 style="color: #354668; border: none">
				<%=Html._("คุณยังไม่ได้ผูกสิทธิ์ให้กับ CostCenter นี้ค่ะ")%></h3>
			<%} %>
			<% } %>
		</div>
		<!-- end GroupData-->
		<%} %>
	</div>
	<div class="eclear">
	</div>
</asp:Content>
