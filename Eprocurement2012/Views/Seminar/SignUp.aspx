﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Seminar>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>ลงทะเบียนสำหรับผู้เข้าสัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์ </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="ลงทะเบียนสำหรับผู้เข้าสัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์" />
	<meta name="keywords" content="ลงทะเบียน, สัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/text.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/reset.css" />
	<script src="/js/jquery-1.8.0.min.js" type="text/javascript"></script>
	<style type="text/css">
		.alert-box
		{
			color: #555;
			border-radius: 10px;
			font-family: Tahoma,Geneva,Arial,sans-serif;
			font-size: 11px;
			padding: 10px 10px 10px 36px;
			margin: 10px;
		}
		.alert-box span
		{
			font-weight: bold;
			text-transform: uppercase;
		}
		.error
		{
			background: #ffecec url('/images/seminar/error.png') no-repeat 10px 50%;
			border: 1px solid #f5aca6;
		}
		.success
		{
			background: #e9ffd9 url('/images/seminar/success.png') no-repeat 10px 50%;
			border: 1px solid #a6ca8a;
		}
		.warning
		{
			background: #fff8c4 url('/images/seminar/warning.png') no-repeat 10px 50%;
			border: 1px solid #f2c779;
		}
		.notice
		{
			background: #e3f7fc url('/images/seminar/notice.png') no-repeat 10px 50%;
			border: 1px solid #8ed9f6;
		}
	</style>
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {

			if ($("#rdCorporate").is(":checked")) {
				$("#CompInput1").show();
				$("#CompInput2").hide();
			}
			else {
				$("#CompInput1").hide();
				$("#CompInput2").show();
			}

			$("#rdCorporate").click(function () {
				$("#CompInput1").show();
				$("#CompInput2").hide();
			});

			$("#rdPersonal").click(function () {
				$("#CompInput1").hide();
				$("#CompInput2").show();
			});

		});
	</script>
</head>
<body>
	<% Html.BeginForm("SignUp", "Seminar", FormMethod.Post); %>
	<div id="Close" style="font-size: 18px; font-weight: bold; background-color: #FFFFFF;
		color: #FF9933; font-family: 'Times New Roman', Times, serif;">
		<%--<marquee direction="left" scrollamount="10">บมจ.ซีโอแอล ขอขอบพระคุณท่านที่ให้ความสนใจ และสมัครเข้าร่วมงานสัมมนา "เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์ คร้งที่ 3" โดยในขณะนี้ มีผู้สมัครเข้าร่วมงานมาเป็นจำนวนมากครบตามที่กำหนดเรียบร้อยแล้ว สำหรับผู้สนใจจะเข้าร่วมงาน สามารถลงทะเบียนเพื่อสำรองที่นั่งไว้สำหรับงานสัมมนาในครั้งต่อไปค่ะ ขอบคุณค่ะ</marquee>--%>
	</div>
	<div id="container" align="center">
		<div align="center" style="margin-top: 50px; width: 1024px; background: white;">
			<div class="box-seminar" style="background: #aaaca7;">
				<a href="<%=Url.Action("SeminarContent")%>" target="_blank">
					<img src="/images/seminar/head.jpg" alt="อ่านรายละเอียดงานสัมมนา" width="918" height="246"
						border="0" title="อ่านรายละเอียดงานสัมมนา" /></a>
			</div>
			<div class="box-seminar" style="margin-top: 10px;">
				<a href="<%=Url.Action("SeminarContent")%>" target="_blank">
					<img src="/images/seminar/Head-T.JPG" border="0" alt="อ่านรายละเอียดงานสัมมนา" title="อ่านรายละเอียดงานสัมมนา" /></a><br />
				<br />
			</div>
			<div class="box-seminar">
				<div style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; text-align: left;">
					<img src="/images/seminar/h-type.gif" /><br />
				</div>
				<div align="left">
					<br />
					<input id="rdCorporate" name="CustType" type="radio" value="Corporate" <%=Model.CheckIsCorporate %> />ลูกค้ากลุ่มองค์กร
					<input id="rdPersonal" name="CustType" type="radio" value="Personal" <%=Model.CheckIsPersonal %> />บุคคลทั่วไป
					<%=TempData["ErrorCustType"]%></div>
			</div>
			<br />
			<div style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; text-align: left;">
				<img src="/images/seminar/h-company.gif" /><br />
			</div>
			<fieldset id="CompInput1">
				<br />
				<table width="750px" border="0" cellpadding="0" cellspacing="0" style="text-align:left;">
					<tr>
						<td align="right" style="width: 200px;">
							ชื่อบริษัท*
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<input id="txtInvAddr1" name="InvAddr1" style="width: 250px;" type="text" maxlength="55"
								tabindex="2" value="<%=Model.InvAddr1 %>" />
							&nbsp;<span class="des"><i>บริษัท ซีโอแอล จำกัด (มหาชน)</i></span>
							<br />
							<%=TempData["ErrorCompName"]%>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							ประเภทธุรกิจ*
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td>
							<%=Html.DropDownListFor(m => m.BusinessType, new SelectList(Model.ListBusinessType, "Name", "Name"), "กรุณาระบุประเภทธุรกิจ")%>
							<%=TempData["ErrorBusinessType"]%>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							ทุนจดทะเบียนบริษัท*
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td>
							<%=Html.TextBoxFor(m => m.Capital, new { @Value = ""})%>
							<%=TempData["ErrorCapital"]%>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							จำนวนพนักงาน (คน)*
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td>
							<%=Html.TextBoxFor(m => m.CustEmpNum,new { @Value = ""})%>
							<%=TempData["ErrorCustEmpNum"]%>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							ที่อยู่*
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<input id="txtInvAddr2" name="InvAddr2" style="width: 250px;" type="text" maxlength="55"
								tabindex="3" value="<%=Model.InvAddr2 %>" />
							&nbsp;<span class="des"><i>24 ซอยอ่อนนุช 66/1</i></span>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							*
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<input id="txtInvAddr3" name="InvAddr3" style="width: 250px;" type="text" maxlength="55"
								tabindex="4" value="<%=Model.InvAddr3 %>" />
							&nbsp;<span class="des"><i>แขวงสวนหลวง เขตสวนหลวง</i></span>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							*
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<input id="txtInvAddr4" name="InvAddr4" style="width: 250px;" type="text" maxlength="55"
								tabindex="5" value="<%=Model.InvAddr4 %>" />
							&nbsp;<span class="des"><i>กรุงเทพมหานคร 10250</i></span>
							<br />
							<%=TempData["ErrorInvAddr"]%>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							เบอร์โทรศัพท์*
						</td>
						<td style="width: 8px">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<input id="txtCompPhone" name="PhoneNo" style="width: 250px;" type="text" maxlength="55"
								tabindex="6" value="<%=Model.PhoneNo %>" />
							&nbsp;<span class="des"><i>027395555</i></span>
							<br />
							<%=TempData["ErrorPhoneNo"]%>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							เบอร์แฟ็กซ์
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<input id="txtCompFax" name="FaxNo" style="width: 250px;" type="text" maxlength="55"
								tabindex="7" value="<%=Model.FaxNo %>" />
							&nbsp;<span class="des"><i>027211717</i></span>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							Website
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<input id="txtCompWeb" name="Website" style="width: 250px;" type="text" maxlength="55"
								tabindex="8" value="<%=Model.Website %>" />
							&nbsp;<span class="des"><i>www.officemate.co.th</i></span>
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							&nbsp;
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<br />
							<b>ท่านเป็นลูกค้าออฟฟิศเมทหรือไม่?</b><br />
							<br />
							<input id="RadIsMember" name="IsCustOFM" type="radio" value="true" <%=Model.CheckIsCustomerOFM %>
								tabindex="10" />ใช่ &nbsp; กรุณาระบุรหัสลูกค้า :
							<input id="txtCustID" name="CustID" style="width: 250px;" type="text" maxlength="7"
								tabindex="9" value="<%=Model.CustID %>" /><br />
							<input id="RadIsNotMember" name="IsCustOFM" type="radio" value="false" <%=Model.CheckNoneCustomerOFM %>
								tabindex="12" />ไม่ใช่
							<br />
							<%=TempData["ErrorCustID"]%>
						</td>
					</tr>
				</table>
			</fieldset>
			<fieldset id="CompInput2">
				<br />
				<table width="750px" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td align="right" style="width: 200px;">
							ชื่อบริษัท
						</td>
						<td style="height: 24px; width: 8px;">
						</td>
						<td align="left" style="height: 40px" valign="middle">
							<input id="txtCompName" name="CompName" style="width: 250px;" type="text" maxlength="55"
								tabindex="2" value="<%=Model.CompName %>" /><br />
							&nbsp;<span class="des"><i>บริษัท ซีโอแอล จำกัด (มหาชน)</i></span>
						</td>
					</tr>
				</table>
			</fieldset>
			<div style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; text-align: left;">
				<img src="/images/seminar/h-entry.gif" /><br />
			</div>
			<fieldset>
				<br />
				<table width="750px" border="0" cellpadding="0" cellspacing="0" style="text-align:left;">
					<tr>
						<td colspan="3">
							<u>ผู้เข้าร่วมสัมมนาท่านที่ 1</u>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td align="right" style="width: 200px;">
							ชื่อ-นามสกุล*
						</td>
						<td style="width: 8px">
						</td>
						<td valign="middle" align="left" style="height: 24px">
							<input id="txtName1" name="ContactName1" style="width: 250px;" type="text" maxlength="100"
								tabindex="10" value="<%=Model.ContactName1 %>" />
							<br />
							<%=TempData["ErrorContactName1"]%>
						</td>
					</tr>
					<tr>
						<td align="right">
							ตำแหน่ง*
						</td>
						<td style="width: 8px">
						</td>
						<td align="left" valign="middle" style="height: 24px">
							<input id="txtPosition1" name="ContactPosition1" style="width: 250px;" type="text"
								maxlength="100" tabindex="11" value="<%=Model.ContactPosition1 %>" />
							<br />
							<%=TempData["ErrorContactPosition1"]%>
						</td>
					</tr>
					<tr>
						<td align="right">
							เบอร์โทรศัพท์*
						</td>
						<td style="width: 8px">
						</td>
						<td valign="middle" align="left" style="height: 24px">
							<input id="txtPhone1" name="ContactPhone1" style="width: 152px;" type="text" maxlength="10"
								tabindex="12" value="<%=Model.ContactPhone1 %>" />
							<br />
							<%=TempData["ErrorContactPhone1"]%>
						</td>
					</tr>
					<tr>
						<td align="right">
							เบอร์มือถือ*
						</td>
						<td style="width: 8px">
						</td>
						<td align="left" valign="middle" style="height: 24px">
							<input id="txtMobile1" name="ContactMobile1" style="width: 152px;" type="text" maxlength="10"
								tabindex="12" value="<%=Model.ContactMobile1 %>" />
							<br />
							<%=TempData["ErrorContactMobile1"]%>
						</td>
					</tr>
					<tr>
						<td align="right">
							อีเมล์*
						</td>
						<td style="width: 8px">
						</td>
						<td valign="middle" align="left" style="height: 24px">
							<input id="txtMail1" name="ContactMail1" style="width: 250px;" type="text" maxlength="100"
								tabindex="14" value="<%=Model.ContactMail1 %>" />
							<br />
							<%=TempData["ErrorContactMail1"]%>
							<%=TempData["ErrorFormatContactMail1"]%>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							<u>ผู้เข้าร่วมสัมมนาท่านที่ 2</u>
						</td>
					</tr>
					<tr>
						<td colspan="3">
							&nbsp;
						</td>
					</tr>
					<tr>
						<td align="right">
							ชื่อ-นามสกุล
						</td>
						<td style="width: 8px">
						</td>
						<td valign="middle" align="left" style="height: 24px">
							<input id="txtName2" name="ContactName2" style="width: 250px;" type="text" maxlength="100"
								tabindex="14" value="<%=Model.ContactName2 %>" />
						</td>
					</tr>
					<tr>
						<td align="right">
							ตำแหน่ง
						</td>
						<td style="width: 8px">
						</td>
						<td align="left" style="height: 24px" valign="middle">
							<input id="txtPosition2" name="ContactPosition2" style="width: 250px;" type="text"
								maxlength="100" tabindex="16" value="<%=Model.ContactPosition2 %>" />
						</td>
					</tr>
					<tr>
						<td align="right">
							เบอร์โทรศัพท์
						</td>
						<td style="width: 8px">
						</td>
						<td valign="middle" align="left" style="height: 24px">
							<input id="txtPhone2" name="ContactPhone2" style="width: 152px;" type="text" maxlength="100"
								tabindex="17" value="<%=Model.ContactPhone2 %>" />
						</td>
					</tr>
					<tr>
						<td align="right">
							เบอร์มือถือ
						</td>
						<td style="width: 8px">
						</td>
						<td align="left" valign="middle" style="height: 24px">
							<input id="txtMobile2" name="ContactMobile2" style="width: 152px;" type="text" maxlength="100"
								tabindex="18" value="<%=Model.ContactMobile2 %>" />
						</td>
					</tr>
					<tr>
						<td align="right">
							อีเมล์
						</td>
						<td style="width: 8px">
						</td>
						<td valign="middle" align="left" style="height: 24px">
							<input id="txtMail2" name="ContactMail2" style="width: 250px;" type="text" maxlength="100"
								tabindex="19" value="<%=Model.ContactMail2 %>" />
							<%=TempData["ErrorFormatContactMail2"]%>
						</td>
					</tr>
				</table>
			</fieldset>
			<div style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; text-align: left;">
				<img src="/images/seminar/h-other.gif" /><br />
			</div>
			<fieldset>
				<br />
				<table width="750px" border="0" cellpadding="0" cellspacing="0">
					<tr>
						<td style="text-align:left;">
							<strong>ท่านได้รับข้อมูลการสัมมนามาจากช่องทางใด?</strong>
							<br />
							&nbsp;
							<input id="radNews1" name="NewsChannel" type="radio" value="1" <%=Model.CheckIsEmailChanel%> />
							<label for="radNews1">
								e-newsletter (e-mail เชิญจาก OfficeMate)</label>
							<br />
							&nbsp;
							<input id="radNews2" name="NewsChannel" type="radio" value="2" <%=Model.CheckIsWebOFMChanel%> />
							<label for="radNews2">
								เว็บไซต์ www.OfficeMate.co.th</label>
							<br />
							&nbsp;
							<input id="radNews3" name="NewsChannel" type="radio" value="3" <%=Model.CheckIsOFMChanel%> />
							<label for="radNews3">
								เจ้าหน้าที่ฝ่ายขาย OfficeMate</label>
							<br />
							&nbsp;
							<input id="radNews4" name="NewsChannel" type="radio" value="4" <%=Model.CheckIsOtherChanel%> />
							<label for="radNews4">
								อื่นๆ</label>
							&nbsp; โปรดระบุ : &nbsp;
							<input id="txtNews" name="NewsChannelOther" style="width: 200px;" type="text" maxlength="100"
								tabindex="20" value="<%=Model.NewsChannelOther %>" />
							<%--&nbsp;<span class="des"><i>(แฟกซ์)</i></span>--%>
							<%=TempData["ErrorNewsChannel"]%>
							<%=TempData["ErrorNewsChannelOther"]%>
						</td>
					</tr>
					<tr>
						<td>
							&nbsp;
						</td>
					</tr>
					<tr>
						<td style="text-align:left;">
							<strong>องค์กรของท่านมีการใช้ระบบภายในอื่นๆหรือไม่ ? </strong>
							<br />
							&nbsp;
							<input id="radSystem1" name="HaveSystem" type="radio" value="false" <%=Model.CheckIsNoneSystem%> />
							<label for="radSystem1">
								ไม่มี</label>
							<br />
							&nbsp;
							<input id="radSystem2" name="HaveSystem" type="radio" value="true" <%=Model.CheckIsSystem%> />
							<label for="radSystem2">
								มี</label>
							&nbsp; โปรดระบุ : &nbsp;
							<input id="txtSystem" name="ERPSystem" style="width: 100px;" type="text" maxlength="100" />
							&nbsp;<span class="des"><i>เช่น SAP, Oracle</i></span>
							<%=TempData["ErrorERPSystem"]%>
						</td>
					</tr>
				</table>
			</fieldset>
			<br />
			<div style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; padding: 10px 0px;
				height: 130px;">
				<%--<div style="float: left; text-align: left; padding: 10px;">
					สมัครด่วน (รับจำนวนจำกัด)<br />
					สำรองที่นั่ง ไม่เกิน 2 ท่าน/บริษัท<br />
					ลงทะเบียนร่วมงาน วันนี้ ถึง 25 มีนาคม 2558<br />
					สอบถามเพิ่มเติมได้ที่ ศูนย์บริการ OfficeMate e-Procurement Tel. 02-739-5555 กด 2
					และ กด 3<br />
					www.officemate.co.th<br />
					<br />
					<a href="<%=Url.Action("SeminarContent")%>" target="_blank">กลับไปดูรายละเอียดงานสัมมนา</a>
				</div>--%>
				<div style="float: left; text-align: left; padding: 10px;">
					ลงทะเบียนสำรองที่นั่งด่วน! (จำนวนจำกัด)<br />
					ตั้งแต่วันนี้ - 9 มีนาคม 2559 จำกัดไม่เกิน 2 ท่าน/บริษัท<br />
					เจ้าหน้าที่ OfficeMate จะโทรกลับไปหาท่านเพื่อทำการยืนยันที่นั่งก่อนวันงาน<br />
					สอบถามเพิ่มเติมได้ที่ ศูนย์บริการ OfficeMate e-Procurement Tel. 02-739-5555 กด 2 , กด 3<br />
					www.officemate.co.th<br />
					<br />
					<a href="<%=Url.Action("SeminarContent")%>" target="_blank">กลับไปดูรายละเอียดงานสัมมนา</a>
				</div>
				<div style="float: right; padding: 10px;">
					<input id="btnSubmit" type="image" src="/images/seminar/btn_register.jpg" value="ลงทะเบียน" />
				</div>
				<br class="clear" />
			</div>
		</div>
		<div align="left" style="font-size: 12px; padding: 20px 0px; width: 1024px; background: white;">
			<div style="padding: 10px;">
				<b>Copyright 2010 &copy; OfficeMate e-Procurement. All Rights Reserved.</b>
				<br />
				<p>
					บริษัท ซีโอแอล จำกัด (มหาชน) : 24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ
					10250<br />
					โทรศัพท์ 02-739-5555 (120 สาย), แฟ็กซ์ 02-721-1717 (30 สาย)
				</p>
			</div>
			<br />
			<script type="text/javascript">
				var _gaq = _gaq || [];
				_gaq.push(['_setAccount', 'UA-11520015-2']);
				_gaq.push(['_trackPageview']);
				(function () {
					var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
					ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
					var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
				})();
			</script>
		</div>
	</div>
	<%Html.EndForm(); %>
	<script language='javascript1.1' src='http://hits.truehits.in.th/data/p0027148.js'></script>
</body>
</html>
