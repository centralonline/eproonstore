﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage<IEnumerable<Eprocurement2012.Models.Seminar>>" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>รายชื่อผู้ลงทะเบียนเข้าร่วมสัมมนา "คุยแบบหมดเปลือกเรื่องช้อปออนไลน์ กับกูรูจัดซื้อมือทองของไทย"
	</title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="ลงทะเบียนสำหรับผู้เข้าสัมมนา คุยแบบหมดเปลือกเรื่องช้อปออนไลน์ กับกูรูจัดซื้อมือทองของไทย" />
	<meta name="keywords" content="ลงทะเบียน, สัมมนา คุยแบบหมดเปลือก, ช้อปออนไลน์, กูรูจัดซื้อมือทองของไทย" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/text.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/seminar.css" />
	<style type="text/css">
		.style5
		{
			font-family: Tahoma;
			font-size: 11px;
			color: #fff;
			padding: 3px;
		}
		.style6
		{
			font-family: Tahoma;
			font-size: 10px;
			color: #555;
			padding: 3px;
		}
	</style>
</head>
<body>
	<div id="container2" align="center">
		<div align="center" style="margin-top: 50px;">
			<%--<a href="<%=Url.Action("SeminarContent")%>">
				<img src="/images/seminar/head2.png" width="918" height="170" alt="อ่านรายละเอียดงานสัมมนา"
					title="อ่านรายละเอียดงานสัมมนา" /></a>--%>
			<a href="<%=Url.Action("SeminarContent")%>">
				<img src="/images/seminar/head.jpg" width="918" height="246" alt="อ่านรายละเอียดงานสัมมนา"
					title="อ่านรายละเอียดงานสัมมนา" /></a>
			<br />
			<div class="box-seminar">
				<%Html.BeginForm("ExportSeminarRegister", "Seminar", FormMethod.Get); %>
				<input id="export" type="submit" value="Export to Excel" />
				<%Html.EndForm(); %>
			</div>
			<div class="box-seminar">
				<div>
					<%if (Model != null)
	   { %>
					<table id="TBSeminar" class="tableSeminar" style="width: 100%;">
						<thead>
							<tr style="background-color: #000; height: 30px;">
								<th align="center" rowspan="2" class="style5">
									<br />
									ลำดับ
								</th>
								<th align="center" rowspan="2" class="style5">
									<br />
									วันที่ลงทะเบียน
								</th>
								<th align="center" colspan="10" class="style5" style="background-color: #333;">
									ข้อมูลองค์กร
								</th>
								<th align="center" colspan="4" class="style5" style="background-color: #555;">
									ข้อมูลผู้เข้าร่วมสัมมนาท่านที่ 1
								</th>
								<th align="center" colspan="4" class="style5" style="background-color: #777;">
									ข้อมูลผู้เข้าร่วมสัมมนาท่านที่ 2
								</th>
							</tr>
							<tr style="background-color: #f80; height: 30px;">
								<th align="left" class="style5">
									ประเภท
								</th>
								<th align="left" class="style5">
									ชื่อบริษัท<br />
									[รหัสลูกค้า]
								</th>
								<th align="left" class="style5">
									ประเภทธุรกิจ
								</th>
								<th align="left" class="style5">
									ทุนจดทะเบียนบริษัท
								</th>
								<th align="left" class="style5">
									จำนวนพนักงาน
								</th>
								<th align="left" class="style5">
									ที่อยู่
								</th>
								<th align="left" class="style5">
									โทรศัพท์ /<br />
									FAX
								</th>
								<th align="left" class="style5">
									Website
								</th>
								<th align="left" class="style5">
									ทราบข่าวจาก
								</th>
								<th align="left" class="style5">
									ERPSystem
								</th>
								<th align="left" class="style5" style="background-color: #bb5;">
									ชื่อ-นามสกุล
								</th>
								<th align="left" class="style5" style="background-color: #bb5;">
									ตำแหน่ง
								</th>
								<th align="left" class="style5" style="background-color: #bb5;">
									โทรศัพท์ /<br />
									mobile
								</th>
								<th align="left" class="style5" style="background-color: #bb5;">
									อีเมล
								</th>
								<th align="left" class="style5" style="background-color: #cc7;">
									ชื่อ-นามสกุล
								</th>
								<th align="left" class="style5" style="background-color: #cc7;">
									ตำแหน่ง
								</th>
								<th align="left" class="style5" style="background-color: #cc7;">
									โทรศัพท์ /<br />
									mobile
								</th>
								<th align="left" class="style5" style="background-color: #cc7;">
									อีเมล
								</th>
							</tr>
						</thead>
						<tbody>
							<%int count = 0; %>
							<%foreach (var item in Model)
		 { %>
							<%count++; %>
							<tr style="border-bottom: 1px solid #aaa;">
								<td class="style6">
									<%=count%>
								</td>
								<td class="style6">
									<%=item.CreateOn.ToString("dd/MM/yyyy HH:mm")%>
								</td>
								<td class="style6">
									<%=item.CustType%>
								</td>
								<td class="style6">
									<br />
									<%=item.CompName%><br />
									<%=!string.IsNullOrEmpty(item.CustID) ? "[" + item.CustID + "]" : ""%>
								</td>
								<td class="style6">
									<%=item.BusinessType%>
								</td>
								<td class="style6" style="text-align: right;">
									<%=item.Capital%>
								</td>
								<td class="style6" style="text-align: center;">
									<%=item.CustEmpNum%>
								</td>
								<td>
									<span class="style6">
										<%=item.InvAddr2%></span>
									<br />
									<span class="style6">
										<%=item.InvAddr3%></span>
									<br />
									<span class="style6">
										<%=item.InvAddr4%></span>
								</td>
								<td class="style6">
									<%=item.PhoneNo%>
									/<br />
									<%=item.FaxNo%>
								</td>
								<td class="style6">
									<%=item.Website%>
								</td>
								<td class="style6">
									<%=item.NewsChannel%>
								</td>
								<td class="style6">
									<%=item.ERPSystem%>
								</td>
								<td class="style6" style="background-color: #ddd;">
									<%=item.ContactName1%>
								</td>
								<td class="style6" style="background-color: #ddd;">
									<%=item.ContactPosition1%>
								</td>
								<td class="style6" style="background-color: #ddd;">
									<%=item.ContactPhone1%>
									/<br />
									<%=item.ContactMobile1%>
								</td>
								<td class="style6" style="background-color: #ddd;">
									<%=item.ContactMail1%>
								</td>
								<td class="style6" style="background-color: #eee;">
									<%=item.ContactName2%>
								</td>
								<td class="style6" style="background-color: #eee;">
									<%=item.ContactPosition2%>
								</td>
								<td class="style6" style="background-color: #eee;">
									<%=item.ContactPhone2%>
									/<br />
									<%=item.ContactMobile2%>
								</td>
								<td class="style6" style="background-color: #eee;">
									<%=item.ContactMail2%>
								</td>
							</tr>
							<%} %>
						</tbody>
					</table>
					<%} %>
				</div>
			</div>
			<div align="left" style="font-size: 12px; padding: 20px;">
				<b>Copyright 2010 &copy; OfficeMate e-Procurement. All Rights Reserved.</b>
				<br />
				<p>
					บริษัท ซีโอแอล จำกัด (มหาชน) : 24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ
					10250<br />
					โทรศัพท์ 02-739-5555 (120 สาย), แฟ็กซ์ 02-721-1717 (30 สาย)</p>
				<br />
				<script type="text/javascript">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', 'UA-11520015-2']);
					_gaq.push(['_trackPageview']);
					(function () {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})();
				</script>
				<script language='javascript1.1' src='http://hits.truehits.in.th/data/p0027148.js'></script>
			</div>
		</div>
	</div>
</body>
</html>
