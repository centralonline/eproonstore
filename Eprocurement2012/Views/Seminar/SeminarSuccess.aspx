﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>ลงทะเบียนสำหรับผู้เข้าสัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์ </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="ลงทะเบียนสำหรับผู้เข้าสัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์" />
	<meta name="keywords" content="ลงทะเบียน, สัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/text.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/reset.css" />
	<script src="/js/jquery-1.8.0.min.js" type="text/javascript"></script>
	<style type="text/css">
		.alert-box
		{
			color: #555;
			border-radius: 10px;
			font-family: Tahoma,Geneva,Arial,sans-serif;
			font-size: 11px;
			padding: 10px 10px 10px 36px;
			margin: 10px;
		}
		.alert-box span
		{
			font-weight: bold;
			text-transform: uppercase;
		}
		.error
		{
			background: #ffecec url('/images/seminar/error.png') no-repeat 10px 50%;
			border: 1px solid #f5aca6;
		}
		.success
		{
			background: #e9ffd9 url('/images/seminar/success.png') no-repeat 10px 50%;
			border: 1px solid #a6ca8a;
		}
		.warning
		{
			background: #fff8c4 url('/images/seminar/warning.png') no-repeat 10px 50%;
			border: 1px solid #f2c779;
		}
		.notice
		{
			background: #e3f7fc url('/images/seminar/notice.png') no-repeat 10px 50%;
			border: 1px solid #8ed9f6;
		}
	</style>
</head>
<body>
	<% Html.BeginForm("SignUp", "Seminar", FormMethod.Post); %>
	<div id="Close" style="font-size: 18px; font-weight: bold; background-color: #FFFFFF;
		color: #FF9933; font-family: 'Times New Roman', Times, serif;">
		<%--<marquee direction="left" scrollamount="10">บมจ.ซีโอแอล ขอขอบพระคุณท่านที่ให้ความสนใจ และสมัครเข้าร่วมงานสัมมนา "เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์ คร้งที่ 3" โดยในขณะนี้ มีผู้สมัครเข้าร่วมงานมาเป็นจำนวนมากครบตามที่กำหนดเรียบร้อยแล้ว สำหรับผู้สนใจจะเข้าร่วมงาน สามารถลงทะเบียนเพื่อสำรองที่นั่งไว้สำหรับงานสัมมนาในครั้งต่อไปค่ะ ขอบคุณค่ะ</marquee>--%>
	</div>
	<div id="container" align="center">
		<div align="center" style="margin-top: 50px; width: 1024px;">
			<a href="<%=Url.Action("SeminarContent")%>">
				<img src="/images/seminar/head.png" alt="อ่านรายละเอียดงานสัมมนา" width="918" height="246"
					border="0" title="อ่านรายละเอียดงานสัมมนา" /></a><br />
			<div class="box-seminar">
				<a href="<%=Url.Action("SeminarContent")%>">
					<img src="/images/seminar/h-time.gif" border="0" alt="อ่านรายละเอียดงานสัมมนา" title="อ่านรายละเอียดงานสัมมนา" /></a><br />
				<br />
				<div style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa;">
					<h1>
						ท่านได้ลงทะเบียบเรียบร้อยแล้วค่ะ</h1>
					<br />
					<a href="<%=Url.Action("SignUp","Seminar")%>">กลับสู่หน้าลงทะเบียน</a>
				</div>
				<div style="border-bottom: 1px solid #aaa; border-top: 1px solid #aaa; padding: 10px 0px;">
					<div style="float: left;">
						<a href="<%=Url.Action("SeminarContent")%>">กลับไปดูรายละเอียดงานสัมมนา</a></div>
					<br class="clear" />
				</div>
			</div>
			<div align="left" style="font-size: 12px; padding: 20px;">
				<b>Copyright 2010 &copy; OfficeMate e-Procurement. All Rights Reserved.</b>
				<br />
				<p>
					บริษัท ซีโอแอล จำกัด (มหาชน) : 24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง เขตสวนหลวง กรุงเทพฯ
					10250<br />
					โทรศัพท์ 02-739-5555 (120 สาย), แฟ็กซ์ 02-721-1717 (30 สาย)</p>
				<br />
				<script language='javascript1.1' src='http://hits.truehits.in.th/data/p0027148.js'></script>
				<script type="text/javascript">
					var _gaq = _gaq || [];
					_gaq.push(['_setAccount', 'UA-11520015-2']);
					_gaq.push(['_trackPageview']);
					(function () {
						var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
						ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
						var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
					})();
				</script>
			</div>
		</div>
	</div>
	<%Html.EndForm(); %>
</body>
</html>
