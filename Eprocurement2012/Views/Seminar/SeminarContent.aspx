﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="description" content="สัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์" />
	<meta name="keywords" content="สัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/reset.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/text.css" media="screen" />
	<link rel="stylesheet" type="text/css" href="/css/seminar/seminar.css" />
	<title>สัมมนา เทรนด์ใหม่กับการพัฒนาธุรกิจผ่านโลกออนไลน์</title>
</head>
<body>
	<div id="container" align="center" >
		<%--<div align="left" style="background-color: #fff; width: 990px;">
			<a href="<%=Url.Action("SignUp", "Seminar")%>">
				<img src="/images/seminar/E-pro-Agenda.jpg" width="990" height="1466" alt=""
					title="" border="0" /></a><br />
		</div>--%>
		<div align="center">
			<%--<a href="<%=Url.Action("SignUp", "Seminar")%>">--%>
				<img src="/images/seminar/AW-InvitationCard.jpg" alt="" title="" border="0" /><%--</a>--%><br />
		</div>
        <div align="center">
		<script type="text/javascript">
			var _gaq = _gaq || [];
			_gaq.push(['_setAccount', 'UA-11520015-2']);
			_gaq.push(['_trackPageview']);
			(function () {
				var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
				ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
				var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
			})();
		</script>
		<script language='javascript1.1' src='http://hits.truehits.in.th/data/p0027148.js'></script>
        </div>
	</div>
</body>
</html>
