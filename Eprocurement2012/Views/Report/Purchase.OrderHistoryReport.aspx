﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/Report.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="spaceleft20">
		<div class="GroupData">
			<h3>
				<img src="../../images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("Report.Menu.OrderHistory")%></h3>
		</div>
		<div style="margin: 10px auto">
			<strong id="before-list">คุณสามารถเลือกดู &gt;</strong>
			<ul id="trackOrder-Tabs">
				<li><%=Html.ActionLink(Html._("Report.MonthFilter.1Week"), "PurchaseOrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.SevenDaysLater }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.SevenDaysLater ? "active-link" : "" })%></li>
				<li><%=Html.ActionLink(Html._("Report.MonthFilter.1Month"), "PurchaseOrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.OneMonthLater }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.OneMonthLater ? "active-link" : "" })%></li>
				<li><%=Html.ActionLink(Html._("Report.MonthFilter.3Month"), "PurchaseOrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.ThreeMonthLater }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.ThreeMonthLater ? "active-link" : "" })%></li>
				<li><%=Html.ActionLink(Html._("Report.MonthFilter.6Month"), "PurchaseOrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.SixMonthLater }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.SixMonthLater ? "active-link" : "" })%></li>
				<li><%=Html.ActionLink(Html._("Report.Filter.All"), "PurchaseOrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.All }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.All ? "active-link" : "" })%></li>
			</ul>
		</div>
		<div style="margin-top: 20px;">
			<div class="GroupData">
				<h4>
					<%=Html._("Report.SearchOrder")%></h4>
			</div>
			<%Html.BeginForm("SearchPurchaseOrderHistory", "Report", FormMethod.Post, new { id = "formOrderHistory" }); %>
			<div>
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
				<label for="CheckOrderDate">
					<%=Html._("Order.ViewDataOrder.OrderDate")%></label><br />
				<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
			</div>
			<div>
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderId, new { id = "CheckOrderId" })%>
				<label for="CheckOrderId">
					<%=Html._("Order.ViewDataOrder.OrderId")%></label><br />
				<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderId)%>
			</div>
			<input class="graybutton" type="submit" value="<%=Html._("Button.Search")%>" />
			<span style="color: Red;">
				<%=TempData["Message"] %></span>
			<%Html.EndForm(); %>
		</div>
		<div>
			<%if (Model.ListOrder.Orders != null)
	 {
		 if (Model.TotalOrdersCount != 0)
		 {%>
			<div class="pager">
				<%if (Model.MonthFilter.HasValue)
	  { %>
				<%=Html.Pager(Model.PageSize, Model.PageNumber, Model.TotalOrdersCount, new { monthFilter = Model.MonthFilter })%>
				<%
				} %>
				<%Model.ListOrder.Index = ((Model.PageNumber - 1) * Model.PageSize);%>
			</div>
			<%if (Model.MonthFilter.HasValue)
	 {%>
			<%Html.BeginForm("ExportPurchaseOrderHistory", "ExportExcel", new { monthFilter = Model.MonthFilter }); %>
			<input class="graybutton" id="export" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<%} %>
			<%else
	 {%>
			<%Html.BeginForm("ExportPurchaseOrderHistory", "ExportExcel", Model.AdvanceSearchField); %>
			<input class="graybutton" id="submit" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<%} %>
			<%Html.RenderPartial("PurchaseListOrderPartial", Model.ListOrder);%>
			<%}
			else
			{%><h3>
			<%=Html._("Order.ViewOrder.EmptryOrder")%></h3>
			<%}
			}%>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});
		});
	</script>
</asp:Content>
