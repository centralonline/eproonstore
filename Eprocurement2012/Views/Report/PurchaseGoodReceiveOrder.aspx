﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/Report.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="spaceleft20">
		<div class="GroupData">
			<h3>
				<img src="../../images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("Report.Menu.GoodReceive")%></h3>
		</div>
		<%if (Model.ListOrder.Orders != null)
	{
		if (Model.TotalOrdersCount != 0)
		{%>
		<div class="pager">
			<%if (Model.DateFilter.HasValue)
	 { %>
			<%=Html.Pager(Model.PageSize, Model.PageNumber, Model.TotalOrdersCount, new { dateFilter = Model.DateFilter })%>
			<%} %>
			<%Model.ListOrder.Index = ((Model.PageNumber - 1) * Model.PageSize);%>
		</div>
		<%Html.BeginForm("ExportPurchaseGoodReceive", "ExportExcel", FormMethod.Get); %>
		<input class="graybutton" id="submit" type="submit" value="Export to Excel" />
		<%Html.EndForm(); %>
		<div id="content-shopping-cart-detail" style="text-align: center;">
			<table id="table-order">
				<tr>
					<td width="30" class="thead">
						<%=Html._("Admin.CompanyInfo.No")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("Order.ViewDataOrder.CustID")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("Order.ViewDataOrder.OrderId")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("Order.ViewDataOrder.OrderStatus")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("Order.ViewDataOrder.OrderDate")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("Order.ViewDataOrder.ApprovrOrderDate")%>
					</td>
					<td width="50" class="thead">
						<%=Html._("Order.ViewOrder.RequesterName")%>
					</td>
					<td width="50" class="thead">
						<%=Html._("Report.GoodReceiveOrder.EmailRequster")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("Order.ViewOrder.ApproverName")%>
					</td>
					<td width="50" class="thead">
						<%=Html._("Report.GoodReceiveOrder.EmailApprover")%>
					</td>
					<td width="70" class="thead">
						<%=Html._("Order.ViewOrder.CostCenterName")%>
					</td>
					<td width="50" class="thead">
						<%=Html._("Order.ConfirmOrder.Quantity")%>
					</td>
					<td width="70" class="thead">
						<%=Html._("Order.PartialReceive.ProductAction")%>
					</td>
					<td width="70" class="thead">
						<%=Html._("Order.PartialReceive.ProductCancel")%>
					</td>
					<td width="70" class="thead">
						<%=Html._("Order.PartialReceive.ProductRemain")%>
					</td>
					<td width="70" class="thead">
						<%=Html._("Order.ConfirmOrder.GrandTotalAmt")%>
					</td>
					<td width="70" class="thead">
						<%=Html._("Order.PartialReceive.ActionDate")%>
					</td>
					<td width="70" class="thead">
						<%=Html._("Report.GoodReceiveOrder.Status")%>
					</td>
				</tr>
				<tbody>
					<%int seq = Model.ListOrder.Index;
	   foreach (var order in Model.ListOrder.Orders)
	   {%>
					<tr>
						<td style="text-align: center">
							<%=++seq%>
						</td>
						<td>
							<%=order.CustId %>
						</td>
						<td>
							<%=Html.ActionLink(order.OrderID,"ViewOrderDetail","Order", new {Id=order.OrderGuid},null) %>
						</td>
						<td>
							<%=Html._("Eprocurement2012.Models.Order+OrderStatus." + order.Status.ToString())%>
						</td>
						<td>
							<%=order.OrderDate.ToString(new DateTimeFormat())%>
						</td>
						<td>
							<%=order.DisplayApproveOrderDate%>
						</td>
						<td>
							<%=order.Requester.DisplayName %>
						</td>
						<td>
							<%=order.Requester.UserId%>
						</td>
						<td>
							<%if (order.PreviousApprover != null)
		 { %>
							<%=order.PreviousApprover.Approver.DisplayName %>
							<%}
		 else
		 { %>
							<%=order.CurrentApprover.Approver.DisplayName %>
							<%} %>
						</td>
						<td>
							<%if (order.PreviousApprover != null)
		 { %>
							<%=order.PreviousApprover.Approver.UserId%>
							<%}
		 else
		 { %>
							<%=order.CurrentApprover.Approver.UserId%>
							<%} %>
						</td>
						<td>
							<%= "[ " + order.CostCenter.CostCenterID +" ] "+ order.CostCenter.CostCenterName %>
						</td>
						<td>
							<%=order.ItemCountOrder %>
						</td>
						<td>
							<%=order.GoodReceiveDetail.ActionQty%>
						</td>
						<td>
							<%=order.GoodReceiveDetail.CancelQty%>
						</td>
						<td>
							<%=order.ItemCountOrder - (order.GoodReceiveDetail.ActionQty + order.GoodReceiveDetail.CancelQty)%>
						</td>
						<td>
							<%=order.GrandTotalAmt.ToString(new MoneyFormat())%>
						</td>
						<td>
							<%=order.GoodReceive.UpdateOn.ToString(new DateTimeFormat())%>
						</td>
						<td>
							<%=Html._("Eprocurement2012.Models.GoodReceive+GoodReceiveStatus." + order.GoodReceive.Status.ToString())%>
						</td>
					</tr>
					<%}%>
				</tbody>
			</table>
		</div>
		<%}
		else
		{%>
		<div style="text-align: center;">
			<h5>
				<%=Html._("Order.ViewOrder.EmptryOrder")%></h5>
		</div>
		<%}
	} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
</asp:Content>
