﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="JavasCript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#OrderDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#AppDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#AppDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			if ($("#CheckOrderStatus").attr("checked")) { $("#divOrderStatus").show(); } else { $("#divOrderStatus").hide(); }
			if ($("#CheckRequester").attr("checked")) { $("#divRequester").show(); } else { $("#divRequester").hide(); }
			if ($("#CheckApprover").attr("checked")) { $("#divApprover").show(); } else { $("#divApprover").hide(); }
			if ($("#CheckCostcenter").attr("checked")) { $("#divCostcenter").show(); } else { $("#divCostcenter").hide(); }

			$("#CheckOrderStatus").live("click", function () {
				if (this.checked) {
					$("#divOrderStatus").show();
				}
				else {
					$("#divOrderStatus").hide();
				}
			});

			$("#CheckRequester").live("click", function () {
				if (this.checked) {
					$("#divRequester").show();
				}
				else {
					$("#divRequester").hide();
				}
			});

			$("#CheckApprover").live("click", function () {
				if (this.checked) {
					$("#divApprover").show();
				}
				else {
					$("#divApprover").hide();
				}
			});

			$("#CheckCostcenter").live("click", function () {
				if (this.checked) {
					$("#divCostcenter").show();
				}
				else {
					$("#divCostcenter").hide();
				}
			});


			$(".order").click(function () {
				var orderId = $(this).attr("orderId");
				$(".orderDetail_" + orderId).toggle();
			});


		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Report.Menu.ConditionalOrder")%></h2>
		<%Html.BeginForm("ConditionalOrder", "Report", FormMethod.Post, new { id = "fromConditionalOrder" }); %>
		<div id="OrderStatus">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderStatus, new { id = "CheckOrderStatus" })%>
			<label for="CheckOrderStatus">
				<%=Html._("Order.ViewDataOrder.OrderStatus")%></label><br />
			<div id="divOrderStatus" style="border: thin outset #CCCCCC; margin: 10px 20px 0px 30px; padding: 10px;">
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusWaiting, new { id = "Waiting" })%>
				<label for="Waiting">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Waiting")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusPartial, new { id = "Partial" })%>
				<label for="Partial">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Partial")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusRevise, new { id = "Revise" })%>
				<label for="Revise">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Revise")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusAdminAllow, new { id = "AdminAllow" })%>
				<label for="AdminAllow">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.AdminAllow")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusWaitingAdmin, new { id = "WaitingAdmin" })%>
				<label for="WaitingAdmin">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.WaitingAdmin")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusApproved, new { id = "Approved" })%>
				<label for="Approved">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Approved")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusShipped, new { id = "Shipped" })%>
				<label for="Shipped">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Shipped")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusCompleted, new { id = "Completed" })%>
				<label for="Completed">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Completed")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusDeleted, new { id = "Deleted" })%>
				<label for="Deleted">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Deleted")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusExpired, new { id = "Expired" })%>
				<label for="Expired">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Expired")%></label>
			</div>
		</div>
		<br />
		<div id="OrderDate">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
			<label for="CheckOrderDate">
				<%=Html._("Order.ViewDataOrder.OrderDate")%></label>
			<br />
			<%=Html._("Report.Conditional.From")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
			<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateTo, new { id = "OrderDateTo" })%>
		</div>
		<br />
		<div id="ApproveDate">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckAppDate, new { id = "CheckAppDate" })%>
			<label for="CheckAppDate">
				<%=Html._("Order.ViewDataOrder.ApprovrOrderDate")%></label>
			<br />
			<%=Html._("Report.Conditional.From")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.AppDateFrom, new { id = "AppDateFrom" })%>
			<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.AppDateTo, new { id = "AppDateTo" })%>
		</div>
		<br />
		<%if (Model.UserRequester != null)
	{%>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckRequester, new { id = "CheckRequester" })%>
			<label for="CheckRequester">
				<%=Html._("SetRequesterLine.Requester")%></label>
			<div id="divRequester" style="width: 50%">
				<table>
					<tr>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadSelect")%>
						</th>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadDisplayName")%>
						</th>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadEmail")%>
						</th>
					</tr>
					<%foreach (var Req in Model.UserRequester)
	   {
		   string requesterChecked = "";
		   if (Model.AdvanceSearchField.Requester.Any(r => r == Req.UserGuid))
		   {
			   requesterChecked = "checked=checked";
		   }
					%>
					<tr>
						<td>
							<input id="<%=Req.UserGuid %>" name="Requester" type="checkbox" value="<%=Req.UserGuid %>"
								<%=requesterChecked%> />
						</td>
						<td>
							<label for="<%=Req.UserGuid %>">
								<%=Req.DisplayName %></label>
						</td>
						<td>
							<%=Req.UserId %>
						</td>
					</tr>
					<%}%>
				</table>
			</div>
		</div>
		<%}%>
		<br />
		<%if (Model.UserApprover != null)
	{%>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckApprover, new { id = "CheckApprover" })%>
			<label for="CheckApprover">
				<%=Html._("SetRequesterLine.Approver")%></label>
			<div id="divApprover" style="width: 50%">
				<table>
					<tr>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadSelect")%>
						</th>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadDisplayName")%>
						</th>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadEmail")%>
						</th>
					</tr>
					<%foreach (var App in Model.UserApprover)
	   {
		   string approverChecked = "";
		   if (Model.AdvanceSearchField.Approver.Any(r => r == App.UserGuid))
		   {
			   approverChecked = "checked=checked";
		   }
					%>
					<tr>
						<td>
							<input id="<%=App.UserGuid %>" name="Approver" type="checkbox" value="<%=App.UserGuid %>"
								<%=approverChecked%> />
						</td>
						<td>
							<label for="<%=App.UserGuid %>">
								<%=App.DisplayName%></label>
						</td>
						<td>
							<%=App.UserId%>
						</td>
					</tr>
					<%}%>
				</table>
			</div>
		</div>
		<%}%>
		<br />
		<%if (Model.Costcenter != null)
	{%>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckCostcenter, new { id = "CheckCostcenter" })%>
			<label for="CheckCostcenter">
				<%=Html._("Report.Conditional.Costcenter")%></label>
			<div id="divCostcenter" style="width: 50%">
				<table>
					<tr>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadSelect")%>
						</th>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadCostCenterID")%>
						</th>
						<th>
							<%=Html._("Report.ConditionalOrderReport.HeadCostCenterName")%>
						</th>
					</tr>
					<%foreach (var Cost in Model.Costcenter)
	   {
		   string costcenterChecked = "";
		   if (Model.AdvanceSearchField.Costcenter.Any(r => r == Cost.CostCenterID))
		   {
			   costcenterChecked = "checked=checked";
		   }
					%>
					<tr>
						<td>
							<input id="<%=Cost.CostCenterID %>" name="Costcenter" type="checkbox" value="<%=Cost.CostCenterID %>"
								<%=costcenterChecked%> />
						</td>
						<td>
							<label for="<%=Cost.CostCenterID %>">
								<%=Cost.CostCenterID %></label>
						</td>
						<td>
							<%=Cost.CostCenterName %>
						</td>
					</tr>
					<%}%>
				</table>
			</div>
			<%}%>
		</div>
		<br />
		<div id="OrderPrice">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderPrice, new { id = "CheckAppDate" })%>
			<label for="CheckAppDate">
				<%=Html._("Report.Conditional.OrderPrice")%></label>
			<br />
			<%=Html._("Report.Conditional.FromPrice")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.MinOrderPrice)%>
			<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.MaxOrderPrice)%>
		</div>
		<br />
		<input type="submit" value="<%=Html._("Button.Search")%>" />
		<span style="color: Red;">
			<%=TempData["Message"] %></span>
		<%Html.EndForm(); %>
		<div>
			<%if (Model.ListOrder.Orders != null)
	 {
		 if (Model.TotalOrdersCount != 0)
		 {%>
		 
			<%Html.BeginForm("ExportConditionalOrder", "ExportExcel", Model.AdvanceSearchField); %>
			<%
	 var data = new RouteValueDictionary { { "AdvanceSearchField", Model.AdvanceSearchField } };

	 data.Add("waiting",Model.AdvanceSearchField.StatusWaiting);
	 data.Add("approved",Model.AdvanceSearchField.StatusApproved);
	 data.Add("partial",Model.AdvanceSearchField.StatusPartial);
	 data.Add("revise",Model.AdvanceSearchField.StatusRevise);
	 data.Add("waitingAdmin",Model.AdvanceSearchField.StatusWaitingAdmin);
	 data.Add("adminAllow",Model.AdvanceSearchField.StatusAdminAllow);
	 data.Add("shipped",Model.AdvanceSearchField.StatusShipped);
	 data.Add("deleted",Model.AdvanceSearchField.StatusDeleted);
	 data.Add("expired",Model.AdvanceSearchField.StatusExpired);
	 data.Add("completed",Model.AdvanceSearchField.StatusCompleted);

	 data.Add("orderDateFrom", Model.AdvanceSearchField.OrderDateFrom);
	 data.Add("orderDateTo", Model.AdvanceSearchField.OrderDateTo);

	 data.Add("appDateFrom", Model.AdvanceSearchField.AppDateFrom);
	 data.Add("appDateTo", Model.AdvanceSearchField.AppDateTo);

	 data.Add("minOrderPrice",Model.AdvanceSearchField.MinOrderPrice);
	 data.Add("maxOrderPrice",Model.AdvanceSearchField.MaxOrderPrice);

	 data.Add("checkAppDate",Model.AdvanceSearchField.CheckAppDate);
	 data.Add("checkOrderDate",Model.AdvanceSearchField.CheckOrderDate);
	 data.Add("checkOrderStatus",Model.AdvanceSearchField.CheckOrderStatus);
	 data.Add("checkApprover",Model.AdvanceSearchField.CheckApprover);
	 data.Add("checkCostcenter",Model.AdvanceSearchField.CheckCostcenter);
	 data.Add("checkOrderPrice",Model.AdvanceSearchField.CheckOrderPrice);
	 data.Add("checkRequester",Model.AdvanceSearchField.CheckRequester);
	 
	 for (int i = 0; i < Model.AdvanceSearchField.Requester.Length; i++)
	 {
		 data.Add(string.Format("Requester[{0}]", i), Model.AdvanceSearchField.Requester[i]);
		 %>
			<input type="hidden" name="requester" value="<%=Model.AdvanceSearchField.Requester[i]%>" />
			<%} %>

			<%for (int i = 0; i < Model.AdvanceSearchField.Approver.Length; i++)
	 {
		 data.Add(string.Format("Approver[{0}]", i), Model.AdvanceSearchField.Approver[i]);
			%>
			<input type="hidden" name="approver" value="<%=Model.AdvanceSearchField.Approver[i]%>" />
			<%} %>

			<%for (int i = 0; i < Model.AdvanceSearchField.Costcenter.Length; i++)
	 {
		 data.Add(string.Format("Costcenter[{0}]", i), Model.AdvanceSearchField.Costcenter[i]);
			%>
			<input type="hidden" name="costcenter" value="<%=Model.AdvanceSearchField.Costcenter[i]%>" />
			<%} %>

			<input id="Export" name="ExportHead.x" type="submit" value="Export Head"/>
			<input id="ExportDetail" name="ExportDetail.x" type="submit" value="Export Detail" />
			<%Html.EndForm(); %>

			<div class="pager">
				<%=Html.Pager(Model.PageSize, Model.PageNumber, Model.TotalOrdersCount, "ConditionalOrderReportPaging", data)%>
				<%Model.ListOrder.Index = ((Model.PageNumber - 1) * Model.PageSize);%>
			</div>

			<%Html.RenderPartial("ListOrderPartial", Model.ListOrder);%>
			<%}
		 else
		 {%><%--<h3>
			<%=Html._("Order.ViewOrder.EmptryOrder")%></h3>--%>
			<p style="margin-top: 20px; color: Red; font-weight: bold;">
				<%=Html._("Order.ViewOrder.EmptryOrder")%></p>
			<%}
	 }%>
		</div>
		<div style="min-height: 100px;">
		</div>
	</div>
</asp:Content>
