﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/Report.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="spaceleft20">
		<div class="GroupData">
			<h3>
				<img src="../../images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("ProductSummary.Summary")%></h3>
		</div>
		<%Html.BeginForm("PurchaseProductSummary", "Report", FormMethod.Post, new { id = "fromPurchaseProductSummary" }); %>
		<div id="OrderDate">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
			<label for="CheckOrderDate">
				<%=Html._("Order.ViewDataOrder.OrderDate")%></label>
			<br />
			<%=Html._("Report.Conditional.From")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
			<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateTo, new { id = "OrderDateTo" })%>
		</div>
		<br />
		<div>
			<div style="width: 50%">
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckDeptProduct, new { id = "CheckDeptProduct" })%>
				<label for="CheckDepartment">
					<%=Html._("ProductSummary.ParentName")%></label>
				<div id="divDepartment" style="border: thin outset #CCCCCC; margin: 10px 20px 0px 30px;
					padding: 10px;">
					<%foreach (var DeptProduct in Model.DeptProduct)
	   {
		   string departmentChecked = "";
		   if (Model.AdvanceSearchField.DeptProduct.Any(r => r == DeptProduct.Id.ToString()))
		   {
			   departmentChecked = "checked=checked";
		   }
					%>
					<input id="<%=DeptProduct.Id%>" name="DeptProduct" type="checkbox" value="<%=DeptProduct.Id%>"
						<%=departmentChecked%> />
					<label for="<%=DeptProduct.Id %>">
						<%=DeptProduct.Name%></label><br />
					<%} %>
				</div>
			</div>
		</div>
		<br />
		<input type="submit" class="graybutton" value="<%=Html._("Button.Search")%>" />
		<span style="color: Red;">
			<%=TempData["Message"] %></span>
		<%Html.EndForm(); %>
		<%if (Model.ProductSummary != null)
	{%>
		<%if (Model.ProductSummary.Any())
	{%>
		<div>
			<%Html.BeginForm("ExportPurchaseProductSummary", "ExportExcel", Model.AdvanceSearchField); %>
			<%
	 var data = new RouteValueDictionary { { "AdvanceSearchField", Model.AdvanceSearchField } };

	 data.Add("CheckDeptProduct", Model.AdvanceSearchField.CheckDeptProduct);

	 for (int i = 0; i < Model.AdvanceSearchField.DeptProduct.Length; i++)
	 {
		 data.Add(string.Format("DeptProduct[{0}]", i), Model.AdvanceSearchField.DeptProduct[i]);
			%>
			<input type="hidden" name="DeptProduct" value="<%=Model.AdvanceSearchField.DeptProduct[i]%>" />
			<%}%>
			<input class="graybutton" id="Export" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
		</div>
		<div id="content-shopping-cart-detail" style="text-align: center;">
			<table id="table-order">
				<tr>
					<td width="100" class="thead">
						<%=Html._("ProductSummary.OrderId")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("ProductSummary.ProductId")%>
					</td>
					<td width="400" class="thead">
						<%=Html._("ProductSummary.ProductName")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("ProductSummary.OrderDate")%>
					</td>
					<td width="200" class="thead">
						<%=Html._("ProductSummary.RequesterName")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("ProductSummary.Qty")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("ProductSummary.Average")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("ProductSummary.Total")%>
					</td>
				</tr>
				<%foreach (var parents in Model.ProductSummary.GroupBy(g => g.DepartmentProduct.ParentId))
				{%>
				<tr>
					<td colspan="9" style="text-align: left; background-color: #666666; color: #FFFFFF;">
						<span class="parentDisplay_<%=parents.First().DepartmentProduct.ParentId%>">-</span>
						<span style="cursor: pointer; font: bold 12px tahoma;" class="parent" parentid="<%=parents.First().DepartmentProduct.ParentId%>">
							<%=parents.First().DepartmentProduct.ParentName%></span>
						<input class="SetParent_<%=parents.First().DepartmentProduct.ParentId%>" type="hidden"
							value="On" />
					</td>
				</tr>
				<%foreach (var depts in parents.GroupBy(g => g.DepartmentProduct.DeptId))
				{%>
				<tr style="text-align: left; background-color: #CCCCCC;" class="<%=parents.First().DepartmentProduct.ParentId%>">
					<td colspan="9">
						<span style="position: absolute; margin-left: 40px;" class="deptDisplay_<%=depts.First().DepartmentProduct.DeptId%>">
							-</span><span style="margin-left: 50px; cursor: pointer; font: bold 12px tahoma;" class="dept <%=parents.First().DepartmentProduct.ParentId%>"
								parentid="<%=parents.First().DepartmentProduct.ParentId%>" deptid="<%=depts.First().DepartmentProduct.DeptId%>">
								<%=depts.First().DepartmentProduct.DeptName%></span>
					</td>
				</tr>
				<%foreach (var product in depts)
				{%>
				<tr class="<%=parents.First().DepartmentProduct.ParentId%> <%=depts.First().DepartmentProduct.DeptId%>"
					parentid="order_<%=parents.First().DepartmentProduct.ParentId%>">
					<td>
						<%=Html.ActionLink(product.Order.OrderID, "ViewOrderDetail", "Order", new { Id = product.Order.OrderGuid }, new { target = "_blank", @style = "font: 11px tahoma!important;" })%>
					</td>
					<td>
						<%=product.OrderDetail.Product.Id%>
					</td>
					<td style="text-align: left;">
						<%=product.OrderDetail.Product.Name %>
						<%--<%if (product.OrderDetail.IsOfmCatalog)
						{ %>
							<p style="color: #0099FF">
								<%=Html._("Order.ConfirmOrder.NotProductCatalog")%>
							</p>
						<%} %>--%>
					</td>
					<td>
						<%=product.Order.OrderDate.ToString(new DateTimeFormat())%>
					</td>
					<td>
						<%=product.Order.Requester.DisplayName%>
					</td>
					<td>
						<%=product.OrderDetail.Quantity%>
					</td>
					<td style="text-align: right;">
						<%=product.OrderDetail.ItemExcVatPrice.ToString(new MoneyFormat())%>
					</td>
					<td style="text-align: right;">
						<%=(product.OrderDetail.ItemExcVatPrice * product.OrderDetail.Quantity).ToString(new MoneyFormat())%>
					</td>
				</tr>
				<%} %>
				<%} %>
				<%} %>
			</table>
		</div>
		<%} %>
		<%else
	{%>
		<span style="color: Red;">
			<%=Html._("ProductSummary.ErrorKeyword")%></span>
		<%} %>
		<%} %>
		<div style="min-height: 100px;">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#OrderDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			if ($("#CheckDeptProduct").attr("checked")) { $("#divDepartment").show(); } else { $("#divDepartment").hide(); }

			$("#CheckDeptProduct").live("click", function () {
				if (this.checked) {
					$("#divDepartment").show();
				}
				else {
					$("#divDepartment").hide();
				}
			});

			$(".parent").click(function () {
				var parentId = $(this).attr("parentId");
				var value_SetParent = $(".SetParent_" + parentId).attr("value");
				$("." + parentId).toggle();

				if (value_SetParent == "On") { $(".SetParent_" + parentId).attr("value", "Off"); }
				else { $(".SetParent_" + parentId).attr("value", "On"); }

				value_SetParent = $(".SetParent_" + parentId).attr("value");

				if (value_SetParent == "On") {
					$(".dept" + "." + parentId).each(function (index) {
						var dept_Id = $(this).attr("deptid");
						$(".parentDisplay_" + parentId).html("-");
						$("." + parentId + "." + dept_Id).show();
						$(this).show();
						$(".deptDisplay_" + dept_Id).html("-");
					});
				}

				if (value_SetParent == "Off") {
					$(".dept" + "." + parentId).each(function (index) {
						var dept_Id = $(this).attr("deptid");
						$(".parentDisplay_" + parentId).html("+");
						$("." + parentId + "." + dept_Id).hide();
						$(this).hide();
					});
				}

			});

			$(".dept").click(function () {
				var parentId = $(this).attr("parentId");
				var deptId = $(this).attr("deptId");
				$("." + deptId).toggle();
				var resultStlye = $("." + deptId).attr("style");
				if (resultStlye.indexOf("none;") == -1) {
					//Not Hide
					$(".deptDisplay_" + deptId).html("-");
				}
				else {
					$(".deptDisplay_" + deptId).html("+");
				}
			});
		});
	</script>
</asp:Content>
