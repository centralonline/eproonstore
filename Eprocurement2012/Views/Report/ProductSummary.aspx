﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		td.thead
		{
			background-color: #e6e6e6;
			font: bold 12px tahoma;
			padding: 5px;
			text-align: center;
		}
		
		tbody td
		{
			border-top: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			background: #eee;
			border-right: 1px solid #bbb;
		}
	</style>
	<div class="spaceleft20">
		<div class="GroupData">
			<h3>
				<img src="../../images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("ProductSummary.Summary")%></h3>
		</div>
		<%Html.BeginForm("ProductSummary", "Report", FormMethod.Post, new { id = "fromPurchaseProductSummary" }); %>
		<div id="OrderStatus" style="width: 50%">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderStatus, new { id = "CheckOrderStatus" })%>
			<label for="CheckOrderStatus">
				<%=Html._("Order.ViewDataOrder.OrderStatus")%></label>
			<div id="divOrderStatus" style="border: thin outset #CCCCCC; margin: 10px 20px 0px 30px;
				padding: 10px;">
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusWaiting, new { id = "Waiting" })%>
				<label for="Waiting">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Waiting")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusPartial, new { id = "Partial" })%>
				<label for="Partial">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Partial")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusRevise, new { id = "Revise" })%>
				<label for="Revise">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Revise")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusAdminAllow, new { id = "AdminAllow" })%>
				<label for="AdminAllow">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.AdminAllow")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusWaitingAdmin, new { id = "WaitingAdmin" })%>
				<label for="WaitingAdmin">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.WaitingAdmin")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusApproved, new { id = "Approved" })%>
				<label for="Approved">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Approved")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusShipped, new { id = "Shipped" })%>
				<label for="Shipped">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Shipped")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusCompleted, new { id = "Completed" })%>
				<label for="Completed">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Completed")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusDeleted, new { id = "Deleted" })%>
				<label for="Deleted">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Deleted")%></label><br />
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusExpired, new { id = "Expired" })%>
				<label for="Expired">
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Expired")%></label>
			</div>
		</div>
		<br />
		<div id="OrderDate">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
			<label for="CheckOrderDate">
				<%=Html._("Order.ViewDataOrder.OrderDate")%></label>
			<br />
			<%=Html._("Report.Conditional.From")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
			<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateTo, new { id = "OrderDateTo" })%>
		</div>
		<br />
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderId, new { id = "CheckOrderId" })%>
			<label for="CheckOrderId">
				<%=Html._("Order.ViewDataOrder.OrderId")%>
			</label>
			<br />
			<%=Html._("Report.Conditional.From")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderIdFrom, new { id = "OrderIdFrom" })%>
			<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderIdTo, new { id = "OrderIdTo" })%>
		</div>
		<br />
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckProductId, new { id = "CheckProductId" })%>
			<label for="CheckProductId">
				<%=Html._("Product.Detail.ProductId")%>
			</label>
			<br />
			<%=Html._("Report.Conditional.From")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.ProductIdFrom, new { id = "ProductIdFrom" })%>
			<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.ProductIdTo, new { id = "ProductIdTo" })%>
		</div>
		<br />
		<div>
			<%if (Model.User.Company.IsCompModelThreeLevel && Model.CompanyDepartment != null)
	 {%>
			<div style="width: 50%">
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckDepartment, new { id = "CheckDepartment" })%>
				<label for="CheckDepartment">
					<%=Html._("Order.ViewOrder.DepartmentName")%></label>
				<div id="divDepartment" style="border: thin outset #CCCCCC; margin: 10px 20px 0px 30px;
					padding: 10px;">
					<%foreach (var Department in Model.CompanyDepartment)
	   {
		   string departmentChecked = "";
		   if (Model.AdvanceSearchField.Department.Any(r => r == Department.DepartmentID))
		   {
			   departmentChecked = "checked=checked";
		   }
					%>
					<input id="<%=Department.DepartmentID%>" name="Department" type="checkbox" value="<%=Department.DepartmentID %>"
						<%=departmentChecked%> />
					<label for="<%=Department.DepartmentID %>">
						[<%=Department.DepartmentID%>]<%=Department.DepartmentName %></label><br />
					<%} %>
				</div>
			</div>
			<%} %>
		</div>
		<br />
		<div>
			<%if (Model.Costcenter != null)
	 {%>
			<div style="width: 50%">
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckCostcenter, new { id = "CheckCostcenter" })%>
				<label for="CheckCostcenter">
					<%=Html._("Report.Conditional.Costcenter")%></label>
				<div id="divCostcenter" style="border: thin outset #CCCCCC; margin: 10px 20px 0px 30px;
					padding: 10px;">
					<%foreach (var Cost in Model.Costcenter)
	   {
		   string costcenterChecked = "";
		   if (Model.AdvanceSearchField.Costcenter.Any(r => r == Cost.CostCenterID))
		   {
			   costcenterChecked = "checked=checked";
		   }
					%>
					<input id="<%=Cost.CostCenterID %>" name="Costcenter" type="checkbox" value="<%=Cost.CostCenterID %>"
						<%=costcenterChecked%> />
					<label for="<%=Cost.CostCenterID %>">
						[<%=Cost.CostCenterID %>]<%=Cost.CostCenterName %></label><br />
					<%}%>
				</div>
			</div>
			<%}%>
		</div>
		<br />
		<input type="submit" class="graybutton" value="<%=Html._("Button.Search")%>" />
		<span style="color: Red;">
			<%=TempData["Message"] %></span>
		<%Html.EndForm(); %>
		<%if (Model.ProductSummary != null)
	{%>
		<%if (Model.ProductSummary.Any())
	{%>
		<div>
			<%Html.BeginForm("ExportProductSummary", "ExportExcel", Model.AdvanceSearchField); %>
			<%
	 var data = new RouteValueDictionary { { "AdvanceSearchField", Model.AdvanceSearchField } };

	 data.Add("checkCostcenter", Model.AdvanceSearchField.CheckCostcenter);
	 data.Add("checkDepartment", Model.AdvanceSearchField.CheckDepartment);

	 for (int i = 0; i < Model.AdvanceSearchField.Costcenter.Length; i++)
	 {
		 data.Add(string.Format("Costcenter[{0}]", i), Model.AdvanceSearchField.Costcenter[i]);
			%>
			<input type="hidden" name="costcenter" value="<%=Model.AdvanceSearchField.Costcenter[i]%>" />
			<%}%>
			<%for (int i = 0; i < Model.AdvanceSearchField.Department.Length; i++)
	 {
		 data.Add(string.Format("Department[{0}]", i), Model.AdvanceSearchField.Department[i]);
			%>
			<input type="hidden" name="department" value="<%=Model.AdvanceSearchField.Department[i]%>" />
			<%} %>
			<input class="graybutton" id="Export" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
		</div>
		<div>
			<table id="table-order">
				<tr>
					<td width="100" class="thead">
						<%=Html._("ProductSummary.OrderId")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("ProductSummary.ProductId")%>
					</td>
					<td width="400" class="thead">
						<%=Html._("ProductSummary.ProductName")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("ProductSummary.OrderDate")%>
					</td>
					<td width="200" class="thead">
						<%=Html._("ProductSummary.RequesterName")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("ProductSummary.Qty")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("ProductSummary.Average")%>
					</td>
					<td width="100" class="thead">
						<%=Html._("ProductSummary.Total")%>
					</td>
				</tr>
				<%foreach (var parents in Model.ProductSummary.GroupBy(g => g.DepartmentProduct.ParentId))
				{%>
				<tr>
					<td colspan="9" style="text-align: left; background-color: #666666; color: #FFFFFF;">
						<span class="parentDisplay_<%=parents.First().DepartmentProduct.ParentId%>">-</span>
						<span style="cursor: pointer; font: bold 12px tahoma;" class="parent" parentid="<%=parents.First().DepartmentProduct.ParentId%>">
							<%=parents.First().DepartmentProduct.ParentName%></span>
						<input class="SetParent_<%=parents.First().DepartmentProduct.ParentId%>" type="hidden"
							value="On" />
					</td>
				</tr>
				<%foreach (var depts in parents.GroupBy(g => g.DepartmentProduct.DeptId))
				{%>
				<tr style="text-align: left; background-color: #CCCCCC;" class="<%=parents.First().DepartmentProduct.ParentId%>">
					<td colspan="9">
						<span style="position: absolute; margin-left: 40px;" class="deptDisplay_<%=depts.First().DepartmentProduct.DeptId%>">
							-</span><span style="margin-left: 50px; cursor: pointer; font: bold 12px tahoma;" class="dept <%=parents.First().DepartmentProduct.ParentId%>"
								parentid="<%=parents.First().DepartmentProduct.ParentId%>" deptid="<%=depts.First().DepartmentProduct.DeptId%>">
								<%=depts.First().DepartmentProduct.DeptName%></span>
					</td>
				</tr>
				<%foreach (var product in depts)
				{%>
				<tr class="<%=parents.First().DepartmentProduct.ParentId%> <%=depts.First().DepartmentProduct.DeptId%>"
					parentid="order_<%=parents.First().DepartmentProduct.ParentId%>">
					<td>
						<%=Html.ActionLink(product.Order.OrderID, "ViewOrderDetail", "Admin", new { Id = product.Order.OrderGuid }, new { target = "_blank", @style = "font: 11px tahoma!important;" })%>
					</td>
					<td>
						<%=product.OrderDetail.Product.Id%>
					</td>
					<td style="text-align: left;">
						<%=product.OrderDetail.Product.Name %>
						<%if (product.OrderDetail.IsOfmCatalog)
						{ %>
							<p style="color: #0099FF">
								<%=Html._("Order.ConfirmOrder.NotProductCatalog")%>
							</p>
						<%} %>
					</td>
					<td>
						<%=product.Order.OrderDate.ToString(new DateTimeFormat())%>
					</td>
					<td>
						<%=product.Order.Requester.DisplayName%>
					</td>
					<td>
						<%=product.OrderDetail.Quantity%>
					</td>
					<td style="text-align: right;">
						<%=product.OrderDetail.ItemExcVatPrice.ToString(new MoneyFormat())%>
					</td>
					<td style="text-align: right;">
						<%=(product.OrderDetail.ItemExcVatPrice * product.OrderDetail.Quantity).ToString(new MoneyFormat())%>
					</td>
				</tr>
				<%} %>
				<%} %>
				<%} %>
			</table>
		</div>
		<%} %>
		<%else
	{%>
		<span style="color: Red;">
			<%=Html._("ProductSummary.ErrorKeyword")%></span>
		<%} %>
		<%} %>
		<div style="min-height: 100px;">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#OrderDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			if ($("#CheckOrderStatus").attr("checked")) { $("#divOrderStatus").show(); } else { $("#divOrderStatus").hide(); }
			if ($("#CheckCostcenter").attr("checked")) { $("#divCostcenter").show(); } else { $("#divCostcenter").hide(); }
			if ($("#CheckDepartment").attr("checked")) { $("#divDepartment").show(); } else { $("#divDepartment").hide(); }

			$("#CheckOrderStatus").live("click", function () {
				if (this.checked) {
					$("#divOrderStatus").show();
				}
				else {
					$("#divOrderStatus").hide();
				}
			});

			$("#CheckCostcenter").live("click", function () {
				if (this.checked) {
					$("#divCostcenter").show();
				}
				else {
					$("#divCostcenter").hide();
				}
			});

			$("#CheckDepartment").live("click", function () {
				if (this.checked) {
					$("#divDepartment").show();
				}
				else {
					$("#divDepartment").hide();
				}
			});

			$(".parent").click(function () {
				var parentId = $(this).attr("parentId");
				var value_SetParent = $(".SetParent_" + parentId).attr("value");
				$("." + parentId).toggle();

				if (value_SetParent == "On") { $(".SetParent_" + parentId).attr("value", "Off"); }
				else { $(".SetParent_" + parentId).attr("value", "On"); }

				value_SetParent = $(".SetParent_" + parentId).attr("value");

				if (value_SetParent == "On") {
					$(".dept" + "." + parentId).each(function (index) {
						var dept_Id = $(this).attr("deptid");
						$(".parentDisplay_" + parentId).html("-");
						$("." + parentId + "." + dept_Id).show();
						$(this).show();
						$(".deptDisplay_" + dept_Id).html("-");
					});

				}

				if (value_SetParent == "Off") {
					$(".dept" + "." + parentId).each(function (index) {
						var dept_Id = $(this).attr("deptid");
						$(".parentDisplay_" + parentId).html("+");
						$("." + parentId + "." + dept_Id).hide();
						$(this).hide();
					});
				}

			});

			$(".dept").click(function () {
				var parentId = $(this).attr("parentId");
				var deptId = $(this).attr("deptId");
				$("." + deptId).toggle();
				var resultStlye = $("." + deptId).attr("style");
				if (resultStlye.indexOf("none;") == -1) {
					//Not Hide
					$(".deptDisplay_" + deptId).html("-");
				}
				else {
					$(".deptDisplay_" + deptId).html("+");
				}
			});
		});
	</script>
</asp:Content>
