﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.ListOrder>" %>
<style type="text/css">
	td.thead
	{
		background-color: #e6e6e6;
		font: bold 12px tahoma;
		padding: 5px;
		text-align: center;
	}
	
	.head
	{
		background-color: #666666;
		font: bold 12px tahoma;
		padding: 5px;
		text-align: center;
		color: #FFFFFF;
	}
	
	.Detail
	{
		background-color: #A09B9B;
		font: bold 12px tahoma;
		padding: 5px;
		text-align: center;
		color: #FFFFFF;
	}
</style>
<div id="table-cart">
	<div id="content-shopping-cart-detail" style="text-align: center;">
		<table id="table-order" width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
			border: solid 1px #c5c5c5; text-align: center;">
			<tr>
				<td class="head">
					<%=Html._("Admin.CompanyInfo.No")%>
				</td>
				<td class="head">
					<%=Html._("Admin.CompanyInfo.CustId")%>
				</td>
				<td class="head">
					<%=Html._("Order.ViewDataOrder.OrderId")%>
				</td>
				<td class="head">
					<%=Html._("Order.ViewDataOrder.OrderStatus")%>
				</td>
				<td class="head">
					<%=Html._("Order.ViewDataOrder.OrderDate")%>
				</td>
				<td class="head">
					<%=Html._("Order.ViewDataOrder.ApprovrOrderDate")%>
				</td>
				<td class="head">
					<%=Html._("Order.ViewOrder.RequesterName")%>
				</td>
				<td class="head">
					<%=Html._("Order.ViewOrder.ApproverName")%>
				</td>
				<td class="head" style="text-align: center">
					<%=Html._("Order.ViewOrder.CostCenterName")%>
				</td>
				<td class="head" style="text-align: center">
					<%=Html._("Order.ConfirmOrder.Quantity")%>
				</td>
				<td class="head" style="text-align: center">
					<%=Html._("Order.ConfirmOrder.GrandTotalAmt")%>
				</td>
			</tr>
			<tbody>
				<%int seq = Model.Index;
	  foreach (var order in Model.Orders)
	  {%>
				<tr>
					<td style="text-align: center">
						<%=++seq%>
					</td>
					<td>
						<%=order.CustId %>
					</td>
					<td>
						<%=Html.ActionLink(order.OrderID,"ViewOrderDetail","Admin", new {Id=order.OrderGuid},null) %>
						<%if (Model.ItemOrders != null)
		{%>
						<span class="order <%=order.OrderID%>" orderid="<%=order.OrderID%>" style="font-weight: bold;
							color: Blue; font-family: serif; cursor: pointer">Detail</span>
						<%} %>
					</td>
					<td>
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus." + order.Status.ToString())%>
					</td>
					<td style="text-align: center">
						<%=order.OrderDate.ToString(new DateTimeFormat())%>
					</td>
					<td style="text-align: center">
						<%=order.DisplayApproveOrderDate%>
					</td>
					<td>
						<%=order.Requester.DisplayName %>
					</td>
					<td>
						<%if (order.PreviousApprover != null)
		{ %>
						<%=order.PreviousApprover.Approver.DisplayName %>
						<%}
		else
		{ %>
						<%=order.CurrentApprover.Approver.DisplayName %>
						<%} %>
					</td>
					<td>
						<%=order.CostCenter.CostCenterName %>
					</td>
					<td style="text-align: center">
						<%=order.ItemCountOrder %>
					</td>
					<td style="text-align: right">
						<%=order.GrandTotalAmt.ToString(new MoneyFormat())%>
					</td>
				</tr>
				<%if (Model.ItemOrders != null)
	  {%>
				<tr class="orderDetail_<%=order.OrderID%>">
					<td colspan="11">
						<table>
							<tr>
								<th class="Detail">
									<%=Html._("Report.ListOrderPartial.ProductId")%>
								</th>
								<th class="Detail">
									<%=Html._("Report.ListOrderPartial.ProductName")%>
								</th>
								<th class="Detail" style="width: 150px;">
									<%=Html._("Report.ListOrderPartial.PriceIncVat")%>
								</th>
								<th class="Detail" style="width: 150px;">
									<%=Html._("Report.ListOrderPartial.PriceExcVat")%>
								</th>
								<th class="Detail" style="width: 80px;">
									<%=Html._("Report.ListOrderPartial.Quantity")%>
								</th>
								<th class="Detail" style="width: 80px;">
									<%=Html._("Report.ListOrderPartial.Unit")%>
								</th>
								<th class="Detail" style="width: 150px;">
									<%=Html._("Report.ListOrderPartial.PriceNet")%>
								</th>
							</tr>
							<%foreach (var orderDetail in Model.ItemOrders.Where(o => o.OrderId == order.OrderID))
		 {%>
							<tr class="orderDetail_<%=orderDetail.OrderId%>">
								<td>
									<%=orderDetail.Product.Id%>
								</td>
								<td>
									<%=orderDetail.Product.Name%>
								</td>
								<td style="text-align: center;">
									<%=orderDetail.Product.PriceIncVat.ToString(new MoneyFormat())%>
								</td>
								<td style="text-align: center;">
									<%=orderDetail.Product.PriceExcVat.ToString(new MoneyFormat())%>
								</td>
								<td style="text-align: center;">
									<%=orderDetail.Quantity%>
								</td>
								<td style="text-align: center;">
									<%=orderDetail.Product.Unit%>
								</td>
								<td style="text-align: right;">
									<%=orderDetail.ItemPriceNet.ToString(new MoneyFormat())%>
								</td>
							</tr>
							<%}%>
						</table>
					</td>
				</tr>
				<%} %>
				<%} %>
			</tbody>
		</table>
	</div>
</div>
