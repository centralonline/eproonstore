﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/Report.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div class="spaceleft20">
	<div class="GroupData">
	<h3><img src="../../images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("Report.Menu.OnProgressOrder")%></h3>
	</div>

	<div>
		<%if (Model.ListOrder.Orders != null)
		{
		if (Model.TotalOrdersCount != 0)
		{%>
		<div class="pager">
			<%if (Model.DateFilter.HasValue)
			{ %>
			<%=Html.Pager(Model.PageSize, Model.PageNumber, Model.TotalOrdersCount, new { dateFilter = Model.DateFilter })%>
			<%} %>
			<%Model.ListOrder.Index = ((Model.PageNumber - 1) * Model.PageSize);%>
		</div>

		<%Html.BeginForm("ExportPurchaseOnProgressOrder", "ExportExcel", FormMethod.Get); %>
		<input class="graybutton" id="submit" type="submit" value="Export to Excel"/>
		<%Html.EndForm(); %>


		<%Html.RenderPartial("PurchaseListOrderPartial", Model.ListOrder);%>
		<%}
		else
		{%><div style="text-align:center;"><h5>
			<%=Html._("Order.ViewOrder.EmptryOrder")%></h5></div>
		<%}
		}%>
	</div>
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
</asp:Content>
