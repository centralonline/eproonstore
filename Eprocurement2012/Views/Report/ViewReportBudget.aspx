﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ReportBudget>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		.format_center
		{
			text-align: center;
		}
		.format_right
		{
			text-align: right;
		}
	</style>

	<div>
		<h2 id="page-heading" style="width: 500px;">
			<%=Html._("ReportBudget.CompanyBudget")%></h2>
	<%if (Model.Budgets.Any())
   {%>
		<div class="grid_2 noPrint" 
			style="width: 120px; border: 1px solid #E5E5E5; padding: 9px;
			border-radius: 5px 5px 5px 5px; position: absolute; margin-left: 550px; margin-top: -60px; top: 228px; left: 203px;">
			<a style="color: #4570B7; ext-decoration: none; font-weight: normal" href="<%=Url.Action("ExportReportBudget","ExportExcel")%>">
				<img src="/images/Excel-icon.png" alt="" title="" />
				Export Excel</a>
		</div>
		<%} %>
		<div>
			<table style="width: 500px;">
				<tbody>
					<tr>
						<td>
							<%=Html._("ReportBudget.CompanyId")%>
						</td>
						<td>
							<%=Model.Company.CompanyId %>
						</td>
					</tr>
					<tr>
						<td>
							<%=Html._("ReportBudget.CompanyModel")%>
						</td>
						<td>
							<%=Model.Company.IsCompModelThreeLevel ? Html._("ReportBudget.Company.3Level") : Html._("ReportBudget.Company.2Level")%>
						</td>
					</tr>
					<tr>
						<td>
							<%=Html._("ReportBudget.BudgetLevelType")%>
						</td>
						<td>
							<%=Model.Company.BudgetLevelType %>
						</td>
					</tr>
					<tr>
						<td>
							<%=Html._("ReportBudget.BudgetPeriod")%>
						</td>
						<td>
							<%=Model.Company.BudgetPeriodType %>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<%if (Model.Budgets.Any())
	{%>
		<%if (Model.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Costcenter)
	{%>
		<div>
			<table width="100%">
				<thead>
					<tr>
						<th class="format_center">
							<%=Html._("ReportBudget.Level")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.CostCenterID")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.CostCenterThaiName")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.CostCenterEngName")%>
						</th>
					</tr>
				</thead>
				<tbody>
					<%int index = 0; %>
					<%foreach (var item in Model.Budgets.GroupBy(g => g.CostCenter.CostCenterID))
	   {%>
					<tr class="costCenter" style="cursor: pointer;">
						<td class="format_center">
							<%=++index%>
						</td>
						<td class="format_center">
							<%=item.First().CostCenter.CostCenterID%>
						</td>
						<td class="format_center">
							<%=item.First().CostCenter.CostCenterThaiName %>
						</td>
						<td class="format_center">
							<%=item.First().CostCenter.CostCenterEngName%>
						</td>
					</tr>
					<tr class="budget">
						<td colspan="4">
							<table>
								<tr>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.CostCenterID")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetPeriod")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetYear")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.OriginalAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.UsedAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.RemainBudgetAmt")%>
									</th>
								</tr>
								<%foreach (var budget in item)
		  {%>
								<tr class="budget">
									<td class="format_center" style="background-color: #F8F8F8;">
										<%=Html.ActionLink(budget.CostCenter.CostCenterID, "ReportBudgetDetail", "Report", new { groupId = budget.Budget.GroupID, periodNo = budget.Budget.PeriodNo, year = budget.Budget.Year }, new { target = "_blank" })%>
									</td>
									<td style="background-color: #F8F8F8;">
										<%=budget.Budget.DisplayPeriodNo%>
									</td>
									<td class="format_center" style="background-color: #F8F8F8;">
										<%=budget.Budget.Year%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=budget.Budget.OriginalAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=budget.Budget.BudgetAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=budget.Budget.UsedAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=budget.Budget.RemainBudgetAmt.ToString(new MoneyFormat())%>
									</td>
								</tr>
								<%} %>
							</table>
						</td>
					</tr>
					<%} %>
				</tbody>
			</table>
		</div>
		<%} %>
		<%if (Model.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Department)
	{%>
		<div>
			<table width="100%">
				<thead>
					<tr>
						<th class="format_center">
							<%=Html._("ReportBudget.Level")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.DepartmentID")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.DepartmentThaiName")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.DepartmentEngName")%>
						</th>
					</tr>
				</thead>
				<tbody>
					<%int index = 0; %>
					<%foreach (var item in Model.Budgets.GroupBy(g => g.Department.DepartmentID))
	   {%>
					<tr class="costCenter" style="cursor: pointer;">
						<td class="format_center">
							<%=++index%>
						</td>
						<td class="format_center">
							<%=item.First().Department.DepartmentID%>
						</td>
						<td>
							<%=item.First().Department.DepartmentThaiName %>
						</td>
						<td>
							<%=item.First().Department.DepartmentEngName%>
						</td>
					</tr>
					<tr class="budget">
						<td colspan="4">
							<table>
								<tr>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.DepartmentID")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetPeriod")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetYear")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.OriginalAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.UsedAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.RemainBudgetAmt")%>
									</th>
								</tr>
								<%foreach (var budget in item)
		  {%>
								<tr class="budget">
									<td class="format_center" style="background-color: #F8F8F8;">
										<%=Html.ActionLink(budget.Department.DepartmentID, "ReportBudgetDetail", "Report", new { groupId = budget.Budget.GroupID, periodNo = budget.Budget.PeriodNo, year = budget.Budget.Year }, new { target = "_blank" })%>
									</td>
									<td style="background-color: #F8F8F8;">
										<%=budget.Budget.DisplayPeriodNo%>
									</td>
									<td class="format_center" style="background-color: #F8F8F8;">
										<%=budget.Budget.Year%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=budget.Budget.OriginalAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=budget.Budget.BudgetAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=budget.Budget.UsedAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=budget.Budget.RemainBudgetAmt.ToString(new MoneyFormat())%>
									</td>
								</tr>
								<%} %>
							</table>
						</td>
					</tr>
					<%} %>
				</tbody>
			</table>
		</div>
		<%} %>
		<%if (Model.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Company)
	{%>
		<div>
			<table width="100%">
				<thead>
					<tr>
						<th class="format_center">
							<%=Html._("ReportBudget.Level")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.CompanyId")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.CompanyThaiName")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.CompanyEngName")%>
						</th>
					</tr>
				</thead>
				<tbody>
					<%int index = 0; %>
					<tr class="costCenter" style="cursor: pointer;">
						<td class="format_center">
							<%=++index%>
						</td>
						<td class="format_center">
							<%=Model.Company.CompanyId %>
						</td>
						<td>
							<%=Model.Company.CompanyThaiName %>
						</td>
						<td>
							<%=Model.Company.CompanyEngName %>
						</td>
					</tr>
					<tr class="budget">
						<td colspan="4">
							<table>
								<tr>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.CompanyId")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetPeriod")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetYear")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.OriginalAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.BudgetAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.UsedAmt")%>
									</th>
									<th class="format_center" style="border-bottom: 2px solid #888; background: #D8D8D8;
										padding: .4em 1em .2em;">
										<%=Html._("ReportBudget.RemainBudgetAmt")%>
									</th>
								</tr>
								<%foreach (var item in Model.Budgets)
		  {%>
								<tr class="budget">
									<td class="format_center" style="background-color: #F8F8F8;">
										<%=Html.ActionLink(Model.Company.CompanyId, "ReportBudgetDetail", "Report", new { groupId = item.Budget.GroupID, periodNo = item.Budget.PeriodNo, year = item.Budget.Year }, new { target = "_blank" })%>
									</td>
									<td class="format_center" style="background-color: #F8F8F8;">
										<%=item.Budget.DisplayPeriodNo%>
									</td>
									<td class="format_center" style="background-color: #F8F8F8;">
										<%=item.Budget.Year%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=item.Budget.OriginalAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=item.Budget.BudgetAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=item.Budget.UsedAmt.ToString(new MoneyFormat())%>
									</td>
									<td class="format_right" style="background-color: #F8F8F8;">
										<%=item.Budget.RemainBudgetAmt.ToString(new MoneyFormat())%>
									</td>
								</tr>
								<%} %>
							</table>
						</td>
					</tr>
				</tbody>
			</table>
		</div>
		<%} %>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
	<script type="text/javascript" language="javascript">
		$(function () {

			$(".costCenter").toggle(function () {
				var row = $(this).next();
				while (row.hasClass("budget")) {
					row.hide();
					row = row.next();
				}
			}, function () {
				var row = $(this).next();
				while (row.hasClass("budget")) {
					row.show();
					row = row.next();
				}
			});
			$(".costCenter").click();

		});
	</script>
</asp:Content>
