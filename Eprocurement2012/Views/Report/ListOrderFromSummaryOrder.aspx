﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackCostCenter>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2>
			<%=Html._("Report.ListOrderFromSummaryOrder")%></h2>
		<%Html.RenderPartial("ListOrderPartial", Model.ListOrder);%>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
</asp:Content>
