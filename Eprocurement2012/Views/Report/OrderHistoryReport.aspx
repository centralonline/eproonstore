﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="JavasCript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Report.Menu.OrderHistory")%></h2>
		<ul class="tabs">
			<li>
				<%=Html.ActionLink(Html._("Report.MonthFilter.1Week"), "OrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.SevenDaysLater }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.SevenDaysLater ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.MonthFilter.1Month"), "OrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.OneMonthLater }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.OneMonthLater ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.MonthFilter.3Month"), "OrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.ThreeMonthLater }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.ThreeMonthLater ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.MonthFilter.6Month"), "OrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.SixMonthLater }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.SixMonthLater ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.Filter.All"), "OrderHistory", "Report", new { Page = 1, MonthFilter = Eprocurement2012.Models.MonthFilter.All }, new { @class = Model.MonthFilter.HasValue && Model.MonthFilter.Value == Eprocurement2012.Models.MonthFilter.All ? "active-link" : "" })%></li>
		</ul>
		<div>
			<h2 id="page-heading">
				<%=Html._("Report.SearchOrder")%></h2>
			<%Html.BeginForm("SearchOrderHistory", "Report", FormMethod.Post, new { id = "formOrderHistory" }); %>
			<div>
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
				<label for="CheckOrderDate">
					<%=Html._("Order.ViewDataOrder.OrderDate")%></label><br />
				<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
			</div>
			<div>
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderId, new { id = "CheckOrderId" })%>
				<label for="CheckOrderId">
					<%=Html._("Order.ViewDataOrder.OrderId")%></label><br />
				<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderId)%>
			</div>
			<div>
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckUserId, new { id = "CheckUserId" })%>
				<label for="CheckUserId">
					<%=Html._("Order.ViewOrder.EmailRequester")%></label><br />
				<%=Html.TextBoxFor(m => m.AdvanceSearchField.UserId)%>
			</div>
			<input type="submit" value="<%=Html._("Button.Search")%>" />
			<span style="color: Red;">
				<%=TempData["Message"] %></span>
			<%Html.EndForm(); %>
		</div>
		<div>
			<%if (Model.ListOrder.Orders != null)
	 {
		 if (Model.TotalOrdersCount != 0)
		 {%>
			<div class="pager">
				<%if (Model.MonthFilter.HasValue)
	  { %>
				<%=Html.Pager(Model.PageSize, Model.PageNumber, Model.TotalOrdersCount, new { monthFilter = Model.MonthFilter })%>
				<%
			} %>
				<%Model.ListOrder.Index = ((Model.PageNumber - 1) * Model.PageSize);%>
			</div>
			<%if (Model.MonthFilter.HasValue)
	 {%>
			<%Html.BeginForm("ExportOrderHistory", "ExportExcel", new { monthFilter = Model.MonthFilter }); %>
			<input id="export" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<%} %>
			<%else
	 {%>
			<%Html.BeginForm("ExportOrderHistory", "ExportExcel", Model.AdvanceSearchField); %>
			<input id="submit" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<%} %>
			<%Html.RenderPartial("ListOrderPartial", Model.ListOrder);%>
			<%}
		else
		{%>
			<p style="margin-top:20px; color:Red; font-weight:bold;">
			 <%=Html._("Order.ViewOrder.EmptryOrder")%></p>
			<%}
		}%>
		</div>
		<div style="min-height:100px;"></div>
	</div>
</asp:Content>
