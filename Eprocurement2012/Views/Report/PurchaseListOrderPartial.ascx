﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.ListOrder>" %>

<style type="text/css">
	.Detail
	{
		background-color: #A09B9B;
		font: bold 12px tahoma;
		padding: 5px;
		text-align: center;
		color: #FFFFFF;
	}
</style>
<div id="table-cart">

<div id="content-shopping-cart-detail" style="text-align:center;">
	<table id="table-order">

			<tr>
				<td width="30" class="thead"><%=Html._("Admin.CompanyInfo.No")%></td>

				<td width="100" class="thead"><%=Html._("Order.ViewDataOrder.OrderId")%></td>

				<td width="80" class="thead"><%=Html._("Order.ViewDataOrder.OrderStatus")%></td>

				<td width="100" class="thead"><%=Html._("Order.ViewDataOrder.OrderDate")%></td>

				<td width="100" class="thead"><%=Html._("Order.ViewDataOrder.ApprovrOrderDate")%></td>

				<td width="50" class="thead"><%=Html._("Order.ViewOrder.RequesterName")%></td>

				<td width="80" class="thead"><%=Html._("Order.ViewOrder.ApproverName")%></td>

				<td width="70" class="thead"><%=Html._("Order.ViewOrder.CostCenterName")%></td>

				<td width="50" class="thead"><%=Html._("Order.ConfirmOrder.Quantity")%></td>

				<td width="70" class="thead"><%=Html._("Order.ConfirmOrder.GrandTotalAmt")%></td>
			</tr>

		<tbody>
			<%int seq = Model.Index;
			foreach (var order in Model.Orders)
			{%>
			<tr>
				<td style="text-align:center">
					<%=++seq%>
				</td>
				<td>
					<%=Html.ActionLink(order.OrderID,"ViewOrderDetail","Order", new {Id=order.OrderGuid},null) %>
					<%if (Model.ItemOrders != null)
		{%>
						<span class="order <%=order.OrderID%>" orderid="<%=order.OrderID%>" style="font-weight: bold;
							color: Blue; font-family: serif; cursor: pointer">Detail</span>
						<%} %>
				</td>
				<td>
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus." + order.Status.ToString())%>
				</td>
				<td>
					<%=order.OrderDate.ToString(new DateTimeFormat())%>
				</td>
				<td>
					<%=order.DisplayApproveOrderDate%>
				</td>
				<td>
					<%=order.Requester.DisplayName %>
				</td>
				<td>
					<%if (order.PreviousApprover != null)
					{ %>
					<%=order.PreviousApprover.Approver.DisplayName %>
					<%}
					else
					{ %>
					<%=order.CurrentApprover.Approver.DisplayName %>
					<%} %>
				</td>
				<td>
					<%=order.CostCenter.CostCenterName %>
				</td>
				<td>
					<%=order.ItemCountOrder %>
				</td>
				<td>
					<%=order.GrandTotalAmt.ToString(new MoneyFormat())%>
				</td>
			</tr>
			<%if (Model.ItemOrders != null)
	  {%>
				<tr class="orderDetail_<%=order.OrderID%>">
					<td colspan="11">
						<table style="width: 100%;border: 1px solid #bbb;margin-bottom: 10px;">
							<tr>
								<th class="Detail">
									<%=Html._("Report.ListOrderPartial.ProductId")%>
								</th>
								<th class="Detail">
									<%=Html._("Report.ListOrderPartial.ProductName")%>
								</th>
								<th class="Detail" style="width: 150px;">
									<%=Html._("Report.ListOrderPartial.PriceIncVat")%>
								</th>
								<th class="Detail" style="width: 150px;">
									<%=Html._("Report.ListOrderPartial.PriceExcVat")%>
								</th>
								<th class="Detail" style="width: 80px;">
									<%=Html._("Report.ListOrderPartial.Quantity")%>
								</th>
								<th class="Detail" style="width: 80px;">
									<%=Html._("Report.ListOrderPartial.Unit")%>
								</th>
								<th class="Detail" style="width: 150px;">
									<%=Html._("Report.ListOrderPartial.PriceNet")%>
								</th>
							</tr>
							<%foreach (var orderDetail in Model.ItemOrders.Where(o => o.OrderId == order.OrderID))
		 {%>
							<tr class="orderDetail_<%=orderDetail.OrderId%>">
								<td>
									<%=orderDetail.Product.Id%>
								</td>
								<td>
									<%=orderDetail.Product.Name%>
								</td>
								<td style="text-align: center;">
									<%=orderDetail.Product.PriceIncVat.ToString(new MoneyFormat())%>
								</td>
								<td style="text-align: center;">
									<%=orderDetail.Product.PriceExcVat.ToString(new MoneyFormat())%>
								</td>
								<td style="text-align: center;">
									<%=orderDetail.Quantity%>
								</td>
								<td style="text-align: center;">
									<%=orderDetail.Product.Unit%>
								</td>
								<td style="text-align: right;">
									<%=orderDetail.ItemPriceNet.ToString(new MoneyFormat())%>
								</td>
							</tr>
							<%}%>
						</table>
					</td>
				</tr>
				<%} %>

			<%}%>
		</tbody>
	</table>
</div>
</div>
