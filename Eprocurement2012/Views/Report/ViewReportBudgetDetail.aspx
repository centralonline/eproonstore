﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ReportBudget>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		.format_center
		{
			text-align: center;
		}
		.format_right
		{
			text-align: right;
		}
	</style>
	<div>
		<h2 id="page-heading" style="width: 500px;">
			<%=Html._("ReportBudget.DetailBudget")%></h2>
		<%if (Model.Budgets.Any())
	{%>
		<div class="grid_2 noPrint" style="width: 120px; border: 1px solid #E5E5E5; padding: 9px;
			border-radius: 5px 5px 5px 5px; position: absolute; margin-left: 550px; margin-top: -60px;">
			<a style="color: #4570B7; ext-decoration: none; font-weight: normal" href="<%=Url.Action("ExportReportBudgetDetail", "ExportExcel", new { groupId = Model.Budgets.FirstOrDefault().Budget.GroupID, periodNo = Model.Budgets.FirstOrDefault().Budget.PeriodNo, year = Model.Budgets.FirstOrDefault().Budget.Year})%>">
				<img src="/images/Excel-icon.png" alt="" title="" />
				Export Excel</a>
		</div>

		<div>
			<table>
				<thead>
					<tr>
						<%if (Model.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Company)
						{%>
						<th class="format_center">
							<%=Html._("ReportBudget.CompanyId")%>
						</th>
						<%} %>
						<%if (Model.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Department)
						{%>
						<th class="format_center">
							<%=Html._("ReportBudget.DepartmentID")%>
						</th>
						<%} %>
						<%if (Model.Company.BudgetLevelType == Eprocurement2012.Models.Company.BudgetLevel.Costcenter)
						{%>
						<th class="format_center">
							<%=Html._("ReportBudget.CostCenterID")%>
						</th>
						<%} %>
						<th class="format_center">
							<%=Html._("ReportBudget.BudgetPeriod")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.BudgetYear")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.BudgetAmt")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.BudgetIncAmt")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.UsedAmt")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.RemainBudgetAmt")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.OrderId")%>
						</th>
						<th class="format_center">
							<%=Html._("ReportBudget.CreateOn")%>
						</th>
					</tr>
				</thead>
				<tbody>
					<%foreach (var item in Model.Budgets)
	   {%>
					<tr>
						<td class="format_center">
							<%=item.Budget.GroupID%>
						</td>
						<td class="format_center">
							<%=item.Budget.DisplayPeriodNo%>
						</td>
						<td class="format_center">
							<%=item.Budget.Year%>
						</td>
						<td class="format_right">
							<%=item.Budget.OriginalAmt.ToString(new MoneyFormat())%>
						</td>
						<td class="format_right">
							<%=item.Budget.BudgetAmt.ToString(new MoneyFormat()) %>
						</td>
						<td class="format_right">
							<%=item.Budget.UsedAmt.ToString(new MoneyFormat()) %>
						</td>
						<td class="format_right">
							<%=item.Budget.RemainBudgetAmt.ToString(new MoneyFormat()) %>
						</td>
						<td class="format_center">
							<%=item.Order.OrderID %>
						</td>
						<td class="format_center">
							<%=item.Budget.CreateOn.ToString(new DateFormat()) %>
						</td>
					</tr>
					<%} %>
				</tbody>
			</table>
		</div>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
</asp:Content>
