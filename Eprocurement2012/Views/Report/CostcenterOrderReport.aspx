﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackCostCenter>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Report.Menu.CostcenterOrder")%></h2>
		<ul class="tabs">
			<li>
				<%=Html.ActionLink(Html._("Report.RankFilter.TopTen"), "CostcenterOrder", "Report", new { RankFilter = Eprocurement2012.Models.RankFilter.TopTen }, new { @class = Model.RankFilter == Eprocurement2012.Models.RankFilter.TopTen ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.RankFilter.TopTwenty"), "CostcenterOrder", "Report", new { RankFilter = Eprocurement2012.Models.RankFilter.TopTwenty }, new { @class = Model.RankFilter == Eprocurement2012.Models.RankFilter.TopTwenty ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.RankFilter.TopFifty"), "CostcenterOrder", "Report", new { RankFilter = Eprocurement2012.Models.RankFilter.TopFifty }, new { @class = Model.RankFilter == Eprocurement2012.Models.RankFilter.TopFifty ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.Filter.All"), "CostcenterOrder", "Report", new { RankFilter = Eprocurement2012.Models.RankFilter.All }, new { @class = Model.RankFilter == Eprocurement2012.Models.RankFilter.All ? "active-link" : "" })%></li>
		</ul>
		<div>
			<%Html.BeginForm("SearchCostcenterOrder", "Report", FormMethod.Post); %>
			<h2 id="page-heading">
				<%=Html._("Report.CostcenterOrderReport.SelectTime")%></h2>
			<div>
				<%=Html.CheckBoxFor(m => m.Quarters1, new { id = "Quarters1" })%><label for="Quarters1"><%=Html._("Report.Quarters.Q1")%></label>
				<%=Html.CheckBoxFor(m => m.Quarters2, new { id = "Quarters2" })%><label for="Quarters2"><%=Html._("Report.Quarters.Q2")%></label>
				<%=Html.CheckBoxFor(m => m.Quarters3, new { id = "Quarters3" })%><label for="Quarters3"><%=Html._("Report.Quarters.Q3")%></label>
				<%=Html.CheckBoxFor(m => m.Quarters4, new { id = "Quarters1" })%><label for="Quarters4"><%=Html._("Report.Quarters.Q4")%></label>
			</div>
			<div>
				<%for (int month = 1; month <= 12; month++)
	  {%>
				<span>
					<input type="checkbox" name="Months" value="<%=month%>" id="<%=month%>" <%=Model.Months.Contains(month) ? "checked=\"checked\"" : "" %> />
					<label for="<%=month%>">
						<%=Html.GetMonthName(month)%></label>
				</span>
				<%}%>
			</div>
			<%=Html.HiddenFor(m => m.RankFilter)%>
			<br />
			<input type="submit" id="SearchOrder" name="SearchOrder.x" value="<%=Html._("Button.Search")%>" />
			<span style="color: Red;">
				<%=TempData["Message"] %></span>
			<br />
			<%Html.EndForm(); %>
		</div>
		<div>
			<h2 id="page-heading">
				<%=Html._("Report.CostcenterOrderReport.RankFilter")%></h2>
			<br />
			<%if (Model.RankFilter == Eprocurement2012.Models.RankFilter.TopTen)
	 {%>
			<%=Html._("Report.RankFilter.TopTen")%>&nbsp;
			<%}
	 else if (Model.RankFilter == Eprocurement2012.Models.RankFilter.TopTwenty)
	 {%>
			<%=Html._("Report.RankFilter.TopTwenty")%>&nbsp;
			<%}
	 else if (Model.RankFilter == Eprocurement2012.Models.RankFilter.TopFifty)
	 {%>
			<%=Html._("Report.RankFilter.TopFifty")%>&nbsp;
			<%}
	 else if (Model.RankFilter == Eprocurement2012.Models.RankFilter.All)
	 {%>
			<%=Html._("Report.RankFilter.All")%>&nbsp;
			<%} %>
			<br />
			<%if (Model.Quarters1)
	 {%>
			<%=Html._("Report.Quarters.Q1")%>&nbsp;
			<%}
	 if (Model.Quarters2)
	 {%>
			<%=Html._("Report.Quarters.Q2")%>&nbsp;
			<%}
	if (Model.Quarters3)
	{%>
			<%=Html._("Report.Quarters.Q3")%>&nbsp;
			<%}
	if (Model.Quarters4)
	{%>
			<%=Html._("Report.Quarters.Q4")%>&nbsp;
			<%} %>
			<br />
			<%for (int month = 1; month <= 12; month++)
	 {
		 if (Model.Months.Contains(month))
		 {%>
			<%=Html.GetMonthName(month)%>&nbsp;
			<%}
	}%>
		</div>
		<br />
		<br />
		<div>
			<%if (Model.ListSummaryOrder != null)
	 {
		 if (Model.ListSummaryOrder.Count() != 0)
		 {%>
			<%Html.BeginForm("ExportCostcenterOrder", "ExportExcel", Model); %>
			<input id="export" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<div id="search-data">
				<%Html.BeginForm("SearchCostcenterOrder", "Report", FormMethod.Post); %>
				<table id="table-order">
					<thead>
						<tr>
							<th>
								<%=Html._("Admin.CompanyInfo.No")%>
							</th>
							<th>
								<%=Html._("Order.ViewDataOrder.CostCenterId")%>
							</th>
							<th>
								<%=Html._("Order.ViewOrder.CostCenterName")%>
							</th>
							<th>
								<%=Html._("Order.ViewDataOrder.GrandTotalAmount")%>
							</th>
							<th>
								<%=Html._("Order.ViewDataOrder.CountOrder")%>
							</th>
							<th>
								<%=Html._("OFMAdmin.Index.Check")%>
							</th>
						</tr>
					</thead>
					<tbody>
						<%int index = 0;
		foreach (var order in Model.ListSummaryOrder)
		{%>
						<tr>
							<td>
								<%=++index%>
							</td>
							<td>
								<%=order.CostcenterId %>
							</td>
							<td>
								<%=order.CostcenterName %>
							</td>
							<td>
								<%=order.SumGrandTotalAmt.ToString(new MoneyFormat()) %>
							</td>
							<td>
								<%=order.CountOrder%>
							</td>
							<td>
								<input type="submit" id="CheckDetail" name="CheckDetail.<%=order.CostcenterId%>.x"
									value="<%=Html._("Admin.CompanyInfo.Verify")%>" />
							</td>
						</tr>
						<%}%>
					</tbody>
				</table>
			</div>
			<%Html.EndForm(); %>
			<%}
		else
		{%>
			<%--<h3>
				<%=Html._("Order.ViewOrder.EmptryOrder")%></h3>--%>
				<p style="margin-top:20px; color:Red; font-weight:bold;">
			 <%=Html._("Order.ViewOrder.EmptryOrder")%></p>
			<%}
	}%>
		</div>
		<div style="min-height:100px;"></div>
	</div>
</asp:Content>
