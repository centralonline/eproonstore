﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		thead td
		{
			background-color: #e6e6e6;
			font: bold 12px tahoma;
			padding: 5px;
			text-align: center;
			border-top: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			border-right: 1px solid #bbb;
		}
		
		tbody td
		{
			border-top: 1px solid #bbb;
			border-bottom: 1px solid #bbb;
			background: #eee;
			border-right: 1px solid #bbb;
		}
		
		#table-wrapper
		{
			position: relative;
		}
		#table-scroll
		{
			height: auto;
			overflow: auto;
			margin-top: 20px;
		}
		#table-wrapper table
		{
			width: 100%;
		}
		/*#table-wrapper table *
		{
			background: yellow;
			color: black;
		}*/
		#table-wrapper table thead th .text
		{
			position: absolute;
			top: -20px;
			z-index: 2;
			height: 20px;
			width: 35%;
			border: 1px solid red;
		}
	</style>
	<div class="spaceleft20">
		<div class="GroupData">
			<h2 id="page-heading">
				<%=Html._("รายงานการสั่งซื้อตาม Supplier")%></h2>
		</div>
		<%Html.BeginForm("ReportProductBySupplier", "Report", FormMethod.Post, new { id = "formProductSupplier" });%>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckSupplier, new { id = "CheckSupplier" })%>
			<label for="CheckSupplier">
				Supplier
			</label>
			<div id="divSupplier" style="width: 50%">
				<table>
					<thead>
						<tr>
							<td>
								เลือก
							</td>
							<td>
								รหัส Supplier
							</td>
							<td>
								ชื่อ Supplier
							</td>
						</tr>
					</thead>
					<%foreach (var item in Model.AdvanceSearchField.ListSuppliers)
	   {
		   string supplierChecked = "";
		   if (Model.AdvanceSearchField.Supplier.Any(m => m == item.SupplierID))
		   {
			   supplierChecked = "checked=checked";
		   }%>
					<tbody>
						<tr>
							<td>
								<input id="<%=item.SupplierID %>" name="Supplier" type="checkbox" value="<%=item.SupplierID %>"
									<%=supplierChecked%> />
							</td>
							<td>
								<label for="<%=item.SupplierID %>">
									<%=item.SupplierID %></label>
							</td>
							<td>
								<%=item.SupplierName %>
							</td>
						</tr>
					</tbody>
					<%} %>
				</table>
			</div>
		</div>
		<br />
		<div id="OrderDate">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
			<label for="CheckOrderDate">
				<%=Html._("Order.ViewDataOrder.OrderDate")%></label><br />
			<%=Html._("Report.Conditional.From")%>
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
			<%=Html._("Report.Conditional.To")%>
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateTo, new { id = "OrderDateTo" })%>
		</div>
		<br />
		<div id="ApproveDate">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckAppDate, new { id = "CheckAppDate" })%>
			<label for="CheckAppDate">
				<%=Html._("Order.ViewDataOrder.ApprovrOrderDate")%></label><br />
			<%=Html._("Report.Conditional.From")%>
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.AppDateFrom, new { id = "AppDateFrom" })%>
			<%=Html._("Report.Conditional.To")%>
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.AppDateTo, new { id = "AppDateTo" })%>
		</div>
		<br />

		<%=Html.Hidden("companyId",Model.AdvanceSearchField.CompanyId)%>
		<input type="submit" class="graybutton" value="<%=Html._("Button.Search")%>" />
		<span style="color: Red;">
			<%=TempData["MessageError"] %></span>
		<%Html.EndForm(); %>

		<!--Start Export Excel-->
		<%if (Model.AdvanceSearchField.ListProductSuppliers != null && Model.AdvanceSearchField.ListProductSuppliers.Any())
	{%>
		<div>
			<%Html.BeginForm("ExportSupplier", "ExportExcel", Model.AdvanceSearchField); %>
			<%
	 var data = new RouteValueDictionary { { "AdvanceSearchField", Model.AdvanceSearchField } };

	 data.Add("checkOrderDate", Model.AdvanceSearchField.CheckOrderDate);
	 data.Add("checkSupplier", Model.AdvanceSearchField.CheckSupplier);
	 data.Add("orderDateFrom", Model.AdvanceSearchField.OrderDateFrom);
	 data.Add("orderDateTo", Model.AdvanceSearchField.OrderDateTo);
	 data.Add("checkAppDate", Model.AdvanceSearchField.CheckAppDate);
	 data.Add("appDateFrom", Model.AdvanceSearchField.AppDateFrom);
	 data.Add("appDateTo", Model.AdvanceSearchField.AppDateTo);

	 for (int i = 0; i < Model.AdvanceSearchField.Supplier.Length; i++)
	 {
		 data.Add(string.Format("Supplier[{0}]", i), Model.AdvanceSearchField.Supplier[i]);
			%>
			<input type="hidden" name="supplier" value="<%=Model.AdvanceSearchField.Supplier[i]%>" />
			<%}%>
			<input id="Export" name="ExportExcel" type="submit" value="Export to Excel" />
			<%=Html.Hidden("companyId", Model.AdvanceSearchField.CompanyId)%>
			<%Html.EndForm(); %>
		</div>
		<%}%>
		<!--End Export Excel-->

		<%if (Model.AdvanceSearchField.ListProductSuppliers != null && Model.AdvanceSearchField.ListProductSuppliers.Any())
	{
		
		%>
		<%foreach (var ListProduct in Model.AdvanceSearchField.ListProductSuppliers.GroupBy(s => s.SupplierID))
	{
		int count = 1;
		%>
		<br />
		<h5>
			<%=ListProduct.FirstOrDefault().DisplaySupplier%>
		</h5>
		<div id="table-wrapper">
			<div id="table-scroll">
				<table class="scroll">
					<thead>
						<tr>
							<td colspan="4" rowspan="3">
								BRANCH
							</td>
							<%foreach (var pid in ListProduct.GroupBy(p => p.OrderDetail.Product.Id).OrderBy(p => p.Key))
		 {%>
							<td colspan="3">
								<%=pid.Key %>
							</td>
							<% } %>
							<td colspan="3" rowspan="3">
								Grand Total
							</td>
						</tr>
						<tr>
							<%foreach (var pid in ListProduct.GroupBy(p => p.OrderDetail.Product.Id).OrderBy(p => p.Key))
		 {%>
							<td colspan="3">
								<%=pid.FirstOrDefault().OrderDetail.Product.Name%>
							</td>
							<% } %>
						</tr>
						<tr>
							<%foreach (var pid in ListProduct.GroupBy(p => p.OrderDetail.Product.Id).OrderBy(p => p.Key))
		 {%>
							<td>
								Cost
							</td>
							<td>
								Qty
							</td>
							<td>
								Amount
							</td>
							<% } %>
						</tr>
					</thead>
					<tbody>
						<%foreach (var branch in ListProduct.GroupBy(p => p.Order.CostCenter.CostCenterID).OrderBy(p => p.Key))
		{%>
						<tr>
							<td>
								<%=count++%>
							</td>
							<td>
								<%=branch.Key %>
							</td>
							<td>
								<%=branch.FirstOrDefault().Order.CostCenter.OracleCode %>
							</td>
							<td>
								<%=branch.FirstOrDefault().Order.CostCenter.CostCenterName %>
							</td>
							<%foreach (var pid in ListProduct.GroupBy(p => p.OrderDetail.Product.Id).OrderBy(p => p.Key))
		 {%>
							<td style="text-align:center">
								<%=pid.FirstOrDefault().OrderDetail.ItemExcVatPrice %>
							</td>
							<td style="text-align:center">
								<%if (pid.Any(c => c.Order.CostCenter.CostCenterID == branch.Key))
		  {%>
								<%=pid.Where(p => p.Order.CostCenter.CostCenterID == branch.Key).Sum(p => p.OrderDetail.Quantity)%>
								<%}
		  else
		  {%>
								-
								<%} %>
							</td>
							<td style="text-align:right">
								<%if (pid.Any(c => c.Order.CostCenter.CostCenterID == branch.Key))
		  {%>
								<%=pid.Where(p => p.Order.CostCenter.CostCenterID == branch.Key).Sum(p => p.OrderDetail.ItemExcVatPrice * p.OrderDetail.Quantity).ToString(new MoneyFormat())%>
								<%}
		  else
		  {%>
								-
								<%} %>
							</td>
							<% } %>
							<td style="text-align:right">
								<%=branch.Sum(p => p.OrderDetail.ItemExcVatPrice * p.OrderDetail.Quantity).ToString(new MoneyFormat())%>
							</td>
						</tr>
						<% } %>
					</tbody>
				</table>
			</div>
		</div>
		<%} %>
		<%} %>
		<%else
	{%>
		<span style="color: Red;">ไม่พบข้อมูลค่ะ</span>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScript" runat="server">
	<script type="text/javascript">
		$(function () {

			if ($("#CheckSupplier").attr("checked")) { $("#divSupplier").show(); } else { $("#divSupplier").hide(); }

			$("#CheckSupplier").live("click", function () {
				if (this.checked) {
					$("#divSupplier").show();
				}
				else {
					$("#divSupplier").hide();
				}
			});

			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#OrderDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#AppDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#AppDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

		});
	</script>
</asp:Content>
