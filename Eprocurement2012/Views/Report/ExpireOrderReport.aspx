﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/ReportAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="JavasCript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Report.Menu.ExpireOrder")%></h2>
		<ul class="tabs">
			<li>
				<%=Html.ActionLink(Html._("Report.DateFilter.Today"), "ExpireOrder", "Report", new { Page = 1, DateFilter = Eprocurement2012.Models.DateFilter.Today }, new { @class = Model.DateFilter.HasValue && Model.DateFilter.Value == Eprocurement2012.Models.DateFilter.Today ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.DateFilter.1To3Day"), "ExpireOrder", "Report", new { Page = 1, DateFilter = Eprocurement2012.Models.DateFilter.OneToThreeDays }, new { @class = Model.DateFilter.HasValue && Model.DateFilter.Value == Eprocurement2012.Models.DateFilter.OneToThreeDays ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.DateFilter.4To7Day"), "ExpireOrder", "Report", new { Page = 1, DateFilter = Eprocurement2012.Models.DateFilter.FourToSevenDays }, new { @class = Model.DateFilter.HasValue && Model.DateFilter.Value == Eprocurement2012.Models.DateFilter.FourToSevenDays ? "active-link" : "" })%></li>
			<li>
				<%=Html.ActionLink(Html._("Report.DateFilter.7DayUp"), "ExpireOrder", "Report", new { Page = 1, DateFilter = Eprocurement2012.Models.DateFilter.SevenDaysLater }, new { @class = Model.DateFilter.HasValue && Model.DateFilter.Value == Eprocurement2012.Models.DateFilter.SevenDaysLater ? "active-link" : "" })%></li>
		</ul>
		<div>
			<h2 id="page-heading">
				<%=Html._("Report.SearchOrder")%></h2>
			<%Html.BeginForm("SearchExpireOrder", "Report", FormMethod.Post, new { id = "formExpireOrder" }); %>
			<div>
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
				<label for="CheckOrderDate">
					<%=Html._("Order.ViewDataOrder.OrderDate")%></label><br />
				<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
			</div>
			<div>
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderId, new { id = "CheckOrderId" })%>
				<label for="CheckOrderId">
					<%=Html._("Order.ViewDataOrder.OrderId")%></label><br />
				<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderId)%>
			</div>
			<div>
				<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckUserId, new { id = "CheckUserId" })%>
				<label for="CheckUserId">
					<%=Html._("Order.ViewOrder.EmailRequester")%></label><br />
				<%=Html.TextBoxFor(m => m.AdvanceSearchField.UserId)%>
			</div>
			<input type="submit" value="<%=Html._("Button.Search")%>" />
			<span style="color: Red;">
				<%=TempData["Message"] %></span>
			<%Html.EndForm(); %>
		</div>
		<div>
			<%if (Model.ListOrder.Orders != null)
	 {
		 if (Model.TotalOrdersCount != 0)
		 {%>
			<div class="pager">
				<%if (Model.DateFilter.HasValue)
	  { %>
				<%=Html.Pager(Model.PageSize, Model.PageNumber, Model.TotalOrdersCount, new { dateFilter = Model.DateFilter })%>
				<%} %>
				<%Model.ListOrder.Index = ((Model.PageNumber - 1) * Model.PageSize);%>
			</div>
			<%if (Model.DateFilter.HasValue)
	 {%>
			<%Html.BeginForm("ExportExpireOrder", "ExportExcel", new { dateFilter = Model.DateFilter }); %>
			<input id="export" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<%} %>
			<%else
	 {%>
			<%Html.BeginForm("ExportExpireOrder", "ExportExcel", Model.AdvanceSearchField); %>
			<input id="submit" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<%} %>
			<%Html.RenderPartial("ListOrderPartial", Model.ListOrder);%>
			<%}
		else
		{%><%--<h3>
			<%=Html._("Order.ViewOrder.EmptryOrder")%></h3>--%>
			<p style="margin-top:20px; color:Red; font-weight:bold;">
			 <%=Html._("Order.ViewOrder.EmptryOrder")%></p>
			<%}
		}%>
		</div>
		<div style="min-height:100px;"></div>
	</div>
</asp:Content>
