﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Report/Report.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="JavaScript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#OrderDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#AppDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#AppDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			if ($("#CheckOrderStatus").attr("checked")) { $("#divOrderStatus").show(); } else { $("#divOrderStatus").hide(); }

			$("#CheckOrderStatus").live("click", function () {
				if (this.checked) {
					$("#divOrderStatus").show();
				}
				else {
					$("#divOrderStatus").hide();
				}
			});

			$(".order").click(function () {
				var orderId = $(this).attr("orderId");
				$(".orderDetail_" + orderId).toggle();
			});

		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="spaceleft20">
	<div class="GroupData">
	<h3><img src="../../images/theme/h3-list-icon.png" alt="Conditional Order" /><%=Html._("Report.Menu.ConditionalOrder")%></h3>
	</div>
	<%Html.BeginForm("PurchaseConditionalOrder", "Report", FormMethod.Post, new { id = "fromConditionalOrder" }); %>
	<div id="OrderStatus">
		<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderStatus, new { id = "CheckOrderStatus" })%>
		<label for="CheckOrderStatus">
				<%=Html._("Order.ViewDataOrder.OrderStatus")%></label>
		<div id="divOrderStatus" style="border: thin outset #CCCCCC; margin: 10px 20px 0px 30px; padding: 10px;">
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusWaiting, new { id = "Waiting" })%>
			<label for="Waiting">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Waiting")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusPartial, new { id = "Partial" })%>
			<label for="Partial">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Partial")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusRevise, new { id = "Revise" })%>
			<label for="Revise">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Revise")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusAdminAllow, new { id = "AdminAllow" })%>
			<label for="AdminAllow">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.AdminAllow")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusWaitingAdmin, new { id = "WaitingAdmin" })%>
			<label for="WaitingAdmin">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.WaitingAdmin")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusApproved, new { id = "Approved" })%>
			<label for="Approved">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Approved")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusShipped, new { id = "Shipped" })%>
			<label for="Shipped">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Shipped")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusCompleted, new { id = "Completed" })%>
			<label for="Completed">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Completed")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusDeleted, new { id = "Deleted" })%>
			<label for="Deleted">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Deleted")%></label><br />
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusExpired, new { id = "Expired" })%>
			<label for="Expired">
				<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Expired")%></label>
		</div>
	</div>
	<br />
	<div id="OrderDate">
		<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
		<label for="CheckOrderDate">
			<%=Html._("Order.ViewDataOrder.OrderDate")%></label>
		<br />
		<%=Html._("Report.Conditional.From")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
		<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateTo, new { id = "OrderDateTo" })%>
	</div>
	<br />
	<div id="ApproveDate">
		<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckAppDate, new { id = "CheckAppDate" })%>
		<label for="CheckAppDate">
			<%=Html._("Order.ViewDataOrder.ApprovrOrderDate")%></label>
		<br />
		<%=Html._("Report.Conditional.From")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.AppDateFrom, new { id = "AppDateFrom" })%>
		<%=Html._("Report.Conditional.To")%><%=Html.TextBoxFor(m => m.AdvanceSearchField.AppDateTo, new { id = "AppDateTo" })%>
	</div>
	<br />
	<input type="submit"class="graybutton" value="<%=Html._("Button.Search")%>" />
	<span style="color: Red;">
		<%=TempData["Message"] %></span>
	<%Html.EndForm(); %>
	<div>
		<%if (Model.ListOrder.Orders != null)
	{
		if (Model.TotalOrdersCount != 0)
		{%>
		<div class="pager">
			<%=Html.Pager(Model.PageSize, Model.PageNumber, Model.TotalOrdersCount, Model.AdvanceSearchField)%>
			<%Model.ListOrder.Index = ((Model.PageNumber - 1) * Model.PageSize);%>
		</div>

		<%Html.BeginForm("ExportPurchaseConditionalOrder", "ExportExcel", Model.AdvanceSearchField); %>
<%--		<input class="graybutton" id="Export1" type="submit" value="Export to Excel"/>
--%>		<input class="graybutton" id="Export" name="ExportHead.x" type="submit" value="Export Head"/>
		<input class="graybutton" id="ExportDetail" name="ExportDetail.x" type="submit" value="Export Detail" />
		<%Html.EndForm(); %>


		<%Html.RenderPartial("PurchaseListOrderPartial", Model.ListOrder);%>
		<%}
		else
		{%><%--<h3>
				<%=Html._("Order.ViewOrder.EmptryOrder")%></h3>--%>
				<p style="margin-top:20px; color:Red; font-weight:bold;">
			 <%=Html._("Order.ViewOrder.EmptryOrder")%></p>
		<%}
		}%>
	</div>
	<div style="min-height:100px;"></div>
	</div>
</asp:Content>
