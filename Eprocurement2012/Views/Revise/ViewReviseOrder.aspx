﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CartRevise>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<%Html.RenderPartial("CartReviseDetailPartial", Model.Cart); %>
		<div class="eclear">
		</div>
		<div class="GroupData">
			<%Html.BeginForm("HandleActionRevise", "Revise", FormMethod.Post, new { enctype = "multipart/form-data" });%>
			<h4>
				<%=Html._("Order.ViewOrder.Remark")%></h4>
			<div class="border2">
				<label>
					<%=Html._("Order.ViewOrder.ApproverRemark")%>
					:</label>
				<input id="ApproverRemark" name="ApproverRemark" type="text" class="large" value="<%=!string.IsNullOrEmpty(Model.ApproverRemark) ? Model.ApproverRemark : ""%>" maxlength="200"/>
				<br />
				<label>
					<%=Html._("Order.ViewOrder.OFMRemark")%>
					:</label>
				<input id="OFMRemark" name="OFMRemark" type="text" class="large" value="<%=!string.IsNullOrEmpty(Model.OFMRemark) ? Model.OFMRemark : ""%>" maxlength="200"/>
				<br />
				<label>
					<%=Html._("Order.ViewOrder.ReferenceRemark")%>
					:</label>
				<input id="ReferenceRemark" name="ReferenceRemark" type="text" class="large" value="<%=!string.IsNullOrEmpty(Model.ReferenceRemark) ? Model.ReferenceRemark : ""%>" maxlength="200"/>
				<br />
				<div class="eclear">
				</div>
				<!--Start AttachFile--->
				<label for="file">
					<%=Html._("Order.AttachFile")%>
					:</label>
				<input type="file" name="file" id="file" style="border: 1px solid #B8B4B4;" />
				<input type="submit" name="UploadFile.x" value="<%=Html._("Shared.MyImage.UploadFile")%>" />
				<a class="help1">
					<img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
				<%if (!String.IsNullOrEmpty(Model.Order.CustFileName))
	  {%>
				<%=Model.Order.CustFileName%>
				<input name="DeleteAttachFile.x" type="image" src="/images/icon/icon_wrong.png" alt="ลบเอกสารแนบ"
					title="ลบเอกสารแนบค่ะ" />
				<%} %>
				<span style="color: Red;">
					<%=TempData["errormessage"]%></span>
				<%=Html.HiddenFor(m => m.Cart.ShoppingCartId)%>
				<%=Html.HiddenFor(m => m.Cart.CustFileName)%>
				<%=Html.HiddenFor(m => m.Cart.SystemFileName)%>
				<div id="All" style="padding: 10px; width: 200px; height: 30px; background-color: #CCCCCC;
					margin-left: 520px;">
					<p>
						<%=Html._("Content.AttachFile")%></p>
				</div>
				<br />
				<br />
				<!--End AttachFile--->
				<br />
				<input class="graybuttonLarge" id="NewOrder" name="NewOrder.x" type="submit" value="<%=Html._("Revise.ButtonNewOrder") %>" />
				<input class="graybuttonLarge" id="CancelReviseOrder" name="CancelReviseOrder.x"
					type="submit" value="<%=Html._("Revise.ButtonCancel") %>" />
				<input class="graybuttonLarge" id="SendToApprover" name="SendToApprover.x" type="submit"
					value="<%=Html._("Revise.ButtonSendToApprover") %>" />
			</div>
			<% Html.EndForm(); %>
		</div>
		<div class="GroupData">
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ViewOrder.StepProcess")%></h4>
				<div class="border2">
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderId")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderID %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderDate")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderStatus")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Html._(Model.Order.Status.ToResourceKey())%>
							<a href="/Help/OrderInstruction" target="_blank">
								<%=Html._("Order.ViewOrder.OrderDetail")%></a></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewOrder.WaitingApproval")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.NextApprover.Approver.DisplayName%></span>
					</div>
					<div class="eclear">
					</div>
					<%Html.RenderPartial("OrderDataFlow", Model.Order); %>
				</div>
			</div>
		</div>
		<!--end GroupData-->
		<!--Start printOrder detail-->
		<div class="GroupData">
			<div class="Column-Full">
				<!--Costcenter Budget-->
				<%if (Model.CurrentBudget != null)
	  {%>
				<h4>
					<%=Html._("CartDetail.CurrentBudget")%>
					[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName%></h4>
				<div class="border2">
					<div class="ApproveStyle">
						<%Html.RenderPartial("CurrentBudget", Model.CurrentBudget); %>
					</div>
					<div class="eclear">
					</div>
					<%if (Model.Cart.SendToAdminAppBudg)
	   { %>
					<div class="position">
					</div>
					<div class="left">
						<label>
							<%=Html._("CartDetail.CurrentBudget.Caution")%>
						</label>
					</div>
					<div class="eclear">
					</div>
					<%} %>
				</div>
				<%} %>
				<div class="eclear">
				</div>
			</div>
		</div>
		<!--end GroupData-->
		<div class="left" style="width: 450px; min-height: 250px; padding: 5px; margin-right: 9px;
			margin-top: 0px; border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;">
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.CustID")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=Model.Order.CostCenter.CostCenterCustID %></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("CartDetail.ShipContactor")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=Model.Order.Contact.ContactorName %></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.UserId")%>
					:
				</label>
			</div>
			<div style="float: left;">
				<p class="printOrderSpanGray">
					<%=Model.Order.Requester.UserId%></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.Phone")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=Model.Order.Contact.ContactorPhone %>
					<%=string.IsNullOrEmpty(Model.Order.Contact.ContactorExtension) ? "" : " #" + Model.Order.Contact.ContactorExtension%></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.Fax")%>
					:
				</label>
			</div>
			<div style="float: left;">
				<p class="printOrderSpanGray">
					<%=Model.Order.Contact.ContactorFax %></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.InvoiceAddress")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=Model.Order.Invoice.Address1 %>
					<%=Model.Order.Invoice.Address2 %>
					<%=Model.Order.Invoice.Address3 %>
					<%=Model.Order.Invoice.Address4 %>
				</p>
			</div>
			<div class="eclear">
			</div>
		</div>
		<div class="right" style="width: 450px; min-height: 250px; margin-right: 9px; margin-top: 0px;
			border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
			padding: 5px;">
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewDataOrder.CompanyId")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=Model.Order.Company.CompanyId %></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.CompanyName")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=Model.Order.Company.CompanyName %></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.DepartmentName")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=string.IsNullOrEmpty(Model.Order.Department.DepartmentID)? "-": "[" + Model.Order.Department.DepartmentID + "] " + Model.Order.Department.DepartmentName %>
				</p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.CostCenterName")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName %></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewDataOrder.OrderDate")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></p>
			</div>
			<div class="eclear">
			</div>
			<div class="printOrderlabel">
				<label>
					<%=Html._("Order.ViewOrder.ShippingAddress")%>
					:
				</label>
			</div>
			<div class="left">
				<p class="printOrderSpanGray">
					<%=Model.Order.Shipping.Address1 %>
					<%=Model.Order.Shipping.Address2 %>
					<%=Model.Order.Shipping.Address3 %>
					<%=Model.Order.Shipping.Address4 %>
					<%=Model.Order.Shipping.Province %></p>
			</div>
			<div class="eclear">
			</div>
		</div>
		<!--end printOrder detail-->
		<div class="eclear">
		</div>
		<div class="Column-Full">
			<h4>
				<%=Html._("Order.ViewOrder.OrderActivity")%></h4>
			<div class="eclear">
			</div>
			<%Html.RenderPartial("OrderActivityPartial", Model.OrderActivity); %>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
			$("#All").hide();
			$(".help1").click(function () {
				$("#All").slideToggle("slow");
			});
		});
	</script>
</asp:Content>
