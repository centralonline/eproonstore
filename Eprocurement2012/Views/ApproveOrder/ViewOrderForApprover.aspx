﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<div class="step">
			<img alt="Step 3" src="/images/theme/Step-3.jpg" /></div>
		<div class="GroupData">
			<h3>
				<img alt="Step 3" src="/images/theme/h3-list-icon.png" /><%=Html._("Order.ViewOrder.OrderCompleted")%></h3>
		</div>
		<!--column right-->
		<div class="GroupData">
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ViewOrder.StepProcess")%></h4>
				<div class="border2">
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderId")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderID %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderDate")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderStatus")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Html._(Model.Order.Status.ToResourceKey())%>
							<a href="/Help/OrderInstruction" target="_blank">
								<%=Html._("Order.ViewOrder.OrderDetail")%></a> </span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewOrder.WaitingApproval")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%if (Model.Order.CurrentApprover == null)
		 {%>
							<%=Model.Order.PreviousApprover.Approver.DisplayName%>
							<%} %>
							<%else
		 {%>
							<%=Model.Order.CurrentApprover.Approver.DisplayName%>
							<%} %>
						</span>
					</div>
					<div class="eclear">
					</div>
					<%Html.RenderPartial("OrderDataFlow", Model.Order); %>
				</div>
			</div>
		</div>
		<!--end GroupData--->
		<div class="eclear">
		</div>
		<!--Print-->
		<div class="eclear">
		</div>
		<div class="box" style="position: absolute; margin-left: 740px; margin-top: 50px;">
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintTHPO"})%>">
					Print PO (TH)</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid, ActionName = "PrintEnPO"})%>">
					Print PO (EN)</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintThQU"})%>">
					Print QU (TH)</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintEnQU"})%>">
					Print QU (EN)</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_printOrder.jpg" />
				<a href="#" onclick="window.print(); return false">Print web form</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/Excel-icon.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintExcel"})%>">
					Export Excel</a>
			</div>
		</div>
		<div id="print" class="right noPrint btnPrint">
			<img src="/images/theme/icon_printOrder.jpg" />
			<span>Print Document</span>
		</div>
		<div style="margin: 30px 0;">
			<img alt="logo" src="/images/theme/logo-ePro.jpg" class="left" />
			<div class="eclear">
			</div>
			<!--Start printOrder detail-->
			<div style="width: 100%; padding: 20px 8px 8px 0px; margin-right: 20px; margin-top: 35px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderID %></p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="left" style="width: 450px; min-height: 250px; padding: 5px; margin-right: 9px;
				margin-top: 0px; border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CustID")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.CostCenter.CostCenterCustID %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("CartDetail.ShipContactor")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.UserId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Requester.UserId%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Phone")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorPhone%>
						<%=string.IsNullOrEmpty(Model.Order.Contact.ContactorExtension) ? "" : " #" + Model.Order.Contact.ContactorExtension%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Fax")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorFax %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.InvoiceAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Invoice.Address1 %>
						<%=Model.Order.Invoice.Address2 %>
						<%=Model.Order.Invoice.Address3 %>
						<%=Model.Order.Invoice.Address4 %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="right" style="width: 450px; min-height: 250px; margin-right: 9px; margin-top: 0px;
				border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
				padding: 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.CompanyId")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyId %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CompanyName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.DepartmentName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=string.IsNullOrEmpty(Model.Order.Department.DepartmentID)? "-": "[" + Model.Order.Department.DepartmentID + "] " + Model.Order.Department.DepartmentName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CostCenterName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderDate")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.ShippingAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Shipping.Address1 %>
						<%=Model.Order.Shipping.Address2 %>
						<%=Model.Order.Shipping.Address3 %>
						<%=Model.Order.Shipping.Address4 %>
						<%=Model.Order.Shipping.Province %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<!--end printOrder detail-->
			<div class="eclear">
			</div>
			<div class="printOrderlabel" style="padding-top: 20px; padding-bottom: 10px;">
				<label>
					<%=Html._("Order.ViewOrder.OrderItems")%>
				</label>
			</div>
			<%Html.RenderPartial("OrderDetailPartial", Model.Order); %>
			<div class="eclear">
			</div>
			<div class="GroupData">
				<div class="Column-Full">
					<h4>
						<%=Html._("Order.ViewOrder.Remark")%></h4>
					<div class="border2">
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.ApproverRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.ApproverRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.OFMRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.OFMRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.ReferenceRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.ReferenceRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<!--Show AttachFile -->
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.AttachFile")%>
								:</label>
						</div>
						<div class="left">
							<span class="printOrderSpanGray">
								<%if (!String.IsNullOrEmpty(Model.Order.CustFileName))
		  {%>
								<a target="_blank" href="<%=Url.Action("DownloadAttachFile", "Order", new { systemFileName = Model.Order.SystemFileName})%>">
									<%=Model.Order.CustFileName %></a>
								<%} %>
								<%else
		  {%>
								<span>-</span>
								<%} %>
							</span>
						</div>
						<div class="eclear">
						</div>
						<!--End AttachFile -->
					</div>
				</div>
				<!--Column-Full-->
				<div class="eclear">
				</div>
				<div class="Column-Full">
					<h4>
						<%=Html._("Order.ViewOrder.OrderActivity")%></h4>
					<div class="eclear">
					</div>
					<%Html.RenderPartial("OrderActivityPartial", Model.OrderActivity); %>
				</div>
				<div class="eclear">
				</div>
				<div class="spaceTop">
				</div>
				<div class="Column-Full">
					<!--Costcenter Budget-->
					<%if (Model.CurrentBudget != null)
	   {%>
					<h4>
						<%=Html._("CartDetail.CurrentBudget")%>
						[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName %></h4>
					<div class="border2">
						<div class="ApproveStyle">
							<%Html.RenderPartial("CurrentBudget", Model.CurrentBudget); %>
						</div>
						<div class="eclear">
						</div>
						<%if (Model.Order.SendToAdminAppBudg)
		{%>
						<div class="position">
						</div>
						<div class="left">
							<%=Html._("CartDetail.CurrentBudget.Caution")%>
						</div>
						<div class="eclear">
						</div>
						<%} %>
					</div>
				</div>
				<div class="eclear">
				</div>
				<%} %>
				<!--Design Approve Box-->
				<%if (Model.Order.CurrentApprover != null && string.Equals(Model.Order.CurrentApprover.Approver.UserId, Model.User.UserId))
	  { %>
				<div class="Column-Full">
					<h4>
						<%=Html._("ApproveOrder.ViewOrderForApprover.OrderConsider")%></h4>
					<div class="border2">
						<div class="completeOrderStyle">
							<div class="completeOrderPosition">
								<h5>
									<%=Html._("ApproveOrder.ViewOrderForApprover.OrderConsider")%>
									<a href="/Help/OrderMoreInstruction" target="_blank">
										<%=Html._("ApproveOrder.ViewOrderForApprover.LearnMore")%></a></h5>
								<% Html.BeginForm("ConfirmHandleOrder", "ApproveOrder", FormMethod.Post);%>
								<input name="orderGuid" type="hidden" value="<%=Model.Order.OrderGuid %>" />
								<table>
									<tr>
										<%if (Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Approved
											&& Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Revise
											&& Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Shipped
											&& Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Deleted
											&& Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Expired
											&& Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Completed)
			{%>
										<%if ((Model.User.IsAdmin || Model.User.IsApprover || Model.User.IsAssistantAdmin)
											&& string.Equals(Model.Order.CurrentApprover.Approver.UserId, Model.User.UserId))
			{%>
										<%if (Model.Order.GrandTotalAmt > Model.Order.CurrentApprover.ApproveCreditLimit && Model.Order.NextApprover != null
											&& (!Model.Order.SendToAdminAppBudg || Model.Order.AdminAllowFlag)
											&& Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin)
			{%>
										<td>
											<input id="PartialApprove" name="PartialApprove.x" type="submit" value="Partial Approve"
												class="Approvebutton" style="margin: 0 10px;" />
											<img src="/images/loadingcreateorder.gif" id="loadingPartialApprove" style="display: none"
												alt="Order" class="right" />
											<a href="/Help/PartialApprover" target="_blank">
												<img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
										</td>
										<%} %>
										<%else if (Model.Order.GrandTotalAmt < Model.Order.CurrentApprover.ApproveCreditLimit
											&& (!Model.Order.SendToAdminAppBudg || Model.Order.AdminAllowFlag)
											&& Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin)
			{%>
										<td>
											<input id="ApproveOrder" name="ApproveOrder.x" type="submit" value="Approve" class="Approvebutton"
												style="margin: 0 10px;" />
											<img src="/images/loadingcreateorder.gif" id="loadingApproveOrder" style="display: none"
												alt="Order" class="right" />
											<a href="/Help/ApproverOrder" target="_blank">
												<img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
										</td>
										<%} %>
										<%else if (Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin)
			{ %>
										<td>
											<input id="AdminAllow" name="AdminAllow.x" type="submit" value="Admin Allow" class="Approvebutton"
												style="margin: 0 10px;" />
											<img src="/images/loadingcreateorder.gif" id="loadingAdminAllow" style="display: none"
												alt="Order" class="right" />
											<a href="/Help/AdminAllowOrder" target="_blank">
												<img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
										</td>
										<%} %>
										<td>
											<input id="DeleteOrder" name="DeleteOrder.x" type="submit" value="Delete" class="graybutton"
												style="margin: 0 10px;" />
											<img src="/images/loadingcreateorder.gif" id="loadingDeleteOrder" style="display: none"
												alt="Order" class="right" />
											<a href="/Help/DeleteOrder" target="_blank">
												<img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
										</td>
										<td>
											<input id="ReviseOrder" name="ReviseOrder.x" type="submit" value="Revise" class="graybutton"
												style="margin: 0 10px;" />
											<img src="/images/loadingcreateorder.gif" id="loadingReviseOrder" style="display: none"
												alt="Order" class="right" />
											<a href="/Help/ReviseOrder" target="_blank">
												<img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
										</td>
										<%} %>
										<%} %>
									</tr>
								</table>
								<%Html.EndForm();%>
							</div>
						</div>
					</div>
					<div class="eclear">
					</div>
				</div>
				<!--Design Approve Box-->
				<%} %>
			</div>
			<!--end GroupData-->
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.box
		{
			width: 200px;
			padding: 10px;
			margin: 10px;
			height: 250px;
		}
		
		.btnPrint
		{
			width: 120px;
			border: 1px solid #E5E5E5;
			border-radius: 5px 5px 5px 5px;
			cursor: pointer;
			-moz-border-radius: 5px 5px 5px 5px;
			padding: 5px;
			margin-top: 5px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});

			$(".box").hide();
			$("#print").click(function () {
				$(".box").slideToggle("slow");
			});

			$("#PartialApprove").click(function () {
				$("#PartialApprove").hide();
				$("#loadingPartialApprove").show();
			});

			$("#ApproveOrder").click(function () {
				$("#ApproveOrder").hide();
				$("#loadingApproveOrder").show();
			});

			$("#AdminAllow").click(function () {
				$("#AdminAllow").hide();
				$("#loadingAdminAllow").show();
			});

			$("#DeleteOrder").click(function () {
				$("#DeleteOrder").hide();
				$("#loadingDeleteOrder").show();
			});

			$("#ReviseOrder").click(function () {
				$("#ReviseOrder").hide();
				$("#loadingReviseOrder").show();
			});
		});
	</script>
</asp:Content>
