﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/SelectSite.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderActivityTransLog>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		table
		{
			border-collapse: collapse;
			border-spacing: 0;
			border: solid 1px #c5c5c5;
			width:100%;
		}
		th
		{
			background-color: #e6e6e6;
			border: solid 1px #c5c5c5;
			font: bold 11px tahoma;
			padding: 5px;
			text-align: center;
		}
		td
		{
			border: solid 1px #c5c5c5;
			padding: 5px;
			color: #666666;
			font: bold 11px tahoma;
		}
		h3
		{
			font-size: 13px;
			font-weight: bold;
			margin: 8px 0;
			padding: 5px;
			text-shadow: 1px 1px 1px white;
			border: solid 1px #C5C5C5;
			border-radius: 2px;
			-moz-border-radius: 2px;
			/*background: url(../images/theme/bg-pattern-h3.jpg) repeat-x;*/
			height: 20px;
			color: #7C7C7C;
			background-color: #e6e6e6;
		}
	</style>
	<div>
		<div>
			<h3>
				Transaction of / การทำงานที่ผ่านมาของ เลขที่ใบสั่งซื้อ :
				<%=Model.OrderTransLogs.First().OrderID %></h3>
			<table>
				<tr>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.ActionUserID")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.Detail")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.ActionDate")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.BeforeStatus")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.AfterStatus")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.Remark")%>
					</th>
				</tr>
				<%foreach (var item in Model.OrderTransLogs)
	  {%>
				<tr>
					<td>
						<%=item.ActionUserID%>
					</td>
					<td>
						<%=item.ActionName%>
					</td>
					<td>
						<%=item.ActionDate.ToString(new DateTimeFormat())%>
					</td>
					<td>
						<%=item.DisplayBeforeStatus%>
					</td>
					<td>
						<%=item.DisplayAfterStatus%>
					</td>
					<td>
						<%=item.ActionRemark %>
					</td>
				</tr>
				<%} %>
			</table>
		</div><br />
		<div>
			<h3>
				Adjust Order of / การแก้ไขสินค้าของ เลขที่ใบสั่งซื้อ :
				<%=Model.OrderDetailTransLogs.First().OrderID%></h3>
			<table>
				<tr>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.ActionUserID")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.Detail")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.ProductID")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.ActionDate")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.OldQty")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.EditQty")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.NewQty")%>
					</th>
					<th>
						<%=Html._("ApproverOrder.OrderAdjustment.Remark")%>
					</th>
				</tr>
				<%foreach (var orderDetailTrans in Model.OrderDetailTransLogs)
	  {%>
				<tr>
					<td>
						<%=orderDetailTrans.EditBy%>
					</td>
					<td>
						<%=orderDetailTrans.ActionEditName%>
					</td>
					<td>
						<%=orderDetailTrans.PID%>
					</td>
					<td>
						<%=orderDetailTrans.EditOn.ToString(new DateTimeFormat())%>
					</td>
					<td style="text-align:center">
						<%=orderDetailTrans.OldQty%>
					</td>
					<td style="text-align:center">
						<%=orderDetailTrans.EditQty%>
					</td>
					<td style="text-align:center">
						<%=orderDetailTrans.NewQty%>
					</td>
					<td>
						<%=orderDetailTrans.EditRemark%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
	</div>
</asp:Content>
