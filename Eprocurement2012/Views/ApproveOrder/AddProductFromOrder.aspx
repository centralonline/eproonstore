﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.RePurchase>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div id="searchresult">
		<h1>
			<%=String.Format(Html._("ApproverOrder.AddProductFromOrder.Content"), Model.OrderId, Model.Products.Count())%>
		</h1>
		<div id="contentCol" style="padding-left: 10px;">
			<%Html.BeginForm("AddProductFromOrder", "ApproveOrder", FormMethod.Post, null); %>
			<%=Html.Hidden("guid", Model.OrderGuid) %>
			<div id="content" class="content">
				<table class="office-list-product" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<td style="width: 90px">
								<input id="checkall" type="checkbox" />
								<label for="checkall">
									<%=Html._("ProductCatalog.HeadCheckAll")%></label>
							</td>
							<td class="text-center fix50">
								<%=Html._("Repurchase.MyProductId")%>
							</td>
							<td class="text-center" style="width: 70%">
								<%=Html._("Repurchase.MyProductName")%>
							</td>
							<td class="text-center bgcolor-hilight">
								<%=Html._("Repurchase.MyProductPrice")%>
							</td>
							<td class="text-center fix50">
								<%=Html._("Repurchase.MyProductUnit")%>
							</td>
						</tr>
					</thead>
					<tbody>
						<%foreach (var item in Model.Products)
						{ %>
						<tr>
							<td>
								<input id="check_<%=item.Id%>" name="productId" type="checkbox" value="<%=item.Id%>" />
							</td>
							<td style="text-align: center">
								<span class="gray">
									<%=item.Id%></span>
							</td>
							<td>
								<div id="product-list" class="office-list-product-infomation-name">
									<div class="quickinfobt" style="width: 100%;">
										<%=Html.Hidden("pid", item.Id, new { @class = "productId" })%>
										<span style="margin-left: 100px; font: normal 13px tahoma;">
											<%=Html.Encode(item.Name)%></span>
									</div>
								</div>
							</td>
							<td class="text-right">
								<%=item.DisplayPrice%>
							</td>
							<td class="text-center">
								<%=item.Unit%>
							</td>
						</tr>
						<%} %>
					</tbody>
				</table>
				<table class="office-list-product" cellpadding="0" cellspacing="0">
					<tr>
						<td style="background-color: #d1d1d1;">
							<div style="float: left; background-color: #d1d1d1; width: auto">
								<input type="image" name="Cancel.x" value="Cancel" class="AddToCartOfficeSupply"
									id="Cancel" src="/images/btn/btn-cancel-gray-h.png" />
							</div>
						</td>
						<td style="background-color: #d1d1d1;">
							<div style="float: right; background-color: #d1d1d1; width: auto">
								<input type="image" name="AddToCatalog.x" value="Add To Catalog" class="AddToCartOfficeSupply"
									id="AddToCatalog" src="/images/btn/btn_add_to_wish_list.png" />
							</div>
						</td>
					</tr>
				</table>
			</div>
			<%Html.EndForm(); %>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CssContent" runat="server">
	<link href="/css/QuickInfo.css" rel="Stylesheet" type="text/css" />
	<style type="text/css">
		div#searchresult
		{
			margin-top: 13px;
			min-height: 1500px;
			min-width: 462px;
			margin-left: 220px;
			padding-right: 0px;
			position: relative;
		}
		div#searchresult-left-panel
		{
			left: 7px;
			position: absolute;
			top: -12px;
		}
		div#searchresult-right-panel
		{
			right: 7px;
			min-height: 600px;
		}
		div#searchresult-right-panel > ol
		{
			border: 0 none inherit;
			font-family: inherit;
			font-size: 100%;
			font-style: inherit;
			font-weight: inherit;
			list-style-type: none;
			margin: 0;
			outline-color: -moz-use-text-color;
			outline-style: none;
			outline-width: 0;
			padding: 0;
			vertical-align: baseline;
		}
		div#searchresult-right-panel > ol
		{
			display: inline-block;
			margin: 15px 5px;
			min-height: 200px;
			min-width: 140px;
			vertical-align: top;
			width: 150px;
		}
		div#searchresult-right-panel > div.product-infomation-description
		{
			display: none;
		}
	</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script src="/js/QuickInfo.js" type="text/javascript"></script>
	<script src="/js/easySlider1.5.js" type="text/javascript"></script>
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});

			$('#checkall').click(function () {
				var check = this.checked;
				$('.checkAllProduct').attr('checked', check);
			});

			$("#slider").easySlider({
				vertical: true,
				nextText: "",
				prevText: ""
			});
			if ($("#slider li").size() < 2) {
				$("#nextBtn").hide();
			}
			initQuickInfo($('.quickinfobt'), '<%=Url.Action("QuickInfo","Product")%>');
		});
	</script>
</asp:Content>
