﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/UserProfile/Profile.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Catalog>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="GroupData 100percent spaceleft20">
		<h3>
			<img alt="" src="/images/theme/h3-list-icon.png" />Edit My Catalog</h3>
		<%Html.BeginForm("ManageCatalog", "Catalog", FormMethod.Post); %>
		<div>
			<label>
				ชื่อ My Catalog :</label>
			<%=Html.TextBoxFor(m => m.CatalogName, new { style = "width:305px;" })%>
			<%=Html.Hidden("GUID", Model.GUID)%>
		</div>
		<div class="eclear">
		</div>
		<div>
			<label>
				รายละเอียด :</label>
			<%=Html.TextAreaFor(m => m.CatalogDesc, new { style = "width:300px; height:100px" })%>
		</div>
		<div>
			<%
				string CatalogDefault = "";
				if (Model.IsCheck == "checked")
				{
					CatalogDefault = "checked='checked'";
				}
				%>
				<input id="IsDefault" <%=CatalogDefault %> name ="IsDefault"  type="checkbox" style ="text-align:right" />ใช้เป็นค่าเริ่มต้น 
		</div>

		<div>
			<input type="submit" name="UpdateCatalog.x" value="Save" />
		</div>
		<%Html.EndForm(); %>

					<span style="color: Red;">
				<%=TempData["EditCompleted"]%></span> <span style="color: Red;">
					<%=TempData["ErrorCatalogNameEmpty"]%></span> <span style="color: Red;">
						<%=TempData["ErrorIsDefault"]%></span>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
