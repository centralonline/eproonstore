﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/UserProfile/Profile.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CatalogData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="GroupData 100percent spaceleft20">
		<h3>
			<img alt="" src="/images/theme/h3-list-icon.png" />My Catalog</h3>
		<div class="border">
			<table style="width: 100%; vertical-align: top;">
				<tr>
					<td style="width: 80%;">
						<%Html.BeginForm("ManageCatalog", "Catalog", FormMethod.Post); %>
						<div style="overflow: auto; margin-bottom: 5px; width: 100%;">
							<input type="submit" name="CreateCatalog.x" value="สร้าง Catalog ใหม่" />
							<input type="submit" name="EditCatalog.x" value="แก้ไข Catalog นี้" />
							<input type="submit" name="DeleteCatalog.x" value="ลบ Catalog นี้" />
							<%if (Model.Catalog != null)
		 {%>
							<%=Html.Hidden("GUID", Model.Catalog.GUID)%>
							<%=Html.Hidden("CatalogName", Model.Catalog.CatalogName)%>
							<%=Html.Hidden("CatalogDesc", Model.Catalog.CatalogDesc)%>
							<%=Html.Hidden("UserID", Model.Catalog.UserID)%>
							<%=Html.Hidden("IsDefault",Model.Catalog.IsDefault)%>
							<%} %>
						</div>
						<div class="eclear">
						</div>
						<div>
							<label style="width: 100px;">
								ชื่อ My Catalog :</label>
							<%if (Model.Catalog != null && Model.ListCatalog.Any())
		 {%>
							<label>
								<%=Model.Catalog.CatalogName%>
							</label>
							<%} %>
						</div>
						<div class="eclear">
						</div>
						<div>
							<label style="width: 80px;">
								รายละเอียด :</label>
							<%if (Model.Catalog != null && Model.ListCatalog.Any())
		 {%>
							<label>
								<%=Model.Catalog.CatalogDesc%>
							</label>
							<%} %>
						</div>
						<div class="eclear">
						</div>
						<div>
							<%Html.BeginForm("AddProductByPID", "Catalog", FormMethod.Post); %>
							<%=Html.Hidden("GUID", Model.Catalog.GUID)%>
							<h5>
								เพิ่มสินค้าใน My Catalog</h5>
							<label style="width: 100px;">
								กรอกรหัสสินค้า :</label>
							<%=Html.TextBoxFor(m => m.Catalog.AddByPID, new { maxlength = "7", id = "AddByPID" })%>
							<%--<%=Html.TextBox("addByPID","" ,new { maxlength = "7" })%>--%>
							<%--<input id="addByPID" name="addByPID" type="text" style="width: 150px" />--%>
							<input id="addPID" type="submit" name="AddProductID.x" value="เพิ่มสินค้า" />
							<span style="color: Red;">
								<%=TempData["ErrorPIDEmpty"]%></span>
							<%Html.EndForm(); %>
						</div>
						<%Html.EndForm(); %>
						<div class="eclear">
						</div>
						<span style="color: Red;">
							<%=TempData["SelectItemIsEmpty"]%></span>
						<%Html.BeginForm("HandleAction", "Catalog", FormMethod.Post); %>
						<%=Html.Hidden("GUID", Model.Catalog.GUID)%>
						<div>
							<%int count = 0; %>
							<table class="office-list-product" cellpadding="0" cellspacing="0">
								<thead>
									<tr>
										<td class="text-center">
											<input name="selectAll" id="itemCheckAll" type="checkbox" />
										</td>
										<td class="text-center" style="width: 50px;">
											ลำดับ
										</td>
										<td class="text-center">
											รหัสสินค้า
										</td>
										<td class="text-center">
											ข้อมูลสินค้า
										</td>
										<td class="text-center">
											ราคาต่อหน่วย
										</td>
										<td class="text-center">
											จำนวน
										</td>
										<td class="text-center" style="width: 50px;">
											หน่วย
										</td>
									</tr>
								</thead>
								<tbody>
									<%if (Model.Catalog != null)
		   {%>
									<%foreach (var product in Model.Catalog.CatalogDetail)
		   {%>
									<tr>
										<td class="text-center">
											<input name="checkItem" type="checkbox" class="itemCheck" value="<%=Html.Encode(product.Id)%>" />
										</td>
										<td class="text-center">
											<%=++count%>
										</td>
										<td>
											<%=Html.Encode(product.Id)%>
										</td>
										<td>
											<%=Html.ActionLink(product.Name, "Details", "Product", new { id = product.Id }, new { target = "_blank" })%>
											<p class="status color-stock">
												<%=product.DisplayStatusText%>
											</p>
											<%=product.IsPromotion ? "<br /><span style='color:Red'>สินค้าโปรโมชั่น</span>" : String.Empty%>
											<%=product.IsContactForDelivery ? "<br /><span style='color:Red'>* สินค้ามีค่าจัดส่ง กรุณาติดต่อเจ้าหน้าที่ ที่เบอร์. 027395555</span>" : String.Empty%>
										</td>
										<td class="text-right">
											<%=Html.Encode(product.DisplayPrice)%>
										</td>
										<%if (Model.User.IsRequester && Model.Catalog.CatalogDetail.Any(p => p.IsDisplayBuyButton))
			{%>
										<td class="text-center">
											<%=Html.Hidden("productId", product.Id)%>
											<%--<%=product.IsDisplayBuyButton ? "<input type='text' style='width:36px;' name='quantity' />" : "<input type='hidden' name='quantity' value='' />"%>
											--%>
											<%=product.IsDisplayBuyButton ? "<input type='text' style='width:30px;' maxlength='4' name='quantity' />" : product.IsContactForDelivery ? "<input type='hidden' name='quantity' value='' />" : "<span style='color:Red;'>สินค้าขาด</span><input type='hidden' name='quantity' value='' />"%>
										</td>
										<%} %>
										<%else
			{%>
										<td>
											<input type='hidden' name='quantity' value='' />
										</td>
										<%} %>
										<td class="text-center">
											<%=Html.Encode(product.Unit)%>
										</td>
									</tr>
									<%} %>
									<%} %>
								</tbody>
							</table>
						</div>
						<div style="margin-top: 10px; float: left;">
							<input type="submit" name="DeleteItemCatalog.x" value="ลบสินค้า" />
						</div>
						<div style="margin-top: 10px; float: right;">
							<input type="submit" name="AddToCartOfficeSupply.x" value="สั่งซื้อสินค้า" style="text-align: right;" />
						</div>
						<%Html.EndForm(); %>
					</td>
					<td style="width: 20%; vertical-align: top;">
						<div>
							<h4 style="text-align: center;">
								เลือกดู Catalog</h4>
							<p style="color: #4570b7; font-weight: bold; text-align: center;">
								ที่คุณสร้างไว้</p>
							<br />
							<%foreach (var item in Model.ListCatalog.Where(o => o.Status == Eprocurement2012.Models.Catalog.CatalogStatus.Active))
		 {%>
							<ul>
								<li style="border-bottom: solid 1px #e0e0e0; margin-bottom: 3px; margin-left: 10px;">
									<img src="/images/icon_my_catalog.gif" />
									<%=Html.ActionLink(item.CatalogName, "ManageCatalog", "Catalog", new { guid = item.GUID },null)%></li>
							</ul>
							<%} %>
						</div>
					</td>
				</tr>
			</table>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		td.text-center
		{
			text-align: center;
		}
		td.text-right
		{
			text-align: right;
		}
		.office-list-product
		{
			width: 100%;
			font: normal 13px tahoma;
		}
		.position
		{
			float: left;
			text-align: right;
			width: 130px;
			margin-right: 15px;
		}
		.textSpanGray
		{
			font-size: 12px;
			margin-left: 30px;
			color: Red6;
			line-height: 25px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#itemCheckAll").click(function () {
				if ($("#itemCheckAll").attr('checked')) {
					$(".itemCheck").attr('checked', true)
				}
				else {
					$(".itemCheck").attr('checked', false)
				}
			});


			//สำหรับคีย์ รหัสสินค้า แล้วกด Enter
			$("#AddByPID").keypress(function (event) {
				if (event.keyCode == 13) {
					event.preventDefault();//ล้างการทำงานก่อนหน้านั้น เพื่อให้ทำงานตามที่เราต้องการ
					$("#addPID").click();
				}
			});
		});
	</script>
</asp:Content>
