﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/UserProfile/Profile.Master"
	Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<div style="text-align: center; margin-top: 20px;">
			<h1 style="font-weight: normal; padding: .5em; margin: 0 0 10px 0; border-bottom: 1px solid #ccc;
				color: #6283B8">
				Delete MyCatalog Success</h1>
			<h2>
				<br />
				<%=Html.ActionLink("คลิกที่นี่", "ManageCatalog", "Catalog")%>
				เพื่อกลับสู่หน้าแสดงรายการ My Catalog</h2>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
