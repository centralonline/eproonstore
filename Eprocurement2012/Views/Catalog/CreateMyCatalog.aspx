﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/UserProfile/Profile.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Catalog>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

	<div class="GroupData 100percent spaceleft20">
		<h3>
			<img alt="" src="/images/theme/h3-list-icon.png" />Create Catalog</h3>
			<%Html.BeginForm("ManageCatalog", "Catalog", FormMethod.Post); %>
			<div>
				<label>ชื่อ My Catalog :</label>
				<%=Html.TextBoxFor(m => m.CatalogName, new { style = "width:305px;", maxlength="200"})%>
			</div>
			<div class="eclear">
			</div>
			<div>
				<label>รายละเอียด :</label>
				<%=Html.TextAreaFor(m => m.CatalogDesc, new { style="width:300px; height:100px"})%>
			</div>
			<div>
				<label for="IsDefault">
				</label>
				<input id="IsDefault" name ="IsDefault" type="checkbox" style ="text-align:right" />ใช้เป็นค่าเริ่มต้น
			</div>
			<div>
				<input type="submit" name="SaveCatalog.x" value="สร้าง Catalog" />
				<span style="color:Red;"><%=TempData["ErrorCatalogNameEmpty"]%></span>
			</div>
			<%Html.EndForm(); %>
	</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
