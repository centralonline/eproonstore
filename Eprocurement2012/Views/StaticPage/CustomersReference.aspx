﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<!--	<div style="width: 100%;">
		<img alt="" width="950" height="425" src="/images/theme/Slide-CustomersReference.jpg" class=" wp-post-image" />
	</div> -->
	<img alt="" src="/images/index/slideShadow.jpg" />
<div id="epro-content" class="100percent">
<div id="spaceEdge">
	<div style="width: 100%; min-height:600px;margin-left:15px; ">
		<img alt="" src="/images/theme/title-CustomersReference.jpg"  />
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;width: 98%; height: 19px; ">
		</div>
		 &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; <p class="epro-p">หลากหลายองค์กรชั้นนำของประเทศไทย ที่ไว้วางใจเลือกใช้บริการ OfficeMate e-Procurement           </p>
			<div id="customer-logo">
				<!--แถว 1-->
				<div class="one_fifth"><img alt="" src="/images/theme/LandAndHouse-Logo.png" /><p>บริษัท แลนด์ แอนด์ เฮ้าส์ จำกัด(มหาชน)</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/AP-Logo.png" style="width:150px;height:68px;"/><p>บริษัท เอพี (ไทยแลนด์) จำกัด (มหาชน)</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/LPN-Logo.png" style="width:150px;height:68px;" /><p>บริษัท แอล.พี.เอ็น.ดีเวลลอปเมนท์ จำกัด (มหาชน)</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/PrinSIRI-Logo.png" style="width:150px;height:68px;"/><p>บริษัท ปริญสิริ จำกัด (มหาชน)</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/WitSawaPat-Logo.png" /><p>บริษัท วิศวภัทร์ จำกัด</p></div>
				<div class="eclear"></div>

				<!--แถว 2-->

				<div class="one_fifth"><img alt="" src="/images/theme/BMW-Logo.png" /><p>บริษัท มิลเลนเนียม ออโต้ จำกัด</p></div>			
				<div class="one_fifth"><img alt="" src="/images/theme/Volvo-Logo.png" /><p>บริษัท วอลโว่ กรุ๊ป (ประเทศไทย) จำกัด</p></div>
				<div class="one_fifth if_last"><img alt="" src="/images/theme/Honda-Logo.png" /><p>บริษัท ฮอนด้า อาร์แอนด์ดี เอเชีย แปซิฟิค จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/Mitsu-Logo.png" style="width:150px;height:68px;" /><p>มิตซูบิชิ อีเล็คทริค กันยงวัฒนา</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/Isuzu-Logo.png" style="width:150px;height:68px;"/><p>บริษัท อีซูซุอันดามันเซลส์ จำกัด</p></div>
				<div class="eclear"></div>

				<!--แถว 3-->
				<div class="one_fifth"><img alt="" src="/images/theme/Hino-Logo.png" /><p>บริษัท ฮีโน่มอเตอร์ส แมนูแฟคเจอริ่ง (ประเทศไทย) จํากัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/Mitcahrin-Logo.png" /><p>บริษัท สยามมิชลิน จำกัด </p></div>
				<div class="one_fifth if_last"><img alt="" src="/images/theme/Bristone-Logo.png" /><p>บริษัท บริดจสโตน สเปเชียลตี้ ไทร์ แมนูแฟคเจอริ่ง (ประเทศไทย) จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/Yuasa-Logo.png" style="width:150px;height:68px;" /><p>บริษัท ยัวซ่าแบตเตอรี่ ประเทศไทย จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/NHK-Logo.png" style="width:150px;height:68px;"/><p>เอ็น เอช เค สปริง (ประเทศไทย)</p></div>
				<div class="eclear"></div>

				<!--แถว 4-->

				<div class="one_fifth"><img alt="" src="/images/theme/Nitto-Logo.png" /><p>นิตโต้ มาเทค (ประเทศไทย)</p></div>			
				<div class="one_fifth"><img alt="" src="/images/theme/Nikon-Logo.png" /><p>บริษัท นิคอน เซลส์ (ประเทศไทย) จำกัด</p></div>
				<div class="one_fifth if_last"><img alt="" src="/images/theme/Huawei-Logo.png" /><p>บริษัท หัวเว่ย เทคโนโลยี่ (ประเทศไทย) จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/schenker-Logo.png" style="width:150px;height:68px;"/><p>บริษัท เช้งเก้อร์ (ไทย) จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/TAPPline-Logo.png" style="width:150px;height:68px;"/><p>บริษัท ท่อส่งปิโตรเลียมไทย จำกัด</p></div>
				<div class="eclear"></div>
				
				<!--แถว 5-->

				<div class="one_fifth"><img alt="" src="/images/theme/Mitphol-Logo.png" style="width:150px;height:68px;"/><p>บริษัท น้ำตาลมิตรผล จำกัด</p></div>
				<div class="one_fifth if_last"><img alt="" src="/images/theme/Novaltis-Logo.png" /><p>บริษัท โนวาร์ตีส (ประเทศไทย) จำกัด </p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/Hafele-Logo.png" style="width:150px;height:68px;"/><p>บริษัท เฮเฟเล่ (ประเทศไทย) จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/KPMG-Logo.png" style="width:150px;height:68px;"/><p>บริษัท เคพีเอ็มจี ภูมิไชย สอบบัญชี จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/Mueangthai-Logo.png" style="width:150px;height:68px;"/><p>บริษัท เมืองไทยประกันชีวิต จำกัด (มหาชน)</p></div>
				<div class="eclear"></div>
								
				<!--แถว 6-->

				<div class="one_fifth"><img alt="" src="/images/theme/thaivivat-Logo.png" /><p>บริษัท ประกันภัยไทยวิวัฒน์ จำกัด (มหาชน)</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/CN-Logo.png" style="width:150px;height:68px;"/><p>บริษัท อเมซอน ฟอลส์ จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/agoda-Logo.png" style="width:150px;height:68px;"/><p>อโกดา เซอร์วิสเซส</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/asiabook-Logo.png" style="width:150px;height:68px;"/><p>บริษัท เอเซียบุ๊คส จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/unoqlu-Logo.png" style="width:150px;height:68px;"/><p>บริษัท ยูนิโคล่ (ประเทศไทย) จำกัด</p></div>
				<div class="eclear"></div>

				<!--แถว 7-->
				<div class="one_fifth"><img alt="" src="/images/theme/Louis-Logo.png" style="width:150px;height:68px;"/><p>บริษัท หลุยส์ วิตตอง (ไทยแลนด์) จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/Olimpus-Logo.png" style="width:150px;height:68px;"/><p>บริษัท โอลิมปัส (ประเทศไทย) จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/fujixerox-Logo.png" style="width:150px;height:68px;"/><p>บริษัท ฟูจิ ซีร็อกซ์ (ประเทศไทย) จํากัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/richo-Logo.png" style="width:150px;height:68px;"/><p>ริโก้ (ประเทศไทย)</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/bumrungrad-Logo.png" style="width:150px;height:68px;"/><p>โรงพยาบาลบำรุงราษฎร์</p></div>
				<div class="eclear"></div>

				<!--แถว 8-->
				<div class="one_fifth"><img alt="" src="/images/theme/eakchon-Logo.png" style="width:150px;height:68px;"/><p>บริษัท โรงพยาบาลเอกชล จำกัด (มหาชน)</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/slimming-Logo.png" style="width:150px;height:68px;" /><p>บริษัท สลิมมิ่งพลัส จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/wuttisak-Logo.png" style="width:150px;height:68px;" /><p>บริษัท วุฒิศักดิ์ คลินิก อินเตอร์ กรุ๊ป จำกัด</p></div>
                <div class="one_fifth"><img alt="" src="/images/theme/Qatar-Logo.png" style="width:150px;height:68px;"/><p>สายการบินกาตาร์ แอร์เวยส์</p></div>
                <div class="one_fifth"><img alt="" src="/images/theme/mahidol-Logo.png" style="width:150px;height:68px;"/><p>หน่วยวิจัยโรคเขตร้อนมหิดล-อ๊อกซ์ฟอร์ด คณะเวชศาสตร์เขตร้อน มหาวิทยาลัยมหิดล</p></div>
				<div class="eclear"></div>

                <!--แถว 9-->
		
				<!-- <div class="one_fifth"><img alt="" src="/images/theme/slimming-Logo.png" style="width:150px;height:68px;" /><p>บริษัท สลิมมิ่งพลัส จำกัด</p></div>
				<div class="one_fifth"><img alt="" src="/images/theme/wuttisak-Logo.jpg" style="width:150px;height:68px;" /><p>บริษัท วุฒิศักดิ์ คลินิก อินเตอร์ กรุ๊ป จำกัด</p></div>
                <div class="one_fifth"><img alt="" src="/images/theme/Qatar-Logo.png" style="width:150px;height:68px;"/><p>สายการบินกาตาร์ แอร์เวยส์</p></div>  -->
				<div class="eclear"></div>



			</div>
				<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;width: 100%; height: 19px;">
		</div>
		
	</div>
</div>
</div><!--id="epro-content"-->
</asp:Content>
