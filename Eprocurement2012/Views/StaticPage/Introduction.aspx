﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master"
	Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="nivo_slider_plugin">
		<div id="nivo" class="nivoSlider">
			 

		 <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-NewYearBless.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-NewYearBless"
						target="_blank">
						<img width="950" height="400" src="/images/theme/Main-NewYear-Blessing.jpg"class="wp-post-image" alt="" /></a> 
                            
                                    <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-SOHO.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-LD-Jan16-SOHO"
										target="_blank">
										<img width="950" height="400" src="/images/theme/MainBanner-Jan16-SOHO.jpg"
											class="wp-post-image" alt="" /></a> 

                                            <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan-16-BrandOne.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan-16-BrandOne" target="_blank">
												<img width="950" height="400" src="/images/theme/MainBanner-Jan-16-Brand-One.jpg" class="wp-post-image"
													alt="" /></a>

                                                     <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-PremuimChair.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-PremuimChair" target="_blank">
												<img width="950" height="400" src="/images/theme/MainBanner-Jan-16-Promotion-PremuimChair02.jpg" class="wp-post-image"
													alt="" /></a>

                                                     <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-Printingsolution.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-Printingsolution" target="_blank">
												<img width="950" height="400" src="/images/theme/Main-Card1Baht.jpg" class="wp-post-image"
													alt="" /></a>
  
		</div>
	</div>
	<img alt="" src="/images/index/slideShadow.jpg" />
	<div id="epro-content" class="100percent">
		<div id="spaceEdge">
			<div style="width: 100%;">
				<img alt="" src="/images/theme/title_introduction.jpg" />
				<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
					width: 100%; height: 19px;">
				</div>
				<p class="epro-p">
					ออฟฟิศเมท ผู้นำด้านการจัดจำหน่ายอุปกรณ์สำนักงาน คอมพิวเตอร์ และเฟอร์นิเจอร์ออฟฟิศ
					ที่คัดสรรสินค้าคุณภาพกว่า 8,000 รายการ จัดจำหน่ายด้วยระบบแค็ตตาล็อก โดยมีพนักงาน
					Call Center บริการรับการสั่งซื้อ และระบบออนไลน์ ทั้งในรูปแบบเว็บไซต์ www.officemate.co.th
					( B2C : Business to Customer ) และรูปแบบ e-Procurement ( B2B : Business to Business
					) พร้อมกันนั้น ยังมีคลังสินค้าที่ทันสมัย สามารถจัดเก็บสินค้าได้ครบทุกรายการ เพื่อพร้อมบริการจัดส่งฟรี
					รวดเร็วภายใน 24 ชั่วโมง</p>
				<p class="pTitle">
					OfficeMate e-Procurement
				</p>
				<p class="epro-p">
					จากการขายผ่านระบบแค็ตตาล็อก ขยายรูปแบบมาสู่การขายผ่านระบบออนไลน์ทั้งระบบ B2C จนมาถึงระบบ
					B2B ตั้งแต่ปี พ.ศ. 2543 ออฟฟิศเมทได้พัฒนาระบบ e-Procurement ให้ตรงกับความต้องการของลูกค้าเฉพาะแต่ละองค์กร
					จึงสามารถรองรับความต้องการที่แตกต่างกัน จากทั้งองค์กรขนาดใหญ่และขนาดเล็ก หรือ องค์กรที่มีสาขากระจายที่ตั้งอยู่ทั่วประเทศ
					โดยไม่ต้องปรับการทำงานหลักภายในองค์กร จึงทำให้ระยะเวลาที่ผ่านมา ออฟฟิศเมท ได้รับความไว้วางใจ
					ในการสร้างระบบจัดซื้อที่มีประสิทธิภาพให้กับองค์กรชั้นนำทั่วประเทศมาแล้วอย่างมากมาย</p>
				<p class="pTitle">
					ขั้นตอนของการสั่งซื้อผ่านระบบ OfficeMate e-Procurement</p>
				<a href="/images/theme/process-intro.jpg">
					<img src="/images/theme/process-intro.jpg" class="image-center image-frame" /></a>
				<br class="eclear" />
				<p class="pTitle">
					ความเหนือกว่าของระบบ OfficeMate e-Procurement
					<ul class="epro-p">
						<li>ออกแบบขี้น เพื่อให้เหมาะสมกับความต้องการของแต่ละองค์กรโดยเฉพาะ</li>
						<li>เป็นระบบที่ช่วยลดเวลา และขั้นตอนในการดำเนินการตั้งแต่ขั้นตอนการสั่งซื้อไปจนถึงขั้นตอนการอนุมัติ
						</li>
						<li>ช่วยลดค่าใช้จ่ายอื่นๆ ในกระบวนการจัดซื้อ เช่น ต้นทุนการผลิตแฝงที่เกิดจากการจัดซื้อนอกระบบ</li>
						<li>สามารถติดตาม ตรวจสอบ ควบคุมและอนุมัติการสั่งซื้อด้วยระบบ Paperless ได้ทุกที่ ทุกเวลาจากระบบออนไลน์</li>
						<li>สามารถติดตามและตรวจสอบขั้นตอนการสั่งซื้อได้อย่างละเอียดด้วยระบบ Report ที่สมบูรณ์แบบที่สุด
							OfficeMate 's e-Procurement จึงพัฒนาขึ้นมาเพื่อตอบสนองความต้องการอีกขั้นหนึ่งของการจัดซื้อได้อย่างมีประสิทธิภาพ
							และเหมาะสมที่สุดสำหรับทุกองค์กร </li>
					</ul>
				</p>
				<p class="pTitle">
					ลดต้นทุนองค์กร
					<ul class="epro-p">
						<li>โปรแกรมฟรีสำหรับลูกค้าออฟฟิศเมท เท่านั้น </li>
						<li>ประหยัดเวลาในการสั่งซื้อ โดย User ไม่ต้องเสียเวลาในการโทรเช็คราคา และการสอบถามรายละเอียดสินค้า
							เนื่องจากใน web มีรูปภาพ และรายละเอียดที่ชัดเจน ที่ทำให้สามารถตัดสินใจซื้อได้ทันที
						</li>
						<li>ลดค่าใช้จ่ายที่เป็นต้นทุนแฝงในการสั่งซื้อ เช่น ค่าโทรศัพท์ ค่าFax ค่ากระดาษ ค่าหมึกพิมพ์
							เป็นต้น</li>
						<li>ลดต้นทุนในการจัดเก็บสินค้าโดยที่ไม่จำเป็นที่ต้องมาสต็อกสินค้าเอง ซึ่งทำให้ไม่เปลืองพื้นที่ในการจัดเก็บ
							รวมถึงบุคลากรที่ต้องเข้ามาดูแลในส่วนนี้ และไม่ต้องนำเงินมาจม โดยทางออฟฟิศเมทจะทำการสต็อคสินค้าแทนท่าน
							ซึ่งสั่งวันนี้ พรุ่งนี้ได้ของ </li>
						<li>ลดข้อผิดพลาดในการปฎิบัติงาน เนื่องจากเป็นการจัดซื้อ แบบอิเลคทรอนิคส์ ทำให้เกิดข้อผิดพลาดน้อยกว่าการโทรสั่ง
							หรือระบบอื่นๆ</li>
					</ul>
				</p>
				<p class="pTitle">
					เพิ่มประสิทธิภาพการดำเนินการของระบบจัดซื้อ
					<ul class="epro-p">
						<li>ผู้ใช้งานสามารถดูรายงานได้ทุกขั้นตอน โดยสามารถสโคปดูได้ตามความต้องการ ไม่ว่าจะเป็น
							การสั่งซื้อย้อนหลัง, รายงานสั่งซื้อของแต่ละหน่วยงาน หรืแต่ละแผนก, รายงานสินค้าที่สั่งซื้อบ่อย
							เป็นต้น </li>
						<li>ควบคุม และติดตามได้ตลอดเวลา ว่าแต่ละ Order อยู่ในขั้นตอนไหนแล้ว เช่น อนุมัติแล้ว,
							ส่งของแล้ว เป็นต้น</li>
						<li>ช่วยควบคุมงบประมาณการสั่งซื้อได้ เป็นรายเดือน, ไตรมาส หรือรายปีก็ได้ ซึ่งทำให้ง่ายต่อการวัด
							และประเมินผล</li>
						<li>เพิ่มความสะดวก รวดเร็ว และแม่นยำในการติดต่อสื่อสาร เนื่องจากใช้ระบบ Internet และ
							e-Mailในการส่งข้อมูลถึงกัน โดยไม่ว่าจะอยู่ที่ไหนก็สามารถเข้าไปสั่งซื้อได้ </li>
					</ul>
				</p>
				<p class="pTitle">
					สร้างความโปร่งใสให้กับองค์กร
					<ul class="epro-p">
						<li>เป็นราคามาตรฐาน ซึ่งพิสูจน์แล้วจาก 70,000 องค์กร </li>
						<li>ตรวจสอบรายละเอียดการสั่งซื้อได้ทุกขั้นตอน เพราะทางผู้บริหารสามารถเรียกดูรายงานการสั่งซื้อได้ตลอดเวลา</li>
						<li>บริษัท ร่วมมือกับ ก.พ.ร. และหน่วยงานภาครัฐ ในเรื่องความโปร่งใสในงานจัดซื้อจัดจ้าง</li>
					</ul>
				</p>
			</div>
		</div>
	</div>
	<!--id="epro-content"-->
</asp:Content>
