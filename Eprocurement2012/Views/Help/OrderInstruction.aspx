﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Help/Help.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		td
		{
			border: solid 1px #c5c5c5;
			padding: 5px;
			color: #666666;
			font: 11px tahoma;
			height: 20px;
		}
		
		th
		{
			background-color: #e6e6e6;
			font: bold 11px tahoma;
			padding: 5px;
			text-align: center;
			border: solid 1px #c5c5c5;
			height: 20px;
		}

	</style>
	<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left: 10px;">
			Status Order : สถานะใบสั่งซื้อ</p>
		<div class="epro-p" style="padding-left: 10px;">
			<table style="border-collapse: collapse; width:100%">
				<tr>
					<th>
						สถานะ (TH)
					</th>
					<th>
						สถานะ (EN)
					</th>
					<th>
						รายละเอียด
					</th>
				</tr>
				<tr>
					<td>
						รอพิจารณาอนุมัติ
					</td>
					<td>
						Waiting for Approve
					</td>
					<td>
						รออนุมัติจากผู้อนุมัติ
					</td>
				</tr>
				<tr>
					<td>
						ผู้ดูแลระบบอนุมัติเสร็จสิ้น
					</td>
					<td>
						Admin Allow
					</td>
					<td>
						ผู้ดูแลระบบอนุมัติใบขอซื้อ
					</td>
				</tr>
				<tr>
					<td>
						อนุมัติเสร็จสิ้น
					</td>
					<td>
						Approved
					</td>
					<td>
						ผู้อนุมัติทำการอนุมัติใบขอซื้อ
					</td>
				</tr>
				<tr>
					<td>
						อนุมัติเบื้องต้น
					</td>
					<td>
						Partial Approve
					</td>
					<td>
						อนุมัติใบขอซื้อสินค้าเบื้องต้น เนื่องจากผู้อนุมัติวงเงิน ไม่เพียงพอ และส่งใบขอซื้อนี้ไปยังผู้อนุมัติลำดับถัดไป
					</td>
				</tr>
				<tr>
					<td>
						ส่งกลับแก้ไข
					</td>
					<td>
						Revise
					</td>
					<td>
						ผู้อนุมัติขอใบสั่งซื้อกลับไปยังผู้สั่งซื้อ เพื่อทำการแก้ไข
					</td>
				</tr>
				<tr>
					<td>
						รออนุมัติกรณีพิเศษ
					</td>
					<td>
						Waiting for Admin Allow
					</td>
					<td>
						รออนุมัติใบขอซื้อสินค้าจากผู้ดูแลระบบเป็นกรณีพิเศษ
					</td>
				</tr>
				<tr>
					<td>
						จัดส่งสินค้า
					</td>
					<td>
						Shipped
					</td>
					<td>
						ใบขอซื้ออยู่ระหว่างการจัดส่งสินค้า
					</td>
				</tr>
				<tr>
					<td>
						หมดอายุอนุมัติ
					</td>
					<td>
						Expired
					</td>
					<td>
						ใบขอซื้อหมดอายุการอนุมัติ
					</td>
				</tr>
				<tr>
					<td>
						จัดส่งสินค้าเสร็จสิ้น
					</td>
					<td>
						Completed
					</td>
					<td>
						ส่งสินค้าถึงมือลูกค้าเรียบร้อยแล้ว
					</td>
				</tr>
			</table>
		</div>
	</div>
</asp:Content>
