﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Help/Help.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<p class="pTitle" style="padding-left: 10px;">
			Partial Approve | อนุมัติ เบื้องต้น
		</p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<!--Content-->
		<div class="epro-p" style="padding-left: 10px;">
			<p>
				ผู้อนุมติ อนุมัติใบขอซื้อสินค้าเบื้องต้น เนื่องจากวงเงินอนุมัติไม่เพียงพอ
			</p>
			<p>
				และส่งใบขอซื้อนี้ไปยังผู้มีอำนาจในการอนุมัติขั้นสูงกว่า (เนื่องจากเกินวงเงินที่กำหนดไว้ของผู้มีอำนาจในการอนุมัติขั้นแรก)</p>
		</div>
		<!--Content-->
	</div>
</asp:Content>
