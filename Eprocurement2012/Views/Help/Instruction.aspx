﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<%--	<h2>
		Instruction</h2>--%>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<title>Officemate e-Procument</title>
	<link rel="stylesheet" href="/css/StaticPage/eproNewDesign.css" type="text/css" />
	<link rel="stylesheet" href="/css/StaticPage/nivo-slider.css" type="text/css" />
	<link rel="stylesheet" href="/css/StaticPage/tab.css" type="text/css" />
	<link href="/css/ui-lightness/jquery-ui-1.8.23.custom.css" rel="stylesheet" type="text/css" />
	<!-- Nivo Slider -->
	<script src="/js/js-slide/jquery.min.js" type="text/javascript"></script>
	<script src="/js/js-slide/custom.js" type="text/javascript"></script>
	<script src="/js/js-slide/nivo.js" type="text/javascript"></script>
	<script src="/js/js-slide/jquery.nivo.slider.js" type="text/javascript"></script>
	<script src="/js/jquery-ui-1.8.23.custom.min.js" type="text/javascript"></script>
	<script type="text/javascript">
		$ = jQuery;
		$(document).ready(function () {
			$("#container").tabs();
		}); 
	</script>
	</head>
	<body id="epro-body">
		<div id="epro-allwraper">
			<%--			<div class="epro-topmenu">
				<div class="epro-menuposition">
					<div class="epro-menu-bar">
						<ul>
							<li class="menu-small"><a href="/Account">Home</a></li>
							<li class="menu-small"><a href="/StaticPage/Introduction">Introduction</a></li>
							<li><a href="/">Customers Reference</a></li>
							<li><a href="/">Privilege</a></li>
							<li class="menu-small"><a href="/Account/ApplyNow">Apply Now</a></li>
							<li><a href="/StaticPage/FAQ">FAQ</a></li>
						</ul>
					</div>
				</div>
			</div>--%>
			<%--<img class="epro-header-bar" alt="" src="/images/index/edge-top.jpg" />--%>
			<!--Content -->
			<link href="../../css/faq.css" rel="stylesheet" type="text/css" />
			<div class="margin">
				<h1>
					คำถามที่พบบ่อย</h1>
				<ol>
					<li><a href="#toc-question">หากมีข้อสงสัยเกี่ยวกับการใช้งาน OfficeMate e-Procurement
						จะติดต่อทางใดได้บ้าง</a> </li>
					<li><a href="#toc-step">ขั้นตอนการทำใบสั่งซื้อเป็นอย่างไร</a> </li>
					<li><a href="#toc-approve">ผู้อนุมัติสามารถทำการอนุมัติได้จากช่องทางไหนบ้าง</a>
					</li>
					<li><a href="#toc-purchase">จะทราบได้ยังไงว่าใบขอซื้อมีการอนุมัติแล้ว</a> </li>
					<li><a href="#toc-require">ต้องการสินค้าที่ไม่มีอยู่ในระบบ e-Procurement จะต้องทำอย่างไร</a>
					</li>
					<li><a href="#toc-product">มีการอนุมัติใบสั่งซื้อไปแล้วทำไมยังไม่ได้รับสินค้า</a>
					</li>
					<li><a href="#toc-record">ต้องการดูประวัติการสั่งซื้อย้อนหลังเพื่อวิเคราะห์การสั่งซื้อ
						ต้องทำอย่างไร</a> </li>
					<li><a href="#toc-catalog">My Catalog คืออะไร แตกต่างจาก Standard Catalog อย่างไร</a>
					</li>
					<li><a href="#toc-password">หากลืม Password ในการ Log in จะต้องทำอย่างไร</a> </li>
				</ol>
				<h2 id="toc-question">
					หากมีข้อสงสัยเกี่ยวกับการใช้งาน OfficeMate e-Procurement จะติดต่อทางใดได้บ้าง</h2>
				<p>
					หากต้องการสอบถามข้อมูลสินค้า หรือการใช้งานระบบ ท่านสามารถติดต่อเจ้าหน้าที่ออฟฟิศเมท
					โดยเลือกในเมนูส่วนของ 'contact us' เพื่อสอบถามข้อมูล หรือข้อสงสัยของท่านจากช่องทางนี้ได้
				</p>
				<h2 id="toc-step">
					ขั้นตอนการทำใบสั่งซื้อเป็นอย่างไร</h2>
				<p>
					ผู้ใช้งานเข้าสู่ระบบ e-Procurement แล้วก็ทำการเลือกสินค้าที่ต้องการโดยเลือกจากหมวดสินค้า
					ประเภทต่าง ๆ ด้านซ้ายมือ ใส่จำนวนที่ต้องการในช่องจำนวนของสินค้าที่เลือก เมื่อทำการเลือกสินค้าเรียบร้อยแล้วให้คลิ๊กที่
					Add to Cart และ คลิ๊กที่ปุ่ม Check out เพื่อทำการเลือก Cost center ที่ต้องการสั่งซื้อสินค้าให้
					จากนั้นคลิ๊กที่ปุ่ม 'สร้างใบขอซื้อ' ระบบจะแสดงข้อความยืนยันการทำใบขอซื้อ แล้วรอผลพิจารณาจากผู้อนุมัติ
					สามารถศึกษาวิธีการใช้งานเพิ่มเติมได้อย่างละเอียดจากที่เมนู ‘Instruction’ หรือดาวน์โหลดไฟล์คู่มือได้ที่เมนู
					‘Download Manual’
				</p>
				<h2 id="toc-approve">
					ผู้อนุมัติสามารถทำการอนุมัติได้จากช่องทางไหนบ้าง</h2>
				<p>
					ช่องทางแรก : จาก e-Mail แจ้งการขอซื้อที่ระบบจัดส่งให้ โดยคลิ๊ก Link ที่คำว่า "คลิ๊กที่นี่เพื่อพิจารณาใบขอซื้อ"
					ระบบจำทำการเปิดใบขอซื้อนั้น เพื่อให้ท่านพิจารณาอนุมัติทันที
				</p>
				<p>
					ช่องทางที่สอง : ผ่านหน้าเว็บไซต์ e-Procurement ของท่าน โดยใช้ User name คือ e-Mail
					Address ของผู้อนุมัติเอง และใช้ Password เดียวกับที่ใช้อนุมัติผ่าน e-Mail การอนุมัติ
					จะต้องกรอก Password เพื่อยืนยันการพิจารณาใบขอซื้อ หลังจากนั้นระบบจะขึ้นข้อความยืนยันให้ทราบ
					และจะมี e-Mail แจ้งกลับไปให้ผู้ขอซื้อทราบด้วยว่าได้รับการอนุมัติใบขอซื้อนั้นแล้ว
				</p>
				<h2 id="toc-purchase">
					จะทราบได้ยังไงว่าใบขอซื้อมีการอนุมัติแล้ว</h2>
				<p>
					หลังจากที่ผู้อนุมัติทำการอนุมัติใบสั่งซื้อแล้ว ระบบจะส่ง e-Mail แจ้งให้ผู้ที่ทำใบขอซื้อทราบ
					หรือผู้ขอซื้อเองก็สามารถตรวจสอบได้ โดยเข้าเมนู 'Report' แล้วคลิ๊กเมนูย่อย 'Report'
					หากสถานะใบสั่งซื้อเป็นสถานะ 'APP' (Approved) นั้นหมายถึงใบขอซื้อนั้นมีการอนุมัติเรียบร้อยแล้ว
				</p>
				<h2 id="toc-require">
					ต้องการสินค้าที่ไม่มีอยู่ในระบบ e-Procurement จะต้องทำอย่างไร</h2>
				<p>
					เนื่องจากสินค้าที่มีอยู่ใน Standard Catalog ของท่านเป็นสินค้าที่ถูกเลือกสรรโดยฝ่ายจัดซื้อกลางขององค์กรท่าน
					หากท่านต้องการสั่งซื้อสินค้า ที่ไม่มีอยู่ใน Standard Catalog ท่านสามารถติดต่อเจ้าหน้าที่จัดซื้อกลาง
					โดยทำการขอรายการสินค้านั้นๆ ได้ที่เมนู Officemate Catalog คลิ๊กในส่วนของ 'ขอเพิ่มรายการสินค้า'
					จากรายการสินค้าที่ต้องการขอเพิ่ม เพื่อส่งให้จัดซื้อกลางของท่านทราบ หากจัดซื้อกลางของท่านเห็นชอบให้ใช้ได้
					เจ้าหน้าที่จัดซื้อกลางจะเป็นผู้ดำเนินการเพิ่มรหัสสินค้านั้นให้
				</p>
				<p>
					หมายเหตุ : การคลิ๊กเลือกขอเพิ่มรายการสินค้านี้ ไม่ใช่การสั่งซื้อสินค้า และไม่ได้หมายถึงรายการสินค้าที่ท่านขอนั้นจะเข้าใน
					Standard Catalog ขององค์กรท่านโดยทันที
				</p>
				<h2 id="toc-product">
					มีการอนุมัติใบสั่งซื้อไปแล้วทำไมยังไม่ได้รับสินค้า</h2>
				<p>
					ผู้ขอซื้อสามารถตรวจสอบได้ว่าใบขอซื้อได้รับการอนุมัติแล้ว หรือไม่ โดยตรวจสอบได้จากเมนู
					'Report' จะพบเลขที่ใบขอซื้อดูในช่องสถานะว่าเป็นสถานะ 'APP' (Approved) แล้วหรือไม่
					ถ้าเป็น สถานะ 'WFA' (Waiting for Approve) ผู้ขอซื้อต้องแจ้งให้ผู้อนุมัติ ดำเนินการอนุมัติใบขอซื้อนั้นก่อน
					หากพบว่าใบขอซื้อ นั้นมีสถานะเป็น 'APP' และถึงกำหนดส่งสินค้า ที่ได้ทำการตกลงกับออฟฟิศเมท
					แต่ยังไม่ได้รับสินค้าสามารถติดต่อ OfficeMate Call Center 02-739-5555 กด 3 เพื่อสอบถามข้อมูลการจัดส่งสินค้า
				</p>
				<h2 id="toc-record">
					ต้องการดูประวัติการสั่งซื้อย้อนหลังเพื่อวิเคราะห์การสั่งซื้อ ต้องทำอย่างไร</h2>
				<p>
					ท่านสามารถดูได้โดยเข้าที่เมนู "Report" แล้วคลิก "My Historical Order" หน้าจอจะแสดง
					ใบขอซื้อทั้งหมด พร้อมวันที่ที่ท่านสร้าง และสถานะใบขอซื้อทั้งหมดว่าอยู่ในขั้นตอนใด
					รวมถึงยอดรวมการขอซื้อทั้งหมด
				</p>
				<h2 id="toc-catalog">
					My Catalog คืออะไร แตกต่างจาก Standard Catalog อย่างไร</h2>
				<p>
					My Catalog จะเป็นอีก 1 เมนูที่จะช่วยให้ผู้ขอซื้อทำงานได้ง่ายขึ้น ผู้ขอซื้อสามารถเลือกเก็บรายการสินค้าที่ต้องการสั่งซื้ออยู่เป็นประจำได้
					โดยไม่จำเป็นต้องไปเลือกใหม่จาก Standard Catalog ในทุกๆครั้งที่ต้องการซื้อสินค้านั้น
					ซึ่งผู้ขอซื้อสามารถสร้างรายการสินค้าที่ต้องการสั่งซื้ออยู่เป็นประจำได้โดยการ คลิกที่
					My catalog setting เพื่อทำการเลือกสินค้าเข้าไว้ใน My Catalog ของท่านได้
				</p>
				<h2 id="toc-password">
					หากลืม Password ในการ Log in จะต้องทำอย่างไร</h2>
				<p>
					หาก User ลืม Password ของตัวเองในการ Log in เข้าสู่ระบบ e-procurement นั้น ให้คลิกที่
					Forgot password ซึ่งจะอยู่ที่หน้าแรกของระบบ e-procurement หลังจากนั้นจะมีหน้าต่างขึ้นมาให้ท่านใส่
					e-Mail Address ของท่าน หลังจากนั้นระบบ จะทำการส่ง Password ไปยัง e-Mail ของท่าน
				</p>
			</div>
			<%--<div class="eclear">
			</div>
			<!--Start Logo -->
			<div class="footerAddress">
				<div id="epro-logo">
					<div class="one_fourth">
						<a href="http://www.officemate.co.th" class="ofmBanner"></a>
					</div>
					<div class="one_fourth">
						<a href="http://www.trendyday.com" class="TDBanner"></a>
					</div>
					<div class="one_fourth">
						<a href="http://www.trendyprint.net" class="TDprintBanner"></a>
					</div>
					<div class="one_fourth if_last">
						<a href="http://add-ons.officemate.co.th/MemberRewards" class="TDreward"></a>
					</div>
					<div class="eclear">
					</div>
				</div>
				<p class="epro-p" style="text-align: center;">
					<span style="color: #000;">บริษัท ซีโอแอล จำกัด (มหาชน) : 24 ซ.อ่อนนุช 66/1 แขวงสวนหลวง
						เขตสวนหลวง กรุงเทพฯ 10250 | โทรศัพท์ 02-739-5555 (120 สาย), แฟ็กซ์ 02-763-5555,
						อีเมล์ :</span> <span style="color: #41789c;">eprocurement@officemate.co.th
					</span>| Copyright 2008 © OfficeMate e-Procurement. All Rights Reserved
				</p>
			</div>--%>
		</div>
		<div class="eclear">
		</div>
</asp:Content>
