﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Help/Help.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<p class="pTitle" style="padding-left: 10px;">
			Approve Order | อนุมัติใบขอซื้อ</p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<!--Content-->
		<div class="epro-p" style="padding-left: 10px;">
			<p>
				เมื่อผู้อนุมัติพิจารณาอนุมัติใบขอซื้อ ระบบจะให้ผู้อนุมัติยืนยันรหัสผ่านอีกครั้ง
				เพื่ออนุมัติใบขอซื้อนี้</p>
			<p>
				อนุมัติเสร็จสิ้น ระบบจะทำการส่งเมล์แจ้งไปยังผู้ขอซื้อ ให้ทราบถึงใบขอซื้อที่ร้องขอมา
				ได้ผ่านการอนุมัติแล้ว</p>
			<p>
				ทางออฟฟิศเมทได้รับคำสั่งซื้อ จากใบขอซื้อที่ผ่านการอนุมัติ และดำเนินการจัดส่ง สินค้าตามข้อตกลง</p>
		</div>
		<!--Content-->
	</div>
</asp:Content>
