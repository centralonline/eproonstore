﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Help/Help.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.IMasterPageData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		label:hover
		{
			color:Blue;
		}
	</style>
	<script type="text/javascript">
		$(document).ready(function () {
			$("#panel1").show();
			$(".detail").hide();
			$(".flip").click(function () {
				var x = $(this).attr("devid")
				$("#" + x).slideToggle();
			});
		});
	</script>
	<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left: 10px;">
			แนะนำการใช้งานระบบ OfficeMate e-Procurement</p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<!--Content-->
		<p class="flip epro-p" devid="panel1" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">กระบวนการสั่งซื้อ
				OfficeMate e-Procurement</label></p>
		<p id="panel1">
			<img src="/images/theme/process-intro.jpg" class="image-center image-frame" style="width: 880px" /></p>
		<p class="epro-p" style="padding-left: 35px; text-decoration: underline;">
			ขั้นตอนของผู้สั่งซื้อ</p>
		<p class="flip epro-p" devid="panel2" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">วิธีการสั่งซื้อสินค้า</label></p>
		<p>
			<ul id="panel2" class="detail epro-p">
				<li>เลือกค้นหารายการสินค้า</li>
				<p>
					<img src="/images/help/searchproduct.jpg" class="image-center image-frame" style="width: 880px" /></p>
				<li>หรือเลือกจากหมวดรายการสินค้า</li>
				<p>
					<img src="/images/help/selectcat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				<li>ระบบสามารถแสดงรายการสินค้าได้ 2 รูปแบบ</li>
				<ul>
					<li>แสดงรายละเอียดสินค้า</li>
					<p>
						<img src="/images/help/viewproddetail.jpg" class="image-center image-frame" style="width: 880px" /></p>
					<li>แสดงเป็น List รายการ</li>
					<p>
						<img src="/images/help/viewprodlist.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</ul>
				<li>ใส่จำนวนสินค้า จากนั้นคลิกปุ่มสั่งซื้อสินค้า</li>
				<p>
					<img src="/images/help/addprodtocart.jpg" class="image-center image-frame" style="width: 880px" /></p>
				<li>ในหน้าตะกร้ามีช่องทางง่ายๆให้คุณเลือกระบุรหัสสินค้า ในกรณีที่คุณทราบรหัสสินค้า</li>
				<p>
					<img src="/images/help/addprodbypid.jpg" class="image-center image-frame" style="width: 880px" /></p>
				<li>จากนั้นเลือกหน่วยงานที่คุณจะสั่งซื้อ</li>
				<p>
					<img src="/images/help/selectcostcenter.jpg" class="image-center image-frame" style="width: 880px" /></p>
				<li>จะระบุหมายเหตุ หรือต้องการให้เจ้าหน้าที่ออฟฟิศเมทติดต่อ สามารถเลือกได้ในส่วนนี้</li>
				<p>
					<img src="/images/help/addremark.jpg" class="image-center image-frame" style="width: 880px" /></p>
				<li>คลิก “ขั้นตอนต่อไป” คุณสามารถตรวจสอบข้อมูลได้อีกครั้ง เพื่อความถูกต้อง</li>
				<p>
					<img src="/images/help/confirmorder.jpg" class="image-center image-frame" style="width: 880px" /></p>
				<li>คลิกปุ่มยืนยันจะได้เลขที่ใบสั่งซื้อ ระบบจะส่งอีเมล์ใบสั่งซื้อนี้เพื่อรออนุมัติจากผู้อนุมัติของหน่วยงานนั้นๆ
					ถือเป็นจบการสร้างใบสั่งซื้อ</li>
				<p>
					<img src="/images/help/completeorder.jpg" class="image-center image-frame" style="width: 880px" /></p>
			</ul>
		</p>
		<p class="flip epro-p" devid="panel3" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การแก้ไขใบขอซื้อ</label></p>
		<p>
			<ul id="panel3" class="detail epro-p">
				<li>ในหน้าตะกร้า คุณสามารถแก้ไขจำนวนสินค้าได้โดยระบุจำนวนใหม่
					<p>
						<img src="/images/help/editqty.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>จากนั้นคลิกปุ่มคำนวณยอดรวมใหม่อีกครั้ง</li>
				<p>
					<img src="/images/help/calamt.jpg" class="image-center image-frame" style="width: 880px" /></p>
				<li>การยกเลิกรายการสินค้า คุณสามารถเลือกติ้กด้านหน้ารายการสินค้าที่ต้องการยกเลิก จากนั้นคลิกปุ่มยกเลิกก็จะเป็นการนำรายการสินค้านั้น
					ออกจากตะกร้าของคุณ</li>
				<p>
					<img src="/images/help/cancelpid.jpg" class="image-center image-frame" style="width: 880px" /></p>
			</ul>
		</p>
		<p class="flip epro-p" devid="panel4" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การสั่งซื้อสินค้าจากใบสั่งซื้อเดิม (RePurchase)</label></p>
		<p>
			<ul id="panel4" class="detail epro-p">
				<li>เลือกเมนู  “สั่งซื้อจากใบสั่งซื้อเดิม”
					<p>
						<img src="/images/help/selectmenurepur.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>เลือกรายการสินค้าจากใบสั่งซื้อเดิมของคุณ เข้าตะกร้า
					<p>
						<img src="/images/help/selectprodpur.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>ส่วนขั้นตอนการทำใบสั่งซื้อ สามารถดำเนินการตามขั้นตอนปกติ
				</li>
			</ul>
		</p>
		<p class="flip epro-p" devid="panel5" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การขอเพิ่มสินค้าจากแคตตาล็อกออฟฟิศเมท</label></p>
		<p>
			<ul id="panel5" class="detail epro-p">
				<li>เลือกรายการสินค้าตามหมวดรายการสินค้าที่คุณต้องการ
					<p>
						<img src="/images/help/SelectProdNonCat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>ในตะกร้าจะมีรายการสินค้า ที่อยู่นอกเหนือจากที่ควบคุมไว้ในแคตตาล็อกองค์กรของคุณ
					<p>
						<img src="/images/Help/CartRequestNonCat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>เลือกหน่วยงาน ที่คุณต้องการสั่งซื้อ  และคลิกไปในขั้นตอนต่อไป
					<p>
						<img src="/images/help/SelectCostNonCat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>ตรวจสอบรายการสินค้า จำนวนที่สั่งซื้อให้ครบถ้วน รวมไปถึงหน่วยงานที่คุณสั่งซื้อถูกต้องหรือไม่  เมื่อทุกส่วนถูกต้องคลิก “ยืนยัน”  เพื่อจัดทำใบสั่งซื้อ
					<p>
						<img src="/images/help/ConfirmOrderNonCat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>จากนั้นระบบจะส่งข้อมูลใบสั่งซื้อของคุณไปยังผู้ดูแลระบบองค์กรของคุณ  เพื่อพิจารณาเพิ่มรายการสินค้า ดังกล่าวเข้าแคตตาล็อกองค์กร และพิจารณาอนุญาติในการสั่งซื้อรายการสินค้าครั้งนี้
					<p>
						<img src="/images/help/AdminApproveNonCat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>ระบบจะส่งอีเมล์แจ้งไปยังผู้อนุมัติของหน่วยงานที่คุณทำการสั่งซื้อ  เพื่อขอพิจารณาอนุมัติใบสั้งซื้อนี้
					<p>
						<img src="/images/help/AdminApproveOrder.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
			</ul>
		</p>
		<p class="flip epro-p" devid="panel6" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การขอเพิ่มรายการสินค้าพิเศษนอกเหนือจากแคตตาล็อกออฟฟิศเมท</label></p>
		<p>
			<ul id="panel6" class="detail epro-p">
				<li>คลิกเลือกเบรนเนอร์ “ขอซื้อสินค้านอกแคตตาล็อก” ซึ่งอยู่ด้านซ้ายของเว็บสั่งซื้อ
					<p>
						<img src="/images/help/SelectBannerRequestNonOFMCat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>ใส่ข้อมูลชื่อรายการสินค้าที่คุณต้องการสั่งซื้อ จากนั้นให้คลิก “Send”
					<p>
						<img src="/images/help/KeyTopicRequestNonOFMCat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>ระบบจะส่งอีเมล์แจ้งผู้ดูระบบองค์กรของคุณ  เพื่อให้รับทราบการร้องขอเพิ่มรายการสินค้านี้ และดำเนินการต่อให้คุณหลังจากนี้
					<p>
						<img src="/images/help/MailTopicRequestNonOFMCat.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
			</ul>
		</p>
		<p class="flip epro-p" devid="panel7" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การตรวจสอบรายงานใบสั่งซื้อ (Report)</label></p>
		<p>
			<ul id="panel7" class="detail epro-p">
				<li>คลิกเลือกเมนู “รายงานการสั่งซื้อ”
					<p>
						<img src="/images/help/SelectMenuReport.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>คุณสามารถดูรายงานการสั่งซื้อได้จากการเลือก สถานะใบสั่งซื้อ หรือระบุวันที่สั่งซื้อ หรือวันที่อนุมัติของผู้อนุมัติ จากนั้นคลิกปุ่ม “ค้นหา” รายงานตามเงื่อนไขที่คุณได้ระบุ
					<p>
						<img src="/images/help/SelectConditionReport.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>ระบบจะแสดงรายการตามเงื่อนไขที่คุณเลือก
					<p>
						<img src="/images/help/ViewReport.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
			</ul>
		</p>
		<p class="epro-p" style="padding-left: 35px; text-decoration: underline;">
			ขั้นตอนของผู้อนุมัติ</p>
		<p class="flip epro-p" devid="panel8" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">วิธีการอนุมัติใบสั่งซื้อ</label></p>
		<p>
			<ul id="panel8" class="detail epro-p">
				<li>เมื่อมีการทำใบสั่งซื้อจากผู้ขอซื้อ ระบบจะส่งอีเมล์แจ้งมายังผู้อนุมัติเพื่อขอพิจารณาอนุมัติการสั่งซื้อ
					<p>
						<img src="/images/help/MailWaitApprove.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>ผู้อนุมัติสามารถเลือกพิจารณาใบสั่งซื้อนี้ ได้ดังนี้
					<ul>
						<li>อนุมัติใบสั่งซื้อ คลิกปุ่ม 
						<p>
							<img src="/images/help/ApproveButton.jpg" class="image-center image-frame" style="width: 880px" /></p>
						</li>
						<li>ไม่อนุมัติใบสั่งซื้อ  คลิกปุ่ม 
						<p>
							<img src="/images/help/CancelButton.jpg" class="image-center image-frame" style="width: 880px" /></p>
						</li>
						<li>ส่งกลับใบสั่งซื้อให้แก้ไขใหม่ 
						<p>
							<img src="/images/help/ReviseButton.jpg" class="image-center image-frame" style="width: 880px" /></p>
						</li>
						<li>อนุมัติใบสั่งซื้อเบื้องต้น   
						<p>
							<img src="/images/help/PartailButton.jpg" class="image-center image-frame" style="width: 880px" /></p>
						</li>
						<p>
							*กรณีวงเงินการอนุมัติของผู้อนุมัติไม่เพียงพอ เมื่อพิจารณาแล้วระบบจะส่งไปยังผู้อนุมัติลำดับถัดไปที่มีวงเงินการอนุมัติที่สูงกว่า
						</p>
					</ul>
					
				</li>
				<li>โดยไม่ว่าผู้อนุมัติจะเลือกพิจารณาแบบใด เพื่อคลิกปุ่มต้องระบุรหัสผ่านเพื่อยืนยันตัวตน และการพิจารณานั้นอีกครั้ง
					<p>
						<img src="/images/help/ApproveAuthenOrder.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>จากนั้นระบบจะส่งอีเมล์แจ้งไปยังผู้ขอซื้อ ให้ทราบถึงผลการพิจารณานั้นๆ
					<p>
						<img src="/images/help/MailApproveAuthen.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				</ul>
			</p>
		<p class="flip epro-p" devid="panel9" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">วิธีการแก้ไขใบสั่งซื้อของผู้อนุมัติ</label></p>
		<p>
			<ul id="panel9" class="detail epro-p">
				<li>
				<pre>
					เมื่อใบสั่งซื้อส่งมาถึงผู้อนุมัติเพื่อพิจารณา  ผู้อนุมัติสามารถแก้ไขจำนวนรายการสินค้าได้ โดยระบุจำนวนสั่งซื้อของรายการสินค้านั้นๆ เช่น รายการสินค้า
					รหัส 2020430 สั้งซื้อไป 2 หน่วย ต้องการเพิ่มจำนวนก็เพียงระบุจำนวนใหม่ คลิกปุ่ม “edit” จากนั้นแก้ไขจำนวน คลิกปุ่มบันทึกระบบจะให้คุณระบุรหัสผ่านอีกครั้งเพื่อยืนยันตัวตน  
					*กรณีต้องการยกเลิกรายการนั้น  สามารถเลือกรายการสินค้านั้นแล้วคลิกปุ่ม “ยกเลิก”
					</pre> 
					<p>
						<img src="/images/help/ApproverEditQty.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
				<li>จากนั้นผู้อนุมัติเลือกพิจารณาอนุมัติใบสั่งซื้อ
					<p>
						<img src="/images/help/ApproverAction.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
			</ul>
			</p>
		<p class="flip epro-p" devid="panel10" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การส่งใบสั่งซื้อแก้ไขใหม่</label></p>
		<p>
			<ul id="panel10" class="detail epro-p">
				<li>กรณีผู้อนุมัติไม่สมควรในการพิจารณาอนุมัติในใบสั่งซื้อนั้นๆ สามารถส่งใบสั่งซื้อนั้นส่งคืนผู้ขอซื้อเพื่อให้ดำเนินการแก้ไขใหม่ โดยผู้อนุมัติระบุข้อมูลการนำกลับไปแก้ไขใหม่นี้ได้ในช่องหมายเหตุ
				<p>
						<img src="/images/help/ApproverReviseOrder.jpg" class="image-center image-frame" style="width: 880px" /></p>
				</li>
			</ul>
		</p>
		<p class="epro-p" style="padding-left: 35px; text-decoration: underline;">
			การตั้งค่าอื่นๆ</p>
		<p class="flip epro-p" devid="panel11" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การเปลี่ยนรหัสผ่าน</label></p>
			<p>
				<ul id="panel11" class="detail epro-p">
					<li>คลิกเลือกเมนู ข้อมูลส่วนตัว > ตั้งค่าข้อมูลส่วนตัว
					<p>
						<img src="/images/help/SelectMenuUserProfile.jpg" class="image-center image-frame" style="width: 880px" /></p>
					</li>
					<li>ระบุรหัสผ่านปัจจุบัน  รหัสผ่านใหม่ และยืนยันรหัสผ่านใหม่อีกครั้ง  คลิกปุ่ม “บันทึก” เพื่อยืนยันข้อมูล
					<p>
						<img src="/images/help/ChangePasswordUserProfile.jpg" class="image-center image-frame" style="width: 880px" /></p>
					</li>
				</ul>
			</p>
		<p class="flip epro-p" devid="panel12" style="padding-left: 10px;">
			<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การเปลี่ยนภาษาแสดงผล</label></p>
			<p>
				<ul id="panel12" class="detail epro-p">
					<li>คลิกเลือกเมนู ข้อมูลส่วนตัว > ตั้งค่าข้อมูลส่วนตัว
					<p>
						<img src="/images/Help/selectmenuuserprofile.jpg" class="image-center image-frame" style="width: 880px" /></p>
					</li>
					<li>คลิกเลือกภาษาที่คุณต้องการ  คลิกปุ่ม “บันทึก” เพื่อยืนยันข้อมูล
					<p>
						<img src="/images/Help/changelang.jpg" class="image-center image-frame" style="width: 880px" /></p>
					</li>
				</ul>
			</p>
			<!-- Admin Site -->
			<%if (Model.User.IsAdmin || Model.User.IsAssistantAdmin || Model.User.IsObserve)
	 {
		%>
			<p class="flip epro-p" devid="panel13" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การกำหนดข้อมูลองค์กร (Company Setting)</label></p>
						<p>
							<ul id="panel13" class="detail epro-p">
								<li>คลิกเลือกแท็บ “ข้อมูลองค์กร”
								<p>
									<img src="/images/help/AdminSelectMenuOrg.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>คุณสามารถตรวจสอบข้อมูลองค์กรได้ในส่วนนี้
								<p>
									<img src="/images/help/AdminOrgDetail.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
							</ul>
						</p>
						<p class="flip epro-p" devid="panel14" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การกำหนดข้อมูลฝ่าย (Department Setting)</label></p>
						<p>
							<ul id="panel14" class="detail epro-p">
								<li>คลิกเลือกแท็บ “ฝ่าย”
								<p>
									<img src="/images/help/AdminSelectMenuDept.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>คุณสามารถตรวจสอบข้อมูลฝ่าย รวมถึงเพิ่มข้อมูลได้ในส่วนนี้
								<p>
									<img src="/images/help/AdminDeptDetail.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
							</ul>
						</p>
						<p class="flip epro-p" devid="panel15" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การกำหนดข้อมูลหน่วยงานที่สั่งซื้อ (Cost Center Setting)</label></p>
						<p>
						<ul id="panel15" class="detail epro-p">
							<li>คลิกเลือกแท็บ “หน่วยงาน/แผนก”
								<p>
									<img src="/images/help/AdminSeelctCost.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>คุณสามารถตรวจสอบข้อมูลหน่วยงาน รวมถึงเพิ่มข้อมูลได้ในส่วนนี้
								<p>
									<img src="/images/help/AdminCostDetail.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
							</ul>
						</p>
						<p class="flip epro-p" devid="panel16" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การเพิ่ม และกำหนดสิทธิ์ผู้ใช้งาน (Users Setting)</label></p>
						<p>
						<ul id="panel16" class="detail epro-p">
							<li>คลิกแท็บ “ผู้ใช้งาน”
								<p>
									<img src="/images/help/AdminSelectUser.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>คุณสามารถตรวจสอบข้อมูลผู้ใช้งาน  รวมถึงเพิ่มข้อมูลผู้ใช้งานในระบบได้
								<p>
									<img src="/images/help/AdminUsersDetail.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>คลิกปุ่ม “เพิ่มผู้ใช้งาน”
								<p>
									<img src="/images/help/AdminCreateUser.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>สามารถกำหนดสิทธิ์ผู้ใช้งานได้ ดังนี้
									<ul>
										<li>
											เป็นผู้สั่งซื้อสินค้า เลือก “ผู้สั่งซื้อ”
										</li>
										<li>
											เป็นผู้อนุมัติ เลือก “ผู้อนุมัติ”
										</li>
										<li>
											เป็นผู้ช่วยผู้ดูแลระบบ เลือก “ผู้ช่วยผู้ดูแลระบบ”
										</li>
										<li>
											เป็นผู้สังเกตการณ์ เข้ามาตรวจสอบรายงานการสั่งซื้อ  เลือก “ผู้สังเกตการณ์”
										</li>
										<p>
											<img src="/images/Help/createuserview.jpg" class="image-center image-frame" style="width: 880px" /></p>
									</ul>
								</li>
								<li>เมื่อคลิกปุ่ม “สร้างผู้ใช้งาน” ระบบจะทำการสร้างข้อมูลผู้ใช้งาน  และส่งเมล์แจ้งผู้ใช้งานให้ยืนยันตัวตนเพื่อเข้าใช้งานระบบ
								<p>
									<img src="/images/help/MailVerifyUsers.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
							</ul>
						</p>
						<p class="flip epro-p" devid="panel17" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การกำหนดสิทธิ์การสั่งซื้อให้กับผู้ใช้งาน</label></p>
						<p>
							<ul id="panel17" class="detail epro-p">
								<li>คลิกเลือกแท็บ “สิทธิ์การสั่งซื้อ”
								<p>
									<img src="/images/help/AdminSelectMenuRole.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>คุณสามารถเลือกวิธีการกำหนดสิทธิ์ได้ 2 แบบ คือ
									<ul>
										<li>
											กำหนดสิทธิ์ตามหน่วยงาน หรือแผนกที่ต้องการสั่งซื้อ
											<p>
											<img src="/images/help/AdminSetRoleCostView.jpg" class="image-center image-frame" style="width: 880px" /></p>
										</li>
										<li>
											กำหนดสิทธิ์ตามผู้ใช้งาน
											<p>
											<img src="/images/help/AdminSetRoleUserView.jpg" class="image-center image-frame" style="width: 880px" /></p>
										</li>
									</ul>
								</li>
							</ul>
						</p>
						<p class="flip epro-p" devid="panel18" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การพิจารณาเพิ่มรายการสินค้าจากผู้สั่งซื้อ</label></p>
						<p>
							<ul id="panel18" class="detail epro-p">
								<li>เมื่อผู้ขอซื้อมีการร้องขอสั่งซื้อรายการสินค้าที่ไม่ได้อยู่ในแคตตาล็อกองค์กร  ระบบจะส่งใบสั่งซื้อให้ผู้ดูแลระบบพิจารณาโดยมีวิธีการ ดังนี้
									<ul>
										<li>
											อนุมัติรายการสินค้าที่ร้องขอ เฉพาะใบสั่งซื้อนี้เท่านั้น
											<p>
											<img src="/images/help/AdminApproveBypassProd.jpg" class="image-center image-frame" style="width: 880px" /></p>
										</li>
										<li>
											อนุมัติรายการสินค้าที่ร้องขอ พร้อมเพิ่มรายการดังกล่าวในแคตตาล็อคองค์กรกำหนดสิทธิ์ตามผู้ใช้งาน
											<p>
											<img src="/images/help/AdminApproveProdAddCompcat.jpg" class="image-center image-frame" style="width: 880px" /></p>
										</li>
									</ul>
								</li>
								<li>
									เมื่อผู้ดูแลระบบอนุมัติแล้ว ระบบจะส่งเมล์แจ้งไปยังผู้อนุมัติท่านที่ 1 เพื่ออนุมัติใบขอซื้อ
									<p>
											<img src="/images/help/AdminApproveOrder.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
							</ul>
						</p>
						<p class="flip epro-p" devid="panel19" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การเปิด และปิดระบบการสั่งซื้อ (Close Site)</label></p>
						<p>
							<ul id="panel19" class="detail epro-p">
								<li>
									คลิกปุ่ม “เปิดใช้งาน” หรือปุ่ม “ออกจากระบบ”
									<p>
											<img src="/images/help/AdminOpenSite.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>
									ระบบจะแจ้งผู้ดูแลระบบให้เลือกเปิดเว็บไซต์สั่งซื้อ ซึ่งมี 2 แบบ คือ
									<ul>
										<li>
											เปิดเว็บไซต์สั่งซื้อโดยไม่แจ้งผู้ใช้งาน
										</li>
										<li>
											เปิดเว็บไซต์สั่งซื้อพร้อมแจ้งผู้ใช้งานผ่านเมล์
											<p>
											<img src="/images/help/AdminSelectOpenSite.jpg" class="image-center image-frame" style="width: 880px" /></p>
										</li>
									</ul>
								</li>
							</ul>
						</p>
						<p class="flip epro-p" devid="panel20" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การจัดการข่าวสาร (News)</label></p>
						<p>
							<ul id="panel20" class="detail epro-p">
								<li>
									คลิกเมนู “ข่าวสารองค์กร”
									<p>
										<img src="/images/help/AdminSelectMenuNews.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>
									กรณีต้องการประกาศข่าว  คลิกปุ่ม “เพิ่มข่าว”
									<p>
										<img src="/images/help/AdminCreateNews.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>
									สิ่งที่ต้องกำหนดในการประกาศข่าว
									<ul>
										<li>
											ระบุหัวข้อข่าว
										</li>
										<li>
											รายละเอียดของข่าว
										</li>
										<li>
											กำหนดช่วงระยะเวลาการแสดงข่าว  แบบแสดงทันที หรือกำหนดระยะเวลาวันเริ่มแสดงข่าว  จนถึงวันสิ้นสุดการแสดงข่าว
										</li>
										<li>
											เลือกการให้ความสำคัญของข่าว แบบปกติ หรือสำคัญ
										</li>
										<li>
											สถานะของข่าว  ให้แสดงข่าว หรือยกเลิกข่าวนี้ออกไป
										</li>
									</ul>
									<p>
											<img src="/images/help/AdminCreateTopicNews.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>
									กรณีต้องการแก้ไขข่าว  คลิกปุ่ม “Edit”
									<p>
										<img src="/images/help/AdminEditNews.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>
									ผู้ดูแลระบบสามารถแก้ไขรายละเอียดของข่าวได้ลักษณะเดียวกับการสร้างข่าว
									<p>
										<img src="/images/help/AdminEditTopicNews.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
							</ul>
						</p>
						<p class="flip epro-p" devid="panel21" style="padding-left: 10px;">
						<img src="/images/icon/current_help.png" style="margin-right: 10px;" /><label style="cursor: pointer">การตรวจสอบโครงสร้างขององค์กร</label></p>
						<p>
							<ul id="panel21" class="detail epro-p">
								<li>
									คลิกเลือกแท็บ “ตรวจสอบภาพรวม”
									<p>
											<img src="/images/help/AdminSelectOrgReview.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>
									คุณสามารถตรวจสอบข้อมูลขององค์กรได้ ดังนี้
									<ul>
										<li>
											ข้อมูลองค์กร  รหัสลูกค้า รวมถึงที่อยู่ใบกำกับในการสั่งซื้อในแต่ละหน่วยงาน
										</li>
										<li>
											ข้อมูลฝ่าย
										</li>
										<li>
											ข้อมูลหน่วยงาน หรือแผนกในการสั่งซื้อ
										</li>
										<li>
											ข้อมูลผู้ใช้งาน รวมถึงสิทธิ์การใช้งานต่างๆ
										</li>
										<li>
											ข้อมูลสิทธ์การสั่งซื้อของหน่วยงาน หรือแผนก
										</li>
									</ul>
									<p>
											<img src="/images/help/OrgReview.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
								<li>
									และสามารถนำข้อมูลโครงสร้างองค์กรมาตรวจสอบโดยการ Export Excel File
									<p>
											<img src="/images/help/ExportOrgReview.jpg" class="image-center image-frame" style="width: 880px" /></p>
								</li>
							</ul>
						</p>
		<%
	 } %>
			
		<!--Content-->
	</div>
</asp:Content>
