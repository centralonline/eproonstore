﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Help/Help.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<p class="pTitle" style="padding-left: 10px;">
			Revise Order | ส่งไปสั่งซื้อกับไปแก้ไข
		</p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<!--Content-->
		<div class="epro-p" style="padding-left: 10px;">
			<p>
				ผู้อนุมัติ ส่งใบสั่งซื้อกลับไปยังผู้สั่งซื้อ เพื่อแก้ไขใบสั่งซื้อ</p>
			<p>
				เมื่อผู้สั่งซื้อแก้ไขไบสั่งซื้อเสร็จสิ้น ใบสั่งซื้อจะถูกส่งกลับมาจากผู้อนุมัติอีกครั้ง</p>
		</div>
		<!--Content-->
	</div>
</asp:Content>
