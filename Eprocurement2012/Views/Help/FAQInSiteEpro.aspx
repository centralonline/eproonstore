﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Help/Help.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".LinkMenu").click(function () {
				var id = $(this).attr("href");
				$(".pTitle").css("background-color", "");
				$(id).css("background-color", "#C0C0C0");
			});
		});
	</script>
	<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left: 10px;">
			คำถามพบบ่อย</p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<div class="Faq_link">
			<div class="margin" style="padding-left: 10px; padding-right: 10px;">
				<ol>
					<li><a id="question" class="LinkMenu" href="#toc-question">หากมีข้อสงสัยเกี่ยวกับการใช้งาน
						OfficeMate e-Procurement จะติดต่อทางใดได้บ้าง</a> </li>
					<li><a id="step" class="LinkMenu" href="#toc-step">ขั้นตอนการทำใบสั่งซื้อเป็นอย่างไร</a>
					</li>
					<li><a id="approve" class="LinkMenu" href="#toc-approve">ผู้อนุมัติสามารถทำการอนุมัติได้จากช่องทางไหนบ้าง</a>
					</li>
					<li><a id="purchase" class="LinkMenu" href="#toc-purchase">จะทราบได้ยังไงว่าใบขอซื้อมีการอนุมัติแล้ว</a>
					</li>
					<li><a id="require" class="LinkMenu" href="#toc-require">ต้องการสินค้าที่ไม่มีอยู่ในระบบ
						e-Procurement จะต้องทำอย่างไร</a> </li>
					<li><a id="product" class="LinkMenu" href="#toc-product">มีการอนุมัติใบสั่งซื้อไปแล้วทำไมยังไม่ได้รับสินค้า</a>
					</li>
					<li><a id="record" class="LinkMenu" href="#toc-record">ต้องการดูประวัติการสั่งซื้อย้อนหลังเพื่อวิเคราะห์การสั่งซื้อ
						ต้องทำอย่างไร</a> </li>
					<li><a id="catalog" class="LinkMenu" href="#toc-catalog">My Catalog คืออะไร แตกต่างจาก
						Standard Catalog อย่างไร</a> </li>
					<li><a id="password" class="LinkMenu" href="#toc-password">หากลืม Password ในการ Log
						in จะต้องทำอย่างไร</a> </li>
				</ol>
				<h2 id="toc-question" class="pTitle">
					หากมีข้อสงสัยเกี่ยวกับการใช้งาน OfficeMate e-Procurement จะติดต่อทางใดได้บ้าง</h2>
				<p class="epro-p">
					หากต้องการสอบถามข้อมูลสินค้า หรือการใช้งานระบบ ท่านสามารถติดต่อเจ้าหน้าที่ออฟฟิศเมท
					โดยเลือกในเมนูส่วนของ 'contact us' เพื่อสอบถามข้อมูล หรือข้อสงสัยของท่านจากช่องทางนี้ได้
				</p>
				<h2 id="toc-step" class="pTitle">
					ขั้นตอนการทำใบสั่งซื้อเป็นอย่างไร</h2>
				<p class="epro-p">
					ผู้ใช้งานเข้าสู่ระบบ e-Procurement แล้วก็ทำการเลือกสินค้าที่ต้องการโดยเลือกจากหมวดสินค้า
					ประเภทต่าง ๆ ด้านซ้ายมือ ใส่จำนวนที่ต้องการในช่องจำนวนของสินค้าที่เลือก เมื่อทำการเลือกสินค้าเรียบร้อยแล้วให้คลิ๊กที่
					Add to Cart และ คลิ๊กที่ปุ่ม Check out เพื่อทำการเลือก Cost center ที่ต้องการสั่งซื้อสินค้าให้
					จากนั้นคลิ๊กที่ปุ่ม 'สร้างใบขอซื้อ' ระบบจะแสดงข้อความยืนยันการทำใบขอซื้อ แล้วรอผลพิจารณาจากผู้อนุมัติ
					สามารถศึกษาวิธีการใช้งานเพิ่มเติมได้อย่างละเอียดจากที่เมนู ‘Instruction’ หรือดาวน์โหลดไฟล์คู่มือได้ที่เมนู
					‘Download Manual’
				</p>
				<h2 id="toc-approve" class="pTitle">
					ผู้อนุมัติสามารถทำการอนุมัติได้จากช่องทางไหนบ้าง</h2>
				<p class="epro-p">
					ช่องทางแรก : จาก e-Mail แจ้งการขอซื้อที่ระบบจัดส่งให้ โดยคลิ๊ก Link ที่คำว่า "คลิ๊กที่นี่เพื่อพิจารณาใบขอซื้อ"
					ระบบจำทำการเปิดใบขอซื้อนั้น เพื่อให้ท่านพิจารณาอนุมัติทันที
				</p>
				<p class="epro-p">
					ช่องทางที่สอง : ผ่านหน้าเว็บไซต์ e-Procurement ของท่าน โดยใช้ User name คือ e-Mail
					Address ของผู้อนุมัติเอง และใช้ Password เดียวกับที่ใช้อนุมัติผ่าน e-Mail การอนุมัติ
					จะต้องกรอก Password เพื่อยืนยันการพิจารณาใบขอซื้อ หลังจากนั้นระบบจะขึ้นข้อความยืนยันให้ทราบ
					และจะมี e-Mail แจ้งกลับไปให้ผู้ขอซื้อทราบด้วยว่าได้รับการอนุมัติใบขอซื้อนั้นแล้ว
				</p>
				<h2 id="toc-purchase" class="pTitle">
					จะทราบได้ยังไงว่าใบขอซื้อมีการอนุมัติแล้ว</h2>
				<p class="epro-p">
					หลังจากที่ผู้อนุมัติทำการอนุมัติใบสั่งซื้อแล้ว ระบบจะส่ง e-Mail แจ้งให้ผู้ที่ทำใบขอซื้อทราบ
					หรือผู้ขอซื้อเองก็สามารถตรวจสอบได้ โดยเข้าเมนู 'Report' แล้วคลิ๊กเมนูย่อย 'Report'
					หากสถานะใบสั่งซื้อเป็นสถานะ 'APP' (Approved) นั้นหมายถึงใบขอซื้อนั้นมีการอนุมัติเรียบร้อยแล้ว
				</p>
				<h2 id="toc-require" class="pTitle">
					ต้องการสินค้าที่ไม่มีอยู่ในระบบ e-Procurement จะต้องทำอย่างไร</h2>
				<p class="epro-p">
					เนื่องจากสินค้าที่มีอยู่ใน Standard Catalog ของท่านเป็นสินค้าที่ถูกเลือกสรรโดยฝ่ายจัดซื้อกลางขององค์กรท่าน
					หากท่านต้องการสั่งซื้อสินค้า ที่ไม่มีอยู่ใน Standard Catalog ท่านสามารถติดต่อเจ้าหน้าที่จัดซื้อกลาง
					โดยทำการขอรายการสินค้านั้นๆ ได้ที่เมนู Officemate Catalog คลิ๊กในส่วนของ 'ขอเพิ่มรายการสินค้า'
					จากรายการสินค้าที่ต้องการขอเพิ่ม เพื่อส่งให้จัดซื้อกลางของท่านทราบ หากจัดซื้อกลางของท่านเห็นชอบให้ใช้ได้
					เจ้าหน้าที่จัดซื้อกลางจะเป็นผู้ดำเนินการเพิ่มรหัสสินค้านั้นให้
				</p>
				<p class="epro-p">
					หมายเหตุ : การคลิ๊กเลือกขอเพิ่มรายการสินค้านี้ ไม่ใช่การสั่งซื้อสินค้า และไม่ได้หมายถึงรายการสินค้าที่ท่านขอนั้นจะเข้าใน
					Standard Catalog ขององค์กรท่านโดยทันที
				</p>
				<h2 id="toc-product" class="pTitle">
					มีการอนุมัติใบสั่งซื้อไปแล้วทำไมยังไม่ได้รับสินค้า</h2>
				<p class="epro-p">
					ผู้ขอซื้อสามารถตรวจสอบได้ว่าใบขอซื้อได้รับการอนุมัติแล้ว หรือไม่ โดยตรวจสอบได้จากเมนู
					'Report' จะพบเลขที่ใบขอซื้อดูในช่องสถานะว่าเป็นสถานะ 'APP' (Approved) แล้วหรือไม่
					ถ้าเป็น สถานะ 'WFA' (Waiting for Approve) ผู้ขอซื้อต้องแจ้งให้ผู้อนุมัติ ดำเนินการอนุมัติใบขอซื้อนั้นก่อน
					หากพบว่าใบขอซื้อ นั้นมีสถานะเป็น 'APP' และถึงกำหนดส่งสินค้า ที่ได้ทำการตกลงกับออฟฟิศเมท
					แต่ยังไม่ได้รับสินค้าสามารถติดต่อ OfficeMate Call Center 02-739-5555 กด 3 เพื่อสอบถามข้อมูลการจัดส่งสินค้า
				</p>
				<h2 id="toc-record" class="pTitle">
					ต้องการดูประวัติการสั่งซื้อย้อนหลังเพื่อวิเคราะห์การสั่งซื้อ ต้องทำอย่างไร</h2>
				<p class="epro-p">
					ท่านสามารถดูได้โดยเข้าที่เมนู "Report" แล้วคลิก "My Historical Order" หน้าจอจะแสดง
					ใบขอซื้อทั้งหมด พร้อมวันที่ที่ท่านสร้าง และสถานะใบขอซื้อทั้งหมดว่าอยู่ในขั้นตอนใด
					รวมถึงยอดรวมการขอซื้อทั้งหมด
				</p>
				<h2 id="toc-catalog" class="pTitle">
					My Catalog คืออะไร แตกต่างจาก Standard Catalog อย่างไร</h2>
				<p class="epro-p">
					My Catalog จะเป็นอีก 1 เมนูที่จะช่วยให้ผู้ขอซื้อทำงานได้ง่ายขึ้น ผู้ขอซื้อสามารถเลือกเก็บรายการสินค้าที่ต้องการสั่งซื้ออยู่เป็นประจำได้
					โดยไม่จำเป็นต้องไปเลือกใหม่จาก Standard Catalog ในทุกๆครั้งที่ต้องการซื้อสินค้านั้น
					ซึ่งผู้ขอซื้อสามารถสร้างรายการสินค้าที่ต้องการสั่งซื้ออยู่เป็นประจำได้โดยการ คลิกที่
					My catalog setting เพื่อทำการเลือกสินค้าเข้าไว้ใน My Catalog ของท่านได้
				</p>
				<h2 id="toc-password" class="pTitle">
					หากลืม Password ในการ Log in จะต้องทำอย่างไร</h2>
				<p class="epro-p">
					หาก User ลืม Password ของตัวเองในการ Log in เข้าสู่ระบบ e-procurement นั้น ให้คลิกที่
					Forgot password ซึ่งจะอยู่ที่หน้าแรกของระบบ e-procurement หลังจากนั้นจะมีหน้าต่างขึ้นมาให้ท่านใส่
					e-Mail Address ของท่าน หลังจากนั้นระบบ จะทำการส่ง Password ไปยัง e-Mail ของท่าน
				</p>
			</div>
		</div>
	</div>
</asp:Content>
