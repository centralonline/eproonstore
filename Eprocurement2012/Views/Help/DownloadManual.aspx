﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Help/Help.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">


		<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left:10px;">ดาวน์โหลดคู่มือใช้งานระบบ OfficeMate e-Procurement</p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<!--Content-->
		<p  class="epro-p" style="padding-left:10px;">
			<a href="<%=Url.Action("DownloadFile","Account", null) %>" target="_blank">
			<img src="/images/icon/pdf.png" alt="ดาวน์โหลดคู่มือใช้งาน" title="" /> คู่มือการใช้งานระบบ</a>
		</p>
		<!--Content-->
	</div>

</asp:Content>
