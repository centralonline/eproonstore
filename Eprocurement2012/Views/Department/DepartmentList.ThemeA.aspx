﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Department/Department.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.DepartmentListData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<table>
			<tr>
				<td id="left-cols">
					<div id="content-department-left-panel">
						<div class="list-box">
							<h3>
								Department
							</h3>
							<%Html.RenderPartial("DepartmentNavigator", Model); %>
						</div>
						<%Html.RenderPartial("RelateDepartment", Model); %>
						<%Html.RenderPartial("DefaultPropertyDepartment", Model); %>
					</div>
				</td>
				<td id="center-cols" style="padding-left: 10px;">
					<div id="content-department-dept">
						<div id="breadcrumbs">
							<%Html.RenderPartial("BreadCrumb", Model.BreadCrumb); %></div>
						<div style="float: left; padding: 5px 5px 0px 0px;">
							<h1>
								<%=Html.Encode(Model.CurrentDepartment.Name) %>
							</h1>
						</div>
						<div style="clear: both;">
						</div>
						<div id="content-department-deptlist">
							<%
								foreach (var deptChild in Model.Departments.Where(d => d.ParentId == Model.CurrentDepartment.Id))
								{
							%>
							<div class="content-department-deptlist-details" style="height: 50px;">
								<a href="<%=Url.Action("DepartmentList", "Department", new { deptChild.Id, Paths = Html.GenerateDepartmentPaths(Model.Paths, Model.CurrentDepartment.Id) })%>">
									<img src="<%=deptChild.ImageDepartmentUrl %>" alt="<%=deptChild.Name %>" class="border-image" /></a><br />
								<%=Html.ActionLink(deptChild.Name, "DepartmentList", "Department", new { deptChild.Id, Paths = Html.GenerateDepartmentPaths(Model.Paths, Model.CurrentDepartment.Id) }, null)%>
								<%
									if (Model.Departments.Where(d => d.ParentId == deptChild.Id).Count() > 0)
									{
										int count = 0;
										int perItem = 10;
										int deptItemCount = Model.Departments.Where(d => d.ParentId == deptChild.Id).Count();
								%>
								<ul class="for-dropdown">
									<li>
										<img alt="" style="width: 45px; height: 16px;" src="/images/btn/btn-plus-small.png" />
										<ul>
											<li>
												<% 	
										foreach (var deptGrandChild in Model.Departments.Where(d => d.ParentId == deptChild.Id))
										{
											count++;
											if (count % perItem == 1)
											{
												%>
												<ol>
													<%
											}
													%>
													<li>
														<%=Html.ActionLink(deptGrandChild.Name, "DepartmentList", "Department", new { deptGrandChild.Id, title = deptGrandChild.Name, Paths = Html.GenerateDepartmentPaths(Model.Paths, Model.CurrentDepartment.Id, deptChild.Id) }, null)%>
													</li>
													<%
											if (count % perItem == 0 || count == deptItemCount)
											{
													%>
												</ol>
												<%
											}
										}
									}
												%>
											</li>
										</ul>
									</li>
								</ul>
							</div>
							<%
								}
							%>
							<% 
								if (Model.CurrentDepartment.DisplayTypeUp == Eprocurement2012.Models.DepartmentDisplayType.Product)
								{
							%>
							<div class="content-department-deptlist-details" style="height: 58px;">
								<span id="view-all-department">
									<br />
									<%=Html.ActionLink("ดูทั้งหมด", "DepartmentList", "Department", new { Model.CurrentDepartment.Id, IsBreadCrumbUp=true }, null)%>
								</span>
							</div>
							<%	
								}
							%>
						</div>
						<br class="clear" />
						<div id="content-department-dept-show">
							<!--Promotion-->
							<%
								if (Model.ProductPromotion.Any())
								{
							%>
							<h3>
								<%=Html._("Department.DepartmentList.ThemeA.ProductPromotion")%>
								<%=Model.CurrentDepartment.Name%></h3>
							<ul>
								<%
									foreach (var item in Model.ProductPromotion.Take(16))
									{

										Html.RenderPartial("Product", item);
									}
								%></ul>
							<%
								}
							%>
							<br />
							<br />
						</div>
						<%=Html._("Department.DepartmentList.ThemeA.ProductVisiting")%>
						<h2 style="display: inline; font-size: 11px; padding: 0px; margin: 0px;">
							<%=Html.Encode(Model.CurrentDepartment.Name)%></h2>
						<br />
						<br />
					</div>
				</td>
				<td id="right-cols">
				</td>
			</tr>
		</table>
		<%
			if (Model.ProductHistory.Any())
			{
		%>
		<div id="product-history">
			<h3 style="background-color: #FFFFFF;">
				<%=Html._("Department.DepartmentList.ThemeA.ProductHistory")%></h3>
			<ul>
				<%
				foreach (var product in Model.ProductHistory)
				{
					Html.RenderPartial("Product", product);
				}
				%></ul>
		</div>
		<%
			}
		%>
	</div>
</asp:Content>
