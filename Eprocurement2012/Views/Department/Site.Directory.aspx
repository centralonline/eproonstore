﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SiteDirectoryData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
		});
	</script>
	<div class="center">
		<div class="Site-Directory-topic">
			<h1>
				<%=Html._("Department.Shopping")%>
			</h1>
		</div>
		<%foreach (var deptChild in Model.Departments.Where(d => d.ParentId == 0)){%>
		<div class="content-department-deptlist-details-all">
			<a href="<%=Url.Action("DepartmentList","Department",new { deptChild.Id, title=deptChild.Name }) %>">
				<img src="<%=deptChild.ImageDepartmentUrl %>" height="100" alt="<%=deptChild.Name %>"
					style="margin-bottom: 10px; border: 1px solid #F0F0F0;" /></a>
			<br />
			<%=Html.ActionLink(deptChild.Name, "DepartmentList", "Department", new { deptChild.Id, title=deptChild.Name }, null)%>
			<ul>
				<%foreach (var deptGrandChild in Model.Departments.Where(d => d.ParentId == deptChild.Id)){
				%>
				<li>
					<%=Html.ActionLink(deptGrandChild.Name, "DepartmentList", "Department", new { deptGrandChild.Id, title = deptGrandChild.Name, Paths = deptChild.Id }, null)%></li>
				<%}%>
			</ul>
		</div>
		<%}%></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
