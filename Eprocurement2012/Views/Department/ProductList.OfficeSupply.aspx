﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Department/Department.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OfficeSupplyProductListData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<table>
			<tr>
				<td id="left-cols">
					<div id="content-department-left-panel">
						<div class="list-box">
							<h3>
								Department
							</h3>
							<%Html.RenderPartial("DepartmentNavigator", Model); %>
						</div>
						<%Html.RenderPartial("RelateDepartment", Model); %>
						<%Html.RenderPartial("PropertyDepartment", Model); %>
						<%Html.RenderPartial("DefaultPropertyDepartment", Model); %>
						<br />
					</div>
				</td>
				<td class="table-layout-column-center2">
					<div id="content-department-product">
						<div id="breadcrumbs">
							<%Html.RenderPartial("BreadCrumb", Model.BreadCrumb);%></div>
						<h1>
							<%=Html.Encode(Model.CurrentDepartment.Name)%></h1>
						<%if (!(Model.IsPromotion))
		{%>
						<div id="content-department-product-display-setting">
							<div id="content-department-product-display-view">
								<b>
									<%=Html._("Department.DisplayType")%>
									»</b> List <a href="<%=Url.Action("DepartmentList", "Department", new { Model.CurrentDepartment.Id, Model.pRefId, Model.bRefId, Model.cRefId, Model.BreadCrumb.IsBreadCrumbUp,promotion = Model.IsPromotion,view="list", Model.SortBy }) %>"
										title="List View">
										<img alt="List View" src="/images/icon_layout_list.gif" /></a> Detail <a href="<%=Url.Action("DepartmentList", "Department", new { Model.CurrentDepartment.Id,Model.pRefId, Model.bRefId, Model.cRefId, Model.BreadCrumb.IsBreadCrumbUp,promotion = Model.IsPromotion,view="thumbnail", Model.SortBy }) %>"
											title="Thumbnail View">
											<img alt="Thumbnail View" src="/images/icon_layout_thum.gif" /></a>
							</div>
							<div id="content-department-product-sortby">
								<%Html.BeginForm("DepartmentList", "Department", new { Model.CurrentDepartment.Id, title = Model.CurrentDepartment.Name }, FormMethod.Get, new { id = "formSortBy" });
		  if (Model.pRefId != null)
		  {
			  Response.Write(Html.HiddenFor(m => m.pRefId));
		  }

		  if (Model.bRefId != null)
		  {
			  Response.Write(Html.HiddenFor(m => m.bRefId));
		  }

		  if (Model.cRefId != null)
		  {
			  Response.Write(Html.HiddenFor(m => m.cRefId));
		  }
								%>
								<%=Html.Hidden("IsBreadCrumbUp", Model.BreadCrumb.IsBreadCrumbUp)%>
								<%=Html.Hidden("view", Model.ProductListViewType) %>
								<span>
									<%=Html._("Department.ProductListViewType")%>&nbsp;</span>
								<select id="sortBy" name="sortBy">
									<option value="New" <%if (Eprocurement2012.Models.DepartmentSortByType.New == Model.SortBy) { %>
										selected='selected' <% } %>>
										<%=Html._("Department.DepartmentSortByType.New")%></option>
									<option value="PriceLowToHigh" <%if (Eprocurement2012.Models.DepartmentSortByType.PriceLowToHigh == Model.SortBy) { %>
										selected='selected' <% } %>>
										<%=Html._("Department.DepartmentSortByType.PriceLowToHigh")%></option>
									<option value="PriceHighToLow" <%if (Eprocurement2012.Models.DepartmentSortByType.PriceHighToLow == Model.SortBy) { %>
										selected='selected' <% } %>>
										<%=Html._("Department.DepartmentSortByType.PriceHighToLow")%></option>
									<option value="Promotion" <%if (Eprocurement2012.Models.DepartmentSortByType.Promotion == Model.SortBy) { %>
										selected='selected' <% } %>>
										<%=Html._("Department.DepartmentSortByType.Promotion")%></option>
									<option value="Brand" <%if (Eprocurement2012.Models.DepartmentSortByType.Brand == Model.SortBy) { %>
										selected='selected' <% } %>>
										<%=Html._("Department.DepartmentSortByType.Brand")%></option>
								</select>
								<input type="hidden" name="PropName" id="PropName" value="" />
								<%
			if (Model.CurrentDepartment.HasSubDept)
			{
								%>
								<%=Html.CheckBox("sortByDept",Model.SortByDept) %>
								<span>
									<%=Html._("Department.SortByDept")%></span><div id="tooltip" style="display: inline;
										margin-left: 5px">
									</div>
								<%
			}
								%>
								<%
			if (Model.PropertyDepartment.Any())
			{
								%>
								<%=Html.CheckBox("groupByDeptSupply", Model.GroupByDeptSupply, new { style="display:none;" })%>
								<%
			}
								%>
								<input id="content-department-product-display-setting-applysort" type="image" src="/images/btn_sort.gif"
									value="Apply Sort" alt="เรียงสินค้า" title="เรียงสินค้า" />
								<%Html.EndForm(); %>
							</div>
						</div>
						<%}%>
						<%Html.BeginForm("HandleBuyOfficeSupply", "Product", FormMethod.Post, new { @class = "handleBuy-OfficeSupply" }); %>
						<ul id="content-department-product-list-office" class="list">
							<%
								foreach (var product in Model.Products)
								{
									if (Model.SKUs[product.Id].Any())
									{
							%>
							<li style="display: block">
								<div style="display: table-cell; width: 150px; position: relative;">
									<a href="<%= Url.Action("Details", "Product", new { product.Id, Paths = Model.Paths })%>">
										<img id="productImage" src="<%=product.ImageSmallUrl %>" width="150" height="150"
											alt="รูปสินค้า <%=Html.Encode(product.Name)%>" title="<%= Html.Encode(product.Name)%>" />
									</a>
									<input type="hidden" name="codeId" value="<%=product.CodeId %>" />
									<input type="hidden" name="pId" value="<%=product.Id %>" />
								</div>
								<%
										if (!String.IsNullOrEmpty(product.DescriptionShort))
										{
								%>
								<div id="content-product-description" style="display: table-cell; font: normal 13px/18px tahoma;
									padding-left: 20px; padding-bottom: 10px; vertical-align: top">
									<p style="font: bold 14px/16px tahoma; padding-left: 0px; margin-top: 10px; margin-bottom: 15px;">
										<a href="<%= Url.Action("Details", "Product", new { product.Id, Paths = Model.Paths })%>">
											<%=product.Name%></a>
									</p>
									<%Html.FormatTextile(product.DescriptionShort); %>
									<p style="display: table-cell">
										<a href="<%= Url.Action("Details", "Product", new { product.Id, Paths = Model.Paths })%>">
											&#8226;
											<%=Html._("Department.ProductList.OfficeSupply.AllDetail")%></a></p>
								</div>
								<%}%>
								<table class="office-list-product" cellpadding="0" cellspacing="0">
									<thead>
										<tr>
											<!---Start สำหรับเพิ่มสินค้าใน MyCatalog--->
											<%if (Model.User.IsRequester)
			 {%>
											<td class="text-center fix50">
											</td>
											<%} %>
											<!---End--->
											<td class="text-center fix50">
												<%=Html._("ProductCatalog.HeadProductId")%>
											</td>
											<td class="text-center" style="width: 80%">
												<%=Html._("ProductCatalog.HeadProductName")%>
											</td>
											<td class="text-center bgcolor-hilight">
												<%=Html._("ProductCatalog.HeadProductPrice")%>
											</td>
											<%if (Model.User.IsRequester && Model.SKUs[product.Id].Where(o => o.IsDisplayBuyButton).Any())
			 { %>
											<td class="text-center bgcolor-hilight2 fix50">
												<%=Html._("ProductCatalog.HeadQuantity")%>
											</td>
											<%} %>
											<td class="text-center fix50">
												<%=Html._("ProductCatalog.HeadProductUnit")%>
											</td>
										</tr>
									</thead>
									<tbody>
										<%
										foreach (var productSku in Model.SKUs[product.Id])
										{
										%>
										<tr>
											<!---Start สำหรับเพิ่มสินค้าใน MyCatalog--->
											<%if (Model.User.IsRequester)
			 {%>
											<td style="text-align: center">
												<input type="checkbox" name="WishListProductId" value="<%=productSku.Id %>" />
											</td>
											<%} %>
											<!---End--->
											<td style="text-align: center">
												<span class="gray">
													<%=productSku.Id%></span>
											</td>
											<td>
												<div class="office-list-product-infomation-name">
													<a href="<%= Url.Action("Details", "Product", new { productSku.Id, Paths=Model.Paths })%>">
														<%=Html.Encode(productSku.Name)%>
													</a>
													<p style="margin: 3px 0;">
														<%=(!String.IsNullOrEmpty(productSku.PromotionText)) ? productSku.PromotionText : String.Empty%></p>
													<p class="status color-stock">
														<%=productSku.DisplayStatusText%>
													</p>
													<%=productSku.IsPromotion ? "<br /><span style='color:Red'>สินค้าโปรโมชั่น</span>" : String.Empty%>
													<%=productSku.IsContactForDelivery ? "<br /><span style='color:Red'>* สินค้ามีค่าจัดส่ง กรุณาติดต่อเจ้าหน้าที่ ที่เบอร์. 027395555</span>" : String.Empty%>
												</div>
											</td>
											<td class="text-center">
												<%=productSku.DisplayPrice%>
											</td>
											<%if (Model.User.IsRequester && Model.SKUs[product.Id].Where(o => o.IsDisplayBuyButton).Any())
			 { %>
											<td class="text-center">
												<%=Html.Hidden("productId", productSku.Id)%>
												<%=productSku.IsDisplayBuyButton ? "<input type='text' style='width:30px;' maxlength='4' name='quantity' />" : productSku.IsContactForDelivery ? "<input type='hidden' name='quantity' value='' />" : "<span style='color:Red;'>สินค้าขาด</span><input type='hidden' name='quantity' value='' />"%>
											</td>
											<%}%>
											<td class="text-center">
												<%=productSku.Unit%>
												<%=Html.Hidden("DepartmentId", Model.CurrentDepartment.Id)%>
												<%=Html.Hidden("Title", Model.CurrentDepartment.Name)%>
												<%=Html.Hidden("Paths", Html.GenerateDepartmentPaths(Model.Paths, Model.CurrentDepartment.Id))%>
											</td>
										</tr>
										<%}%>
									</tbody>
								</table>
								<table class="office-list-product" cellpadding="0" cellspacing="0">
									<tr>
										<!---Start เพิ่มปุ่ม Add to Catalog--->
										<%if (Model.User.IsRequester)
			{%>
										<td colspan="2" style="background-color: #d1d1d1;">
											<%
										if (Model.MyCatalogs.Any())
											{
											%>
											<%=Html.DropDownList("catalogGUID", Model.MyCatalogs, new { style = "min-width:50px;max-width:170px; margin-bottom:5px;", @class = "catalog" })%>
											<%
										}
											%>
											<input type="image" name="AddToWishListOfficeSupply" value="AddToWishListOfficeSupply"
												class="AddToWishListOfficeSupply" src="/images/btn/btn_add_to_wish_list.png" />
										</td>
										<%} %>
										<!--End--->
										<td colspan="6" style="background-color: #d1d1d1;">
											<div style="float: right; background-color: #d1d1d1; width: auto">
												<%if (Model.User.IsRequester)
			  { %>
												<%if (Model.SKUs[product.Id].Where(o => o.IsDisplayBuyButton).Any())
			  {%>
												<input type="image" name="AddToCartOfficeSupply" value="AddToCartOfficeSupply" class="AddToCartOfficeSupply"
													src="/images/theme/button-addTocart.png" />
												<%} %>
												<%} %>
											</div>
										</td>
									</tr>
								</table>
								<div style="height: 25px; padding-bottom: 5px; margin-bottom: 30px; clear: both;">
									<div class="left" style="font-size: 14px; color: #d30; font-weight: bold; background-color: #fff;
										padding: 5px;">
										<%=TempData["WarningMessageCatalog"]%><%=TempData["WarningMessage"]%></div>
									<div class="right">
										<a href="#" style="color: Gray; margin-left: 3px; line-height: 30px">back to top</a></div>
								</div>
							</li>
							<%
									}
								}
							%>
						</ul>
						<% if (Model.User.Company.ShowOFMCat && Model.ProductsOFM != null && Model.ProductsOFM.Any())
		 {%>
						<ul id="content-department-product-list-office" class="list">
							<%
			 foreach (var product in Model.ProductsOFM)
			 {
				 if (Model.OFMSKUs[product.Id].Any())
				 {
							%>
							<li style="display: block">
								<div style="display: table-cell; width: 150px; position: relative;">
									<a href="<%= Url.Action("Details", "Product", new { product.Id, Paths = Model.Paths })%>">
										<img id="Img1" src="<%=product.ImageSmallUrl %>" width="150" height="150" alt="รูปสินค้า <%=Html.Encode(product.Name)%>"
											title="<%= Html.Encode(product.Name)%>" />
									</a>
									<input type="hidden" name="codeId" value="<%=product.CodeId %>" />
									<input type="hidden" name="pId" value="<%=product.Id %>" />
								</div>
								<%
					 if (!String.IsNullOrEmpty(product.DescriptionShort))
					 {
								%>
								<div id="content-product-description" style="display: table-cell; font: normal 13px/18px tahoma;
									padding-left: 20px; padding-bottom: 10px; vertical-align: top">
									<p style="font: bold 14px/16px tahoma; padding-left: 0px; margin-top: 10px; margin-bottom: 15px;">
										<a href="<%= Url.Action("Details", "Product", new { product.Id, Paths = Model.Paths })%>">
											<%=product.Name %></a>
									</p>
									<%--<%= product.DescriptionShort %>--%>
									<%Html.FormatTextile(product.DescriptionShort); %>
									<p style="display: table-cell">
										<a href="<%= Url.Action("Details", "Product", new { product.Id, Paths = Model.Paths })%>">
											&#8226;
											<%=Html._("Department.ProductList.OfficeSupply.AllDetail")%></a></p>
								</div>
								<%}%>
								<table class="office-list-product" cellpadding="0" cellspacing="0">
									<thead>
										<tr>
											<td class="text-center fix50">
												<%=Html._("ProductCatalog.HeadProductId")%>
											</td>
											<td class="text-center" style="width: 80%">
												<%=Html._("ProductCatalog.HeadProductName")%>
											</td>
											<td class="text-center bgcolor-hilight">
												<%=Html._("ProductCatalog.HeadProductPrice")%>
											</td>
											<%if (Model.User.IsRequester && Model.OFMSKUs[product.Id].Where(o => o.IsDisplayBuyButton).Any())
			 { %>
											<td class="text-center bgcolor-hilight2 fix50">
												<%=Html._("ProductCatalog.HeadQuantity")%>
											</td>
											<%} %>
											<td class="text-center fix50">
												<%=Html._("ProductCatalog.HeadProductUnit")%>
											</td>
										</tr>
									</thead>
									<tbody>
										<%
					 foreach (var productSku in Model.OFMSKUs[product.Id])
					 {
										%>
										<tr>
											<td style="text-align: center">
												<span class="gray">
													<%=productSku.Id %></span>
											</td>
											<td>
												<div class="office-list-product-infomation-name">
													<a href="<%= Url.Action("Details", "Product", new { productSku.Id, Paths=Model.Paths })%>">
														<%=Html.Encode(productSku.Name)%>
													</a>
													<p style="margin: 3px 0;">
														<%=(!String.IsNullOrEmpty(productSku.PromotionText)) ? productSku.PromotionText : String.Empty %></p>
													<p class="status color-stock">
														<%=productSku.DisplayStatusText%>
													</p>
													<p>
														<%=Html._("CartDetailPartial.ProductNotInYourCatalog")%>
													</p>
													<%=productSku.IsPromotion ? "<br /><span style='color:Red'>สินค้าโปรโมชั่น</span>" : String.Empty %>
													<%=productSku.IsContactForDelivery ? "<br /><span style='color:Red'>* สินค้ามีค่าจัดส่ง กรุณาติดต่อเจ้าหน้าที่ ที่เบอร์. 027395555</span>" : String.Empty%>
												</div>
											</td>
											<td class="text-center">
												<%=productSku.DisplayPrice %>
											</td>
											<%if (Model.User.IsRequester && Model.OFMSKUs[product.Id].Where(o => o.IsDisplayBuyButton).Any())
			 { %>
											<td class="text-center">
												<%=Html.Hidden("productId", productSku.Id)%>
												<%=!productSku.IsContactForDelivery && productSku.IsDisplayBuyButton ? "<input type='text' style='width:30px;' maxlength='4' name='quantity' />" : productSku.IsContactForDelivery ? "<span style='color:Red;'>สินค้ามีค่าจัดส่ง</span><input type='hidden' name='quantity' value='' />" : "<span style='color:Red;'>สินค้าขาด</span><input type='hidden' name='quantity' value='' />"%>
											</td>
											<%} %>
											<td class="text-center">
												<%=productSku.Unit%>
												<%=Html.Hidden("DepartmentId", Model.CurrentDepartment.Id) %>
												<%=Html.Hidden("Title", Model.CurrentDepartment.Name) %>
												<%=Html.Hidden("Paths", Html.GenerateDepartmentPaths(Model.Paths, Model.CurrentDepartment.Id)) %>
											</td>
										</tr>
										<%
					 }
										%>
									</tbody>
								</table>
								<table class="office-list-product" cellpadding="0" cellspacing="0">
									<tr>
										<td style="background-color: #d1d1d1;">
											<div style="float: right; background-color: #d1d1d1; width: auto">
												<%if (Model.User.IsRequester)
			  { %>
												<%if (Model.OFMSKUs[product.Id].Where(o => o.IsDisplayBuyButton).Any())
			  {%>
												<input type="image" name="AddToCartOfficeSupply" value="AddToCartOfficeSupply" class="AddToCartOfficeSupply"
													src="/images/theme/button-addTocart.png" />
												<%} %>
												<%} %>
											</div>
										</td>
									</tr>
								</table>
								<div style="height: 25px; padding-bottom: 5px; margin-bottom: 30px; clear: both;">
									<div class="left" style="font-size: 14px; color: #d30; font-weight: bold; background-color: #fff;
										padding: 5px;">
										<%=TempData["WarningMessageCatalog"]%><%=TempData["WarningMessage"]%></div>
									<div class="right">
										<a href="#" style="color: Gray; margin-left: 3px; line-height: 30px">back to top</a></div>
								</div>
							</li>
							<%
				 }
			 }
							%>
						</ul>
						<%} %>
						<%Html.EndForm(); %>
						<div class="clear">
						</div>
						<%
							if (Model.Products.Any())
							{
						%>
						<%--<%Html.EndForm(); %>--%>
						<div class="clear">
						</div>
						<%
								if (Model.SimilarDepartment.Any())
								{
						%>
						<div id="content-department-dept-show">
							<h4 style="background-color: #6E736E; color: #FFFFFF; font: bold 15px tahoma; padding: 5px 10px">
								<%=Html._("Department.ProductList.ListView.SimilarDepartment")%></h4>
							<%
									foreach (var deptItem in Model.SimilarDepartment)
									{
							%>
							<h3 style="color: #6E736E; padding-left: 15px; padding-right: 15px; background-image: none;
								border-bottom: 1px solid #CCCCCC; border-top: 1px dashed #CCCCCC; margin: 0;
								background-color: #fff">
								<%=deptItem.Name%>
								<span style="float: right; padding: 0px 0px 0px 0px;"><span id="viewall" style="font-size: 13px;
									font-weight: bold; text-decoration: underline">
									<%=Html.ActionLink(Html._("Button.All"), "DepartmentList", "Department", new { deptItem.Id, title = deptItem.Name, IsBreadCrumbUp = true }, null)%>
								</span></span>
							</h3>
							<ul id="content-department-product-list">
								<%
										foreach (var productItem in Model.ProductsOfRelateDept[deptItem.RefId].Take(16))
										{
											Html.RenderPartial("Product", productItem);
										}
								%>
							</ul>
							<%
									}
							%>
						</div>
						<%
								}
							}
						%>
					</div>
				</td>
				<td class="right-cols">
				</td>
			</tr>
		</table>
		<%
			if (Model.ProductHistory.Any())
			{
		%>
		<div id="product-history">
			<h3 style="background-color: #FFFFFF;">
				<%=Html._("Department.DepartmentList.ThemeA.ProductHistory")%>
			</h3>
			<ul>
				<%
				foreach (var product in Model.ProductHistory)
				{
					Html.RenderPartial("Product", product);
				}
				%>
			</ul>
		</div>
		<%
			}
		%>
	</div>
	<div id="message-Popup" style="display: none; text-align: center;">
	</div>
	<div id="BackToShopOrGotoCheckOut" style="display: none;">
		<br />
		<br />
		<br />
		<input type="image" src="/images/btn/btn_shop_continue.gif" value="กลับไป shop" id="backtoshop"
			onclick="closeMessagePopup();" />
		หรือ <a href="<%=Url.Action("CartDetail", "Cart") %>">
			<img src="/images/btn/btn_go2_checkout.gif" alt="ไปหน้า Check Out" /></a>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		var popupTimer;
		$(function () {
			$('.AddToCartOfficeSupply').click(addToCartOfficeSupply);
			//$('.AddToWishListOfficeSupply').click(addToWishListOfficeSupply);
			$("#message-Popup").bind("dialogclose", function (event, ui) {
				clearTimeout(popupTimer);
			});

			$('.catalog').change(function () {
				var result = $(this).attr('value');
				$('select[class=catalog] option[value=' + result + ']').attr('selected', 'selected');
			});
		});

		function addToCartOfficeSupply(event) {
			event.preventDefault();
			if ($('input:text[name="quantity"][value!=""]').length > 0) {
				var thisForm = $(this).closest('form');
				var sendData = thisForm.serialize() + "&AddToCartOfficeSupply.AddToCartOfficeSupply.x=";
				formProcess(sendData);
			}
		}

		function addToWishListOfficeSupply(event) {
			event.preventDefault();
			if ($('input:checked[name="WishListProductId"][value!=""]').length > 0) {
				var thisForm = $(this).closest('form');
				var sendData = thisForm.serialize() + "&AddToWishListOfficeSupply.AddToWishListOfficeSupply.x=";
				formProcess(sendData);
			}
		}

		function formProcess(formData) {
			$("#message-Popup").dialog({
				autoOpen: false,
				modal: true,
				width: 420,
				height: 200
			});
			$.ajax({
				url: '<%=Url.Action("HandleBuyOfficeSupply", "Product") %>',
				type: "POST",
				data: formData,
				success: function (data) {
					if (data.Result) {
						$("#header-shoppingcart-countproduct").text(data.ItemCount);
						$("[name='quantity']").val('');
						$("#message-Popup").html("<h3>" + data.Message + "</h3>");
						$("#message-Popup").append($("#BackToShopOrGotoCheckOut").html());
						$("#message-Popup").dialog('open');
						popupTimer = setTimeout(closeMessagePopup, 30000);

					} else {
						$("#message-Popup").html("<h3>" + data.Message + "</h3>");
						$("#message-Popup").append($("#BackToShopOrGotoCheckOut").html());
						$("#message-Popup").dialog('open');
					}
				}
			});
		}

		function closeMessagePopup() {
			clearTimeout(popupTimer);
			$("#message-Popup").dialog('close');
		}
	</script>
</asp:Content>
