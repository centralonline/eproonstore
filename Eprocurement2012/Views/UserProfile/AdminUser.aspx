﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Eprocurement.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.HomePageData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="GroupData 100percent">
		<h3>
			<img src="../../images/theme/h3-list-icon.png" alt="cart and order" /><%=Html._("UserProfile.AdminUser")%></h3>
		<div class="box_white">
			<%if (Model != null)
			{ %>
			<p class="pTitle" style="padding-left: 0px;">
				<%=Html._("Account.AdminUser.RootAdmin")%></p>
			<%foreach (var itemUser in Model.UserAdmin.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin))
			{%>
			<div class="adminbox2">
				<img src="<%=Url.Action("ProfileImage","UserProfile", new {itemUser.UserGuid }) %>"
					alt="<%=itemUser.DisplayName%>" width="100" height="100" class="imgepro-left frameImages" />
				<p class="epro-p">
					<%=itemUser.DisplayName%><br />
					<span style="color: #999;">
						<%=Html._("Shared.DataUserProfile.PhoneNo")%>
						:<%=itemUser.UserContact.Phone != null ? itemUser.UserContact.Phone.PhoneNo : "-"%>
						<%=itemUser.UserContact.Phone != null && !string.IsNullOrEmpty(itemUser.UserContact.Phone.Extension) ? "#" + itemUser.UserContact.Phone.Extension : ""%>
					</span>
					<br />
					<span style="color: #999;">
						<%=Html._("Shared.DataUserProfile.mobile")%>
						:<%=itemUser.UserContact.Mobile != null ? itemUser.UserContact.Mobile.PhoneNo : "-"%>
					</span>
					<br />
					<span style="color: #999;">
						<%=Html._("Shared.MyProfile.Email")%>
						:<%=itemUser.UserContact.Email%></span><br />
					<br />
				</p>
			</div>
			<% }%>
			<%if (Model.UserAdmin.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin).Count() > 0)
			{ %>
			<div class="eclear">
			</div>
			<p class="pTitle" style="padding-left: 0px;">
				<%=Html._("Account.AdminUser.AssistantAdmin")%></p>
			<%foreach (var itemUser in Model.UserAdmin.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
			{ %>
			<div class="adminbox2">
				<img src="<%=Url.Action("ProfileImage","UserProfile", new {itemUser.UserGuid }) %>"
					alt="<%=itemUser.DisplayName%>" width="100" height="100" class="imgepro-left frameImages" />
				<p class="epro-p">
					<%=itemUser.DisplayName%><br />
					<span style="color: #999;">
						<%=Html._("Shared.DataUserProfile.PhoneNo")%>
						:<%=itemUser.UserContact.Phone != null ? itemUser.UserContact.Phone.PhoneNo : "-"%>
						<%=itemUser.UserContact.Phone != null && !string.IsNullOrEmpty(itemUser.UserContact.Phone.Extension) ? "#" + itemUser.UserContact.Phone.Extension : ""%>
					</span>
					<br />
					<span style="color: #999;">
						<%=Html._("Shared.DataUserProfile.mobile")%>
						:<%=itemUser.UserContact.Mobile != null ? itemUser.UserContact.Mobile.PhoneNo : "-"%>
					</span>
					<br />
					<span style="color: #999;">
						<%=Html._("Shared.MyProfile.Email")%>
						:<%=itemUser.UserContact.Email%></span>
				</p>
				<div class="eclear">
				</div>
			</div>
			<% } %>
			<% } %>
			<% }%>
		</div>
	</div>
</asp:Content>
