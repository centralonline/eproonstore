﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.User>" %>
<div id="profile-pic-preview" style="text-align: center; margin-right: 20px; float: left;">
	<img src="<%=Url.Action("ProfileImage","UserProfile", new { Model.UserGuid }) %>" alt="<%=Model.DisplayName%>" width="100" height="100" /><br />
	<br />
	<%Html.BeginForm("RemoveProfileImage", "UserProfile", FormMethod.Post); %>
	<input class="graybutton" type="submit" value="Remove" />
	<%Html.EndForm(); %>
</div>
<div class="right" style="width: 400px; float: left;">	
	<%Html.BeginForm("UploadImage", "UserProfile", FormMethod.Post, new { enctype = "multipart/form-data" }); %>
	<%=Html._("Shared.MyImage.Text")%><br />
	<label for="file"><%=Html._("Shared.MyImage.SelectFile")%></label>
	<input type="file" name="file" id="file" />
	<br />
	<input class="graybutton" type="submit" value="<%=Html._("Shared.MyImage.UploadFile")%>" />
	<span class="errormessage">
		<%=TempData["errormessage"]%>
	</span>
	<%Html.EndForm(); %>
</div>
