﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/UserProfile/Profile.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.EditProfileUser>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="GroupData 100percent spaceleft20">
		<h3>
			<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("Account.SetProfileUser.ProfileSetting")%></h3>
		<div class="border">
			<h5>
				<%=Html._("Account.SetProfileUser.ChangeMyImage")%> &nbsp;&nbsp;<%=TempData["Notify"]%></h5>
			<%Html.RenderPartial("MyImage", Model.User); %>
			<div class="eclear">
			</div>
			<div class="spaceTop">
			</div>
			<%Html.BeginForm("EditMyProfile", "UserProfile", FormMethod.Post); %>
			<%Html.RenderPartial("EditProfile", Model); %>
			<%Html.EndForm(); %>
			<div class="eclear">
			</div>
			<h5>
				<%=Html._("Account.SetProfileUser.ChangePassword")%></h5>
			<div class="ChangePassContact">
				<%Html.BeginForm("ForceChangePasswordInProfile", "UserProfile", FormMethod.Post); %>
				<input id="redirectUrl" name="redirectUrl" type="hidden" value="<%=Model.RedirectUrl %>" />
				<%Html.RenderPartial("ChangePassword", new Eprocurement2012.Models.Password()); %>
				<%Html.EndForm(); %>
			</div>
			<div class="eclear">
			</div>
			<h5>
				<%=Html._("Account.SetProfileUser.ChangeLanguage")%></h5>
			<div class="position">
				<label>
					<%=Html._("Account.SetProfileUser.ChangeLanguage")%></label>
			</div>
			<%Html.BeginForm("ChangeLanguage", "UserProfile", FormMethod.Post); %>
			<p>
				<%Html.RenderPartial("ChangeLanguage", Model.User); %></p>
			<%Html.EndForm(); %>
		</div>
	</div>
	<!--end GroupData-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
