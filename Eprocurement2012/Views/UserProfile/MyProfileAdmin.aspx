﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/UserProfile/ProfileAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.UserProfile>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h3 class="Head3">
			<img src="/images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("Menu.Home.MyProfile")%></h3>
		<div style="border: 1px solid #C8C8C8; -moz-border-radius: 5px 5px 5px 5px; padding: 10px;
			min-height: 280px;">
			<div id="profile-pic-preview" style="text-align: center; margin-right: 20px; float: left;">
				<img src="<%=Url.Action("ProfileImage","UserProfile", new { Model.User.UserGuid }) %>"
					alt="<%=Model.User.DisplayName%>" width="100" height="100" />
				<br />
			</div>
			<div class="clear">
			</div>
			<div id="AdminPro">
				<br />
				<%Html.RenderPartial("DataUserProfile", Model.User); %>
			</div>
		</div>
		<div class="clear">
		</div>
		<h3 class="Head3">
			<img src="/images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("MyProfile.Mypermission")%></h3>
		<%Html.RenderPartial("DataUserPermission", Model.ItemUserPermissions); %>
		<div class="spaceTop">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.Head3
		{
			font-size: 13px;
			font-weight: bold;
			padding: 5px;
			margin: 20px 0 10px 0;
			border: 1px solid #c5c5c5;
			background: url(../images/theme/bg-pattern-h3.jpg) repeat-x;
			height: 20px;
			color: #7c7c7c;
		}
	</style>
</asp:Content>
