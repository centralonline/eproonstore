﻿<%@ Page Language="C#" MasterPageFile="~/Views/UserProfile/Profile.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.UserProfile>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="GroupData 100percent spaceleft20">
		<h3>
			<img src="/images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("Menu.Home.MyProfile")%></h3>
		<div class="border">
			<div id="profile-pic-preview" style="text-align: center; margin-right: 20px; float: left;">
				<img src="<%=Url.Action("ProfileImage","UserProfile", new { Model.User.UserGuid }) %>" alt="<%=Model.User.DisplayName%>" width="100" height="100" />
				<br />
			</div>
			<div class="eclear">
			</div>
			<%Html.RenderPartial("DataUserProfile", Model.User); %>
		</div>
		<div class="spaceTop">
		</div>
		<h3>
			<img src="/images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("MyProfile.Mypermission")%></h3>
		<%Html.RenderPartial("DataUserPermission", Model.ItemUserPermissions); %>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
