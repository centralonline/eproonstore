﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/UserProfile/ProfileAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.EditProfileUser>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="grid_16">
		<h3 class="Head3">
			<img src="/images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("Account.SetProfileUser.ProfileSetting")%></h3>
		<div style="border: 1px solid #C8C8C8; -moz-border-radius: 5px 5px 5px 5px; padding: 10px;
			min-height: 280px;">
			<h5 class="Head5">
				<%=Html._("Account.SetProfileUser.ChangeMyImage")%>
				&nbsp;&nbsp;<%=TempData["Notify"]%></h5>
			<%Html.RenderPartial("AdminMyImage", Model.User); %>
			<div class="eclear">
			</div>
			<div class="spaceTop">
			</div>
			<div class="GroupData position">
				<%Html.BeginForm("EditMyProfileAdmin", "UserProfile", FormMethod.Post); %>
				<%Html.RenderPartial("EditProfile", Model); %>
				<%Html.EndForm(); %>
			</div>
			<div class="eclear">
			</div>
			<h5 class="Head5">
				<%=Html._("Account.SetProfileUser.ChangePassword")%></h5>
			<div class="ChangePassContact GroupData position">
				<%Html.BeginForm("ForceChangePasswordInProfileAdmin", "UserProfile", FormMethod.Post); %>
				<input id="redirectUrl" name="redirectUrl" type="hidden" value="<%=Model.RedirectUrl %>" />
				<%Html.RenderPartial("ChangePassword", new Eprocurement2012.Models.Password()); %>
				<%Html.EndForm(); %>
			</div>
			<div class="eclear">
			</div>
			<h5 class="Head5">
				<%=Html._("Account.SetProfileUser.ChangeLanguage")%></h5>
			<div class="position GroupData">
				<label>
					<%=Html._("Account.SetProfileUser.ChangeLanguage")%></label>
			</div>
			<%Html.BeginForm("ChangeLanguageAdmin", "UserProfile", FormMethod.Post); %>
			<p class="GroupData">
				<%Html.RenderPartial("ChangeLanguage", Model.User); %></p>
			<%Html.EndForm(); %>
		</div>
		<div class="grid_16">
			<div class="spaceTop">
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.Head3
		{
			font-size: 13px;
			font-weight: bold;
			padding: 5px;
			margin: 20px 0 10px 0;
			border: 1px solid #c5c5c5;
			background: url(../images/theme/bg-pattern-h3.jpg) repeat-x;
			height: 20px;
			color: #7c7c7c;
		}
		.GroupData .position
		{
			float: left;
			text-align: right;
			width: 130px;
			margin-right: 15px;
		}
		.GroupData label, .GroupData span.label
		{
			color: #666666;
			display: block;
			float: left; /*font-weight:normal;*/
			margin-right: 10px;
			padding-right: 10px;
			position: relative;
			text-align: left;
			width: 180px;
			line-height: 25px;
			font-weight: bold;
			color: #666666;
			font-size: 12px;
		}
		.textSpanGray
		{
			font-size: 12px;
			margin-left: 30px;
			color: #666666;
			line-height: 25px;
		}
		.medium
		{
			min-width: 240px;
			width: 26%;
		}
		.positionBtnContact
		{
			margin-left: 0px;
			line-height: 25px;
		}
		.Head5
		{
			font-size: 12px;
			color: #666666;
			font-weight: bold;
			margin: 0;
			padding-left: 0;
			padding-bottom: 8px;
			border-bottom: solid 1px #C5C5C5;
			margin-bottom: 20px;
		}
		.uiIcon .iconCorrectSmall
		{
			width: 13px;
			height: 13px;
			top: 0px;
			background-position: -0px -45px;
			background-repeat: no-repeat;
			display: inline-block;
			background-image: url('/images/icon/tRpY94D8F.png');
		}
		.uiIcon
		{
			font-size: 11px;
			text-align: left;
			color: #C4484B;
			font-weight: normal;
		}
	</style>
</asp:Content>
