﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SearchRequesterLine>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("OverViewMenuBar");%>
		<h2>
			<%=Html._("OFMAdmin.SearchUser.SpecifiedSearch")%></h2>
		<hr />
		<div>
			<%Html.BeginForm("SearchRequesterLine", "OFMAdmin", FormMethod.Post); %>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCompanyId, new { id = "CompanyId" })%>
						<label for="CompanyId">
							<%=Html._("รหัสองค์กร")%></label></legend>
					<%=Html.TextBoxFor(m => m.CompanyId)%>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckRequesterUserId, new { id = "RequesterUserId" })%>
						<label for="RequesterUserId">
							<%=Html._("รหัสผู้สั่งซื้อ (e-mail Account)")%></label></legend>
					<%=Html.TextBoxFor(m => m.RequesterUserId)%>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCostcenterId, new { id = "CostcenterId" })%>
						<label for="CostcenterId">
							<%=Html._("รหัสหน่วยงาน/แผนก")%></label></legend>
					<%=Html.TextBoxFor(m => m.CostcenterId)%>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckApproverUserId, new { id = "ApproverUserId" })%>
						<label for="ApproverUserId">
							<%=Html._("รหัสผู้อนุมัติ (e-mail Account)")%></label></legend>
					<%=Html.TextBoxFor(m => m.ApproverUserId)%>
				</fieldset>
			</div>
			<div style="width: 100%; display: inline-block;">
				<input type="submit" value="Search" />
				<span style="color: Red;">
					<%=TempData["ErrorMessage"]%></span>
			</div>
			<%Html.EndForm(); %>
		</div>
		<hr />
		<%if (Model.SearchRequesterLineData != null && Model.SearchRequesterLineData.Any())
	{%>
		<div style="margin: 10px;">
			<table id="sortBy">
				<thead>
					<tr>
						<th style="text-align: center;">
							ลำดับ
						</th>
						<th style="text-align: center;">
							รหัสองค์กร
						</th>
						<th style="text-align: center;">
							รหัสหน่วยงาน/แผนก
						</th>
						<th style="text-align: center;">
							ชื่อหน่วยงาน/แผนก (TH)
						</th>
						<th style="text-align: center;">
							ชื่อหน่วยงาน/แผนก (EN)
						</th>
						<th style="text-align: center;">
							รหัสผู้สั่งซื้อ
						</th>
						<th style="text-align: center;">
							ชื่อผู้สั่งซื้อ (TH)
						</th>
						<th style="text-align: center;">
							ชื่อผู้สั่งซื้อ (EN)
						</th>
						<th style="text-align: center;">
							รหัสผู้อนุมัติ
						</th>
						<th style="text-align: center;">
							ชื่อผู้อนุมัติ (TH)
						</th>
						<th style="text-align: center;">
							ชื่อผู้อนุมัติ (EN)
						</th>
						<th style="text-align: center;">
							ลำดับผู้อนุมัติ
						</th>
						<th style="text-align: center;">
							วงเงินอนุมัติ
						</th>
						<th style="text-align: center;">
							เวลาการอนุมัติ
						</th>
						<th style="width: 70px; text-align: center;">
							วันที่สร้าง
						</th>
						<th style="text-align: center;">
							ผู้สร้าง
						</th>
						<th style="width: 70px; text-align: center;">
							แก้ไขล่าสุด
						</th>
						<th style="text-align: center;">
							ผู้ที่แก้ไข
						</th>
					</tr>
				</thead>
				<%int count = 0;%>
				<%foreach (var requesterLine in Model.SearchRequesterLineData)
	  {%>
				<tr>
					<td>
						<%=++count%>
					</td>
					<td>
						<%=requesterLine.CompanyId%>
					</td>
					<td>
						<%=requesterLine.CostcenterId%>
					</td>
					<td>
						<%=requesterLine.CostCenter.CostCenterThaiName%>
					</td>
					<td>
						<%=requesterLine.CostCenter.CostCenterEngName%>
					</td>
					<td>
						<%=requesterLine.RequesterUserId%>
					</td>
					<td>
						<%=requesterLine.UserReqDetail.UserThaiName%>
					</td>
					<td>
						<%=requesterLine.UserReqDetail.UserEngName%>
					</td>
					<td>
						<%=requesterLine.ApproverUserId%>
					</td>
					<td>
						<%=requesterLine.ApproverThaiName%>
					</td>
					<td>
						<%=requesterLine.ApproverEngName%>
					</td>
					<td style="text-align: center;">
						<%=requesterLine.AppLevel%>
					</td>
					<td style="text-align: right;">
						<%=requesterLine.AppCreditlimit.ToString(new MoneyFormat())%>
					</td>
					<td style="text-align: center;">
						<%=requesterLine.Parkday%>
					</td>
					<td>
						<%=requesterLine.CreateOn.ToString(new DateFormat())%>
					</td>
					<td>
						<%=requesterLine.CreateBy%>
					</td>
					<td>
						<%=requesterLine.UpdateOn.ToString(new DateFormat())%>
					</td>
					<td>
						<%=requesterLine.UpdateBy%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script src="/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<link href="/css/dataTables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(function () {

			var fuzzyNum = function (x) {
				return +x.replace(/[^\d\.\-]/g, "");
			};

			jQuery.fn.dataTableExt.oSort['prettynumbers-asc'] = function (x, y) {
				return fuzzyNum(x) - fuzzyNum(y);
			};
			jQuery.fn.dataTableExt.oSort['prettynumbers-desc'] = function (x, y) {
				return fuzzyNum(y) - fuzzyNum(x);
			};
			$('#sortBy').dataTable({
				"aoColumns": [
									null, null, null, null, null, null, null, null, null, null, null, null,
									{ "sType": "prettynumbers" }, null, null, null, null, null
							]
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
