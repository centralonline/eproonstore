﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
				<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId) %>
			<br />
			<div style="text-align: center;">
				<img src="/images/step01.png" alt="" /></div>
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<p class="btn" style="float: right;">
					<input type="submit" id="CompanyGuide" name="CompanyGuide.x" value="<< Previous" /></p>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<p class="btn" style="float: left;">
					<input type="submit" id="CompanyLogo" name="CompanyLogo.x" value="Next >>" /></p>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.CompanyInformation")%></h2>
		</div>
		<div class="grid_16">
			<%--<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post);%>--%>
			<div class="grid_16">
				<label>
					<%=Html._("Admin.CompanyInfo.Content")%>
				</label>
			</div>
			<br class="clear" />
			<div class="grid_8">
				<div class="table-format">
					<label>
						<%=Html._("Admin.CompanyInfo.CompanyId")%>
					</label>
					<%=Model.CompanyId%><%=Html.HiddenFor(m => m.CompanyId) %>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.CompanyInfo.CompanyThaiName")%>
					</label>
					<%=Html.TextBoxFor(m=>m.CompanyThaiName)%>
					<span style="color: Red">*
						<%=Html._("Admin.CompanyInfo.ShowWeb")%><%=Html.LocalizedValidationMessageFor(m => m.CompanyThaiName)%></span>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.CompanyInfo.CompanyEngName")%>
					</label>
					<%=Html.TextBoxFor(m=>m.CompanyEngName)%>
					<span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.CompanyEngName)%></span>
				</div>
				<div class="table-format">
					<label>
						<%=Html._("Admin.CompanyInfo.DefaultCustId")%>
					</label>
					<%=Model.DefaultCustId%><%=Html.HiddenFor(m => m.DefaultCustId) %>
					<span>
						<%=Html._("Admin.CompanyInfo.SetOFM")%></span>
				</div>
				<br />
				<input type="submit" id="UpdateInfo" value="Save" name="UpdateInfo.x" />
			</div>
			<br class="clear" />
			<div class="grid_16" style="padding-top: 15px">
				<label>
					<%=Html._("Admin.CompanyInfo.Detail")%></label>
				<table>
					<tr>
						<th>
							<%=Html._("Admin.CompanyInfo.CustId")%>
						</th>
						<th>
							<%=Html._("Admin.CompanyInfo.InvoiceAddress")%>
						</th>
						<th>
							<%=Html._("Admin.CompanyInfo.ShippingAddress")%>
						</th>
						<th>
							<%=Html._("Admin.CompanyInfo.Data")%>
						</th>
					</tr>
					<%foreach (var item in Model.ListCompanyAddress)%>
					<%{%>
					<tr>
						<td>
							<%=item.CustId%>
						</td>
						<td>
							<%=item.InvoiceAddress1%>
						</td>
						<td>
							<%=item.CountShip%>
							<span>
								<%=Html._("Admin.CompanyInfo.Shipping")%></span>
						</td>
						<td>
						<input type="submit" id="Check_<%=item.CustId%>" value="<%=Html._("Admin.CompanyInfo.Verify")%>"
								name="CheckShipping.<%=item.CustId%>.x" />
						</td>
					</tr>
					<%} %>
				</table>
			</div>
			<div class="grid_16" style="padding-top: 15px">
				<%if (Model.CompanyInvoice != null && Model.ListShipAddress != null)
	  { %>
				<%Html.RenderPartial("ListShipAddress", Model); %>
				<%} %>
			</div>
			<div style="min-height: 500px;">
			</div>
			<%Html.EndForm();%>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
