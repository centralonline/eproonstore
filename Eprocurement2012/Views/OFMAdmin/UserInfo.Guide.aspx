﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CreateNewUser>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.DefaultCompanyId);%>
		<div class="grid_16">

		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.DefaultCompanyId)%>
			<p style="float: right;">
				<input type="submit" id="CostCenterGuide" name="CostCenterGuide.x" value="<< Previous" /></p>
			<%Html.EndForm(); %>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.DefaultCompanyId)%>
			<p class="btn" style="float: left">
				<input type="submit" id="ViewAllUser" name="ViewAllUser.x" value="Next >>" /></p>
			<%Html.EndForm(); %>
		</div>

			<h2 id="page-heading">
				<%=Html._("Admin.UserInfo")%></h2>
		</div>
		<div class="grid_16">
			<div>
				<label>
					เริ่มการตั้งค่า Users</label><br />
				<img src="/images/Company_info/Company_sec03.jpg" alt="you are here" />
			</div>
		</div>
		<div style="min-height: 500px;">
		</div>
		<!--Lin's min-height-->
		<%--<div class="grid_8">
			<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.DefaultCompanyId)%>
			<p class="btn">
				<input type="submit" id="CostCenterGuide" name="CostCenterGuide.x" value="Back" /></p>
			<%Html.EndForm(); %>
		</div>
		<div class="grid_8">
			<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.DefaultCompanyId)%>
			<p class="btn" style="float: right">
				<input type="submit" id="ViewAllUser" name="ViewAllUser.x" value="Continue" /></p>
			<%Html.EndForm(); %>
		</div>--%>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
