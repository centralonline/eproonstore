﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ViewAllDepartmentData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: right;">
					<input type="submit" id="DepartmentGuide" name="DepartmentGuide.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: left;">
					<input type="submit" id="CostCenterGuide" name="CostCenterGuide.x" value="Next >>" /></p>
				<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.Department.ContentInfo")%></h2>
		</div>
		<%if (Model.IsCompModelThreeLevel)
		{%>
		<div class="grid_16">
			<div class="grid_16">
				<%int count = 0; %>
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: right">
					<input type="submit" id="CreateNewDepartment" name="CreateNewDepartment.x" value="Create Department" /></p>
				<h5>
					<%=Html._("Admin.Department.DeptInfo")%></h5>
				<%if (Model.Departments.Any())
				{ %>
				<table>
					<tr>
						<th>
							<%=Html._("Admin.CompanyInfo.No")%>
						</th>
						<th>
							<%=Html._("Admin.Department.DeptID")%>
						</th>
						<th>
							<%=Html._("Admin.Department.ThaiName")%>
						</th>
						<th>
							<%=Html._("Admin.Department.EngName")%>
						</th>
						<th>
							<%=Html._("Admin.Department.Status")%>
						</th>
						<th>
							<%=Html._("Admin.Department.Modify")%>
						</th>
						<th>
							<%=Html._("Admin.Department.Cancel")%>
						</th>
					</tr>
					<%foreach (var item in Model.Departments)%>
					<%{%>
					<tr>
						<td>
							<%=++count%>
						</td>
						<td>
							<%=item.DepartmentID%>
						</td>
						<td>
							<%=item.DepartmentThaiName%>
						</td>
						<td>
							<%=item.DepartmentEngName%>
						</td>
						<td>
							<%=item.DisplayDepartmentStatus%>
						</td>
						<td>
							<input type="submit" id="edit_<%=item.DepartmentID%>" value="edit" name="EditDepartment.<%=item.DepartmentID%>.x" />
						</td>
						<td>
							<%if (item.DepartmentStatus == Eprocurement2012.Models.CompanyDepartment.DeptStatus.Active)
							{%>
							<input type="submit" id="remove_<%=item.DepartmentID%>" value="Cancel" name="RemoveDepartment.<%=item.DepartmentID%>.x" />
							<%} %>
						</td>
					</tr>
					<%} %>
				</table>
				<%} %>
				<%Html.EndForm();%>
			</div>
		</div>
		<%}%>
		<div style="min-height: 550px;">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
