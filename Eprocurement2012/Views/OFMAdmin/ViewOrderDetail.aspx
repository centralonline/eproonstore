﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<div class="GroupData">
			<h4>
				<%=Html._("OFMAdmin.ViewOrderDetail.Order")%></h4>
			<div class="border2">
				<div class="position">
					<label>
						<%=Html._("OFMAdmin.ViewOrderDetail.OrderStatus")%>:</label></div>
				<%if (Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Waiting || Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Partial
				  || Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Revise || Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.AdminAllow
				  || Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin || Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Approved)
	  {%>
				<div style="float: left;">
					<span class="textSpanGray">
						<%=Eprocurement2012.Models.JobStatus.Open.ToString() %></span></div>
				<%}
	  else
	  {%>
				<div style="float: left;">
					<span class="textSpanGray">
						<%=Eprocurement2012.Models.JobStatus.Close.ToString() %></span></div>
				<%} %>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Order.ViewOrder.CustID")%>:</label></div>
				<div style="float: left;">
					<span class="textSpanGray">
						<%=Model.Order.CostCenter.CostCenterCustID %></span></div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("OFMAdmin.ViewOrderDetail.RefInvNo")%>:</label></div>
				<div style="float: left;">
					<span class="textSpanGray">
						<%=Model.Order.RefInvNo %></span></div>
				<div class="eclear">
				</div>
			</div>
		</div>
		<div class="GroupData">
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ViewOrder.StepProcess")%></h4>
				<div class="border2">
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderId")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderID %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderDate")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderStatus")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Html._(Model.Order.Status.ToResourceKey())%>
							<a href="/Help/OrderInstruction" target="_blank">
								<%=Html._("Order.ViewOrder.OrderDetail")%></a></span>
					</div>
					<div class="eclear">
					</div>
					<%if (Model.Order.CurrentApprover != null)
	   {%>
					<div class="position">
						<label>
							<%=Html._("Order.ViewOrder.WaitingApproval")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.CurrentApprover.Approver.DisplayName%>
						</span>
					</div>
					<%} %>
					<div class="eclear">
					</div>
					<%Html.RenderPartial("OrderDataFlow", Model.Order); %>
				</div>
			</div>
		</div>
		<div class="eclear">
		</div>
		<!--Print-->
		<div class="eclear">
			<div class="boxPrint" style="position: absolute; margin-left: 740px; margin-top: 50px;">
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintTHPO"})%>">
						Print PO (TH)</a>
				</div>
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid, ActionName = "PrintEnPO"})%>">
						Print PO (EN)</a>
				</div>
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintThQU"})%>">
						Print QU (TH)</a>
				</div>
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintEnQU"})%>">
						Print QU (EN)</a>
				</div>
				<div class="right noPrint btnPrint">
					<img src="/images/theme/icon_printOrder.jpg" />
					<a href="#" onclick="window.print(); return false">Print web form</a>
				</div>
			</div>
			<div id="print" class="right noPrint btnPrint">
				<img src="/images/theme/icon_printOrder.jpg" />
				<span>Print Document</span>
			</div>
		</div>
		<div style="margin: 30px 0;">
			<img alt="logo" src="/images/theme/logo-ePro.jpg" class="left" />
			<div class="eclear">
			</div>
			<!--Start printOrder detail-->
			<div style="width: 100%; padding: 20px 8px 8px 0px; margin-right: 20px; margin-top: 35px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderID %></p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="left" style="width: 450px; min-height: 320px; padding: 5px; margin-right: 9px;
				margin-top: 0px; border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CustID")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.CostCenter.CostCenterCustID %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("CartDetail.ShipContactor")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.UserId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Requester.UserId%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Phone")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorPhone %>
						<%=string.IsNullOrEmpty(Model.Order.Contact.ContactorExtension) ? "" : " #" + Model.Order.Contact.ContactorExtension%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Fax")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorFax %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.InvoiceAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Invoice.Address1 %>
						<%=Model.Order.Invoice.Address2 %>
						<%=Model.Order.Invoice.Address3 %>
						<%=Model.Order.Invoice.Address4 %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="left" style="width: 450px; min-height: 320px; margin-right: 9px; margin-top: 0px;
				border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
				padding: 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.CompanyId")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyId %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CompanyName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.DepartmentName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=string.IsNullOrEmpty(Model.Order.Department.DepartmentID)? "-": "[" + Model.Order.Department.DepartmentID + "] " + Model.Order.Department.DepartmentName %>
					</p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CostCenterName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderDate")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.ShippingAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Shipping.Address1 %>
						<%=Model.Order.Shipping.Address2 %>
						<%=Model.Order.Shipping.Address3 %>
						<%=Model.Order.Shipping.Address4 %>
						<%=Model.Order.Shipping.Province %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<!--end printOrder detail-->
			<div class="eclear">
			</div>
			<div class="printOrderlabel" style="padding-top: 20px; padding-bottom: 10px;">
				<label>
					<%=Html._("Order.ViewOrder.OrderItems")%>
				</label>
			</div>
			<div class="eclear">
			</div>
			<div style="float: left">
				<%Html.RenderPartial("OrderDetailPartial", Model.Order); %>
			</div>
			<div class="eclear">
			</div>
			<div class="GroupData">
				<div class="Column-Full">
					<h4>
						<%=Html._("Order.ViewOrder.Remark")%></h4>
					<div class="border2">
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.ApproverRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.ApproverRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.OFMRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.OFMRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.ReferenceRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.ReferenceRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<!--Show AttachFile -->
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.AttachFile")%>
								:</label>
						</div>
						<div class="left">
							<span class="printOrderSpanGray">
								<%if (!String.IsNullOrEmpty(Model.Order.CustFileName))
		  {%>
								<a target="_blank" href="<%=Url.Action("DownloadAttachFile", "Order", new { systemFileName = Model.Order.SystemFileName})%>">
									<%=Model.Order.CustFileName %></a>
								<%} %>
							</span>
						</div>
						<div class="eclear">
						</div>
						<!--End AttachFile -->
					</div>
				</div>
				<div class="eclear">
				</div>
				<div class="Column-Full">
					<h4>
						<%=Html._("Order.ViewOrder.OrderActivity")%></h4>
					<div class="eclear">
					</div>
					<%Html.RenderPartial("OrderActivityPartial", Model.OrderActivity); %>
				</div>
				<div class="eclear">
				</div>
				<div class="spaceTop">
				</div>
			</div>
		</div>
		<div class="eclear">
		</div>
		<div id="mailTransLog">
			<h4>
				<%=Html._("OFMAdmin.ViewOrderDetail.MailTransLog")%></h4>
			<table>
				<thead>
					<tr>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadCount")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadSubject")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadCreateOn")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadSendMailSuccess")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadEmailTo")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadEmailCC")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadEmailBCC")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadLogID")%>
						</th>
					</tr>
				</thead>
				<tbody>
					<%
						int countMail = 0;
						foreach (var logMail in Model.MailTransLog)
						{
					%>
					<tr>
						<td>
							<%=++countMail%>
						</td>
						<td>
							<%=logMail.Subject%>
						</td>
						<td>
							<%=logMail.CreateOn.ToString(new DateTimeFormat())%>
						</td>
						<td>
							<%=logMail.SendMailSuccess%>
						</td>
						<td>
							<%=logMail.EmailTo%>
						</td>
						<td>
							<%=logMail.EmailCC%>
						</td>
						<td>
							<%=logMail.EmailBCC%>
						</td>
						<td>
							<%Html.BeginForm("ViewOrderMail", "OFMAdmin", new { guid = Model.Order.OrderGuid, logId = logMail.LogID }); %>
							<input id="Submit1" type="submit" value="Detail" />
							<%Html.EndForm(); %>
						</td>
					</tr>
					<%}%>
				</tbody>
			</table>
		</div>
		<div id="edit-OrderInformation">
			<%if (Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Approved && Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Completed && Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Expired && Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Shipped && Model.Order.Status != Eprocurement2012.Models.Order.OrderStatus.Deleted)
	 {%>
			<div id="edit-OrderStatus">
				<h4>
					ตั้งค่าสถานะเอกสาร</h4>
				<%Html.BeginForm("EditOrderStatus", "OFMAdmin", new { orderGuid = Model.Order.OrderGuid }, FormMethod.Post); %>
				<%=Html.RadioButton("isOrderDeleted",true)%>
				<label for="isOrderDeleted">
					<%=Html._("ยกเลิกใบสั่งซื้อ")%></label>
				<br />
				<span>หมายเหตุอื่นๆ ที่ต้องการแจ้งลูกค้า:</span><br />
				<%=Html.TextArea("Remark" , new { maxlength = 100, cols = "50", rows = "4" }) %><br />
				<input type="submit" value="Save" />
				<input type="hidden" name="orderStatus" value="<%=Model.Order.Status%>" />
				<%Html.EndForm(); %>
			</div>
			<%} %>
			<%else
	 {%>
			<span style="color: Red;">ใบสั่งซื้อเลขที่
				<%=Model.Order.OrderID%>
				ไม่สามารถทำการยกเลิกได้ เนื่องจากสถานะออร์เดอร์เป็น
				<%=Model.Order.Status%>
				ค่ะ</span>
			<%} %>
			<%--<div id="edit-ProcessRemark">
				<h4>
					<%=Html._("OFMAdmin.ViewOrderDetail.ProcessRemark")%></h4>
				<%Html.BeginForm("EditProcessRemark", "OFMAdmin", new { guid = Model.Order.OrderGuid }, FormMethod.Post, new { id = "formEditProcessRemark" }); %>
				<%=Html._("OFMAdmin.ViewOrderDetail.SelectProcess")%>:
				<select id="select-ProcessRemark" name="ProcessRemark">
					<option value="">--
						<%=Html._("OFMAdmin.ViewOrderDetail.SelectProcessRemark")%>
						--</option>
					<option value="รอสินค้าเข้า">
						<%=Html._("OFMAdmin.ViewOrderDetail.IntoProduct")%></option>
				</select><br />
				<span>
					<%=Html._("OFMAdmin.ViewOrderDetail.Remark")%>:</span><br />
				<%=Html.TextArea("Remark" , new { maxlength = 100, cols = "50", rows = "4" }) %><br />
				<input type="submit" value="Save" />
				<%Html.EndForm(); %>
			</div>
			<div id="edit-ShippingType">
				<h4>
					<%=Html._("OFMAdmin.ViewOrderDetail.ShippingType")%></h4>
				<%Html.BeginForm("EditShippingType", "OFMAdmin", new { guid = Model.Order.OrderGuid }, FormMethod.Post, new { id = "formEditShippingType" }); %>
				<%=Html._("OFMAdmin.ViewOrderDetail.SelectProcess")%>:
				<select id="select-ShippingType" name="ShippingType">
					<option value="" <%=Model.Order.ShipType == "" ? "selected = selected" : "" %>>--
						<%=Html._("OFMAdmin.ViewOrderDetail.SelectProcess")%>
						--</option>
					<option value="OFM" <%=Model.Order.ShipType == "OFM" ? "selected = selected" : "" %>>
						<%=Html._("OFMAdmin.ViewOrderDetail.OFMShipType")%></option>
					<option value="POS" <%=Model.Order.ShipType == "POS" ? "selected = selected" : "" %>>
						<%=Html._("OFMAdmin.ViewOrderDetail.POSShipType")%></option>
				</select><br />
				<span>
					<%=Html._("OFMAdmin.ViewOrderDetail.Remark")%>:</span><br />
				<%=Html.TextArea("Remark" , new { maxlength = 100, cols = "50", rows = "4" }) %><br />
				<input type="submit" value="Save" />
				<%Html.EndForm(); %>
			</div>--%>
		</div>
		<div id="orderProcessLog">
			<h4>
				<%=Html._("OFMAdmin.ViewOrderDetail.ProcessLog")%></h4>
			<table>
				<thead>
					<tr>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadCount")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadProcessRemark")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadRemark")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadDateTime")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadCreateBy")%>
						</th>
					</tr>
				</thead>
				<tbody>
					<%
						int countlog = 0;
						foreach (var log in Model.ProcessLog)
						{
					%>
					<tr>
						<td>
							<%=++countlog%>
						</td>
						<td>
							<%=log.ProcessRemark %>
						</td>
						<td>
							<%=log.Remark %>
						</td>
						<td>
							<%=log.CreateOn.ToString("dd/MM/yyyy HH:mm") %>
						</td>
						<td>
							<%=log.CreateBy %>
						</td>
					</tr>
					<%}%>
				</tbody>
			</table>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#formEditOrderStatus").submit(confirmdialog);
			$("#formEditProcessRemark").submit(confirmdialog);
			$("#formEditShippingType").submit(confirmdialog);

			$(".boxPrint").hide();
			$("#print").click(function () {
				$(".boxPrint").slideToggle("slow");
			});
		});

		function confirmdialog(event) {
			var form = $(this);
			var message;
			if (form.attr("id") == "formEditOrderStatus") { message = "ยืนยันการแก้ไขสถานะใบสั่งซื้อ?" }
			if (form.attr("id") == "formEditProcessRemark") { message = "ยืนยันการแก้ไขการดำเนินรายการอื่นๆ?" }
			if (form.attr("id") == "formEditShippingType") { message = "ยืนยันการแก้ไขรูปแบบการจัดส่ง?" }

			if (!confirm(message)) {
				event.preventDefault();
			}
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.GroupData
		{
			margin-top: 10px;
			margin-right: 0px;
			margin-left: 0px;
			margin-bottom: 0px;
		}
		
		.GroupData h3
		{
			font-size: 13px;
			font-weight: bold;
			margin: 8px 0;
			padding: 5px;
			text-shadow: 1px 1px 1px white;
			border: solid 1px #C5C5C5;
			border-radius: 2px;
			-moz-border-radius: 2px;
			background: url(../images/theme/bg-pattern-h3.jpg) repeat-x;
			height: 20px;
			color: #7C7C7C;
		}
		
		.Column-Full
		{
			width: 100%;
			float: left;
		}
		
		.clear
		{
			clear: both;
		}
		.eclear
		{
			clear: both;
			height: 0px !important;
			line-height: 10px !important;
			float: none !important;
			margin-bottom: 10px !important;
		}
		
		#content-shopping-cart-detail
		{
			clear: both;
		}
		
		.printOrderlabel
		{
			color: #7C7C7C;
			display: block;
			float: left; /*font-weight:normal;*/
			margin-right: 10px;
			padding-right: 10px;
			position: relative;
			text-align: left;
			width: 150px;
			line-height: 25px;
			font-weight: bold;
			font-size: 12px;
			top: 0px;
			left: 0px;
		}
		.left
		{
			float: left;
		}
		#content-shopping-cart-detail td.thead, .format-table td.thead
		{
			background-color: #e6e6e6;
			font: bold 11px tahoma;
			padding: 5px;
			text-align: center;
		}
		#content-shopping-cart-detail td
		{
			border: solid 1px #c5c5c5;
			padding: 5px;
			color: #666666;
		}
		.centerwrap
		{
			width: 950px;
			margin: 0 auto;
			margin-top: 30px;
		}
		.GroupData h4
		{
			font-size: 13px;
			color: #4570b7;
			font-weight: bold;
			margin: 0;
			padding-left: 0;
			background: none;
		}
		a
		{
			color: #4570B7;
			text-decoration: none;
		}
		.right
		{
			float: right;
		}
		label
		{
			color: Black;
		}
		
		.boxPrint
		{
			width: 200px;
			padding: 10px;
			margin: 10px;
			height: 250px;
		}
		
		.btnPrint
		{
			width: 120px;
			border: 1px solid #E5E5E5;
			border-radius: 5px 5px 5px 5px;
			cursor: pointer;
			-moz-border-radius: 5px 5px 5px 5px;
			padding: 5px;
			margin-top: 5px;
			font-size: 11px;
		}
	</style>
</asp:Content>
