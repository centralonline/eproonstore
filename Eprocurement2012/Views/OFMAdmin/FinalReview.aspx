﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.FinalReview>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<div class="noPrint">
			<%Html.RenderPartial("CompanySettingMenu", Model.FinalCompany.CompanyId);%></div>
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<%Html.BeginForm("RequesterLine", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.FinalCompany.CompanyId)%>
			<p style="float: right;">
				<input type="submit" id="RequesterLineGuide" name="RequesterLineGuide.x" value="<< Previous" /></p>
			<%Html.EndForm(); %>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<%Html.BeginForm("AllSetting", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.FinalCompany.CompanyId)%>
			<p class="btn" style="float: left">
				<input type="submit" id="ViewAllSetting" name="ViewAllSetting.x" value="Next >>" />
				<%Html.EndForm(); %>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.FinalReview.Head")%></h2>
		<div class="grid_2 noPrint" style="width: 120px; border: 1px solid #E5E5E5; padding: 9px;
			border-radius: 5px 5px 5px 5px;">
			<a style="color: #4570B7; ext-decoration: none; font-weight: normal" href="<%=Url.Action("OFMAdminExportFinalReview", "ExportExcel", new { companyId = Model.FinalCompany.CompanyId })%>">
				<img src="/images/Excel-icon.png" alt="" title="" />
				<%=Html._("Print.ExportExcel")%></a>
		</div>
		<!--Print-->
		<div class="eclear">
		</div>
		<div class="grid_2 noPrint" style="width: 120px; margin-left: 180px; margin-top: -58px;
			border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
			padding: 5px; float: left">
			<img src="/images/theme/icon_printOrder.jpg" />
			<a style="color: #4570B7; text-decoration: none; font-weight: normal" href="#" onclick="window.print(); return false">
				<%=Html._("Order.ViewOrder.Print")%></a>
		</div>
		<br />
		<div class="eclear">
		</div>
		<div>
			<%Html.BeginForm("FinalReview", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.FinalCompany.CompanyId)%>
			<div>
				<%int count = 0;%>
				<table>
					<tr>
						<td colspan="2">
							<%=Html._("Admin.CompanyInfo.CompanyId")%>
							:<%=Model.FinalCompany.CompanyId%><br />
							<%=Html._("Admin.CompanyInfo.CompanyThaiName")%>:<%=Model.FinalCompany.CompanyThaiName%><br />
							<%=Html._("Admin.CompanyInfo.CompanyEngName")%>:<%=Model.FinalCompany.CompanyEngName%><br />
							<%=Html._("Admin.CompanyInfo.DefaultCustId")%>:<%=Model.FinalCompany.DefaultCustId%><br />
							<br />
						</td>
						<td style="padding-right: 500px;">
							<%=Html._("Admin.CompanyLogo")%><br />
							<img src="<%=Url.Action("CompanyLogoImage","OFMAdmin", new { companyId = Model.FinalCompany.CompanyId }) %>"
								alt="Company Logo" width="200px" /><br />
							<br />
						</td>
					</tr>
				</table>
			</div>
			<div>
				<table>
					<tr>
						<th>
							ลำดับ
						</th>
						<th>
							รหัสลูกค้า
						</th>
						<th>
							ที่อยู่ตามใบกำกับภาษี
						</th>
						<th>
							ที่อยู่สถานที่จัดส่ง
						</th>
					</tr>
					<%foreach (var item in Model.Invoice)
	   {%>
					<tr>
						<td>
							<%=++count%>
						</td>
						<td>
							<%=item.CustId%>
						</td>
						<td>
							<%=item.Address1%>
							<%=item.Address2%>
							<%=item.Address3%>
							<%=item.Address4%>
						</td>
						<td>
							<%foreach (var shipping in Model.FinalCompany.ListShipAddress.Where(s => s.CustId == item.CustId))
		 { %>
							<div style="width: 220px; padding: 10px; border: 3px solid gray; margin: 0px;">
								<%=shipping.Address1%>
								<%=shipping.Address2%>
								<%=shipping.Address3%>
								<%=shipping.Address4%>
							</div>
							<br />
							<%} %>
						</td>
					</tr>
					<%} %>
				</table>
			</div>
			<!--Department-->
			<div>
				<h5>
					<a href="<%=Url.Action("FinalReview", "OFMAdmin", new { name = "Department" ,companyId = Model.FinalCompany.CompanyId})%>"
						target="_blank"><span style="font-weight: bold; cursor: pointer;" id="Department">
							<%=Html._("Admin.Department.DeptInfo")%><img src="/images/icon/icon_2.png" alt="open" /></span>
					</a>
				</h5>
			</div>
			<div id="DisplayDepartment" style="display: none;">
			</div>
			<!--CostCenter-->
			<div>
				<h5>
					<a href="<%=Url.Action("FinalReview", "OFMAdmin", new { name = "CostCenter" ,companyId = Model.FinalCompany.CompanyId})%>"
						target="_blank"><span style="font-weight: bold; cursor: pointer;" id="CostCenter">
							<%=Html._("Admin.CostCenter.CostCenterInfo")%><img src="/images/icon/icon_2.png"
								alt="open" /></span> </a>
				</h5>
			</div>
			<div id="DisplayCostCenter" style="display: none;">
			</div>
			<!--UserData-->
			<div>
				<h5>
					<a href="<%=Url.Action("FinalReview", "OFMAdmin", new { name = "User" ,companyId = Model.FinalCompany.CompanyId})%>" target="_blank">
						<span style="font-weight: bold; cursor: pointer;" id="User">
							<%=Html._("Admin.FinalReview.UserData")%><img src="/images/icon/icon_2.png" alt="open" /></span>
					</a>
				</h5>
			</div>
			<div id="DisplayUser" style="display: none;">
			</div>
			<!--Requester-->
			<div>
				<h5>
					<a href="<%=Url.Action("FinalReview", "OFMAdmin", new { name = "RequesterLine" ,companyId = Model.FinalCompany.CompanyId})%>" target="_blank">
						<span style="font-weight: bold; cursor: pointer;" id="RequesterPermission">
							<%=Html._("Admin.FinalReview.RequesterPermission")%><img src="/images/icon/icon_2.png"
								alt="open" /></span> </a>
				</h5>
			</div>
			<div id="DisplayRequesterPermission" style="display: none;">
			</div>
			<%Html.EndForm(); %>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<%--<script type="text/javascript">
		$(function () {
			$(".complain-toggle").click(function () {
				$(this).parent().next().toggle();
			});


			var companyId = $("#companyId").val();

			$("#CostCenter").click(function () {
				GetAllCostCenterFinalReview(companyId);
			});

			$("#User").click(function () {
				GetAllUserFinalReview(companyId);
			});

			$("#RequesterPermission").click(function () {
				GetRequesterLineFinalReview(companyId);
			});

			$("#Department").click(function () {
				GetAllDepartmentFinalReview(companyId);
			});

		});

		function GetAllCostCenterFinalReview(companyId) {
			var isVisible = $("#DisplayCostCenter").is(":visible"); //เช็คว่า dev เป็น true หรือ false
			if (!isVisible) {
				$.ajax({
					type: "POST",
					url: '<%=Url.Action("FinalReview","OFMAdmin")%>',
					data: { companyId: companyId, name: "CostCenter" },
					success: function (data) {
						$("#DisplayCostCenter").html(data);
						$("#DisplayCostCenter").show();
					}
				});
			}
			else {
				$("#DisplayCostCenter").hide();
			}
		};


		function GetAllUserFinalReview(companyId) {
			var isVisible = $("#DisplayUser").is(":visible");
			if (!isVisible) {
				$.ajax({
					type: "POST",
					url: '<%=Url.Action("FinalReview","OFMAdmin")%>',
					data: { companyId: companyId, name: "User" },
					success: function (data) {
						$("#DisplayUser").html(data);
						$("#DisplayUser").show();
					}
				});
			}
			else {
				$("#DisplayUser").hide();
			}
		};

		function GetRequesterLineFinalReview(companyId) {
			var isVisible = $("#DisplayRequesterPermission").is(":visible");
			if (!isVisible) {
				$.ajax({
					type: "POST",
					url: '<%=Url.Action("FinalReview","OFMAdmin")%>',
					data: { companyId: companyId, name: "RequesterLine" },
					success: function (data) {
						$("#DisplayRequesterPermission").html(data);
						$("#DisplayRequesterPermission").show();
					}
				});
			}
			else {
				$("#DisplayRequesterPermission").hide();
			}
		};


		function GetAllDepartmentFinalReview(companyId) {
			var isVisible = $("#DisplayCostCenter").is(":visible");
			if (!isVisible) {
				$.ajax({
					type: "POST",
					url: '<%=Url.Action("FinalReview","OFMAdmin")%>',
					data: { companyId: companyId, name: "Department" },
					success: function (data) {
						$("#DisplayDepartment").html(data);
						$("#DisplayDepartment").show();
					}
				});
			}
			else {
				$("#DisplayDepartment").hide();
			}
		};


	</script>--%>
</asp:Content>
