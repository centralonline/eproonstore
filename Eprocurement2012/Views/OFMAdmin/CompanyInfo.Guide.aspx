﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
		<div class="grid_2" style="position:absolute;left:75%;margin-top:1%;">
			<%Html.BeginForm("CompanyHome", "OFMAdmin", FormMethod.Post); %>
			<p class="btn" style="float:right;">
				<input type="submit" id="Check_<%=Model.CompanyId%>" name="Check.<%=Model.CompanyId%>.x" value="<< Previous" /></p>
			<%Html.EndForm(); %>
		</div>
		<div class="grid_2" style="position:absolute;right:1%;margin-top:1%;">
			<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId) %>
			<p class="btn" style="float:left;">
				<input type="submit" id="Submit2" name="CompanyInfo.x" value="Next >>" /></p>
			<%Html.EndForm(); %>
		</div>
			<h2 id="page-heading">
				<%=Html._("Admin.CompanyInfo")%></h2>
		</div>
		<div class="grid_16">
			<label>
				เริ่มต้นการตั้งค่าข้อมูลองค์กร คุณสามารถตั้งค่าเบื้องต้นขององค์กรในการสั่งซื้อ</label><br />
			<img src="/images/Company_info/Company_sec02_1.jpg" alt="you are here" /><br />
		</div>
		<div style="min-height: 500px;">
		</div>
		<div class="grid_8">
		</div>
		<%--<div class="grid_8">
			<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId) %>
			<p class="btn" style="float: right">
				<input type="submit" id="Submit1" name="CompanyInfo.x" value="Continue" /></p>
			<%Html.EndForm(); %>
		</div>--%>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
