﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CompanyDepartment>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId) %>
				<p class="btn" style="float: right;">
					<input type="submit" id="ViewAllDepartment" name="ViewAllDepartment.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
				<%=Html.HiddenFor(m=> m.CompanyId)%>
				<p class="btn" style="float: left;">
					<input type="submit" id="CostCenterGuide" name="CostCenterGuide.x" value="Next >>" /></p>
				<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.Department.ContentModify")%></h2>
		</div>
		<div class="grid_16">
			<div class="grid_8">
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
				<%=Html.HiddenFor(m=> m.CompanyId)%>
				<p class="btn" style="float: right">
					<input type="submit" name="CreateNewDepartment.x" value="Create New Department" />
					<input type="submit" name="ViewAllDepartment.x" value="View All Department" /></p>
				<%Html.EndForm();%>
				<h5>
					<%=Html._("Admin.Department.InfoModify")%></h5>
				<div class="box" style="margin-top: 15px">
					<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
					<div class="table-format">
						<%=Html.HiddenFor(m=>m.CompanyId)%>
						<label>
							<%=Html._("Admin.Department.DeptID")%></label>
						<%=Model.DepartmentID%>
						<%=Html.HiddenFor(m=>m.DepartmentID)%>
						<span style="color: Red">
							<%=Html.LocalizedValidationMessageFor(m => m.DepartmentID)%></span>
					</div>
					<div class="table-format">
						<label>
							<%=Html._("Admin.Department.ThaiName")%></label>
						<%=Html.TextBoxFor(m=>m.DepartmentThaiName)%>
						<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m=>m.DepartmentThaiName)%></span>
					</div>
					<div class="table-format">
						<label>
							<%=Html._("Admin.Department.EngName")%></label>
						<%=Html.TextBoxFor(m=>m.DepartmentEngName)%>
						<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m=>m.DepartmentEngName)%></span>
					</div>
					<%if (Model.DepartmentStatus == Eprocurement2012.Models.CompanyDepartment.DeptStatus.Cancel)
	   { %>
					<div class="table-format">
						<label>
							<%=Html._("Admin.Department.StatusModify")%></label>
						<%=Html.DropDownListFor(m => m.DepartmentStatus, new SelectList(Model.ListDepartmentStatus, "Id", "Name"))%>
					</div>
					<%}
	   else
	   {%>
					<%=Html.HiddenFor(m=>m.DepartmentStatus) %>
					<%} %>
					<div style="padding-left: 176px; margin-bottom: 5px">
						<%=Html.HiddenFor(m=>m.CompanyId)%>
						<input type="submit" id="UpdateDepartment" name="UpdateDepartment.x" value="Save" />
					</div>
					<%Html.EndForm();%>
				</div>
			</div>
		</div>
		<div style="min-height: 500px;">
		</div>
		<%--<div class="grid_8">
			<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
			<%=Html.HiddenFor(m=> m.CompanyId)%>
			<p class="btn">
				<input type="submit" id="ViewAllDepartment" name="ViewAllDepartment.x" value="Back" /></p>
			<%Html.EndForm();%>
		</div>
		<div class="grid_8">
			<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
			<%=Html.HiddenFor(m=> m.CompanyId)%>
			<p class="btn" style="float: right">
				<input type="submit" id="CostCenterGuide" name="CostCenterGuide.x" value="Continue" /></p>
			<%Html.EndForm();%>
		</div>--%>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
