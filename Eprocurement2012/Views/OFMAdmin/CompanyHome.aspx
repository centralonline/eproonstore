﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<h2 id="page-heading">
				<%=Html._("Admin.CompanyHome")%></h2>
		</div>
		<div class="grid_8">
			<div class="block">
			<p>ตัวช่วยสร้างนี้คือ การกำหนดค่าข้อมูลต่างๆ ขององค์กรสำหรับผู้ดูแลระบบที่จะนำคุณไปสู่การกำหนดค่าต่างๆ
				และการปรับแต่งที่จำเป็นสำหรับระบบการทำงาน อาทิ ข้อมูลองค์กร ฝ่ายหรือหน่วยงานที่ทำการสั่งซื้อ
				รวมถึงการกำหนดสิทธิ์ให้กับ user ให้ทำงานตามสิทธิ์ที่กำหนดให้</p>
				<%--<p>
					Lorem Ipsum คือ เนื้อหาจำลองแบบเรียบๆ ที่ใช้กันในธุรกิจงานพิมพ์หรืองานเรียงพิมพ์
					มันได้กลายมาเป็นเนื้อหาจำลองมาตรฐานของธุรกิจดังกล่าวมาตั้งแต่ศตวรรษที่ 16 เมื่อเครื่องพิมพ์โนเนมเครื่องหนึ่งนำรางตัวพิมพ์มาสลับสับตำแหน่งตัวอักษรเพื่อทำหนังสือตัวอย่าง
					Lorem Ipsum อยู่ยงคงกระพันมาไม่ใช่แค่เพียงห้าศตวรรษ แต่อยู่มาจนถึงยุคที่พลิกโฉมเข้าสู่งานเรียงพิมพ์ด้วยวิธีทางอิเล็กทรอนิกส์
					และยังคงสภาพเดิมไว้อย่างไม่มีการเปลี่ยนแปลง มันได้รับความนิยมมากขึ้นในยุค ค.ศ. 1960
					เมื่อแผ่น Letraset วางจำหน่ายโดยมีข้อความบนนั้นเป็น Lorem Ipsum และล่าสุดกว่านั้น
					คือเมื่อซอฟท์แวร์การทำสื่อสิ่งพิมพ์ (Desktop Publishing) อย่าง Aldus PageMaker ได้รวมเอา
					Lorem Ipsum เวอร์ชั่นต่างๆ เข้าไว้ในซอฟท์แวร์ด้วย</p>--%>
			</div>
			<img src="/images/Company_info/Company_sec01.jpg" alt="you are here" /><br />
		</div>
		<div class="grid_4">
			<%--<p>
				คุณสามารถกำหนดข้อมูลองค์กร หน่วยงาน/แผนก ผู้ใช้งานในการสั่งซื้อ</p>--%>
				<p>คุณเริ่มต้นการกำหนดค่าต่างๆได้ที่นี่</p>
			<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId) %>
			<input class="btn" type="submit" id="CompanyGuide" name="CompanyGuide.x" value="Continue to Company Info" />
			<%Html.EndForm(); %>
		</div>
		<br />
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
