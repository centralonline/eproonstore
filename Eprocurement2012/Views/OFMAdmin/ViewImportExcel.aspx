﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<h2>ViewImportExcel</h2>
	<%Html.BeginForm("ImportExcel", "OFMAdmin", FormMethod.Post, new { enctype = "multipart/form-data" }); %>
	<%=Html.Hidden("companyId", Model.CompanyId)%>
		Upload File Here <input name="uploadFile" type="file" /> <input type="submit" value="UploadFile" title="Upload File" />
	<%Html.EndForm(); %>
	<%=TempData["Error"]%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
