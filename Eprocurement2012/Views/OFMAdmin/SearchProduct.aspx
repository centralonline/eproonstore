﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SearchProduct>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("OverViewMenuBar");%>
		<h2>
			<%=Html._("OFMAdmin.SearchUser.SpecifiedSearch")%></h2>
		<hr />
		<div>
			<%Html.BeginForm("SearchProduct", "OFMAdmin", FormMethod.Post); %>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCreateProduct, new { id = "CreateProduct" })%><label
							for="CreateProduct"><%=Html._("OFMAdmin.SearchProduct.CreateProduct")%></label></legend>
					<%=Html._("Admin.NewsCompany.Since")%>
					<%=Html.TextBox("fromDate")%>
					<%=Html._("Admin.NewsCompany.To")%>
					<%=Html.TextBox("toDate")%>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCompanyId, new { id = "CompanyId" })%><label for="CompanyId"><%=Html._("OFMAdmin.SearchProduct.CompanyId")%></label></legend>
					<%=Html.TextBoxFor(m => m.CompanyId)%>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckProductId, new { id = "ProductId" })%><label for="ProductId"><%=Html._("OFMAdmin.SearchProduct.ProductId")%></label></legend>
					<span>
						<%=Html._("จากรหัส")%><%=Html.TextBoxFor(m => m.ProductIdFrom)%></span> <span>
							<%=Html._("ถึง")%><%=Html.TextBoxFor(m => m.ProductIdTo)%></span>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCreateBy, new { id = "CreateBy" })%><label for="CreateBy"><%=Html._("OFMAdmin.SearchProduct.CreateByProduct")%></label></legend>
					<%=Html.TextBoxFor(m => m.CreateBy)%>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCatalogTypeId, new { id = "CatalogTypeId" })%><label
							for="CatalogTypeId"><%=Html._("OFMAdmin.SearchProduct.CatalogTypeId")%></label></legend>
					<%=Html.TextBoxFor(m => m.CatalogTypeId)%>
				</fieldset>
			</div>
			<div style="width: 100%; display: inline-block;">
				<input type="submit" value="Search" />
				<span style="color: Red;">
					<%=TempData["ErrorMessage"]%></span>
			</div>
			<%Html.EndForm(); %>
		</div>
		<hr />
		<%if (Model.SearchProductData != null && Model.SearchProductData.Any())
	{%>
		<div style="margin: 10px;">
			<div>
				<%Html.BeginForm("ExportSearchProduct", "ExportExcel", Model); %>
				<input id="submit" type="submit" value="Export to Excel" />
				<%Html.EndForm(); %>
			</div>

			<table id="sortBy">
				<thead>
					<tr>
						<th style="text-align: center;">
							<%=Html._("Sequence")%>
						</th>
						<th style="text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.CompanyId")%>
						</th>
						<th style="text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.ProductId")%>
						</th>
						<th style="width: 200px; text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.ThaiName")%>
						</th>
						<th style="width: 200px; text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.EngName")%>
						</th>
						<th style="text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.ThaiUnit")%>
						</th>
						<th style="text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.EngUnit")%>
						</th>
						<th style="width: 80px; text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.PriceExcVat")%>
						</th>
						<th style="width: 80px; text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.PriceIncVat")%>
						</th>
						<th style="text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.PriceType")%>
						</th>
						<th style="text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.IsBestDeal")%>
						</th>
						<th style="text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.IsVat")%>
						</th>
						<th style="width: 100px; text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.AddProdcut")%>
						</th>
						<th style="width: 60px; text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.PeriodOrder")%>
						</th>
						<th style="width: 60px; text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.CreateOn")%>
						</th>
						<th style="width: 100px; text-align: center;">
							<%=Html._("OFMAdmin.SearchProduct.CreateBy")%>
						</th>
					</tr>
				</thead>
				<%int count = 0;%>
				<%foreach (var product in Model.SearchProductData)
	  {%>
				<tr>
					<td>
						<%=++count%>
					</td>
					<td>
						<%=product.Company.CompanyId%>
					</td>
					<td>
						<%=product.ProductDetail.Id%>
					</td>
					<td>
						<%=product.ProductDetail.ThaiName%>
					</td>
					<td>
						<%=product.ProductDetail.EngName%>
					</td>
					<td style="text-align: center;">
						<%=product.ProductDetail.ThaiUnit%>
					</td>
					<td style="text-align: center;">
						<%=product.ProductDetail.EngUnit%>
					</td>
					<td style="text-align: right;">
						<%=product.ProductDetail.PriceExcVat.ToString(new MoneyFormat())%>
					</td>
					<td style="text-align: right;">
						<%=product.ProductDetail.PriceIncVat.ToString(new MoneyFormat())%>
					</td>
					<td style="text-align: center;">
						<%=product.ProductDetail.PriceType%>
					</td>
					<%if (product.ProductDetail.IsBestDeal)
	   {%>
					<td style="text-align: center;">
						<%=Html._("Yes")%>
					</td>
					<%} %>
					<%else
	   {%>
					<td style="text-align: center;">
						<%=Html._("No")%>
					</td>
					<%} %>
					<%if (product.ProductDetail.IsVat)
	   {%>
					<td style="text-align: center;">
						<%=Html._("Yes")%>
					</td>
					<%} %>
					<%else
	   {%>
					<td style="text-align: center;">
						<%=Html._("No")%>
					</td>
					<%} %>
					<%--<td><%=product.ProductDetail.IsBestDeal%></td>
					<td><%=product.ProductDetail.IsVat%></td>--%>
					<td>
						<%=product.CatalogTypeId%>
					</td>
					<td>
						<%=product.ProductDetail.ValidFrom.ToString(new DateFormat()) + "-" + product.ProductDetail.ValidTo.ToString(new DateFormat())%>
					</td>
					<td>
						<%=product.CreateOn.ToString(new DateFormat())%>
					</td>
					<td>
						<%=product.CreateBy%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script src="/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<link href="/css/dataTables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(function () {
			$("#fromDate").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});
			$("#toDate").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			var fuzzyNum = function (x) {
				return +x.replace(/[^\d\.\-]/g, "");
			};

			jQuery.fn.dataTableExt.oSort['prettynumbers-asc'] = function (x, y) {
				return fuzzyNum(x) - fuzzyNum(y);
			};
			jQuery.fn.dataTableExt.oSort['prettynumbers-desc'] = function (x, y) {
				return fuzzyNum(y) - fuzzyNum(x);
			};
			$('#sortBy').dataTable({
				"aoColumns": [
									null, null, null, null, null, null, null, { "sType": "prettynumbers" }, { "sType": "prettynumbers" }, null, null, null,
									null, null, null, null
							]
			});

		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
