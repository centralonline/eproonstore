﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.CostCenter>>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.Value.First().CompanyId);%>
		<h2 id="page-heading">
			<%=Html._("Admin.CostCenter.ContentInfo")%></h2>
		<div>
			<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
			<div style="float: right">
				<input type="submit" id="CreateNewCostCenter" name="CreateNewCostCenter.x" value="<%=Html._("Admin.CostCenter.Add")%>" />
				<%=Html.Hidden("companyId",Model.Value.First().CompanyId)%>
			</div>
			<%Html.EndForm();%>
			<%Html.BeginForm("RequesterLine", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.Value.First().CompanyId)%>
			<div>
				<table>
					<tr>
						<th>
							<%=Html._("Admin.CostCenter.DepartmentID")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.CostCenterID")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.ThaiName")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.EngName")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.Status")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.Modify")%>
						</th>
						<th>
							<%=Html._("Admin.Department.Cancel")%>
						</th>
						<th>
							<%=Html._("Admin.CompanySetting.HeadUser")%>
						</th>
					</tr>
					<%foreach (var item in Model.Value)%>
					<%{%>
					<tr>
						<td>
							<%=item.CostCenterDepartment.DepartmentID%>
						</td>
						<td>
							<%=item.CostCenterID%>
						</td>
						<td>
							<%=item.CostCenterThaiName%>
						</td>
						<td>
							<%=item.CostCenterEngName%>
						</td>
						<td>
							<%--<%=item.CostStatus%>--%>
							<%=item.DisplayCostCenterStatus%>
						</td>
						<td>
							<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
							<input type="submit" id="EditCostCenter_<%=item.CostCenterID %>" value="<%=Html._("Admin.CostCenter.Edit")%>"
								name="EditCostCenter.<%=item.CostCenterID %>.x" />
							<%=Html.Hidden("companyId", Model.Value.First().CompanyId)%>
							<%Html.EndForm();%>
						</td>
						<td>
							<%if (item.CostStatus == Eprocurement2012.Models.CostCenter.CostCenterStatus.Active)
		 {%>
							<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
							<input type="submit" id="RemoveCostCenter_<%=item.CostCenterID %>" value="Cancel"
								name="RemoveCostCenter.<%=item.CostCenterID %>.x" />
							<%=Html.Hidden("companyId", Model.Value.First().CompanyId)%>
							<%Html.EndForm();%>
							<%} %>
						</td>
						<td>
							<input type="submit" id="EditReqLine_<%=item.CostCenterID%>" value="<%=Html._("Admin.CompanySetting.User")%>"
								name="EditReqLine.<%=item.CostCenterID%>.x" />
						</td>
					</tr>
					<%} %>
				</table>
			</div>
			<div>
				<input type="submit" id="RequesterLineGuide" name="RequesterLineGuide.x" value="Back" />
			</div>
			<%Html.EndForm(); %>
		</div>
		<br />
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
