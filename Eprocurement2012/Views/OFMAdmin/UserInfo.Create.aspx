﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CreateNewUser>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		.content
		{
			overflow: auto;
			width: 100%;
		}
		.left
		{
			float: left;
		}
		label .addWidth
		{
			width: 350px;
		}
		.right
		{
			float: right;
			width: 82%;
		}
	</style>
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.DefaultCompanyId);%>
		<div class="grid_16">
			<h2 id="page-heading">
				<%=Html._("OFMAdmin.UserInfo.Create")%></h2>
			<br />
			<div>
				<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.DefaultCompanyId)%>
				<%=Html.HiddenFor(m => m.DefaultCompanyId)%>
				<%=Html.HiddenFor(m => m.DefaultCustId) %>
				<div class="left">
					<label class="addWidth">
						<%=Html._("CreateUser.UserId")%>:</label>
				</div>
				<div class="right">
					<%=Html.TextBoxFor(m => m.Email, new { maxlength=100})%><span style="color: red;">*</span><br />
					<span style="color: red;">
						<%=Html.LocalizedValidationMessageFor(m => m.Email)%></span>
					<label>
						<%=Html._("CreateUser.DetailUserId")%></label>
				</div>
				<div class="left">
					<p>
					</p>
				</div>
				<div class="right">
					<%=Html.CheckBoxFor(m => m.IsInitialPassword, new { id = "IsInitialPassword" })%>
					<label for="IsInitialPassword">
						<%=Html._("CreateUser.InitializePassword")%></label>
				</div>
				<div class="right">
					<%=Html.PasswordFor(m => m.Password, new { id = "Password", maxlength = 60 })%>
					<label>
						<%=Html._("CreateUser.DetailPassword")%></label>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.Password")%>:</label>
				</div>
				<div class="right">
					<%=Html.PasswordFor(m => m.RePassword, new { id = "RePassword", maxlength = 100 })%><br />
					<span style="color: red;">
						<%=Html.LocalizedValidationMessageFor(m => m.Password)%></span>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.RePassword")%>:</label>
				</div>
				<div class="right">
					<%=Html.TextBoxFor(m => m.ThaiName,new{maxlength = 50}) %><span style="color: red;">*</span><br />
					<span style="color: red;">
						<%=Html.LocalizedValidationMessageFor(m => m.ThaiName)%></span>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.ThaiName")%>:</label>
				</div>
				<div class="right">
					<%=Html.TextBoxFor(m => m.EngName, new { maxlength = 50 })%><span style="color: red;">*</span><br />
					<span style="color: red;">
						<%=Html.LocalizedValidationMessageFor(m => m.EngName)%></span>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.EngName")%>:</label>
				</div>
				<div class="right">
					<%=Html.TextBoxFor(m => m.Phone, new { maxlength = 10, @class = "number" })%><span
						style="color: red;">*</span>
					<label>
						<%=Html._("CreateUser.PhoneExt")%></label>
					<%=Html.TextBoxFor(m => m.PhoneExt, new { maxlength = 5, @class = "number" })%><br />
					<span style="color: red;">
						<%=Html.LocalizedValidationMessageFor(m => m.Phone)%>
						<%=Html.LocalizedValidationMessageFor(m => m.PhoneExt)%>
					</span>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.Phone")%>:</label>
				</div>
				<div class="right">
					<%=Html.TextBoxFor(m => m.Mobile, new { maxlength = 10, @class = "number" })%><%--<span style="color: red;">*</span><br />--%>
					<%--<span style="color: red;">
						<%=Html.LocalizedValidationMessageFor(m => m.Mobile)%>--%>
					</span>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.Mobile")%>:</label>
				</div>
				<div class="right">
					<%=Html.TextBoxFor(m => m.Fax, new { maxlength = 10, @class = "number" })%><br />
					<span style="color: red;">
						<%=Html.LocalizedValidationMessageFor(m => m.Fax)%>
					</span>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.Fax")%>:</label>
				</div>
				<div class="right">
					<%=Html.CheckBoxFor(m => m.IsRequester, new { id = "IsRequester" })%>
					<label for="IsRequester">
						<%=Html._("CreateUser.RoleRequester")%></label>
					<%=Html.CheckBoxFor(m => m.IsApprover, new { id = "IsApprover" })%>
					<label for="IsApprover">
						<%=Html._("CreateUser.RoleApprover")%></label>
					<%=Html.CheckBoxFor(m => m.IsAssistantAdmin, new { id = "IsAssistantAdmin" })%>
					<label for="IsAssistantAdmin">
						<%=Html._("CreateUser.RoleAssistantAdmin")%></label>
					<%=Html.CheckBoxFor(m => m.IsObserve, new { id = "IsObserve" })%>
					<label for="IsObserve">
						<%=Html._("CreateUser.RoleObserve")%></label><br />
					<span style="color: red;">
						<%=Html.LocalizedValidationMessageFor(m => m.IsApprover)%></span>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.Role")%>:</label>
				</div>
				<div class="right">
					<%=Html.RadioButtonFor(m => m.IsThaiLanguage, true, new { id = "THLang", @checked = "checked" })%>
					<label for="THLang">
						<%=Html._("CreateUser.ThaiLanguage")%></label>
					<%=Html.RadioButtonFor(m => m.IsThaiLanguage, false, new { id = "ENLang" })%>
					<label for="ENLang">
						<%=Html._("CreateUser.EngLanguage")%></label>
				</div>
				<div class="left">
					<label>
						<%=Html._("CreateUser.Language")%>:</label>
				</div>
<%--				<div class="right">
					<%=Html.CheckBoxFor(m => m.SetVerifyNow, new { id = "SetVerifyNow" })%>
					<label for="SetVerifyNow">
						<%=Html._("CreateUser.VerifyNow")%>
					</label>
				</div>--%>
				<%if (TempData["ErrorOFMSystem"] != null)
				{%>
				<div class="right" style="margin-top: 20px;">
					<%=Html.CheckBoxFor(o => o.IsUserOFM, new { id = "IsUserOFM" })%>
					<span style="color: Red">
						<%=TempData["ErrorOFMSystem"]%></span>
				</div>
				<%} %>
				<div class="right">
					<input type="submit" id="SaveNewUser" name="SaveNewUser.x" value="<%=Html._("Button.Create")%>" />
				</div>
				<div class="right" style="margin-top: 1%">
					<div style="width: 40%; height: auto;">
						<img src="/images/icon/help_content.png" />
						เมื่อยืนยันการสร้างข้อมูลผู้ใช้งาน ระบบจะส่งอีเมล์แจ้งกับผู้ใช้งาน ตามรหัสผู้ใช้งาน(อีเมล์)
						ที่ผู้ดูแลระบบสร้างขึ้น เพื่อยืนยันตัวตน และต้องเปลี่ยนรหัสผ่านก่อนเข้าใช้งานระบบ</div>
				</div>
				<%Html.EndForm(); %>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			if ($("#IsInitialPassword").attr("checked")) {
				$("#Password").attr("disabled", "disabled");
				$("#RePassword").attr("disabled", "disabled");
			}
			$("#IsInitialPassword").click(function () {
				if ($("#IsInitialPassword").attr("checked")) {
					$("#Password").attr("disabled", "disabled");
					$("#RePassword").attr("disabled", "disabled");
				}
				else {
					$("#Password").removeAttr("disabled");
					$("#RePassword").removeAttr("disabled");
				}
			});
			$('#IsAssistantAdmin').click(function () {
				if ($('#IsAssistantAdmin').attr('checked')) {
					$("#IsObserve").attr("disabled", "disabled");
				}
				else {
					$("#IsObserve").removeAttr("disabled");
				}
			});
			$('#IsObserve').click(function () {
				if ($('#IsObserve').attr('checked')) {
					$("#IsAssistantAdmin").attr("disabled", "disabled");
				}
				else {
					$("#IsAssistantAdmin").removeAttr("disabled");
				}
			});

			$(".number").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
						event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
						(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});
		});
	</script>
</asp:Content>
