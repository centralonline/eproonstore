﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId) %><br />
			<div style="text-align: center;">
				<img src="/images/step02.png" alt="" /></div>
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<p class="btn" style="float: right;">
					<input type="submit" id="CompanyInfo" name="CompanyInfo.x" value="<< Previous" /></p>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<p class="btn" style="float: left;">
					<input type="submit" id="CompanySetting" name="CompanySetting.x" value="Next >>" /></p>
			</div>
			<%Html.EndForm(); %>
			<h2 id="page-heading">
				<%=Html._("Admin.CompanyLogo")%></h2>
		</div>
		<div class="grid_16">
			<div class="grid_2">
				<label>
					<%=Html._("Admin.CompanyLogo.Example")%></label>
				<img src="/images/tip.jpg" alt="tip" /><br />
			</div>
			<div class="grid_14">
				<p>
					<%=Html._("Admin.CompanyLogo.Content")%></p>
				<br />
				<img src="<%=Url.Action("CompanyLogoImage","OFMAdmin", new { companyId = Model.CompanyId }) %>" alt="Company Logo" width="200px" />
				<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post, new { enctype = "multipart/form-data" }); %>
				<br />
				<div>
					<label>
						<%=Html._("Admin.CompanyLogo.Upload")%></label>
				</div>
				<div>
					<%=Html.HiddenFor(m => m.CompanyId) %>
					<%=Html.TextBoxFor(m => m.CompanyLogo, new { type="file"})%>
					<input type="submit" id="UploadLogo" value="Upload" name="UploadLogo.x" />
					<span style="color: Red;">
						<%=TempData["errormessage"]%></span>
				</div>
				<div>
					<p class="f11 colorgray">
						<i>
							<%=Html._("Admin.CompanyLogo.FileExtension")%></i></p>
				</div>
			</div>
		</div>
		<div style="min-height: 500px;">
		</div>
<%--		<div class="grid_8">
			<p class="btn">
				<input type="submit" id="CompanyInfo" value="Back" name="CompanyInfo.x" /></p>
		</div>
		<div class="grid_8">
			<p class="btn" style="float: right">
				<input type="submit" id="CompanySetting" value="Continue" name="CompanySetting.x" /></p>
		</div>--%>
		<%Html.EndForm(); %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
