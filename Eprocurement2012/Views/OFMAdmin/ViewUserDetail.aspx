﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.EditProfileUser>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div>
<%Html.RenderPartial("CompanySettingMenu", Model.CurrentCompanyId);%>
<div class="grid_16">
		<h2 id="page-heading">
			<img src="../../images/theme/h3-list-icon.png" alt="myprofile" /><%=Html._("Menu.Home.MyProfile")%></h2>
	</div>
	<div class="grid_16">
		<div class="table-format">
			<div id="profile-pic-preview" style="text-align: center; margin-right: 20px; float: left;">
				<img src="<%=Url.Action("ProfileImage","UserProfile", new { Model.UserGuid }) %>"
					alt="<%=Model.User.DisplayName%>" width="100" height="100" /><br />
				<br />
			</div>
		</div>
		<div class="grid_8">
		<div class="table-format">
			<label>
				<%=Html._("CreateUser.UserId")%>:</label>
			<%=Model.Email%><br />
		</div>
		<div class="table-format">
			<label>
				<%=Html._("CreateUser.ThaiName")%>:</label>
				<%=Model.ThaiName %><br />
		</div>
		<div class="table-format">
			<label>
				<%=Html._("CreateUser.EngName")%>:</label>
			<%=Model.EngName%><br />
		</div>
		<div class="table-format">
			<label>
				<%=Html._("CreateUser.Phone")%>:</label>
			<%=Model.Phone%>
			<span>
				<%=Html._("CreateUser.PhoneExt")%>:</span>
			<%=Model.PhoneExt%><br />
		</div>
		<div class="table-format">
			<label>
				<%=Html._("CreateUser.Mobile")%>:</label>
			<%=Model.Mobile%><br />
		</div>
		<div class="table-format">
			<label>
				<%=Html._("CreateUser.Fax")%>:</label>
			<%=Model.Fax%><br />
		</div>
		<div class="table-format">
			<label>
				<%=Html._("CreateUser.Role")%>:</label>
			<%=Model.IsRequester == true ? Html._("CreateUser.RoleRequester") + " " : ""%>
			<%=Model.IsApprover == true ? Html._("CreateUser.RoleApprover") + " " : ""%> 
			<%=Model.IsAssistantAdmin == true ? Html._("CreateUser.RoleAssistantAdmin") + " " : ""%>
			<%=Model.IsObserve == true ? Html._("CreateUser.RoleObserve") + " " : ""%><br />
		</div>
		<div class="table-format">
			<label>
				<%=Html._("CreateUser.Language")%>:</label>
			<%=Model.DefaultLang == "TH"? Html._("CreateUser.ThaiLanguage") : Html._("CreateUser.EngLanguage")%><br />
		</div>
		<div class="table-format">
			<label>
				<%=Html._("Admin.CostCenter.StatusModify")%>
			</label>
			<%=Model.Status == Eprocurement2012.Models.User.UserStatus.Active ? Html._("Admin.ListUserStatus.Active") : Html._("Admin.ListUserStatus.Cancel")%>
		</div>
		</div>
	</div>
	<!--Lin's min-height-->
		<div class="grid_8">
			<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CurrentCompanyId)%>
			<p class="btn">
				<input type="submit" id="ViewAllUser" name="ViewAllUser.x" value="Back" /></p>
			<%Html.EndForm(); %>
		</div>
</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
