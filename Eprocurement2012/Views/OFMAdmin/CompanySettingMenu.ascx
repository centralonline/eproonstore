﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.SubMenuOFMAdmin" %>
<div>
	<ul class="nav subnav">
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "CompanyHome")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.Home"), "CompanyHome", "OfficemateAdmin", new { companyId = Model }, null)%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "CompanyInfo")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.CompanyInfo"), "CompanyInfo", "OfficemateAdmin", new { companyId = Model }, null)%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "Department")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.Department"), "Department", "OfficemateAdmin", new { companyId = Model }, null)%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "CostCenter", "ManageShipping")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.CostCenter"), "CostCenter", "OfficemateAdmin", new { companyId = Model }, null)%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "UserInfo", "RequesterLineForUser", "ViewUserDetail", "SetVerifyUser")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.UserInfo"), "UserInfo", "OfficemateAdmin", new { companyId = Model }, null)%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "RequesterLine")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.RequesterLine"), "RequesterLine", "OfficemateAdmin", new { companyId = Model }, null)%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "FinalReview")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.FinalReview"), "FinalReview", "OfficemateAdmin", new { companyId = Model }, null)%></li>
		<li class="<%=GetTextIfCurrentActionIsInArray("active", "AllSetting")%>">
			<%=Html.ActionLink(Html._("Admin.CompanySettingMenu.AllSetting"), "AllSetting", "OfficemateAdmin", new { companyId = Model }, null)%></li>
	</ul>
</div>
<style type="text/css">
	ul.subnav li.active a
	{
		background-color: #e17c00;
	}
	ul.nav li a.show-sub-menu
	{
		color: #fff;
		background: #fd8c00;
		cursor: default;
		font-weight: bold;
		border-top: solid 1px #e88000
	}
</style>
