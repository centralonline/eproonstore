﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.EditProfileUser>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.RenderPartial("CompanySettingMenu", Model.CurrentCompanyId);%>
	<div class="grid_16">
		<h2 id="page-heading">
			<%=Html._("Shared.DataListUser.EditUser")%>
		</h2>
		<div>
			<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CurrentCompanyId)%>
			<div class="table-format2">
				<label>
					<%=Html._("CreateUser.UserId")%>:</label>
				<%=Model.Email%>
				<%=Html.HiddenFor(m => m.Email) %><%=Html.HiddenFor(m => m.SequenceId) %><%=Html.HiddenFor(m => m.UserGuid) %><%=Html.HiddenFor(m => m.CustId) %>
			</div>
			<div class="table-format2">
				<label>
					<%=Html._("CreateUser.ThaiName")%>:</label>
				<%=Html.TextBoxFor(m => m.ThaiName) %>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(m => m.ThaiName)%></span>
			</div>
			<div class="table-format2">
				<label>
					<%=Html._("CreateUser.EngName")%>:</label>
				<%=Html.TextBoxFor(m => m.EngName) %>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(m => m.EngName)%></span>
			</div>
			<div class="table-format2">
				<label>
					<%=Html._("CreateUser.Phone")%>:</label>
				<%=Html.TextBoxFor(m => m.Phone, new { maxlength = "10", @class = "number" })%>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(m => m.Phone)%></span>
				<%=Html.HiddenFor(m => m.PhoneId) %><%=Html._("CreateUser.PhoneExt")%>:
				<%=Html.TextBoxFor(m => m.PhoneExt, new { maxlength = "5", @class = "number" })%>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(m => m.PhoneExt)%></span>
			</div>
			<div class="table-format2">
				<label>
					<%=Html._("CreateUser.Mobile")%>:</label>
				<%=Html.TextBoxFor(m => m.Mobile, new { maxlength = "10", @class = "number" })%>
				<%=Html.HiddenFor(m => m.MobileId) %>
				<span style="color: red;">
					<%=Html.LocalizedValidationMessageFor(m => m.Mobile)%>
				</span>
			</div>
			<div class="table-format2">
				<label>
					<%=Html._("CreateUser.Fax")%>:</label>
				<%=Html.TextBoxFor(m => m.Fax, new { maxlength = "10", @class = "number" })%>
				<%=Html.HiddenFor(m => m.FaxId) %>
				<span style="color: red;">
					<%=Html.LocalizedValidationMessageFor(m => m.Fax)%>
				</span>
			</div>
			<div>
				<label style="float: left; text-align: right; width: 150px;">
					<%=Html._("CreateUser.Role")%>:</label>
				<span style="margin-left: 40px;">
					<%=Html.CheckBoxFor(m => m.IsRequester, new { id = "IsRequester" })%>
					<label for="IsRequester">
						<%=Html._("CreateUser.RoleRequester")%></label>
					<%=Html.CheckBoxFor(m => m.IsApprover, new { id = "IsApprover" })%>
					<label for="IsApprover">
						<%=Html._("CreateUser.RoleApprover")%></label>
					<% if (!Model.IsRootAdmin)
		{ %>
					<%=Html.CheckBoxFor(m => m.IsAssistantAdmin, new { id = "IsAssistantAdmin" })%>
					<label for="IsAssistantAdmin">
						<%=Html._("CreateUser.RoleAssistantAdmin")%></label>
					<%=Html.CheckBoxFor(m => m.IsObserve, new { id = "IsObserve" })%>
					<label for="IsObserve">
						<%=Html._("CreateUser.RoleObserve")%></label>
					<% } %>
					<%=Html.LocalizedValidationMessageFor(m => m.IsApprover)%>
				</span>
			</div>
			<div>
				<label style="float: left; text-align: right; width: 150px;">
					<%=Html._("CreateUser.Language")%>:</label>
				<span style="margin-left: 40px;">
					<%=Html.RadioButtonFor(m => m.DefaultLang, "TH", new { id = "THLang" })%>
					<label for="THLang">
						<%=Html._("CreateUser.ThaiLanguage")%></label>
					<%=Html.RadioButtonFor(m => m.DefaultLang, "EN", new { id = "ENLang" })%>
					<label for="ENLang">
						<%=Html._("CreateUser.EngLanguage")%></label>
				</span>
			</div>
			<div>
				<label style="float: left; text-align: right; width: 150px;">
					<%=Html._("Admin.CostCenter.StatusModify")%>
				</label>
				<span style="margin-left: 40px;">
					<%=Html.DropDownListFor(m => m.Status,new SelectList(Model.ListUserStatus,"Id","Name"))%>
				</span>
			</div>
			<div style="margin-left: 190px; margin-top: 20px;">
				<input type="submit" id="UpdateUser" name="UpdateUser.x" value="<%=Html._("Button.Save")%>" />
				<span style="color: Red;">
					<%=TempData["EditUserComplete"]%></span> <span style="color: Red;">
						<%=TempData["EditUserCancel"]%></span>
			</div>
			<%Html.EndForm(); %>
		</div>
	</div>
	<div style="min-height: 500px;">
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".number").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
						event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
						(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});

			if ($("#IsObserve").attr('checked')) {
				$('#IsRequester').attr('checked', false);
				$('#IsApprover').attr('checked', false);
				$('#IsAssistantAdmin').attr('checked', false);
				$('#IsRequester').attr('disabled', true);
				$('#IsApprover').attr('disabled', true);
				$('#IsAssistantAdmin').attr('disabled', true);
			}
			else {
				$('#IsRequester').attr('disabled', false);
				$('#IsApprover').attr('disabled', false);
				$('#IsAssistantAdmin').attr('disabled', false);
			}

			$('#IsObserve').click(function () {
				if ($("#IsObserve").attr('checked')) {
					$('#IsRequester').attr('checked', false);
					$('#IsApprover').attr('checked', false);
					$('#IsAssistantAdmin').attr('checked', false);
					$('#IsRequester').attr('disabled', true);
					$('#IsApprover').attr('disabled', true);
					$('#IsAssistantAdmin').attr('disabled', true);
				}
				else {
					$('#IsRequester').attr('disabled', false);
					$('#IsApprover').attr('disabled', false);
					$('#IsAssistantAdmin').attr('disabled', false);
				}
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.table-format2 label
		{
			float: left; /*line-height: 2em;*/
			margin-right: 3%;
			text-align: right;
			width: 150px;
		}
	</style>
</asp:Content>
