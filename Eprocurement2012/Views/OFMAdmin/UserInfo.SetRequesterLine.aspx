﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SetRequesterLine>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<%Html.BeginForm("RequesterLineForUser", "OFMAdmin", FormMethod.Post); %>
			<h2 id="page-heading">
				<%=Html._("Admin.SetRequesterLine")%></h2>
			<fieldset>
				<legend>
					<%=Html._("OFMAdmin.UserInfo.AllUser")%></legend>
				<p>
					<%=Html._("Shared.DataUserProfile.UserId")%>:
					<%=Model.UserReqDetail.UserId %>
					<%=Html.Hidden("userGuid", Model.UserReqDetail.UserGuid)%>
					<%=Html.Hidden("companyId", Model.CompanyId)%>
				</p>
				<p>
					<%=Html._("Shared.DataUserProfile.DisplayName")%>:
					<%=Model.UserReqDetail.DisplayName %>
				</p>
			</fieldset>
			<div class="content">
				<div style="float: left; width: 25%">
					<label>
						CostCenter:</label>
					<input type="text" id="tags" style="width: 250px;" />
					<input type="button" id="AddCostCenter" name="AddCostCenter" value="Add" />
					<div id="DisplayCostcenter">
						<%if (TempData["costcenterId"] != null)
		{%>
						<%foreach (var costcenterId in (IEnumerable<string[,]>)TempData["costcenterId"])
		{%>
						<input id="<%=costcenterId[0,0]%>" class="chk" type="checkbox" value="<%=costcenterId[0,0]%>"
							checked="checked" name="costcenterId" />
						<label for="<%=costcenterId[0,0]%>">
							[<%=costcenterId[0, 0]%>]
							<%=costcenterId[0, 1]%>
						</label>
						<br />
						<input id="costName_<%=costcenterId[0, 0]%>" type="hidden" name="costcenterName"
							value="<%=costcenterId[0,1]%>" />
						<%} %>
						<%} %>
					</div>
				</div>
				<%--				<%if (Model.ListCostCenter != null)
				{ %>
				<div style="float: left; width: 25%">
					<h3>
						<%=Html._("Admin.CostCenterInfo")%></h3>
					<hr />
					<%foreach (var item in Model.ListCostCenter)
					{
						string reqCheck = "";
						string reqEnable = "";
						string reqClass = "style='color: #0000FF'";
						if (Model.CostCurrentPermission.Any(c => c.CostCenterID == item.CostCenterID))
						{
							reqCheck = "checked='checked'";
							reqEnable = "disabled='disabled'";
							reqClass = "";
						}
						if ((Model.SelectCostcenter ?? new string[] { }).Any(o => o == item.CostCenterID))
						{
							reqCheck = "checked='checked'";
						}%>
					<input id="<%=item.CostCenterID%>" name="costcenterId" type="checkbox" <%=reqEnable %> value="<%=item.CostCenterID%>" <%=reqCheck%> />
					<label for="<%=item.CostCenterID%>" <%=reqClass%>>[<%=item.CostCenterID%>]<%=item.CostCenterName%></label>
					<br />
					<%} %>
				</div>
				<%}%>--%>
				<% if (Model.UserApprover != null)
	   { %>
				<div style="float: right; width: 75%">
					<h3>
						<%=Html._("SetRequesterLine.Approver")%></h3>
					<hr />
					<div>
						<%=Html._("SetRequesterLine.ApproverLevel1")%>
						<select name="userApprover">
							<option value="">
								<%=Html._("Admin.CostCenter.SelectList")%></option>
							<%foreach (var app in Model.UserApprover)
		 {
			 string select = "";
			 if (Model.SelectApprover[0] != null && Model.SelectApprover[0] == app.UserId) { select = "selected='selected'"; }%>
							<option value="<%=app.UserId %>" <%=select %>>
								<%=app.DisplayNameAndEmail %></option>
							<%} %>
						</select>
						<%=Html._("SetRequesterLine.ApprovalBudget")%>
						<input type="text" name="approverBudget" value="<%=Model.SelectBudget[0] == null ? 0 : Model.SelectBudget[0]%>"
							id="UserApprover_First_Budget" maxlength="7" />
						<%=Html._("SetRequesterLine.ApprovalPeriod")%>
						<select name="parkday">
							<%foreach (var item in Model.ListParkDay)
		 {
			 string select = "";
			 if (Model.SelectParkday[0] != null && Model.SelectParkday[0] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
							<option value="<%=item.Id %>" <%=select %>>
								<%=item.Name%></option>
							<%} %>
						</select>
						<%=Html._("Admin.SetRequsterLine.Day")%>
					</div>
					<div>
						<%=Html._("SetRequesterLine.ApproverLevel2")%>
						<select name="userApprover">
							<option value="">
								<%=Html._("Admin.CostCenter.SelectList")%></option>
							<%foreach (var app in Model.UserApprover)
		 {
			 string select = "";
			 if (Model.SelectApprover[1] != null && Model.SelectApprover[1] == app.UserId) { select = "selected='selected'"; }%>
							<option value="<%=app.UserId %>" <%=select %>>
								<%=app.DisplayNameAndEmail %></option>
							<%} %>
						</select>
						<%=Html._("SetRequesterLine.ApprovalBudget")%>
						<input type="text" name="approverBudget" value="<%=Model.SelectBudget[1] == null ? 0 : Model.SelectBudget[1]%>"
							id="UserApprover_Second_Budget" maxlength="7" />
						<%=Html._("SetRequesterLine.ApprovalPeriod")%>
						<select name="parkday">
							<%foreach (var item in Model.ListParkDay)
		 {
			 string select = "";
			 if (Model.SelectParkday[1] != null && Model.SelectParkday[1] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
							<option value="<%=item.Id %>" <%=select %>>
								<%=item.Name%></option>
							<%} %>
						</select>
						<%=Html._("Admin.SetRequsterLine.Day")%>
					</div>
					<div>
						<%=Html._("SetRequesterLine.ApproverLevel3")%>
						<select name="userApprover">
							<option value="">
								<%=Html._("Admin.CostCenter.SelectList")%></option>
							<%foreach (var app in Model.UserApprover)
		 {
			 string select = "";
			 if (Model.SelectApprover[2] != null && Model.SelectApprover[2] == app.UserId) { select = "selected='selected'"; }%>
							<option value="<%=app.UserId %>" <%=select %>>
								<%=app.DisplayNameAndEmail %></option>
							<%} %>
						</select>
						<%=Html._("SetRequesterLine.ApprovalBudget")%>
						<input type="text" name="approverBudget" value="<%=Model.SelectBudget[2] == null ? 0 : Model.SelectBudget[2]%>"
							id="UserApprover_Third_Budget" maxlength="7" />
						<%=Html._("SetRequesterLine.ApprovalPeriod")%>
						<select name="parkday">
							<%foreach (var item in Model.ListParkDay)
		 {
			 string select = "";
			 if (Model.SelectParkday[2] != null && Model.SelectParkday[2] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
							<option value="<%=item.Id %>" <%=select %>>
								<%=item.Name%></option>
							<%} %>
						</select>
						<%=Html._("Admin.SetRequsterLine.Day")%>
					</div>
					<%} %>
					<input id="SetRequesterLine" type="submit" value="<%=Html._("Button.Save")%>" name="SetUserReq.x" />
					<span style="color: Red">
						<%=TempData["Error"]%></span>
				</div>
			</div>
			<br />
			<hr />
			<div style="width: 100%;">
				<%if (Model.InformationRequesterLine.Any())
	  { %>
				<h3>
					<%=Html._("SetRequesterLine.RoleRequesterAndApprovals")%></h3>
				<hr />
				<%foreach (var item in Model.InformationRequesterLine.GroupBy(o => o.CostcenterID))
	  { %>
				<div style="width: 70%; border-width: thin; background-color: #EEEEEE; border-style: ridge">
					<table>
						<thead>
							<tr>
								<th style="width: 20%;">
									<%=Html._("SetRequesterLine.CoscenterName")%>
								</th>
								<th style="width: 50%;">
									<%=Html._("Shared.DataListUser.Approver")%>
								</th>
							</tr>
						</thead>
						<tbody>
							<tr>
								<td>
									<div>
										[<%=item.Key%>]
										<%=item.First().CostcenterName%><br />
										<input type="submit" name="ResetUserReq.<%=item.Key%>.x" value="<%=Html._("SetRequesterLine.ResetApproval")%>" />
										<br />
										<%--<span><%=Html._("Content.ResetApprover")%></span>--%>
									</div>
								</td>
								<td>
									<div>
										<table>
											<%foreach (var itemapp in item.OrderBy(a => a.ApproverLevel))
			 { %>
											<tr>
												<td style="width: 365px;">
													<strong>
														<%=Html._("SetRequesterLine.Approver")%><%=Html._("SetRequesterLine.ApproverLevel" + itemapp.ApproverLevel)%>:</strong>
													<%=itemapp.ApproverName%><br />
													<%=itemapp.ApproverId%>
												</td>
												<td style="width: 301px;">
													<strong>
														<%=Html._("SetRequesterLine.ApprovalBudget")%>:</strong>
													<%=itemapp.ApproverCreditBudget.ToString(new MoneyFormat())%>
													<%=Html._("Order.ConfirmOrder.Baht")%>
												</td>
												<td style="width: 239px;">
													<strong>
														<%=Html._("SetRequesterLine.ApprovalPeriod")%>:</strong>
													<%=itemapp.Parkday%>
													<%=Html._("Admin.CompanySetting.Day")%>
												</td>
											</tr>
											<%} %>
										</table>
									</div>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
				<br />
				<%} %>
				<%} %>
			</div>
			<input type="submit" value="Back" name="CancelSet.x" />
			<%Html.EndForm(); %>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			var checknum = function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
								 event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
								(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			};

			$("#UserApprover_First_Budget").keydown(checknum);
			$("#UserApprover_Second_Budget").keydown(checknum);
			$("#UserApprover_Third_Budget").keydown(checknum);

			//สำหรับทำ AutoComplete ของ CostCenter
			var companyId = $("#companyId").val(); ;
			var userId = '<%=Model.UserReqDetail.UserId %>';

			$("#tags").autocomplete({
				source: function (costcenter, response) {
					$.ajax({
						url: '<%=Url.Action("AutoCompleteSetRequesterLineForUser","OFMAdmin")%>',
						cache: false,
						type: "GET",
						data: ({ companyId: companyId, rows: 20, term: costcenter.term }),
						success: function (data) {
							response(data);
						}
					});
				},
				minLength: 1
			}).autocomplete("widget").addClass("highlight"); ;

			$("#AddCostCenter").click(function () {

				var str = $("#tags").val();
				var n = str.indexOf("[");
				var m = str.indexOf("]");
				var costcenterId = str.substring(n + 1, m);
				var costcenterName = str.substring(m + 1);

				if (costcenterId == "") {//กรณีที่ไม่เลือก AutoComplete แล้วกด Add
					costcenterId = str;
				}

				var text = $("input[class='chk'][value='" + costcenterId + "']").val();

				if (costcenterId != text) {
					var costcenterInSystem = HasCostCenterInSystem(companyId, costcenterId);
					var result = HasCostCenter(companyId, costcenterId, userId);

					if (costcenterInSystem) {//มี costcenter ในระบบ
						if (result) {
							alert("มีข้อมูลในระบบแล้วค่ะ");
							$("#tags").val("");
						}
						else {
							var html = $('<input/>').attr({ type: 'checkbox', value: costcenterId, checked: 'checked', class: "chk", name: "costcenterId", id: costcenterId });
							var costcenterhtml = $('<input/>').attr({ type: 'hidden', value: costcenterName, name: "costcenterName", id: "costName_" + costcenterId });
							$("#DisplayCostcenter").append(html, str, '<br/>', costcenterhtml);
							$("#tags").val("");
						}
					}
					else {
						alert("ไม่มีข้อมูลในระบบค่ะ");
						$("#tags").val("");
					}
				}
			});

			$(".chk").live("click", function () {
				var result = $(this).attr("id");
				var costName = "costName_" + result;

				if ($(this).attr("checked")) {
					$("input[id='" + costName + "']").attr("disabled", false);
				}
				else {
					$("input[id='" + costName + "']").attr("disabled", true);
				}
			});


		});

		function HasCostCenter(companyId, costcenterId, userId) {
			var isHasCostCenter = false;
			$.ajax({
				url: '<%=Url.Action("CheckHasRequesterLine","OFMAdmin")%>',
				cache: false,
				type: "GET",
				async: false,
				data: ({ companyId: companyId, costcenterId: costcenterId, reqUserId: userId }),
				success: function (data) {
					isHasCostCenter = data;
				}
			});
			return isHasCostCenter;
		};

		function HasCostCenterInSystem(companyId, costcenterId) {
			var isHasCostCenterInSystem = false;
			$.ajax({
				url: '<%=Url.Action("CheckHasCostCenterInSystem","OFMAdmin")%>',
				cache: false,
				type: "GET",
				async: false,
				data: ({ companyId: companyId, costcenterId: costcenterId }),
				success: function (data) {
					isHasCostCenterInSystem = data;
				}
			});
			return isHasCostCenterInSystem;
		};


	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.highlight
		{
			background: #e9e9e9;
		}
	</style>
</asp:Content>
