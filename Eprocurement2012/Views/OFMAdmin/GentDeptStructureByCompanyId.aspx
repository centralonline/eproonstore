﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
<div>
	<h2>
		สร้าง DeptStructure ด้วย CompanyId</h2>
	<div>
		<%Html.BeginForm("GentDeptStructureByCompanyId", "OFMAdmin", FormMethod.Post); %>
		<br />
		<br />
		เลือกประเภท site สั่งซื้อ : <input type="radio" name="siteType" id="EPRO" value="EPRO" checked="checked" style="margin: 0px 5px 0px 10px" /><label for="EPRO">eProcurement</label>
		<input type="radio" name="siteType" id="OCI" value="OCI" style="margin: 0px 5px 0px 10px" /><label for="OCI">OCI Service</label>
		<br />
		<br />
		CompanyId ที่ต้องการสร้างข้อมูล : <input type="text" id="companyId" name="companyId" />
		<br />
		<br />
		<br />
		<input type="submit" id="submit" name="submit" value="submit" /><span style="color:Red"><%=TempData["data"]%></span>
		<%Html.EndForm(); %>
	</div>
	<br />
</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
