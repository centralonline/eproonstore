﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<div style="text-align: center;"><br />
				<img src="/images/step04.png" alt="" /></div>

			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post);%>
			<%=Html.HiddenFor(m => m.CompanyId) %>
			<p class="btn" style="float: right;">
					<input type="submit" id="CompanySetting" name="CompanySetting.x" value="<< Previous" /></p>
			<%Html.EndForm();%>
			</div>
			<div class="grid_2" style="position: absolute;  right: 1%; margin-top: 1%;">
				<p class="btn" style="float: left;">
					<%if (Model.IsCompModelThreeLevel)
	   { %>
					<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
					<%=Html.HiddenFor(m => m.CompanyId) %>
						<input type="submit" name="Department.x" value="Next >>" />
					<%Html.EndForm();%>
					<%} %>
					<%else
	   { %>
					<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
					<%=Html.HiddenFor(m => m.CompanyId) %>
						<input type="submit" name="CostCenter.x" value="Next >>" />
					<%Html.EndForm();%>
					<%} %></p>
			</div>


			<h2 id="page-heading">
				<%=Html._("Admin.CompanyInfoReview")%></h2>
		</div>
		<div class="grid_16">
			<div class="grid_16">
				<div class="grid_2" style="padding-top:8px;">
					<label>
						<%=Html._("Admin.CompanyInfoReview.Introduction")%></label>
				</div>
				<div class="grid_2" style="padding-top:8px;">
					<%if (false)
	   {%>
					<span style="color: Red">Not Complete</span>
					<%} %>
					<%else
	   {%>
					<span style="color: Green">Complete</span>
					<%} %>
				</div>
				<div class="grid_2">
					<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post);%>
					<%=Html.HiddenFor(m => m.CompanyId) %>
					<input type="submit" name="CompanyInfo.x" value="<%=Html._("Button.Edit") %>" />
					<%Html.EndForm();%>
				</div>
			</div>
			<div class="grid_16">
				<div class="grid_2" style="padding-top:8px;">
					<label>
						<%=Html._("Admin.CompanyInfoReview.CompanyLogo")%></label>
				</div>
				<div class="grid_2" style="padding-top:8px;">
					<%if (Model.CompanyLogoReview.Bytes.All(b => b == 0))
	   {%>
					<span style="color: Red">Not Complete</span>
					<%} %>
					<%else
	   {%>
					<span style="color: Green">Complete</span>
					<%} %>
				</div>
				<div class="grid_2">
					<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post);%>
					<%=Html.HiddenFor(m => m.CompanyId) %>
					<input type="submit" name="CompanyLogo.x" value="<%=Html._("Button.Edit") %>" />
					<%Html.EndForm();%>
				</div>
			</div>
			<div class="grid_16">
				<div class="grid_2" style="padding-top:8px;">
					<label>
						<%=Html._("Admin.CompanyInfoReview.CompanySetting")%></label>
				</div>
				<div class="grid_2" style="padding-top:8px;">
					<%if (false)
	   {%>
					<span style="color: Red">Not Complete</span>
					<%} %>
					<%else
	   {%>
					<span style="color: Green">Complete</span>
					<%} %>
				</div>
				<div class="grid_2">
					<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post);%>
					<%=Html.HiddenFor(m => m.CompanyId) %>
					<input type="submit" name="CompanySetting.x" value="<%=Html._("Button.Edit") %>" />
					<%Html.EndForm();%>
				</div>
			</div>
		</div>
		<div style="min-height: 550px;">
		</div>
		<%--<div class="grid_8">
			<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post);%>
			<%=Html.HiddenFor(m => m.CompanyId) %>
			<p class="btn">
				<input type="submit" name="CompanySetting.x" value="Back" /></p>
			<%Html.EndForm();%>
		</div>
		<div class="grid_8">
			<p class="btn" style="float: right">
				<%if (Model.IsCompModelThreeLevel)
	  { %>
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
				<%=Html.HiddenFor(m => m.CompanyId) %>
				<p class="btn" style="float: right">
					<input type="submit" name="Department.x" value="Continue" /></p>
				<%Html.EndForm();%>
				<%} %>
				<%else
	  { %>
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
				<%=Html.HiddenFor(m => m.CompanyId) %>
				<p class="btn" style="float: right">
					<input type="submit" name="CostCenter.x" value="Continue" /></p>
				<%Html.EndForm();%>
				<%} %>
			</p>
		</div>--%>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
