﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.NewSiteData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<br />
		<fieldset>
		<img src="<%=Url.Action("CompanyLogoImage","OFMAdmin", new { Model.CompanyId }) %>" alt="Company Logo" width="200px" />
			</fieldset>
	</div>
	<div>
		<div>
			<fieldset>
				<legend>
					<%=Html._("Account.ApplyNow.InformationCorporate")%></legend>
				<div>
					<label>
						<%=Html._("Admin.CompanyInfo.CompanyId")%>
						:</label>
					<%=Model.CompanyId%>
				</div>
				<div>
					<label>
						<%=Html._("Admin.CompanyInfo.CompanyThaiName")%>
						:</label>
					<%=Model.CompanyTName%>
				</div>
				<div>
					<label>
						<%=Html._("Admin.CompanyInfo.CompanyEngName")%>
						:</label>
					<%=Model.CompanyEName%>
				</div>
				<div>
					<label>
						<%=Html._("Admin.CompanyInfo.CustId")%>
						:</label>
					<%=Model.CustId%>
				</div>
				<br />
				<div>
					<label>
						<%=Html._("Admin.CompanyInfo.InvoiceAddress")%>
						:</label>
					<%=Model.InvoiceAddress.Address1%><br />
					<%=Model.InvoiceAddress.Address2%><br />
					<%=Model.InvoiceAddress.Address3%><br />
					<%=Model.InvoiceAddress.Address4%><br />
				</div>
				<br />
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.ShipAddr")%>
						:</label>
					<%=Model.ShippingAddress.Address1%><br />
					<%=Model.ShippingAddress.Address2%><br />
					<%=Model.ShippingAddress.Address3%><br />
					<%=Model.ShippingAddress.Address4%>
				</div>
				<br />
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.SiteActive")%>
						:</label>
						<%=Model.IsSiteActivate %>
				</div>
			</fieldset>
		</div>
		<div>
			<fieldset>
				<legend>
					<%=Html._("OFMAdmin.CreateNewSite.OrderControlType")%></legend>
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.CompanyModel")%>
						:</label>
					<%=Model.CompanyModel%>
				</div>
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.OrderControlType")%>
						:</label>
					<%=Model.OrderControlType%>
				</div>
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.BudgetLevelType")%>
						:</label>
					<%=Model.BudgetLevelType%>
				</div>
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.BudgetPeriodType")%>
						:</label>
					<%=Model.BudgetPeriodType%>
				</div>
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.PriceType")%>
						:</label>
					<%=Model.PriceType%>
				</div>
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.CompanyCatalog")%>
						:</label>
					<%=Model.UseOfmCatalog%>
				</div>
				<div>
					<label>
						<%=Html._("OFMAdmin.CreateNewSite.OrderIDFormat")%>
						:</label>
					<%=Model.OrderIDFormat%>
				</div>
			</fieldset>
		</div>
		<div>
		<fieldset>
				<legend>
					<%=Html._("OFMAdmin.UserInfo.AllUser")%></legend>
			<div>
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.RootAdmin")%>
					:</label>
				<%=Model.UserID%>
			</div>
			<div>
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminTName")%>
					:</label>
				<%=Model.UserTName%>
			</div>
			<div>
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminEName")%>
					:</label>
				<%=Model.UserEName%>
			</div>
			<div>
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminPhone")%>
					:</label>
				<%=Model.PhoneNumber%>
				<span>
					<%=Html._("OFMAdmin.CreateNewSite.AdminExt")%></span>
				<%=Model.ExtentionPhoneNumber%>
			</div>
			<div>
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminMobile")%>
					:</label>
				<%=Model.MobileNumber%>
			</div>
			<div>
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminFax")%>
					:</label>
				<%=Model.FaxNumber%>
			</div>
			<div>
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.OtherRole")%>
					:</label>
				<%=Model.IsRequester ? Html._("OFMAdmin.CreateNewSite.IsRequester") : ""%>
				<%=Model.IsApprover ? Html._("OFMAdmin.CreateNewSite.IsApprover") : ""%>
			</div>
			<div>
				<label>
					<%=Html._("OFMAdmin.ViewNewSiteDetail.RePasswordText")%>
					:</label>
				<%=Model.RePasswordText%>
			</div>
			<div>
				<label>
					<%=Html._("OFMAdmin.ViewNewSiteDetail.DefaultLang")%>
					:</label>
				<%=Model.DefaultLang%>
			</div>
			</fieldset>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
