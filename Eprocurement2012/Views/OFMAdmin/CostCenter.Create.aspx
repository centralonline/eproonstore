﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CostCenter>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<br />
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p style="float: right;">
					<input type="submit" id="DepartmentGuide" name="DepartmentGuide.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: left">
					<input type="submit" id="CompanyGuide" name="CompanyGuide.x" value="Next >>" /></p>
				<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.CostCenter.ContentCreate")%></h2>
			<div class="grid_8">
				<br />
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
				<%=Html.HiddenFor(m => m.CompanyId)%>
				<%=Html.HiddenFor(m => m.IsCompModelThreeLevel)%>
				<%if (Model.IsCompModelThreeLevel)
	  { %>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.SelcetDept")%>
					</label>
				</div>
				<div>
					<%=Html.DropDownListFor(m => m.DepartmentID, new SelectList(Model.ListCompanyDepartment, "DepartmentID", "DepartmentDisplayname"), "กรุณาเลือก")%>
					<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.DepartmentID)%></span>
				</div>
				<%} %>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.CostCenterID")%>
					</label>
				</div>
				<!-- Start TBRunningnumbers Standard-->
				<div>
					<%if (Model.OrderIDFormat == Eprocurement2012.Models.Company.OrderIDFormatType.Standard.ToString())
	   {%>
					<span>
						<%=Html.TextBoxFor(m => m.CostCenterID, new { maxlength = 15, @id="costId" })%></span>
					<%} %>
					<%else
	   {%>
					<span>
						<%=Html.TextBoxFor(m => m.CostCenterID, new { maxlength = 8, @id="costId" })%></span>
					<%} %>
					<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CostCenterID)%></span>
				</div>
				<!-- End TBRunningnumbers Standard-->
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.OracleCode")%>
					</label>
				</div>
				<div>
					<span>
						<%=Html.TextBoxFor(m => m.OracleCode)%>
					</span>
				</div>
				<div class="ContentCostCenterID">
					<label for="IsByPassAdmin">
						<%=Html._("Admin.CostCenter.ContentCostCenterID")%></label>
				</div>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.ThaiName")%>
					</label>
				</div>
				<div>
					<span>
						<%=Html.TextBoxFor(m => m.CostCenterThaiName)%>
					</span><span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CostCenterThaiName)%></span>
				</div>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.EngName")%>
					</label>
				</div>
				<div>
					<span>
						<%=Html.TextBoxFor(m => m.CostCenterEngName)%></span> <span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CostCenterEngName)%></span>
				</div>
				<%if (Model.IsByPassAdmin)
	  { %>
				<div class="IsByPassAdmin">
					<%=Html.CheckBoxFor(m => m.UseByPassAdmin, new { id = "UseByPassAdmin" })%>
					<label for="UseByPassAdmin">
						<%=Html._("Admin.CostCenter.IsByPassAdmin")%></label>
				</div>
				<%} %>
				<%if (Model.IsAutoApprove)
	  { %>
				<div class="IsAutoApprove">
					<%=Html.CheckBoxFor(m => m.UseAutoApprove, new { id = "UseAutoApprove" })%>
					<label for="UseAutoApprove">
						<%=Html._("Admin.CostCenter.IsAutoApprove")%></label>
				</div>
				<%} %>
				<%if (Model.IsByPassApprover)
	  { %>
				<div class="IsByPassApprover">
					<%=Html.CheckBoxFor(m => m.UseByPassApprover, new { id = "UseByPassApprover" })%>
					<label for="UseByPassApprover">
						<%=Html._("Admin.CostCenter.IsByPassApprover")%></label>
				</div>
				<%} %>
				<div class="ContentCostCenterID">
					<%=Html.CheckBoxFor(m => m.IsDeliCharge, new { id = "IsDeliCharge" })%>
					<label for="IsDeliCharge">
						<%=Html._("Admin.CostCenter.IsDeliCharge")%>
					</label>
				</div>
				<br />
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.SelectInvoice")%>
					</label>
				</div>
				<div>
					<%=Html.DropDownListFor(m => m.SelectInvoice, new SelectList(Model.ListInvoiceAddress, "CustId", "DisplayInvoiceAddress"), new { @style = "width: 300px;" })%>
					<input type="submit" id="ShowAddressInv" name="ShowAddressForCreate.x" value="<%=Html._("Admin.CostCenter.ShowInvoiceAddress")%>" />
					<input type="submit" id="AddNewShipAddr" name="AddNewShipAddress.x" value="<%=Html._("Admin.CostCenter.AddNewShipAddress")%>" />
				</div>
				<div class="changInv">
					<!-- Start changeInv-->
					<%if (Model.CostCenterInvoice != null)
	   {%>
					<div class="format">
						<label>
							<%=Html._("Admin.CostCenter.CustID")%>
						</label>
						<%=Model.CostCenterCustID%>
						<%=Html.HiddenFor(m => m.CostCenterCustID)%>
					</div>
					<br />
					<div class="format" style="border-collapse: collapse;">
						<label>
							<%=Html._("Admin.CostCenter.InvoiceAddress")%>
						</label>
					</div>
					<div class="boxAddress format">
						<%=Model.CostCenterInvoice.Address1%><br />
						<%=Model.CostCenterInvoice.Address2%><br />
						<%=Model.CostCenterInvoice.Address3%><br />
						<%=Model.CostCenterInvoice.Address4%><br />
					</div>
					<%} %>
					<%--	<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.SelectShipping")%>
					</label>
				</div>--%>
					<%--<div>
					<%=Html.DropDownListFor(m => m.SelectShipID, new SelectList(Model.ListShipAddress, "ShipId", "DisplayShipAddress"), new { @style = "width: 300px;" })%>
					<input type="submit" id="ShowAddressShip" name="ShowAddressForCreate.x" value="<%=Html._("Admin.CostCenter.ShowShipAddress")%>" />
					<input type="submit" id="AddNewShipAddr" name="AddNewShipAddress.x" value="<%=Html._("Admin.CostCenter.AddNewShipAddress")%>" />
				</div>--%>
					<%--				<%if (Model.CostCenterShipping != null)
	  {%>
				<div class="format">
					<%=Html.HiddenFor(m =>m.CostCenterShipping.ShipID)%>
					<label>
						<%=Html._("Admin.CostCenter.ShippingAddress")%>
					</label>
				</div>
				<div class="boxAddress format">
					<%=Model.CostCenterShipping.Address1%><br />
					<%=Model.CostCenterShipping.Address2%><br />
					<%=Model.CostCenterShipping.Address3%><br />
				</div>
				<br />
				<br />
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.Province")%>
					</label>
					<%=Model.CostCenterShipping.Province%>
				</div>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.ZipCode")%>
					</label>
					<%=Model.CostCenterShipping.ZipCode%>
				</div>
				<br />
				<%} %>--%>
					<!-- Start แสดงผลแบบตาราง-->
					<div class="format">
						<label>
							<%=Html._("Admin.CostCenter.SelectShipping")%>
						</label>
					</div>
					<div class="format">
						<label style="text-align: left; color: Red;">
							<%=TempData["ErrorSelectShipID"]%>
						</label>
					</div>
					<div class="SubmitCostCenter" style="float: right;">
						<input id="SaveNewCostCenter" type="submit" name="SaveNewCostCenter.x" value="Save" />
						<input id="Cancel" type="reset" name="Cancel.x" value="Cancel" />
					</div>
					<div>
						<table id="sortBy">
							<thead>
								<tr>
									<th>
									</th>
									<th>
										ลำดับ
									</th>
									<th>
										รหัสสถานที่จัดส่ง
									</th>
									<th>
										ที่อยู่สถานที่จัดส่ง
									</th>
									<th>
										เบอร์ติดต่อ
									</th>
									<th>
										จังหวัด
									</th>
									<th>
										รหัสไปรษณีย์
									</th>
								</tr>
							</thead>
							<%int count = 0;%>
							<%foreach (var shipping in Model.ListShipAddress)
		 {%>
							<tr>
								<td>
									<%=Html.RadioButtonFor(m => m.SelectShipID, shipping.ShipID, new { id = shipping.ShipID, @name = "SelectShipID" })%>
								</td>
								<td>
									<%=++count%>
								</td>
								<td>
									<%=shipping.ShipID %>
								</td>
								<td>
									<%=shipping.Address1%><br />
									<%=shipping.Address2%><br />
									<%=shipping.Address3%><br />
								</td>
								<td>
									<%=shipping.ShipPhoneNo %>
								</td>
								<td>
									<%=shipping.Province %>
								</td>
								<td>
									<%=shipping.ZipCode %>
								</td>
							</tr>
							<%} %>
						</table>
					</div>
					<br />
					<br />
					<!-- End แสดงผลแบบตาราง-->
				</div>
				<!-- End changeInv-->
				<%Html.EndForm();%>
			</div>
		</div>
	</div>
	<br />
	<br />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script src="/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<link href="/css/dataTables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(function () {
			$("#costId").keypress(function (e) {
				var key = String.fromCharCode(e.which);
				if (isLetter(key) == null) {
					return false;
				}
			});
			$('#sortBy').dataTable();

			$("#SelectInvoice").change(function () {
				$(".changInv").hide();
			});
		});
		function isLetter(s) {
			//return s.match("^[a-zA-Z0-9\(\)]+$");
			return s.match("^[A-Za-z0-9_\-]*$");
		}
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.format label
		{
			float: left;
			margin-right: 3%;
			text-align: right;
			width: 30%;
		}
		.forlabel
		{
			width: 250px;
			float: left;
		}
		.boxAddress
		{
			float: left;
			margin-right: 3%;
			text-align: left;
			width: 60%;
		}
		.ContentCostCenterID
		{
			margin-left: 33%;
		}
		.IsByPassAdmin
		{
			margin-left: 33%;
		}
		.IsAutoApprove
		{
			margin-left: 33%;
		}
		.IsByPassApprover
		{
			margin-left: 33%;
		}
		.SubmitCostCenter
		{
			margin-left: 250px;
		}
	</style>
</asp:Content>
