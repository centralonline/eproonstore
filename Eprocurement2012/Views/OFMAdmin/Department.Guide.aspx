﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<%if (Model.IsCompModelThreeLevel)
		{%>
		<div class="grid_16">
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post); %>
				<%=Html.HiddenFor(m=> m.CompanyId)%>
				<p class="btn" style="float: right;">
					<input type="submit" id="CompanyInfo" name="CompanyInfo.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post); %>
				<%=Html.HiddenFor(m=> m.CompanyId)%>
				<p class="btn" style="float: left;">
					<input type="submit" id="ViewAllDepartment" name="ViewAllDepartment.x" value="Next >>" /></p>
				<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.DepartmentGuide")%></h2>
		</div>
		<div class="grid_16">
			<img src="/images/Company_info/Company_sec02_2.jpg" alt="you are here" />
		</div>
		<%}
		else
		{ %>
		<div class="grid_16">
			<h5>องค์กรนี้ ใช้ model 2 level ไม่ต้องกำหนด Department ค่ะ</h5>
		</div>
		<%} %>
		<div style="min-height: 500px;">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
