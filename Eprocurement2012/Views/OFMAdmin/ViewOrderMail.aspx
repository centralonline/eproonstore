﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<Eprocurement2012.Models.MailTransLog>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<div class="head">
			<br />
			<br />
			<div>
				<label style="float: left;">
					<%=Html._("OFMAdmin.ViewOrderMail.OrderId")%>
					:
				</label>
			</div>
			<div>
				<%=Model.Value.OrderId%>
			</div>
		</div>
		<br />
		<div class="border">
			<div>
				<div class="format">
					<label>
						<%=Html._("OFMAdmin.ViewOrderMail.Subject")%>
						:
					</label>
				</div>
				<div>
					<%=Model.Value.Subject%>
				</div>
			</div>
			<div>
				<div class="format">
					<label>
						<%=Html._("OFMAdmin.ViewOrderMail.CreateOn")%>
						:
					</label>
				</div>
				<div>
					<%=Model.Value.CreateOn%>
				</div>
			</div>
			<div>
				<div class="format">
					<label>
						<%=Html._("OFMAdmin.ViewOrderMail.SendMailSuccess")%>
						:
					</label>
				</div>
				<div>
					<%=Model.Value.SendMailSuccess%>
				</div>
			</div>
			<div>
				<div class="format">
					<label>
						<%=Html._("OFMAdmin.ViewOrderMail.EmailTo")%>
						:
					</label>
				</div>
				<div>
					<%=Model.Value.EmailTo%>
				</div>
			</div>
			<div>
				<div class="format">
					<label>
						<%=Html._("OFMAdmin.ViewOrderMail.EmailCC")%>
						:
					</label>
				</div>
				<div>
					<%=Model.Value.EmailCC%>
				</div>
			</div>
			<div>
				<div class="format">
					<label>
						<%=Html._("OFMAdmin.ViewOrderMail.EmailBCC")%>
						:
					</label>
				</div>
				<div>
					<%=Model.Value.EmailBCC%>
				</div>
			</div>
			<div>
				<div class="format">
					<label>
						<%=Html._("OFMAdmin.ViewOrderMail.EmailFrom")%>
						:
					</label>
				</div>
				<div>
					<%=Model.Value.EmailFrom%>
				</div>
			</div>
		</div>
		<br />
		<div class="border">
			<%=Model.Value.MailContent %>
		</div>
		<br />
		<div class="head">
			<label>
				ส่งเมล์แจ้งผู้ใช้งานอีกครั้ง (Resend Mail)</label>
		</div>
		<br />
		<%Html.BeginForm("ResendOrderMail", "OFMAdmin", FormMethod.Post); %>
		<div class="border">
			<br />
			<%=Html.Hidden("logId", Model.Value.LogID)%>
			<%=Html.Hidden("guid", Model.Value.Guid)%>
			<span>ระบุหมายเหตุภายในของการดำเนินการ : </span>
			<input id="remark" name="remark" type="text" value="" style="min-width: 360px; width: 50%" />
			<span style="color: Red;"><%=TempData["SendComplete"]%></span>
			<br />
			<input type="submit" name="sendMail" value="Send Mail" style="margin-left: 500px;" />
		</div>
		<%Html.EndForm(); %>
		<br />
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.format label
		{
			float: left;
			margin-right: 3%;
			text-align: right;
			width: 200px;
		}
		.border
		{
			width: 800px;
			min-height: 80px;
			padding: 5px;
			margin-right: 9px;
			margin-top: 0px;
			border: 1px solid #E5E5E5;
			border-radius: 5px 5px 5px 5px;
			margin-left: 50px;
		}
		.head
		{
			border-bottom: 1px solid #ccc;
			width: 800px;
			margin-left: 50px;
		}
	</style>
</asp:Content>
