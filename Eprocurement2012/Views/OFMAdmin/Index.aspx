﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.Company>>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("OverViewMenuBar");%>
		<br />
		<div>
			<%int count = 0;%>
			<table>
				<tr>
					<th>
						<%=Html._("OFMAdmin.Index.Order")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.CompanyId")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.CompanyName")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.CreateNewSite.CustId")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.Discount")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.CompStatus")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.OrderControlType")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.PriceType")%>
					</th>
					<th>
						จำนวนผู้ใช้งาน
					</th>
				</tr>
				<%foreach (var company in Model.Value)
				{%>
				<tr>
					<td>
						<%=++count%>
					</td>
					<td>
						<%=company.CompanyId%>
					</td>
					<td>
						<%=company.CompanyName%>
					</td>
					<td>
						<%=company.DefaultCustId%>
					</td>
					<td>
						<%=company.DiscountRate%>
					</td>
					<td>
						<%=company.DisplayCompanyStatus %>
					</td>
					<td>
						<%=company.DisplayControlTypeName%>
					</td>
					<td>
						<%=company.DisplayPriceType%>
					</td>
					<td>
						<%=company.DefaultParkDay%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
