﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.NewSiteData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<br />
	<%Html.BeginForm("CreateNewSite", "OFMAdmin", FormMethod.Post); %>
	<fieldset class="white">
		<legend>
			<%=Html._("OFMAdmin.CreateNewSite.SearchData")%></legend>
		<div>
			<%=Html._("OFMAdmin.CreateNewSite.SearchCustID")%>:<%=Html.TextBox("SearchCustID", "", new { maxlength = 6 })%>
			<input type="submit" value="<%=Html._("Button.Search")%>" name="SearchCustID.x" /><span
				style="color: Red"><%=TempData["ErrorMessage"]%></span>
		</div>
	</fieldset>
	<%Html.EndForm(); %>
	<%if (!string.IsNullOrEmpty(Model.CustId))
   { %>
	<%Html.BeginForm("CreateNewSite", "OFMAdmin", FormMethod.Post, new { enctype = "multipart/form-data" }); %>
	<fieldset class="white">
		<legend>
			<%=Html._("OFMAdmin.CreateNewSite.SettingCompany")%></legend>
		<div>
			<table>
				<tr>
					<td>
						<div class="div-SettingCompany">
							<label>
								<%=Html._("OFMAdmin.CreateNewSite.CompanyId")%>:
							</label>
							<%=Html.TextBoxFor(m => m.CompanyId, new { maxlength =6})%>
							<span style="color: Red">
								<%=Html.LocalizedValidationMessageFor(model => model.CompanyId)%></span><br />
						</div>
						<div class="div-SettingCompany">
							<label>
								<%=Html._("OFMAdmin.CreateNewSite.CompanyTName")%>:
							</label>
							<%=Html.TextBoxFor(m => m.CompanyTName, new {maxlength = 100 })%>
							<span style="color: Red">
								<%=Html.LocalizedValidationMessageFor(model => model.CompanyTName)%></span><br />
						</div>
						<div class="div-SettingCompany">
							<label>
								<%=Html._("OFMAdmin.CreateNewSite.CompanyEName")%>:
							</label>
							<%=Html.TextBoxFor(m => m.CompanyEName, new { maxlength = 100 })%>
							<span style="color: Red">
								<%=Html.LocalizedValidationMessageFor(model => model.CompanyEName)%></span><br />
						</div>
						<div class="div-SettingCompany">
							<label>
								<%=Html._("OFMAdmin.CreateNewSite.CustId")%>:
							</label>
							<%=Model.CustId %>
						</div>
						<div class="div-SettingCompany">
							<label>
								<%=Html._("OFMAdmin.CreateNewSite.Discount")%>:
							</label>
							<%=Model.DiscountRate %>
							<input type="hidden" name="DiscountRate" value="<%=Model.DiscountRate %>" />
						</div>
					</td>
					<td>
						<%=Html._("OFMAdmin.CreateNewSite.InvAddr")%><br />
						<%=Html.TextBoxFor(m => m.InvoiceAddress.Address1, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html.TextBoxFor(m => m.InvoiceAddress.Address2, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html.TextBoxFor(m => m.InvoiceAddress.Address3, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html.TextBoxFor(m => m.InvoiceAddress.Address4, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html._("OFMAdmin.CreateNewSite.ShipAddr")%><br />
						<%=Html.TextBoxFor(m => m.ShippingAddress.Address1, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html.TextBoxFor(m => m.ShippingAddress.Address2, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html.TextBoxFor(m => m.ShippingAddress.Address3, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html.TextBoxFor(m => m.ShippingAddress.Province, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html.TextBoxFor(m => m.ShippingAddress.ZipCode, new { style = "width:200px;", @readonly = true })%><br />
						<%=Html._("OFMAdmin.CreateNewSite.SaleRequest")%>:<br />
						<%=Html.TextBoxFor(m => m.SaleRequest, new { style = "width:200px;"})%>
						<span style="color: Red">
							<%=Html.LocalizedValidationMessageFor(model => model.SaleRequest)%></span><br />
						<%=Html._("OFMAdmin.CreateNewSite.AdminOfmRemark")%>:<br />
						<%=Html.TextBoxFor(m => m.AdminOfmRemark, new { style = "width:200px;"})%>
						<span style="color: Red">
							<%=Html.LocalizedValidationMessageFor(model => model.AdminOfmRemark)%></span><br />
					</td>
				</tr>
				<tr>
					<td colspan="4">
						<%=Html._("OFMAdmin.CreateNewSite.SiteActive")%>:
						<%=Html.DropDownListFor(m => m.IsSiteActivate, new SelectList(Model.SiteActivate, "Id", "Name")) %>
						<span style="color: Red">
							<%=Html.LocalizedValidationMessageFor(model => model.IsSiteActivate)%></span><br />
					</td>
				</tr>
			</table>
		</div>
	</fieldset>
	<fieldset class="white">
		<legend>
			<%=Html._("OFMAdmin.CreateNewSite.UploadLogo")%>: </legend>
		<div>
			<%=Html.TextBoxFor(m => m.FileCompanyLogo, new { type = "file" })%>
		</div>
	</fieldset>
	<fieldset class="white">
		<legend>
			<%=Html._("OFMAdmin.CreateNewSite.AllSetting")%></legend>
		<div>
			<%=Html._("OFMAdmin.CreateNewSite.CompanyModel")%>:
			<%=Html.RadioButtonFor(m => m.CompanyModel, "2Level", new { id = "2Level" })%>
			<label for="2Level">
				<%=Html._("OFMAdmin.CreateNewSite.2Model")%>
			</label>
			<%=Html.RadioButtonFor(m => m.CompanyModel, "3Level", new { id = "3Level" })%>
			<label for="3Level">
				<%=Html._("OFMAdmin.CreateNewSite.3Model")%>
			</label>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(model => model.CompanyModel)%></span>
			<br />
			<div class="div-format">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.OrderControlType")%>:
				</label>
				<%=Html.DropDownListFor(m => m.OrderControlType, new SelectList(Model.ListOrderControlType, "Id", "Name"))%>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(model => model.OrderControlType)%></span><br />
			</div>
			<div class="div-format" id="BudgetLevelType">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.BudgetLevelType")%>:
				</label>
				<%=Html.DropDownListFor(m => m.BudgetLevelType, new SelectList(Model.ListBudgetLevelType, "Id", "Name")) %>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(model => model.BudgetLevelType)%></span><br />
			</div>
			<div class="div-format" id="BudgetPeriodType">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.BudgetPeriodType")%>:
				</label>
				<%=Html.DropDownListFor(m => m.BudgetPeriodType, new SelectList(Model.ListBudgetPeriodType, "Id", "Name")) %>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(model => model.BudgetPeriodType)%></span><br />
			</div>
			<div class="div-format">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.PriceType")%>:
				</label>
				<%=Html.DropDownListFor(m => m.PriceType, new SelectList(Model.ListPriceType, "Id", "Name"))%>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(model => model.PriceType)%></span><br />
			</div>
			<%=Html._("OFMAdmin.CreateNewSite.CompanyCatalog")%>:
			<%=Html.RadioButtonFor(m => m.UseOfmCatalog, "No", new { id = "UseCompanyCatalog" })%>
			<label for="UseCompanyCatalog">
				<%=Html._("OFMAdmin.CreateNewSite.UseCompanyCatalog")%>:
			</label>
			<%=Html.RadioButtonFor(m => m.UseOfmCatalog, "Yes", new { id = "UseOFMCatalog", Checked = "checked" })%>
			<label for="UseOFMCatalog">
				<%=Html._("OFMAdmin.CreateNewSite.NotUseCompanyCatalog")%>:
			</label>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(model => model.UseOfmCatalog)%></span><br />
			<%=Html._("OFMAdmin.CreateNewSite.OrderIDFormat")%>
			<%=Html.RadioButtonFor(m => m.OrderIDFormat, "Standard", new { id = "StandardOrderId" })%>
			<label for="StandardOrderId">
				<%=Html._("OFMAdmin.CreateNewSite.StandardFormat")%>
			</label>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(model => model.OrderIDFormat)%></span><br />
			<div>
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.CompanyFormat")%>:
				</label>
				<br />
				<%=Html.RadioButtonFor(m => m.OrderIDFormat, "TypeA", new { id = "OrderIdTypeA" })%>
				<label for="OrderIdTypeA">
					<%=Html._("OFMAdmin.CreateNewSite.OrderIDTypeA")%>:
				</label>
				<u>Ex</u> OFM-001-120100001<br />
				<%=Html.RadioButtonFor(m => m.OrderIDFormat, "TypeB", new { id = "OrderIdTypeB" })%>
				<label for="OrderIdTypeB">
					<%=Html._("OFMAdmin.CreateNewSite.OrderIDTypeB")%>:
				</label>
				<u>Ex</u> MK-001-120100001<br />
				<%=Html.RadioButtonFor(m => m.OrderIDFormat, "TypeC", new { id = "OrderIdTypeC" })%>
				<label for="OrderIdTypeC">
					<%=Html._("OFMAdmin.CreateNewSite.OrderIDTypeC")%>:
				</label>
				<u>Ex</u> 001-120100001<br />
			</div>
		</div>
	</fieldset>
	<fieldset class="white">
		<legend>
			<%=Html._("OFMAdmin.CreateNewSite.RootAdmin")%>
		</legend>
		<div>
			<div class="div-format">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.RootAdmin")%>:
				</label>
				<label>
					<%=Model.UserID%>
				</label>
				<%=Html.HiddenFor(m => m.UserID)%><%=Html.HiddenFor(m => m.SequenceId)%>
			</div>
			<br />
			<div class="div-format">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminTName")%>:
				</label>
				<%=Html.TextBoxFor(m => m.UserTName) %>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(model => model.UserTName)%></span><br />
			</div>
			<div class="div-format">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminEName")%>:
				</label>
				<%=Html.TextBoxFor(m => m.UserEName) %>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(model => model.UserEName)%></span><br />
			</div>
			<div class="div-format">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminPhone")%>:
				</label>
				<%=Html.HiddenFor(m => m.PhoneId)%>
				<%=Html.TextBoxFor(m => m.PhoneNumber, new { @class = "number", maxlength = 10 })%>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(model => model.PhoneNumber)%></span>
				<%=Html._("OFMAdmin.CreateNewSite.AdminExt")%>:
				<%=Html.TextBoxFor(m => m.ExtentionPhoneNumber, new { @class = "number", maxlength = 5 })%><br />
			</div>
			<div class="div-format">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminMobile")%>:
				</label>
				<%=Html.HiddenFor(m => m.MobileId)%>
				<%=Html.TextBoxFor(m => m.MobileNumber, new { @class = "number", maxlength = 10 })%><br />
			</div>
			<div class="div-format">
				<label>
					<%=Html._("OFMAdmin.CreateNewSite.AdminFax")%>:
				</label>
				<%=Html.HiddenFor(m => m.FaxId)%>
				<%=Html.TextBoxFor(m => m.FaxNumber, new { @class = "number", maxlength = 10 })%><br />
			</div>
			<%=Html._("OFMAdmin.CreateNewSite.OtherRole")%>:
			<%=Html.CheckBoxFor(m => m.IsRequester, new { id = "IsRequester" })%>
			<label for="IsRequester">
				<%=Html._("OFMAdmin.CreateNewSite.IsRequester")%>
			</label>
			<%=Html.CheckBoxFor(m => m.IsApprover, new { id = "IsApprover" })%>
			<label for="IsApprover">
				<%=Html._("OFMAdmin.CreateNewSite.IsApprover")%>
			</label>
			<br />
			<%=Html._("OFMAdmin.CreateNewSite.RePasswordPeriod")%>
			<br />
			<%=Html.RadioButtonFor(m => m.RePasswordText, "3Month", new { id = "3Month", Checked = "checked"  })%>
			<label for="3Month">
				<%=Html._("OFMAdmin.CreateNewSite.RadioButton3Month")%>
			</label>
			<%=Html.RadioButtonFor(m => m.RePasswordText, "2Month", new { id = "2Month" })%>
			<label for="2Month">
				<%=Html._("OFMAdmin.CreateNewSite.RadioButton2Month")%>
			</label>
			<%=Html.RadioButtonFor(m => m.RePasswordText, "1Month", new { id = "1Month" })%>
			<label for="1Month">
				<%=Html._("OFMAdmin.CreateNewSite.RadioButton1Month")%>
			</label>
			<%=Html.RadioButtonFor(m => m.RePasswordText, "XMonth", new { id = "XMonth" })%>
			<label for="XMonth">
				<%=Html._("OFMAdmin.CreateNewSite.RadioButtonXMonth")%>
			</label>
			<%=Html.TextBoxFor(m => m.RePasswordPeriodFromUser) %>
			<%=Html._("OFMAdmin.CreateNewSite.XMonthDay")%>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(model => model.RePasswordPeriod)%></span><br />
			<%=Html._("OFMAdmin.CreateNewSite.DefaultLang")%>
			<br />
			<%=Html.RadioButtonFor(m => m.DefaultLang, "TH", new { id = "THLang" })%>
			<label for="THLang">
				<%=Html._("OFMAdmin.CreateNewSite.DefaultLangTH")%>
			</label>
			<%=Html.RadioButtonFor(m => m.DefaultLang, "EN", new { id = "ENLang" })%>
			<label for="ENLang">
				<%=Html._("OFMAdmin.CreateNewSite.DefaultLangEN")%>
			</label>
			<span style="color: Red">
				<%=Html.LocalizedValidationMessageFor(model => model.DefaultLang)%></span><br />
		</div>
	</fieldset>
	<fieldset class="white">
		<legend>
			<%=Html._("OFMAdmin.CreateNewSite.OtherFeature")%>
		</legend>
		<div>
			<label style="font-weight: bold;">
				ฟีเจอร์พื้นฐาน</label>
			<div style="margin-left: 20px;">
				<%=Html._("OFMAdmin.CreateNewSite.DefaultParkDay")%>:
				<%=Html.DropDownListFor(m => m.DefaultParkDay, new SelectList(Model.ListParkDay, "Id","Name"))%>
				<span style="color: Red">
					<%=Html.LocalizedValidationMessageFor(model => model.ListParkDay)%></span>
				<br />
				<%=Html.CheckBoxFor(m => m.ShowContactUs, new { id = "ShowContactUs", @checked = "checked" })%>
				<label for="ShowContactUs">
					<%=Html._("OFMAdmin.CreateNewSite.ShowContactUs")%>
				</label>
				<br />
				<%=Html.CheckBoxFor(m => m.ShowSpecialProd, new { id = "ShowSpecialProd", @checked = "checked" })%>
				<label for="ShowSpecialProd">
					<%=Html._("OFMAdmin.CreateNewSite.ShowSpecialProd")%>
				</label>
				<br />
				<%=Html.CheckBoxFor(m => m.UseCompanyNews, new { id = "UseCompanyNews", @checked = "checked" })%>
				<label for="UseCompanyNews">
					<%=Html._("OFMAdmin.CreateNewSite.UseCompanyNews")%>
				</label>
				<br />
				<%=Html.CheckBoxFor(m => m.UseOfmNews, new { id = "UseOfmNews", @checked = "checked" })%>
				<label for="UseOfmNews">
					<%=Html._("OFMAdmin.CreateNewSite.UseOfmNews")%>
				</label>
				<br />
				<%=Html.CheckBoxFor(m => m.UseSMSFeature, new { id = "UseSMSFeature", @checked = "checked" })%>
				<label for="UseSMSFeature">
					<%=Html._("OFMAdmin.CreateNewSite.UseSMSFeature")%>
				</label>
				<br />
			</div>
			<br />
			<label style="font-weight: bold;">
				ฟีเจอร์พิเศษ</label>
			<div style="margin-left: 20px;">
				<%=Html.CheckBoxFor(m => m.IsByPassApprover, new { id = "IsByPassApprover"})%>
				<label for="IsByPassApprover">
					<%=Html._("OFMAdmin.CreateNewSite.IsByPassApprover")%>
				</label>
				<br />
				<%=Html.CheckBoxFor(m => m.IsByPassAdmin, new { id = "IsByPassAdmin" })%>
				<label for="IsByPassAdmin">
					<%=Html._("OFMAdmin.CreateNewSite.IsByPassAdmin")%>
				</label>
				<span style="color: red;">* by Budget and Order เท่านั้น</span>
				<br />
				<%=Html.CheckBoxFor(m => m.IsAutoApprove, new { id = "IsAutoApprove" })%>
				<label for="IsAutoApprove">
					<%=Html._("OFMAdmin.CreateNewSite.IsAutoApprove")%>
				</label>
				<br />
				<%=Html.CheckBoxFor(m => m.DefaultDeliCharge, new { id = "DefaultDeliCharge" })%>
				<label for="DefaultDeliCharge">
					<%=Html._("OFMAdmin.CreateNewSite.DefaultDeliCharge")%>
				</label>
				<br />
				<%=Html.CheckBoxFor(m => m.AllowAddProduct, new { id = "AllowAddProduct" })%>
				<label for="AllowAddProduct">
					<%=Html._("OFMAdmin.CreateNewSite.AllowAddProduct")%>
				</label>
				<span style="color: red;">* กรณีเลือกใช้แคตตาล็อดองค์กรเท่านั้น</span>
				<br />
				<%=Html.CheckBoxFor(m => m.ShowOutofStock, new { id = "ShowOutofStock" })%>
				<label for="ShowOutofStock">
					<%=Html._("OFMAdmin.CreateNewSite.ShowOutofStock")%>
				</label>
				<br />
				<%=Html.CheckBoxFor(m => m.ShowOfmCat, new { id = "ShowOfmCat" })%>
				<label for="ShowOfmCat">
					<%=Html._("OFMAdmin.CreateNewSite.ShowOfmCat")%>
				</label>
				<span style="color: red;">* กรณีเลือกใช้แคตตาล็อดองค์กรเท่านั้น</span>
				<br />
				<%=Html.CheckBoxFor(m => m.UseReferenceCode, new { id = "UseReferenceCode" })%>
				<label for="UseReferenceCode">
					<%=Html._("OFMAdmin.CreateNewSite.UseReferenceCode")%>
				</label>
				<br />
			</div>
		</div>
	</fieldset>
	<%=Html.HiddenFor(m => m.IsMember)%>
	<%=Html.HiddenFor(m => m.CustId)%>
	<input type="submit" value="<%=Html._("Button.Save")%>" name="SaveData.x" />
	<%Html.EndForm(); %>
	<%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".number").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
								event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
								(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});

			if ($("#OrderControlType").val() == "ByOrder") {
				$("#BudgetLevelType").hide();
				$("#BudgetPeriodType").hide();
				$("#IsByPassAdmin").attr("disabled", true);
			}

			$("#OrderControlType").change(function () {
				if ($(this).val() == "ByOrder") {
					$("#BudgetLevelType").hide();
					$("#BudgetPeriodType").hide();
					$("#IsByPassAdmin").attr("disabled", true);
				}
				else {
					$("#BudgetLevelType").show();
					$("#BudgetPeriodType").show();
					$("#IsByPassAdmin").removeAttr("disabled");
				}
			});
		});


	</script>
</asp:Content>
