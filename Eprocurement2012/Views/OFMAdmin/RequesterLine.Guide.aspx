﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SetRequesterLine>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p style="float: right;">
					<input type="submit" id="UserInfoGuide" name="UserInfoGuide.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("FinalReview", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: left">
					<input type="submit" id="FinalReview" name="FinalReview.x" value="Next >>" />
					<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				RequesterLine Guide</h2>
		</div>
		<div class="grid_16">
			<img src="/images/Company_sec04.jpg" alt="Company" />
		</div>
		<div style="min-height: 300px;">
		</div>
		<div class="grid_3">
			<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<input type="submit" id="ViewAllCostCenter" name="ViewAllCostCenter.x" value="กำหนดสิทธิ์ตามหน่วยงาน/แผนก" />
			<%Html.EndForm(); %>
		</div>
		<div class="grid_3">
			<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<input type="submit" id="ViewAllUser" name="ViewAllUser.x" value="กำหนดสิทธิ์ตามผู้ใช้งาน" />
			<%Html.EndForm(); %>
		</div>
		<div style="min-height: 500px;">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
