﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.NewCustCompany>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<h2>
		CreateCustCompany</h2>
	<div>
		<%Html.BeginForm("CreateCustCompany", "OFMAdmin", FormMethod.Post);%>
		<div class="divbox">
			<fieldset>
				<legend>ตรวจสอบข้อมุลองค์กร</legend>
				<div>
					<p style="margin-bottom: 0em; display: inline;">
						รหัสองค์กร:</p>
					<%=Html.TextBoxFor(m=>m.CompanyId)%>
					<input type="submit" value="ค้นหา" name="Search.x" />
					<span style="color: Red;">
						<%=TempData["ErrorCompanyId"]%></span>
				</div>
			</fieldset>
		</div>
		<%if (!string.IsNullOrEmpty(Model.DefaultCustId))
	{%>
		<div class="divbox">
			<fieldset>
				<legend>ข้อมูลองค์กร</legend>
				<div>
					<p class="inline" style="margin-bottom: 0em;">
						รหัสองค์กร:</p>
					<%=Model.CompanyId %>
				</div>
				<div>
					<p class="inline" style="margin-bottom: 0em;">
						ชื่อองค์กร(ไทย):</p>
					<%=Model.CompanyThaiName%>
				</div>
				<div>
					<p class="inline" style="margin-bottom: 0em;">
						ชื่อองค์กร(อังกฤษ):</p>
					<%=Model.CompanyEngName%>
				</div>
				<div>
					<p class="inline" style="margin-bottom: 0em;">
						รหัสลูกค้า(ตั้งต้น):</p>
					<%=Model.DefaultCustId%>
				</div>
				<div>
					<p class="inline" style="margin-bottom: 0em;">
						อีเมล์ ผู้ดูแลระบบ:</p>
					<%=Model.UserId %>
				</div>
				<div>
					<p class="inline" style="margin-bottom: 0em;">
						ชื่อผู้ดูแลระบบ:</p>
					<%=Model.DisplayName %>
				</div>
				<br />
				<div>
					<h6>
						ที่อยู่ใบกำกับภาษี</h6>
				</div>
				<div>
					<%=Model.InvAddr1%>
				</div>
				<div>
					<%=Model.InvAddr2%>
				</div>
				<div>
					<%=Model.InvAddr3%>
				</div>
				<div>
					<%=Model.InvAddr4%>
				</div>
			</fieldset>
		</div>
		<div class="divbox">
			<fieldset>
				<legend>เพิ่มรหัสลูกค้า</legend>
				<div id="p_scents">
					<%if (Model.CustId.Any())
	   {
		   int count = 0;
		   foreach (var item in Model.CustId)
		   {%>
					<%if (count == 0)
					{%>
					<p style="margin-bottom: 0em;" for="p_scnts">
						รหัสลูกค้า :
						<input type="text" size="20" name="CustID" value="<%=item%>" maxlength="6"/>
						<a href="#" id="add">Add Input Box</a></p>
					<%}
					else
					{%>
					<p style="margin-bottom: 0em;" for="p_scnts">
						รหัสลูกค้า :
						<input type="text" size="20" name="CustID" value="<%=item%>" maxlength="6"/>
						<a href="#" id="remove">Remove</a></p>
					<%}%>
					<%
			count++;
		   }
	   }
	   else
	   {%>
					<p style="margin-bottom: 0em;" for="p_scnts">
						รหัสลูกค้า :
						<input type="text" size="20" name="CustID" value="" maxlength="6"/>
						<a href="#" id="add">Add Input Box</a></p>
					<%} %>
				</div>
				<input type="submit" value="สร้างข้อมูล" name="CreateCustId.x" />
				<span style="color: Red;">
					<%=TempData["ErrorCustId"]%></span>
			</fieldset>
		</div>
		<%} %>
		<%Html.EndForm();%>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			var result = $('#p_scents');
			var i = $('#p_scents p').size() + 1;

			$('#add').live('click', function () {
				$('<p style="margin-bottom: 0em;" for="p_scnts">รหัสลูกค้า : <input type="text" size="20" name="CustID" value="" maxlength="6"/><a href="#" id="remove"> Remove</a></p>').appendTo(result);
				i++;
				return false;
			});

			$('#remove').live('click', function () {
				if (i > 2) {
					$(this).parents('p').remove();
					i--;
				}
				return false;
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		fieldset p input
		{
			width: 150px;
		}
		.divbox
		{
			margin: 0px 0px 10px 0px;
			padding: 10px 20px 10px 20px;
		}
		.inline
		{
			display: inline-block;
			width: 130px;
		}
	</style>
</asp:Content>
