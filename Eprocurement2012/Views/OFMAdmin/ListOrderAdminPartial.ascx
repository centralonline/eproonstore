﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl<Eprocurement2012.Models.ListOrder>" %>
<div id="search-data">
	<table id="table-order">
		<thead>
			<tr>
				
				<th style="text-align: center">
					<%=Html._("Admin.CompanyInfo.No")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewDataOrder.CustID")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewDataOrder.OrderId")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewDataOrder.OrderStatus")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewDataOrder.OrderDate")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewDataOrder.ApprovrOrderDate")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewOrder.Requester")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewOrder.RequesterName")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewOrder.Phone")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewOrder.CurrentApprover")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewOrder.CurrentApproverName")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewOrder.PreviousApprover")%>
				</th>
				<th style="text-align: center">
					<%=Html._("Order.ViewOrder.PreviousApproverName")%>
				</th>
			</tr>
		</thead>
		<tbody>
			<%int seq = Model.Index;
	 foreach (var order in Model.Orders)
	 {%>
			<tr>
				<td style="text-align: center">
					<%=++seq%>
				</td>
				<td>
					<%=order.Requester.CustId %>
				</td>
				<td>
					<%=Html.ActionLink(order.OrderID, "ViewOrderDetail", new { guid = order.OrderGuid })%>
				</td>
				<td>
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus." + order.Status.ToString())%>
				</td>
				<td style="text-align: center">
					<%=order.OrderDate.ToString(new DateTimeFormat())%>
				</td>
				<td style="text-align: center">
					<%=order.DisplayApproveOrderDate %>
				</td>
				<td>
					<%=order.Requester.UserId %>
				</td>
				<td>
					<%=order.Requester.DisplayName %>
				</td>
				<td>
					<%=order.Requester.TelephoneNumber%>
				</td>
				
					<%if (!string.IsNullOrEmpty(order.CurrentApprover.Approver.UserId))
				   {%>
						<td><%=order.CurrentApprover.Approver.UserId%></td>
				   <%} %>
				   <%else
				   {%>
						<td style="text-align:center;"><span>-</span></td>
				   <%} %>
				
					<%if (!string.IsNullOrEmpty(order.CurrentApprover.Approver.UserId))
				   {%>
						<td><%=order.CurrentApprover.Approver.DisplayName%></td>
				   <%} %>
				   <%else
				   {%>
						<td style="text-align:center;"><span>-</span></td>
				   <%} %>

					<%if (!string.IsNullOrEmpty(order.PreviousApprover.Approver.UserId))
				   {%>
						<td><%=order.PreviousApprover.Approver.UserId%></td>
				   <%} %>
				   <%else
				   {%>
						<td style="text-align:center;"><span>-</span></td>
				   <%} %>

					<%if (!string.IsNullOrEmpty(order.PreviousApprover.Approver.UserId))
				   {%>
						<td><%=order.PreviousApprover.Approver.DisplayName%></td>
				   <%} %>
				   <%else
				   {%>
						<td style="text-align:center;"><span>-</span></td>
				   <%} %>
			</tr>
			<%}%>
		</tbody>
	</table>
</div>
