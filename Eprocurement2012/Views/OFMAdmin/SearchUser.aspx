﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SearchUser>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("OverViewMenuBar");%>
		<h2>
			<%=Html._("OFMAdmin.SearchUser.SpecifiedSearch")%></h2>
		<hr />
		<div>
			<%Html.BeginForm("SearchUser", "OFMAdmin", FormMethod.Post); %>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckUserId, new { id = "UserId" })%><label for="UserId"><%=Html._("OFMAdmin.SearchUser.UserIdEmail")%></label></legend>
					<%=Html.TextBoxFor(m => m.UserId)%>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckUserName, new { id = "UserName" })%><label for="UserName"><%=Html._("OFMAdmin.SearchUser.UserName")%></label></legend>
					<%=Html.TextBoxFor(m => m.UserName)%>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCompanyId, new { id="CompanyId"})%><label for="CompanyId"><%=Html._("OFMAdmin.SearchUser.CompanyId")%></label>
					</legend>
					<%=Html.TextBoxFor(m => m.CompanyId)%>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckUserRoleName, new { id = "UserTypeName" })%><label
							for="UserRoleName"><%=Html._("OFMAdmin.SearchUser.UserRoleName")%></label>
					</legend>
					<%=Html.RadioButtonFor(m => m.UserRoleName, "Approver", new { id = "Approver" })%>
					<label for="Approver">
						<%=Html._("CreateUser.RoleApprover")%></label>
					<%=Html.RadioButtonFor(m => m.UserRoleName, "Requester", new { id = "Requester" })%>
					<label for="Requester">
						<%=Html._("CreateUser.RoleRequester")%></label>
					<%=Html.RadioButtonFor(m => m.UserRoleName, "RootAdmin", new { id = "RootAdmin" })%>
					<label for="RootAdmin">
						<%=Html._("Account.AdminUser.RootAdmin")%></label>
					<%=Html.RadioButtonFor(m => m.UserRoleName, "AssistantAdmin", new { id = "AssistantAdmin" })%>
					<label for="AssistantAdmin">
						<%=Html._("CreateUser.RoleAssistantAdmin")%></label>
					<%=Html.RadioButtonFor(m => m.UserRoleName, "Observer", new { id = "Observer" })%>
					<label for="Observer">
						<%=Html._("CreateUser.RoleObserve")%></label>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckUserStatusName, new { id = "UserStatusName" })%><label
							for="UserStatusName"><%=Html._("OFMAdmin.SearchUser.UserStatusName")%></label>
					</legend>
					<%=Html.RadioButtonFor(m => m.UserStatusName, "Active", new { id = "Active" })%>
					<label for="Active">
						<%=Html._("OFMAdmin.SearchUser.Active")%></label>
					<%=Html.RadioButtonFor(m => m.UserStatusName, "Cancel", new { id = "Cancel" })%>
					<label for="Cancel">
						<%=Html._("OFMAdmin.SearchUser.Cancel")%></label>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCreateBy, new { id = "CreateBy" })%><label for="CreateBy"><%=Html._("OFMAdmin.SearchUser.CreateByEmail")%></label>
					</legend>
					<%=Html.TextBoxFor(m => m.CreateBy)%>
				</fieldset>
			</div>
			<div style="width: 100%; display: inline-block;">
				<input type="submit" value="Search" />
				<span style="color: Red;">
					<%=TempData["ErrorMessage"]%></span>
			</div>
			<%Html.EndForm(); %>
		</div>
		<hr />
		<%if (Model.ShearchUserData != null && Model.ShearchUserData.Any())
	{%>
		<div style="margin: 10px;">
			<table id="sortBy">
				<thead>
					<tr>
						<th>
							<%=Html._("OFMAdmin.Index.Order")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UserId")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UserThaiName")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UserEngName")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.CompanyId")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UserRoleName")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UserStatusName")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UserLastActive")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.PhoneNo")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.ChangePasswordOn")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UserLanguage")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.CreateOn")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.CreateBy")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UpdateOn")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.SearchUser.UpdateBy")%>
						</th>
					</tr>
				</thead>
				<%int count = 0;%>
				<%foreach (var user in Model.ShearchUserData)
	  {%>
				<tr>
					<td>
						<%=++count%>
					</td>
					<td>
						<%=user.UserId%>
					</td>
					<td>
						<%=user.UserThaiName%>
					</td>
					<td>
						<%=user.UserEngName%>
					</td>
					<td>
						<%=user.Company.CompanyId%>
					</td>
					<td>
						<%=user.DisplayUserRoleName%>
					</td>
					<td>
						<%=user.UserStatusName%>
					</td>
					<td>
						<%=user.UserLastActive.ToString(new DateTimeFormat())%>
					</td>
					<td>
						<%=user.UserContact.Phone.PhoneNo%>
						<%if (!string.IsNullOrEmpty(user.UserContact.Phone.Extension))
		{%>
						<%= Html._("OFMAdmin.SearchUser.Extension") + user.UserContact.Phone.Extension%>
						<%} %>
					</td>
					<td>
						<%=user.ChangePasswordOn.ToString(new DateTimeFormat())%>
					</td>
					<td>
						<%=user.UserLanguage%>
					</td>
					<td>
						<%=user.CreateOn.ToString(new DateTimeFormat())%>
					</td>
					<td>
						<%=user.CreateBy%>
					</td>
					<td>
						<%=user.UpdateOn.ToString(new DateTimeFormat())%>
					</td>
					<td>
						<%=user.UpdateBy%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script src="/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<link href="/css/dataTables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(function () {
			$('#sortBy').dataTable();
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
