﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CostCenter>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<h2 id="page-heading">
				<%=Html._("Admin.CostCenter.ContentEdit")%></h2>
			<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
			<%=Html.HiddenFor(m => m.CompanyId)%>
			<%=Html.HiddenFor(m => m.IsCompModelThreeLevel)%>
			<div class="grid_16">
				<p class="btn" style="float: right">
					<input type="submit" name="CreateNewCostCenter.x" value="Create CostCenter" />
					<input type="submit" name="ViewAllCostCenter.x" value="View All CostCenter" /></p>
			</div>
			<div class="grid_8">
				<%if (Model.IsCompModelThreeLevel)
	  { %>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.DepartmentID")%>
					</label>
					<%=Model.CostCenterDepartment.DepartmentID%>
					<%=Html.HiddenFor(m => m.CostCenterDepartment.DepartmentID)%>
					<%=Html.HiddenFor(m => m.DepartmentID) %>
				</div>
				<div class="format">
					<label>
						<%=Html._("Admin.Department.ThaiName")%>
					</label>
					<%=Model.CostCenterDepartment.DepartmentThaiName%>
					<%=Html.HiddenFor(m => m.CostCenterDepartment.DepartmentThaiName)%>
				</div>
				<div class="format">
					<label>
						<%=Html._("Admin.Department.EngName")%>
					</label>
					<%=Model.CostCenterDepartment.DepartmentEngName%>
					<%=Html.HiddenFor(m => m.CostCenterDepartment.DepartmentEngName)%>
				</div>
				<%} %>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.CostCenterID")%>
					</label>
					<%=Model.CostCenterID%>
					<%=Html.HiddenFor(m => m.CostCenterID)%>
				</div>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.OracleCode")%>
					</label>
					<%=Model.OracleCode%>
				</div>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.ThaiName")%>
					</label>
					<%=Html.TextBoxFor(m=>m.CostCenterThaiName)%>
					<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CostCenterThaiName)%></span>
				</div>
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.EngName")%>
					</label>
					<%=Html.TextBoxFor(m=>m.CostCenterEngName)%>
					<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.CostCenterEngName)%></span>
				</div>
				<br />
				<%if (Model.IsByPassAdmin)
	  { %>
				<div class="ContentCostCenterID">
					<%=Html.CheckBoxFor(m => m.UseByPassAdmin, new { id = "UseByPassAdmin" })%>
					<label for="UseByPassAdmin">
						<%=Html._("Admin.CostCenter.IsByPassAdmin")%></label>
				</div>
				<%} %>
				<%if (Model.IsAutoApprove)
	  { %>
				<div class="ContentCostCenterID">
					<%=Html.CheckBoxFor(m => m.UseAutoApprove, new { id = "UseAutoApprove" })%>
					<label for="UseAutoApprove">
						<%=Html._("Admin.CostCenter.IsAutoApprove")%></label>
				</div>
				<%} %>
				<%if (Model.IsByPassApprover)
	  { %>
				<div class="ContentCostCenterID">
					<%=Html.CheckBoxFor(m => m.UseByPassApprover, new { id = "UseByPassApprover" })%>
					<label for="UseByPassApprover">
						<%=Html._("Admin.CostCenter.IsByPassApprover")%></label>
				</div>
				<%} %>
				<div class="ContentCostCenterID">
					<%=Html.CheckBoxFor(m => m.IsDeliCharge, new { id = "IsDeliCharge" })%>
					<label for="IsDeliCharge">
						<%=Html._("Admin.CostCenter.IsDeliCharge")%>
					</label>
				</div>
				<br />
				<div class="format">
					<label>
						<%=Html._("Admin.CostCenter.SelectInvoice")%></label>
					<%=Html.DropDownListFor(m => m.SelectInvoice, new SelectList(Model.ListInvoiceAddress, "CustId", "DisplayInvoiceAddress", Model.ListInvoiceAddress.FirstOrDefault(m => m.CustId == Model.CostCenterCustID).CustId), new { @style = "width: 200px;" })%>
					<input type="submit" id="ShowAddressInv" name="ShowAddressForEdit.x" value="<%=Html._("Admin.CostCenter.ShowInvoiceAddress")%>" />
				</div>
				<br />
				<!-- Start changeInv-->
				<div class="changInv">
					<%if (Model.CostCenterInvoice != null)
	   { %>
					<div>
						<div class="format">
							<label>
								<%=Html._("Admin.CostCenter.CustID")%>
							</label>
							<%=Model.CostCenterCustID%>
							<%=Html.HiddenFor(m => m.CostCenterCustID)%>
						</div>
						<div class="format" style="border-collapse: collapse;">
							<label>
								<%=Html._("Admin.CostCenter.InvoiceAddress")%>
							</label>
							<div class="boxAddress format">
								<%=Model.CostCenterInvoice.Address1%><br />
								<%=Model.CostCenterInvoice.Address2%><br />
								<%=Model.CostCenterInvoice.Address3%><br />
								<%=Model.CostCenterInvoice.Address4%><br />
							</div>
						</div>
					</div>
					<%} %>
					<br />
					<br />
					<%if (Model.CostStatus == Eprocurement2012.Models.CostCenter.CostCenterStatus.Cancel)
	   { %>
					<div class="format">
						<label>
							<%=Html._("Admin.CostCenter.StatusModify")%>
						</label>
						<%=Html.DropDownListFor(m => m.CostStatus, new SelectList(Model.ListCostCenterStatus, "Id", "Name"))%>
					</div>
					<%}
	   else
	   {%>
					<%=Html.HiddenFor(m=>m.CostStatus) %>
					<%} %>
					<!-- Start แสดงผลแบบตาราง-->
					<div class="format">
						<label>
							<%=Html._("Admin.CostCenter.SelectShipping")%>
						</label>
					</div>
					<div class="format">
						<label style="text-align: left; color: Red;">
							<%=TempData["ErrorSelectShipID"]%>
						</label>
					</div>
					<br />
					<div class="format" style="margin-left: 300px;">
						<label>
							&nbsp;</label>
						<input id="UpdateCostCenter" type="submit" name="UpdateCostCenter.x" value="<%=Html._("Button.Save")%>" />
					</div>
					<div>
						<table id="sortBy">
							<thead>
								<tr>
									<th>
									</th>
									<th>
										ลำดับ
									</th>
									<th>
										รหัสสถานที่จัดส่ง
									</th>
									<th>
										ที่อยู่สถานที่จัดส่ง
									</th>
									<th>
										จังหวัด
									</th>
									<th>
										รหัสไปรษณีย์
									</th>
								</tr>
							</thead>
							<%int count = 0;%>
							<%foreach (var shipping in Model.ListShipAddress)
		 {%>
							<tr>
								<td>
									<%if (shipping.ShipID.ToString() == Model.SelectShipID)
		   { %>
									<%=Html.RadioButtonFor(m => m.SelectShipID, shipping.ShipID, new { id = shipping.ShipID, @checked = "checked" })%>
									<%} %>
									<%else
		   { %>
									<%=Html.RadioButtonFor(m => m.SelectShipID, shipping.ShipID, new { id = shipping.ShipID })%>
									<%} %>
								</td>
								<td>
									<%=++count%>
								</td>
								<td>
									<%=shipping.ShipID %>
								</td>
								<td>
									<%=shipping.Address1%><br />
									<%=shipping.Address2%><br />
									<%=shipping.Address3%><br />
								</td>
								<td>
									<%=shipping.Province %>
								</td>
								<td>
									<%=shipping.ZipCode %>
								</td>
							</tr>
							<%} %>
						</table>
					</div>
					<br />
					<br />
					<!-- End แสดงผลแบบตาราง-->
				</div>
				<!-- End changeInv-->
			</div>
			<%Html.EndForm();%>
		</div>
		<br />
		<br />
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.format label
		{
			float: left;
			margin-right: 3%;
			text-align: right;
			width: 30%;
		}
		.forlabel
		{
			width: 250px;
			float: left;
		}
		.boxAddress
		{
			float: left;
			margin-right: 3%;
			text-align: left;
			width: 60%;
		}
		.ContentCostCenterID
		{
			margin-left: 33%;
		}
		.IsByPassAdmin
		{
			margin-left: 28%;
		}
		.IsAutoApprove
		{
			margin-left: 28%;
		}
		.IsByPassApprover
		{
			margin-left: 33%;
		}
		.SubmitCostCenter
		{
			margin-left: 250px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script src="/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<link href="/css/dataTables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(function () {
			$('#sortBy').dataTable();

			$("#SelectInvoice").change(function () {
				$(".changInv").hide();
			});
		});
	</script>
</asp:Content>
