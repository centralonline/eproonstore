﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.EditBudget>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<h2>
			งบประมาณองค์กร</h2>
		<hr />
		<div>
			<%Html.BeginForm("ManageBudget", "OFMAdmin", FormMethod.Post);%>
			<fieldset class="white" style="width: 45%; height: auto; margin: 5px;">
				<legend>แก้ไขงบประมาณ</legend>
				<label style="float: left; margin-right: 3%; text-align: right; width: 10%;">
					รหัสองค์กร:
				</label>
				<%=Html.TextBoxFor(m=>m.Budget.CompanyID)%><br />
				<label style="float: left; margin-right: 3%; text-align: right; width: 10%;">
					รหัสควบคุม:</label>
				<%=Html.TextBoxFor(m => m.Budget.GroupID)%><br />
				<div style="margin-left: 15%;">
					<input type="submit" name="SearchBudget.x" value="Search" />
				</div>
				<span style="color: Red;">
					<%=TempData["ErrorMessage"]%></span> <span style="color: Red;">
						<%= TempData["ErrorCompanyId"]%>
					</span>
			</fieldset>
			<%Html.EndForm();%>
		</div>
		<br />
		<%if (Model.ItemBudget != null && Model.ItemBudget.Any())
	{%>
		<div>
			<div>
				<label>
					รหัสองค์กร :
					<%=Model.Company.CompanyId%></label>
				<%--<input style="margin-left:75%;" type="submit" name="EditBudgetAllCompany.x" value="แก้ไขทั้งองค์กร" />--%>
			</div>
			<br />
			<div>
				<table>
					<thead>
						<tr>
							<th class="center">
								ลำดับที่
							</th>
							<th class="center">
								รหัสองค์กร
							</th>
							<th class="center">
								รหัสควบคุม
							</th>
							<th class="center">
								ช่วงการควบคุม
							</th>
							<th class="center">
								ปี
							</th>
							<th class="center">
								งบประมาณคงเหลือ
							</th>
							<th class="center">
								ตั้งค่า
							</th>
						</tr>
					</thead>
					<%int count = 0;%>
					<%var groupBudget = Model.ItemBudget.GroupBy(b => b.GroupID); %>
					<%foreach (var budget in groupBudget)
	   {%>
					<tr>
						<td class="center">
							<%=++count%>
						</td>
						<td>
							<%=budget.FirstOrDefault().CompanyID%>
						</td>
						<td>
							<%=budget.FirstOrDefault().GroupID%>
						</td>
						<td>
							<%=budget.FirstOrDefault().BudgetPeriod%>
						</td>
						<td class="center">
							<%=budget.FirstOrDefault().Year%>
						</td>
						<td class="right">
							<%=budget.Sum(b => b.RemainBudgetAmt).ToString(new MoneyFormat())%>
						</td>
						<td class="center">
							<%Html.BeginForm();%>
							<input type="submit" name="EditBudget.x" value="แก้ไข" />
							<input type="hidden" name="groupID" value="<%=budget.FirstOrDefault().GroupID%>" />
							<input type="hidden" name="companyID" value="<%=budget.FirstOrDefault().CompanyID%>" />
							<%Html.EndForm();%>
						</td>
					</tr>
					<%} %>
				</table>
			</div>
		</div>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.center
		{
			text-align:center;
		}
		.right
		{
			text-align:right;
		}

	</style>
</asp:Content>
