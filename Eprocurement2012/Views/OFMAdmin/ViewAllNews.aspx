﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.News>>>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<h2 id="page-heading">
			<%=Html._("OFMAdmin.ViewAllNews.News")%></h2>
		<br />
		<%Html.BeginForm("ManageNewsSearch", "OFMAdmin"); %>
		<input type="submit" id="ViewAllNews" name="ViewAllNews.x" value="<%=Html._("Report.Filter.All")%>" />
		<input type="submit" id="CreateNews" name="CreateNews.x" value="<%=Html._("Admin.NewsCompany.Add")%>" />
		<br />
		<br />
		<fieldset class="white">
			<legend><%=Html._("OFMAdmin.ViewAllNews.SearchCompanyId")%></legend>
			<div>
				<%=Html.TextBox("CompanyId")%><input type="submit" id="Search" name="Search.x" value="<%=Html._("Button.Search")%>" />
			</div>
		</fieldset>
		<br />
		<br />
		<div>
			<%if (Model.Value.Any())
			{ %>
			<table>
				<tr>
					<th>
						<%=Html._("Admin.NewsCompany.Sequence")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.ViewAllNews.Company")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.NewsTitle")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.NewsDetail")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.Period")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.NewsStatus")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.ViewAllNews.NewsType")%>
					</th>
					<th>
						<%=Html._("Admin.NewsCompany.Edit")%>
					</th>
				</tr>
				<%int index = 0; foreach (var item in Model.Value)%>
				<%{%>
				<tr>
					<td>
						<%=++index%>
					</td>
					<td>
						<%=item.CompanyID%>
					</td>
					<td>
						<%=item.NewsTitle%>
					</td>
					<td>
						<input type="submit" id="ViewDetail" name="ViewDetail.<%=item.NewsGUID%>.x" value="<%=Html._("Button.ViewDetail")%>" />
					</td>
					<td>
						<%=item.ValidFrom.ToString(new DateFormat()) + " " + Html._("Admin.NewsCompany.To") + " " + item.ValidTo.ToString(new DateFormat())%>
					</td>
					<td>
						<%=item.DisplayNewsStatus%>
					</td>
					<td>
						<%=item.DisplayNewsType%>
					</td>
					<td>
						<input type="submit" id="EditNews" name="EditNews.<%=item.NewsGUID%>.x" value="<%=Html._("Button.Edit")%>" />
					</td>
				</tr>
				<%}
				%>
				<%Html.EndForm(); %>
			</table>
			<%}
	 else
	 {%><h3>
		 <%=Html._("Order.ViewOrder.EmptryOrder")%></h3>
			<%}%>
		</div>
		<%Html.EndForm(); %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
