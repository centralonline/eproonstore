﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.EditBudget>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<h2>
			งบประมาณองค์กร</h2>
		<hr />
		<div>
			<%Html.BeginForm("ManageBudget", "OFMAdmin", FormMethod.Post);%>
			<input type="hidden" name="companyId" value="<%=Model.Budget.CompanyID %>" />
			<input type="hidden" name="groupId" value="<%=Model.Budget.GroupID %>" />
			<fieldset class="white" style="width: 45%; height: auto; margin: 5px;">
				<legend>แก้ไขงบประมาณ</legend>
				<label style="float: left; margin-right: 3%; text-align: right; width: 10%;">
					งบประมาณ:
				</label>
				<%=Html.TextBoxFor(m=>m.NewBudgetAmt)%>
				<span>บาท</span>
				<br />
				<%=Html.RadioButtonFor(m => m.IsCreateBudgetAmt, false, new { id = "AddRemainBudgetAmt" })%>
				<label for="AddRemainBudgetAmt">
					เพิ่มจากงบประมาณที่เหลือ</label>

				<%=Html.RadioButtonFor(m => m.IsCreateBudgetAmt, true, new { id = "CreateBudgetAmt" })%>
				<label for="CreateBudgetAmt">
					สร้างงบประมาณใหม่</label>

				<div style="margin-left: 15%;">
					<input type="submit" name="SaveEditBudget.x" value="Save" />
				</div>
				<span style="color: Red;">
					<%=TempData["ErrorMessage"]%></span>
			</fieldset>
			<%Html.EndForm();%>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
