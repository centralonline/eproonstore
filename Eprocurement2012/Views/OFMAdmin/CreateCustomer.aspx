﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.NewCustMasterData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<h2>
		Create New Customer</h2>
	<div>
		<%Html.BeginForm("CreateCustomer", "OFMAdmin", FormMethod.Post); %>
		<div id="checkout-shipping-Orthershipping">
			<fieldset>
				<legend>ตรวจสอบข้อมูลลูกค้า</legend>
				<div>
					<p style="margin-bottom: 0em; margin-left: 1em; display: inline-block; width: 130px;">
						รหัสลูกค้า:</p>
					<%=Html.TextBox("customerId", Model.CustomerId, new { maxlength = 6 })%>
					<input type="submit" value="Check" name="CheckData.x" />
					<span style="color: Red; margin-left: 10px;">
						<%=TempData["ErrorMessage"]%></span><br />
					<p style="margin-bottom: 0em; margin-left: 1em; display: inline-block; width: 130px;">
						e-Mail:</p>
					<%=Html.TextBox("email", Model.ContactEmail, new { maxlength = 100 })%><br />
				</div>
			</fieldset>
		</div>
		<%Html.EndForm(); %>
	</div>
	<%if (!string.IsNullOrEmpty(Model.CustomerId) && !string.IsNullOrEmpty(Model.ContactEmail))
   {%>
	<div>
		<%Html.BeginForm("CreateCustomer", "OFMAdmin", FormMethod.Post); %>
		<div id="checkout-shipping-contactdata">
			<fieldset>
				<legend>ข้อมูลผู้ติดต่อ</legend>
				<div>
					<p style="margin-bottom: 0em;">
						รหัสลูกค้า:</p>
					<%=Model.CustomerId %>
					<%=Html.HiddenFor(m => m.CustomerId) %>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						e-Mail:</p>
					<%=Model.ContactEmail %>
					<%=Html.HiddenFor(m => m.ContactEmail) %>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						ชื่อ-นามสกุล:</p>
					<%=Html.TextBoxFor(m => m.ContactName, new { maxlength = 50 })%><span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.ContactName)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						เบอร์โทรศัพท์:</p>
					<%=Html.TextBoxFor(m => m.ContactPhoneNumber, new { maxlength = 10 })%><span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.ContactPhoneNumber)%></span>
					เบอร์ต่อ
					<%=Html.TextBoxFor(m => m.ContactPhoneNumberExtension, new { maxlength = 4, style = "width:50px;" })%>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						เบอร์มือถือ:</p>
					<%=Html.TextBoxFor(m => m.ContactMobileNumber, new { maxlength = 10 })%><span style="color: Red"><%=Html.LocalizedValidationMessageFor(m => m.ContactMobileNumber)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						เบอร์แฟ็กซ์:</p>
					<%=Html.TextBoxFor(m => m.ContactFaxNumber, new { maxlength = 10 })%><span style="color: Red"><%=Html.LocalizedValidationMessageFor(m => m.ContactFaxNumber)%></span>
				</div>
			</fieldset>
		</div>
		<div id="checkout-shipping-billingdata">
			<fieldset>
				<legend>ที่อยู่ใบกำกับภาษี</legend>
				<div>
					<p style="margin-bottom: 0em;">
						ที่อยู่ใบกำกับภาษี:</p>
					<%=Html.TextBoxFor(m => m.InvAddr1, new { maxlength = 55 })%><span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.InvAddr1)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
					</p>
					<%=Html.TextBoxFor(m => m.InvAddr2, new { maxlength = 55 })%><span style="color: Red"><%=Html.LocalizedValidationMessageFor(m => m.InvAddr2)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
					</p>
					<%=Html.TextBoxFor(m => m.InvAddr3, new { maxlength = 55 })%><span style="color: Red"><%=Html.LocalizedValidationMessageFor(m => m.InvAddr3)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
					</p>
					<%=Html.TextBoxFor(m => m.InvAddr4, new { maxlength = 55 })%><span style="color: Red"><%=Html.LocalizedValidationMessageFor(m => m.InvAddr4)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						Payment Type:</p>
					<%=Html.DropDownListFor(m => m.PaymentSelected, new SelectList(Model.PaymentTypeList, "NameInDB", "Type"), "กรุณาเลือก")%>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						Discount Rate:</p>
					<%=Html.TextBoxFor(m => m.DiscountRate, new { @class = "number" })%>
				</div>
			</fieldset>
		</div>
		<div id="checkout-shipping-currentshippingdata">
			<fieldset>
				<legend>ข้อมูลสถานที่จัดส่ง</legend>
				<div>
					<p style="margin-bottom: 0em;">
						ชื่อ-นามสกุล:</p>
					<%=Html.TextBoxFor(m => m.ShipAddress1, new { maxlength = 50 })%><span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress1)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						เบอร์ติดต่อกลับ:</p>
					<%=Html.TextBoxFor(m => m.ShipPhoneNumber, new { maxlength = 10 })%><span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.ShipPhoneNumber)%></span> เบอร์ต่อ<%=Html.TextBoxFor(m => m.ShipPhoneNumberExtension, new { maxlength = 4, style = "width:50px;" })%>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						ที่อยู่จัดส่ง:</p>
					<%=Html.TextBoxFor(m => m.ShipAddress2, new { maxlength = 55 })%><span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress2)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
					</p>
					<%=Html.TextBoxFor(m => m.ShipAddress3, new { maxlength = 55 })%><span style="color: Red"><%=Html.LocalizedValidationMessageFor(m => m.ShipAddress3)%></span>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						จังหวัด:</p>
					<%=Html.DropDownListFor(m => m.ProvinceSelected, Model.ProvinceList)%>
				</div>
				<div>
					<p style="margin-bottom: 0em;">
						รหัสไปรษณีย์:</p>
					<%=Html.TextBoxFor(m => m.ShipAddress4, new { maxlength = 5 })%><span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress4)%></span>
				</div>
			</fieldset>
		</div>
		<div style="margin: 0px 0px 10px 0px; padding: 0px 20px 10px 20px;">
			<input type="submit" value="Create Customer" name="SaveData.x" />
		</div>
		<%Html.EndForm(); %>
	</div>
	<%} %>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".number").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
						event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
						(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
