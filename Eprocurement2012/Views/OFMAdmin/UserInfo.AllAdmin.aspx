﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ModelViewListUserData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<br />
			<div style="text-align: center; margin-bottom: 10px;">
				<img src="/images/step3step.png" alt="" /></div>
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p style="float: right;">
					<input type="submit" id="CostCenterGuide" name="CostCenterGuide.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("RequesterLine", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p style="float: right;">
				<input type="submit" id="RequesterLineGuide" name="RequesterLineGuide.x" value="Next >>" /></p>
				<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				<%=Html._("OFMAdmin.UserInfo.AllAdmin")%></h2>
		</div>
		<div class="grid_16">
			<div id="UnderlineLink" align="right">
				<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<table>
					<tr>
						<td>
							<%=Html._("Admin.ViewAllUser.SelectType")%>
							:
							<input type="submit" id="ViewAllAdmin" name="ViewAllAdmin.x" value="<%=Html._("OFMAdmin.UserInfo.AllAdmin")%>" />
							|
							<input type="submit" id="ViewAllUser" name="ViewAllUser.x" value="<%=Html._("OFMAdmin.UserInfo.AllUser")%>" />
						</td>
						<td width="100">
							<input type="submit" id="CreateNewUser" name="CreateNewUser.x" value="<%=Html._("OFMAdmin.UserInfo.Create")%>" />
						</td>
					</tr>
				</table>
				<%Html.EndForm(); %>
			</div>
			<div class="box">
				<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<span style="color:Red;float:right;"><%=TempData["ErrorSetRootAdmin"]%></span>
				<%Html.RenderPartial("DataListUser", Model.UserData); %>
				<%Html.EndForm(); %>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
