﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ShippingData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<h2 id="page-heading">
				ข้อมูลสถานที่จัดส่ง</h2>
			<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<div class="grid_16">
				<p class="btn" style="float: right">
					<input type="submit" name="CreateNewCostCenter.x" value="Create CostCenter" />
					<input type="submit" name="ViewAllCostCenter.x" value="View All CostCenter" /></p>
			</div>
			<%Html.EndForm(); %>
			<%Html.BeginForm("ManageShipping", "OFMAdmin", FormMethod.Post);%>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<%=Html.HiddenFor(m => m.ShipID)%>
			<%=Html.HiddenFor(m => m.PhoneID)%>
			<div>
				<fieldset>
					<legend>ข้อมูลสถานที่จัดส่ง</legend>
					<div class="table-format">
						<%if (Model.ShipID != 0)
		{%>
						<label>
							รหัสลูกค้า:</label>
						<%=Model.CustIdSelected %>
						<%=Html.HiddenFor(m => m.CustIdSelected)%>
						<%}
		else
		{%>
						<label>
							เลือกรหัสลูกค้า</label>
						<%=Html.DropDownListFor(m => m.CustIdSelected, new SelectList(Model.ListCustId), new { @style = "width:100px;" })%>
						<%} %>
					</div>
					<div class="table-format">
						<label>
							ชื่อ-นามสกุล:</label>
						<%=Html.TextBoxFor(m => m.ShipAddress1, new { maxlength = 55 , @style = "width:200px;" })%>
						<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress1)%></span>
					</div>
					<div class="table-format">
						<label>
							เบอร์ติดต่อกลับ:</label>
						<%=Html.TextBoxFor(m => m.ShipPhoneNumber, new { maxlength=10, @style = "width:200px;" })%>
						<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.ShipPhoneNumber)%></span>
						<span>เบอร์ต่อ</span>
						<%=Html.TextBoxFor(m => m.ShipPhoneNumberExtension, new { maxlength = 5, @style = "width:100px;" })%>
					</div>
					<div class="table-format">
						<label>
							ที่อยู่จัดส่ง</label>
						<%=Html.TextBoxFor(m => m.ShipAddress2, new { maxlength = 55, @style = "width:500px;" })%>
						<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress2)%></span>
					</div>
					<div class="table-format">
						<label>
							&nbsp;</label>
						<%=Html.TextBoxFor(m => m.ShipAddress3, new { maxlength = 55, @style = "width:500px;" })%>
					</div>
					<div class="table-format">
						<label>
							จังหวัด</label>
						<%=Html.DropDownListFor(m => m.ProvinceSelected, Model.ProvinceList, new { @style = "width:200px;" })%>
					</div>
					<div class="table-format">
						<label>
							รหัสไปรษณีย์</label>
						<%=Html.TextBoxFor(m => m.ShipAddress4, new { maxlength = 5, @style = "width:200px;"})%>
						<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m => m.ShipAddress4)%></span>
					</div>
					<%if (Model.ShipID == 0)
	   {%>
					<div class="table-format">
						<label>
							&nbsp;</label>
						<input type="submit" name="CreateShipping.x" value="บันทึก" />
					</div>
					<%} %>
					<%else
	   {%>
					<div class="table-format">
						<label>
							&nbsp;</label>
						<input type="submit" name="SaveShipping.x" value="บันทึก" />
						<input type="submit" name="CancelShipping.x" value="ยกเลิก" />
					</div>
					<%} %>
				</fieldset>
			</div>
			<%Html.EndForm();%>
			<div>
				<%if (Model.OtherShippings.Any())
	  {%>
				<fieldset>
					<legend>ข้อมูลสถานจัดส่งอื่นๆ</legend>
					<table>
						<tr>
							<th>
								ลำดับ
							</th>
							<th>
								รหัสลูกค้า
							</th>
							<th>
								ชื่อผู้รับสินค้า
							</th>
							<th>
								เบอร์ติดต่อ
							</th>
							<th>
								ที่อยู่จัดส่ง
							</th>
							<th>
							</th>
						</tr>
						<%int count = 0;
		foreach (var shipping in Model.OtherShippings)
		{%>
						<tr>
							<td>
								<%=++count%>
							</td>
							<td>
								<%=shipping.CustId%>
							</td>
							<td>
								<%=shipping.ShipContactor%>
							</td>
							<td>
								<%=shipping.DisplayShipPhoneNo%>
							</td>
							<td>
								<%=shipping.Address1 %><br />
								<%=shipping.Address2%><br />
								<%=shipping.Address3%><br />
								<%=shipping.Address4%>
							</td>
							<td>
								<%=Html.ActionLink("แก้ไข", "ManageShipping", "OFMAdmin", new { companyId = Model.CompanyId, custId = shipping.CustId, shipId = shipping.ShipID }, null)%>
							</td>
						</tr>
						<%} %>
					</table>
				</fieldset>
				<%} %>
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
