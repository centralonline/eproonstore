﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ProductCatalog>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">

    <h2>ProductControlImport</h2>
		<div class="grid_16">
			<div class="content">
			<p>	<a href="/OFMAdmin/GetImportFile"><img src="/images/excel-8.png" alt="templete excel" /> DownLoad Templete File</a></p>
			
			<%Html.BeginForm("ProductControlImport", "OFMAdmin", FormMethod.Post); %>
				<fieldset>
						<legend>
							<%=Html._("Admin.CompanyInfo.Detail")%></legend>
						<label for="CompanyId">
							<%=Html._("OFMAdmin.Index.CompanyId")%></label>
							<%=Html.TextBoxFor(m => m.CompanyId)%>
						<input type="submit" name="CheckCompany.x" value="Search" /><span style="color: #FF0000"><%=TempData["ErrorSelectCompany"]%></span>
						<%if (Model.Company != null)
						{ %>
						<div style="margin: 20px">
							<label>
								<%=Html._("Admin.CompanyInfo.CompanyId")%>
								:
							</label>
							<%=Model.Company.CompanyId %>,
							<label>
								<%=Html._("Admin.CompanyInfo.CompanyThaiName")%>
								:
							</label>
							<%=Model.Company.CompanyThaiName %>,
							<label>
								<%=Html._("Admin.CompanySetting.PriceType")%>
								:
							</label>
							<%=Model.Company.DisplayPriceType %>
						</div>
						<%} %>
					</fieldset>
						<%=TempData["ImportComplete"]%>
			<%Html.EndForm(); %>
					<%if (Model.Company != null)
					{ %>
					<fieldset>
						<legend><%=Html._("Admin.CompanyInfo.Detail")%></legend>
						<%Html.BeginForm("UploadExcel", "OFMAdmin", FormMethod.Post, new { enctype = "multipart/form-data" }); %>
							<label for="file">File Excel</label>
							<input type="file" name="file" id="file" />
							<br />
							<input class="graybutton" type="submit" value="Import" />
						
						<span class="errormessage">
						<%=TempData["errormessage"]%>
						</span>
						<input id="companyId" name="companyId" type="hidden" value="<%=Model.Company.CompanyId %>" />
						<%Html.EndForm(); %>
					</fieldset>
					<%} %>
			</div>
		
		</div>

</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
