﻿<%@ Control Language="C#" Inherits="System.Web.Mvc.ViewUserControl" %>
<div>
	<h2>
		<%=Html._("OFMAdmin.OverViewMenuBar.SelectData")%></h2>
	<br />
	<ul class="nav subnav">
		<li>
			<%=Html.ActionLink("ภาพรวมลูกค้า", "Index", "OfficemateAdmin")%></li>
		<li>
			<%=Html.ActionLink("ตรวจสอบข้อมูลองค์กร", "SearchCompany", "OfficemateAdmin")%></li>
		<li>
			<%=Html.ActionLink("ตรวจสอบข้อมูลผู้ใช้งาน", "SearchUser", "OfficemateAdmin")%></li>
		<li>
			<%=Html.ActionLink("ตรวจสอบข้อมูลสินค้า", "SearchProduct", "OfficemateAdmin")%></li>
		<li>
			<%=Html.ActionLink("ตรวจสอบข้อมูลการกำหนดสิทธิ์การสั่งซื้อ", "SearchRequesterLine", "OfficemateAdmin")%></li>
		<li>
			<%=Html.ActionLink("ตรวจสอบข้อมูลการส่งเมล์", "SearchMail", "OfficemateAdmin")%></li>
	</ul>
</div>
