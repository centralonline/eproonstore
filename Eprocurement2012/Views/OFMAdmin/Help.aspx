﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="JavasCript" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".LinkMenu").click(function () {
				var id = $(this).attr("href");
				$(".pTitle").css("background-color", "");
				$(id).css("background-color", "#C0C0C0");
			});
		});
	</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.pTitle
		{
			color: #43799B;
			font-size: 16px;
			font-weight: bold;
		}
		a
		{
			color: #41789c; /*font-weight: bold;*/
			text-decoration: none;
		}
		.groove
		{
			border:2px;
			border-color:#e1e1e1;
			border-style: solid;
		}
	</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="MainContent" runat="server">
	<div style="color: #727272; font-weight: normal; font-size: 15px; line-height: 18px;
		padding: 10px;" class="grid_16">
		<div class="grid_4 groove">
			<ul>
				<li><a id="A1" class="LinkMenu" href="#toc-Structure">รูปแบบโครงสร้างองค์กร</a></li>
				<li><a id="A2" class="LinkMenu" href="#toc-Handling">รูปแบบโครงสร้างการจัดการ และควบคุมการสั่งซื้อ</a></li>
				<li><a id="A3" class="LinkMenu" href="#toc-user">ประเภทผู้ใช้งาน</a></li>
				<li><a id="A4" class="LinkMenu" href="#toc-role">สิทธิการใช้งานในส่วนของการสั่งซื้อ</a></li>
				<li><a id="A5" class="LinkMenu" href="#toc-menu">เมนูข้อมูลสินค้า</a></li>
			</ul>
		</div>
		<div class="grid_12" style="position:relative;margin-right:0px;float:right;">
			<p id="toc-Structure" class="pTitle">
				รูปแบบโครงสร้างองค์กร มี 2 รูปแบบ</p>
			<ol>
				<li>แบบ 2 ลำดับชั้น : ในระดับองค์กร และคอร์สเซ็นเตอร์</li>
				<li>แบบ 3 ลำดับชั้น : ในระดับองค์กร, ระดับฝ่าย และคอร์สเซ็นเตอร์</li>
			</ol>
			<p id="toc-Handling" class="pTitle">
				รูปแบบโครงสร้างการจัดการ และควบคุมการสั่งซื้อ มี 3 รูปแบบ</p>
			<ol>
				<li>ระบบจัดการที่มีการควบคุมการสั่งซื้อ (byOrder)<br />
					Description : ระบบจัดการที่มีการควบคุมการสั่งซื้อ โดยมีวงเงินในการอนุมัติ และการพิจารณาของผู้อนุมัติ
					ในแต่ละการสั่งซื้อนั้นๆ </li>
				<li>ระบบจัดการที่มีการควบคุมงบประมาณ (byBudget)<br />
					Description : ระบบจัดการที่มีการควบคุมงบประมาณ โดยมีวงเงินงบประมาณเป็นตัวควบคุม
					ซึ่งสามารถกำหนดการควบคุมตามได้ตามโครงสร้างองค์กร คือ องค์กร, ฝ่าย หรือ คอร์สเซ็นเตอร์
				</li>
				<li>ระบบการจัดการที่มีการควบคุมทั้งงบประมาณองค์กร รวมถึงการควบคุมการพิจารณาใบขอซื้อนั้นๆ
					(byBudget and Order)
					<br />
					Description : ระบบการจัดการที่สามารถควบคุมได้ทั้งงบประมาณการจัดซื้อ รวมถึงการควบคุมเพื่อพิจารณาการขอซื้อจากผู้อนุมัติ
					ในแต่ละการสั่งซื้อนั้นๆ </li>
			</ol>
			<p id="toc-user" class="pTitle">
				ประเภทผู้ใช้งาน มี 3 ประเภท คือ</p>
			<ol>
				<li>ผู้ดูแลระบบ (A : Admin) </li>
				<li>ผู้ใช้งานส่วนของการสั่งซื้อ (U : User) </li>
				<li>เป็นทั้งผู้ดูแลระบบ และผู้ใช้งานส่วนของการสั่งซื้อ (B : Both)</li>
			</ol>
			<p id="toc-role" class="pTitle">
				สิทธิการใช้งานในส่วนของการสั่งซื้อ มี 4 ประเภท คือ</p>
			<ol>
				<li>ผู้อนุมัติใบขอซื้อ (App : Approver) </li>
				<li>ผู้ขอซื้อสินค้า (Req : Requester) </li>
				<li>เป็นทั้งผู้ขอซื้อ และผู้อนุมัติใบขอซื้อ (Bth : Both)</li>
				<li>ไม่มีส่วนการทำงานส่วนใดเลย(Not) * เกิดขึ้นในกรณีประเภทการใช้งานเป็นผู้ดูแลระบบเพียงอย่างเดียว</li>
			</ol>
			<p id="toc-menu" class="pTitle">
				เมนูข้อมูลสินค้า</p>
			<p>
				เป็นการตรวจสอบรายการสินค้า StandardCatalog ของลูกค้าของแต่ละองค์กร ซึ่งสามารถตรวจสอบจากข้อมูลสินค้า
				ได้ 2 ส่วนหลักๆคือ
			</p>
			<ol>
				<li>รายการสินค้าใน Standard Catalog ขององค์กร</li>
				<li>รายการสินค้าที่กำหนดให้กับผู้ขอซื้อแต่ละราย</li>
			</ol>
		</div>
	</div>
</asp:Content>
