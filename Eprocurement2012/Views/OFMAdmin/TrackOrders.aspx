﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.TrackOrders>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<h2>
			<%=Html._("OFMAdmin.TrackOrders")%></h2>
		<hr />
	</div>
	<div>
		<ul id="trackOrder-Tabs" class="tabs">
			<li style="color: #676767 !important;"><b>
				<%=Html._("OFMAdmin.TrackOrders.Select")%>
				>></b></li>
			<li>
				<%=Model.JobStatus == Eprocurement2012.Models.JobStatus.Open ? MvcHtmlString.Create("ใบสั่งซื้อกำลังดำเนินการ") : Html.ActionLink("ใบสั่งซื้อกำลังดำเนินการ", "TrackOrders", "OFMAdmin", new { Page = 1, JobStatus = Eprocurement2012.Models.JobStatus.Open }, null)%></li>
			<li>
				<%=Model.JobStatus == Eprocurement2012.Models.JobStatus.Close ? MvcHtmlString.Create("ใบสั่งซื้อที่เสร็จสิ้น") : Html.ActionLink("ใบสั่งซื้อที่เสร็จสิ้น", "TrackOrders", "OFMAdmin", new { Page = 1, JobStatus = Eprocurement2012.Models.JobStatus.Close }, null)%></li>
		</ul>
		<%if (Model.JobStatus == Eprocurement2012.Models.JobStatus.Open)
	{ %>
		<ul class="tabs" style="color: Maroon; text-decoration: none; font-weight: bold;
			margin-top: 10px;">
			<li style="display: inline; margin-left: 10px;">
				<%=Html.ActionLink(Html._("Report.DateFilter.Today"), "TrackOrders", "OFMAdmin", new { Page = 1, JobStatus = Model.JobStatus, DateFilter = Eprocurement2012.Models.DateFilter.Today }, new { @class = Model.DateFilter.HasValue && Model.DateFilter.Value == Eprocurement2012.Models.DateFilter.Today ? "active-link" : "" })%></li>
			<li style="display: inline; margin-right: 20px;">
				<%=Html.ActionLink(Html._("Report.DateFilter.1To3Day"), "TrackOrders", "OFMAdmin", new { Page = 1, JobStatus = Model.JobStatus, DateFilter = Eprocurement2012.Models.DateFilter.OneToThreeDays }, new { @class = Model.DateFilter.HasValue && Model.DateFilter.Value == Eprocurement2012.Models.DateFilter.OneToThreeDays ? "active-link" : "" })%></li>
			<li style="display: inline; margin-right: 20px;">
				<%=Html.ActionLink(Html._("Report.DateFilter.4To7Day"), "TrackOrders", "OFMAdmin", new { Page = 1, JobStatus = Model.JobStatus, DateFilter = Eprocurement2012.Models.DateFilter.FourToSevenDays }, new { @class = Model.DateFilter.HasValue && Model.DateFilter.Value == Eprocurement2012.Models.DateFilter.FourToSevenDays ? "active-link" : "" })%></li>
			<li style="display: inline; margin-right: 20px;">
				<%=Html.ActionLink(Html._("Report.DateFilter.7DayUp"), "TrackOrders", "OFMAdmin", new { Page = 1, JobStatus = Model.JobStatus, DateFilter = Eprocurement2012.Models.DateFilter.SevenDaysLater }, new { @class = Model.DateFilter.HasValue && Model.DateFilter.Value == Eprocurement2012.Models.DateFilter.SevenDaysLater ? "active-link" : "" })%></li>
		</ul>
		<%} %>
	</div>
	<div>
		<h1>
			<%=Html._("OFMAdmin.TrackOrders.SearchOrder")%></h1>
		<%Html.BeginForm("SearchOrderAdvance", "OFMAdmin", FormMethod.Post, new { id = "formSearchAdvance" }); %>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckCompanyId, new { id = "CheckCompanyId" })%>
			<label for="CheckCompanyId">
				<%=Html._("Order.ViewDataOrder.CompanyId")%></label><br />
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.CompanyId)%>
		</div>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckCustId, new { id = "CheckCustId" })%>
			<label for="CheckCustId">
				<%=Html._("Order.ViewOrder.CustID")%></label><br />
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.CustId)%>
		</div>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderId, new { id = "CheckOrderId" })%>
			<label for="CheckOrderId">
				<%=Html._("Order.ViewDataOrder.OrderId")%></label><br />
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderId)%>
		</div>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckUserId, new { id = "CheckUserId" })%>
			<label for="CheckUserId">
				<%=Html._("Order.ViewOrder.Requester")%></label><br />
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.UserId)%>
		</div>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderDate, new { id = "CheckOrderDate" })%>
			<label for="CheckOrderDate">
				<%=Html._("Order.ViewDataOrder.OrderDate")%></label><br />
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateFrom, new { id = "OrderDateFrom" })%>
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.OrderDateTo, new { id = "OrderDateTo" })%>
		</div>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckAppDate, new { id = "CheckAppDate" })%>
			<label for="CheckAppDate">
				<%=Html._("Order.ViewDataOrder.ApprovrOrderDate")%></label><br />
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.AppDateFrom, new { id = "AppDateFrom" })%>
			<%=Html.TextBoxFor(m => m.AdvanceSearchField.AppDateTo, new { id = "AppDateTo" })%>
		</div>
		<div>
			<%=Html.CheckBoxFor(m => m.AdvanceSearchField.CheckOrderStatus, new { @id = "OrderStatus" })%>
			<label for="CheckOrderStatus">
				<%=Html._("Order.ViewDataOrder.OrderStatus")%></label>
			:<br />
			<div id="divCheckOrderStatus">
				<span style="margin-left: 30px;">
					<%if (Model.JobStatus == Eprocurement2012.Models.JobStatus.Open)
					{ %>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusWaiting, new { id = "Waiting" })%>
					<label for="Waiting">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Waiting")%></label>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusPartial, new { id = "Partial" })%>
					<label for="Partial">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Partial")%></label>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusRevise, new { id = "Revise" })%>
					<label for="Revise">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Revise")%></label>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusAdminAllow, new { id = "AdminAllow" })%>
					<label for="AdminAllow">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.AdminAllow")%></label>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusWaitingAdmin, new { id = "WaitingAdmin" })%>
					<label for="WaitingAdmin">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.WaitingAdmin")%></label>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusApproved, new { id = "Approved" })%>
					<label for="Approved">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Approved")%></label>
					<% }
					else
					{ %>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusShipped, new { id = "Shipped" })%>
					<label for="Shipped">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Shipped")%></label>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusCompleted, new { id = "Completed" })%>
					<label for="Completed">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Completed")%></label>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusDeleted, new { id = "Deleted" })%>
					<label for="Deleted">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Deleted")%></label>
					<%=Html.CheckBoxFor(m => m.AdvanceSearchField.StatusExpired, new { id = "Expired" })%>
					<label for="Expired">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Expired")%></label>
					<%} %>
				</span>
			</div>
		</div>
		<%=Html.Hidden("jobStatus", Model.JobStatus) %>
		<input type="submit" value="<%=Html._("Button.Search")%>" />
		<span style="color: Red;">
			<%=TempData["Message"] %></span>
		<%Html.EndForm(); %>
	</div>
	<br />
	<div>
		<%if (Model.ListOrder.Orders != null)
		{
		if (Model.TotalOrdersCount != 0)
		{%>
		<div class="pager">
			<%if (ViewContext.RouteData.Values["action"].ToString() == "TrackOrders")
			{ %>
			<%=Html.Pager(Model.PageSize, Model.PageNumber, Model.TotalOrdersCount, Model)%>
			<%Model.ListOrder.Index = ((Model.PageNumber - 1) * Model.PageSize);%>
			<%} %>
		</div>
		<div>
			<%if (ViewContext.RouteData.Values["action"].ToString() == "TrackOrders")
			{%>
			<%--<%Html.BeginForm("ExportTrackOrder", "ExportExcel", new { dateFilter = Model.DateFilter.Value, jobStatus = Model.JobStatus }); %>--%>
			<%Html.BeginForm("ExportTrackOrder", "ExportExcel",Model.DateFilter); %>
			<%=Html.Hidden("dateFilter",Model.DateFilter) %>
			<%=Html.Hidden("jobStatus", Model.JobStatus) %>
			<input id="submit" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<%}
			else
			{%>
			<%Html.BeginForm("ExportSearchOrderAdvance", "ExportExcel", Model.AdvanceSearchField); %>
			<%=Html.Hidden("jobStatus", Model.JobStatus) %>
			<input id="submit1" type="submit" value="Export to Excel" />
			<%Html.EndForm(); %>
			<%} %>
		</div>
		<div>
			<%Html.RenderPartial("ListOrderAdminPartial", Model.ListOrder);%>
		</div>
			<%}
			else
			{%>
		<h3>
			<%=Html._("Order.ViewOrder.EmptryOrder")%></h3>
		<%}
		}%>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#OrderDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#OrderDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#AppDateFrom").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#AppDateTo").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			if ($("#OrderStatus").attr("checked")) { $("#divCheckOrderStatus").show(); } else { $("#divCheckOrderStatus").hide(); }


			$("#OrderStatus").live("click", function () {
				if (this.checked) {
					$("#divCheckOrderStatus").show();
				}
				else {
					$("#divCheckOrderStatus").hide();
				}
			});
		});
	</script>
</asp:Content>
