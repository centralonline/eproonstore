﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CompanyDepartment>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post); %>
				<%=Html.HiddenFor(m=> m.CompanyId)%>
				<p class="btn" style="float: right;">
					<input type="submit" id="DepartmentGuide" name="DepartmentGuide.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
				<%=Html.HiddenFor(m=> m.CompanyId)%>
				<p class="btn" style="float: left;">
					<input type="submit" id="CostCenterGuide" name="CostCenterGuide.x" value="Next >>" /></p>
				<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.Department.ContentCreate")%></h2>
		</div>
		<div class="grid_16">
			<div class="grid_8">
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
				<%=Html.HiddenFor(m=> m.CompanyId)%>
				<p class="btn" style="float: right">
					<input type="submit" id="ViewAllDepartment" name="ViewAllDepartment.x" value="View All Department" /></p>
				<%Html.EndForm();%>
				<h5>
					<%=Html._("Admin.Department.Create")%></h5>
				<div class="box" style="margin-top: 15px">
					<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post);%>
					<div class="table-format">
						<label>
							<%=Html._("Admin.Department.DeptID")%></label>
						<%=Html.TextBoxFor(m => m.DepartmentID, new { maxlength = 8, @id = "departmentId" })%>
						<span style="color: Red">
							<%=Html.LocalizedValidationMessageFor(m => m.DepartmentID)%></span>
					</div>
					<div style="margin-left: 23%; margin-bottom: 5px;">
						<label>
							<%=Html._("Admin.CostCenter.ContentCostCenterID")%></label>
					</div>
					<div class="table-format">
						<label>
							<%=Html._("Admin.Department.ThaiName")%></label>
						<%=Html.TextBoxFor(m=>m.DepartmentThaiName)%>
						<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m=>m.DepartmentThaiName)%></span>
					</div>
					<div class="table-format">
						<label>
							<%=Html._("Admin.Department.EngName")%></label>
						<%=Html.TextBoxFor(m=>m.DepartmentEngName)%>
						<span style="color: Red">*<%=Html.LocalizedValidationMessageFor(m=>m.DepartmentEngName)%></span>
					</div>
					<div style="padding-left: 176px; margin-bottom: 5px">
						<%=Html.HiddenFor(m=>m.CompanyId)%>
						<input type="submit" id="SaveNewDepartment" name="SaveNewDepartment.x" value="Save" />
					</div>
					<%Html.EndForm();%>
				</div>
			</div>
		</div>
		<div style="min-height: 500px;">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#departmentId").keypress(function (e) {
				var key = String.fromCharCode(e.which);
				if (isLetter(key) == null) {
					return false;
				}
			});
		});
		function isLetter(s) {
			//return s.match("^[a-zA-Z0-9\(\)]+$");
			return s.match("^[A-Za-z0-9_\-]*$");
		}
	</script>
</asp:Content>
