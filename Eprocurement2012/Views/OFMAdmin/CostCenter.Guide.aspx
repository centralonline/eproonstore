﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%if (Model.IsCompModelThreeLevel)
				{ %>
				<%Html.BeginForm("Department", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: right;">
					<input type="submit" id="DepartmentGuide" name="DepartmentGuide.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
				<%} %>
				<%else
				{ %>
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: right;">
					<input type="submit" id="CompanyGuide" name="CompanyGuide.x" value="Next >>" /></p>
				<%Html.EndForm(); %>
				<%} %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post); %>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: left">
					<input type="submit" id="ViewAllCostCenter" name="ViewAllCostCenter.x" value="Continue" /></p>
				<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.CostCenterGuide")%></h2>
		</div>
		<div class="grid_16">
			<img src="/images/Company_info/Company_sec02_3.jpg" alt="you are here" /><br />
		</div>
		<div style="min-height: 500px;">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
