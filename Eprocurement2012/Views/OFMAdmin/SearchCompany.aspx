﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SearchCompany>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("OverViewMenuBar");%>
		<h2>
			<%=Html._("OFMAdmin.SearchUser.SpecifiedSearch")%></h2>
		<hr />
		<div>
			<%Html.BeginForm("SearchCompany", "OFMAdmin", FormMethod.Post); %>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCompId, new { id="CompanyId" })%><label for="CompanyId"><%=Html._("OFMAdmin.Index.CompanyId")%></label></legend>
					<%=Html.TextBoxFor(m => m.CompanyId)%>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCompName, new { id="CompanyName" })%><label for="CompanyName"><%=Html._("OFMAdmin.Index.CompanyName")%></label></legend>
					<%=Html.TextBoxFor(m => m.CompanyName)%>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCustId, new { id = "CustId" })%><label for="CustId"><%=Html._("OFMAdmin.CreateNewSite.CustId")%></label></legend>
					<%=Html.TextBoxFor(m => m.CustId)%>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCompStatus, new { @id = "CheckCompStatus" })%><label
							for="CheckCompStatus"><%=Html._("OFMAdmin.Index.CompStatus")%></label></legend>
					<%=Html.RadioButtonFor(m => m.CompanyStatus, "Active", new { id = "StatusActive" })%><label
						for="StatusActive">Active</label>
					<%=Html.RadioButtonFor(m => m.CompanyStatus, "Delete", new { id = "StatusDelete" })%><label
						for="StatusDelete">Cancel</label>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckPriceFormat, new { @id = "PriceFormat" })%><label
							for="PriceFormat"><%=Html._("OFMAdmin.CreateNewSite.PriceType")%></label></legend>
					<%=Html.RadioButtonFor(m => m.PriceFormat, "Fix", new { id = "FixPrice" })%><label
						for="FixPrice"><%=Html._("NewSiteData.ListPriceType.Fix")%></label>
					<%=Html.RadioButtonFor(m => m.PriceFormat, "Float", new { id = "FloatPrice" })%><label
						for="FloatPrice"><%=Html._("NewSiteData.ListPriceType.Float")%></label>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckOrderFormat, new { @id = "OrderFormat" })%><label
							for="OrderFormat"><%=Html._("OFMAdmin.CreateNewSite.OrderControlType")%></label></legend>
					<%=Html.RadioButtonFor(m => m.OrderFormat, "ByOrder", new { id = "ByOrder" })%><label
						for="ByOrder"><%=Html._("Model.Company.OrderTypeByOrder")%></label>
					<%=Html.RadioButtonFor(m => m.OrderFormat, "ByBudgetAndOrder", new { id = "ByBudget" })%><label
						for="ByBudget"><%=Html._("Model.Company.OrderTypeByBudget")%></label>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCatalogFormat, new { @id = "CatalogFormat" })%><label
							for="CatalogFormat"><%=Html._("OFMAdmin.CreateNewSite.CompanyCatalog")%></label></legend>
					<%=Html.RadioButtonFor(m => m.CatalogFormat, "Yes", new { id = "CompCat" })%><label
						for="CompCat"><%=Html._("OFMAdmin.CreateNewSite.UseCompanyCatalog")%></label>
					<%=Html.RadioButtonFor(m => m.CatalogFormat, "No", new { id = "OfmCat" })%><label
						for="OfmCat"><%=Html._("OFMAdmin.CreateNewSite.NotUseCompanyCatalog")%></label>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckSiteStatus, new { @id = "CheckSiteStatus" })%><label
							for="CheckSiteStatus"><%=Html._("OFMAdmin.Index.ActiveSite")%></label></legend>
					<%=Html.RadioButtonFor(m => m.SiteStatus, "Yes", new { id = "StatusYes" })%><label
						for="StatusYes"><%=Html._("NewSiteData.SiteActivate.Yes")%></label>
					<%=Html.RadioButtonFor(m => m.SiteStatus, "No", new { id = "StatusNo" })%><label
						for="StatusNo"><%=Html._("NewSiteData.SiteActivate.No")%></label>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCreateDate, new { id = "CreateDate" })%><label for="CreateDate"><%=Html._("OFMAdmin.Index.CreateOn")%></label></legend>
					<%=Html._("Admin.NewsCompany.Since")%>
					<%=Html.TextBox("fromDate")%>
					<%=Html._("Admin.NewsCompany.To")%>
					<%=Html.TextBox("toDate")%>
				</fieldset>
				<fieldset class="white" style="float: left; width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckCreateBy, new { id = "CreateBy" })%><label for="CreateBy"><%=Html._("OFMAdmin.Index.CreateBy")%></label></legend>
					<%=Html.TextBoxFor(m => m.CreateBy)%>
				</fieldset>
			</div>
			<div style="width: 100%; display: inline-block;">
				<input type="submit" value="Search" />
			</div>
			<%Html.EndForm(); %>
		</div>
		<hr />
		<%if (Model.ShearchData != null && Model.ShearchData.Any())
	{ %>
		<div style="margin: 10px;">
			<table>
				<tr>
					<th>
						<%=Html._("OFMAdmin.Index.Order")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.Check")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.CompanyId")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.CompanyName")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.CreateNewSite.CustId")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.Discount")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.SearchCompany.CompanyModel")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.OrderControlType")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.PriceType")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.Catalog")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.CompStatus")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.ActiveSite")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.CreateOn")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.Index.CreateBy")%>
					</th>
					<th>
						<%=Html._("OFMAdmin.SearchCompany.Remark")%>
					</th>
					<%--<th>
						<%=Html._("OFMAdmin.Index.Export")%>
					</th>--%>
				</tr>
				<%Html.BeginForm("CompanyHome", "OFMAdmin", FormMethod.Post); %>
				<%int count = 0;%>
				<%foreach (var company in Model.ShearchData)
	  {%>
				<tr>
					<td>
						<%=++count%>
					</td>
					<td>
						<input type="submit" id="Check_<%=company.CompanyId%>" name="Check.<%=company.CompanyId%>.x"
							value="<%=Html._("OFMAdmin.Index.Check")%>" />
					</td>
					<td>
						<%=company.CompanyId%>
					</td>
					<td>
						<%=company.CompanyName%>
					</td>
					<td>
						<%=company.DefaultCustId%>
					</td>
					<td>
						<%=company.DiscountRate%>
					</td>
					<td>
						<%=company.DisplayCompanyModel%>
					</td>
					<td>
						<%=company.DisplayControlTypeName%>
					</td>
					<td>
						<%=company.DisplayPriceType%>
					</td>
					<td>
						<%=company.DisplayUseCompanyCatalog%>
					</td>
					<td>
						<%=company.DisplayCompanyStatus%>
					</td>
					<td>
						<%=company.DisplayIsSiteActive%>
					</td>
					<td>
						<%=company.CreateOn.ToString(new DateTimeFormat())%>
					</td>
					<td>
						<%=company.CreateBy %>
					</td>
					<td>
						<%=company.SaleRequest%>
					</td>
					<%--<td>
						<input type="submit" id="Export_<%=company.CompanyId%>" name="ViewAllDepartment.<%=company.CompanyId%>.x"
							value="<%=Html._("OFMAdmin.Index.Export")%>" />
					</td>--%>
				</tr>
				<%} %>
				<%Html.EndForm(); %>
			</table>
		</div>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#fromDate").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#toDate").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			//			$("#divCheckCompStatus").hide();
			//			$("#CheckCompStatus").live("click", function () {
			//				if (this.checked) {
			//					$("#divCheckCompStatus").show();
			//				}
			//				else {
			//					$("#divCheckCompStatus").hide();
			//				}
			//			});

			//			$("#divPriceFormat").hide();
			//			$("#PriceFormat").live("click", function () {
			//				if (this.checked) {
			//					$("#divPriceFormat").show();
			//				}
			//				else {
			//					$("#divPriceFormat").hide();
			//				}
			//			});

			//			$("#divOrderFormat").hide();
			//			$("#OrderFormat").live("click", function () {
			//				if (this.checked) {
			//					$("#divOrderFormat").show();
			//				}
			//				else {
			//					$("#divOrderFormat").hide();
			//				}
			//			});

			//			$("#divCatalogFormat").hide();
			//			$("#CatalogFormat").live("click", function () {
			//				if (this.checked) {
			//					$("#divCatalogFormat").show();
			//				}
			//				else {
			//					$("#divCatalogFormat").hide();
			//				}
			//			});

			//			$("#divCheckSiteStatus").hide();
			//			$("#CheckSiteStatus").live("click", function () {
			//				if (this.checked) {
			//					$("#divCheckSiteStatus").show();
			//				}
			//				else {
			//					$("#divCheckSiteStatus").hide();
			//				}
			//			});

		});
	</script>
</asp:Content>
