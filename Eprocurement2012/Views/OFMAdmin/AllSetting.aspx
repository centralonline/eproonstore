﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
			<%Html.BeginForm("FinalReview", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<p class="btn" style="float: right;">
				<input type="submit" id="FinalReview" name="FinalReview.x" value="<< Previous" /></p>
			<%Html.EndForm(); %>
		</div>
		<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
			<%Html.BeginForm("CompanyHome", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<p class="btn" style="float: left;">
				<input type="submit" id="Check_<%=Model.CompanyId%>" name="Check.<%=Model.CompanyId%>.x"
					value="Home" /></p>
			<%Html.EndForm(); %>
		</div>
		<h2 id="page-heading">
			<%=Html._("Admin.AllSetting.Setting")%></h2>
		<div class="grid_16">
			<%Html.BeginForm("AllSetting", "OFMAdmin", FormMethod.Post);%>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<%--<h6>
				<%=Html._("Admin.AllSetting.CompanySetup")%></h6>--%>
			<br />
			<img alt="" src="/images/icon/bank.png" /><input type="submit" id="RequesterLines"
				value="<%=Html._("Admin.AllSetting.FinalReview")%>" name="RequesterLines.x" style="background: none;
				cursor: pointer; width: 150px; text-align: left;" /><br />
			<img alt="" src="/images/icon/bank.png" /><input type="submit" id="CompanyInfo" 
				value="<%=Html._("Admin.AllSetting.CompanyInfo")%>"
				name="CompanyInfo.x" style="background: none; cursor: pointer; width: 150px;
				text-align: left;" /><br />
			<img alt="" src="/images/icon/photography.png" /><input type="submit" id="Submit1"
				value="<%=Html._("Admin.AllSetting.CompanyLogo")%>" name="CompanyLogo.x" style="background: none;
				cursor: pointer; width: 150px; text-align: left;" /><br />
			<img alt="" src="/images/icon/category.png" /><input type="submit" id="Department"
				value="<%=Html._("Admin.AllSetting.Departments")%>" name="Department.x" style="background: none;
				cursor: pointer; width: 150px; text-align: left;" /><br />
			<img alt="" src="/images/icon/category.png" /><input type="submit" id="CostCenter"
				value="<%=Html._("Admin.AllSetting.CostCenters")%>" name="CostCenter.x" style="background: none;
				cursor: pointer; width: 150px; text-align: left;" /><br />
			<img alt="" src="/images/icon/user.png" /><input type="submit" id="UserInfo"
				 value="<%=Html._("Admin.AllSetting.Users")%>" name="UserInfo.x" style="background: none; 
				cursor: pointer; width: 150px; text-align: left;" /><br />
			<img alt="" src="/images/icon/role.png" /><input type="submit" id="Submit2" 
				value="<%=Html._("Admin.AllSetting.RoleUsers")%>" name="RoleUsers.x" style="background: none; 
				cursor: pointer; width: 150px; text-align: left;" /><br />
			<img alt="" src="/images/icon/role.png" /><input type="submit" id="Approvals" 
				value="<%=Html._("Admin.AllSetting.RequesterLinesAndApprovals")%>" name="Approvals.x" style="background: none; 
			   cursor: pointer; width: 150px; text-align: left;" /><br />
			<img alt="" src="/images/icon/config.png" /><input type="submit" id="Submit3" 
				value="<%=Html._("Admin.AllSetting.CompanySetting")%>" name="CompanySetting.x" style="background: none; 
			    cursor: pointer; width: 150px; text-align: left;" /><br />
			<br />
			<%--<h6>
				<%=Html._("Admin.AllSetting.Reporting")%></h6>
			<br />
						<img alt="" src="/images/icon/category.png" /><input type="submit" id="Dashboard"
				value="Dashboard" name="Dashboard.x" style="background: none; cursor: pointer;
				width: 150px; text-align: left;" /><br />
			<img alt="" src="/images/icon/category.png" /><input type="submit" id="BudgetReport"
				value="Budget Report" name="BudgetReport.x" style="background: none; cursor: pointer;
				width: 150px; text-align: left;" /><br />--%>
			<%Html.EndForm();%>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
