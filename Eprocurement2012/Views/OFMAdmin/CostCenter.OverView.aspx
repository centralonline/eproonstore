﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ViewAllCostCenterData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: right;">
					<input type="submit" id="CostCenterGuide" name="CostCenterGuide.x" value="<< Previous" /></p>
				<%Html.EndForm(); %>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<%Html.BeginForm("UserInfo", "OFMAdmin", FormMethod.Post);%>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<p class="btn" style="float: left;">
					<input type="submit" id="UserGuide" name="UserGuide.x" value="Next >>" /></p>
				<%Html.EndForm(); %>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.CostCenter.ContentInfo")%></h2>
		</div>
		<div class="grid_16">
			<%int count = 0; %>
			<%Html.BeginForm("CostCenter", "OFMAdmin", FormMethod.Post);%>
			<%=Html.Hidden("companyId", Model.CompanyId)%>
			<p class="btn" style="float: right">
				<input type="submit" id="CreateNewCostCenter" name="CreateNewCostCenter.x" value="Create CostCenter" /></p>
			<h5>
				<%=Html._("Admin.Department.DeptInfo")%></h5>
			<div class="block" id="tables">
			<%if (Model.CostCenters.Any())
			{ %>
				<table>
					<tr>
						<th>
							<%=Html._("Admin.CompanyInfo.No")%>
						</th>
						<%if (Model.IsCompModelThreeLevel)
						{ %>
						<th>
							<%=Html._("Admin.CostCenter.DepartmentID")%>
						</th>
						<%} %>
						<th>
							<%=Html._("Admin.CostCenter.CostCenterID")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.ThaiName")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.EngName")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.Status")%>
						</th>
						<th>
							<%=Html._("Admin.CostCenter.Modify")%>
						</th>
						<th>
							<%=Html._("Admin.Department.Cancel")%>
						</th>
						<th>
							<%=Html._("Admin.CompanySetting.HeadUser")%>
						</th>
					</tr>
					<%foreach (var item in Model.CostCenters)%>
					<%{%>
					<tr>
						<td>
							<%=++count%>
						</td>
						<%if (Model.IsCompModelThreeLevel)
						{ %>
						<td>
							<%=item.CostCenterDepartment.DepartmentID%>
						</td>
						<%} %>
						<td>
							<%=item.CostCenterID%>
						</td>
						<td>
							<%=item.CostCenterThaiName%>
						</td>
						<td>
							<%=item.CostCenterEngName%>
						</td>
						<td>
							<%=item.CostStatus%>
						</td>
						<td>
							<input type="submit" id="edit_<%=item.CostCenterID%>" value="<%=Html._("Admin.CostCenter.Edit")%>"
								name="EditCostCenter.<%=item.CostCenterID%>.x" />
						</td>
						<td>
							<%if (item.CostStatus == Eprocurement2012.Models.CostCenter.CostCenterStatus.Active)
							{%>
							<input type="submit" id="remove_<%=item.CostCenterID%>" value="Cancel" name="RemoveCostCenter.<%=item.CostCenterID%>.x" />
							<%} %>
						</td>
						<td>
							<input type="submit" id="set_<%=item.CostCenterID%>" value="<%=Html._("Admin.CompanySetting.User")%>"
								name="SetRequesterLine.<%=item.CostCenterID%>.x" />
						</td>
					</tr>
					<%} %>
				</table>
			<%} %>
			</div>
			<%Html.EndForm(); %>
		</div>
		<div style="min-height: 550px;">
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
