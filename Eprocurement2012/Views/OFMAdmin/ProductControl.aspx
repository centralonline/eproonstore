﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ProductCatalog>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<h2>
			<%=Html._("OFMAdmin.ProductControl")%></h2>
		<hr />
		<p>	<a href="/OFMAdmin/ProductControlImport"><img src="/images/excel-8.png" alt="templete excel" width="30" height="30" />นำข้อมูลสินค้าเข้าระบบโดยใช้ File Excel</a></p>
		<div class="grid_16">
			<%Html.BeginForm("ProductControl", "OFMAdmin", FormMethod.Post); %>
			<div class="content">
				<div class="left">
					<fieldset>
						<legend>
							<%=Html._("Admin.CompanyInfo.Detail")%></legend>
						<label for="CompanyId">
							<%=Html._("OFMAdmin.Index.CompanyId")%></label>
							<%=Html.TextBoxFor(m => m.CompanyId)%>
						<input type="submit" name="CheckCompany.x" value="Search" /><span style="color: #FF0000"><%=TempData["ErrorSelectCompany"]%></span>
						<%if (Model.Company != null)
						{ %>
						<div style="margin: 20px">
							<label>
								<%=Html._("Admin.CompanyInfo.CompanyId")%>
								:
							</label>
							<%=Model.Company.CompanyId %>,
							<label>
								<%=Html._("Admin.CompanyInfo.CompanyThaiName")%>
								:
							</label>
							<%=Model.Company.CompanyThaiName %>,
							<label>
								<%=Html._("Admin.CompanySetting.PriceType")%>
								:
							</label>
							<%=Model.Company.DisplayPriceType %>
						</div>
						<%} %>
					</fieldset>
				</div>
				<div class="right">
					<fieldset>
						<legend>
							<%=Html._("Product.Detail.Description")%></legend>
						<label for="productId">
							<%=Html._("ProductCatalog.ProductID")%>:</label>
							<%=Html.TextBoxFor(m => m.ProductId, new { maxlength = "7" })%>
						<input type="submit" name="CheckProductId.x" value="Search" /><span style="color: #FF0000"><%=TempData["ErrorSelectProduct"]%></span>
						<%if (Model.ProductDetail != null)
						{ %>
						<div style="margin: 20px">
							<label>
								<%=Html._("ProductCatalog.HeadProductId")%>
								:
							</label>
							<%=Model.ProductDetail.Id%>,
							<label>
								<%=Html._("ProductCatalog.HeadProductName")%>
								:
							</label>
							<%=Model.ProductDetail.Name%>,
							<label>
								<%=Html._("ProductCatalog.HeadProductPrice")%>
								:
							</label>
							<%=Model.ProductDetail.PriceIncVat%>,
							<label>
								<%=Html._("ProductCatalog.HeadProductUnit")%>
								:
							</label>
							<%=Model.ProductDetail.Unit%>
						</div>
						<%} %>
					</fieldset>
				</div>
			</div>
			<%if (Model.Company != null && Model.ProductDetail != null)
			{ %>
			<div class="content">
				<div class="left">
					<fieldset>
						<legend>
							<%=Html._("SetProductCatalogRequest.SelectType")%></legend>
						<%=Html.RadioButtonFor(o => o.IsByCompany, true, new { id = "IsByCompany", name="IsByCompany", @checked = "checked" })%>
						<label>
							<%=Html._("SetProductCatalogRequest.SelectAllCompany")%></label>
						<%=Html.RadioButtonFor(o => o.IsByCompany, false, new { id = "IsByRequester", name = "IsByCompany" })%>
						<label>
							<%=Html._("SetProductCatalogRequest.SelectByRequester")%></label>
					</fieldset>
					<input id="chkDuringDate" type="checkbox" /><%=Html._("SetProductCatalogRequest.SelectExpireDate")%>
					<div id="divExpireDate">
						<p>
							<label>
								<%=Html._("SetProductCatalogRequest.StartDate")%></label>
							<%=Html.TextBoxFor(o => o.StartDate, new { id = "StartDate", name = "StartDate", maxlength = 10 })%>
						</p>
						<p>
							<label>
								<%=Html._("SetProductCatalogRequest.EndDate")%></label>
							<%=Html.TextBoxFor(o => o.EndDate, new { id = "EndDate", name = "EndDate", maxlength = 10 })%>
						</p>
					</div>
					<br />
					<input id="SaveProduct" name="SaveProduct.x" type="submit" value="Save" /><span style="color: #FF0000"><%=TempData["Error"]%></span>
				</div>
				<div class="right" id="divListUser">
					<%if (Model.UserRequester != null)
					{ %>
					<fieldset>
						<legend>
							<%=Html._("SetProductCatalogRequest.ListRequester")%></legend>
						<table>
							<thead>
								<tr>
									<th>
										<%=Html._("SetProductCatalogRequest.HeadSelectUserId")%>
									</th>
									<th>
										<%=Html._("SetProductCatalogRequest.HeadUserId")%>
									</th>
									<th>
										<%=Html._("SetProductCatalogRequest.HeadUserName")%>
									</th>
								</tr>
							</thead>
							<tbody>
								<%foreach (var item in Model.UserRequester)
								{ %>
								<tr>
									<td>
									<% string reqCheck = "";
									if ((Model.listUser ?? new string[] { }).Any(o => item.UserId == o))
									{
										reqCheck = "checked='checked'";
									}%>
										<input type="checkbox" id="select_<%=item.UserId %>" name="listuserId" class="classlistuserId" value="<%=item.UserId %>" <%=reqCheck%> />
									</td>
									<td>
										<%=item.UserId %>
									</td>
									<td>
										<%=item.DisplayName%>
									</td>
								</tr>
								<%} %>
							</tbody>
						</table>
					</fieldset>
					<%} %>
				</div>
			</div>
			<%} %>
			<%Html.EndForm(); %>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script type="text/javascript" language="javascript">
		$(function () {
			$("#divExpireDate").hide();
			$("#divListUser").hide();
			$(".classlistuserId").attr("disabled", "disabled ");

			if ($('#chkDuringDate:checked').is(':checked')) { $("#divExpireDate").show(); }

			if ($('#IsByCompany').is(':checked')) {
				$(".classlistuserId").attr("disabled", "disabled ");
				$('.classlistuserId').prop('checked', false);
				$("#divListUser").hide();
			}

			if ($('#IsByRequester').is(':checked')) {
				$(".classlistuserId").removeAttr("disabled");
				$("#divListUser").show();
			}

			$("#chkDuringDate").click(function () {
				var isChecked = $('#chkDuringDate:checked').is(':checked');
				if (isChecked) { $("#divExpireDate").show(); }
				else { $("#divExpireDate").hide(); }
			});
			$("#IsByRequester").click(function () {
				var isChecked = $('#IsByRequester').is(':checked');
				if (isChecked) {
					$(".classlistuserId").removeAttr("disabled");
					$("#divListUser").show();
				}
			});
			$("#IsByCompany").click(function () {
				var isChecked = $('#IsByCompany').is(':checked');
				if (isChecked) {
					$(".classlistuserId").attr("disabled", "disabled ");
					$('.classlistuserId').prop('checked', false);
					$("#divListUser").hide();
				}
			});

			$("#StartDate").datepicker({ showOn: "button", buttonImage: "/images/icon/icon_my_catalog.gif", buttonImageOnly: true });
			$("#EndDate").datepicker({ showOn: "button", buttonImage: "/images/icon/icon_my_catalog.gif", buttonImageOnly: true });
			$("#StartDate").datepicker("option", "dateFormat", "dd-mm-yy");
			$("#EndDate").datepicker("option", "dateFormat", "dd-mm-yy");
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.content
		{
			overflow: auto;
			width: 100%;
		}
		.left
		{
			float: left;
			width: 35%;
		}
		.right
		{
			float: right;
			width: 65%;
		}
	</style>
</asp:Content>
