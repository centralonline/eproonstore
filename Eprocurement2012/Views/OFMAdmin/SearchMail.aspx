﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SearchMail>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("OverViewMenuBar");%>
		<h2>
			<%=Html._("OFMAdmin.SearchUser.SpecifiedSearch")%></h2>
		<hr />
		<div>
			<%Html.BeginForm("SearchMail", "OFMAdmin", FormMethod.Post); %>
			<div>
				<fieldset class="white" style="width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckEmailTo, new { id = "CheckEmailTo" })%><label for="CheckEmailTo">ส่งถึง</label>
					</legend>
					<%=Html.TextBoxFor(m => m.EmailTo, new { id = "EmailTo" })%>
				</fieldset>
				<fieldset class="white" style="width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckEmailCC)%><label for="CheckEmailCC">สำเนาถึง</label>
					</legend>
					<%=Html.TextBoxFor(m => m.EmailCC)%>
				</fieldset>
			</div>
			<div>
				<fieldset class="white" style="width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckDate, new { id = "CheckDate" })%><label for="CheckDate">ระบุตามช่วงเวลา</label>
					</legend>
					<span>
						<%=Html._("ตั้งแต่")%><%=Html.TextBox("fromDate")%>
						<%=Html._("เวลา")%>
						<%=Html.TextBox("timeFrom", "00:00", new { @class = "ui-timepicker-div", style = "width: 50px;" })%>
					</span>
					<span>
						<%=Html._("ถึง")%><%=Html.TextBox("toDate")%>
						<%=Html._("เวลา")%>
						<%=Html.TextBox("timeTo", "23:59", new { @class = "ui-timepicker-div", style = "width: 50px;" })%>
					</span>
				</fieldset>
				<fieldset class="white" style="width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckMailSuccess)%><label for="CheckMailSuccess">สถานะส่งเมล์</label>
					</legend>
					<%=Html.RadioButtonFor(m => m.MailSuccess, "Yes", new { id = "Yes" })%>
					<label for="Yes">
						สำเร็จ</label>
					<%=Html.RadioButtonFor(m => m.MailSuccess, "No", new { id = "No" })%>
					<label for="No">
						ไม่สำเร็จ</label>
				</fieldset>
			</div>
			<div style="height: 70%;">
				<fieldset class="white" style="width: 45%; height: 70px; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckOrderId)%><label for="CheckOrderId">เลขที่ใบสั่งซื้อ</label>
					</legend>
					<%=Html.TextBoxFor(m => m.OrderId)%>
				</fieldset>
				<fieldset id="mail" class="white" style="width: 45%; margin: 5px;">
					<legend>
						<%=Html.CheckBoxFor(m => m.CheckMailCategory, new { @id = "MailCategory" })%><label
							for="CheckMailCategory">ประเภทเมล์</label>
					</legend>
					<div id="CheckMailCategory">
						<%=Html.CheckBoxFor(m => m.TypeNotifyProduct, new { id = "NotifyProduct" })%><label
							for="Notify">NotifyProduct</label><br />
						<%=Html.CheckBoxFor(m => m.TypeVerifyUser, new { id = "VerifyUser" })%><label for="Verify">VerifyUser</label><br />
						<%=Html.CheckBoxFor(m => m.TypeForgotPassword, new { id = "ForgotPassword" })%><label
							for="Forgot">ForgotPassword</label><br />
						<%=Html.CheckBoxFor(m => m.TypeApplyNow, new { id = "ApplyNow" })%><label for="ApplyNow">ApplyNow</label><br />
						<%=Html.CheckBoxFor(m => m.TypeCreateOrder, new { id = "CreateOrder" })%><label for="CreateOrder">CreateOrder</label><br />
						<%=Html.CheckBoxFor(m => m.TypeRequestAdminAllowBudget, new { id = "RequestAdminAllowBudget" })%><label
							for="Budget">RequestAdminAllowBudget</label><br />
						<%=Html.CheckBoxFor(m => m.TypeRequestAdminAllowProduct, new { id = "RequestAdminAllowProduct" })%><label
							for="Product">RequestAdminAllowProduct</label><br />
						<%=Html.CheckBoxFor(m => m.TypeRequesterSentReviseOrder, new { id = "RequesterSentReviseOrder" })%><label
							for="Revise">RequesterSentReviseOrder</label><br />
						<%=Html.CheckBoxFor(m => m.TypeApproveOrder, new { id = "ApproveOrder" })%><label
							for="ApproveOrder">ApproveOrder</label><br />
						<%=Html.CheckBoxFor(m => m.TypeApproveAndForwardOrder, new { id = "ApproveAndForwardOrder" })%><label
							for="ForwardOrder">ApproveAndForwardOrder</label><br />
						<%=Html.CheckBoxFor(m => m.TypeApproverDeleteOrder, new { id = "ApproverDeleteOrder" })%><label
							for="Delete">ApproverDeleteOrder</label><br />
						<%=Html.CheckBoxFor(m => m.TypeApproverSentReviseOrder, new { id = "ApproverSentReviseOrder" })%><label
							for="SentRevise">ApproverSentReviseOrder</label><br />
						<%=Html.CheckBoxFor(m => m.TypeAdminAllowBudgetToRequester, new { id = "AdminAllowBudgetToRequester" })%><label
							for="Requester">AdminAllowBudgetToRequester</label><br />
						<%=Html.CheckBoxFor(m => m.TypeAdminAllowBudgetToApprover, new { id = "AdminAllowBudgetToApprover" })%><label
							for="Approver">AdminAllowBudgetToApprover</label><br />
						<%=Html.CheckBoxFor(m => m.TypeRequestSpecialProduct, new { id = "RequestSpecialProduct" })%><label
							for="Special">RequestSpecialProduct</label><br />
						<%=Html.CheckBoxFor(m => m.TypeContactUs, new { id = "ContactUs" })%><label for="ContactUs">ContactUs</label><br />
						<%=Html.CheckBoxFor(m => m.TypeCreateNewSite, new { id = "CreateNewSite" })%><label
							for="NewSite">CreateNewSite</label><br />
						<%=Html.CheckBoxFor(m => m.TypeGoodReceive, new { id = "GoodReceive" })%><label for="GoodReceive">GoodReceive</label>
					</div>
				</fieldset>
			</div>
			<div style="width: 100%; display: inline-block;">
				<input type="submit" value="Search" />
				<span style="color: Red;">
					<%=TempData["ErrorMessage"]%></span>
			</div>
			<%Html.EndForm();%>
		</div>
		<!--แสดงข้อมูล-->
		<%if (Model.MailTransLog != null && Model.MailTransLog.Any())
	{%>
		<%Html.BeginForm("ReSendSearchMail", "OFMAdmin", Model); %>
		<div style="margin: 10px;">
			<table id="sortBy">
				<thead>
					<tr>
						<th>
							<input id="checkAll" type="checkbox" />
							<label for="checkall">
								<%=Html._("ProductCatalog.HeadCheckAll")%></label>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadCount")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadSubject")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadCreateOn")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadSendMailSuccess")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadEmailTo")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadEmailCC")%>
						</th>
						<th>
							<%--<%=Html._("OFMAdmin.ViewOrderDetail.HeadEmailBCC")%>--%>
							<%=Html._("ประเภทเมล์")%>
						</th>
						<th>
							<%=Html._("OFMAdmin.ViewOrderDetail.HeadLogID")%>
						</th>
					</tr>
				</thead>
				<tbody>
					<%
	int countMail = 0;
	foreach (var logMail in Model.MailTransLog)
	{
					%>
					<tr>
						<td>
							<input id="chk<%=logMail.LogID%>" name="listLogId" type="checkbox" value="<%=logMail.LogID + "_"+logMail.Guid%>" />
						</td>
						<td>
							<%=++countMail%>
						</td>
						<td>
							<%=logMail.Subject%>
						</td>
						<td>
							<%=logMail.CreateOn.ToString(new DateTimeFormat())%>
						</td>
						<td>
							<%=logMail.SendMailSuccess%>
						</td>
						<td>
							<%=logMail.EmailTo%>
						</td>
						<td>
							<%=logMail.EmailCC%>
						</td>
						<td>
							<%=logMail.MailCategory%>
						</td>
						<td>
							<%=Html.ActionLink("Detail","ViewOrderMail", "OFMAdmin", new { guid = logMail.Guid, logId = logMail.LogID },null)%>
						</td>
					</tr>
					<%}%>
				</tbody>
			</table>
			<div>
				<input type="submit" name="SendMail" value="Send Mail" />
				<span style="color: Red;">
					<%=TempData["MessageSendMail"]%></span> <span style="color: Red;">
						<%=TempData["SendComplete"]%></span>
			</div>
		</div>
		<%Html.EndForm(); %>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
	<script src="/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<link href="/css/dataTables/jquery.dataTables.css" rel="stylesheet" type="text/css" />
	<script type="text/javascript">
		$(function () {

			if ($("#MailCategory").attr("checked")) { $("#CheckMailCategory").show(); } else { $("#CheckMailCategory").hide(); }

			$("#MailCategory").live("click", function () {
				if (this.checked) {
					$("#CheckMailCategory").show();
					$("#mail").attr("height", "700");
				}
				else {
					$("#CheckMailCategory").hide();
				}
			});

			$('#sortBy').dataTable({
				"bSort": false,
				"aaSorting": [[3, 'Desc']]
			});

			$('#checkAll').click(function () {
				$('td input:checkbox').prop('checked', this.checked);
			});

			$('.paginate_enabled_next').click(function () {
				$("#checkAll").prop("checked", false);
				$('td input:checkbox').prop('checked', false);
			});

			$("#fromDate").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			$("#toDate").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy'
			});

			//เพิ่ม scope ช่วงเวลาในการค้นหา
			$('#timeFrom').timepicker();
			$('#timeTo').timepicker();
		});
	</script>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
