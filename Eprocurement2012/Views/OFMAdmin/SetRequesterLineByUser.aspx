﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SetRequesterLine>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<h2 id="page-heading">
			<%=Html._("Admin.SetRequesterLine")%></h2>
		<div>
			<br />
			<fieldset>
				<%Html.BeginForm("RequesterLineForUser", "OFMAdmin", FormMethod.Post); %>
				<legend>
					<%=Html._("Admin.CostCenterInfo")%></legend>
				<%=Html.Hidden("companyId", Model.CompanyId)%>
				<%=Html.Hidden("costcenterId", Model.CostCenter.CostCenterID)%>
				<%=Html.Hidden("userGuid", Model.UserReqDetail.UserGuid)%>
				<%=Html.Hidden("userRequester", Model.UserReqDetail.UserId)%>
				<%=Html._("Shared.DataUserProfile.UserId")%>:
				<%=Model.UserReqDetail.UserId %><br />
				<%=Html._("Shared.DataUserProfile.DisplayName")%>:
				<%=Model.UserReqDetail.DisplayName %><br />
				<%=Html._("SetRequesterLine.CoscenterId")%>:
				<%=Model.CostCenter.CostCenterID %><br />
				<%=Html._("SetRequesterLine.CoscenterName")%>:
				<%=Model.CostCenter.CostCenterName %><br />
			</fieldset>
			<% if (Model.UserApprover != null)
	  { %>
			<div style="float: left; width: 75%">
				<h3>
					<%=Html._("SetRequesterLine.Approver")%></h3>
				<hr />
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel1")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[0] != null && Model.SelectApprover[0] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[0] == null ? 0 : Model.SelectBudget[0]%>"
						id="UserApprover_First_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[0] != null && Model.SelectParkday[0] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<%=Html._("Admin.SetRequsterLine.Day")%>
				</div>
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel2")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[1] != null && Model.SelectApprover[1] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[1] == null ? 0 : Model.SelectBudget[1]%>"
						id="UserApprover_Second_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[1] != null && Model.SelectParkday[1] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<%=Html._("Admin.SetRequsterLine.Day")%>
				</div>
				<div>
					<%=Html._("SetRequesterLine.ApproverLevel3")%>
					<select name="userApprover">
						<option value="">
							<%=Html._("Admin.CostCenter.SelectList")%></option>
						<%foreach (var app in Model.UserApprover)
		{
			string select = "";
			if (Model.SelectApprover[2] != null && Model.SelectApprover[2] == app.UserId) { select = "selected='selected'"; }%>
						<option value="<%=app.UserId %>" <%=select %>>
							<%=app.DisplayNameAndEmail %></option>
						<%} %>
					</select>
					<%=Html._("SetRequesterLine.ApprovalBudget")%>
					<input type="text" name="approverBudget" value="<%=Model.SelectBudget[2] == null ? 0 : Model.SelectBudget[2]%>"
						id="UserApprover_Third_Budget" maxlength="7" />
					<%=Html._("SetRequesterLine.ApprovalPeriod")%>
					<select name="parkday">
						<%foreach (var item in Model.ListParkDay)
		{
			string select = "";
			if (Model.SelectParkday[2] != null && Model.SelectParkday[2] == Convert.ToInt32(item.Id)) { select = "selected='selected'"; }%>
						<option value="<%=item.Id %>" <%=select %>>
							<%=item.Name%></option>
						<%} %>
					</select>
					<%=Html._("Admin.SetRequsterLine.Day")%>
				</div>
				<input id="SaveRequesterLine" type="submit" value="<%=Html._("Button.Save")%>" name="SaveRequesterLine.x" />
				<%if (String.IsNullOrEmpty(TempData["ErrorResetReqLine"].ToString()))
	  {%>
				<input id="DeleteRequesterLine" name="DeleteRequesterLine.x" type="submit" value="<%=Html._("Button.DeleteRequesterLine")%>" />
				<%} %>
				<input id="SetRequesterLineBack" name="SetRequesterLineBack.x" type="submit" value="<%=Html._("Button.Back")%>" />
				<span style="color: Red">
					<%=TempData["Error"]%>
				</span><span style="color: Blue">
					<%=TempData["Success"]%>
				</span>
				<div>
					<span style="color: Red">
						<%=TempData["ErrorResetReqLine"]%>
					</span>
				</div>
				<%if (String.IsNullOrEmpty(TempData["ErrorResetReqLine"].ToString()))
	  {%>
				<div>
					<span>
						<%=Html._("Content.ResetApprover")%></span>
				</div>
				<%} %>
			</div>
			<%} %>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
