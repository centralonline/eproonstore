﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/OFMAdmin/OFMAdmin.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Company>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<%Html.RenderPartial("CompanySettingMenu", Model.CompanyId);%>
		<div class="grid_16">
			<%Html.BeginForm("CompanyInfo", "OFMAdmin", FormMethod.Post); %>
			<%=Html.Hidden("companyId", Model.CompanyId) %>
			<div style="text-align: center;">
				<br />
				<img src="/images/step03.png" alt="" /></div>
			<div class="grid_2" style="position: absolute; left: 75%; margin-top: 1%;">
				<p class="btn" style="float: right;">
					<input type="submit" id="CompanyLogo" name="CompanyLogo.x" value="<< Previous" /></p>
			</div>
			<div class="grid_2" style="position: absolute; right: 1%; margin-top: 1%;">
				<p class="btn" style="float: left;">
					<input type="submit" id="CompanyReview" name="CompanyReview.x" value="Next >>" /></p>
			</div>
			<h2 id="page-heading">
				<%=Html._("Admin.CompanySetting.User")%></h2>
		</div>
		<div class="grid_2">
			<p style="font-size: 11px; color: #98accb">
				<%=Html._("Admin.CompanySetting.ContentImage")%></p>
		</div>
		<div class="grid_12">
			<fieldset class="white">
				<legend>
					<%=Html._("Admin.CompanySetting.User")%></legend>
				<p class="block">
					<%=Html._("Admin.CompanySetting.RePassword")%></p>
				<%=Html.LocalizedValidationMessageFor(model => model.RePasswordPeriod)%>
				<div>
					<ul class="select-radio-inline">
						<li>
							<%=Html.RadioButtonFor(m => m.RePasswordText, "3Month", new { id = "3Month" })%>
							<label for="3Month">
								<%=Html._("Admin.CompanySetting.3Month")%></label></li>
						<li>
							<%=Html.RadioButtonFor(m => m.RePasswordText, "2Month", new { id = "2Month" })%>
							<label for="2Month">
								<%=Html._("Admin.CompanySetting.2Month")%></label></li>
						<li>
							<%=Html.RadioButtonFor(m => m.RePasswordText, "1Month", new { id = "1Month" })%>
							<label for="1Month">
								<%=Html._("Admin.CompanySetting.1Month")%></label></li>
						<li>
							<%=Html.RadioButtonFor(m => m.RePasswordText, "XMonth", new { id = "XMonth" })%>
							<label for="XMonth">
								<%=Html._("Admin.CompanySetting.XMonth")%></label>
							<%=Html.TextBoxFor(m => m.RePasswordPeriodFromUser, new { size=1})%>
							<%=Html._("Admin.CompanySetting.Day")%></li>
					</ul>
					<p class="block">
						<%=Html._("Admin.CompanySetting.DefaultLang")%></p>
					<ul class="select-radio-inline">
						<li>
							<%=Html.RadioButtonFor(m => m.DefaultLang, "TH", new { id = "THLang" })%>
							<label for="THLang">
								<%=Html._("Admin.CompanySetting.THLang")%></label></li>
						<li>
							<%=Html.RadioButtonFor(m => m.DefaultLang, "EN", new { id = "ENLang" })%>
							<label for="ENLang">
								<%=Html._("Admin.CompanySetting.ENLang")%></label></li>
					</ul>
					<%=Html.LocalizedValidationMessageFor(model => model.DefaultLang)%>
				</div>
			</fieldset>
			<fieldset class="white">
				<legend>
					<%=Html._("Admin.CompanySetting.Order")%></legend>
				<div>
					<ul class="select-radio-inline">
						<li>
							<%=Html._("Admin.CompanySetting.CompanyModel")%>
							<%=Html.RadioButtonFor(m => m.IsCompModelThreeLevel, false, new { id = "2Level" })%>
							<label for="2Level">
								<%=Html._("Admin.CompanySetting.2Level")%></label>
							<%=Html.RadioButtonFor(m => m.IsCompModelThreeLevel, true, new { id = "3Level" })%>
							<label for="3Level">
								<%=Html._("Admin.CompanySetting.3Level")%></label>
							<%=Html.LocalizedValidationMessageFor(model => model.IsCompModelThreeLevel)%>
						</li>
						<li>
							<%=Html._("Admin.CompanySetting.Control")%>
							<%=Html.DropDownListFor(m => m.OrderControlType, new SelectList(Model.ListOrderControlType, "Id", "Name"))%>
							<%=Html.LocalizedValidationMessageFor(model => model.OrderControlType)%>
						</li>
						<li>
							<%=Html._("Admin.CompanySetting.BudgetLevel")%>
							<%=Html.DropDownListFor(m => m.BudgetLevelType, new SelectList(Model.ListBudgetLevelType, "Id", "Name")) %>
							<%=Html.LocalizedValidationMessageFor(model => model.BudgetLevelType)%>
						</li>
						<li>
							<%=Html._("Admin.CompanySetting.BudgetPeriod")%>
							<%=Html.DropDownListFor(m => m.BudgetPeriodType, new SelectList(Model.ListBudgetPeriodType, "Id", "Name")) %>
							<%=Html.LocalizedValidationMessageFor(model => model.BudgetPeriodType)%>
						</li>
						<li>
							<%=Html._("Admin.CompanySetting.PriceType")%>
							<%=Html.DropDownListFor(m => m.PriceType, new SelectList(Model.ListPriceType, "Id", "Name"))%>
							<%=Html.LocalizedValidationMessageFor(model => model.PriceType)%>
						</li>
						<li>
							<%=Html._("Admin.CompanySetting.CompanyCatalog")%>
							<%=Html.RadioButtonFor(m => m.UseOfmCatalog, false, new { id = "UseCompanyCatalog" })%>
							<label for="UseCompanyCatalog">
								<%=Html._("Admin.CompanySetting.UseCompanyCatalog")%></label>
							<%=Html.RadioButtonFor(m => m.UseOfmCatalog, true, new { id = "UseOFMCatalog" })%>
							<label for="UseOFMCatalog">
								<%=Html._("Admin.CompanySetting.UseOFMCatalog")%></label>
							<%=Html.LocalizedValidationMessageFor(model => model.UseOfmCatalog)%>
						</li>
					</ul>
					<p class="block">
						<%=Html._("Admin.CompanySetting.OrderIDFormat")%></p>
					<ul class="select-radio-inline">
						<li>
							<%=Html.RadioButtonFor(m => m.OrderIDFormat, "Standard", new { id = "StandardOrderId" })%>
							<label for="StandardOrderId">
								<%=Html._("Admin.CompanySetting.StandardOrderId")%></label>
							<%=Html.LocalizedValidationMessageFor(model => model.OrderIDFormat)%>
						</li>
						<li>
							<%=Html.RadioButtonFor(m => m.OrderIDFormat, "TypeA", new { id = "OrderIdTypeA" })%>
							<label for="OrderIdTypeA">
								<%=Html._("Admin.CompanySetting.OrderIdTypeA")%></label>
							<u>Ex</u> OFM-MK-120100001 </li>
						<li>
							<%=Html.RadioButtonFor(m => m.OrderIDFormat, "TypeB", new { id = "OrderIdTypeB" })%>
							<label for="OrderIdTypeB">
								<%=Html._("Admin.CompanySetting.OrderIdTypeB")%></label>
							<u>Ex</u> OFM-001-120100001 </li>
						<li>
							<%=Html.RadioButtonFor(m => m.OrderIDFormat, "TypeC", new { id = "OrderIdTypeC" })%>
							<label for="OrderIdTypeC">
								<%=Html._("Admin.CompanySetting.OrderIdTypeC")%></label>
							<u>Ex</u> MK-001-120100001 </li>
						<li>
							<%=Html.RadioButtonFor(m => m.OrderIDFormat, "TypeD", new { id = "OrderIdTypeD" })%>
							<label for="OrderIdTypeD">
								<%=Html._("Admin.CompanySetting.OrderIdTypeD")%></label>
							<u>Ex</u> 001-120100001 </li>
					</ul>
				</div>
			</fieldset>
			<fieldset class="white">
				<legend>
					<%=Html._("Admin.CompanySetting.OtherFeature")%></legend>
				<ul class="select-radio-inline">
					<li>
						<%=Html._("Admin.CompanySetting.ParkDay")%>
						<%=Html.DropDownListFor(m => m.DefaultParkDay, new SelectList(Model.ListParkDay, "Id","Name"))%>
						<%=Html.LocalizedValidationMessageFor(model => model.DefaultParkDay)%></li>
					<li>
						<%=Html.CheckBoxFor(m => m.IsByPassApprover, new { id = "IsByPassApprover" })%>
						<label for="IsByPassApprover">
							<%=Html._("Admin.CompanySetting.IsByPassApprover")%></label></li>
					<li>
						<%=Html.CheckBoxFor(m => m.IsByPassAdmin, new { id = "IsByPassAdmin" })%>
						<label for="IsByPassAdmin">
							<%=Html._("Admin.CompanySetting.IsByPassAdmin")%></label></li>
					<li>
						<%=Html.CheckBoxFor(m => m.IsAutoApprove, new { id = "IsAutoApprove" })%>
						<label for="IsAutoApprove">
							<%=Html._("Admin.CompanySetting.IsAutoApprove")%>
							(Auto Approve)</label></li>
					<li>
						<%=Html.CheckBoxFor(m => m.UseCompanyNews, new { id = "UseCompanyNews" })%>
						<label for="UseCompanyNews">
							<%=Html._("Admin.CompanySetting.UseCompanyNews")%></label></li>
					<li>
						<%=Html.CheckBoxFor(m => m.UseOfmNews, new { id = "UseOfmNews" })%>
						<label for="UseOfmNews">
							<%=Html._("Admin.CompanySetting.UseOfmNews")%></label></li>
					<li>
						<%=Html.CheckBoxFor(m => m.UseSMSFeature, new { id = "UseSMSFeature" })%>
						<label for="UseSMSFeature">
							<%=Html._("Admin.CompanySetting.UseSMSFeature")%></label></li>
					<li>
						<%=Html.CheckBoxFor(m => m.ShowContactUs, new { id = "ShowContactUs" })%>
						<label for="ShowContactUs">
							<%=Html._("Admin.CompanySetting.ShowContactUs")%></label></li>
					<li>
						<%=Html.CheckBoxFor(m => m.ShowSpecialProd, new { id = "ShowSpecialProd" })%>
						<label for="ShowSpecialProd">
							<%=Html._("Admin.CompanySetting.ShowSpecialProd")%></label></li>
				</ul>
			</fieldset>
			<input type="submit" id="UpdateSetting" name="UpdateSetting.x" value="Save" />
			<span style="color: Red;">
				<%=TempData["CompanySettingComplete"]%></span>
		</div>
		<%--<div class="grid_8">
			<p class="btn">
				<input type="submit" id="CompanyLogo" value="Back" name="CompanyLogo.x" /></p>
		</div>
		<div class="grid_8">
			<p class="btn" style="float: right">
				<input type="submit" id="CompanyReview" value="Continue" name="CompanyReview.x" /></p>
		</div>--%>
		<%Html.EndForm();%>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavasCript" runat="server">
</asp:Content>
