﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ContactUs>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<div class="GroupData">
			<h3>
				<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("Home.ContactUs.Title")%></h3>
			<div class="border">
				<p>
					<%=Html._("Home.ContactUs.Content")%></p>
				<div class="eclear">
				</div>
				<%Html.BeginForm("ViewContactUs", "Home", FormMethod.Post); %>
				<div class="position">
					<label>
						<%=Html._("Home.ContactUs.UserName")%>
						:</label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.UserName%></p>
					<%=Html.HiddenFor(m=>m.UserName)%>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Home.ContactUs.Email")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.UserID%></p>
					<%=Html.HiddenFor(m => m.UserID)%>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Home.ContactUs.CompanyId")%>
						:</label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.CompanyId%></p>
					<%=Html.HiddenFor(m => m.CompanyId)%>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Home.ContactUs.CompanyName")%>
						:</label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Model.CompanyName%></p>
					<%=Html.HiddenFor(m => m.CompanyName)%>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Home.ContactUs.PhoneNo")%>
						: <span style="color: Red;">*</span></label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Html.TextBoxFor(m=>m.PhoneNo)%></p>
					<p class="msAlert">
						<%=Html.LocalizedValidationMessageFor(m=>m.PhoneNo)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Home.ContactUs.ContactTitle")%>
						: <span style="color: Red;">*</span></label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Html.DropDownListFor(m => m.ContactTitle, new SelectList(Model.ListContactTitle, "Name", "Name"), Html._("Home.ContactUs.SelectContactTitle"))%></p>
					<p class="msAlert">
						<%=Html.LocalizedValidationMessageFor(m=>m.ContactTitle)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Home.ContactUs.ContactDetail")%>
						: <span style="color: Red;">*</span></label>
				</div>
				<div class="left">
					<p class="textSpanGray">
						<%=Html.TextAreaFor(m=>m.ContactDetail,new { cols = 50, rows = 5})%></p>
					<p class="msAlert">
						<%=Html.LocalizedValidationMessageFor(m=>m.ContactDetail)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="positionBtnContact">
					<input class="graybutton" type="submit" value="<%=Html._("Button.Sent")%>" name="SendContactUs" />
				<span style="color: Red;"><%=TempData["SendmailContactMessage"]%></span>
				</div>

				<div class="eclear">
				</div>
				<%Html.EndForm();%>
				<div class="positionBtnContact">
					<div class="livechatstyle">
						<%=Html._("Home.ContactOfficemate")%></div>
					<p>
						<%=Html._("Home.MoreInformation")%>
					</p>
					<p>
						<img src="/images/theme/Chat.jpg" alt="ภาพประกอบ" />
						<%=Html._("Home.ImageRight")%></p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<!--end border-->
		</div>
	</div>
	<!--end CenterWrap-->
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
		});
	</script>
</asp:Content>
