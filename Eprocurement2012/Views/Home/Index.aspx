﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/Eprocurement.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.HomePageData>" %>

<asp:Content ID="Content3" runat="server" ContentPlaceHolderID="MainContent">
	<div class="GroupData 100percent">
		<h3>
			<img src="../../images/theme/h3-list-icon.png" alt="cart and order" /><%=Html._("Home.Index.ItemInYourCartAndYourOrderOnProcess")%></h3>
		<div class="Column-cart">
			<h4>
				<%=Html._("Home.Index.ItemInYourCart")%></h4>
			<div class="border">
				<h5>
					<%=Html._("Home.Index.YourProductsInCart")%></h5>
				<div class="<%=Html._("Home.Index.CSSCartItem")%>">
					<%if (Model.User.IsRequester)
	   {%>
					<span style="color: Red; font-weight: bold; font-size: 16px;">
						<%=Html.ActionLink(Model.ItemCountQty.ToString(), "CartDetail", "Cart") %>
					</span>
					<%} %>
					<%else
	   {%>
					<span style="color: Red; font-weight: bold; font-size: 16px;">0</span>
					<%} %>
				</div>
				<%if (Model.User.IsRequester)
	  {%>
				<%=Html._("Home.Index.YourProductsInCartUnit")%>
				<%=Html.ActionLink(Model.ItemCount.ToString(), "CartDetail", "Cart")%>
				<%=Html._("Home.Index.YourProductsInCartUnitName")%>
				<%} %>
				<br />
				<br />
				<%if (Model.User.Company.UseGoodReceive && Model.IsGoodReceivePeriod)
	  {%>
				<a href="<%=Url.Action("PurchaseGoodReceiveOrder","Report")%>">
					<h4 style="color: Red; border: none">
						<%=Html._("คุณยังไม่ได้ทำ Good Receive ค่ะ")%></h4>
				</a>
				<%} %>
				<div class="eclear">
				</div>
			</div>
		</div>
		<div>
		</div>
		<div class="Column-order">
			<h4>
				<%=Html._("Home.Index.OrderOnProcess")%></h4>
			<div class="border">
				<h5>
					<%=Html._("SearchDataOrder.SearchMyOrder")%></h5>
				<%Html.BeginForm("SearchMyOrder", "Order", FormMethod.Get);%>
				<%Html.RenderPartial("SearchOrder", Model.SearchOrder);%>
				<input id="redirectUrl" name="redirectUrl" type="hidden" value="<%=Model.RedirectUrl %>" />
				<%Html.EndForm();%>
				<div class="eclear">
				</div>
				<h5>
					<%=Html._("Home.Index.OrderOnProcess")%></h5>
				<p>
					<%=Html._("Home.Index.DoyouhaveOrder")%>
					<%if (Model.ItemOrderProcess.CountOrderWaiting > 0)
	   { %>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderProcess.CountOrderWaiting.ToString(), "GetDataOrderWaitingProcess", "Order")%></span>
					<% }%>
					<%else
	   { %>
					<span>0</span>
					<%} %>
					<%=Html._("Home.Index.Order")%></p>
				<%if (Model.User.IsRequester)
	  { %>
				<div class="eclear">
				</div>
				<h5 style="padding-top: 10px;">
					<%=Html._("Home.Index.OrderOnRevise")%></h5>
				<p>
					<%=Html._("Home.Index.DoyouhaveOrder")%>
					<%if (Model.ItemOrderProcess.CountOrderRevise > 0)
	   { %>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderProcess.CountOrderRevise.ToString(), "GetDataOrderReviseProcess", "Order")%></span>
					<% }%>
					<%else
	   { %>
					<span>0</span>
					<%} %>
					<%=Html._("Home.Index.Order")%></p>
				<%} %>
				<%if (Model.User.IsApprover)
	  { %>
				<div class="eclear">
				</div>
				<h5 style="padding-top: 10px;">
					<%=Html._("Home.Index.OrderOnApprove")%></h5>
				<p>
					<%=Html._("Home.Index.DoyouhaveOrder")%>
					<%if (Model.ItemOrderProcess.CountOrderApprove > 0)
	   { %>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderProcess.CountOrderApprove.ToString(), "GetDataOrderApproveProcess", "Order")%></span>
					<% }%>
					<%else
	   { %>
					<span>0</span>
					<%} %>
					<%=Html._("Home.Index.Order")%></p>
				<%} %>
				<div class="eclear">
				</div>
			</div>
		</div>
		<div class="eclear">
		</div>
	</div>
	<div class="spaceTop">
	</div>
	<div class="GroupData 100percent">
		<h3>
			<img alt="" src="../../images/theme/h3-list-icon.png" /><%=Html._("Home.Index.Overview")%></h3>
		<div class="Column-Chart">
			<div class="border">
				<h5>
					<%=Html._("Home.Index.Orderdetail")%></h5>
				<div style="font-size: 15px; font-weight: 15px; line-height: 25px; color: #666666;">
					<%=Html._("Admin.Index.Order+OrderStatus.Waiting")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Waiting).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Waiting).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.Waiting },null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Admin.Index.Order+OrderStatus.Partial")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Partial).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Partial).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.Partial }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Admin.Index.Order+OrderStatus.WaitingAdmin")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Admin.Index.Order+OrderStatus.AdminAllow")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.AdminAllow).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.AdminAllow).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.AdminAllow }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Admin.Index.Order+OrderStatus.Revise")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Revise).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Revise).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.Revise }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Admin.Index.Order+OrderStatus.Approved")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Approved).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Approved).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.Approved }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Admin.Index.Order+OrderStatus.Shipped")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Shipped).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Shipped).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.Shipped }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Admin.Index.Order+OrderStatus.Completed")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Completed).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Completed).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.Completed }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Admin.Index.Order+OrderStatus.Expired")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Expired).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Expired).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.Expired }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
					<%=Html._("Eprocurement2012.Models.Order+OrderStatus.Deleted")%>:
					<%if (Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Deleted).Any())
	   {%>
					<span class="p-red">
						<%=Html.ActionLink(Model.ItemOrderAllStatus.Where(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Deleted).Count().ToString(), "GetDataOrderForIndex", "Order", new { orderStatus = Eprocurement2012.Models.Order.OrderStatus.Deleted }, null)%></span><br />
					<%} %>
					<%else
	   {%>
					<span>0</span><br />
					<%} %>
				</div>
			</div>
		</div>
	</div>
</asp:Content>
