﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.News>>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<div class="GroupData">
			<%Html.RenderPartial("AllNews", Model);%>
			<%Html.BeginForm("Index", "Home", FormMethod.Post);%>
			<div>
				<input id="button" class="graybutton" type="submit" value="<%=Html._("Button.IndexBack")%>" />
			</div>
			<%Html.EndForm();%>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
<style type="text/css">
		#button
		{
			clear: both;
			width: 140px;
			background-size: 140px 33px;
		}

	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
		});
	</script>
</asp:Content>
