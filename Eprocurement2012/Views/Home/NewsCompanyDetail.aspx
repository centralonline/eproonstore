﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.News>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<%Html.BeginForm("NewsCompany", "Home", FormMethod.Post);%>
		<%=Html.Hidden("newsType", Model.NewsType)%>
		<%Html.RenderPartial("NewsDetail", Model);%><br />
		<div>
			<input class="graybutton" type="submit" id="ViewAll" value="<%=Html._("Button.NewsBack")%>" style="margin-left: 270px;" />
		</div>
		<%Html.EndForm();%>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
		});
	</script>
</asp:Content>
