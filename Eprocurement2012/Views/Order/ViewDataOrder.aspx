﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.MasterPageData<IEnumerable<Eprocurement2012.Models.Order>>>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%if (Model.Value.Count() > 0)
   { %>
	<div class="centerwrap" style="margin-bottom: 30px;">
		<div class="GroupData">
			<h3>
				<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("Order.ViewDataOrder.OrderInformation")%></h3>
		</div>
		<div id="content-shopping-cart-detail">
			<table width="100%" border="1" cellpadding="0" cellspacing="0" class="expandbg" style="margin: auto;
				border: solid 1px #c5c5c5; text-align: center;">
				<tr>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.OrderId")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.OrderDate")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.OrderStatus")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.CompanyId")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.CostCenterId")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ConfirmOrder.Quantity")%>
					</td>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.GrandTotalAmount")%>
					</td>
					<%if (Model.User.IsApprover && Model.User.Company.ShowMultiApprove
						&& Model.Value.Any(o => o.CurrentApprover.Approver.UserId == Model.User.UserId)
						&& (Model.Value.Any(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Waiting)
						|| Model.Value.Any(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Partial)
						|| Model.Value.Any(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.AdminAllow)))
	   {%>
					<%if (Model.User.Company.OrderControlType == Eprocurement2012.Models.Company.OrderControl.ByBudgetAndOrder)
	   {%>
					<td class="thead">
						<%=Html._("Order.ViewDataOrder.RemainBudget")%>
					</td>
					<%}%>
					<td class="thead" style="width: 330px;">
						<%=Html._("Order.ViewDataOrder.OrderConsider")%>
					</td>
					<%} %>
				</tr>
				<%if (Model.Value.Any())%>
				<%{%>
				<%foreach (var item in Model.Value)%>
				<%{ %>
				<tr id="tr_<%=item.OrderGuid%>" class="expandtr">
					<td align="left">
						<%if (item.CurrentApprover.Approver.UserId == Model.User.UserId
						&& (item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.Waiting)
						|| item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin)
						|| item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.Partial)
						|| item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.AdminAllow)))
		{ %>
						<%=Html.ActionLink(item.OrderID, "ViewOrderForApprover", "ApproveOrder", new { Id = item.OrderGuid }, null)%>
						<%}
		else
		{ %>
						<%=Html.ActionLink(item.OrderID, "ViewOrderDetail", "Order", new { Id = item.OrderGuid }, null)%>
						<%} %>
						<%if (item.Status.Equals(Eprocurement2012.Models.Order.OrderStatus.Revise) && ViewContext.RouteData.Values["action"].ToString() == "GetDataOrderReviseProcess")
		{ %>
						<%=Html.ActionLink(Html._("Order.ViewDataOrder.ReviseOrder"), "ViewReviseOrder", "Revise", new { Id = item.OrderGuid }, new { Style = "color:Red" })%>
						<%} %>
					</td>
					<td align="left">
						<%=item.OrderDate.ToString(new DateTimeFormat())%>
					</td>
					<td align="center">
						<%=Html._("Eprocurement2012.Models.Order+OrderStatus." + item.Status.ToString())%>
					</td>
					<td align="left">
						<%=item.Company.CompanyId%>
					</td>
					<td align="left">
						<%="["+item.CostCenter.CostCenterID+"]"+" "+item.CostCenter.CostCenterName%>
					</td>
					<td align="center">
						<%=item.ItemCountOrder%>
					</td>
					<td align="right">
						<%=item.GrandTotalAmt.ToString(new MoneyFormat())%>
					</td>
					<%if (Model.User.IsApprover && Model.User.Company.ShowMultiApprove
						&& Model.Value.Any(o => o.CurrentApprover.Approver.UserId == Model.User.UserId)
						&& (Model.Value.Any(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Waiting)
						|| Model.Value.Any(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.Partial)
						|| Model.Value.Any(o => o.Status == Eprocurement2012.Models.Order.OrderStatus.AdminAllow)))
	   {%>
					<%if (Model.User.Company.OrderControlType == Eprocurement2012.Models.Company.OrderControl.ByBudgetAndOrder)
	   {%>
					<td align="center" style="width: 65px;">
						<%=item.CurrentBudget.ToString(new MoneyFormat())%>
						<img alt="ดูรายละเอียด budget" id="<%=item.OrderGuid%>" cost_id="<%=item.CostCenter.CostCenterID%>"
							class="bgdetail" src="/images/icon/help_content.png" style="cursor: pointer;
							margin-left: 3px; display: none;" title="ดูรายละเอียด budget" />
					</td>
					<%}%>
					<td style="text-align: right;">
						<% Html.BeginForm("HandleDataOrder", "ApproveOrder", FormMethod.Post);%>
						<input name="orderGuid" type="hidden" value="<%=item.OrderGuid%>" />
						<%if (Model.User.IsApprover && string.Equals(item.CurrentApprover.Approver.UserId, Model.User.UserId))
		{%>
						<%if (item.GrandTotalAmt > item.CurrentApprover.ApproveCreditLimit && item.NextApprover != null
							&& (!item.SendToAdminAppBudg || item.AdminAllowFlag)
							&& item.Status != Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin)
		{%>
						<input id="PartialApprove_<%=item.OrderGuid %>" name="PartialApprove.x" type="submit"
							value="Partial Approve" class="Approvebutton PartialApprove" style="margin: 0 5px;" />
						<img id="loadingPartialApprove_<%=item.OrderGuid%>" src="/images/loadingcreateorder.gif"
							style="display: none; width: 100px;" alt="Order" />
						<%} %>
						<%else if (item.GrandTotalAmt < item.CurrentApprover.ApproveCreditLimit
							&& (!item.SendToAdminAppBudg || item.AdminAllowFlag)
							&& item.Status != Eprocurement2012.Models.Order.OrderStatus.WaitingAdmin)
		{%>
						<input id="ApproveOrder_<%=item.OrderGuid %>" name="ApproveOrder.x" type="submit"
							value="Approve" class="Approvebutton ApproveOrder" style="margin: 0 5px;" />
						<img id="loadingApproveOrder_<%=item.OrderGuid%>" src="/images/loadingcreateorder.gif"
							style="display: none; width: 100px;" alt="Order" />
						<%} %>
						<input id="DeleteOrder_<%=item.OrderGuid %>" name="DeleteOrder.x" type="submit" value="Delete"
							class="graybutton DeleteOrder" style="margin: 0 5px;" />
						<img id="loadingDeleteOrder_<%=item.OrderGuid%>" src="/images/loadingcreateorder.gif"
							style="display: none; width: 100px;" alt="Order" />
						<input id="ReviseOrder_<%=item.OrderGuid %>" name="ReviseOrder.x" type="submit" value="Revise"
							class="graybutton ReviseOrder" style="margin: 0 5px;" />
						<img id="loadingReviseOrder_<%=item.OrderGuid%>" src="/images/loadingcreateorder.gif"
							style="display: none; width: 100px;" alt="Order" />
						<%} %>
						<%Html.EndForm();%>
					</td>
					<%} %>
				</tr>
				<%} %>
				<%} %>
			</table>
		</div>
	</div>
	<%} %>
	<%else
   { %>
	<div class="centerwrap" style="text-align: center;">
		<h2>
			<%=Html._("Order.ViewOrder.EmptryOrder")%></h2>
	</div>
	<%} %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});

			$(".PartialApprove").click(function () {
				var PartialApproveId = $(this).attr("id");
				var res = PartialApproveId.split("_");
				$("#" + PartialApproveId).hide();
				$("#loadingPartialApprove_" + res[1]).show();
			});

			$(".ApproveOrder").click(function () {
				var approverId = $(this).attr("id");
				var res = approverId.split("_");
				$("#" + approverId).hide();
				$("#loadingApproveOrder_" + res[1]).show();
			});

			$(".DeleteOrder").click(function () {
				var DeleteOrderId = $(this).attr("id");
				var res = DeleteOrderId.split("_");
				$("#" + DeleteOrderId).hide();
				$("#loadingDeleteOrder_" + res[1]).show();
			});

			$(".ReviseOrder").click(function () {
				var ReviseOrderId = $(this).attr("id");
				var res = ReviseOrderId.split("_");
				$("#" + ReviseOrderId).hide();
				$("#loadingReviseOrder_" + res[1]).show();
			});

			$(".bgdetail").show();

			var container = null;
			var elements = null;
			var oldid = "";

			$('.bgdetail').click(function () {
				container = $(this).parents('table[class="expandbg"]');
				elements = container.children('tr[class="expandtr"]');

				$('tr').removeClass('selected'); // reset selected element

				var id = $(this).attr('id');
				var cost_id = $(this).attr('cost_id');
				var selectedli = $('tr[id="tr_' + id + '"]');

				selectedli.addClass('selected'); // mark new selected element

				var expandUrl = '<%=Url.Action("ExpandBudget", "ApproveOrder")%>';

				if (oldid != id) {
					$('.info-bg').slideUp("fast", function () { $(this).remove(); });
					$.ajax({ url: expandUrl,
						cache: false,
						type: "Get",
						data: ({ costid: cost_id }),
						success: function (data) {
							oldid = id;
							$('.selected').after('<tr class="info-bg"><td colspan="9"><div class="info-cl"></div></td></tr>');
							$('.info-cl').html(data);
							$('.info-bg').slideDown("slow");
						},
						error: function () {
							$('.selected').after('<tr class="info-bg"><td colspan="9"><div style="color:#682929; text-align:center; font-weight:bold;">ขอโทษค่ะ ระบบไม่สามารถแสดงหน้านี้ได้ในขณะนี้</div></td></tr>');
							$('.info-bg').slideDown("slow");
						},
						timeout: function () {
							$('.selected').after('<tr class="info-bg"><td colspan="9"><div style="color:#682929; text-align:center; font-weight:bold;">time out</div></td></tr>');
							$('.info-bg').slideDown("slow");
						}
					});
				}
				else {
					$('.info-bg').slideUp("fast", function () { $(this).remove(); });
					$('tr').removeClass('selected'); // reset selected element

					elements = null;
					container = null;
					oldid = "";
				}
			});
		});
	</script>
</asp:Content>
