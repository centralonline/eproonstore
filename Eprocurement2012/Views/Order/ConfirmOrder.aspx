﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.CartData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});

			$("#confirm_order").click(function () {
				$("#confirm_order").hide();
				$("#loadingCreateOrder").show();
			});
		});
	</script>
	<div class="centerwrap">
		<div class="step">
			<img alt="" src="/images/theme/Step-2.jpg" /></div>
		<div class="GroupData">
			<h3>
				<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("Order.ConfirmOrder")%></h3>
		</div>
		<div id="content-shopping-cart-detail">
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="border: solid 1px #c5c5c5;
				margin-top: 0px;">
				<tr>
					<td class="thead">
						<%=Html._("Order.ConfirmOrder.ProductName")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("Order.ConfirmOrder.Price")%>
						<br />
						<%=Html._("Order.ConfirmOrder.IncVat")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("Order.ConfirmOrder.Price")%>
						<br />
						<%=Html._("Order.ConfirmOrder.ExcVat")%>
					</td>
					<td width="40" class="thead">
						<%=Html._("Order.ConfirmOrder.Quantity")%>
					</td>
					<td width="40" class="thead">
						<%--<%=Html._("Order.ConfirmOrder.Discount")%>--%>
						<%=Html._("CartDetailPartial.Unit")%>
					</td>
					<td width="80" class="thead">
						<%=Html._("Order.ConfirmOrder.Sum")%>
						<%--<br />
							<%=Html._("Order.ConfirmOrder.ExcVat")%>--%>
					</td>
				</tr>
				<%
					foreach (Eprocurement2012.Models.CartDetail Items in Model.Cart.ItemCart)
					{ 
				%>
				<tr>
					<td style="margin-top: 10px;">
						<img src="<%=Html.Encode(Items.Product.ImageSmallUrl) %>" width="50" height="50"
							alt="" style="background-color: #CCCCCC; margin: 10px; margin-top: 0px;" class="left" />
						<strong style="color: #000066;">
							<%=Html._("Order.ConfirmOrder.ProductId")%>
							:
							<%=Html.Encode(Items.Product.Id)%></strong><br />
						<div class="content-shopping-cart-table">
							<p class="productnameCart">
								<%=Html.ActionLink(Items.Product.Name, "Details", "Product", new { Items.Product.Id, title = Items.Product.Name }, null)%>
							</p>
							<%if (Items.IsOfmCatalog)
		 { %>
							<p>
								<%=Html._("Order.ConfirmOrder.NotProductCatalog")%>
							</p>
							<%} %>
						</div>
					</td>
					<td valign="top">
						<%--<div align="center">
								<%=Html.Encode(Items.Product.PriceIncVat.ToString(new MoneyFormat()))%></div>--%>
						<div align="center">
							<%=Html.Encode(Items.ItemIncVatPrice.ToString(new MoneyFormat()))%></div>
					</td>
					<td valign="top">
						<%--<div align="center">
								<%=Html.Encode(Items.Product.PriceExcVat.ToString(new MoneyFormat()))%></div>--%>
						<div align="center">
							<%=Html.Encode(Items.ItemExcVatPrice.ToString(new MoneyFormat()))%></div>
					</td>
					<td valign="top">
						<div align="center">
							<%=Html.Encode(Items.Quantity)%><br />
							<%--<%=Html.Encode(Items.Product.Unit)%>--%>
						</div>
					</td>
					<td valign="top">
						<div align="center">
							<%--<% = Items.ItemPriceDiscount.ToString(new MoneyFormat())%>--%>
							<%=Html.Encode(Items.Product.Unit)%>
						</div>
					</td>
					<td colspan="2" valign="top">
						<div align="center">
							<%=Html.Encode(Items.ItemPriceNet.ToString(new MoneyFormat()))%><%=Html.Hidden("ProductID", Items.Product.Id)%></div>
					</td>
				</tr>
				<%}%>
				<tr style="line-height: 25px">
					<td colspan="1">
						<p class="left" style="color: #a64a00">
						</p>
						<div align="right">
							<%=Html._("Order.ConfirmOrder.Sum")%>&nbsp;</div>
					</td>
					<td valign="top">
						<div align="center">
						</div>
					</td>
					<td valign="top">
						<div align="center">
						</div>
					</td>
					<td valign="top">
						<div align="center">
							<b>
								<% = Html.Encode(Model.Cart.ItemCountCart.ToString())%></b>
							<%--<%=Html._("Order.ConfirmOrder.Piece")%>--%></div>
					</td>
					<td valign="top">
						<div align="center">
							<%--<b><% = Html.Encode(Model.Cart.TotalAllDiscount.ToString(new MoneyFormat()))%></b>--%>
							<b>
								<%=Html._("Order.ConfirmOrder.Piece")%></b>
						</div>
					</td>
					<td valign="top">
						<div align="center">
							<b>
								<% = Html.Encode(Model.Cart.TotalPriceProductWithDisCountAmount.ToString(new MoneyFormat()))%></b></div>
					</td>
				</tr>
			</table>
		</div>
		<div style="width: 100%" class="clear">
			<div id="total" class=" margin-top-10 gray-m">
				<%if (Model.Cart.TotalDeliveryFee > 0)
	  { %>
				<p style="text-align: right">
					<label>
						<%=Html._("Order.ConfirmOrder.TotalDeliveryFee")%></label>
					<span>
						<%=Html.Encode(Model.Cart.TotalDeliveryFee.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("Order.ConfirmOrder.Baht")%>
				</p>
				<%} %>
				<%
					if (Model.Cart.TotalDeliveryCharge > 0)
					{%>
				<p style="text-align: right">
					<label>
						<%=Html._("Order.ConfirmOrder.TotalDeliveryCharge")%></label>
					<span>
						<%=Html.Encode(Model.Cart.TotalDeliveryCharge.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("Order.ConfirmOrder.Baht")%>
				</p>
				<%	} %>
				<p style="text-align: right">
					<label>
						<%=Html._("Order.ConfirmOrder.TotalPriceProductNoneVatWithDisCountAmount")%></label>
					<span>
						<%=Html.Encode(Model.Cart.TotalPriceProductNoneVatWithDisCountAmount.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("Order.ConfirmOrder.Baht")%>
				</p>
				<p style="text-align: right">
					<label>
						<%=Html._("Order.ConfirmOrder.TotalPriceProductExcVatWithDisCountAmount")%></label>
					<span>
						<%=Html.Encode(Model.Cart.TotalPriceProductExcVatWithDisCountAmount.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("Order.ConfirmOrder.Baht")%>
				</p>
				<p style="text-align: right">
					<label>
						<%=Html._("Order.ConfirmOrder.TotalVatAmt")%></label>
					<span>
						<%=Html.Encode(Model.Cart.TotalVatAmt)%></span>&nbsp;<%=Html._("Order.ConfirmOrder.Baht")%>
				</p>
				<div style="border-bottom: 1px solid #999999; width: 370px; float: right; margin-top: 5px;">
				</div>
				<br class="clear" />
				<p style="text-align: right">
					<label>
						<%=Html._("Order.ConfirmOrder.GrandTotalAmt")%></label>
					<span class="price" style="color: #CC0000; font-size: 13px; font-weight: bolder;">
						<%=Html.Encode(Model.Cart.GrandTotalAmt.ToString(new MoneyFormat()))%></span>&nbsp;<%=Html._("Order.ConfirmOrder.Baht")%>
				</p>
			</div>
			<br class="clear" />
		</div>
		<br class="clear" />
		<div class="GroupData">
			<%if (Model.CurrentUsedCostCenter != null)
	 { %>
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ConfirmOrder.CurrentUsedCostCenter")%></h4>
				<div class="border2">
					<% if (Model.Cart.ItemCountCart > 0 && Model.CurrentUsedCostCenter != null)
		{ %>
					<div class="position">
						<label>
							<%=Html._("Order.ConfirmOrder.OrderToCostCenter")%>:
						</label>
					</div>
					<div class="left">
						<span class="textSpanGray">[<%= Model.CurrentUsedCostCenter.CostCenterID%>]
							<%= Model.CurrentUsedCostCenter.CostCenterName%></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewOrder.CustID")%>
							:
						</label>
					</div>
					<div class="left">
						<span class="textSpanGray">
							<%= Model.CurrentUsedCostCenter.CostCenterCustID%></span>
					</div>
					<div class="eclear">
					</div>
					<%} %>
					<%Html.RenderPartial("CostCenterAddress", Model.CurrentUsedCostCenter); %>
					<div class="eclear">
					</div>
				</div>
			</div>
			<!--end Column-Full-->
			<%} %>
			<h4>
				<%=Html._("Order.ConfirmOrder.ListUserApprover")%></h4>
			<div class="border2">
				<div class="ApproveStyle">
					<%Html.RenderPartial("MyApprover", Model); %>
				</div>
				<div class="eclear">
				</div>
			</div>
			<!--Costcenter Budget-->
			<%if (Model.CurrentBudget != null)
	 {%>
			<h4>
				<%=Html._("CartDetail.CurrentBudget")%>[<%= Model.CurrentUsedCostCenter.CostCenterID%>]<%= Model.CurrentUsedCostCenter.CostCenterName%></h4>
			<div class="border2">
				<div class="ApproveStyle">
					<%Html.RenderPartial("CurrentBudget", Model.CurrentBudget); %>
				</div>
				<div class="eclear">
				</div>
			</div>
			<%} %>
			<% Html.BeginForm("CreateOrder", "Order", FormMethod.Post, new { @id = "CreateOrderForm" });  %>
			<h4>
				<%=Html._("Order.ViewOrder.Remark")%></h4>
			<div class="border2">
				<div class="position">
					<label>
						<%=Html._("Order.ViewOrder.ApproverRemark")%>
						:
					</label>
				</div>
				<div class="left">
					<span class="textSpanGray">
						<%=!String.IsNullOrEmpty(Model.ApproverRemark) ? Model.ApproverRemark : "-"%>
						<%=Html.Hidden("ApproverRemark", Model.ApproverRemark)%>
					</span>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Order.ViewOrder.OFMRemark")%>
						:
					</label>
				</div>
				<div class="left">
					<span class="textSpanGray">
						<%=!String.IsNullOrEmpty(Model.OFMRemark) ? Model.OFMRemark : "-"%>
						<%=Html.Hidden("OFMRemark", Model.OFMRemark)%>
					</span>
				</div>
				<div class="eclear">
				</div>
				<div class="position">
					<label>
						<%=Html._("Order.ViewOrder.ReferenceRemark")%>
						:</label>
				</div>
				<div class="left">
					<span class="textSpanGray">
						<%=!String.IsNullOrEmpty(Model.ReferenceRemark) ? Model.ReferenceRemark : "-"%>
						<%=Html.Hidden("ReferenceRemark", Model.ReferenceRemark)%>
					</span>
				</div>
				<div class="eclear">
				</div>
				<!--Show AttachFile -->
				<div class="position">
					<%=Html.HiddenFor(m => m.Cart.CustFileName)%>
					<label>
						<%=Html._("Order.AttachFile")%>
						:</label>
				</div>
				<div class="left">
					<span class="textSpanGray">
						<%if (!String.IsNullOrEmpty(Model.Cart.CustFileName))
		{%>
						<a target="_blank" href="<%=Url.Action("DownloadAttachFile", "Order", new { systemFileName = Model.Cart.SystemFileName})%>">
							<%=Model.Cart.CustFileName %></a>
						<%} %>
						<%else
		{%>
						<span>-</span>
						<%} %>
					</span>
				</div>
				<div class="eclear">
				</div>
				<!--End AttachFile -->
				<%if (Model.IsCallBackRequestStatus)
	  { %>
				<div class="left">
					<span class="textSpanGray">
						<%=Html._("CartDetail.CallBackRequestStatus")%>
					</span>
				</div>
				<div class="eclear">
				</div>
				<%} %>
				<%if (Model.IsAutoApprove)
	  { %>
				<div class="left">
					<span class="textSpanGray">
						<%=Html._("Admin.CostCenter.IsAutoApprove")%>
					</span>
				</div>
				<div class="eclear">
				</div>
				<%} %>
			</div>
			<%=Html.Hidden("AutoApprove", Model.AutoApprove)%>
			<%=Html.Hidden("CallBackRequestStatus", Model.CallBackRequestStatus)%>
			<%=Html.Hidden("costcenterId", Model.CurrentUsedCostCenter.CostCenterID)%>
			<%=Html.Hidden("approverId", Model.CurrentSelectApprover.Approver.UserGuid)%>
			<%=Html.Hidden("AdminAllowFlag", Model.AdminAllowFlag)%>
			<%if (Model.Cart.IsOverCreditlimit || Model.Cart.SendToAdminAppBudg)
			{%>
			<div class="border2">
				<%if (Model.Cart.IsOverCreditlimit)
				{%>
				<label style="color: #FF0000; width: 900px;">
					<%=Html._("ConfirmOrder.ErrorOverCreditLimit")%><br />
				</label>
				<%}%>
				<%if (Model.Cart.SendToAdminAppBudg)
				{%>
				<label style="color: #FF0000; width: 900px;">
					<%=Html._("CartDetail.CurrentBudget.Caution")%><br />
				</label>
				<%}%>
			</div>
			<%}%>
			<p>
				<a href="<%=Url.FullUrlAction("CartDetail", "Cart")%>">
					<img src="/images/btn/btn_back.jpg" alt="กลับไปเลือกซื้อสินค้า" class="left" /></a>
				<%if (!Model.Cart.IsOverCreditlimit)
				{%>
				<%if (Model.User.Company.UseSMSFeature)
				{%>
				<!--SMS-Confirm Order-->
				<%--<div style="text-align: right; margin-bottom: 10px;">
					<%=Html.CheckBox("UseSMSFeature", Model.User.Company.UseSMSFeature)%>
					<img src="/images/icon/sms-confirmOrder.jpg" style="border: none;" />
				</div>--%>
				<!--end SMS-Confirm Order-->
				<%}%>
				<input type="image" src="/images/btn/btn_confirm_step.jpg" name="CheckLogIn" alt="ไปหน้าถัดไป"
					id="confirm_order" class="right" />
				<img src="/images/loadingcreateorder.gif" id="loadingCreateOrder" style="display: none"
					alt="Order" class="right" />
				<%}%>
			</p>
			<%Html.EndForm(); %>
		</div>
		<!--GroupData-->
	</div>
	<br class="clear" />
</asp:Content>
