﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SearchOrder>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<div class="GroupData">
			<h3>
				<img src="../../images/theme/h3-list-icon.png" alt="SearchData" /><%=Html._("Order.ViewSearchDataOrder.Head")%></h3>
			<%Html.RenderPartial("SearchOrderData", Model.Orders); %>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});
		});
	</script>
</asp:Content>
