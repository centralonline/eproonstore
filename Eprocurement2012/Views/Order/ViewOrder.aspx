﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<%@ Import Namespace="Eprocurement2012.Models" %>
<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<div class="step noPrint">
			<%if (Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Completed)
	 {%>
			<img alt="Completed" src="/images/theme/Step-5.jpg" />
			<%} %>
			<%else if (Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Shipped)
	 {%>
			<img alt="Shipped" src="/images/theme/Step-4.jpg" />
			<%} %>
			<%else
	 {%>
			<img alt="On Process" src="/images/theme/Step-3.jpg" />
			<%} %>
		</div>
		<div class="GroupData">
			<h3 class="noPrint">
				<img alt="Order Completed" src="/images/theme/h3-list-icon.png" /><%=Html._("Order.ViewOrder.OrderCompleted")%></h3>
			<div class="Column-Full noPrint">
				<h4>
					<%=Html._("Order.ViewOrder.StepProcess")%></h4>
				<div class="border2">
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderId")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderID %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderDate")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderStatus")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Html._(Model.Order.Status.ToResourceKey())%>
							<a href="/Help/OrderInstruction" target="_blank">
								<%=Html._("Order.ViewOrder.OrderDetail")%></a> </span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewOrder.WaitingApproval")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.CurrentApprover != null? Model.Order.CurrentApprover.Approver.DisplayName : Model.Order.PreviousApprover.Approver.DisplayName%></span>
					</div>
					<div class="eclear">
					</div>
					<%Html.RenderPartial("OrderDataFlow", Model.Order); %>
				</div>
			</div>
		</div>
		<!--end GroupData-->
		<!---->
		<div class="eclear">
		</div>
		<!--Print-->
		<div class="eclear">
		</div>
		<div class="box" style="position: absolute; margin-left: 740px; margin-top: 40px;">
			<div class="right noPrint btnPrint" style="background-color: white;">
				<img alt="Print PO (TH)" src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintTHPO"})%>">
					Print PO (TH) </a>
			</div>
			<div class="right noPrint btnPrint" style="background-color: white;">
				<img alt="Print PO (EN)" src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintEnPO"})%>">
					Print PO (EN) </a>
			</div>
			<div class="right noPrint btnPrint" style="background-color: white;">
				<img alt="Print QU (TH)" src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintThQU"})%>">
					Print QU (TH) </a>
			</div>
			<div class="right noPrint btnPrint" style="background-color: white;">
				<img alt="Print QU (EN)" src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "ExportExcel", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintEnQU"})%>">
					Print QU (EN) </a>
			</div>
			<div class="right noPrint btnPrint" style="background-color: white;">
				<img alt="" src="/images/theme/icon_printOrder.jpg" /><a href="#" onclick="window.print(); return false">
					Print web form </a>
			</div>
		</div>
		<div id="print" class="right noPrint btnPrint">
			<img alt="print Order" src="/images/theme/icon_printOrder.jpg" />
			<span>Print Document</span>
		</div>
		<div style="margin: 30px 0;">
			<img alt="e-Procurement" src="/images/theme/logo-ePro.jpg" class="left" />
			<div class="eclear">
			</div>
			<!--Start printOrder detail-->
			<div class="GroupData">
				<div style="text-align: center;">
					<h1>
						<%=Html._("Order.ViewDataOrder.OrderSummary")%></h1>
				</div>
				<div style="width: 100%; padding: 20px 8px 8px 0px; margin-right: 20px; margin-top: 35px;">
					<div class="printOrderlabel">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderId")%>
							:
						</label>
					</div>
					<div style="float: left;">
						<p class="printOrderSpanGray">
							<%=Model.Order.OrderID %></p>
					</div>
					<div class="eclear">
					</div>
				</div>
			</div>
			<!--end GroupData-->
			<div class="left" style="width: 450px; min-height: 250px; padding: 5px; margin-right: 9px;
				margin-top: 0px; border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CustID")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.CostCenter.CostCenterCustID %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("CartDetail.ShipContactor")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.UserId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.Email %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Phone")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorPhone %>
						<%=string.IsNullOrEmpty(Model.Order.Contact.ContactorExtension) ? "" : " #" + Model.Order.Contact.ContactorExtension%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Fax")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorFax %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.InvoiceAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Invoice.Address1 %>
						<%=Model.Order.Invoice.Address2 %>
						<%=Model.Order.Invoice.Address3 %>
						<%=Model.Order.Invoice.Address4 %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="right" style="width: 450px; min-height: 250px; margin-right: 9px; margin-top: 0px;
				border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
				padding: 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.CompanyId")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyId %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CompanyName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.DepartmentName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=string.IsNullOrEmpty(Model.Order.Department.DepartmentID)? "-": "[" + Model.Order.Department.DepartmentID + "] " + Model.Order.Department.DepartmentName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CostCenterName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderDate")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.ShippingAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Shipping.Address1 %>
						<%=Model.Order.Shipping.Address2 %>
						<%=Model.Order.Shipping.Address3 %>
						<%=Model.Order.Shipping.Address4 %>
						<%=Model.Order.Shipping.Province %></p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<!--end printOrder detail-->
			<div class="eclear">
			</div>
			<div class="printOrderlabel" style="padding-top: 20px; padding-bottom: 10px;">
				<label>
					<%=Html._("Order.ViewOrder.OrderItems")%>
				</label>
			</div>
			<%Html.RenderPartial("OrderDetailPartial", Model.Order); %>
			<div class="eclear">
			</div>
			<div class="GroupData">
				<div class="Column-Full">
					<h4>
						<%=Html._("Order.ViewOrder.Remark")%></h4>
					<div class="border2">
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.ApproverRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.ApproverRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.OFMRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.OFMRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.ViewOrder.ReferenceRemark")%>
								:</label>
						</div>
						<div class="left">
							<p class="printOrderSpanGray">
								<%=Model.Order.ReferenceRemark %></p>
						</div>
						<div class="eclear">
						</div>
						<!--Show AttachFile -->
						<div class="Remarklabel">
							<label>
								<%=Html._("Order.AttachFile")%>
								:</label>
						</div>
						<div class="left">
							<span class="printOrderSpanGray">
								<%if (!String.IsNullOrEmpty(Model.Order.CustFileName))
		  {%>
								<a target="_blank" href="<%=Url.Action("DownloadAttachFile", "Order", new { systemFileName = Model.Order.SystemFileName})%>">
									<%=Model.Order.CustFileName %></a>
								<%} %>
								<%else
		  {%>
								<span>-</span>
								<%} %>
							</span>
						</div>
						<div class="eclear">
						</div>
						<!--End AttachFile -->
					</div>
				</div>
				<!--Column-Full-->
				<div class="eclear">
				</div>
				<div class="Column-Full">
					<h4>
						<%=Html._("Order.ViewOrder.OrderActivity")%></h4>
					<div class="eclear">
					</div>
					<%Html.RenderPartial("OrderActivityPartial", Model.OrderActivity); %>
				</div>
				<div class="eclear">
				</div>
				<!--Start ยืนยันการรับสินค้า-->
				<%if (!Model.User.Company.UseGoodReceive && Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Shipped && Model.User.IsRequester)
	  {%>
				<div class="Column-Full ">
					<h4>
						ดำเนินการขั้นตอนจัดส่งสินค้า</h4>
					<div class="border2">
						<%=Html.ActionLink("ยืนยันการรับสินค้า", "CompleteOrder", "Account", new { guid = Model.Order.OrderGuid }, new { @style = "font: 13px tahoma!important;" })%>
					</div>
				</div>
				<%} %>
				<!--End ยืนยันการรับสินค้า-->
				<div class="eclear">
				</div>
				<%if (Model.User.Company.UseGoodReceive && Model.Order.Status == Eprocurement2012.Models.Order.OrderStatus.Shipped && Model.User.IsRequester)
	  {%>
				<div class="Column-Full">
					<h4>
						<%=Html._("Order.PartialReceive.ActionReceive")%></h4>
					<%if (Model.Order.GoodReceive.Status != Eprocurement2012.Models.GoodReceive.GoodReceiveStatus.Completed)
	   {%>
					<div class="border2">
						<%if (Model.Order.GoodReceive.Status == Eprocurement2012.Models.GoodReceive.GoodReceiveStatus.Waiting)
		{%>
						<%Html.BeginForm("ReceiveAll", "Order", FormMethod.Post, new { style = "width: 150px; float: left;" }); %>
						<input id="ReceivedAll" name="ReceivedAll.x" type="submit" value="Received All" class="Approvebutton"
							style="margin: 0 10px;" />
						<input type="hidden" name="orderGuid" value="<%=Model.Order.OrderGuid %>" />
						<a class="help1">
							<img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
						<div id="All" style="padding: 10px; width: 200px; height: 30px; background-color: #CCCCCC;">
							<p>
								<%=Html._("Order.ViewOrder.HelpReceivedAll")%></p>
						</div>
						<%Html.EndForm(); %>
						<%} %>
						<%Html.BeginForm("PartialReceive", "Order", FormMethod.Post, new { style = "width: 150px; float: left;" }); %>
						<input class="PartialReceive Approvebutton" name="PartialReceive.x" type="submit"
							value="Partial Receive" style="margin: 0 10px;" />
						<input type="hidden" name="orderGuid" value="<%=Model.Order.OrderGuid %>" />
						<a class="help2">
							<img alt="Learn More" src="/images/theme/LearnMore.jpg" style="cursor: pointer;" /></a>
						<div class="Partial" style="padding: 10px; width: 200px; height: 30px; background-color: #CCCCCC;">
							<p>
								<%=Html._("Order.ViewOrder.HelpPartialReceive")%></p>
						</div>
						<%Html.EndForm(); %>
						<span style="margin-left: 800px; margin-top: -20px;">
							<%=string.Format(Html._("Order.ViewOrder.NumOfAction"),Model.Order.GoodReceive.NumOfAction)%>
						</span>
					</div>
					<%} %>
					<%else
	   {%>
					<div class="border2">
						<h4 style="color: Red">
							<%=Html._("Order.ViewOrder.CompleteAllReceive")%></h4>
					</div>
					<%}%>
				</div>
				<%}%>
			</div>
			<div class="eclear">
			</div>
		</div>
		<!--end GroupData-->
	</div>
	<div class="eclear">
	</div>
</asp:Content>
<asp:Content ID="Content1" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.box
		{
			width: 200px;
			padding: 10px;
			margin: 10px;
			height: 250px;
		}
		.btnPrint
		{
			width: 120px;
			border: 1px solid #E5E5E5;
			border-radius: 5px 5px 5px 5px;
			cursor: pointer;
			-moz-border-radius: 5px 5px 5px 5px;
			padding: 5px;
			margin-top: 5px;
		}
	</style>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {
			$("#jMenu").load('<%=Url.Action("PatialMenu","Home", new { c="20110606" }) %>', function () {
				$("#menuAnchor").show();
				$(".flyoutAnchor").hover(function () { incCounter(); $("#jMenu").show(); }, decCounter);
				$(".navShopAll").hover(incCounter, decCounter);
				$("#jMenu").hover(incCounter, decCounter);
			});

			$(".box").hide();
			$("#print").click(function () {
				$(".box").slideToggle("slow");
			});

			$("#All").hide();
			$(".Partial").hide();
			$(".help1").click(function () {
				$("#All").slideToggle("slow");
			});
			$(".help2").click(function () {
				$(".Partial").slideToggle("slow");
			});

		});
	</script>
</asp:Content>
