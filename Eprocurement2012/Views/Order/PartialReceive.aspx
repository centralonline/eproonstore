﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Shared/Master.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.GoodReceive>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="centerwrap">
		<div class="GroupData" style="text-align:center; width: 200px;">
			<h3><%=Html.ActionLink(Html._("Report.Menu.GoodReceive"), "PurchaseGoodReceiveOrder", "Report")%></h3>
		</div>
		<div class="GroupData">
			<h3>
				<img alt="" src="/images/theme/h3-list-icon.png" />
				<%=Html._("Order.PartialReceive.DataGoodReceive")%></h3>
		</div>
		<div id="content-shopping-cart-detail">
			<label>
				<%=Html._("Order.PartialReceive.OrderId")%></label>
			<%=Model.OrderId %><br />
			<label>
				<%=Html._("Order.PartialReceive.GoodReceiveStatus")%></label>
			<%=Html._("Eprocurement2012.Models.GoodReceive+GoodReceiveStatus." + Model.Status.ToString())%>
		</div>
		<div class="eclear">
		</div>
		<div class="GroupData">
			<h3>
				<img alt="" src="/images/theme/h3-list-icon.png" />
				<%=Html._("Order.PartialReceive.OrderItems")%></h3>
		</div>
		<div id="content-shopping-cart-detail">
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
				border: solid 1px #c5c5c5; text-align: center;">
				<tr>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductId")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.Items")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductUnit")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductOriginal")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductAction")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductCancel")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductRemain")%>
					</td>
				</tr>
				<%foreach (var oriItem in Model.ItemReceiveDetail.Where(o => o.NumOfAction == 0))
				{%>
				<tr>
					<td>
						<%=oriItem.PId%>
					</td>
					<td style="text-align: left">
						<%=oriItem.Product.ThaiName%>
					</td>
					<td>
						<%=oriItem.Product.ThaiUnit%>
					</td>
					<td>
						<%=oriItem.OriginalQty%>
					</td>
					<td>
						<%=oriItem.ActionQty%>
					</td>
					<td>
						<%=oriItem.CancelQty%>
					</td>
					<td>
						<%=oriItem.RemainQty%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<br />
		<%foreach (var groupItem in Model.ItemReceiveDetail.Where(g => g.NumOfAction != 0).GroupBy(d => d.NumOfAction))
		{%>
		<div id="content-shopping-cart-detail">
			<label>
				<%=Html._("Order.PartialReceive.NumOfAction")%>
				<%=groupItem.First().NumOfAction %></label><br />
			<label>
				<%=Html._("Order.PartialReceive.ActionDate")%>
				<%=groupItem.First().ActionDate.ToString(new DateFormat()) %></label>
			<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
				border: solid 1px #c5c5c5; text-align: center;">
				<tr>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductId")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.Items")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductUnit")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductOriginal")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.Remain")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductAction")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductCancel")%>
					</td>
					<td class="thead" style="height: 24px">
						<%=Html._("Order.PartialReceive.ProductRemain")%>
					</td>
				</tr>
				<%foreach (var item in groupItem)
				{%>
				<tr>
					<td>
						<%=item.PId%>
					</td>
					<td style="text-align: left">
						<%=item.Product.ThaiName%>
					</td>
					<td>
						<%=item.Product.ThaiUnit%>
					</td>
					<td>
						<%=item.OrderQTY %>
					</td>
					<td>
						<%=item.OriginalQty%>
					</td>
					<td>
						<%=item.ActionQty%>
					</td>
					<td>
						<%=item.CancelQty%>
					</td>
					<td>
						<%=item.RemainQty%>
					</td>
				</tr>
				<%} %>
			</table>
		</div>
		<br />
		<%} %>
		<%if (Model.Status != Eprocurement2012.Models.GoodReceive.GoodReceiveStatus.Completed)
	{%>
		<%Html.BeginForm("SavePartialReceive", "Order", FormMethod.Get); %>
		<%=Html.Hidden("orderGuid", Model.orderGuid)%>
		<input class="graybutton" type="submit" value="<%=Html._("Order.PartialReceive.ButtonReceive") %>" />
		<%Html.EndForm(); %>
		<%} %>
		<!-- เมื่อกด "รับสินค้า" จะแสดงข้อมูลเพื่อให้กรอกตัวเลขสินค้าที่ได้รับ -->
		<div class="eclear">
		</div>
		<%if (Model.Status != Eprocurement2012.Models.GoodReceive.GoodReceiveStatus.Completed && Model.IsReceive)
	{%>
		<div id="action_receive">
			<div class="GroupData">
				<h3>
					<img alt="" src="/images/theme/h3-list-icon.png" /><%=Html._("Order.PartialReceive.ActionReceive")%></h3>
			</div>
			<div id="content-shopping-cart-detail">
				<table width="100%" border="1" cellpadding="0" cellspacing="0" style="margin: auto;
					border: solid 1px #c5c5c5; text-align: center;">
					<tr>
						<td class="thead">
							<%=Html._("Order.PartialReceive.ProductId")%>
						</td>
						<td class="thead">
							<%=Html._("Order.PartialReceive.Items")%>
						</td>
						<td class="thead">
							<%=Html._("Order.PartialReceive.ProductUnit")%>
						</td>
						<td class="thead" style="height: 24px">
							<%=Html._("Order.PartialReceive.ProductOriginal")%>
						</td>
						<td class="thead">
							<%=Html._("Order.PartialReceive.ProductBacklog")%>
						</td>
						<td class="thead">
							<%=Html._("Order.PartialReceive.ProductAction")%>
						</td>
						<td class="thead">
							<%=Html._("Order.PartialReceive.ProductCancel")%>
						</td>
					</tr>
					<%Html.BeginForm("SavePartialReceive", "Order", FormMethod.Post); %>
					<%
					foreach (var item in Model.ItemReceiveNow)
					{%>
					<tr>
						<td>
							<%=item.PId %><input type="hidden" name="pID" value="<%=item.PId%>" />
						</td>
						<td style="text-align: left">
							<%=item.Product.Name %><br />
							<span style="color: Red">
								<%=TempData["Error"+ item.PId]%></span>
						</td>
						<td>
							<%=item.Product.Unit%>
						</td>
						<td>
							<%=item.OrderQTY %>
						</td>
						<td>
							<span class="Remain">
								<%=item.RemainQty%></span>
						</td>
						<td>
							<input type="text" name="actionQty" class="Action number" value="<%=item.ActionQty_New%>" />
						</td>
						<td>
							<input type="text" name="cancelQty" class="number" value="<%=item.CancelQty_New%>" />
						</td>
					</tr>
					<%} %>
				</table>
			</div>
			<div>
				<label>
					<%=Html._("Order.PartialReceive.ActionDate")%></label>
				<input id="actionDate" type="text" name="actionDate" value="<%=Model.ActionDate%>"
					onkeydown="return false;" />
				<span style="color: Red">
					<%=TempData["Error"]%></span>
				<input id="All" type="checkbox" name="receiveAll" value="" />
				<label>
					<%=Html._("Order.PartialReceive.AllReceive")%></label>
				<input type="hidden" name="orderGuid" value="<%=Model.orderGuid %>" />
				<input class="graybutton save" type="submit" name="saveReceive.x" value="<%=Html._("Order.PartialReceive.ButtonSave") %>" />
				<input id="cancel" class="graybutton" type="submit" name="cancelReceive.x" value="<%=Html._("Order.PartialReceive.ButtonCancel") %>" />
				<%Html.EndForm(); %>
			</div>
		</div>
		<%} %>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CssContent" runat="server">
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(document).ready(function () {

			$(".number").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
						event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
						(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});

			$("#All").click(function () {
				var result = $('#All').attr('checked');
				if (result == "checked") {
					$(".Remain").each(function (index) {
						var value_text = $(this).text();
						$(".Action:eq(" + index + ")").attr("value", $.trim(value_text));
					});
				}
				else {
					$(".Remain").each(function (index) {
						var value_text = $(this).text();
						$(".Action:eq(" + index + ")").attr("value", "0");
					});
				}
			});

			$("#actionDate").datepicker({
				changeMonth: true,
				dateFormat: 'dd/mm/yy',
				maxDate: "+0M +0D",
				minDate: -60
			});

		})
	</script>
</asp:Content>
