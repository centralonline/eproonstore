﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/Department/Department.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.RePurchase>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div class="list-box" style="margin-left: 6px; float: left">
		<h3>
			<%=Html._("Repurchase.MyOrder")%></h3>
		<%foreach (var item in Model.Orders)
	{%>
		<div id="<%=item.OrderGuid %>" style="padding-left: 10px;">
			<%=Html.ActionLink(item.OrderID, "ViewProductInOrder", "RePurchase", new { orderGuid = item.OrderGuid }, null)%>
		</div>
		<%} %>
	</div>
	<div id="contentCol" style="padding-left: 10px;">
		<div class="orderByCode">
			<img src="../../images/theme/man.png" class="man" alt="man" />
			<div class="productCode GroupData">
				<h5>
					<%=Html._("CartDetailPartial.AddCartByProductId")%></h5>
				<div class="field">
					<%Html.BeginForm("AddToCartByProductId", "Cart", FormMethod.Post, new { id = "formAddToCartByProductId" }); %>
					<div class="by-ProductID">
						<label for="pID">
							<%=Html._("CartDetailPartial.ProductId")%>
							:</label>
						<input id="productId" name="productId" maxlength="7" type="text" /><span id="showname"></span>
					</div>
					<br clear="all" />
					<div class="by-ProductID">
						<label for="pQty">
							<%=Html._("CartDetailPartial.Quantity")%>
							:</label>
						<input id="quantity" name="quantity" class="quantity" maxlength="4" type="text" />
					</div>
					<br />
					<span style="margin: 10px 0px 0px 125px;">
						<input type="image" name="AddToCartByPId" value="AddToCartByPId" class="default"
							src="/images/theme/<%=Html._("CartDetailPartial.ButtonAddcart")%> " />
					</span><span style="color: Blue">
						<%=TempData["ErrorAddProductByPid"]%></span>
					<%Html.EndForm();%>
				</div>
			</div>
		</div>
		<div id="content" class="content">
			<%
				if (Model.Orders != null && Model.Orders.Any())
				{
			%>
			<div id="right">
				<h1>
					<%=Html._("Repurchase.MyOrderId")%>:<%=Model.OrderId%></h1>
				<div class="eclear">
				</div>
				<%
					if (Model.Products != null)
					{
				%>
				<%Html.BeginForm("HandleBuyOfficeSupply", "Product", FormMethod.Post, new { @class = "handleBuy-OfficeSupply" }); %>
				<table class="office-list-product" cellpadding="0" cellspacing="0">
					<thead>
						<tr>
							<td class="text-center fix50">
								<%=Html._("Repurchase.MyProductId")%>
							</td>
							<td class="text-center" style="width: 80%">
								<%=Html._("Repurchase.MyProductName")%>
							</td>
							<td class="text-center bgcolor-hilight">
								<%=Html._("Repurchase.MyProductPrice")%>
							</td>
							<td class="text-center bgcolor-hilight2 fix50">
								<%=Html._("Repurchase.MyProductQuantity")%>
							</td>
							<td class="text-center fix50">
								<%=Html._("Repurchase.MyProductUnit")%>
							</td>
						</tr>
					</thead>
					<tbody>
						<%foreach (var item in Model.Products)
		{%>
						<tr>
							<td style="text-align: center">
								<span class="gray">
									<%=item.Id%></span>
							</td>
							<td>
								<div id="product-list" class="office-list-product-infomation-name">
									<div class="quickinfobt" style="width: 100%;">
										<%=Html.Hidden("pid", item.Id, new { @class = "productId" })%>
										<span style="margin-left: 100px; font: normal 13px tahoma;">
											<%=Html.Encode(item.Name)%></span>
										<%if (!Model.User.Company.UseOfmCatalog && item.IsOfmCatalog)
			{%>
										<br />
										<p style="margin-left: 100px;">
											<%=Html._("Order.ConfirmOrder.NotProductCatalog")%></p>
										<%} %>
									</div>
								</div>
							</td>
							<td class="text-right">
								<%=item.DisplayPrice%>
							</td>
							<td class="text-center">
								<%=Html.Hidden("productId", item.Id)%>
								<%if (!Model.User.Company.UseOfmCatalog && !Model.User.Company.ShowOFMCat && item.IsOfmCatalog)
		  {%>
								<input type='hidden' name='quantity' value='' />
								<%}
		  else
		  {%>
								<%=item.IsDisplayBuyButton ? "<input type='text' style='width:30px;' maxlength='4' name='quantity' class='quantity' />" : item.IsContactForDelivery ? "<span style='color:Red;'>สินค้ามีค่าจัดส่ง</span>" : "<span style='color:Red;'>สินค้าขาด</span><input type='hidden' name='quantity' value='' />"%>
								<%} %>
							</td>
							<td class="text-center">
								<%=item.Unit%>
							</td>
						</tr>
						<%} %>
					</tbody>
				</table>
				<table class="office-list-product" cellpadding="0" cellspacing="0">
					<tr>
						<td style="background-color: #d1d1d1;">
							<div style="float: right; background-color: #d1d1d1; width: auto">
								<input type="image" name="AddToCartOfficeSupply" value="AddToCartOfficeSupply" class="AddToCartOfficeSupply"
									id="AddToCartOfficeSupply" src="/images/theme/button-addTocart.png" />
							</div>
						</td>
					</tr>
				</table>
				<%Html.EndForm(); %>
				<%} %>
			</div>
		</div>
	</div>
	<!--end contentCol-->
	<div id="message-Popup" style="display: none; text-align: center;">
	</div>
	<div id="BackToShopOrGotoCheckOut" style="display: none;">
		<br />
		<br />
		<br />
		<input type="image" src="/images/btn/btn_shop_continue.gif" value="กลับไป shop" id="backtoshop"
			onclick="closeMessagePopup();" />
		หรือ <a href="<%=Url.Action("CartDetail", "Cart") %>">
			<img src="/images/btn/btn_go2_checkout.gif" alt="ไปหน้า Check Out" id="gotocheckout" />
		</a>
	</div>
	<%} %>
	<%else
				{ %>
	<h3>
		<%=Html._("Repurchase.ErrorMyOrderEmptry")%></h3>
	<%} %>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="CssContent" runat="server">
	<style type="text/css">
		.content
		{
			overflow: auto;
			width: 100%;
		}
		.left
		{
			float: left;
			width: 20%;
		}
		.leftDialog
		{
			float: left;
			width: 50%;
		}
		.right
		{
			float: right; /*width: 80%;เปิดแล้วตำแหน่ง profileพัง เพราะมี column rightอยู่แล้ว Linda*/
		}
		.rightDialog
		{
			float: right;
			width: 50%;
		}
	</style>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script language="javascript" type="text/javascript">
		var popupTimer;
		$(function () {

			$(".quantity").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
							 event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
							(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});

			$('.AddToCartOfficeSupply').click(addToCartOfficeSupply);
			$("#message-Popup").bind("dialogclose", function (event, ui) {
				clearTimeout(popupTimer);
			});

			$("#showname").html("");
			$('#productId').keyup(function (e) {
				if ($(this).val().length == 7) {
					$.getJSON('<%=Url.FullUrlAction("SearchProductSKU","Product")%>', { pId: $(this).val() }, function (json) {
						$("#showname").html(json.ProductName);
					});
				}
			});

		});

		function addToCartOfficeSupply(event) {
			event.preventDefault();
			if ($('input:text[name="quantity"][value!=""]').length > 0) {
				var thisForm = $(this).closest('form');
				var sendData = thisForm.serialize() + "&AddToCartOfficeSupply.AddToCartOfficeSupply.x=";
				formProcess(sendData);
			}
		}

		function formProcess(formData) {
			$("#message-Popup").dialog({
				autoOpen: false,
				modal: true,
				width: 420,
				height: 200
			});
			$.ajax({
				url: '<%=Url.Action("HandleBuyOfficeSupply", "Product") %>',
				type: "POST",
				data: formData,
				success: function (data) {
					if (data.Result) {
						$("#header-shoppingcart-countproduct").text(data.ItemCount);
						$("[name='quantity']").val('');
						$("#message-Popup").html("<h3>" + data.Message + "</h3>");
						$("#message-Popup").append($("#BackToShopOrGotoCheckOut").html());
						$("#message-Popup").dialog('open');
						popupTimer = setTimeout(closeMessagePopup, 30000);

					} else {
						$("#message-Popup").html("<h3>" + data.Message + "</h3>");
						$("#message-Popup").append($("#BackToShopOrGotoCheckOut").html());
						$("#message-Popup").dialog('open');
					}
				}
			});
		}

		function closeMessagePopup() {
			clearTimeout(popupTimer);
			$("#message-Popup").dialog('close');
		}

	</script>
</asp:Content>
