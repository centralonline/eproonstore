﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/ApproveOrder/Order.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		body
		{
			font: normal 11px/14px Tahoma;
			color: #7C7C7C;
			line-height: 25px;
		}
		p
		{
			color: #7C7C7C;
		}
		.Loginlabel
		{
			color: #000000;
			display: block;
			float: left;
			font-weight: normal;
			position: relative;
			text-align: left;
			width: 80px;
			font-size: 13px;
		}
		.LoginSpanGray
		{
			font-size: 13px;
			margin-left: 0px;
			padding-top: 0px;
			color: #666666;
			margin-top: -5px;
		}
		.LoginAlert
		{
			font-size: 10px;
			margin-left: 0px;
			padding-top: 0px;
			color: Red;
			margin-top: -5px;
		}
		.forget a:link, .forget a:visited
		{
			color: #38789B;
			font-size: 13px;
		}
		.forget a:hover
		{
			color: #000000;
		}
		.bg-login
		{
			background-image: url(/images/index/bg-login.jpg);
			background-position: top;
			width: 299px;
			height: 205px;
		}
		.centerwrap
		{
			width: 950px;
			margin: 0 auto;
			margin-top: 30px;
		}
		.box
		{
			width: 200px;
			padding: 10px;
			margin: 10px;
			height: 250px;
		}
		
		.btnPrint
		{
			width: 120px;
			border: 1px solid #E5E5E5;
			border-radius: 5px 5px 5px 5px;
			cursor: pointer;
			-moz-border-radius: 5px 5px 5px 5px;
			padding: 5px;
			margin-top: 5px;
		}
	</style>
	<div class="centerwrap">
		<!--Print-->
		<div class="eclear">
		</div>
		<div class="box" style="position: absolute; margin-left: 740px; margin-top: 50px;">
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "Account", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintTHPO"})%>">
					Print PO (TH)</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "Account", new {  orderGuid = Model.Order.OrderGuid, ActionName = "PrintEnPO"})%>">
					Print PO (EN)</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "Account", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintThQU"})%>">
					Print QU (TH)</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_pdf.png" /><a target="_blank" href="<%=Url.Action("PrintPO", "Account", new {  orderGuid = Model.Order.OrderGuid , ActionName = "PrintEnQU"})%>">
					Print QU (EN)</a>
			</div>
			<div class="right noPrint btnPrint">
				<img src="/images/theme/icon_printOrder.jpg" />
				<a href="#" onclick="window.print(); return false">Print web form</a>
			</div>
		</div>
		<div id="print" class="right noPrint btnPrint">
			<img src="/images/theme/icon_printOrder.jpg" />
			<span>Print Document</span>
		</div>
		<%--<% Html.BeginForm("PrintPo", "Account", new { OrderGuid = Model.Order.OrderGuid }, FormMethod.Post, new { target = "_blank" });%>
				<input name="PrintThPO.x" id="PrintThPO" type="image" src="/images/btn_printPOTh.GIF"
					alt="PrintThQU" title="คลิกเพื่อ print PO ภาษาไทยค่ะ" class="right" />
				<input name="PrintEnPO.x" id="PrintEnPO" type="image" src="/images/btn_printPOEn.gif"
					alt="PrintEnPO" title="คลิกเพื่อ print PO ภาษาอังกฤษค่ะ" class="right" />
				<% Html.EndForm();%>--%>
		<div style="margin: 30px 0;">
			<img alt="logo" src="/images/theme/logo-ePro.jpg" class="left" />
			<div class="eclear">
			</div>
			<!--Start printOrder detail-->
			<div style="width: 100%; padding: 20px 8px 8px 0px; margin-right: 20px; margin-top: 35px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderID %></p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="left" style="width: 450px; min-height: 250px; padding: 5px; margin-right: 9px;
				margin-top: 0px; border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CustID")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.CostCenter.CostCenterCustID %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("CartDetail.ShipContactor")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.UserId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Requester.UserId%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Phone")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorPhone %>
						<%=string.IsNullOrEmpty(Model.Order.Contact.ContactorExtension) ? "" : " #" + Model.Order.Contact.ContactorExtension%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Fax")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorFax %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.InvoiceAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Invoice.Address1 %>
						<%=Model.Order.Invoice.Address2 %>
						<%=Model.Order.Invoice.Address3 %>
						<%=Model.Order.Invoice.Address4 %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="right" style="width: 450px; min-height: 250px; margin-right: 9px; margin-top: 0px;
				border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
				padding: 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.CompanyId")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyId %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CompanyName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.DepartmentName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=string.IsNullOrEmpty(Model.Order.Department.DepartmentID)? "-": "[" + Model.Order.Department.DepartmentID + "] " + Model.Order.Department.DepartmentName %>
					</p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CostCenterName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderDate")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.ShippingAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Shipping.Address1 %>
						<%=Model.Order.Shipping.Address2 %>
						<%=Model.Order.Shipping.Address3 %>
						<%=Model.Order.Shipping.Address4 %>
						<%=Model.Order.Shipping.Province %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<!--end printOrder detail-->
			<div class="eclear">
			</div>
			<div class="printOrderlabel" style="padding-top: 20px; padding-bottom: 10px;">
				<label>
					<%=Html._("Order.ViewOrder.OrderItems")%>
				</label>
			</div>
			<%Html.RenderPartial("OrderDetailPartial", Model.Order); %>
			<div class="eclear">
			</div>
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ViewOrder.Remark")%></h4>
				<div class="border2">
					<div class="Remarklabel">
						<label>
							<%=Html._("Order.ViewOrder.ApproverRemark")%>
							:</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.ApproverRemark %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="Remarklabel">
						<label>
							<%=Html._("Order.ViewOrder.OFMRemark")%>
							:</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.OFMRemark %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="Remarklabel">
						<label>
							<%=Html._("Order.ViewOrder.ReferenceRemark")%>
							:</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.ReferenceRemark %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="Remarklabel">
						<label>
							<%=Html._("Order.AttachFile")%>
							:</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%if (!String.IsNullOrEmpty(Model.Order.CustFileName))
							{%>
							<a target="_blank" href="<%=Url.Action("DownloadAttachFile", "Account", new { systemFileName = Model.Order.SystemFileName})%>">
								<%=Model.Order.CustFileName %></a>
							<%} %>
						</p>
					</div>
					<div class="eclear">
					</div>
				</div>
			</div>
			<!--Column-Full-->
			<div class="eclear">
			</div>
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ViewOrder.OrderActivity")%></h4>
				<div class="eclear">
				</div>
				<%Html.RenderPartial("OrderActivityPartial", Model.OrderActivity); %>
			</div>
			<div class="eclear">
			</div>
			<div class="spaceTop">
			</div>
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<script type="text/javascript">
		$(function () {

			$(".box").hide();
			$("#print").click(function () {
				$(".box").slideToggle("slow");
			});
		});
	</script>
</asp:Content>
