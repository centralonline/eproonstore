﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/SelectSite.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.Site>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div>
		<link href="/css/SelectCompany/selectCompany.css" rel="stylesheet" type="text/css" />
	</div>
	<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left: 10px;">
			<%=Html._("Account.SiteUnAvailable.UpdateCloseSite")%></p>
		<p class="epro-p" style="padding-left: 10px;">
			<%=Html._("Account.SiteUnAvailable.UpdateSystemNow")%></p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<!--New Design Feb 2013-->
		<div style="padding-left: 10px;">
			<div class="positionApplylabel">
				<div class="Sitelabel2">
					<label>
						<%=Html._("Account.SiteUnAvailable.CloseAdminSiteName")%></label>
				</div>
				<div class="eclear">
				</div>
				<div class="leftFloat">
				<label><%= !string.IsNullOrEmpty(Model.AdminSiteUserId) ? Model.AdminSiteName : "-"%></label>
				</div>
				<div class="eclear">
				</div>

				<div class="Sitelabel2">
					<label>
						<%=Html._("Account.SiteUnAvailable.AdminRemark")%></label>
				</div>
				<div class="eclear">
				</div>
				<div class="leftFloat">
				<label><%= !string.IsNullOrEmpty(Model.AdminRemark) ? Model.AdminRemark : "-"%></label>
				</div>
				<div class="eclear">
				</div>
				<div class="Sitelabel2">
					<label>
						<%=Html._("Account.SiteUnAvailable.AdminQuery")%></label>
				</div>
				<div class="eclear">
				</div>
				<div class="box_siteUn">
					<%if (Model.ListUserAdmins != null)
					{ %>
					<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
						width: 100%; height: 19px;">
					</div>
					<p class="pTitle" style="padding-left: 0px;">
						<%=Html._("Account.AdminUser.RootAdmin")%></p>
					<%foreach (var itemUser in Model.ListUserAdmins.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.RootAdmin))
					{ %>
					<div class="adminbox2">
						<img src="<%=Url.Action("ProfileImage","UserProfile", new {itemUser.UserGuid }) %>"
							alt="<%=itemUser.DisplayName%>" width="100" height="100" class="imgepro-left frameImages" />
						<p class="epro-p">
							<%=itemUser.DisplayName%><br />
							<span style="color: #999;">
								<%=Html._("Shared.MyProfile.PhoneNumber")%>
								:<%=itemUser.UserContact.Phone != null ? itemUser.UserContact.Phone.PhoneNo : "-"%>
								<%=itemUser.UserContact.Phone != null && !string.IsNullOrEmpty(itemUser.UserContact.Phone.Extension) ? "#" + itemUser.UserContact.Phone.Extension : ""%>
							</span>
							<br />
							<span style="color: #999;">
								<%=Html._("Shared.MyProfile.MobileNumber")%>
								:<%=itemUser.UserContact.Mobile != null ? itemUser.UserContact.Mobile.PhoneNo : "-"%>
							</span>
							<br />
							<span style="color: #999;">
								<%=Html._("Shared.MyProfile.Email")%>
								:
								<%=itemUser.UserContact.Email%></span><br />
							<br />
							<%if (itemUser.UserId == Model.User.UserId)
							{ %>
							<%Html.BeginForm("GoToOpenSitePage", "Account");%>
							<input id="Submit3" class="button-e" type="submit" value="<%=Html._("Button.OpenSitePage")%>"
								style="width: 100px; margin: 0 auto; position: absolute; float: left; border: none;
								text-decoration: none; padding-top: 0;" />
							<%Html.EndForm(); %>
							<%} %>
						</p>
						<div class="eclear">
						</div>
					</div>
					<% }%>
					<%if (Model.ListUserAdmins.Any(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
					{ %>
					<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
						width: 100%; height: 19px;">
					</div>
					<p class="pTitle" style="padding-left: 10px;">
						<%=Html._("Account.AdminUser.AssistantAdmin")%></p>
					<%foreach (var itemUser in Model.ListUserAdmins.Where(o => o.UserRoleName == Eprocurement2012.Models.User.UserRole.AssistantAdmin))
					{ %>
					<div class="adminbox2">
						<img src="<%=Url.Action("ProfileImage","UserProfile", new {itemUser.UserGuid }) %>"
							alt="<%=itemUser.DisplayName%>" width="100" height="100" class="imgepro-left frameImages" />
						<div class="right" style="width: 450px; float: left;">
							<p class="epro-p">
								<%=itemUser.DisplayName%><br />
								<span style="color: #999;">
									<%=Html._("Shared.MyProfile.PhoneNumber")%>
									:<%=itemUser.UserContact.Phone != null ? itemUser.UserContact.Phone.PhoneNo : "-"%>
								<%=itemUser.UserContact.Phone != null && !string.IsNullOrEmpty(itemUser.UserContact.Phone.Extension) ? "#" + itemUser.UserContact.Phone.Extension : ""%>
								</span>
								<br />
								<span style="color: #999;">
									<%=Html._("Shared.MyProfile.MobileNumber")%>
									:<%=itemUser.UserContact.Mobile != null ? itemUser.UserContact.Mobile.PhoneNo : "-"%>
								</span>
								<br />
								<span style="color: #999;">
									<%=Html._("Shared.MyProfile.Email")%>
									:<%=itemUser.UserContact.Email%>
								</span>
								<br />
								<br />
								<%if (itemUser.UserId == Model.User.UserId)
								{ %>
								<%Html.BeginForm("GoToOpenSitePage", "Account");%>
								<input id="Submit4" class="button-e" type="submit" value="<%=Html._("Button.OpenSitePage")%>"
									style="width: 100px; margin: 0 auto; position: absolute; float: left; border: none;
									text-decoration: none; padding-top: 0;" />
								<%Html.EndForm(); %>
								<%} %>
							</p>
						</div>
						<div class="eclear">
						</div>
					</div>
					<% } %>
					<% } %>
					<% } %>
					<%Html.BeginForm("LogOut", "Account");%>
					<input class="button-e" style="margin-top: 15px; margin-left: 0px; cursor: pointer;
						border: none;" type="submit" value="<%=Html._("Button.Home")%>" />
					<%Html.EndForm(); %>
				</div>
				<div class="eclear">
				</div>
			</div>
			<!--end positionApplylabel-->
		</div>
	<!--New Design Feb 2013-->
	</div>
	<!--end epro-content-->
</asp:Content>
