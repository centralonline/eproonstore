﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master"
	Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<script type="text/javascript" src="/js/AC_RunActiveContent.js"></script>
	<style type="text/css">
		.cbox2
		{
			width: 500px;
			height: 370px;
			font-family: Tahoma, verdana;
			font-size: 13px;
			color: #666666;
			margin: 15px 0px;
			border-bottom: solid 1px #c1c1c1;
			margin-left: 80px;
		}
		img.m10
		{
			margin-right: 10px;
			float: left;
			border: solid 2px orange;
		}
	</style>
	<div>
		<div class="boxcontent">

        <!--Video 4-->
			<div class="cbox2">
				<div style="height:80px; float: left; width: 496px">
					<span style="font: normal 1.5em/24px tahoma;">งานสัมมนา Officemate e-Procurement<br /> ทันกระแสดิจิตอล จัดซื้ออนไลน์&nbsp;</span><br />
					
					
				</div>
				<div style="text-align: center; padding-top: 5px; margin: 0; float: left; width: 496px">
					<div style="text-align: center; padding-top:5px; margin: 0; float: left; width: 496px">
						<div id="Div1">
							<iframe width="300" height="200" src="https://www.youtube.com/embed/AAyKa_2fo8A" frameborder="0" allowfullscreen></iframe>
						</div>
					</div>
				</div>
			</div>

			<!-- End Video 4-->




			<!--Video 1-->
			<div class="cbox2">
				<div style="height: 120px; float: left; width: 496px">
					<span style="font: normal 1.5em/24px tahoma;">เสียงสัมภาษณ์จากผู้ใช้งานจริงบนระบบ e-Procurement
						ในงานสัมมนา “ซื้ออย่างไร ให้ทันยุคดิจิตอล”&nbsp;</span><br />
					<br />
					ท่านที่ 1 คุณ สถิตย์ เจ้าหน้าที่จัดซื้อ บริษัทวุฒิศักดิ์ คลินิก อินเตอร์ กรุ๊ป จำกัด<br />
					ท่านที่ 2 คุณรัตติยา เป็งปัน เจ้าหน้าที่จัดซื้อ บริษัท ฮุนได มอเตอร์ (ไทยแลนด์)
					จำกัด<br />
					ท่านที่ 3 คุณรมิดา ฐานุธนาคุณ Sales Planner & Purchasing Supervisor บจก. อีสท์ เวสท์
					ซีด อินเตอร์เนชั่นแนล<br />
				</div>
				<div style="text-align: center; padding-top: 10px; margin: 0; float: left; width: 496px">
					<div style="text-align: center; padding-top: 10px; margin: 0; float: left; width: 496px">
						<div id="videoPlayer1">
							<script type="text/javascript" src="/swfobject/swfobject.js"></script>
							<script type="text/javascript">
								var flashvars = {
									htmlPage: document.location,
									settingsFile: "seminar.xml"
								};
								var params = {
									allowFullScreen: "true"
								};

								swfobject.embedSWF("/videoPlayer.swf", "videoPlayer1", "300", "200", "9.0.115", "/swfobject/expressInstall.swf", flashvars, params);
							</script>
							<p>
								<a href="http://www.adobe.com/go/getflashplayer">
									<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
										alt="Get Adobe Flash player" /></a></p>
						</div>
					</div>
				</div>
			</div>

			<!-- End Video 1-->

            <!--Video 2-->
            <div class="cbox2">
				<div style="height: 80px; float: left; width: 496px">
					<img src="/images/logo/silkspan.gif" alt="" class="m10" />
					<span style="font: normal 1.5em/24px tahoma;">บริษัท ซิลด์สแปน จำกัด&nbsp;</span><br />
					คุณบุตรรัตน์ จรูญสมิทธิ์<br />
					กรรมการผู้จัดการ บริษัท ซิลด์สแปน จำกัด<br />
				</div>
				<div style="text-align: center; padding-top: 10px; margin: 0; float: left; width: 496px">
					<div style="text-align: center; padding-top: 10px; margin: 0; float: left; width: 496px">
						<div id="videoPlayer2">
							<script type="text/javascript" src="/swfobject/swfobject.js"></script>
							<script type="text/javascript">
							    var flashvars = {
							        htmlPage: document.location,
							        settingsFile: "silkspan.xml"
							    };
							    var params = {
							        allowFullScreen: "true"
							    };

							    swfobject.embedSWF("/videoPlayer.swf", "videoPlayer2", "300", "200", "9.0.115", "/swfobject/expressInstall.swf", flashvars, params);
							</script>
							<p>
								<a href="http://www.adobe.com/go/getflashplayer">
									<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
										alt="Get Adobe Flash player" /></a></p>
						</div>
					</div>
				</div>
			</div>
            <!-- End Video 2-->

              <!--Video 3-->
              <div class="cbox2">
				<div style="height: 100px; float: left; width: 496px">
					<img src="/images/logo/tata.gif" alt="" class="m10" />
					<span style="font: normal 1.5em/24px tahoma;">บริษัท ทาทา มอเตอร์ส (ประเทศไทย) จำกัด&nbsp;</span><br />
					คุณเยาวลักษณ์ กิจกังวล<br />
					Executive Secretary<br />
				</div>
				<div style="text-align: center; padding-top: 10px; margin: 0; float: left; width: 496px">
					<div id="videoPlayer3">
						<script type="text/javascript" src="/swfobject/swfobject.js"></script>
						<script type="text/javascript">
						    var flashvars = {
						        htmlPage: document.location,
						        settingsFile: "tata.xml"
						    };
						    var params = {
						        allowFullScreen: "true"
						    };

						    swfobject.embedSWF("/videoPlayer.swf", "videoPlayer3", "300", "200", "9.0.115", "/swfobject/expressInstall.swf", flashvars, params);
						</script>
						<p>
							<a href="http://www.adobe.com/go/getflashplayer">
								<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
									alt="Get Adobe Flash player" /></a></p>
					</div>
				</div>
			</div>
              <!-- End Video 3-->
		</div>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
