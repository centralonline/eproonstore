﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left: 10px;">
			<%=Html._("Account.SelectSiteAdmin")%></p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<div style="padding-left: 10px;">
			<div>
				<table>
					<tr>
						<td style="width: 50%;">
							<p class="pTitle" style="padding-left: 10px; padding-top: 0;">
								<%=Html._("Account.SelectSiteAdmin.Admin")%></p>
							<p class="epro-p" style="padding-left: 10px;">
								<%=Html._("Account.SelectSiteAdmin.AdminContent")%>
							</p>
							<div style="padding-left: 10px;">
								<a class="button-e2" href="<%= Url.Action("GoToCloseSitePage", "Account")%>">
									<%=Html._("Menu.Home.MaintainSystem")%></a>
							</div>
							<div class="eclear">
							</div>
							<div style="padding-left: 10px;">
								<img src="/images/theme/icon_admin_setting.jpg" alt="Alternate Text" width="100"
									height="100" />
							</div>
						</td>
						<td style="width: 50%;">
							<p class="pTitle" style="padding-left: 10px; padding-top: 0;">
								<%=Html._("Account.SelectSiteAdmin.ReportOrder")%></p>
							<p class="epro-p" style="padding-left: 10px;padding-bottom:20px;">
								<%=Html._("Account.SelectSiteAdmin.ReportOrdeContent")%></p>
							<div style="padding-left: 10px;">
								<a class="button-e2" href="<%= Url.Action("Index", "Report")%>">
									<%=Html._("Menu.Home.ReportAdmin")%></a>
							</div>
							<div class="eclear">
							</div>
							<div style="padding-left: 10px;">
								<img src="/images/theme/icon_admin_Report.jpg" alt="Alternate Text" width="100" height="100" />
							</div>
						</td>
					</tr>
				</table>
			</div>
		</div>
	</div>
</asp:Content>
