﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master" Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.SetPassword>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div style="width: 100%">
		<img alt="" width="950" height="425" src="/images/theme/Slide-Password.jpg" class=" wp-post-image" />
	</div>
	<img alt="" src="/images/index/slideShadow.jpg" />
	<div id="epro-content" class="100percent" style="min-height: 400px;">
		<div id="applyform" class="100percent">
			<img alt="" src="/images/theme/title-newPassword.jpg" />
			<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
				width: 100%; height: 19px;">
			</div>
			<p class="positionApplylabel">
				<%=Html._("Account.SetPassword.RulesChangePassword")%></p>
			<div class="positionApplylabel">
				<% Html.BeginForm("SetPassword", "Account", FormMethod.Post);%>
				<div class="applylabel">
					<label for="NewPassword">
						<%=Html._("Account.SetPassword.NewPassword")%><span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray">
						<%=Html.PasswordFor(m => m.NewPassword)%></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(m => m.NewPassword)%><%=Html.LocalizedValidationMessageFor(m => m)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label for="ConfirmNewPassword">
						<%=Html._("Account.SetPassword.ConfirmNewPassword")%><span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray">
						<%=Html.PasswordFor(m => m.ConfirmNewPassword, new { size = "30" })%></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(m => m.ConfirmNewPassword)%></p>
				</div>
				<div class="eclear">
				</div>
				<%=Html.HiddenFor(m=>m.UserGuid)%>
				<%=Html.HiddenFor(m=>m.VerifyKey)%>
				<%=Html.HiddenFor(m=>m.IsFirstPassword)%>
				<div class="setPassPosition">
					<input class="graybutton" type="submit" id="btnSetPassword" name="btnSetPassword" value="<%=Html._("Account.SetPassword.btnSetPassword")%>" />
				</div>
				<% Html.EndForm();%>
			</div>
		</div>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px; margin-top: 60px;">
		</div>
	</div>
</asp:Content>
