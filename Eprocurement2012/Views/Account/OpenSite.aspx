﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/SelectSite.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<%Html.BeginForm("OpenSite", "Site", FormMethod.Post); %>
	<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left: 10px;">
			<%=Html._("Account.OpenSite.SelectSite")%></p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<table>
			<tr>
				<td>
					<p class="pTitle" style="padding-left: 10px;">
						<%=Html._("Account.OpenSite.NoUserNotify")%></p>
					<p class="epro-p" style="padding-left: 10px;">
						<%=Html._("Account.OpenSite.NoUserNotifyEmail")%></p>
					<input class="button-e2" id="opensite" name="openSite.x" type="submit" value="<%=Html._("Button.OpenSite")%>" />
					<div class="eclear">
					</div>
					<div style="padding-left: 10px;">
						<img src="/images/theme/icon_admin_setting.jpg" alt="Alternate Text" width="100"
							height="100" />
					</div>
				</td>
				<td>
					<p class="pTitle" style="padding-left: 10px;">
						<%=Html._("Account.OpenSite.Email")%></p>
					<p class="epro-p" style="padding-left: 10px;">
						<%=Html._("Account.OpenSite.UserNotifyEmail")%></p>
					<input class="button-e2" id="Submit1" name="openSiteMail.x" type="submit" value="<%=Html._("Button.OpenSite.Email")%>" />
					<div class="eclear">
					</div>
					<div style="padding-left: 10px;">
						<img src="/images/theme/icon_admin_settingMail.jpg" alt="Alternate Text" width="100"
							height="100" />
					</div>
				</td>
			</tr>
		</table>
	</div>
	<%Html.EndForm(); %>
</asp:Content>
