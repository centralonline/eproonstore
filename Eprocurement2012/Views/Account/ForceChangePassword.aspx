﻿<%@ Page Language="C#" MasterPageFile="~/Views/Shared/SelectSite.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left: 10px;">
			<%=Html._("Account.ForceChangePassword.Periodecally")%></p>
		<p class="epro-p" style="padding-left: 10px;">
			<%=Html._("Account.ForceChangePassword.PasswordExpired")%></p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<div class="eclear">
		</div>
		<div style="padding-top: 0px; width: 510px; margin-top: 20px; margin-left: 20px;
			text-align: left; font-size: 15px; font-weight: normal;">
			<p>
				<%=Html._("Account.ForceChangePassword.RulesChangePassword")%></p>
			<p style="color: #C00; text-decoration: underline;">
				<%=Html._("Account.ForceChangePassword.RulesSetPassword")%></p>
			<p>
				<%=Html._("Account.ForceChangePassword.NumbersOrLettersAtLeastEight")%><br />
				<%=Html._("Account.ForceChangePassword.NotUseSymbols")%>
			</p>
			<div>
				<div style="padding-top: 30px;">
					<%Html.BeginForm("ForceChangePassword", "Account", FormMethod.Post); %>
					<%Html.RenderPartial("ChangePassword", new Eprocurement2012.Models.Password());%>
					<%Html.EndForm(); %>
				</div>
			</div>
		</div>
	</div>
	<!--End new design-->
</asp:Content>
