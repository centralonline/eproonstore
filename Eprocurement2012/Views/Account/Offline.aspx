﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>officemate e-Procurement ปิดทำการปรับปรุงระบบชั่วคราว กราบขอภัยมา ณ ที่นี้</title>
</head>
<body>
	<div style="display: table; height: 100%; left: 0px; position: absolute; top: 0px;
		width: 100%;">
		<p style="text-align: center; display: table-cell; vertical-align: middle;font-size:small;">
			<img alt="officemate" src="/images/logo-epro.gif"/><br />
			officemate e-Procurement ปิดทำการปรับปรุงระบบชั่วคราว<br />
			กราบขออภัยมา ณ ที่นี้<br />
		</p>
	</div>
</body>
</html>
