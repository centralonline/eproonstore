﻿<%@ Page Language="C#" Inherits="System.Web.Mvc.ViewPage" MasterPageFile="~/Views/Shared/SelectSite.Master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<% 
		Html.BeginForm("CloseSite", "Site", FormMethod.Post); 
	%>
	<div id="epro-content" class="100percent">
		<p class="pTitle" style="padding-left: 10px;">
			<%=Html._("Account.CloseSite.SelectSite")%></p>
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>
		<table>
			<tr>
				<td>
					<p class="pTitle" style="padding-left: 10px;">
						<%=Html._("Account.CloseSite.NoUserNotify")%></p>
					<p class="epro-p" style="padding-left: 10px;">
						<%=Html._("Account.CloseSite.NoUserNotifyEmail")%></p>
					<p class="epro-p" style="padding-left: 10px;">
						<%=Html._("Account.CloseSite.UserNotifyMessage")%></p>
					<div style="padding-left: 10px;">
						<textarea id="Textarea1" name="adminRemark" cols="40" rows="5"></textarea>
						<p class="epro-p">
							*<%=Html._("Account.CloseSite.WebMessages")%></p>
						<input class="button-e2" id="Submit1" name="closeSite.x" type="submit" value="<%=Html._("Button.CloseSite")%>" />
					</div>
					<div class="eclear">
					</div>
					<div style="padding-left: 10px;">
						<img src="/images/theme/icon_admin_setting.jpg" alt="Alternate Text" width="100"
							height="100" />
					</div>
				</td>
				<td>
					<p class="pTitle" style="padding-left: 10px;">
						<%=Html._("Account.CloseSite.Email")%></p>
					<p class="epro-p" style="padding-left: 10px;">
						<%=Html._("Account.CloseSite.UserNotifyEmail")%></p>
					<p class="epro-p" style="padding-left: 10px;">
						<%=Html._("Account.CloseSite.UserNotifyMessage")%></p>
					<div style="padding-left: 10px;">
						<textarea id="Textarea2" name="adminRemarkMail" cols="40" rows="5"></textarea>
						<p class="epro-p">
							*<%=Html._("Account.CloseSite.WebMessagesEmail")%></p>
						<input class="button-e2" id="Submit2" name="closeSiteMail.x" type="submit" value="<%=Html._("Button.CloseSite.Email")%>" />
					</div>
					<div class="eclear">
					</div>
					<div style="padding-left: 10px;">
						<img src="/images/theme/icon_admin_settingMail.jpg" alt="Alternate Text" width="100"
							height="100" />
					</div>
				</td>
			</tr>
		</table>
	</div>
	<% 
		Html.EndForm(); 
	%>
</asp:Content>
