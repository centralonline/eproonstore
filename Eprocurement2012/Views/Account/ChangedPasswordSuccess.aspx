﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master"
	Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<div style="width: 100%">
		<img alt="" width="950" height="425" src="/images/theme/Slide-Password.jpg" class=" wp-post-image" />
	</div>
	<img alt="" src="/images/index/slideShadow.jpg" />
	<div id="epro-content" class="100percent" style="min-height: 200px;">
		<div id="applyform" class="100percent">
			<img alt="" src="/images/theme/title-newPassword.jpg" />
			<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
				width: 100%; height: 19px;">
			</div>
			<p class="positionApplylabel">
				<%=Html._("Account.ChangedPasswordSuccess")%></p>
			<%Html.BeginForm("Index", "Home"); %>
			<input class="graybutton" type="submit" value="<%=Html._("Button.IndexBack")%>" /><!--ดีไซน์ไว้รอโค๊ดค่ะ-->
			<%Html.EndForm(); %>
		</div>
	</div>
</asp:Content>
