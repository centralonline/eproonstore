﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/ApproveOrder/Order.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.OrderData>" %>

<asp:Content ID="Content2" ContentPlaceHolderID="MainContent" runat="server">
	<style type="text/css">
		body
		{
			font: normal 11px/14px Tahoma;
			color: #7C7C7C;
			line-height: 25px;
		}
		p
		{
			color: #7C7C7C;
		}
		.Loginlabel
		{
			color: #000000;
			display: block;
			float: left;
			font-weight: normal;
			position: relative;
			text-align: left;
			width: 80px;
			font-size: 13px;
		}
		.LoginSpanGray
		{
			font-size: 13px;
			margin-left: 0px;
			padding-top: 0px;
			color: #666666;
			margin-top: -5px;
		}
		.LoginAlert
		{
			font-size: 10px;
			margin-left: 0px;
			padding-top: 0px;
			color: Red;
			margin-top: -5px;
		}
		.forget a:link, .forget a:visited
		{
			color: #38789B;
			font-size: 13px;
		}
		.forget a:hover
		{
			color: #000000;
		}
		.bg-login
		{
			background-image: url(/images/index/bg-login.jpg);
			background-position: top;
			width: 299px;
			height: 205px;
		}
	</style>
	<script type="text/javascript" language="javascript">

	</script>
	<div class="centerwrap">
		<div class="spaceTop">
		</div>
		<%if (!Model.User.IsLogin)
	{ %>
		<!--login Box-->
		<div class="GroupData">
			<div class="border2">
				<div style="text-align: center;">
					<h3 style="color: Red;">
						กรุณา LogIn เข้าสู่ระบบก่อนค่ะ</h3>
				</div>
				<br />
				<div id="epro-login" class="one_third bg-login" style="margin: 0 auto;">
					<% Html.BeginForm("LogInForApprovalOrder", "Account", FormMethod.Post); %>
					<%=Html.Hidden("id", Model.Order.OrderGuid) %>
					<%=Html.Hidden("IsGoodReceive", false)%>
					<div style="padding-top: 60px; margin-left: 25px; padding-bottom: 0;">
						<div class="Loginlabel">
							<label style="width: 80px;">
								<%=Html._("Shared.LogInCenter.UserId")%></label>
						</div>
						<div style="float: left;">
							<p class="LoginSpanGray">
								<%= Html.TextBox("UserId")%></p>
							<p class="LoginAlert">
								<%=TempData["Error"]%></p>
						</div>
						<div class="eclear">
						</div>
						<div class="Loginlabel">
							<label style="width: 80px;">
								<%=Html._("Shared.LogInCenter.Password")%></label>
						</div>
						<div style="float: left;">
							<p class="LoginSpanGray">
								<%= Html.Password("Password")%></p>
						</div>
						<div class="eclear">
						</div>
					</div>
					<div style="width: 250px; height: 50px; margin-top: 0px; position: absolute; margin-left: 10px;">
						<p style="float: left; margin-top: 0px; cursor: pointer; padding-left: 95px;">
							<input class="imagebut" id="loginUser" name="loginUser" type="submit" value="<%=Html._("Shared.LogInCenter.ButtonLogIn")%>" /></p>
						<div class="forget" style="width: 70px; padding-top: 5px; padding-left: 0px; float: right;
							padding-right: 0px;">
							<a href="<%=Url.FullUrlAction("ForgotPassword","Account")%>">
								<%=Html._("Account.LogInCenter.ForgotPassword")%></a></div>
					</div>
					<% Html.EndForm(); %>
				</div>
				<!--login Box-->
				<%}%>
			</div>
		</div>
		<div class="spaceTop">
		</div>
	</div>
	<div class="centerwrap">
		<div class="step">
			<img alt="Step 3" src="../../images/theme/Step-3.jpg" /></div>
		<div class="GroupData">
			<h3>
				<img alt="Step 3" src="../../images/theme/h3-list-icon.png" /><%=Html._("Order.ViewOrder.OrderCompleted")%></h3>
		</div>
		<!--column right-->
		<div class="GroupData">
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ViewOrder.StepProcess")%></h4>
				<div class="border2">
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderId")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderID %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderDate")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewDataOrder.OrderStatus")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%=Html._(Model.Order.Status.ToResourceKey())%>
							<a href="/Help/OrderInstruction" target="_blank">
								<%=Html._("Order.ViewOrder.OrderDetail")%></a></span>
					</div>
					<div class="eclear">
					</div>
					<div class="position">
						<label>
							<%=Html._("Order.ViewOrder.WaitingApproval")%>
							:</label>
					</div>
					<div style="float: left;">
						<span class="textSpanGray">
							<%if (Model.Order.CurrentApprover == null)
		 {%>
							<%=Model.Order.PreviousApprover.Approver.DisplayName%>
							<%} %>
							<%else
		 {%>
							<%=Model.Order.CurrentApprover.Approver.DisplayName%>
							<%} %>
						</span>
					</div>
					<div class="eclear">
					</div>
					<%Html.RenderPartial("OrderDataFlow", Model.Order); %>
				</div>
			</div>
		</div>
		<!--end GroupData--->
		<div class="eclear">
		</div>
		<!--Print-->
		<div class="eclear">
		</div>
		<div style="margin: 30px 0;">
			<img src="/images/theme/logo-ePro.jpg" class="left" />
			<div class="right" style="width: 120px; margin-right: 9px; margin-top: 10px; border: 1px solid #E5E5E5;
				border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px; padding: 5px;">
				<img src="/images/theme/icon_printOrder.jpg" />
				<a href="#" onclick="window.print(); return false">
					<%=Html._("Order.ViewOrder.Print")%></a>
			</div>
			<div class="eclear">
			</div>
			<!--Start printOrder detail-->
			<div style="width: 100%; padding: 20px 8px 8px 0px; margin-right: 20px; margin-top: 35px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderID %></p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="left" style="width: 450px; min-height: 250px; padding: 5px; margin-right: 9px;
				margin-top: 0px; border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CustID")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.CostCenter.CostCenterCustID %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("CartDetail.ShipContactor")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.UserId")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Requester.UserId%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Phone")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorPhone %>
						<%=string.IsNullOrEmpty(Model.Order.Contact.ContactorExtension) ? "" : " #" + Model.Order.Contact.ContactorExtension%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.Fax")%>
						:
					</label>
				</div>
				<div style="float: left;">
					<p class="printOrderSpanGray">
						<%=Model.Order.Contact.ContactorFax %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.InvoiceAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Invoice.Address1 %>
						<%=Model.Order.Invoice.Address2 %>
						<%=Model.Order.Invoice.Address3 %>
						<%=Model.Order.Invoice.Address4 %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<div class="right" style="width: 450px; min-height: 250px; margin-right: 9px; margin-top: 0px;
				border: 1px solid #E5E5E5; border-radius: 5px 5px 5px 5px; -moz-border-radius: 5px 5px 5px 5px;
				padding: 5px;">
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.CompanyId")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyId %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CompanyName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Company.CompanyName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.DepartmentName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=string.IsNullOrEmpty(Model.Order.Department.DepartmentID)? "-": "[" + Model.Order.Department.DepartmentID + "] " + Model.Order.Department.DepartmentName %>
					</p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.CostCenterName")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						[<%=Model.Order.CostCenter.CostCenterID%>]<%=Model.Order.CostCenter.CostCenterName %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewDataOrder.OrderDate")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.OrderDate.ToString(new DateTimeFormat()) %></p>
				</div>
				<div class="eclear">
				</div>
				<div class="printOrderlabel">
					<label>
						<%=Html._("Order.ViewOrder.ShippingAddress")%>
						:
					</label>
				</div>
				<div class="left">
					<p class="printOrderSpanGray">
						<%=Model.Order.Shipping.Address1 %>
						<%=Model.Order.Shipping.Address2 %>
						<%=Model.Order.Shipping.Address3 %>
						<%=Model.Order.Shipping.Address4 %>
						<%=Model.Order.Shipping.Province %>
					</p>
				</div>
				<div class="eclear">
				</div>
			</div>
			<!--end printOrder detail-->
			<div class="eclear">
			</div>
			<div class="printOrderlabel" style="padding-top: 20px; padding-bottom: 10px;">
				<label>
					<%=Html._("Order.ViewOrder.OrderItems")%>
				</label>
			</div>
			<%Html.RenderPartial("OrderDetailPartial", Model.Order); %>
			<div class="eclear">
			</div>
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ViewOrder.Remark")%></h4>
				<div class="border2">
					<div class="Remarklabel">
						<label>
							<%=Html._("Order.ViewOrder.ApproverRemark")%>
							:</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.ApproverRemark %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="Remarklabel">
						<label>
							<%=Html._("Order.ViewOrder.OFMRemark")%>
							:</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.OFMRemark %></p>
					</div>
					<div class="eclear">
					</div>
					<div class="Remarklabel">
						<label>
							<%=Html._("Order.ViewOrder.ReferenceRemark")%>
							:</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%=Model.Order.ReferenceRemark %></p>
					</div>
					<div class="eclear">
					</div>
					<!--Show AttachFile -->
					<div class="Remarklabel">
						<label>
							<%=Html._("Order.AttachFile")%>
							:</label>
					</div>
					<div class="left">
						<p class="printOrderSpanGray">
							<%if (!String.IsNullOrEmpty(Model.Order.CustFileName))
		 {%>
							<a target="_blank" href="<%=Url.Action("DownloadAttachFile", "Order", new { systemFileName = Model.Order.SystemFileName})%>">
								<%=Model.Order.CustFileName %></a>
							<%} %>
							<%else
		 {%>
							<p>
								-</p>
							<%} %>
						</p>
					</div>
					<div class="eclear">
					</div>
					<!--End AttachFile -->
				</div>
			</div>
			<!--Column-Full-->
			<div class="eclear">
			</div>
			<div class="Column-Full">
				<h4>
					<%=Html._("Order.ViewOrder.OrderActivity")%></h4>
				<div class="eclear">
				</div>
				<%Html.RenderPartial("OrderActivityPartial", Model.OrderActivity); %>
			</div>
			<div class="eclear">
			</div>
		</div>
	</div>
</asp:Content>
