﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.LogIn>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<%--	<div class="nivo_slider_plugin">
		<div id="nivo" class="nivoSlider">
			
                   
		    <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-NewYearBless.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-NewYearBless"
						target="_blank">
						<img width="950" height="400" src="/images/theme/Main-NewYear-Blessing.jpg"class="wp-post-image" alt="" /></a> 
                            
                                    <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-SOHO.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-LD-Jan16-SOHO"
										target="_blank">
										<img width="950" height="400" src="/images/theme/MainBanner-Jan16-SOHO.jpg"
											class="wp-post-image" alt="" /></a> 

                                            <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan-16-BrandOne.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan-16-BrandOne" target="_blank">
												<img width="950" height="400" src="/images/theme/MainBanner-Jan-16-Brand-One.jpg" class="wp-post-image"
													alt="" /></a>

                                                     <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-PremuimChair.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-PremuimChair" target="_blank">
												<img width="950" height="400" src="/images/theme/MainBanner-Jan-16-Promotion-PremuimChair02.jpg" class="wp-post-image"
													alt="" /></a>

                                                     <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-Printingsolution.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-Printingsolution" target="_blank">
												<img width="950" height="400" src="/images/theme/Main-Card1Baht.jpg" class="wp-post-image"
													alt="" /></a>
		</div>
	</div>--%>
	<img alt="" src="/images/index/slideShadow.jpg" />
	<div id="epro-content" class="100percent">
		<!--Log in-->
		<div id="epro-login" class="one_third if_last bg-login">
			<% Html.BeginForm("LogIn", "Account", FormMethod.Post); %>
			<div id="mobile">
				<%=Html.Hidden("RedirectUrl", Model.RedirectUrl)%>
				<div class="positionLoginlabel">
					<div class="Loginlabel">
						<label>
							<%=Html._("Shared.LogInCenter.UserId")%></label>
					</div>
					<div class="leftFloat" style="margin-bottom: 5px;">
						<p class="LoginSpanGray">
							<%= Html.TextBoxFor(model => model.UserId, new { maxlength = 100 })%></p>
						<p class="LoginAlert" style="margin-top: -10px;">
							<%=Html.LocalizedValidationMessageFor(model => model.UserId)%></p>
					</div>
					<div class="eclear">
					</div>
					<div class="Loginlabel">
						<label>
							<%=Html._("Shared.LogInCenter.Password")%></label>
					</div>
					<div class="leftFloat" style="margin-bottom: 2px;">
						<p class="LoginSpanGray">
							<%= Html.PasswordFor(model => model.Password, new { maxlength = 60 })%></p>
						<p class="LoginAlert" style="margin-top: -10px;">
							<%=Html.LocalizedValidationMessageFor(model => model.Password)%></p>
					</div>
					<div class="eclear">
					</div>
				</div>
			</div>
			<div class="eclear">
			</div>
			<div id="positionButtonLogin" style="padding-top: 5px;">
				<p>
					<input class="imagebut" id="loginUser" name="loginUser" type="submit" value="<%=Html._("Shared.LogInCenter.ButtonLogIn")%>" /></p>
				<div id="forget" class="forget styleforget">
					<a href="<%=Url.FullUrlAction("ForgotPassword","Account")%>">
						<%=Html._("Account.LogInCenter.ForgotPassword")%></a>
				</div>
			</div>
			<!--ปิดResponsiveชั่วคราว<div id="forgetButton" class="forget forgetButtonstyle">
				<a href="<%=Url.FullUrlAction("ForgotPassword","Account")%>">ลืมรหัสผ่าน?</a></div>-->
		</div>
		<% Html.EndForm(); %>
	</div>
	<!--Log in-->
	<%--<div id="epro-register" class="one_third">
		<a href="/Account/ApplyNow">
			<img alt="" src="/images/index/title_b5.jpg" id="title-regis" /></a><a href="/Account/ApplyNow">
				<img alt="" src="/images/index/banner2.jpg" /></a>
	</div>
	<div id="epro-catalog" class="one_third">
		<a href="http://www.officemate.co.th/CatalogRegister">
			<img alt="" src="/images/index/title_b1.jpg" /></a><a href="http://www.officemate.co.th/CatalogRegister">
				<img alt="" src="/images/index/Catalog2016-Epro.jpg" /></a>
	</div>--%>
	<div class="two_third">
		<img width="630" src="/images/theme/BannerMain.jpg" class="wp-post-image" alt="" />
	</div>
	<div class="eclear">
	</div>
	<div id="epro-news" class="two_third">
		<%--<img alt="" src="/images/index/newsEvent.jpg" />
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
			width: 100%; height: 19px;">
		</div>--%>
		<%--<div class="100percent">
			<img alt="" src="/images/index/customer1.jpg" class="imgepro-left" />
			<p class="epro-p">
				<%=Html._("Account.LogInCenter.NewsAndEventContent1")%>
				<a style="color: #38789B; font-size: 14px;" href="<%=Url.Action("News1","StaticPage")%>"
					target="_blank">
					<%=Html._("Account.LogInCenter.Readmore")%></a><br />
				<span class="e-highlight">
					<%=Html._("Account.LogInCenter.EndNewsAndEventContent1")%></span>
			</p>
			<br clear="all" />
		</div>--%>
		<%--<div class="100percent">
			<img alt="" src="/images/index/banner-embassy.jpg" class="imgepro-left" />
			<p class="epro-p">
					ประกาศรางวัลบัตรภาพยนตร์ Dracula Untold แบบเหมาโรงที่ Embassy (Sep 2014)<br />
					<a style="color: #38789B; font-size: 14px;" href="http://www.officemate.co.th/Activity/news/EmbassySep2.html?utm_source=ePro-home&utm_medium=main&utm_campaign=eprocurement-101057-EmbassySep3"
					target="_blank">...Click</a><br />
			</p>
			<br clear="all" />
		</div>--%>
		<div class="eclear">
		</div>
		<%--  %><div class="100percent">
			<img alt="" src="/images/index/News-Update-Epro-Saminar-min.jpg" class="imgepro-left" />
			<p class="epro-p">
				<%=Html._("Account.LogInCenter.NewsAndEventContent2")%>
				<a style="color: #38789B; font-size: 14px;" href="<%=Url.Action("News2","StaticPage")%>"
					target="_blank">
					<%=Html._("Account.LogInCenter.Readmore")%></a><br />
				<span class="e-highlight">
					<%=Html._("Account.LogInCenter.EndNewsAndEventContent2")%></span>
			</p>
			<br clear="all" />
		</div>--%>
		<div class="eclear">
		</div>
        <%--<div class="100percent">
			<img alt="" src="/images/index/News-Update-Epro-Saminar-min.jpg" class="imgepro-left" />
			<p class="epro-p">
				ออฟฟิศเมทจัดสัมมนาครั้งใหญ่ เปิดวิสัยทัศน์เหล่านักจัดซื้อบนกระแสออนไลน์ ในหัวข้อ“ทันกระแสดิจิตอล จัดซื้อออนไลน์” 
				<br />
				<a style="color: #38789B; font-size: 14px;" href="http://www.officemate.co.th/Activity/news/LandingPage-Sep-2015-Epro-Saminar.html?utm_source=ofm-home&utm_medium=main&utm_campaign=Sep-2015-Epro-Saminar"
					target="_blank">คลิกอ่านรายละเอียด</a><br />
			</p>
			<br clear="all" />
		</div>--%>
		<div class="eclear">
		</div>

	<%--	<div class="100percent">
			<img alt="" src="/images/index/Even001-Epro.jpg" class="imgepro-left" />
			<p class="epro-p">
				ออฟฟิศเมทจัดสัมมนาครั้งใหญ่ตอกย้ำกระแสยุคดิจิตอล ในหัวข้อเรื่อง “ซื้ออย่างไร ให้ทันยุคดิจิตอล”
				<br />
				<a style="color: #38789B; font-size: 14px;" href="http://www.officemate.co.th/Activity/news/June2015-Landing-Even001-Epro.html?utm_source=ofm-home&utm_medium=main&utm_campaign=Infomation-News"
					target="_blank">คลิกอ่านรายละเอียด</a><br />
			</p>
			<br clear="all" />
		</div>--%>
		<div class="eclear">
		</div>
		<%--<div class="100percent">
			<img alt="" src="/images/index/Even002-Epro.jpg" class="imgepro-left" />
			<p class="epro-p">
				กลุ่มออฟฟิศเมทเปลี่ยนชื่อใหม่เป็น ซีโอแอล
				<br />
				<a style="color: #38789B; font-size: 14px;" href="http://www.officemate.co.th/Activity/news/June2015-Landing-Even002-Epro.html?utm_source=ofm-home&utm_medium=main&utm_campaign=COL"
					target="_blank">คลิกอ่านรายละเอียด</a><br />
			</p>
			<br clear="all" />
		</div>--%>
	</div>
	<!-- end two_third-->
<%--	<div id="epro-testimonial" class="one_third if_last">
		<div style="width: 100%">

        <img alt="" src="/images/index/Head-Vdo-Epro.jpg" />
			<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
				width: 100%; height: 19px;">
			</div>
			<!-- Video -->
           <!--  <div id="player"></div>-->

    <iframe width="300" height="200" src="https://www.youtube.com/embed/QZVTrB_zMqs" frameborder="0" allowfullscreen></iframe>

        </ br>
         </ br>
			<img alt="" src="/images/index/title_b4.jpg" style=" margin-top:20px;" />
			<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
				width: 100%; height: 19px;">
			</div>
			<!-- Video -->
           <!--  <div id="player"></div>-->

    <iframe width="300" height="200" src="https://www.youtube.com/embed/TNyZQ5qMGZU" frameborder="0" allowfullscreen></iframe>

		<!--<div class="100percent">
				<div id="videoPlayer">
                       <iframe width="300" height="200"src="">
    </iframe>
					<script type="text/javascript" src="/swfobject/swfobject.js"></script>
					<script type="text/javascript">
						var flashvars = {
							htmlPage: document.location,
							settingsFile: "seminar.xml"
						};
						var params = {
							allowFullScreen: "true"
						};

						swfobject.embedSWF("/videoPlayer.swf", "videoPlayer", "300", "200", "9.0.115", "/swfobject/vdo-web-epro-guest final.wmv", flashvars, params);
					</script>
					<p>
						<a href="http://www.adobe.com/go/getflashplayer">
							<img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif"
								alt="Get Adobe Flash player" /></a></p>
				</div>
			</div> -->
			<br />
			<!-- End Video -->
			<div>
				<a style="color: #38789B; font-size: 14px;" href="<%=Url.Action("Video","Account")%>"
					target="_blank">View All</a>
			</div>
		</div>
	</div>--%>
	<!--end one_third -->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScriptContent" runat="server">
	<!--[if IE 6]>
<SCRIPT LANGUAGE="Javascript">
			alert("Congratulations! You are running Internet Explorer 6 or greater.");
			<p>
		Thank you for closing the message box.</p>
	</script>
<![endif]-->
</asp:Content>
