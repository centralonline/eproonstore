﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master" Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div style="margin-top:150px;color:#43799B;">
		<h2 style="text-align:center">
			e-Mail ของคุณ ยังไม่ได้ Verify User<br />
			กรุณาคลิก Link จาก e-Mail เพื่อเข้าใช้งานค่ะ<br />
			หากไม่ได้รับ e-Mail กรุณา <%=Html.ActionLink("คลิกที่นี่", "SendMailVerifyUser", "Account", new { userGuid = ViewData["userGuid"].ToString() }, null)%> ค่ะ<br /><br />
			<span style="color:Red"> <%=ViewData["Data"]%> </span>
		</h2>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
