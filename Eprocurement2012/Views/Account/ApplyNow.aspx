﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master"
	Inherits="System.Web.Mvc.ViewPage<Eprocurement2012.Models.ApplyNow>" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<script language="javascript" type="text/javascript">
		$(document).ready(function () {
			$(".number").keydown(function (event) {
				// Allow: backspace, delete, tab, escape, and enter
				if (event.keyCode == 46 || event.keyCode == 8 || event.keyCode == 9 || event.keyCode == 27 ||
							 event.keyCode == 13 || (event.keyCode == 65 && event.ctrlKey === true) ||
							(event.keyCode >= 35 && event.keyCode <= 39))
				{ }
				else {
					if (event.shiftKey || (event.keyCode < 48 || event.keyCode > 57) && (event.keyCode < 96 || event.keyCode > 105)) {
						event.preventDefault();
					}
				}
			});
		});
	</script>
	<div id="nivo" class="nivoSlider">
					

			   
		    <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-NewYearBless.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-NewYearBless"
						target="_blank">
						<img width="950" height="400" src="/images/theme/Main-NewYear-Blessing.jpg"class="wp-post-image" alt="" /></a> 
                            
                                    <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-SOHO.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-LD-Jan16-SOHO"
										target="_blank">
										<img width="950" height="400" src="/images/theme/MainBanner-Jan16-SOHO.jpg"
											class="wp-post-image" alt="" /></a> 

                                            <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan-16-BrandOne.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan-16-BrandOne" target="_blank">
												<img width="950" height="400" src="/images/theme/MainBanner-Jan-16-Brand-One.jpg" class="wp-post-image"
													alt="" /></a>

                                                     <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-PremuimChair.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-PremuimChair" target="_blank">
												<img width="950" height="400" src="/images/theme/MainBanner-Jan-16-Promotion-PremuimChair02.jpg" class="wp-post-image"
													alt="" /></a>

                                                     <a href="http://storesupplyuse.officemate.int/Activity/news/LD-Jan16-Printingsolution.html?utm_source=eprocurement-home&utm_medium=main&utm_campaign=Jan16-Printingsolution" target="_blank">
												<img width="950" height="400" src="/images/theme/Main-Card1Baht.jpg" class="wp-post-image"
													alt="" /></a>
                        
                     
  
		</div>
	<img alt="" src="/images/index/slideShadow.jpg" />
	<div id="epro-content" class="100percent">
		<div id="applyform" class="100percent">
			<img alt="" src="/images/theme/title_ApplyNow.jpg" />
			<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;
				width: 100%; height: 19px;">
			</div>
			<p class="pTitle">
				<%=Html._("Account.ApplyNow.Title")%></p>
			<p class="positionApplylabel">
				<%=Html._("Account.ApplyNow.Content")%></p>
			<%Html.BeginForm("ApplyNow", "Account", FormMethod.Post); %>
			<h4>
				<%=Html._("Account.ApplyNow.InformationCorporate")%></h4>
			<!--Form-->
			<!--************ลงดีไซน์เรียบร้อยแล้วค่ะ ถ้าลงโค๊ดตัวข้อความแจ้งงเตือน ก็ใส่ในช่อง p ตำแหน่งแสดงผลข้อความแจ้งเตือนได้เลยจ้า Linda************************-->
			<div class="positionApplylabel">
				<div class="applylabel">
					<label>
						<%=Html._("Account.ApplyNow.CompanyName")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray">
						<%=Html.TextBoxFor(m => m.CompanyName, new { maxlength = 100 })%></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.CompanyName)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label>
						<%=Html._("Account.ApplyNow.BusinessType")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray">
						<%=Html.DropDownListFor(m => m.BusinessType, new SelectList(Model.ListBusinessType, "Name", "Name"), Html._("Account.ApplyNow.SelectBusinessType"))%></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.BusinessType)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label>
						<%=Html._("Account.ApplyNow.EmployeesNumber")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray number">
						<%=Html.TextBoxFor(m => m.CustEmpNum)%>
						<%=Html._("Account.ApplyNow.People")%></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.CustEmpNum)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label>
						<%=Html._("Account.ApplyNow.NumberOfCompany")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray number">
						<%=Html.TextBoxFor(m => m.NumOfCompany) %>
						<%=Html._("Account.ApplyNow.NoneCompany")%></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.NumOfCompany)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label>
						<%=Html._("Account.ApplyNow.NameAndLastName")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray">
						<%=Html.TextBoxFor(m => m.Name) %></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.Name)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label>
						<%=Html._("Account.ApplyNow.Position")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray">
						<%=Html.TextBoxFor(m => m.Position) %></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.Position)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label>
						<%=Html._("Shared.DataUserProfile.PhoneNo")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray number">
						<%=Html.TextBoxFor(m => m.PhoneNo, new { maxlength = "10" })%></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.PhoneNo)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label>
						<%=Html._("Shared.DataUserProfile.mobile")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray number">
						<%=Html.TextBoxFor(m => m.MobileNo, new { maxlength="10"})%></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.MobileNo)%></p>
				</div>
				<div class="eclear">
				</div>
				<div class="applylabel">
					<label>
						<%=Html._("Shared.MyProfile.Email")%>
						<span style="color: Red">*</span></label>
				</div>
				<div class="leftFloat">
					<p class="applySpanGray">
						<%=Html.TextBoxFor(m => m.Email) %></p>
					<p class="applyAlert">
						<%=Html.LocalizedValidationMessageFor(model => model.Email)%></p>
				</div>
				<div class="eclear">
				</div>
				<h4>
					<%=Html._("Account.ApplyNow.OfficemateCustomer")%>
					<span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(model => model.IsCustomer)%></span></h4>
				<div class="positionApplylabel">
					<p>
						<%=Html.RadioButtonFor(m => m.IsCustomer,"Yes") %><%=Html._("Account.ApplyNow.SpecifiesCustId")%>
						<%=Html.TextBoxFor(m => m.CustID, new { maxlength = 6})%>
						<%=Html._("Account.ApplyNow.CustomerCode")%></p>
					<p>
						<%=Html.RadioButtonFor(m => m.IsCustomer,"No") %>
						<%=Html._("Account.ApplyNow.NotService")%></p>
				</div>
				<h4>
					<%=Html._("Account.ApplyNow.OrganizationUseE-Procurement")%>
					<span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(model => model.InterestProgram)%></span></h4>
				<div class="positionApplylabel">
					<p>
						<%=Html.RadioButtonFor(m => m.InterestProgram, Html._("Account.ApplyNow.UseTheSystemImmediately"))%><%=Html._("Account.ApplyNow.UseTheSystemImmediately")%></p>
					<p>
						<%=Html.RadioButtonFor(m => m.InterestProgram, Html._("Account.ApplyNow.DecisionForInformation"))%><%=Html._("Account.ApplyNow.DecisionForInformation")%></p>
				</div>
				<h4>
					<%=Html._("Account.ApplyNow.AffiliateWantSystemE-Procurement")%>
					<span style="color: Red">*
						<%=Html.LocalizedValidationMessageFor(model => model.UseOnline)%></span></h4>
				<div class="positionApplylabel">
					<p>
						<%=Html.RadioButtonFor(m => m.UseOnline, Html._("Account.ApplyNow.AffiliateOpenOfService"))%><%=Html._("Account.ApplyNow.AffiliateOpenOfService")%></p>
					<p>
						<%=Html.RadioButtonFor(m => m.UseOnline, Html._("Account.ApplyNow.TheTryToSome"))%><%=Html._("Account.ApplyNow.TheTryToSome")%></p>
				</div>
				<p>
					<%=Html._("Account.ApplyNow.CanActivate")%></p>
				<div class="positionApplylabel">
					<p>
						<a href="">
							<img alt="Captcha" src="<%=Url.Action("CaptchaImage","Captcha") %>" style="" /></a>
					</p>
					<p>
						<%=Html.EditorFor(m =>m.Captcha)%>
						<p class="applyAlert">
							<%=Html.ValidationMessageFor(m => m.Captcha)%>
						</p>
					</p>
				</div>
				<div id="btn-tablet">
					<input type="submit" value="<%=Html._("Button.Confirm")%>" class="graybutton" /></div>
				<!--Hide Responsive<div id="btn-mobile"><input type="submit" value="submit" class="imagebut"/></div>-->
			</div>
			<!--Form-->
		</div>
	</div>
	<%Html.EndForm(); %>
</asp:Content>
