﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master"
	Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div class="nivo_slider_plugin">
		<div id="nivo" class="nivoSlider">
			<a href="http://www.officemate.co.th/Activity/news/FurnitureFair2.html?utm_source=epro-home&utm_medium=main&utm_campaign=eprocurement-040857-FurnitureFair"
				target="_blank">
				<img width="950" height="425" src="/images/theme/slide-01.jpg" class="wp-post-image"
					alt="" />
			</a><a href="http://www.officemate.co.th/Activity/news/AutomatoinSale2.html?utm_source=epro-home&utm_medium=main&utm_campaign=eprocurement-040857-AutomatoinSale"
				target="_blank">
				<img width="950" height="425" src="/images/theme/slide-02.jpg" class="wp-post-image"
					alt="" />
			</a><a href="http://www.officemate.co.th/Activity/news/CrazySale2.html?utm_source=epro-home&utm_medium=main&utm_campaign=eprocurement-010857-CrazySale"
				target="_blank">
				<img width="950" height="425" src="/images/theme/slide-03.jpg" class="wp-post-image"
					alt="" />
			</a><a href="http://www.officemate.co.th/Activity/news/LeaflePro2014.html?utm_source=ePro-home&utm_medium=main&utm_campaign=eprocurement-040657-LeaflePro2014"
				target="_blank">
				<img width="950" height="425" src="/images/theme/slide-04.jpg" class="wp-post-image"
					alt="" />
			</a><a href="http://www.officemate.co.th/PrintingSolution/promotion2.html?utm_source=officemate&utm_medium=main&utm_campaign=officemate-050857-Printing"
				target="_blank">
				<img width="950" height="425" src="/images/theme/slide-05.jpg" class="wp-post-image"
					alt="" />
			</a>
		</div>
	</div>
	<img alt="" src="/images/index/slideShadow.jpg" />
	<div class="complete-format" style="width: 950px">
		<p style="font-size: 13px; margin-top: 5px">
			<img src="/images/icon/icon_right.png" alt="right" class="left" style="margin: 5px" />
			<%=ViewData["message"]%></p>
		<p style="font-size: 13px; margin-top: 5px">
			หากต้องการซื้อสินค้าเพิ่มเติม <a href="/">คลิกที่นี่</a> เพื่อเลือกซื้อสินค้าต่อได้เลยค่ะ</p>
	</div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="JavaScriptContent" runat="server">
</asp:Content>
