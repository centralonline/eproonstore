﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Views/StaticPage/Intro.Master"
	Inherits="System.Web.Mvc.ViewPage" %>

<asp:Content ID="Content1" ContentPlaceHolderID="MainContent" runat="server">
	<div style="width: 100%">
		<img alt="" width="950" height="425" src="/images/theme/Slide-Password.jpg" class=" wp-post-image" />
	</div>
	<img alt="" src="/images/index/slideShadow.jpg" />
	<div id="epro-content" class="100percent" style="min-height:200px;">
		<div id="applyform" class="100percent">
		<img alt="" src="/images/theme/title-ForgotPass.jpg" />
		<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;width: 100%; height: 19px;"></div>
		<p class="positionApplylabel"><%=Html._("Account.ForgotPassword.Content")%></p>
	<div>
		<%Html.BeginForm("ForgotPassword", "Account", FormMethod.Post); %>
		<%=Html._("Account.ForgotPassword.Mail")%>
		<%=Html.TextBox("UserId", "", new { maxlength = 100 })%>
		<input  class="graybutton" type="submit" id="btnForgot" name="btnForgot" value="<%=Html._("Account.ForgotPassword.btnForgot")%>" />
		<span style="color: Red;">
			<%=TempData["Message"]%></span>
		<%Html.EndForm();%>
	</div>
	<div style="background-image: url(/images/index/grayLine.jpg); background-repeat: repeat-x;width: 100%; height: 19px; margin-top:60px;"></div>
		</div>
	</div>
</asp:Content>
