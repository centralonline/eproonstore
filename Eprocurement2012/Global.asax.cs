﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Configuration;
using Eprocurement2012.Controllers.Helpers;

namespace Eprocurement2012
{
	// Note: For instructions on enabling IIS6 or IIS7 classic mode, 
	// visit http://go.microsoft.com/?LinkId=9394801

	public class MvcApplication : System.Web.HttpApplication
	{

		private static log4net.ILog _log;

		protected void Application_Start()
		{
			InitilizeLog4Net();

			AreaRegistration.RegisterAllAreas();

			RegisterRoutes(RouteTable.Routes);
		}

		public static void RegisterRoutes(RouteCollection routes)
		{
			routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

			routes.MapRoute(
				"SearchProductSKU",
				"Product/SearchProductSKU",
			new { controller = "Product", action = "SearchProductSKU" }
			);

			routes.MapRoute(
				"Seminar",
				"Seminar/{action}",
			new { controller = "Seminar", action = "SignUp" }
			);

			routes.MapRoute(
				"Notifyme-Product",
				"Product/NotifyMe",
			new { controller = "Product", action = "NotifyMe" }
			);

			routes.MapRoute(
				"Search-Product",
				"Product/SearchProduct",
			new { controller = "Product", action = "SearchProduct" }
			);

			routes.MapRoute(
				"SearchResult",
				"Product/Search",
			new { controller = "Product", action = "Search" }
			);

			routes.MapRoute(
				"Department",
				"Department/{id}/{title}",
			new { controller = "Department", action = "DepartmentList", title = UrlParameter.Optional }
			);

			routes.MapRoute(
				"Site-Directory",
				"Site-Directory",
				new { controller = "Department", action = "SiteDirectory" }
			);

			routes.MapRoute(
				"Product-HandleBuyOfficeSupply",
				"CGI/HandleBuyOfficeSupply",
			new { controller = "Product", action = "HandleBuyOfficeSupply" }
			);

			routes.MapRoute(
				"ProductHandleBuy",
				"Product/HandleBuy",
			new { controller = "Product", action = "HandleBuy" }
			);

			routes.MapRoute(
				"QuickInfoProcessStatus",
				"Product/QuickInfoProcessStatus",
			new { controller = "Product", action = "QuickInfoProcessStatus" }
			);

			routes.MapRoute(
				"Product-QuickInfo",
				"Product/QuickInfo",
			new { controller = "Product", action = "QuickInfo" }
			);

			routes.MapRoute(
				"Product",
				"Product/{id}",
				new { controller = "Product", action = "Details", title = UrlParameter.Optional }
			);

			routes.MapRoute(
				"AccountProfile", // Route name
				"Account/{action}", // URL with parameters
			new { controller = "Account", action = "LogIn" } // Parameter defaults
			);

			routes.MapRoute(
				"Account", // Route name
				"Account/{action}/{id}", // URL with parameters
			 new { controller = "Account", id = UrlParameter.Optional } // Parameter defaults
			);

			routes.MapRoute(
				"UserProfile", // Route name
				"UserProfile/{action}/{id}", // URL with parameters
			 new { controller = "UserProfile", id = UrlParameter.Optional } // Parameter defaults
			);

			routes.MapRoute(
				"OfficemateAdmin", // Route name
				"OfficemateAdmin/{action}/{id}", // URL with parameters
			 new { controller = "OFMAdmin", action = "Index", id = UrlParameter.Optional } // Parameter defaults
			);

			routes.MapRoute(
				"Report", // Route name
				"Report/{action}/{id}", // URL with parameters
			 new { controller = "Report", action = "Index", id = UrlParameter.Optional } // Parameter defaults
			);

			routes.MapRoute(
				"StaticPage", // Route name
				"StaticPage/{viewName}", // URL with parameters
				new { controller = "StaticPage", action = "Index" } // Parameter defaults
			);

			routes.MapRoute(
				"Help", // Route name
				"Help/{viewName}", // URL with parameters
				new { controller = "Help", action = "Index" } // Parameter defaults
			);

			routes.MapRoute(
				"Default", // Route name
				"{controller}/{action}/{id}", // URL with parameters
				new { controller = "Home", action = "Index", id = UrlParameter.Optional } // Parameter defaults
			);

			routes.MapRoute(
				"Home",
				"Home/{action}",
				new { controller = "Home", action = "Index" }
			);

			RouteTable.Routes.MapRoute(
				"NotFound",
				"{*url}",
				new { controller = "Home", action = "UrlNotFound" }
			);

		}

		private void InitilizeLog4Net()
		{
			bool isTelnetAppenderActivated = true;
			if (Boolean.Parse(WebConfigurationManager.AppSettings["IsProduction"]))
			{
				string line = new string('=', 80);
				log4net.Layout.PatternLayout pattern = new log4net.Layout.PatternLayout(line + "%newline%-5level %logger%newline" + line + "%newline%message%newline");
				log4net.Appender.SmtpAppender smtpAppender = new log4net.Appender.SmtpAppender
				{
					From = "log4net@officemate.co.th",
					Subject = "Logged Events (OfficeMate e-Procurement Version5)",
					Threshold = log4net.Core.Level.Warn,
					Layout = pattern,
					BufferSize = 20,
					To = "monitorserver@officemate.co.th",
					SmtpHost = WebConfigurationManager.AppSettings["ProductionSmtpServer"],
					Username = WebConfigurationManager.AppSettings["ProductionSmtpServerUsername"],
					Password = WebConfigurationManager.AppSettings["ProductionSmtpServerPassword"]
				};
				smtpAppender.ActivateOptions();
				log4net.Config.BasicConfigurator.Configure(smtpAppender);

				try
				{
					log4net.Appender.TelnetAppender telnetAppender = new log4net.Appender.TelnetAppender
					{
						Layout = pattern,
						Threshold = log4net.Core.Level.Warn,
						Port = 6666
					};
					telnetAppender.ActivateOptions();
					log4net.Config.BasicConfigurator.Configure(telnetAppender);
				}
				catch (System.Net.Sockets.SocketException)
				{
					try
					{
						log4net.Appender.TelnetAppender telnetAppender = new log4net.Appender.TelnetAppender
						{
							Layout = pattern,
							Threshold = log4net.Core.Level.Warn,
							Port = 6667
						};
						telnetAppender.ActivateOptions();
						log4net.Config.BasicConfigurator.Configure(telnetAppender);
					}
					catch (System.Net.Sockets.SocketException)
					{
						isTelnetAppenderActivated = false;
					}
				}
			}
			else
			{
				log4net.Appender.UdpAppender udpAppender = new log4net.Appender.UdpAppender
				{
					Layout = new log4net.Layout.XmlLayoutSchemaLog4j(),
					RemoteAddress = System.Net.IPAddress.Loopback,
					RemotePort = 7071,
					Threshold = log4net.Core.Level.All
				};
				udpAppender.ActivateOptions();
				log4net.Config.BasicConfigurator.Configure(udpAppender);
			}

			if (Boolean.Parse(WebConfigurationManager.AppSettings["IsLogFile"]))
			{
				string line = new string('=', 80);
				log4net.Layout.PatternLayout pattern = new log4net.Layout.PatternLayout(line + "%newline %date [%thread] %newline%-5level %logger %newline" + line + "%newline %message %newline");
				log4net.Appender.FileAppender logFileAppender = new log4net.Appender.RollingFileAppender
				{
					File = System.Web.Hosting.HostingEnvironment.MapPath("/logger/log.txt"),
					AppendToFile = true,
					Layout = pattern,
					RollingStyle = log4net.Appender.RollingFileAppender.RollingMode.Size,
					MaxFileSize = 2 * 1024 * 1024,
					StaticLogFileName = true,
					Threshold = log4net.Core.Level.Warn,
					Encoding = System.Text.Encoding.UTF8,
					LockingModel = new log4net.Appender.FileAppender.MinimalLock(),
					CountDirection = 3
				};
				logFileAppender.ActivateOptions();
				log4net.Config.BasicConfigurator.Configure(logFileAppender);
			}

			log4net.LogManager.GetRepository().RendererMap.Put(typeof(ExceptionContext), new ExceptionContextRenderer());
			_log = log4net.LogManager.GetLogger(this.GetType());
			_log.Debug("log4net Configured");
			if (!isTelnetAppenderActivated) { _log.Warn("Unable to activate TelnetAppender"); }
		}
	}
}