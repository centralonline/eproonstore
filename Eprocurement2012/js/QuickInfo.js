﻿function createDialog() {
	var dialog = $("<div>", { id: "dialog-quickinfo" }).hide();
	var dialog_background = $("<div>", { id: "dialog-quickinfo-background" });
	var dialog_loading = $("<div>", { id: "dialog-quickinfo-loading" });
	var dialog_quickinfo = $("<div>", { id: "dialog-quickinfo-content" });
	var dialog_close = $("<div>", { id: "dialog-quickinfo-close" }).click(hideDialog);

	dialog.append(dialog_background);
	dialog.append(dialog_quickinfo);
	dialog.append(dialog_loading);
	dialog.append(dialog_close);
	$("body").append(dialog);
}

var dialogPadding = 50;

function showQuickInfo(sender, pid, quickInfoUrl) {
	var documentHeight = $(document).height();
	$("#dialog-quickinfo-background").css("height", documentHeight);

	var dialog_loading = $("#dialog-quickinfo-loading");
	dialog_loading.show();
	$("#dialog-quickinfo").show();

	var dialog_quickinfo = $("#dialog-quickinfo-content");
	dialog_quickinfo.hide();
	$("#dialog-quickinfo-close").hide();

	var windowHeight = $(window).height();
	var windowWidth = $(window).width();

	var loadingTop = ((windowHeight - dialog_loading.height()) / 2) + $(window).scrollTop();
	var loadingLeft = (windowWidth - dialog_loading.width()) / 2;

	if (loadingTop > documentHeight - dialog_loading.height() - dialogPadding) {
		loadingTop = documentHeight - dialog_loading.height() - dialogPadding;
	}
	if (loadingTop < dialogPadding) {
		loadingTop = dialogPadding;
	}

	if (loadingLeft < dialogPadding) {
		loadingLeft = dialogPadding;
	}


	$("body").css("overflow", "hidden");
	var dialog_loading = $("#dialog-quickinfo-loading");

	dialog_loading
	.css("top", loadingTop)
	.css("left", loadingLeft);
	$.ajax({ url: quickInfoUrl,
		cache: false,
		type: "Get",
		data: ({ id: pid }),
		success: function(data) {
			dialog_quickinfo.html(data);
			dialog_quickinfo.show();
			$("#dialog-quickinfo-close").show();
			var contentTop = ((windowHeight - dialog_quickinfo.height()) / 2) + $(window).scrollTop();
			var contentLeft = (windowWidth - dialog_quickinfo.width()) / 2;
			dialog_quickinfo
				.css("top", contentTop)
				.css("left", contentLeft);
			$("#dialog-quickinfo-close")
				.css("top", contentTop)
				.css("left", contentLeft + dialog_quickinfo.width() - 60);
			dialog_loading.hide();
		},
		error: function() {
			dialog_quickinfo.html("<div style='color:#682929;text-align:center;font-weight:bold;'>ขอโทษค่ะ ระบบไม่สามารถแสดงหน้านี้ได้ในขณะนี้</div>");
			dialog_quickinfo.show();
			$("#dialog-quickinfo-close").show();
			var contentTop = ((windowHeight - dialog_quickinfo.height()) / 2) + $(window).scrollTop();
			var contentLeft = (windowWidth - dialog_quickinfo.width()) / 2;
			dialog_quickinfo
					.css("top", contentTop)
					.css("left", contentLeft);
			$("#dialog-quickinfo-close")
					.css("top", contentTop)
					.css("left", contentLeft + dialog_quickinfo.width() - 60);
			dialog_loading.hide();
		},
		timeout: function() {
			dialog_quickinfo.html("<div style='color:#682929;text-align:center;font-weight:bold;'>time out</div>");
			dialog_quickinfo.show();
			$("#dialog-quickinfo-close").show();
			var contentTop = ((windowHeight - dialog_quickinfo.height()) / 2) + $(window).scrollTop();
			var contentLeft = (windowWidth - dialog_quickinfo.width()) / 2;
			dialog_quickinfo
						.css("top", contentTop)
						.css("left", contentLeft);
			$("#dialog-quickinfo-close")
						.css("top", contentTop)
						.css("left", contentLeft + dialog_quickinfo.width() - 60);
			dialog_loading.hide();
		}

	});
}

function hideDialog() {
	$("#dialog-quickinfo").hide();
	$("body").css("overflow", "auto");
}

function initQuickInfo(obj, url) {
	createDialog();
	obj.addClass("icon-quicklook");
	obj.click(function(event) {
		showQuickInfo(this, $(this).find('#pid').val(), url);
	});
}
