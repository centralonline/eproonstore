// JavaScript Document

	$j(function() {
		$j('#nivo').nivoSlider({
			effect:'sliceDownLeft', //Specify sets like: 'fold,fade,sliceDown' //random//boxRainGrow
			slices:12,
			animSpeed:1000,
			pauseTime:7000, // How long each slide will show
        	startSlide:0, // Set starting Slide (0 index)
			boxCols: 8,
			boxRows: 8,
			directionNav:true, //Next and Prev
			directionNavHide:false, //Only show on hover
			controlNav:true //1,2,3...
		});
	});