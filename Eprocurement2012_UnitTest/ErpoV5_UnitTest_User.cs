﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using System.Data.SqlClient;
using Eprocurement2012.Models;
using Eprocurement2012.Models.Repositories;
using Eprocurement2012.Controllers.Helpers;
using Eprocurement2012.Controllers;

namespace Eprocurement2012_UnitTest
{
	[TestFixture]
	public class ErpoV5_UnitTest_User
	{
		private string UserId = "nhungaruthai@officemate.int";
		private string Password = "123456789";
		private string CompanyId = "OFMBO";
		private string guid;
		private UserRepository UserRepository;
		private SiteRepository SiteRepository;
		private SqlConnection Connection;
		[TestFixtureSetUp]
		public void TestFixtureSetUp()
		{

			Connection = new SqlConnection(ConnectDB.ConnectionString);
			UserRepository = new UserRepository(Connection);
			SiteRepository = new SiteRepository(Connection);
			
		}

		[TestFixtureTearDown]
		public void TestFixtureTearDown()
		{
			Connection.Dispose();
		}

		[Test]
		public void SuccessLoginReturnTrueAndGuid()
		{
			bool login = UserRepository.TryGetUserGuid(UserId, Password, out guid);
			Assert.IsTrue(login);
			Assert.IsNotNullOrEmpty(guid);
		}

		[Test]
		public void SuccessCheckUserMultiCompanyReturnTrue()
		{
			bool IsMultiCompany = UserRepository.IsUserMultiCompany(UserId);
			Assert.IsTrue(IsMultiCompany);
		}

		[Test]
		public void SuccessCheckIsForceChangePasswordReturnFalse()
		{
			bool IsForceChange  = UserRepository.IsForceChangePassword(UserId);
			Assert.IsFalse(IsForceChange);
		}

		[Test]
		public void SuccessCheckIsSiteActiveReturnTrue()
		{
			bool IsSiteActive  = SiteRepository.IsSiteActive(CompanyId);
			Assert.IsTrue(IsSiteActive);
		}

		[Test]
		public void SuccessCheckIsOFMAdminReturnTrue()
		{
			bool IsOFMAdmin  = UserRepository.IsOFMAdmin(guid);
			Assert.IsTrue(IsOFMAdmin);
		}

	
		
		
	}
}
