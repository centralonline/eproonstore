﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;

namespace Eprocurement2012_UnitTest
{
	class ConnectDB
	{
		public static string ConnectionString { get { return ConnectDataBase(); } }
		private static string ConnectDataBase()
		{

			string siteConfig = ConfigurationSettings.AppSettings["SetProductionSite"].ToString().ToUpper();
			switch (siteConfig)
			{
				case "P":
					return Production();
				case "I":
					return InUsedOFM();
				case "D":
					return DevServer();
				default:
					return "";
			}
		}
		private static string Production()
		{
			return "Server=10.10.10.2;Database=DBEPROV5;User ID=trendy;Password=tren@dy;";
		}
		private static string InUsedOFM()
		{
			return "Server=DBLog;Database=DBEPROV5;User ID=trendy;Password=tren@dy;";
		}
		private static string DevServer()
		{
			return "Server=Dev-server;Database=DBEPROV5;User ID=sa;Password=ofm@dev;"; ;
		}
	}
}
